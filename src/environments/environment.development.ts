export const environment = {
  serverAuth: "https://auth.pmrms-dev.impstudio.id/",
  serverAwp: "https://awp.pmrms-dev.impstudio.id/",
  serverFile: "https://files.pmrms-dev.impstudio.id/files",
  serverKnowledge: "https://knowledge.pmrms-dev.impstudio.id/",
  serverResource: "https://resources.pmrms-dev.impstudio.id/",
  serverTableuLogin: "https://siapbelajar.kemenag.go.id/",
  production: true,
  defaultauth: "fackbackend",
  serverTableuDashboardPmrms: "http://34.101.129.153/",
  urlTableauProduction: true,
  // untuk penomoran
  serverScrumProject: "https://penomoran.pmrms-dev.impstudio.id/",
};
