export const environment = {
  serverAuth: "http://auth.pmrms-pre-prod.greatpmo.com/",
  serverAwp: "http://awp.pmrms-pre-prod.greatpmo.com/",
  serverFile: "http://files.pmrms-pre-prod.greatpmo.com/files",
  serverKnowledge: "http://knowledge.pmrms-pre-prod.greatpmo.com/",
  serverResource: "http://resources.pmrms-pre-prod.greatpmo.com/",
  serverTableuLogin: "https://siapbelajar.kemenag.go.id/",
  production: true,
  defaultauth: "fackbackend",
  serverTableuDashboardPmrms: "http://34.101.129.153/",
  urlTableauProduction: false,
  // untuk penomoran
  serverScrumProject: "https://penomoran.pmrms-dev.impstudio.id/",
};
