export const environment = {
  serverAuth: "https://auth.pmrms.impstudio.id/",
  serverAwp: "https://awp.pmrms.impstudio.id/",
  serverFile: "https://files.pmrms.impstudio.id/files",
  serverKnowledge: "https://knowledge.pmrms.impstudio.id/",
  serverResource: "https://resources.pmrms.impstudio.id/",
  serverTableuLogin: "https://siapbelajar.kemenag.go.id/",
  production: true,
  defaultauth: "fackbackend",
  serverTableuDashboardPmrms: "http://34.101.129.153/",
  urlTableauProduction: true,
  // untuk penomoran
  serverScrumProject: "https://penomoran.pmrms-dev.impstudio.id/",
};
