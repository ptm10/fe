export const environment = {
  serverAuth: "http://auth.pmrms-dev.greatpmo.com/",
  serverAwp: "http://awp.pmrms-dev.greatpmo.com/",
  serverFile: "http://files.pmrms-dev.greatpmo.com/files",
  serverKnowledge: "http://knowledge.pmrms-dev.greatpmo.com/",
  serverResource: "http://resources.pmrms-dev.greatpmo.com/",
  serverTableuLogin: "https://siapbelajar.kemenag.go.id/",
  production: true,
  defaultauth: "fackbackend",
  serverTableuDashboardPmrms: "http://34.101.129.153/",
  urlTableauProduction: false,
  // untuk penomoran
  serverScrumProject: "https://penomoran.pmrms-dev.impstudio.id/",
};
