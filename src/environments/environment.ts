// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  defaultauth: "fackbackend",
  serverAuth: "http://auth.pmrms-dev.greatpmo.com/",
  serverAwp: "http://awp.pmrms-dev.greatpmo.com/",
  serverFile: "http://files.pmrms-dev.greatpmo.com/files",
  serverKnowledge: "http://knowledge.pmrms-dev.greatpmo.com/",
  serverResource: "http://resources.pmrms-dev.greatpmo.com/",
  serverTableuLogin: "https://siapbelajar.kemenag.go.id/",
  serverTableuDashboardPmrms: "http://34.101.129.153/",
  urlTableauProduction: false,
  // untuk penomoran
  serverScrumProject: "https://penomoran.pmrms-dev.impstudio.id/",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
