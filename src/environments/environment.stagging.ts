export const environment = {
  serverAuth: "http://auth.pmrms.greatpmo.com/",
  serverAwp: "http://awp.pmrms.greatpmo.com/",
  serverFile: "http://files.pmrms.greatpmo.com/files",
  serverKnowledge: "http://knowledge.pmrms.greatpmo.com/",
  serverResource: "http://resources.pmrms.greatpmo.com/",
  serverTableuLogin: "https://siapbelajar.kemenag.go.id/",
  production: true,
  defaultauth: "fackbackend",
  serverTableuDashboardPmrms: "http://34.101.129.153/",
  urlTableauProduction: true,
  // untuk penomoran
  serverScrumProject: "https://penomoran.pmrms-dev.impstudio.id/",
};
