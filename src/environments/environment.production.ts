export const environment = {
  serverAuth: "https://auth.pmrms.kemenag.go.id/",
  serverAwp: "https://awp.pmrms.kemenag.go.id/",
  serverFile: "https://files.pmrms.kemenag.go.id/files",
  serverKnowledge: "https://knowledge.pmrms.kemenag.go.id/",
  serverResource: "https://resources.pmrms.kemenag.go.id/",
  serverTableuLogin: "https://siapbelajar.kemenag.go.id/",
  production: true,
  defaultauth: "fackbackend",
  serverTableuDashboardPmrms: "http://34.101.129.153/",
  urlTableauProduction: true,
  // untuk penomoran
  serverScrumProject: "https://penomoran.pmrms-dev.impstudio.id/",
};
