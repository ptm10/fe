import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UIModule } from './ui/ui.module';

import { WidgetModule } from './widget/widget.module';
import { FooterAplicationComponent } from './footer-aplication/footer-aplication.component';
import { HeaderApplicationComponent } from './header-application/header-application.component';

@NgModule({
  declarations: [
    FooterAplicationComponent,
    HeaderApplicationComponent
  ],
  imports: [
    CommonModule,
    UIModule,
    WidgetModule
  ],
  exports: [
    HeaderApplicationComponent,
    FooterAplicationComponent
  ]
})

export class SharedModule { }
