import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header-application',
  templateUrl: './header-application.component.html',
  styleUrls: ['./header-application.component.scss']
})
export class HeaderApplicationComponent implements OnInit {

  constructor(
      private router: Router
  ) { }

  ngOnInit(): void {
  }

  goToHome() {
    this.router.navigate([''])
  }

}
