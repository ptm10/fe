import {HttpHeaders} from "@angular/common/http";

export class CommonCons {
    static getToken() {
        return 'Bearer ' + localStorage.getItem('accessToken');
    }

    static getHttpOptions() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Content-Type-Options': 'nosniff',
                'Authorization': this.getToken()
            })
        };
    }

    static deleteHttpOptions(idSelected) {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'X-Content-Type-Options': 'nosniff',
                'Authorization': this.getToken()
            }),
            body : {
                id: idSelected
            }
        };
    }

    static getHttpDownloadOptions() {
        return {
            headers: new HttpHeaders({
                'X-Content-Type-Options': 'nosniff',
                'Authorization': this.getToken()
            }),
            responseType: 'blob' as 'json'
        };
    }

    static getHttpOptionsPicture() {
        return {
            headers: new HttpHeaders({
                'Authorization': this.getToken(),
                'Accept': 'image/png,image/jpeg,image/jpg'
            }),
            responseType: 'blob'
        };
    }

    static getHttpTableu() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'text/html; charset=UTF-8',
            }),
            responseType: 'text' as 'json'
        };
    }
}
