import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterAplicationComponent } from './footer-aplication.component';

describe('FooterAplicationComponent', () => {
  let component: FooterAplicationComponent;
  let fixture: ComponentFixture<FooterAplicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterAplicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterAplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
