import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {VizCreateOptions, TableauComponent} from "ngx-tableau";
import {DashboardService} from "../../core/services/Dashboard/dashboard.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit, OnDestroy {

  @ViewChild(TableauComponent)
  tableauComponent: TableauComponent


  showNavigationIndicators: any;
  data: any
  loadingTableau = false
  pathRoute: any

  // Options
  options: VizCreateOptions = {
    hideTabs: false,
    hideToolbar: false,
    width: '100%',
    height: '1100px',
  };

  // url tableau
  urlTableu: string
  prefixTableauProduction: string
  baseUrlTableauPmrms = environment.serverTableuDashboardPmrms

  constructor(
      private router: Router,
      private modalService: NgbModal,
      private service: DashboardService,
  ) { }

  ngOnInit(): void {
    this.pathRoute = this.router.url
    this.loginTableu()
    if (environment.urlTableauProduction) {
      this.prefixTableauProduction = 'prod/summary'
    } else {
      this.prefixTableauProduction = 'dev/summary'
    }
  }

  ngOnDestroy() {
    if (this.tableauComponent) {
      this.tableauComponent?.tableauViz?.dispose()
    }
  }

  loginTableu() {
    this.loadingTableau = true
    this.service.loginTableu().subscribe(
        resp => {
          this.loadingTableau = false
          this.data = resp
          this.urlTableu = 'https://dashboardemis.kemenag.go.id/trusted/' + this.data + '/views/PMRMS_Summary_'+ this.prefixTableauProduction + '?:embed=yes'
        }, error => {
          this.loadingTableau = false
        }
    )
  }


  goToLogin() {
    this.router.navigate(['admin/login'])
  }

  goToComponen1() {
    this.closeModal()
    this.router.navigate(['landing/component1'])
  }

  goToComponen2() {
    this.closeModal()
    this.router.navigate(['landing2/component2'])
  }

  goToComponen3() {
    this.closeModal()
    this.router.navigate(['landing3/component3'])
  }

  goToComponen4() {
    this.closeModal()
    this.router.navigate(['landing4/component4'])
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  goToComponent1() {
    this.closeModal()
    this.router.navigate(['landing/component1'])
  }

}
