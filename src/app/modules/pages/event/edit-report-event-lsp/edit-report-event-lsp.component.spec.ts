import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditReportEventLspComponent } from './edit-report-event-lsp.component';

describe('EditReportEventLspComponent', () => {
  let component: EditReportEventLspComponent;
  let fixture: ComponentFixture<EditReportEventLspComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditReportEventLspComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditReportEventLspComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
