import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailProgressEventComponent } from './detail-progress-event.component';

describe('DetailProgressEventComponent', () => {
  let component: DetailProgressEventComponent;
  let fixture: ComponentFixture<DetailProgressEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailProgressEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailProgressEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
