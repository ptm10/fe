import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EventActivityService} from "../../../../core/services/EventActivity/event-activity.service";
import Swal from "sweetalert2";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {ProgressEventModel} from "../../../../core/models/progress-event.model";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Pdfs} from "../../../../core/models/pdfViewer.model";
import {FileService} from "../../../../core/services/Image/file.service";
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {parse} from "date-fns";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-progress-event',
  templateUrl: './detail-progress-event.component.html',
  styleUrls: ['./detail-progress-event.component.scss']
})
export class DetailProgressEventComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  detailIdProgressEvent: string
  dataDetailEvent: ProgressEventModel
  fieldSubstantiv: number
  showMoreDocumentation = false
  flagApproveReject: number
  loadingSaveData = true
  rejectMessage: string
  participantPmu = []
  participantPcu = []

  //loading
  finishLoadData = true
  finishLoadDataCount = 0

  // pdf
  pdfCountKegiatan: Pdfs[] = [];
  pdfCountRraFile: Pdfs[] = [];
  pdfSrcKegiatan: any
  pdfSrcRraa: any

  ZOOM_STEP = 0.25;
  DEFAULT_ZOOM = 1;

  public zoomIn(pdf: Pdfs) {
    pdf.pdfZoom += this.ZOOM_STEP;
  }

  public zoomOut(pdf: Pdfs) {
    if (pdf.pdfZoom > this.DEFAULT_ZOOM) {
      pdf.pdfZoom -= this.ZOOM_STEP;
    }
  }

  public resetZoom(pdf: Pdfs) {
    pdf.pdfZoom = this.DEFAULT_ZOOM;
  }

  nextPage(pdf: Pdfs) {
    pdf.page += 1;
  }

  previousPage(pdf: Pdfs) {
    pdf.page -= 1;
  }

  afterLoadComplete(pdfData: any, pdf: Pdfs) {
    pdf.totalPages = pdfData.numPages;
  }

  constructor(
      private activatedRoute: ActivatedRoute,
      private service: EventActivityService,
      private router: Router,
      private modalService: NgbModal,
      private serviceImage: FileService,
      private serviceNotification: ProfileService,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Event' }, { label: 'Detail Event' }, { label: 'Detail Laporan kemajuan', active: true }]
    this.detailIdProgressEvent = this.activatedRoute.snapshot.paramMap.get('id')
    this.fieldSubstantiv = 1
    this.detailProgressEvent()
    this.updateStatusTask()
  }

  detailProgressEvent() {
    this.startLoading()
    this.service.detailEventProgress(this.detailIdProgressEvent).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.participantPcu = []
            this.participantPmu = []
            this.dataDetailEvent = resp.data
            this.dataDetailEvent.convertStartDate = new Date(this.dataDetailEvent.startDate)
            this.dataDetailEvent.convertEndDate = new Date(this.dataDetailEvent.endDate)
            this.dataDetailEvent.convertCreatedAt = parse(this.dataDetailEvent.createdAt, 'dd-MM-yyyy HH:mm:ss', new Date());
            if (this.dataDetailEvent?.staff.length > 0) {
              this.dataDetailEvent?.staff.forEach(x => {
                if (x.resources?.user?.component?.code === '-') {
                  this.participantPcu.push(x)
                } else if (x.resources?.user?.component?.code !== '-') {
                  this.participantPmu.push(x)
                }
              })
            }
            if (this.dataDetailEvent?.attachmentFile.fileExt === 'pdf') {
              this.getPdfLampiranViewer(this.dataDetailEvent?.attachmentFile?.id)
            }
            if (this.dataDetailEvent?.rraFile?.fileExt === 'pdf') {
              this.getPdfRrraViewer(this.dataDetailEvent?.rraFile?.id)
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeMenuFieldProgress(menu: any) {
    this.fieldSubstantiv = menu
  }

  goToDetailEvent() {
    this.router.navigate(['activity/detail-event/' + this.dataDetailEvent.eventId])
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  openModalHistory(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  openModal(selectedMenu: any, flagApproveReject: any) {
    this.flagApproveReject = flagApproveReject
    this.modalService.open(selectedMenu, { size: 'md', centered: true });
  }

  showMoreFileDocumentation(showMore: boolean) {
    this.showMoreDocumentation = showMore;
  }
  downloadFile(id, name) {
    // this.loadingDownloadPdf = true
    this.service.exportFile(id, name).subscribe(
        resp => {
          // this.loadingDownloadPdf = false
        }, error => {
          // this.loadingDownloadPdf = false
        }
    )
  }

  getPdfLampiranViewer(id) {
    this.pdfCountKegiatan = []
    this.serviceImage.getUrlPdf(id).subscribe(
        resp => {
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.pdfSrcKegiatan = reader.result;
            const pdf: Pdfs = {
              src: this.pdfSrcKegiatan,
              isCollapsed: false,
              totalPages: 1,
              page: 1,
              pdfZoom: 1
            };
            this.pdfCountKegiatan.push(pdf);
          }, false);
          reader.readAsDataURL(resp);
        }
    )
  }

  getPdfRrraViewer(id) {
    this.pdfCountRraFile = []
    this.serviceImage.getUrlPdf(id).subscribe(
        resp => {
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.pdfSrcRraa = reader.result;
            const pdf: Pdfs = {
              src: this.pdfSrcRraa,
              isCollapsed: false,
              totalPages: 1,
              page: 1,
              pdfZoom: 1
            };
            this.pdfCountRraFile.push(pdf);
          }, false);
          reader.readAsDataURL(resp);
        }
    )
  }

  updateFromConsultant() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdProgressEvent,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromConsultantEventProgress(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailEvent()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  goToEditProgressEvent() {
    this.router.navigate(['activity/edit-progress-event/' + this.detailIdProgressEvent])
  }

  updateStatusTask() {
    const params = {
      taskId: this.detailIdProgressEvent,
      taskType: 7,
      status: 1
    }
    this.service.updateStatusTask(params).subscribe(
        resp => {
          if (resp.success) {
            this.serviceNotification.updateNotification.emit()
          }
        }
    )
  }

}
