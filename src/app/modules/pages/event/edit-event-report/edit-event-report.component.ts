import { Component, OnInit } from '@angular/core';
import {ProgressEventModel} from "../../../../core/models/progress-event.model";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {Event, EventDocumentType, MangementType, PokCode, Province} from "../../../../core/models/awp.model";
import {
  NgbCalendar,
  NgbDate,
  NgbDateParserFormatter,
  NgbDateStruct,
  NgbModal,
  NgbRatingConfig
} from "@ng-bootstrap/ng-bootstrap";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {resourcesModel} from "../../../../core/models/resources.model";
import {ActivatedRoute, Router} from "@angular/router";
import {EventActivityService} from "../../../../core/services/EventActivity/event-activity.service";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import Swal from "sweetalert2";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {EvaluationPoint, ReportEventModel} from "../../../../core/models/report-event.model";
import {parse} from "date-fns";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {QuestionnareEvent} from "../../../../core/models/Questionnare";

@Component({
  selector: 'app-edit-event-report',
  templateUrl: './edit-event-report.component.html',
  styleUrls: ['./edit-event-report.component.scss']
})
export class EditEventReportComponent implements OnInit {

  selectValueParticipant: string[]

  //loading
  finishLoadData = true
  finishLoadDataCount = 0

  // sasaran peserta
  listParticipantTarget = []
  fromParticipantTarget: string
  totalParticipantTarget: number
  listParticipantTargetFromApi: EvaluationPoint[]
  deleteParticipantTargetExisting = []

  dataDetailEventReport: ReportEventModel
  detailProgressEvent: ProgressEventModel
  listEvaluationPoint: EvaluationPoint[]
  breadCrumbItems: Array<{}>;
  detailEventId: string
  // dataDetailEventReport: EventActivityModel
  codeSelected: any
  selectValue = []
  public outputEvent = ClassicEditor;
  public summaryEvent = ClassicEditor;
  public evaluationEvent = ClassicEditor;
  public paragraph1 = ClassicEditor;
  public paragraph2 = ClassicEditor;
  public paragraph3 = ClassicEditor;
  public paragraph4 = ClassicEditor;
  public paragraph5 = ClassicEditor;
  public participantTarget = ClassicEditor;
  fieldSubstantiv: number
  loadingSaveProgressEvent = false
  minDateYear: any
  maxDateYear: any

  //Moda Kegiatan
  listModaType: MangementType[]
  listModaTypeMaster: MangementType[]
  selectedModaActivity: any

  // data event
  selectedComponentExist: ComponentModel
  selectedSubComponentExist: ComponentModel
  selectedSubSubComponentExist: ComponentModel
  kegiatanName: string
  eventName: string
  selectedEvent: Event
  uploadFileExcelAgenda: Array<File> = [];
  uploadFileExcelIdsAgenda: Array<File> = [];
  uploadFileRaa: Array<File> = [];
  uploadFileRaaIdsAgenda: Array<File> = [];
  agendaFile: any
  raaFile: any
  dataIdAttachmentFile: string
  dataIdFileRaa: string
  fileDocumentationId: string

  // POK
  listCodePok: PokCode[]
  listCodePokMaster: PokCode[]
  pokRequest = []
  addPok = []
  removePok = []
  searchPok = []
  pokExisting = []
  deletedPokExisting = []

  // create field
  existOnAnggaran: boolean
  existOnPok: boolean
  budgetAwp: number
  budgetPok: number
  timeOtherInformation: string
  location: string
  eventOutput: string
  participantCount: number
  participantMale: number
  participantFemale: number
  participantOtherInformation: string
  summaryEventOutput: string = null
  documentationEvent = []
  uploadFileDocumentationEvent: Array<File> = [];
  documentationEventName: string
  documentationEventDescription: string
  documentationEventDate: any
  documentationEventPlace: string
  documentationEventToApi = []
  themeEvent: string
  eventCode: string
  targetParticipant: string
  evaluationEventOutput: string = null
  conclutionAndAdvice: string = null
  sourceFounds: string
  yearBudget: any
  MakNumber: string
  totalAnggaranRealitation: number
  budgetCeiling: number
  listPointEvaluation = []
  introductionParagraph1: string = null
  introductionParagraph2: string = null
  introductionParagraph4: string = null
  introductionParagraph5: string = null
  totalDurationParagraph3: string
  schemaEventParagraph3: string
  locationEventParagraph3: string
  involveEventParagraph3: string
  deletedEvaluationExisting = []
  tempatInformasiLainnya: string
  // point evaluation
  evaluationTitle: string
  noteEvaluation: string
  valueEvaluationRating: number = 0
  readonly = true


// date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;

  fromDateIntroduction: Date
  toDateIntroduction: Date

  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  codeEvent: any
  minEvent: NgbDate
  maxEvent: NgbDate
  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  listCityAllExist: Province[]
  listCityAllMasterExist: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []
  listCityFromApi: string[];
  selectedCityFromList = []

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  listProvinceExist: Province[]
  listProvinceExistMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province
  provinceAndRegenciesExist = []
  searchFilterProvinces: any
  listMeqrResourceSelected = []

  // peserta
  listResources: resourcesModel[]
  listResourcesMaster: resourcesModel[]
  selectedResourcesExist: resourcesModel
  resourcesRequest = []
  resourcesRequestIds = []
  addResources = []
  removeResources = []
  searchResources = []
  otherStaffIds = []
  otherStaff = []

  //contact person
  listContactPerson: resourcesModel[]
  listContactPersonMaster: resourcesModel[]
  selectedContactPerson: resourcesModel

  //componen
  selectedUnitExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]

  // unit
  listComponentStaff: ComponentModel[]
  componentExist: ComponentModel[]

  // project officer
  listProjectOfficer = []
  listProjectOfficerMaster = []
  selectedProjectOfficer: resourcesModel
  loadingDataProjectOfficer = false

  nameNewStaff: string
  namePosition: string
  nameInstitution: string
  deletedDocumentIds = []

  typeDocumentation: string[] = []
  selectedTypeDocumentation: string
  listAllDocumentType: EventDocumentType[]

  // question evaluator
  existingQuestionCompliance: QuestionnareEvent[]  = []
  existingQuestionEconomy: QuestionnareEvent[]  = []
  existingQuestionEfective: QuestionnareEvent[]  = []
  existingQuestionEfisiens: QuestionnareEvent[]  = []
  existingQuestionParticipant: QuestionnareEvent[]  = []
  listAllQuestionEvent: QuestionnareEvent[]
  questionAnswerAllCategory = []

  constructor(
      private activatedRoute: ActivatedRoute,
      private serviceEvent: EventActivityService,
      private router: Router,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private serviceAwp: AwpService,
      private modalService: NgbModal,
      private serviceResources: KegiatanService,
      private config: NgbRatingConfig,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Event' }, { label: 'Detail Event'}, { label: 'Detail Laporan Event'}, { label: 'Edit Laporan Event', active: true }]
    this.fieldSubstantiv = 1
    this.detailEventId = this.activatedRoute.snapshot.paramMap.get('id')
    this.getListEventEvaluationPoint()
    this.getDetailEvent()
    this.config.max = 5;
    this.minDateYear = new Date(2020, 1, 1)
    this.maxDateYear = new Date(2024, 11, 30)
    this.yearBudget = new Date()
    this.listAllModa()
    this.getListParticipantTarget()
    this.listEventDocumentType()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  goToResultFormEvaluator() {
    this.router.navigate([]).then(result => {  window.open('activity/summary-evaluator/' + this.dataDetailEventReport?.event?.id); })
  }

  goToResultFormParticipant() {
    this.router.navigate([]).then(result => {  window.open('activity/summary-participant/' + this.dataDetailEventReport?.event?.id); })
  }

  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.serviceAwp.listModa().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */

  /** Input date calender */
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  goToDetailEvent() {
    this.router.navigate(['activity/detail-report-event/' + this.detailEventId + '/2'])
  }

  changeMenuFieldProgress(menu: any) {
    this.fieldSubstantiv = menu
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  calculatePs(event, from) {
    if (this.participantMale || this.participantFemale) {
      if (from == 1) {
        if (this.participantMale < this.participantCount) {
          this.participantFemale = 0
          if (this.participantMale + this.participantFemale < this.participantCount) {
            this.participantFemale = this.participantCount - this.participantMale
          }
        } else {
          this.participantMale = this.participantCount
          this.participantFemale = 0
        }
      } else {
        if (this.participantFemale < this.participantCount) {
          this.participantMale = 0
          if (this.participantMale + this.participantFemale < this.participantCount) {
            this.participantMale = this.participantCount - this.participantFemale
          }
        } else {
          this.participantFemale = this.participantCount
          this.participantMale = 0
        }
      }
    }
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  getDetailEvent() {
    this.startLoading()
    this.serviceEvent.detailEventReport(this.detailEventId).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataDetailEventReport = resp.data
            this.existOnAnggaran = this.dataDetailEventReport?.budgetAvailable
            this.selectedComponentExist = this.dataDetailEventReport?.event?.awpImplementation?.component
            this.selectedSubComponentExist = this.dataDetailEventReport?.event?.awpImplementation?.subComponent
            this.selectedSubSubComponentExist = this.dataDetailEventReport?.event?.awpImplementation?.subSubComponent
            this.codeSelected = this.dataDetailEventReport?.event?.awpImplementation?.componentCode?.code
            this.kegiatanName = this.dataDetailEventReport?.event?.awpImplementation?.name
            this.eventName = this.dataDetailEventReport?.event?.name
            this.selectedEvent = this.dataDetailEventReport?.event?.eventType
            this.existOnPok = this.dataDetailEventReport?.pokEvent
            this.budgetAwp = this.dataDetailEventReport?.event?.awpImplementation?.budgetAwp
            this.budgetPok = this.dataDetailEventReport?.event?.budgetPok
            this.timeOtherInformation = this.dataDetailEventReport?.eventOtherInformation
            this.location = this.dataDetailEventReport?.location
            this.selectedContactPerson = this.dataDetailEventReport?.event?.contactPersonResources
            this.selectedProvinceExist = this.dataDetailEventReport?.province
            this.selectedCityExist = this.dataDetailEventReport?.regency

            this.dataDetailEventReport.event.convertStartDate = new Date(this.dataDetailEventReport.event.startDate)
            this.dataDetailEventReport.event.convertEndDate = new Date(this.dataDetailEventReport.event.endDate)
            this.minEvent = new NgbDate(this.dataDetailEventReport.event.convertStartDate.getFullYear(), this.dataDetailEventReport.event.convertStartDate.getMonth() + 1, this.dataDetailEventReport.event.convertStartDate.getDate())
            this.maxEvent = new NgbDate(this.dataDetailEventReport.event.convertEndDate.getFullYear(), this.dataDetailEventReport.event.convertEndDate.getMonth() + 1, this.dataDetailEventReport.event.convertEndDate.getDate())

            if (this.dataDetailEventReport?.regency) {
              this.selectedProvinceAndCity.push({
                provinceId: this.selectedProvinceExist,
                regencyId: this.selectedCityExist
              })
            } else {
              this.selectedProvinceAndCity.push({
                provinceId: this.selectedProvinceExist,
                regencyId: null
              })
            }
            this.listMeqrResourceSelected = JSON.parse(JSON.stringify(this.resourcesRequest))
            this.participantOtherInformation = this.dataDetailEventReport?.participantOtherInformation
            this.selectedProjectOfficer = this.dataDetailEventReport?.event?.awpImplementation?.projectOfficerResources

            this.dataDetailEventReport.event.staff.forEach(x => {
              this.listProjectOfficer.push(x.resources)
              this.listProjectOfficerMaster.push(x.resources)
            })
            let x = this.dataDetailEventReport.event.staff.filter(staffId => staffId.resources.id === this.selectedProjectOfficer.id)
            if (x.length == 0) {
              this.listProjectOfficer.push(this.selectedProjectOfficer)
              this.listProjectOfficerMaster.push(this.selectedProjectOfficer)
            }
            this.listProjectOfficer = this.listProjectOfficer.filter(x => x.id !== this.selectedProjectOfficer.id)

            this.themeEvent = this.dataDetailEventReport.topic
            this.eventCode = this.dataDetailEventReport.eventCode
            this.dataDetailEventReport.convertStartDate = new Date(this.dataDetailEventReport.startDate)
            this.dataDetailEventReport.convertEndDate = new Date(this.dataDetailEventReport.endDate)
            this.fromDate = new NgbDate(this.dataDetailEventReport.convertStartDate.getFullYear(), this.dataDetailEventReport.convertStartDate.getMonth() + 1, this.dataDetailEventReport.convertStartDate.getDate())
            this.toDate = new NgbDate(this.dataDetailEventReport.convertEndDate.getFullYear(), this.dataDetailEventReport.convertEndDate.getMonth() + 1, this.dataDetailEventReport.convertEndDate.getDate())
            this.eventOutput = this.dataDetailEventReport.eventOutput
            this.targetParticipant = this.dataDetailEventReport.participantTarget
            this.participantMale = this.dataDetailEventReport.participantMale
            this.participantFemale = this.dataDetailEventReport.participantFemale
            this.participantCount = this.dataDetailEventReport.participantCount
            this.participantOtherInformation = this.dataDetailEventReport.participantOtherInformation
            this.summaryEventOutput = this.dataDetailEventReport.implementationSummary
            this.evaluationEventOutput = this.dataDetailEventReport.eventEvaluationDescription
            this.conclutionAndAdvice = this.dataDetailEventReport.conclusionsAndRecommendations
            this.agendaFile = this.dataDetailEventReport.attachmentFile
            this.raaFile = this.dataDetailEventReport.rraFile
            this.sourceFounds = this.dataDetailEventReport.budgetSource
            this.MakNumber = this.dataDetailEventReport.makNumber
            this.totalAnggaranRealitation = this.dataDetailEventReport.rraBudget
            this.budgetCeiling = this.dataDetailEventReport.budgetCeiling
            this.introductionParagraph1 = this.dataDetailEventReport.paragraph1
            this.introductionParagraph2 = this.dataDetailEventReport.paragraph2
            this.introductionParagraph4 = this.dataDetailEventReport.paragraph4
            this.introductionParagraph5 = this.dataDetailEventReport.paragraph5
            this.totalDurationParagraph3 = this.dataDetailEventReport.totalDuration
            this.schemaEventParagraph3 = this.dataDetailEventReport.schemaEvent
            this.dataDetailEventReport.p3StartDateConvertDate = new Date(this.dataDetailEventReport.p3StartDate)
            this.dataDetailEventReport.p3EndDateConvertDate = new Date(this.dataDetailEventReport.p3EndDate)
            this.involveEventParagraph3 = this.dataDetailEventReport.p3Involve
            this.locationEventParagraph3 = this.dataDetailEventReport.p3Location
            this.fromDateIntroduction = parse(this.dataDetailEventReport.startDate, 'yyyy-MM-dd', new Date());
            this.toDateIntroduction = parse(this.dataDetailEventReport.endDate, 'yyyy-MM-dd', new Date());
            this.selectedModaActivity = this.dataDetailEventReport?.activityMode
            if (this.dataDetailEventReport?.pok.length > 0) {
              this.dataDetailEventReport?.pok.forEach(x => {
                this.pokExisting.push(x)
              })
            }
            this.tempatInformasiLainnya = this.dataDetailEventReport?.locationOtherInformation
            this.listProvince()
            this.setTimeRangeEvent()
            this.listComponent()
            this.listPokEvent()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * POK Code */
  listPokEvent() {
    this.startLoading()
    this.serviceResources.listCodePok().subscribe(
        resp => {
          if (resp.success) {
            this.listCodePok = resp.data.content
            this.listCodePokMaster = resp.data.content
            this.searchPok = resp.data.content
            if (this.pokExisting.length > 0) {
              this.pokExisting.forEach(dataPokExist => {
                this.listCodePok = this.listCodePok.filter(x => x.id !== dataPokExist?.pok?.id)
                this.searchPok = this.listCodePok.filter(x => x.id !== dataPokExist?.pok?.id)
              })
            }
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addPokSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pokRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePok.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePok.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPok.push(data)
        }
        this.pokRequest.push(data)
      }
    } else {
      this.pokRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pokRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPok.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPok.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePok.push(data)
      }
    }
    this.listCodePok = this.listCodePokMaster

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.pok?.id)
      })
    }

    this.pokRequest.forEach(dataProvince => {
      this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPok = this.listCodePok
  }

  searchFilterPok(e) {
    const searchStr = e.target.value
    this.listCodePok = this.searchPok.filter((product) => {
      return (product.code.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.title.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePokExisting(data) {
    this.pokExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.deletedPokExisting.push(data.id)
        this.pokExisting.splice(index, 1)
      }
    })
    this.listCodePok = this.listCodePokMaster
    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.pok?.id)
      })
      this.pokRequest.forEach(dataPok => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataPok.id)
      })
    } else {
      this.pokRequest.forEach(dataProvince => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPok = this.listCodePok
  }
  /** * End POK Code */


  clearOldDocuments(data) {
    this.detailProgressEvent?.documents.forEach((check, index) => {
      if (check.id === data.id) {
        this.deletedDocumentIds.push(check.id)
        this.detailProgressEvent?.documents.splice(index, 1)
      }
    })

    this.dataDetailEventReport?.documents.forEach((check, index) => {
      if (check.id === data.id) {
        this.deletedDocumentIds.push(check.id)
        this.dataDetailEventReport?.documents.splice(index, 1)
      }
    })
  }

  /** * List Project Officer */
  selectProjectOfficer(data) {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster

    this.selectedProjectOfficer = data
    this.listProjectOfficer = this.listProjectOfficer.filter(dataFilter => dataFilter.id != this.selectedProjectOfficer.id)
  }

  removeProjectOfficer() {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster
  }

  searchFilterProjectOfficer(e) {
    const searchStr = e.target.value
    let filter = this.listProjectOfficerMaster
    if (this.selectedProjectOfficer) {
      filter = filter.filter(x => x.id !== this.selectedProjectOfficer.id)
    } else {
      filter = this.listProjectOfficerMaster
    }

    this.listProjectOfficer = filter.filter((product) => {
      return ((product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List Project Officer */
  dataResources() {
    let dataListResource = []
    this.loadingDataProjectOfficer = true
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.selectedUnitExist.id
        }
      ],
      paramIn: [],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.serviceResources.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDataProjectOfficer = false
            this.listResources = resp.data.content
            this.listResourcesMaster = resp.data.content
            this.searchResources = resp.data.content
            const result = this.listResources.filter(({ id: id1 }) => !this.resourcesRequest.some(({ id: id2 }) => id2 === id1));
            this.listResources = result
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Add New Unit*/

  selectedUnit(data) {
    this.selectedUnitExist = null
    this.listComponentStaff = this.componentExist

    this.selectedUnitExist = data
    this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedUnitExist.id)
    this.dataResources()
  }

  searchFilterUnit(e) {
    const searchStr = e.target.value
    let filter = this.componentExist
    if (this.selectedUnitExist) {
      filter = filter.filter(x => x.id !== this.selectedUnitExist.id)
    } else {
      filter = this.componentExist
    }

    this.listComponentStaff = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  listComponent() {
    this.serviceResources.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff = resp.data.content
            this.listComponentStaff.forEach(dataCode => {
              dataCode.code = 'Komponen ' + dataCode.code
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  setTimeRangeEvent() {
    let currentDate = new Date()
    // let newConvertDate = new NgbDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate())
    // if (currentDate >= this.dataDetailEventReport.awpImplementation.convertStartDate) {
    //   this.minDate = {
    //     year: newConvertDate.year,
    //     month: newConvertDate.month,
    //     day: newConvertDate.day + 1
    //   };
    // } else {
    //   this.minDate = {
    //     year: this.minEvent.year,
    //     month: this.minEvent.month,
    //     day: this.minEvent.day
    //   };
    // }
    this.minDate = {
      year: this.minEvent.year,
      month: this.minEvent.month,
      day: this.minEvent.day
    };

    this.maxDate = {
      year: this.maxEvent.year,
      month: this.maxEvent.month,
      day: this.maxEvent.day
    };
  }


  /** * List Province */
  listProvince() {
    let dataProvinceExist = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            this.listProvinceExist = resp.data.content
            // this.listProvinceAll = this.listProvinceAll.filter(x => x.id !== this.selectedProvinceExist.id)

            this.dataDetailEventReport?.event?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
              let dataExist = this.listProvinceAll.find(x => dataProvince.province.id == x.id)
              if (dataExist) {
                dataProvinceExist.push(dataExist)
              }
            })
            this.detailProgressEvent?.event?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
              let dataExist = this.listProvinceAll.find(x => dataProvince.province.id == x.id)
              if (dataExist) {
                dataProvinceExist.push(dataExist)
              }
            })

            this.listProvinceExist = dataProvinceExist
            this.listProvinceExistMaster = dataProvinceExist
            this.listCity()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedProvinceExist = null
    this.listProvinceExist = this.listProvinceExistMaster

    this.selectedProvinceExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      this.listProvinceExist = this.listProvinceExistMaster
    }
    console.log(this.listProvinceExistMaster)
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    console.log(searchStr)
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    if (searchStr == '') {
      this.listProvinceExist = this.listProvinceExistMaster
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      return this.listProvinceExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  /** * End List Province */

  /** * List City */

  listCity() {
    let cityExistFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.serviceAwp.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            this.listCityAllExist = resp.data.content
            let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
            if (checkProvinceExist.length > 0) {
              this.dataDetailEventReport?.event?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
                if (dataProvince.regencies.length > 0) {
                  dataProvince.regencies.forEach(dataCity => {
                    let cityExist = this.listCityAll.find(x => dataCity.regency?.id == x.id)
                    if (cityExist) {
                      cityExistFromApi.push(cityExist)
                    }
                  })
                }
              })
              this.detailProgressEvent?.event?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
                if (dataProvince.regencies.length > 0) {
                  dataProvince.regencies.forEach(dataCity => {
                    let cityExist = this.listCityAll.find(x => dataCity.regency?.id == x.id)
                    if (cityExist) {
                      cityExistFromApi.push(cityExist)
                    }
                  })
                }
              })
              this.listCityAllExist = cityExistFromApi
              this.listCityAllMasterExist = JSON.parse(JSON.stringify(this.listCityAllExist))
            } else {
              this.listCityAllExist = this.listCityAll
            }

            this.listCityAllMaster = resp.data.content
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist

    this.selectedCityExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    } else {
      this.listCityAllExist = this.listCityAll
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    }
  }

  searchFilterCity(e) {
    const searchStr = e.target.value
    let filter = this.listCityAll
    if (this.selectedCityExist) {
      filter = filter.filter(x => x.id !== this.selectedCityExist.id)
    } else {
      filter = this.listCityAll
    }

    if (searchStr == '') {
      let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
      if (checkProvinceExist.length > 0) {
        this.listCityAllExist = this.listCityAllMasterExist
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      } else {
        this.listCityAllExist = this.listCityAll
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      }
    } else {
      return this.listCityAllExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  addSelectedProvinceAndCity() {
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityExist
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.selectedProvinceExist = null
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist
    this.listProvinceExist = this.listProvinceExistMaster
    console.log(this.listProvinceExist)
    console.log(this.listProvinceExistMaster)
  }

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      this.fromDateIntroduction = new Date(this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'))
      this.toDateIntroduction = new Date(this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'))
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }
  /** * end date */

  /** New Documentation Event */

  listEventDocumentType() {
    let tag = []
    this.serviceEvent.getListDocumentType().subscribe(
        resp => {
          if (resp.success) {
            this.listAllDocumentType = resp.data.content
            this.listAllDocumentType.forEach(x => {
              tag.push(x.name)
            })
          }
          this.typeDocumentation = tag
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addDocumentationEvent() {
    let startDate
    let selectedTypeDocument

    if (this.documentationEventDate) {
      startDate = this.documentationEventDate.year.toString() + '-' + this.documentationEventDate.month.toString().padStart(2, '0') + '-' + this.documentationEventDate.day.toString().padStart(2, '0')
    } else {
      startDate = null
    }

    selectedTypeDocument = this.listAllDocumentType.filter(x => x.name === this.selectedTypeDocumentation)

    this.documentationEvent.push({
      title: this.documentationEventName,
      date: startDate,
      place: this.documentationEventPlace,
      description: this.documentationEventDescription,
      fileIds: this.uploadFileDocumentationEvent,
      fileId: null,
      eventDocumentTypeId: selectedTypeDocument[0].id,
      eventDocumentTypeName: this.selectedTypeDocumentation

    })
    this.documentationEventName = ''
    this.documentationEventDate = null
    this.documentationEventPlace = ''
    this.documentationEventDescription = ''
    this.selectedTypeDocumentation = ''
    this.uploadFileDocumentationEvent = []
    this.closeModal()
  }
  removeDocumentationFile(data) {
    this.documentationEvent.forEach((check, index) => {
      if (check.title === data.title) {
        this.documentationEvent.splice(index, 1)
      }
    })
  }

  addEvaluationPoint() {
    let filteredExist = this.listEvaluationPoint.filter(x => x.name === this.evaluationTitle)
    if (filteredExist.length > 0) {
      this.listPointEvaluation.push({
        id: this.listPointEvaluation.length + 1,
        newEvaluationPoint: this.evaluationTitle,
        evaluationPointId: filteredExist[0].id,
        value: this.valueEvaluationRating,
        note: this.noteEvaluation
      })
    } else {
      this.listPointEvaluation.push({
        id: this.listPointEvaluation.length + 1,
        newEvaluationPoint: this.evaluationTitle,
        evaluationPointId: null,
        value: this.valueEvaluationRating,
        note: this.noteEvaluation
      })
    }
    this.closeModal()
    this.evaluationTitle = ''
    this.valueEvaluationRating = 0
    this.noteEvaluation = ''
  }

  clearEvaluationPointNew(data) {
    this.listPointEvaluation.forEach((check, index) => {
      if (data.id === check.id) {
        this.listPointEvaluation.splice(index, 1)
      }
    })
  }

  clearEvaluationPointExisting(data) {
    this.dataDetailEventReport.evaluationPoints.forEach((check, index) => {
      if (check.id === data.id) {
        this.deletedEvaluationExisting.push(check.id)
        this.dataDetailEventReport.evaluationPoints.splice(index, 1)
      }
    })
  }

  getListEventEvaluationPoint() {
    this.startLoading()
    let tag = []
    this.serviceEvent.getListEventEvaluation().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            tag = []
            this.listEvaluationPoint = resp.data.content
            this.listEvaluationPoint.forEach(x => {
              tag.push(x.name)
            })
            this.selectValue = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  CreateNew(city){
    return city
  }

  /** New Participant Target Event */


  CreateNewParticipantTarget(data){
    return data
  }

  addNewParticipantTarget() {
    let filtered = this.listParticipantTargetFromApi.filter(x => x.name === this.fromParticipantTarget)
    if (filtered.length > 0) {
      this.listParticipantTarget.push({
        id: this.listParticipantTarget.length + 1,
        newParticipantTarget: this.fromParticipantTarget,
        participantTargetId: filtered[0].id,
        value: this.totalParticipantTarget
      })
    } else {
      this.listParticipantTarget.push({
        id: this.listParticipantTarget.length + 1,
        newParticipantTarget: this.fromParticipantTarget,
        participantTargetId: null,
        value: this.totalParticipantTarget
      })
    }
    this.closeModal()
    this.fromParticipantTarget = ''
    this.totalParticipantTarget = null
  }

  clearNewParticipantTarget(data) {
    this.listParticipantTarget.forEach((check, index) => {
      if (data.id === check.id) {
        this.listParticipantTarget.splice(index, 1)
      }
    })
  }

  clearParticipantTargetExisting(data) {
    this.dataDetailEventReport?.participantTargets.forEach((check, index) => {
      if (data?.id === check.id) {
        this.deleteParticipantTargetExisting.push(data?.id)
        this.dataDetailEventReport?.participantTargets.splice(index, 1)
      }
    })
  }

  getListParticipantTarget() {
    this.startLoading()
    let tag = []
    this.serviceEvent.getListParticipantEvent().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            tag = []
            this.listParticipantTargetFromApi = resp.data.content
            this.listParticipantTargetFromApi.forEach(x => {
              tag.push(x.name)
            })
            this.selectValueParticipant = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** End Participant Target Event */

  /** Upload File Kegiatan */
  doUploadAttachmentFile() {
    this.loadingSaveProgressEvent = true
    if (this.uploadFileExcelAgenda.length > 0) {
      this.serviceResources.uploadAttachment(this.uploadFileExcelAgenda[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdAttachmentFile = resp.data[0].id
              this.doUploadAttachmentRaa()
            } else {
              this.loadingSaveProgressEvent = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            // this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.dataIdAttachmentFile = this.dataDetailEventReport.attachmentFile.id
      this.doUploadAttachmentRaa()
    }
  }

  doUploadAttachmentRaa() {
    if (this.uploadFileRaa.length > 0) {
      this.serviceResources.uploadKegiatanRaa(this.uploadFileRaa[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdFileRaa = resp.data[0].id
              this.doUploadRab()
            } else {
              this.loadingSaveProgressEvent = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            // this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.dataIdFileRaa = this.dataDetailEventReport.rraFile.id
      this.doUploadRab()
    }
  }

  doUploadRab() {
    this.documentationEventToApi = []
    if (this.documentationEvent.length > 0) {
      this.documentationEvent.forEach(data => {
        this.serviceResources.uploadDocumentation(data.fileIds[0]).subscribe(resp => {
              if (resp.success) {
                this.fileDocumentationId = resp.data[0].id
                data.fileId = this.fileDocumentationId
                this.documentationEventToApi.push({
                  title: data?.title,
                  date: data?.date,
                  place: data?.place,
                  description: data?.description,
                  fileId: this.fileDocumentationId,
                  eventDocumentTypeId: data?.eventDocumentTypeId
                })
                if (this.documentationEvent.length == this.documentationEventToApi.length) {
                  this.createReportEvent()
                }
              } else {
                this.loadingSaveProgressEvent = false
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: resp.message,
                });
              }
            }, error => {
              // this.loadingSaveKegiatan = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: error.data[0]?.message,
              });
            }
        );
      })
    } else {
      this.createReportEvent()
    }
  }

  createReportEvent() {
    let regencyId
    let startDate
    let endDate
    let listEvaluationFiltered = JSON.parse(JSON.stringify(this.listPointEvaluation))
    let listEvaluationToApi = []
    let pokRequestApi = []
    let PokExisting = []
    let listParticipantTargetToApi = []
    let listParticipantFiltered = JSON.parse(JSON.stringify(this.listParticipantTarget))

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(x => {
        PokExisting.push(x.id)
      })
    } else {
      PokExisting = []
    }

    if (this.pokRequest.length > 0) {
      this.pokRequest.forEach(x => {
        pokRequestApi.push(x.id)
      })
    }
    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')


    listEvaluationFiltered.forEach(x => {
      if (x.evaluationPointId !== null) {
        x.newEvaluationPoint = null
      }
      listEvaluationToApi.push({
        newEvaluationPoint: x.newEvaluationPoint,
        evaluationPointId: x.evaluationPointId,
        value: x.value,
        note: x.note
      })
    })

    listParticipantFiltered.forEach(x => {
      if (x.participantTargetId !== null) {
        x.newParticipantTarget = null
      }
      listParticipantTargetToApi.push({
        newParticipantTarget: x.newParticipantTarget,
        participantTargetId: x.participantTargetId,
        value: x.value
      })
    })

    if (this.selectedProvinceAndCity[0]?.regencyId) {
      regencyId = this.selectedProvinceAndCity[0]?.regencyId.id
    } else {
      regencyId = null
    }
    let params = {
      eventId: this.dataDetailEventReport.eventId,
      topic: this.themeEvent,
      eventCode: this.eventCode,
      provinceId: this.selectedProvinceAndCity[0].provinceId.id,
      regencyId: regencyId,
      location: this.location,
      participantMale: this.participantMale,
      participantFemale: this.participantFemale,
      participantCount: this.participantCount,
      participantOtherInformation: this.participantOtherInformation,
      participantTarget: this.targetParticipant,
      implementationSummary: this.summaryEventOutput,
      eventEvaluationDescription: this.evaluationEventOutput,
      conclusionsAndRecommendations: this.conclutionAndAdvice,
      startDate: startDate,
      endDate: endDate,
      attachmentFileId: this.dataIdAttachmentFile,
      rraFileId: this.dataIdFileRaa,
      budgetSource: this.sourceFounds,
      budgetYear: this.dataDetailEventReport?.event?.awpImplementation?.awp?.year,
      makNumber: this.MakNumber,
      budgetCeiling: this.budgetCeiling,
      rraBudget: this.totalAnggaranRealitation ,
      paragraph1: this.introductionParagraph1,
      paragraph2: this.introductionParagraph2,
      p3StartDate: startDate,
      p3EndDate: endDate,
      p3Involve: this.involveEventParagraph3,
      paragraph4: this.introductionParagraph4,
      paragraph5: this.introductionParagraph5,
      technicalImplementer: this.selectedProjectOfficer.id,
      totalDuration: this.totalDurationParagraph3,
      schemaEvent: this.schemaEventParagraph3,
      newDocuments: this.documentationEventToApi,
      newEventReportEvaluationPoints: listEvaluationToApi,
      deletedDocumentIds: this.deletedDocumentIds,
      deletedEventReportEvaluationPointsId: this.deletedEvaluationExisting,
      p3EventType: this.selectedEvent.id,
      eventOutput: this.eventOutput,
      p3Location: this.locationEventParagraph3,
      activityModeId: this.selectedModaActivity.id,
      locationOtherInformation: this.tempatInformasiLainnya,
      newPokIds: pokRequestApi,
      deletedPokIds: this.deletedPokExisting,
      newEventReportParticipantTargets: listParticipantTargetToApi,
      deletedEventRerportParticipantTargetsId: this.deleteParticipantTargetExisting,
    }
    this.serviceEvent.updateEventReport(this.detailEventId ,params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveProgressEvent = false
            this.goToDetailEvent()
          }
        }, error => {
          this.loadingSaveProgressEvent = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}

