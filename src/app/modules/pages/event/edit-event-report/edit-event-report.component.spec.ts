import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEventReportComponent } from './edit-event-report.component';

describe('EditEventReportComponent', () => {
  let component: EditEventReportComponent;
  let fixture: ComponentFixture<EditEventReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEventReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEventReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
