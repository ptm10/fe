import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ReportEventModel} from "../../../../core/models/report-event.model";
import {Pdfs} from "../../../../core/models/pdfViewer.model";
import {EventActivityService} from "../../../../core/services/EventActivity/event-activity.service";
import {NgbModal, NgbRatingConfig} from "@ng-bootstrap/ng-bootstrap";
import {FileService} from "../../../../core/services/Image/file.service";
import Swal from "sweetalert2";
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {parse} from "date-fns";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {ProgressEventModel} from "../../../../core/models/progress-event.model";
import {EventQuestion, QuestionnareEvent} from "../../../../core/models/Questionnare";

@Component({
  selector: 'app-detail-report-event',
  templateUrl: './detail-report-event.component.html',
  styleUrls: ['./detail-report-event.component.scss']
})
export class DetailReportEventComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  dataDetailEventReport: ReportEventModel
  fieldSubstantiv: number
  showMoreDocumentation = false
  showMoreEvaluation = false
  showParticipantTarget = false
  flagApproveReject: number
  loadingSaveData = true
  rejectMessage: string
  detailIdReportEvent: string
  fromPage: string
  readonly = true
  lspId = '';
  participantPmu = []
  participantPcu = []
  detailProgressEvents: ProgressEventModel

  //loading
  finishLoadData = true
  finishLoadDataCount = 0

  // pdf
  pdfCountKegiatan: Pdfs[] = [];
  pdfCountRraFile: Pdfs[] = [];
  pdfSrcKegiatan: any
  pdfSrcRraa: any

  ZOOM_STEP = 0.25;
  DEFAULT_ZOOM = 1;

  public zoomIn(pdf: Pdfs) {
    pdf.pdfZoom += this.ZOOM_STEP;
  }

  public zoomOut(pdf: Pdfs) {
    if (pdf.pdfZoom > this.DEFAULT_ZOOM) {
      pdf.pdfZoom -= this.ZOOM_STEP;
    }
  }

  public resetZoom(pdf: Pdfs) {
    pdf.pdfZoom = this.DEFAULT_ZOOM;
  }

  nextPage(pdf: Pdfs) {
    pdf.page += 1;
  }

  previousPage(pdf: Pdfs) {
    pdf.page -= 1;
  }

  afterLoadComplete(pdfData: any, pdf: Pdfs) {
    pdf.totalPages = pdfData.numPages;
  }

  // list question
  existingQuestionCompliance: QuestionnareEvent[]  = []
  existingQuestionEconomy: QuestionnareEvent[]  = []
  existingQuestionEfective: QuestionnareEvent[]  = []
  existingQuestionEfisiens: QuestionnareEvent[]  = []
  existingQuestionParticipant: QuestionnareEvent[]  = []
  listAllQuestionEvent: QuestionnareEvent[]

  constructor(
      private activatedRoute: ActivatedRoute,
      private service: EventActivityService,
      private router: Router,
      private modalService: NgbModal,
      private serviceImage: FileService,
      private config: NgbRatingConfig,
      private serviceNotification: ProfileService,
      public roles: AuthfakeauthenticationService,
      private swalAlert: SwalAlert,
      private serviceEvent: EventActivityService,
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Event' }, { label: 'Detail Event' }, { label: 'Detail Laporan Event', active: true }]
    this.detailIdReportEvent = this.activatedRoute.snapshot.paramMap.get('id')
    this.fromPage = this.activatedRoute.snapshot.paramMap.get('from')
    this.detailProgressEvent()
    this.fieldSubstantiv = 1
    this.config.max = 5;
    this.updateStatusTask()
  }

  detailProgressEvent() {
    this.startLoading()
    this.service.detailEventReport(this.detailIdReportEvent).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.participantPcu = []
            this.participantPmu = []
            this.dataDetailEventReport = resp.data
            this.lspId = resp.data.createdByUser.lspId;
            this.dataDetailEventReport.convertStartDate = new Date(this.dataDetailEventReport.startDate)
            this.dataDetailEventReport.convertEndDate = new Date(this.dataDetailEventReport.endDate)
            this.dataDetailEventReport.convertCreatedAt = parse(this.dataDetailEventReport.createdAt, 'dd-MM-yyyy HH:mm:ss', new Date());
            if (this.dataDetailEventReport?.attachmentFile.fileExt === 'pdf') {
              this.getPdfLampiranViewer(this.dataDetailEventReport?.attachmentFile?.id)
            }
            if (this.dataDetailEventReport?.rraFile.fileExt === 'pdf') {
              this.getPdfRrraViewer(this.dataDetailEventReport?.rraFile?.id)
            }
            if (this.dataDetailEventReport?.event?.staff.length > 0) {
              this.dataDetailEventReport?.event?.staff.forEach(x => {
                if (x.resources?.user?.component?.code === '-') {
                  this.participantPcu.push(x)
                } else if (x.resources?.user?.component?.code !== '-') {
                  this.participantPmu.push(x)
                }
              })
            }
            this.getDetailLastReport()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getDetailLastReport() {
    this.startLoading()
    this.serviceEvent.detailEventLastReport(this.dataDetailEventReport?.event?.id).subscribe(
        resp => {
          if (resp.success) {
            // this.participantPcu = []
            // this.participantPmu = []
            this.stopLoading()
            this.detailProgressEvents = resp.data
            if (this.detailProgressEvents.id) {
              this.participantPcu = []
              this.participantPmu = []
              this.detailProgressEvents?.staff.forEach(x => {
                if (x.resources?.user?.component?.code === '-') {
                  this.participantPcu.push(x)
                } else if (x.resources?.user?.component?.code !== '-') {
                  this.participantPmu.push(x)
                }
              })
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeMenuFieldProgress(menu: any) {
    this.fieldSubstantiv = menu
  }

  goToDetailEvent() {
    if (this.fromPage === '1') {
      this.router.navigate(['implement/detail-kegiatan/' + this.dataDetailEventReport?.event?.awpImplementation?.id])
    } else if (this.fromPage === '2') {
      this.router.navigate(['activity/detail-event/' + this.dataDetailEventReport.eventId])
    } else {
      history.back()
    }
  }

  goToResultFormEvaluator() {
    this.router.navigate([]).then(result => {  window.open('activity/summary-evaluator/' + this.dataDetailEventReport?.event?.id); })
  }

  goToResultFormParticipant() {
    this.router.navigate([]).then(result => {  window.open('activity/summary-participant/' + this.dataDetailEventReport?.event?.id); })
  }

  openQuestionForParticipant(url: string){
    window.open(url, "_blank");
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  openModalHistory(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  openModal(selectedMenu: any, flagApproveReject: any) {
    this.flagApproveReject = flagApproveReject
    this.modalService.open(selectedMenu, { size: 'md', centered: true });
  }

  openModalQuestion(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  showMoreFileDocumentation(showMore: boolean) {
    this.showMoreDocumentation = showMore;
  }

  showMoreFileEvaluation(showMore: boolean) {
    this.showMoreEvaluation = showMore;
  }

  showMoreParticipantTarget(showMore: boolean) {
    this.showParticipantTarget = showMore;
  }

  downloadFile(id, name) {
    // this.loadingDownloadPdf = true
    this.service.exportFile(id, name).subscribe(
        resp => {
          // this.loadingDownloadPdf = false
        }, error => {
          // this.loadingDownloadPdf = false
        }
    )
  }

  getPdfLampiranViewer(id) {
    this.pdfCountKegiatan = []
    this.serviceImage.getUrlPdf(id).subscribe(
        resp => {
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.pdfSrcKegiatan = reader.result;
            const pdf: Pdfs = {
              src: this.pdfSrcKegiatan,
              isCollapsed: false,
              totalPages: 1,
              page: 1,
              pdfZoom: 1
            };
            this.pdfCountKegiatan.push(pdf);
          }, false);
          reader.readAsDataURL(resp);
        }
    )
  }

  getPdfRrraViewer(id) {
    this.pdfCountRraFile = []
    this.serviceImage.getUrlPdf(id).subscribe(
        resp => {
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.pdfSrcRraa = reader.result;
            const pdf: Pdfs = {
              src: this.pdfSrcRraa,
              isCollapsed: false,
              totalPages: 1,
              page: 1,
              pdfZoom: 1
            };
            this.pdfCountRraFile.push(pdf);
          }, false);
          reader.readAsDataURL(resp);
        }
    )
  }

  updateFromConsultant() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdReportEvent,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromConsultantEventReport(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailEvent()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromCoordinator() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdReportEvent,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromCoordinatorEventReport(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailEvent()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromTreasure() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdReportEvent,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromTreasureEventReport(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailEvent()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromHeadPmu() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdReportEvent,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromHeadPmuEventReport(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailEvent()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  goToEditProgressEvent() {
    if (this.roles.hasAuthority('LSP')) {
      this.router.navigate(['activity/edit-report-event-lsp/' + this.detailIdReportEvent])
    } else {
      this.router.navigate(['activity/edit-report-event/' + this.detailIdReportEvent])
    }
  }

  updateStatusTask() {
    const params = {
      taskId: this.detailIdReportEvent,
      taskType: 8,
      status: 1
    }
    this.service.updateStatusTask(params).subscribe(
        resp => {
          if (resp.success) {
            this.serviceNotification.updateNotification.emit()
          }
        }
    )
  }

  downloadWordAwp() {
    this.loadingSaveData = false
    this.service.exportWordAwpEventReport(this.detailIdReportEvent, this.dataDetailEventReport?.event?.name).subscribe(
        resp => {
          this.loadingSaveData = true
          this.closeModal()
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


}
