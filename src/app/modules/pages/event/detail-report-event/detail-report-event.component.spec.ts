import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailReportEventComponent } from './detail-report-event.component';

describe('DetailReportEventComponent', () => {
  let component: DetailReportEventComponent;
  let fixture: ComponentFixture<DetailReportEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailReportEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailReportEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
