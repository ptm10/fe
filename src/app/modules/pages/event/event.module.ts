import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule,
    NgbDatepickerModule,
    NgbRatingModule,
    NgbTimepickerModule,
    NgbAccordionModule,
    NgbToastModule,
    NgbAlertModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {EventRoutingModule} from './event-routing.module'
import {DataTablesModule} from 'angular-datatables';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import {UIModule} from "../../../shared/ui/ui.module";
import {ArchwizardModule} from "angular-archwizard";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {FileUploadModule} from "@iplab/ngx-file-upload";
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import {EventComponent} from "./event.component";
import { DetailEventComponent } from './detail-event/detail-event.component';
import { EditEventComponent } from './edit-event/edit-event.component';
import {SimplebarAngularModule} from "simplebar-angular";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {FullCalendarModule} from "@fullcalendar/angular";
import {DateHelper} from "../../../core/helpers/date-helper";
import { AddProgressEventComponent } from './add-progress-event/add-progress-event.component';
import { DetailProgressEventComponent } from './detail-progress-event/detail-progress-event.component';
import { EditProgressEventComponent } from './edit-progress-event/edit-progress-event.component';
import { AddReportEventComponent } from './add-report-event/add-report-event.component';
import { DetailReportEventComponent } from './detail-report-event/detail-report-event.component';
import { EditEventReportComponent } from './edit-event-report/edit-event-report.component';
import { AddReportEventLspComponent } from './add-report-event-lsp/add-report-event-lsp.component';
import { EditReportEventLspComponent } from './edit-report-event-lsp/edit-report-event-lsp.component';
import {SwalAlert} from "../../../core/helpers/Swal";
import {ClipboardModule} from "@angular/cdk/clipboard";
import { SummaryQuestionnaireParticipantComponent } from './summary-questionnaire-participant/summary-questionnaire-participant.component';
import {NgApexchartsModule} from "ng-apexcharts";
import { ChartQuestionnaireComponent } from './summary-questionnaire-participant/chart-questionnaire/chart-questionnaire.component';
import { SummaryQuestionnaireEvaluatorComponent } from './summary-questionnaire-evaluator/summary-questionnaire-evaluator.component';
import { ChartQuestionnaireEvaluatorComponent } from './summary-questionnaire-evaluator/chart-questionnaire-evaluator/chart-questionnaire-evaluator.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;


@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [EventComponent, DetailEventComponent, EditEventComponent, AddProgressEventComponent, DetailProgressEventComponent, EditProgressEventComponent, AddReportEventComponent, DetailReportEventComponent, EditEventReportComponent, AddReportEventLspComponent, EditReportEventLspComponent, SummaryQuestionnaireParticipantComponent, ChartQuestionnaireComponent, SummaryQuestionnaireEvaluatorComponent, ChartQuestionnaireEvaluatorComponent],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        EventRoutingModule,
        DataTablesModule,
        NgbTooltipModule,
        NgxMaskModule.forRoot(),
        UIModule,
        ArchwizardModule,
        NgbDatepickerModule,
        BsDatepickerModule,
        NgbRatingModule,
        FileUploadModule,
        CKEditorModule,
        NgbTimepickerModule,
        SimplebarAngularModule,
        NgbAccordionModule,
        PdfViewerModule,
        FullCalendarModule,
        ClipboardModule,
        NgApexchartsModule,
        NgbToastModule,
        NgbAlertModule,
    ],
  providers: [
      DateHelper,
      SwalAlert
  ]
})
export class EventModule { }
