import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EventComponent} from "./event.component";
import {DetailEventComponent} from "./detail-event/detail-event.component";
import {EditEventComponent} from "./edit-event/edit-event.component";
import {AddProgressEventComponent} from "./add-progress-event/add-progress-event.component";
import {DetailProgressEventComponent} from "./detail-progress-event/detail-progress-event.component";
import {EditProgressEventComponent} from "./edit-progress-event/edit-progress-event.component";
import {AddReportEventComponent} from "./add-report-event/add-report-event.component";
import {DetailReportEventComponent} from "./detail-report-event/detail-report-event.component";
import {EditEventReportComponent} from "./edit-event-report/edit-event-report.component";
import {AddReportEventLspComponent} from "./add-report-event-lsp/add-report-event-lsp.component";
import {EditReportEventLspComponent} from "./edit-report-event-lsp/edit-report-event-lsp.component";
import {
    SummaryQuestionnaireParticipantComponent
} from "./summary-questionnaire-participant/summary-questionnaire-participant.component";
import {
    SummaryQuestionnaireEvaluatorComponent
} from "./summary-questionnaire-evaluator/summary-questionnaire-evaluator.component";
import {AuthGuard} from "../../../core/guards/auth.guard";



const routes: Routes = [
    {
        path: 'event',
        component: EventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-event/:id',
        component: DetailEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'edit-event/:id',
        component: EditEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'progress-event/:id',
        component: AddProgressEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-progress-event/:id',
        component: DetailProgressEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'edit-progress-event/:id',
        component: EditProgressEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'add-report-event/:id',
        component: AddReportEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-report-event/:id/:from',
        component: DetailReportEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'edit-report-event/:id',
        component: EditEventReportComponent, canActivate: [AuthGuard]
    },
    {
        path: 'add-report-event-lsp/:id',
        component: AddReportEventLspComponent, canActivate: [AuthGuard]
    },
    {
        path: 'edit-report-event-lsp/:id',
        component: EditReportEventLspComponent, canActivate: [AuthGuard]
    },
    {
        path: 'summary-participant/:id',
        component: SummaryQuestionnaireParticipantComponent, canActivate: [AuthGuard]
    },
    {
        path: 'summary-evaluator/:id',
        component: SummaryQuestionnaireEvaluatorComponent, canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EventRoutingModule {}
