import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {Kegiatan, Province} from "../../../core/models/awp.model";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../core/services/Awp/awp.service";
import {Router} from "@angular/router";
import {leangueIndoDT} from "../../../shared/pagination-custom";
import {DataResponse} from "../../../core/models/dataresponse.models";
import {environment} from "../../../../environments/environment";
import {CommonCons} from "../../../shared/CommonCons";
import Swal from "sweetalert2";
import {EventActivityModel} from "../../../core/models/event-activity.model";
import {parse} from "date-fns";
import {AuthfakeauthenticationService} from "../../../core/services/authfake.service";
import {
  NgbCalendar,
  NgbDate,
  NgbDateParserFormatter,
  NgbDateStruct,
  NgbModal,
  NgbNavChangeEvent
} from "@ng-bootstrap/ng-bootstrap";
import {EventActivityService} from "../../../core/services/EventActivity/event-activity.service";
import {ManagementHolidays} from "../../../core/models/permission.model";
import {AdministrationService} from "../../../core/services/Administration/administration.service";
import {CalendarOptions, EventClickArg, EventInput} from "@fullcalendar/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {DateHelper} from "../../../core/helpers/date-helper";
import {FullCalendarComponent} from "@fullcalendar/angular";
import {KegiatanService} from "../../../core/services/kegiatan.service";
import {ComponentModel} from "../../../core/models/componentModel";
import {SwalAlert} from "../../../core/helpers/Swal";
import { Clipboard } from '@angular/cdk/clipboard'
import {EventFormEmailParticipant} from "../../../core/models/event.model";
@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {
  @ViewChild('modalShow') modalShow: TemplateRef<any>;
  @ViewChild('editmodalShow') editmodalShow: TemplateRef<any>;
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtElement2: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtOptions2: DataTables.Settings = {};
  dtOptions3: DataTables.Settings = {};
  filterData: any
  filterDataParticipant: any
  filterDataEvaluator: any
  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCalender = true
  finishLoadDataCount = 0
  start = 1
  modelEvent: EventActivityModel[]
  modelKegiatan: Kegiatan[]
  modelKegiatanMaster: Kegiatan[]
  filterYear: number
  selectedYear: number
  dataListYear = []
  dataListYearExist = []
  paramsLike = []
  paramsIs = []
  componentLogin: string
  checkedButton: number
  editEvent: any;
  showToast = false

  // upload email peserta
  uploadFileExcelForm: Array<File> = [];
  selectedForFormEmail: number
  selectedEvent: EventActivityModel
  loadingUploadFormParticipant = false
  idFileSelected: string
  modelSuccessEmailParticipant: EventFormEmailParticipant
  loadingGetDataSuccessEmailParticipant = false

  // filter table participant
  paramsIsParticipant = []
  paramsLikeParticipant = []
  paramDateBetweenParticipant = []
  modelEventParticipant: EventActivityModel[]

  // filter table Evaluator
  paramsIsEvaluator = []
  paramsLikeEvaluator = []
  paramDateBetweenEvaluator = []
  modelEventEvaluator: EventActivityModel[]

  //date participant
  fromDateParticipant: NgbDate | null;
  toDateParticipant: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  hoveredDateParticipant: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []

  //date evaluator
  fromDateEvaluator: NgbDate | null;
  toDateEvaluator: NgbDate | null;
  hoveredDateEvaluator: NgbDate | null = null;

  // unit
  checkedComponent = []
  checkedComponentWithoutUser = []
  listComponentStaff = []
  componentExist: ComponentModel[]
  selectedComponentExist: ComponentModel

  // filter unit
  checkedComponentFilter = []
  listComponentStaffFilter = []
  componentExistFilter: ComponentModel[]
  checkedComponentFilterFilter = []

  //code
  selectedCode: any = null
  selectedCodeExisting: any
  listCode: any
  listCodeExisting: any
  kegiatan: string

  // filter
  selectComponent: string[] = [];
  selectedFilterComponent: string
  selectSubComponent: string[] = [];
  selectedFilterSubComponent: string
  filterSubComponent = []

  // filter province regencies
  selectProvince: string[] = []
  selectedFilterProvince: string
  selectCity: string[] = []
  selectedFilterTableProvince = []
  selectedFilterTableRegencies = []
  selectedFilterCity: string
  listProvince: Province[]
  listRegencies: Province[]

  //date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;

  holidayManagement: ManagementHolidays[]
  listHolidays = []
  paramDateBetween = []

  // calender
  dataCalender: EventInput[]
  calenderModelEvent: EventActivityModel[]
  dataCalenderInput: EventInput[] = []
  tempDataCalender: EventInput[] = []
  startDate: Date
  endDate: Date
  id: string
  tittle: string
  backgroundColorEventCalender: string
  formData: FormGroup;
  formEditData: FormGroup;
  titleDate: Date
  date: Date
  existingDate: Date
  nextDate: Date
  previousDate: Date
  tittleEventSelected: string
  descriptionEventSelected: string
  staffEvent = []
  taskTypeEventSelected: number
  listDescriptionEventSelected: any
  listStaffEvent = []
  otherStaffEvent = []
  province: any
  regencies: any
  startDateEvent: any
  endDateEvent: any
  checkedSelfEvent = false
  checkedPmuEventStaff = false
  checkedStaffEvent: boolean = true
  checkedStaffSelf: boolean = true
  checkedStaffOther: boolean = false
  unitLogin: string
  loadingExportWord = false

  // model calender
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: '',
      right: ''
    },
    initialView: "dayGridMonth",
    themeSystem: "bootstrap",
    initialEvents: [],
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    // dateClick: this.openModalCalender.bind(this),
    eventClick: this.handleEventClick.bind(this),
    displayEventTime: false,
    customButtons: {
      myCustomButton: {
        text: 'custom!',
        click: function() {
          alert('clicked the custom button!');
        }
      }
    },
  };

  constructor(
      private http: HttpClient,
      private service: AwpService,
      private serviceResources: KegiatanService,
      private serviceEvent: EventActivityService,
      private router: Router,
      public roles: AuthfakeauthenticationService,
      private modalService: NgbModal,
      public formatter: NgbDateParserFormatter,
      private serviceAdministration: AdministrationService,
      private calendar: NgbCalendar,
      private dateHelper: DateHelper,
      private formBuilder: FormBuilder,
      private swalAlert: SwalAlert,
      private serviceAwp: AwpService,
      private clipboard: Clipboard,
  ) { }

  showAlertCopy() {
    this.showToast = true
    setTimeout(() => this.showToast = false, 2000)
  }
  closeAlertCopy() {
    console.log('testing')
    this.showToast = false
  }

  ngOnInit(): void {
    this.getListProvince()
    this.checkedButton = 1
    this.unitLogin = localStorage.getItem('unit')
    this.componentLogin = localStorage.getItem('code_component')
    this.breadCrumbItems = [{ label: 'Event', active: true }];
    this.getDataTable()
    this.getListYear()
    this.getDataActivity()
    this.date = new Date()
    this.existingDate = new Date()
    this.nextDate = new Date(this.existingDate.setMonth(this.existingDate.getMonth() + 1))
    this.previousDate = this.dateHelper.minusDate(new Date(this.date), 1)
    this.titleDate = new Date()
    this.listComponent()
    setTimeout( function() {
      window.dispatchEvent(new Event('resize'))
    }, 1)
    this.listComponentFilter()
  }

  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  myClassParticipant(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  myClassEvaluator(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  openQuestionForParticipant(url: string){
    window.open(url, "_blank");
  }

  goToSummaryQuestionnaire(id) {
    this.router.navigate(['activity/summary-participant/' + id])
  }

  goToSummaryQuestionnaireEvaluator(id) {
    this.router.navigate(['activity/summary-evaluator/' + id])
  }
  /**
   * function fetch data event
   */
  getDataTable() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramsIs,
          paramIn: [],
          paramNotIn: [],
          paramDateBetween: this.paramDateBetween,
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'event/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.modelEvent = resp.data.content;
            this.modelEvent.forEach(data => {
              data.convertStartDate = parse(data.startDate, 'yyyy-MM-dd', new Date());
              data.convertEndDate = parse(data.endDate, 'yyyy-MM-dd', new Date());
            })
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'updatedAt' },{ data: 'awpImplementation.component.code' },{ data: 'awpImplementation.componentCode.code' },{ data: 'awpImplementation.name' },{ data: 'name' }, { data: 'startDate' }, { data: 'province.name' }, { data: 'status' }, { data: 'createdAt' }],
      order: [[1, 'asc']]
    };
  }

  /**
   * function fetch data participant
   */
  getDataTableParticipant() {
    this.startLoading()
    this.dtOptions2 = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLikeParticipant,
          paramIs: this.paramsIsParticipant,
          paramIn: [],
          paramNotIn: [],
          paramDateBetween: this.paramDateBetweenParticipant,
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'event/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.modelEventParticipant = resp.data.content;
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'updatedAt' },{ data: 'awpImplementation.componentCode.code' },{ data: 'name' }, { data: 'status' }, { data: 'name' }, { data: 'name' }],
      order: [[2, 'asc']]
    };
  }

  /**
   * function fetch data evaluator
   */
  getDataTableEvaluator() {
    this.startLoading()
    this.dtOptions3 = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLikeEvaluator,
          paramIs: this.paramsIsEvaluator,
          paramIn: [],
          paramNotIn: [],
          paramDateBetween: this.paramDateBetweenEvaluator,
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'event/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.modelEventEvaluator = resp.data.content;
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'updatedAt' },{ data: 'awpImplementation.componentCode.code' },{ data: 'name' }, { data: 'status' }, { data: 'name' }, { data: 'name' }],
      order: [[2, 'asc']]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.filterYear = this.selectedYear
    this.paramsIs = []
    this.paramDateBetween = []

    if (this.fromDate && this.toDate) {
      let startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
      let endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')
      const selectedDateStartDate = {
        field: "startDate",
        startDate: startDate,
        endDate: endDate,
      }
      this.paramDateBetween.push(selectedDateStartDate)
    }

    if (this.selectedYear) {
      const selectedYearChoosed = {
        field: "awpImplementation.awp.year",
        dataType: "int",
        value: this.filterYear
      }
      this.paramsIs.push(selectedYearChoosed)
    }

    if (this.checkedComponentFilterFilter.length > 0) {
      const selectedComponent = {
        field: "awpImplementation.componentId",
        dataType: "string",
        value: this.checkedComponentFilterFilter[0]
      }
      this.paramsIs.push(selectedComponent)
    }

    if (this.filterSubComponent.length > 0) {
      const selectedSubComponent = {
        field: "awpImplementation.subComponentId",
        dataType: "string",
        value: this.filterSubComponent[0].id
      }
      this.paramsIs.push(selectedSubComponent)
    }

    if (this.selectedFilterTableProvince.length > 0) {
      const selectedProvince = {
        field: "provinceId",
        dataType: "string",
        value: this.selectedFilterTableProvince[0]
      }
      this.paramsIs.push(selectedProvince)
    }

    if (this.selectedFilterTableRegencies.length > 0) {
      const selectedProvince = {
        field: "regencyId",
        dataType: "string",
        value: this.selectedFilterTableRegencies[0]
      }
      this.paramsIs.push(selectedProvince)
    }

    this.paramsLike = []
    if (this.filterData) {
      const paramsFirstName = {
        field: "name",
        dataType: "string",
        value: this.filterData
      }
      const paramsComponentCode =  {
        field: "awpImplementation.componentCode.code",
        dataType: "string",
        value: this.filterData
      }
      const paramsComponentName = {
        field: "awpImplementation.name",
        dataType: "string",
        value: this.filterData
      }
      const paramsEventName = {
        field: "name",
        dataType: "string",
        value: this.filterData
      }
      this.paramsLike.push(paramsComponentCode)
      this.paramsLike.push(paramsFirstName)
      this.paramsLike.push(paramsComponentName)
      this.paramsLike.push(paramsEventName)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  /**
   * function refresh datatable participant
   */
  refreshDatatableParticipant(): void {
    this.filterYear = this.selectedYear
    this.paramsIsParticipant = []
    this.paramDateBetweenParticipant = []

    this.paramsIsParticipant.push({
      field: "status",
      dataType: "int",
      value: 6
    })

    if (this.fromDateParticipant && this.toDateParticipant) {
      let startDate = this.fromDateParticipant.year.toString() + '-' + this.fromDateParticipant.month.toString().padStart(2, '0') + '-' + this.fromDateParticipant.day.toString().padStart(2, '0')
      let endDate = this.toDateParticipant.year.toString() + '-' + this.toDateParticipant.month.toString().padStart(2, '0') + '-' + this.toDateParticipant.day.toString().padStart(2, '0')
      const selectedDateStartDate = {
        field: "startDate",
        startDate: startDate,
        endDate: endDate,
      }
      this.paramDateBetweenParticipant.push(selectedDateStartDate)
    }

    this.paramsLikeParticipant = []
    if (this.filterDataParticipant) {
      const paramsFirstName = {
        field: "name",
        dataType: "string",
        value: this.filterDataParticipant
      }
      const paramsComponentCode =  {
        field: "awpImplementation.componentCode.code",
        dataType: "string",
        value: this.filterDataParticipant
      }
      this.paramsLikeParticipant.push(paramsFirstName)
      this.paramsLikeParticipant.push(paramsComponentCode)
    } else {
      this.paramsLikeParticipant = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  /**
   * function refresh datatable Evaluator
   */
  refreshDatatableEvaluator(): void {
    this.filterYear = this.selectedYear
    this.paramsIsEvaluator = []
    this.paramDateBetweenEvaluator = []

    this.paramsIsEvaluator.push({
      field: "status",
      dataType: "int",
      value: 6
    })

    if (this.fromDateEvaluator && this.toDateEvaluator) {
      let startDate = this.fromDateEvaluator.year.toString() + '-' + this.fromDateEvaluator.month.toString().padStart(2, '0') + '-' + this.fromDateEvaluator.day.toString().padStart(2, '0')
      let endDate = this.toDateEvaluator.year.toString() + '-' + this.toDateEvaluator.month.toString().padStart(2, '0') + '-' + this.toDateEvaluator.day.toString().padStart(2, '0')
      const selectedDateStartDate = {
        field: "startDate",
        startDate: startDate,
        endDate: endDate,
      }
      this.paramDateBetweenEvaluator.push(selectedDateStartDate)
    }

    this.paramsLikeEvaluator = []
    if (this.filterDataEvaluator) {
      const paramsFirstName = {
        field: "name",
        dataType: "string",
        value: this.filterDataEvaluator
      }
      const paramsComponentCode =  {
        field: "awpImplementation.componentCode.code",
        dataType: "string",
        value: this.filterDataEvaluator
      }
      this.paramsLikeEvaluator.push(paramsFirstName)
      this.paramsLikeEvaluator.push(paramsComponentCode)
    } else {
      this.paramsLikeEvaluator = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  /**
   * Calender Participant
   */

  isHoveredParticipant(date: NgbDate) {
    return this.fromDateParticipant && !this.toDateParticipant && this.hoveredDateParticipant && date.after(this.fromDateParticipant) && date.before(this.hoveredDateParticipant);
  }

  isInsideParticipant(date: NgbDate) {
    return this.toDateParticipant && date.after(this.fromDateParticipant) && date.before(this.toDateParticipant);
  }

  isRangeParticipant(date: NgbDate) {
    return date.equals(this.fromDateParticipant) || (this.toDateParticipant && date.equals(this.toDateParticipant)) || this.isInsideParticipant(date) || this.isHoveredParticipant(date);
  }

  onDateSelectionParticipant(date: NgbDate) {
    if (!this.fromDateParticipant && !this.toDateParticipant) {
      this.fromDateParticipant = date;
    } else if (this.fromDateParticipant && !this.toDateParticipant && date && date.after(this.fromDateParticipant) || date.equals(this.fromDateParticipant)) {
      this.toDateParticipant = date;
      this.refreshDatatableParticipant()
    } else {
      this.toDateParticipant = null;
      this.fromDateParticipant = date;
    }
  }

  /**
   * Calender Evaluator
   */

  isHoveredEvaluator(date: NgbDate) {
    return this.fromDateEvaluator && !this.toDateEvaluator && this.hoveredDateEvaluator && date.after(this.fromDateEvaluator) && date.before(this.hoveredDateEvaluator);
  }

  isInsideEvaluator(date: NgbDate) {
    return this.toDateEvaluator && date.after(this.fromDateEvaluator) && date.before(this.toDateEvaluator);
  }

  isRangeEvaluator(date: NgbDate) {
    return date.equals(this.fromDateEvaluator) || (this.toDateEvaluator && date.equals(this.toDateEvaluator)) || this.isInsideEvaluator(date) || this.isHoveredEvaluator(date);
  }

  onDateSelectionEvaluator(date: NgbDate) {
    if (!this.fromDateEvaluator && !this.toDateEvaluator) {
      this.fromDateEvaluator = date;
    } else if (this.fromDateEvaluator && !this.toDateEvaluator && date && date.after(this.fromDateEvaluator) || date.equals(this.fromDateEvaluator)) {
      this.toDateEvaluator = date;
      this.refreshDatatableEvaluator()
    } else {
      this.toDateEvaluator = null;
      this.fromDateEvaluator = date;
    }
  }

  refreshCalender(changeEvent: NgbNavChangeEvent) {
    if (changeEvent.nextId === 2) {
      this.finishLoadDataCalender = true
      this.listCalenderEvent()
    } else if (changeEvent.nextId === 3) {
      this.paramsIsParticipant = []
      this.paramsIsParticipant.push({
        field: "status",
        dataType: "int",
        value: 6
      })
      this.getDataTableParticipant()
    } else if (changeEvent.nextId === 1) {
      this.getDataTable()
    } else if (changeEvent.nextId === 4) {
      this.paramsIsEvaluator = []
      this.paramsIsEvaluator.push({
        field: "status",
        dataType: "int",
        value: 6
      })
      this.getDataTableEvaluator()
    }
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  selectedAwpYear(data) {
    this.selectedYear = data
    this.refreshDatatable()
  }

  clearFilterYear() {
    this.selectedYear = null
    this.refreshDatatable()
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.dataListYear = this.dataListYearExist.filter((product) => {
      return product.toString().search(searchStr.toString().toLowerCase()) !== -1
    });
  }

  /**
   * get list year
   */
  getListYear() {
    this.startLoading()
    this.service.listYear().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataListYear = []
            this.dataListYear = resp.data
            this.dataListYearExist = resp.data
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.stopLoading()
          // this.finishLoadData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  goToDetailKegiatan(id, status) {
    this.router.navigate(['activity/detail-event/' + id])
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'md', centered: true });
  }

  openModalForm(selectedMenu: any, formFor: number, selectedEventForm: EventActivityModel, uploadForm: boolean) {
    this.selectedForFormEmail = null
    this.selectedEvent = null
    this.selectedEvent = selectedEventForm
    this.selectedForFormEmail = formFor
    if (!uploadForm) {
      this.checkStatusEmailStatus()
    }
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  goToCreateCn(data) {
    this.closeModal()
    this.router.navigate(['implement/add-concept-note/' + data.id + '/2'])
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  getDataActivity() {
    this.startLoading()
    const params = {
      enablePage: false,
      sort: [
        {
          field: "componentCode.code",
          direction: "asc"
        }
      ]
    }
    this.serviceEvent.getListActivity(params).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.modelKegiatan = resp.data.content
            this.modelKegiatanMaster = resp.data.content
            this.modelKegiatan = this.modelKegiatan.filter(x => x.status === 5)
            this.modelKegiatanMaster = this.modelKegiatanMaster.filter(x => x.status === 5)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCodeFromList(data) {
    this.selectedCode = null
    this.modelKegiatan = this.modelKegiatanMaster

    this.selectedCode = data
    this.modelKegiatan = this.modelKegiatan.filter(data => data.componentCode.id !== this.selectedCode.componentCode.id)
    this.kegiatan = this.selectedCode.name
    console.log(this.kegiatan)
  }

  clearSelectedCode() {
    this.selectedCode = null
    this.modelKegiatan = this.modelKegiatanMaster
    this.kegiatan = ''
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      this.refreshDatatable()
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  getListHolidayManagement() {
    this.listHolidays = []
    this.serviceAdministration.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  clearFilterDate() {
    this.fromDate = null
    this.toDate = null
    this.refreshDatatable()
  }

  clearFilterDateParticipant() {
    this.fromDateParticipant = null
    this.toDateParticipant = null
    this.refreshDatatableParticipant()
  }

  clearFilterDateEvaluator() {
    this.fromDateEvaluator = null
    this.toDateEvaluator = null
    this.refreshDatatableEvaluator()
  }

  /** Change Data Calender */

  listCalenderEvent() {
    let params
    if (this.roles.hasAuthority('LSP')) {
      params = {
        self: true,
        staff: false,
        componentIds: []
      }
    } else {
      params = {
        self: this.checkedSelfEvent,
        staff: this.checkedPmuEventStaff,
        componentIds: this.checkedComponentWithoutUser
      }
    }
    
    /** Change Data Calender */
    this.service.calenderEvent(params).subscribe(
        resp => {
          if (resp.success) {
            this.tempDataCalender = []
            this.calenderModelEvent = resp.data
            this.calenderModelEvent.forEach(data => {
              data.backgroundColorCalender = ''
              this.startDate = new Date(data.startDate)
              this.endDate = new Date(data.endDate)
              this.id = data.id
              this.tittle = data.name
              if (data.awpImplementation.component.code === '1') {
                data.backgroundColorCalender = 'bg-komponen1 text-white'
              } else if (data.awpImplementation.component.code === '2') {
                data.backgroundColorCalender = 'bg-komponen2 text-white'
              } else if (data.awpImplementation.component.code === '3') {
                data.backgroundColorCalender = 'bg-komponen3 text-white'
              } else if (data.awpImplementation.component.code === '4') {
               if (data.awpImplementation.subComponent.code === '4.1') {
                  data.backgroundColorCalender = 'bg-komponen41 text-white'
                } else if (data.awpImplementation.subComponent.code === '4.2') {
                  data.backgroundColorCalender = 'bg-komponen42 text-white'
                } else if (data.awpImplementation.subComponent.code === '4.3') {
                  data.backgroundColorCalender = 'bg-komponen43 text-white'
                } else if (data.awpImplementation.subComponent.code === '4.4') {
                  data.backgroundColorCalender = 'bg-komponen44 text-white'
                }
              }
              this.backgroundColorEventCalender = data.backgroundColorCalender
              this.tempDataCalender.push({
                id: this.id,
                title: this.tittle,
                start: this.startDate,
                end: this.endDate,
                className: this.backgroundColorEventCalender,
                extendedProps : {
                  description: data.description,
                  staffEvent: data.staff,
                  otherStaffEvent: data.otherStaff,
                  province: data.province,
                  regencies: data.regency,
                  startDate: new Date(data.startDate),
                  endDate: new Date(data.endDate)
                }
              })
            })
            // this.finishLoadData = true
            setTimeout(() => {
              this.dataCalenderInput = this.tempDataCalender
              this.calendarOptions.events = this.dataCalenderInput
              this.finishLoadDataCalender = false
              this.checkedComponent = this.listComponentStaff.filter(data => data.checked == true)
            }, 1000 )
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeDateCalender() {
    this.titleDate = this.date
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate(this.date);
  }

  moveDateCalender(from: number) {
    if (from === 1) {
      this.date.setMonth(this.date.getMonth() + 1)
    } else {
      this.date = this.dateHelper.minusDate(new Date(this.date), 1)
    }
    this.existingDate = new Date(this.date)
    this.nextDate = new Date(this.existingDate.setMonth(this.existingDate.getMonth() + 1))
    this.previousDate = this.dateHelper.minusDate(new Date(this.date), 1)
    this.titleDate = new Date(this.date)
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate(this.date);
  }

  /*** Event click modal show */
  handleEventClick(clickInfo: EventClickArg) {
    this.tittleEventSelected = ''
    this.descriptionEventSelected = ''
    this.tittleEventSelected = clickInfo.event.title
    this.descriptionEventSelected = clickInfo.event.extendedProps.description
    this.taskTypeEventSelected = clickInfo.event.extendedProps.taskType
    this.listStaffEvent = clickInfo.event.extendedProps.staffEvent
    this.otherStaffEvent = clickInfo.event.extendedProps.otherStaffEvent
    this.province = clickInfo.event.extendedProps.province
    this.regencies = clickInfo.event.extendedProps.regencies
    this.startDateEvent = clickInfo.event.extendedProps.startDate
    this.endDateEvent = clickInfo.event.extendedProps.endDate
    this.editEvent = clickInfo.event;
    this.formEditData = this.formBuilder.group({
      editTitle: clickInfo.event.title,
      dataDescription: clickInfo.event.extendedProps.description,
      start: clickInfo.event.start
    });
    this.modalService.open(this.editmodalShow, { centered: true });
  }

  /** Close Modal */
  closeEventModal() {
    this.formData = this.formBuilder.group({
      title: '',
      category: '',
    });
    this.modalService.dismissAll();
  }

  listComponent() {
    this.serviceResources.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff.push({
              id: 1,
              code: 'Saya'
            })
            this.listComponentStaff.push({
              id: 2,
              code: 'Staf',
            })
            this.componentExist.forEach(dataCode => {
              dataCode.checked = false
              dataCode.code = 'Komponen ' + dataCode.code
              this.listComponentStaff.push(dataCode)
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedComponent() {
    this.checkedComponent = this.listComponentStaff.filter(data => data.checked == true)
    let filterComponentSelected = this.listComponentStaff.filter(data => data.checked == true && data.id !== 1 && data.id !== 2)
    let filterComponentUser = this.listComponentStaff.filter(data => data.checked == true && data.id == 1)
    let filterComponentStaff = this.listComponentStaff.filter(data => data.checked == true && data.id == 2)
    this.checkedComponentWithoutUser = filterComponentSelected.filter(data => data.checked).map(data => data.id)
    this.checkedSelfEvent = filterComponentUser.length > 0;
    this.checkedPmuEventStaff = filterComponentStaff.length > 0;
    this.listCalenderEvent()
  }

  clearFilter() {
    this.checkedComponent = []
    this.listComponentStaff.forEach(data => {
      data.checked = false
    })
    this.checkedComponentWithoutUser = []
    this.checkedSelfEvent = false
    this.checkedPmuEventStaff = false
    console.log(this.listComponentStaff)
    this.listCalenderEvent()
  }

  changeFilterOtherPmu() {
    if (this.checkedStaffEvent) {
      this.checkedStaffSelf = true
      this.checkedStaffOther = false
    } else {
      this.checkedStaffSelf = false
      this.checkedStaffOther = true
    }
    this.listCalenderEvent()
  }

  clearFilterOtherPmu() {
    this.checkedStaffSelf = false
    this.checkedStaffOther = false
    this.checkedStaffEvent = null
    this.listCalenderEvent()
  }

  // filter component
  listComponentFilter() {
    let tag = []
    this.service.listAllComponent().subscribe(
        resp => {
          if (resp.success) {
            this.componentExistFilter = resp.data
            this.componentExistFilter.forEach(dataCode => {
              dataCode.checked = false
              dataCode.code = 'Komponen ' + dataCode.code
              this.listComponentStaffFilter.push(dataCode)
              tag.push(dataCode.code)
            })
            this.selectComponent = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeFilterComponent() {
    this.checkedComponentFilterFilter = []
    let tagSubComponent = []
    let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
    let filteredComponent = JSON.parse(JSON.stringify(this.componentExistFilter))
    this.selectedFilterSubComponent = null
    if (this.selectedFilterComponent) {
      findComponent = findComponent.replace('komponen ', '')
      filteredComponent = filteredComponent.filter(x => x.code === findComponent)
      if (filteredComponent.length > 0) {
        tagSubComponent = []
        this.checkedComponentFilterFilter.push(filteredComponent[0]?.id)
        filteredComponent[0].subComponent.forEach(x => {
          x.code = 'Sub-Komponen ' + x.code
          tagSubComponent.push(x.code)
        })
        this.selectSubComponent = []
        this.selectSubComponent = tagSubComponent
      }
    }
    this.refreshDatatable()
  }

  changeFilterSubComponent() {
    let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
    let findSubComponent = JSON.parse(JSON.stringify(this.selectedFilterSubComponent))
    let filteredComponent = JSON.parse(JSON.stringify(this.componentExistFilter))
    let selectedFilterSubComponent
    if (this.selectedFilterSubComponent) {
      findComponent = findComponent.replace('komponen ', '')
      filteredComponent = filteredComponent.filter(x => x.code === findComponent)
      if (filteredComponent) {
        findSubComponent = findSubComponent.replace('Sub-Komponen ', '')
        selectedFilterSubComponent = filteredComponent[0].subComponent.filter(x =>x.code === findSubComponent)
        this.filterSubComponent = selectedFilterSubComponent
      }
      this.refreshDatatable()
    }
  }

  clearFilterComponent() {
    this.selectedFilterComponent = null
    this.selectedFilterSubComponent = null
    this.selectSubComponent = []
    this.checkedComponentFilter = []
    this.filterSubComponent = []
    this.refreshDatatable()
  }

  getListProvince() {
    let tag = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvince = resp.data.content
            this.listProvince.forEach(x => {
              tag.push(x.name)
            })
            this.selectProvince = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  downloadWordConceptNote(data) {
    this.loadingExportWord = true
    this.service.exportWordAwpConceptNote(data.id, data.name).subscribe(
        resp => {
          this.loadingExportWord = false
          this.closeModal()
        }, error => {
          this.loadingExportWord = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeFilterProvince() {
    this.selectedFilterTableProvince = []
    let findProvince = JSON.parse(JSON.stringify(this.listProvince))
    let filteredProvince = JSON.parse(JSON.stringify(this.listProvince))
    this.selectedFilterCity = null
    if (this.selectedFilterProvince) {
      console.log(filteredProvince)
      filteredProvince = filteredProvince.filter(x => x.name === this.selectedFilterProvince)
      if (filteredProvince.length > 0) {
        this.selectedFilterTableProvince.push(filteredProvince[0]?.id)
      }
    }
    this.listCity()
    this.refreshDatatable()
  }

  clearFilterProvinceRegencies() {
    this.selectedFilterCity = null
    this.selectedFilterProvince = null
    this.selectedFilterTableProvince = []
    this.selectedFilterTableRegencies = []
    this.refreshDatatable()
  }

  changeFilterCity() {
    this.selectedFilterTableRegencies = []
    let findRegencies = JSON.parse(JSON.stringify(this.listRegencies))
    let filteredRegencies = JSON.parse(JSON.stringify(this.listRegencies))
    if (this.selectedFilterCity) {
      filteredRegencies = filteredRegencies.filter(x => x.name === this.selectedFilterCity)
      if (filteredRegencies.length > 0) {
        this.selectedFilterTableRegencies.push(filteredRegencies[0]?.id)
      }
    }
    this.refreshDatatable()
  }

  listCity() {
    this.selectCity = []
    let tag = []
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedFilterTableProvince[0]
        }
      ]
    }
    this.serviceAwp.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.listRegencies = resp.data.content
            this.listRegencies.forEach(x => {
              tag.push(x.name)
            })
            this.selectCity = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /**
   * Download Template Excel
   */
  exportExcelTempalateForm() {
    this.loadingExportWord = true
    this.service.exportWordTemplateForm().subscribe(
        resp => {
          this.loadingExportWord = false
        }, error => {
          this.loadingExportWord = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /**
 * Upload Form Email Participant
 */

  doUploadFileEmailParticipant(uploadFrom: number) {
    this.loadingUploadFormParticipant = true
    if (this.uploadFileExcelForm.length > 0) {
      this.serviceResources.uploadFileEmailParticipant(this.uploadFileExcelForm[0]).subscribe(resp => {
            if (resp.success) {
              this.idFileSelected = resp.data[0].id
              this.doUploadFileExcelToEvent(uploadFrom)
            } else {
              this.loadingUploadFormParticipant = false
            }
          }, error => {
            this.loadingUploadFormParticipant = false
            this.swalAlert.showAlertSwal(error)
        }
      );
    }
  }

  doUploadFileExcelToEvent(uploadFrom: number) {
    let params = {
      fileId: this.idFileSelected,
      eventId: this.selectedEvent?.id,
      createdFor: uploadFrom
    }
    this.service.uploadFormUser(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingUploadFormParticipant = false
            this.uploadFileExcelForm = []
            this.closeModal()
            if (uploadFrom === 1) {
              this.refreshDatatableParticipant()
            } else if (uploadFrom === 2) {
              this.refreshDatatableEvaluator()
            }
          }
        }, error => {
          this.loadingUploadFormParticipant = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  checkStatusEmailStatus() {
    this.loadingGetDataSuccessEmailParticipant = true
    this.service.resultFormUser(this.selectedEvent?.id, this.selectedForFormEmail).subscribe(
        resp => {
          if (resp.success) {
            this.loadingGetDataSuccessEmailParticipant = false
            this.modelSuccessEmailParticipant = resp.data
          }
        }, error => {
          this.loadingGetDataSuccessEmailParticipant = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
