import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {ActivatedRoute, Router} from "@angular/router";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {EventActivityService} from "../../../../core/services/EventActivity/event-activity.service";
import Swal from "sweetalert2";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {Event, MangementType, PokCode, Province} from "../../../../core/models/awp.model";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {parse} from "date-fns";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {resourcesModel} from "../../../../core/models/resources.model";
import {data} from "jquery";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {ListUnit} from "../../../../core/models/RoleUsers";
import {ProfileService} from "../../../../core/services/Profile/profile.service";

@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {

  public latarBelakangEvent = ClassicEditor;
  public eventDescription = ClassicEditor;
  public purposeEvent = ClassicEditor;
  public outputEvent = ClassicEditor;
  public participantTarget = ClassicEditor;

  detailIdCn: string
  fromPage: string
  breadCrumbItems: Array<{}>;
  dataDetailEvent: EventActivityModel

  //edit data
  eventName: string
  timeOtherInformation: string
  location: string
  background: string
  description: string
  purpose: string
  eventOutput: string
  participantCount: number
  participantMale: number
  participantFemale: number
  participantOtherInformation: string
  existOnAnggaran: boolean
  loadingSaveKegiatan = false
  dataIdRab: string
  dataIdKegiatan: string
  tempatInformasiLainnya: string
  targetParticipant: string = null
  // data exist
  kegiatanName: string
  existOnPok: boolean
  budgetAwp: number
  budgetPok: number
  otherStaff = []
  otherStaffIds = []
  nameNewStaff: string
  namePosition: string
  nameInstitution: string
  rabFile: any
  agendaFile: any

  // POK
  listCodePok: PokCode[]
  listCodePokMaster: PokCode[]
  pokRequest = []
  addPok = []
  removePok = []
  searchPok = []
  pokExisting = []

  //loading component
  finishLoadData = true
  finishLoadDataCount = 0

  //Moda Kegiatan
  listModaType: MangementType[]
  listModaTypeMaster: MangementType[]
  selectedModaActivity: any

  // unit
  listComponentStaff: ComponentModel[]
  componentExist: ComponentModel[]
  selectedComponentExist: ComponentModel
  // sub componen
  selectedSubComponentExist: ComponentModel
  selectedSubComponentExistMaster: ComponentModel
  selectedSubComponentExistEdit: ComponentModel

  //sub-sub componen
  selectedSubSubComponentExist: ComponentModel
  selectedSubSubComponentExistMaster: ComponentModel
  selectedSubSubComponentExistEdit: ComponentModel
  codeSelected: any = 'pilih'
  listCodeSelected = []

  // event
  listEvent: Event[]
  listEventMaster: Event[]
  selectedEvent: Event
  selectedEventTypeIsPenyedia: boolean

  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  codeEvent: any
  minEvent: NgbDate
  maxEvent: NgbDate
  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  listCityAllExist: Province[]
  listCityAllMasterExist: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []
  listCityFromApi: string[];
  selectedCityFromList = []

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  listProvinceExist: Province[]
  listProvinceExistMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province
  provinceAndRegenciesExist = []
  searchFilterProvinces: any
  listMeqrResourceSelected = []

  // peserta
  listResources: resourcesModel[]
  listResourcesMaster: resourcesModel[]
  selectedResourcesExist: resourcesModel
  resourcesRequest = []
  resourcesRequestIds = []
  addResources = []
  removeResources = []
  searchResources = []
  listMeqrPmuSelected = []
  listMeqrPcuSelected = []

  //contact person
  listContactPerson: resourcesModel[]
  listContactPersonMaster: resourcesModel[]
  selectedContactPerson: resourcesModel

  //componen
  selectedUnitExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]

  // project officer
  listProjectOfficer: resourcesModel[]
  listProjectOfficerMaster: resourcesModel[]
  selectedProjectOfficer: resourcesModel
  loadingDataProjectOfficer = false

  // upload file
  uploadFileExcel: Array<File> = [];
  uploadFileExcelIds: Array<File> = [];
  uploadFileExcelAgenda: Array<File> = [];
  uploadFileExcelIdsAgenda: Array<File> = [];

  //work unit
  selectedWorkUnit: string
  listUnit: ListUnit[]

  //province
  listProvinceAllStaff: Province[]
  listProvinceAllMasterStaff: Province[]
  listProvinceApi: string[];
  selectedProvinceStaff: string

  constructor(
      private activatedRoute: ActivatedRoute,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private service: EventActivityService,
      public roles: AuthfakeauthenticationService,
      private serviceAwp: AwpService,
      private serviceAdministration: AdministrationService,
      private modalService: NgbModal,
      private serviceResources: KegiatanService,
      private router: Router,
      private swalAlert: SwalAlert,
      private serviceProfile: ProfileService
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Event' }, { label: 'Detail Event'}, { label: 'Edit Concept Note', active: true }]
    this.detailIdCn = this.activatedRoute.snapshot.paramMap.get('id')
    this.getDetailCn()
    this.listAllModa()
    this.getListWorkUnit()
    this.listProvinceStaff()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** * Work Unit */

  changeSelectedWorkUnit() {
    this.selectedUnitExist = null
    this.listComponentStaff = this.componentExist
    this.selectedProvinceStaff = null
  }

  changeUserStaffPcu() {
    this.dataResources()
  }

  getListWorkUnit() {
    this.startLoading()
    this.serviceProfile.listUnit().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listUnit = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  checkValidationAddStaff() {
    if (this.selectedWorkUnit) {
      if (this.selectedWorkUnit === 'PMU') {
        return !this.selectedUnitExist
      } else if (this.selectedWorkUnit === 'PCU') {
        return !this.selectedProvinceStaff
      }
    } else {
      return true
    }
  }
  /** * End Work Unit */

  /** * List Province */

  listProvinceStaff() {
    let tag = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAllStaff = resp.data.content
            this.listProvinceAllMasterStaff = resp.data.content
            this.listProvinceAllStaff.forEach(x => {
              tag.push(x.name)
            })
            this.listProvinceApi = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End List Province */

  /** * POK Code */
  listPokEvent() {
    this.startLoading()
    this.serviceResources.listCodePok().subscribe(
        resp => {
          if (resp.success) {
            this.listCodePok = resp.data.content
            this.listCodePokMaster = resp.data.content
            this.searchPok = resp.data.content
            if (this.pokExisting.length > 0) {
              this.pokExisting.forEach(dataPokExist => {
                this.listCodePok = this.listCodePok.filter(x => x.id !== dataPokExist?.pok?.id)
                this.searchPok = this.listCodePok.filter(x => x.id !== dataPokExist?.pok?.id)
              })
            }
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addPokSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pokRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePok.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePok.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPok.push(data)
        }
        this.pokRequest.push(data)
      }
    } else {
      this.pokRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pokRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPok.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPok.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePok.push(data)
      }
    }
    this.listCodePok = this.listCodePokMaster

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.pok?.id)
      })
    }

    this.pokRequest.forEach(dataProvince => {
      this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPok = this.listCodePok
  }

  searchFilterPok(e) {
    const searchStr = e.target.value
    this.listCodePok = this.searchPok.filter((product) => {
      return (product.code.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.title.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePokExisting(data) {
    this.pokExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.pokExisting.splice(index, 1)
      }
    })
    this.listCodePok = this.listCodePokMaster
    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.pok?.id)
      })
      this.pokRequest.forEach(dataPok => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataPok.id)
      })
    } else {
      this.pokRequest.forEach(dataProvince => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPok = this.listCodePok
  }
  /** * End POK Code */

  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.serviceAwp.listModa().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */


  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  goToListKegiatan() {
    this.router.navigate(['activity/detail-event/' + this.detailIdCn])
  }

  back(){
    history.back()
  }

  getDetailCn() {
    this.startLoading()
    this.service.detailEvent(this.detailIdCn).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataDetailEvent = resp.data

            this.selectedComponentExist = this.dataDetailEvent?.awpImplementation?.component
            this.selectedSubComponentExist = this.dataDetailEvent?.awpImplementation?.subComponent
            if (this.dataDetailEvent?.awpImplementation?.subSubComponent) {
              this.selectedSubSubComponentExist = this.dataDetailEvent?.awpImplementation?.subSubComponent
            }
            this.codeSelected = this.dataDetailEvent?.awpImplementation?.componentCode?.code
            this.kegiatanName = this.dataDetailEvent?.awpImplementation?.name
            this.existOnPok = this.dataDetailEvent?.pokEvent
            this.budgetAwp = this.dataDetailEvent?.awpImplementation?.budgetAwp
            this.budgetPok = this.dataDetailEvent?.budgetPok

            this.eventName = this.dataDetailEvent?.name
            this.selectedEvent = this.dataDetailEvent?.eventType
            this.timeOtherInformation = this.dataDetailEvent?.eventOtherInformation

            this.dataDetailEvent.awpImplementation.convertStartDate = new Date(this.dataDetailEvent.awpImplementation.startDate)
            this.dataDetailEvent.awpImplementation.convertEndDate = new Date(this.dataDetailEvent.awpImplementation.endDate)
            this.minEvent = new NgbDate(this.dataDetailEvent.awpImplementation.convertStartDate.getFullYear(), this.dataDetailEvent.awpImplementation.convertStartDate.getMonth() + 1, this.dataDetailEvent.awpImplementation.convertStartDate.getDate())
            this.maxEvent = new NgbDate(this.dataDetailEvent.awpImplementation.convertEndDate.getFullYear(), this.dataDetailEvent.awpImplementation.convertEndDate.getMonth() + 1, this.dataDetailEvent.awpImplementation.convertEndDate.getDate())

            this.dataDetailEvent.convertStartDate = new Date(this.dataDetailEvent.startDate)
            this.dataDetailEvent.convertEndDate = new Date(this.dataDetailEvent.endDate)
            this.fromDate = new NgbDate(this.dataDetailEvent.convertStartDate.getFullYear(), this.dataDetailEvent.convertStartDate.getMonth() + 1, this.dataDetailEvent.convertStartDate.getDate())
            this.toDate = new NgbDate(this.dataDetailEvent.convertEndDate.getFullYear(), this.dataDetailEvent.convertEndDate.getMonth() + 1, this.dataDetailEvent.convertEndDate.getDate())

            this.selectedProvinceExist = this.dataDetailEvent?.province
            this.selectedCityExist = this.dataDetailEvent?.regency
            this.location = this.dataDetailEvent?.location
            this.selectedModaActivity = this.dataDetailEvent?.activityMode
            if (this.dataDetailEvent?.regency) {
              this.selectedProvinceAndCity.push({
                provinceId: this.selectedProvinceExist,
                regencyId: this.selectedCityExist
              })
            } else {
              this.selectedProvinceAndCity.push({
                provinceId: this.selectedProvinceExist,
                regencyId: null
              })
            }
            this.dataDetailEvent?.staff.forEach(dataStaff => {
              this.resourcesRequest.push(dataStaff.resources)
              this.resourcesRequestIds.push(dataStaff)
            })
            if (this.dataDetailEvent?.otherStaff.length > 0) {
              this.dataDetailEvent?.otherStaff.forEach(dataOtherStaff => {
                this.otherStaffIds.push(dataOtherStaff)
              })
            }
            if (this.dataDetailEvent?.pok.length > 0) {
              this.dataDetailEvent?.pok.forEach(x => {
                this.pokExisting.push(x)
              })
            }
            this.background = this.dataDetailEvent?.background
            this.description = this.dataDetailEvent?.description
            this.purpose = this.dataDetailEvent?.purpose
            this.eventOutput = this.dataDetailEvent?.eventOutput
            this.participantCount = this.dataDetailEvent?.participantCount
            this.participantMale = this.dataDetailEvent?.participantMale
            this.participantFemale = this.dataDetailEvent?.participantFemale
            this.participantOtherInformation = this.dataDetailEvent?.participantOtherInformation
            this.selectedContactPerson = this.dataDetailEvent?.contactPersonResources
            this.listContactPersonMaster = JSON.parse(JSON.stringify(this.resourcesRequest))
            this.listContactPerson = JSON.parse(JSON.stringify(this.resourcesRequest))
            this.listContactPerson = this.listContactPerson.filter(x => x.id !== this.selectedContactPerson.id)
            this.rabFile = this.dataDetailEvent?.rabFile
            this.agendaFile = this.dataDetailEvent?.agendaFile
            this.tempatInformasiLainnya = this.dataDetailEvent?.locationOtherInformation
            this.targetParticipant = this.dataDetailEvent?.participantTarget
            this.listMeqrResourceSelected = JSON.parse(JSON.stringify(this.resourcesRequest))
            this.listMeqrResourceSelected.forEach(x => {
              if (x.user?.component?.code !== '-') {
                this.listMeqrPmuSelected.push(x)
              } else if (x.user?.component?.code === '-') {
                this.listMeqrPcuSelected.push(x)
              }
            })
            this.dataEvent()
            this.setTimeRangeEvent()
            this.listProvince()
            this.listComponent()
            this.listPokEvent()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  calculatePs(event, from) {
    if (from == 1) {
      if (this.participantMale < this.participantCount) {
        this.participantFemale = 0
        if (this.participantMale + this.participantFemale < this.participantCount) {
          this.participantFemale = this.participantCount - this.participantMale
        }
      } else {
        this.participantMale = this.participantCount
        this.participantFemale = 0
      }
    } else {
      if (this.participantFemale < this.participantCount) {
        this.participantMale = 0
        if (this.participantMale + this.participantFemale < this.participantCount) {
          this.participantMale = this.participantCount - this.participantFemale
        }
      } else {
        this.participantFemale = this.participantCount
        this.participantMale = 0
      }
    }
  }
  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  /** * List Event */
  dataEvent() {
    this.startLoading()
    this.serviceAwp.eventType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEvent = resp.data.content
            this.listEventMaster = resp.data.content
            this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent.id)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedEventType(data) {
    this.selectedEvent = null
    this.listEvent = this.listEventMaster

    this.selectedEvent = data
    this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent.id)
  }

  searchFilterEvent(e) {
    const searchStr = e.target.value
    let filter = this.listEventMaster
    if (this.selectedEvent) {
      filter = filter.filter(x => x.id !== this.selectedEvent.id)
    } else {
      filter = this.listEventMaster
    }

    this.listEvent = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }
  /** * end List Event */


  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  setTimeRangeEvent() {
    let currentDate = new Date()
    let newConvertDate = new NgbDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate())
    if (currentDate >= this.dataDetailEvent.awpImplementation.convertStartDate && currentDate < this.dataDetailEvent.awpImplementation?.convertEndDate) {
      this.minDate = {
        year: newConvertDate.year,
        month: newConvertDate.month,
        day: newConvertDate.day + 1
      };
    } else {
      this.minDate = {
        year: this.minEvent.year,
        month: this.minEvent.month,
        day: this.minEvent.day
      };
    }

    this.maxDate = {
      year: this.maxEvent.year,
      month: this.maxEvent.month,
      day: this.maxEvent.day
    };
  }

  /** * end date */

  /** * list holidays */
  getListHolidayManagement() {
    this.listHolidays = []
    this.serviceAdministration.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * end list holidays */


  /** * List Province */
  listProvince() {
    let dataProvinceExist = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            this.listProvinceExist = resp.data.content
            // this.listProvinceAll = this.listProvinceAll.filter(x => x.id !== this.selectedProvinceExist.id)

            this.dataDetailEvent?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
              let dataExist = this.listProvinceAll.find(x => dataProvince.province.id == x.id)
              if (dataExist) {
                dataProvinceExist.push(dataExist)
              }
            })
            this.listProvinceExist = dataProvinceExist
            this.listProvinceExistMaster = dataProvinceExist
            this.listCity()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedProvinceExist = null
    this.listProvinceExist = this.listProvinceExistMaster

    this.selectedProvinceExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      this.listProvinceExist = this.listProvinceExistMaster
    }
    console.log(this.listProvinceExistMaster)
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    console.log(searchStr)
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    if (searchStr == '') {
      this.listProvinceExist = this.listProvinceExistMaster
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      return this.listProvinceExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  /** * End List Province */

  /** * List City */

  listCity() {
    let cityExistFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.serviceAwp.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            this.listCityAllExist = resp.data.content
            let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
            if (checkProvinceExist.length > 0) {
              this.dataDetailEvent?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
                if (dataProvince.regencies.length > 0) {
                  dataProvince.regencies.forEach(dataCity => {
                    let cityExist = this.listCityAll.find(x => dataCity.regency?.id == x.id)
                    if (cityExist) {
                      cityExistFromApi.push(cityExist)
                    }
                  })
                }
              })
              this.listCityAllExist = cityExistFromApi
              this.listCityAllMasterExist = JSON.parse(JSON.stringify(this.listCityAllExist))
            } else {
              this.listCityAllExist = this.listCityAll
            }

            this.listCityAllMaster = resp.data.content
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist

    this.selectedCityExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    } else {
      this.listCityAllExist = this.listCityAll
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    }
  }

  searchFilterCity(e) {
    const searchStr = e.target.value
    let filter = this.listCityAll
    if (this.selectedCityExist) {
      filter = filter.filter(x => x.id !== this.selectedCityExist.id)
    } else {
      filter = this.listCityAll
    }

    if (searchStr == '') {
      let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
      if (checkProvinceExist.length > 0) {
        this.listCityAllExist = this.listCityAllMasterExist
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      } else {
        this.listCityAllExist = this.listCityAll
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      }
    } else {
      return this.listCityAllExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  addSelectedProvinceAndCity() {
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityExist
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.selectedProvinceExist = null
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist
    this.listProvinceExist = this.listProvinceExistMaster
  }

  /** * Resources (Peserta) */

  getAllResources() {
    this.serviceResources.resourceAll().subscribe(
        resp => {
          if (resp.success) {
            this.listResources = resp.data.content
            this.listResourcesMaster = resp.data.content
            this.searchResources = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addResourcesSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.resourcesRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removeResources.forEach((check, index) => {
          if (check.id === data.id) {
            this.removeResources.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addResources.push(data)
        }
        this.resourcesRequest.push(data)
      }
    } else {
      this.resourcesRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.resourcesRequest.splice(index, 1)
          this.listMeqrResourceSelected.splice(index, 1)
        }
      })
      this.listMeqrPcuSelected = []
      this.listMeqrPmuSelected = []
      this.listMeqrResourceSelected.forEach(x => {
        if (x.user?.component?.code !== '-') {
          this.listMeqrPmuSelected.push(x)
        } else if (x.user?.component?.code === '-') {
          this.listMeqrPcuSelected.push(x)
        }
      })
      let checkAddExist = false

      this.addResources.forEach((check, index) => {
        if (check.id === data.id) {
          this.addResources.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      let x = this.resourcesRequest.filter(data => data.id === this.dataDetailEvent.contactPersonResources.id)
      if (x.length == 0) {
        this.selectedContactPerson = null
      } else {
        this.selectedContactPerson = x[0]
      }

      this.listContactPerson = this.listContactPerson.filter(x => x.id !== this.selectedContactPerson.id)

      if (!checkAddExist) {
        data.status = 0
        this.removeResources.push(data)
      }
    }
    this.listResources = this.listResourcesMaster

    this.resourcesRequest.forEach(dataProvince => {
      this.listResources = this.listResources.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })
    this.searchResources = this.listResources
    this.listContactPersonMaster = Object.assign([], this.resourcesRequest)
    this.listContactPerson = Object.assign([], this.resourcesRequest)
    let x = this.listContactPerson.filter(data => data.id === this.dataDetailEvent.contactPersonResources.id)
    if (x.length == 0) {
      this.selectedContactPerson = null
    } else {
      this.selectedContactPerson = x[0]
    }
  }

  searchFilterResources(e) {
    const searchStr = e.target.value
    this.listResources = this.searchResources.filter((product) => {
      return ((product.user.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.firstName !== null) || (product?.user?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.lastName !== null))
    });
  }

  addResourceSelectedMeqr() {
    this.listMeqrPmuSelected = []
    this.listMeqrPcuSelected = []
    this.closeModal()
    this.listMeqrResourceSelected = Object.assign([], this.resourcesRequest)
    this.listContactPersonMaster = Object.assign([], this.resourcesRequest)
    this.listContactPerson = Object.assign([], this.resourcesRequest)
    this.listMeqrResourceSelected.forEach(x => {
      if (x.user?.component?.code !== '-') {
        this.listMeqrPmuSelected.push(x)
      } else if (x.user?.component?.code === '-') {
        this.listMeqrPcuSelected.push(x)
      }
    })
    let x = this.listContactPerson.filter(data => data.id === this.dataDetailEvent.contactPersonResources.id)
    if (x.length == 0) {
      this.selectedContactPerson = null
    } else {
      this.selectedContactPerson = x[0]
    }
    this.listContactPerson = this.listContactPerson.filter(x => x.id !== this.selectedContactPerson.id)
  }
  /** * end Resources (Peserta) */

  /** Add New Unit*/

  selectedUnit(data) {
    this.selectedUnitExist = null
    this.listComponentStaff = this.componentExist

    this.selectedUnitExist = data
    this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedUnitExist.id)
    this.dataResources()
  }

  searchFilterUnit(e) {
    const searchStr = e.target.value
    let filter = this.componentExist
    if (this.selectedUnitExist) {
      filter = filter.filter(x => x.id !== this.selectedUnitExist.id)
    } else {
      filter = this.componentExist
    }

    this.listComponentStaff = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  listComponent() {
    this.serviceResources.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff = resp.data.content
            this.listComponentStaff.forEach(dataCode => {
              dataCode.code = 'Komponen ' + dataCode.code
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * List Project Officer */
  dataResources() {
    let dataListResource = []
    let selectedWorkUnitExist
    let params
    let selectedProvincePcu

    if (this.selectedWorkUnit === 'PMU') {
      selectedWorkUnitExist = [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.selectedUnitExist.id
        }
      ]
    } else if (this.selectedWorkUnit === 'PCU') {
      selectedProvincePcu = this.listProvinceAllStaff.filter(x => x.name === this.selectedProvinceStaff)
      selectedWorkUnitExist = [
        {
          field: "user.provinceId",
          dataType: "string",
          value: selectedProvincePcu[0].id
        }
      ]
    }

    this.loadingDataProjectOfficer = true
    params = {
      enablePage: false,
      paramIs: selectedWorkUnitExist,
      paramIn: [],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.serviceResources.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDataProjectOfficer = false
            this.listResources = resp.data.content
            this.listResourcesMaster = resp.data.content
            this.searchResources = resp.data.content
            const result = this.listResources.filter(({ id: id1 }) => !this.resourcesRequest.some(({ id: id2 }) => id2 === id1));
            this.listResources = result
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Add New Staff Outside */
  addNewStaffOutside() {
    this.otherStaff.push({
      name: this.nameNewStaff,
      position: this.namePosition,
      institution: this.nameInstitution,
    })
    this.closeModal()
    this.nameNewStaff = null
    this.namePosition = null
    this.nameInstitution = null
  }

  deleteNewStaffOutside(data) {
    this.otherStaff.forEach((check, index) => {
      if (check.name === data.name) {
        this.otherStaff.splice(index, 1)
      }
    })
  }
  deleteNewStaffOutsideIds(data) {
    this.otherStaffIds.forEach((check, index) => {
      if (check.name === data.name) {
        this.otherStaffIds.splice(index, 1)
      }
    })
  }
  /** End Add New Staff Outside */

  /** * Narahubung */
  selectedContactPersonExist(data) {
    this.selectedContactPerson = null
    this.listContactPerson = this.listContactPersonMaster

    this.selectedContactPerson = data
    this.listContactPerson = this.listContactPerson.filter(dataFilter => dataFilter.id != this.selectedContactPerson.id)
  }

  searchFilterContactPerson(e) {
    const searchStr = e.target.value
    let filter = this.listContactPersonMaster
    if (this.selectedContactPerson) {
      filter = filter.filter(x => x.id !== this.selectedContactPerson.id)
    } else {
      filter = this.listContactPersonMaster
    }

    this.listContactPerson = filter.filter((product) => {
      return ((product.user.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.firstName !== null) || (product?.user?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.lastName !== null))
    });
  }
  /** * End Naraubung */

  /** Upload File Rab */
  doUploadRab() {
    this.loadingSaveKegiatan = true
    if (this.uploadFileExcel.length > 0) {
      this.serviceResources.upload(this.uploadFileExcel[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdRab = resp.data[0].id
              this.doUploadKegiatan()
            } else {
              this.loadingSaveKegiatan = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      if (this.dataDetailEvent?.rabFile) {
        this.dataIdRab = this.dataDetailEvent?.rabFile?.id
      } else {
        this.dataIdRab = null
      }
      this.doUploadKegiatan()
    }
  }

  /** Upload File Kegiatan */
  doUploadKegiatan() {
    if (this.uploadFileExcelAgenda.length > 0) {
      this.serviceResources.uploadKegiatan(this.uploadFileExcelAgenda[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdKegiatan = resp.data[0].id
              this.editCn()
            } else {
              this.loadingSaveKegiatan = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      if (this.dataDetailEvent?.agendaFile) {
        this.dataIdKegiatan = this.dataDetailEvent?.agendaFile?.id
      } else {
        this.dataIdKegiatan = null
      }
      this.editCn()
    }
  }

  editCn() {
    let startDate
    let endDate
    let listStaff = []
    let regencyId
    let otherStaffIds = []
    let staffIds = JSON.parse(JSON.stringify(this.listMeqrResourceSelected))
    let staffIdsToApi = []
    let staffIdsToApiFilter = []
    let pokRequestApi = []
    let PokExisting = []

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(x => {
        PokExisting.push(x.id)
      })
    } else {
      PokExisting = []
    }

    if (this.pokRequest.length > 0) {
      this.pokRequest.forEach(x => {
        pokRequestApi.push(x.id)
      })
    }


    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')

    if (this.selectedProvinceAndCity[0]?.regencyId) {
      regencyId = this.selectedProvinceAndCity[0]?.regencyId.id
    } else {
      regencyId = null
    }

    if (this.otherStaffIds.length > 0) {
      this.otherStaffIds.forEach(x => {
        otherStaffIds.push(x.id)
      })
    } else {
      otherStaffIds = []
    }

    this.listMeqrResourceSelected.forEach(x => {
      this.resourcesRequestIds.forEach(y => {
        if (x.id === y.resources.id) {
          staffIdsToApi.push(y)
        }
      })
    })

    if (staffIdsToApi.length > 0) {
      staffIdsToApi.forEach(x => {
        staffIdsToApiFilter.push(x.id)
      })
    } else {
      staffIdsToApiFilter = []
    }

    if (staffIdsToApi.length > 0) {
      staffIdsToApi.forEach(x => {
        staffIds.forEach((check, index ) => {
          if (check.id == x.resources.id) {
            staffIds.splice(index, 1)
          }
        })
      })
      staffIds.forEach(x => {
        listStaff.push(x.id)
      })
    } else {
      this.listMeqrResourceSelected.forEach(data => {
        listStaff.push(data?.id)
      })
    }

    const params = {
      awpImplementationId: this.dataDetailEvent?.awpImplementation?.id,
      name: this.eventName,
      startDate: startDate,
      endDate: endDate,
      eventOtherInformation: this.timeOtherInformation,
      eventTypeId: this.selectedEvent.id,
      budgetPok: this.budgetPok,
      budgetAwp: this.budgetPok,
      background: this.background,
      description: this.description,
      purpose: this.purpose,
      eventOutput: this.eventOutput,
      participantMale: this.participantMale,
      participantFemale: this.participantFemale,
      participantCount: this.participantCount,
      participantOtherInformation: this.participantOtherInformation,
      contactPerson: this.selectedContactPerson.id,
      provinceId: this.selectedProvinceAndCity[0].provinceId.id,
      regencyId: regencyId,
      location: this.location,
      rabFileId: this.dataIdRab,
      agendaFileId: this.dataIdKegiatan,
      budgetAvailable: this.existOnAnggaran,
      staffIds: staffIdsToApiFilter,
      newStaff: listStaff,
      otherStaffIds: otherStaffIds,
      newOtherStaff: this.otherStaff,
      activityModeId: this.selectedModaActivity.id,
      locationOtherInformation: this.tempatInformasiLainnya,
      participantTarget: this.targetParticipant,
      pokIds: PokExisting,
      newPokIds: pokRequestApi,
    }

    this.service.updateCn(this.detailIdCn, params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveKegiatan = false
            this.router.navigate(['activity/detail-event/' + this.detailIdCn])
          }
        }, error => {
          this.loadingSaveKegiatan = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
