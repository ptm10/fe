import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReportEventComponent } from './add-report-event.component';

describe('AddReportEventComponent', () => {
  let component: AddReportEventComponent;
  let fixture: ComponentFixture<AddReportEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddReportEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReportEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
