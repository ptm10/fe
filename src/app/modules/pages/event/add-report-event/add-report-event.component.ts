import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EventActivityService} from "../../../../core/services/EventActivity/event-activity.service";
import {
  NgbCalendar,
  NgbDate,
  NgbDateParserFormatter,
  NgbDateStruct,
  NgbModal,
  NgbRatingConfig
} from "@ng-bootstrap/ng-bootstrap";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import Swal from "sweetalert2";
import {ProgressEventModel} from "../../../../core/models/progress-event.model";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {Event, EventDocumentType, MangementType, PokCode, Province} from "../../../../core/models/awp.model";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {resourcesModel} from "../../../../core/models/resources.model";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {EvaluationPoint} from "../../../../core/models/report-event.model";
import {parse} from "date-fns";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {QuestionEventAnswer} from "../../../../core/models/event.model";
import {EventQuestion, QuestionnareEvent} from "../../../../core/models/Questionnare";

@Component({
  selector: 'app-add-report-event',
  templateUrl: './add-report-event.component.html',
  styleUrls: ['./add-report-event.component.scss']
})
export class AddReportEventComponent implements OnInit {

  selectValue: string[];
  selectValueParticipant: string[]

  //loading
  finishLoadData = true
  finishLoadDataCount = 0

  detailProgressEvent: ProgressEventModel
  breadCrumbItems: Array<{}>;
  detailEventId: string
  dataDetailEvent: EventActivityModel
  dataDetailEventSpecial: EventActivityModel
  codeSelected: any
  keteranganKegiatan: string


  public outputEvent = ClassicEditor;
  public summaryEvent = ClassicEditor;
  public evaluationEvent = ClassicEditor;
  public participantTarget = ClassicEditor;
  public paragraph1 = ClassicEditor;
  public paragraph2 = ClassicEditor;
  public paragraph3 = ClassicEditor;
  public paragraph4 = ClassicEditor;
  public paragraph5 = ClassicEditor;
  fieldSubstantiv: number
  loadingSaveProgressEvent = false
  minDateYear: any
  maxDateYear: any
  listEvaluationPoint: EvaluationPoint[]
  listParticipantTargetFromApi: EvaluationPoint[]

  // data event
  selectedComponentExist: ComponentModel
  selectedSubComponentExist: ComponentModel
  selectedSubSubComponentExist: ComponentModel
  kegiatanName: string
  eventName: string
  selectedEvent: Event
  uploadFileExcelAgenda: Array<File> = [];
  uploadFileExcelIdsAgenda: Array<File> = [];
  uploadFileRaa: Array<File> = [];
  uploadFileRaaIdsAgenda: Array<File> = [];
  agendaFile: any
  raaFile: any
  dataIdAttachmentFile: string
  dataIdFileRaa: string
  fileDocumentationId: string
  tempatInformasiLainnya: string

  // create field
  existOnAnggaran: boolean
  existOnPok: boolean
  budgetAwp: number
  budgetPok: number
  timeOtherInformation: string
  location: string
  eventOutput: string
  participantCount: number
  participantMale: number
  participantFemale: number
  participantOtherInformation: string
  summaryEventOutput: string = null
  documentationEvent = []
  uploadFileDocumentationEvent: Array<File> = [];
  documentationEventName: string
  documentationEventDescription: string
  documentationEventDate: any
  documentationEventPlace: string
  documentationEventToApi = []
  themeEvent: string
  eventCode: string
  targetParticipant: string = null
  evaluationEventOutput: string = null
  conclutionAndAdvice: string = null
  sourceFounds: string
  yearBudget: any
  MakNumber: string
  totalAnggaranRealitation: number
  budgetCeiling: number
  listPointEvaluation = []
  introductionParagraph1: string = null
  introductionParagraph2: string = null
  introductionParagraph4: string = null
  introductionParagraph5: string = null
  totalDurationParagraph3: string
  schemaEventParagraph3: string
  locationEventParagraph3: string
  involveEventParagraph3: string


  // point evaluation
  evaluationTitle: string
  noteEvaluation: string
  valueEvaluationRating: number = 0
  readonly = true

  // sasaran peserta
  listParticipantTarget = []
  fromParticipantTarget: string
  totalParticipantTarget: number

  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;

  fromDateIntroduction: Date
  toDateIntroduction: Date
  hoveredDateIntroduction: NgbDate | null = null;

  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  codeEvent: any
  minEvent: NgbDate
  maxEvent: NgbDate
  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  listCityAllExist: Province[]
  listCityAllMasterExist: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []
  listCityFromApi: string[];
  selectedCityFromList = []

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  listProvinceExist: Province[]
  listProvinceExistMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province
  provinceAndRegenciesExist = []
  searchFilterProvinces: any
  listMeqrResourceSelected = []

  // peserta
  listResources: resourcesModel[]
  listResourcesMaster: resourcesModel[]
  selectedResourcesExist: resourcesModel
  resourcesRequest = []
  resourcesRequestIds = []
  addResources = []
  removeResources = []
  searchResources = []
  otherStaffIds = []
  otherStaff = []

  //contact person
  listContactPerson: resourcesModel[]
  listContactPersonMaster: resourcesModel[]
  selectedContactPerson: resourcesModel

  //componen
  selectedUnitExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]

  // unit
  listComponentStaff: ComponentModel[]
  componentExist: ComponentModel[]

  // project officer
  listProjectOfficer = []
  listProjectOfficerMaster = []
  selectedProjectOfficer: resourcesModel
  loadingDataProjectOfficer = false

  nameNewStaff: string
  namePosition: string
  nameInstitution: string

  //Moda Kegiatan
  listModaType: MangementType[]
  listModaTypeMaster: MangementType[]
  selectedModaActivity: any

  // POK
  listCodePok: PokCode[]
  listCodePokMaster: PokCode[]
  pokRequest = []
  addPok = []
  removePok = []
  searchPok = []
  pokExisting = []

  typeDocumentation: string[] = []
  selectedTypeDocumentation: string
  listAllDocumentType: EventDocumentType[]

  // question evaluator
  existingQuestionCompliance: EventQuestion[]  = []
  existingQuestionEconomy: EventQuestion[]  = []
  existingQuestionEfective: EventQuestion[]  = []
  existingQuestionEfisiens: EventQuestion[]  = []
  existingQuestionParticipant: EventQuestion[]  = []
  listAllQuestionEvent: EventQuestion[]
  questionAnswerAllCategory = []

  constructor(
      private activatedRoute: ActivatedRoute,
      private serviceEvent: EventActivityService,
      private router: Router,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private serviceAwp: AwpService,
      private modalService: NgbModal,
      private serviceResources: KegiatanService,
      private config: NgbRatingConfig,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Event' }, { label: 'Detail Event'}, { label: 'Tambah Laporan Event', active: true }]
    this.fieldSubstantiv = 1
    this.detailEventId = this.activatedRoute.snapshot.paramMap.get('id')
    // this.getDetailEvent()
    this.config.max = 5;
    this.minDateYear = new Date(2020, 1, 1)
    this.maxDateYear = new Date(2024, 11, 30)
    this.yearBudget = new Date()
    this.introductionParagraph1 = '<p>Event Workshop/Bimbingan Teknis/Pelatihan/Konsinyering merupakan salah satu tahapan Event yang diperlukan dalam rangka pencapaian Kerangka Hasil (Result Framework) sebagaimana tercantum dalam Project Operation Manual (POM) dan Annual Work Plan (AWP) Tahun Anggaran 2021 yang ditetapkan oleh Project Management Unit Realizing Education’s Promise – Madrasah Education Quality Reform (IBRD 8992-ID).\t</p>'
    this.introductionParagraph2 = '<p>Event ini bertujuan untuk</p>'
    this.introductionParagraph4 = '<p>Kami menyampaikan apresiasi dan terima kasih yang setinggi-tingginya kepada para pihak yang telah memberikan kontribusi besar sehingga Event dapat terlaksana dengan lancar. Penghargaan dan terima kasih juga kami sampaikan kepada para narasumber, moderator, dan seluruh panitia pelaksana yang telah berpartisipasi dalam menyukseskan Event ini.</p>'
    this.introductionParagraph5 = '<p>Akhirnya, kami berharap laporan Event ini dapat bermanfaat sebagai salah satu bentuk transparansi dan akuntabilitas pelaksanaan Event. Kami juga berharap Event ini dapat memberikan kontribusi positif sebagai necessary conditions dalam peningkatan mutu pendidikan madrasah.</p>'
    this.getListEventEvaluationPoint()
    this.getDetailLastReport()
    this.getListParticipantTarget()
    this.listEventDocumentType()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  /** Input date calender */
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  goToDetailEvent() {
    this.router.navigate(['activity/detail-event/' + this.detailEventId])
  }

  changeMenuFieldProgress(menu: any) {
    this.fieldSubstantiv = menu
  }

  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.serviceAwp.listModa().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  calculatePs(event, from) {
    if (this.participantMale || this.participantFemale) {
      if (from == 1) {
        if (this.participantMale < this.participantCount) {
          this.participantFemale = 0
          if (this.participantMale + this.participantFemale < this.participantCount) {
            this.participantFemale = this.participantCount - this.participantMale
          }
        } else {
          this.participantMale = this.participantCount
          this.participantFemale = 0
        }
      } else {
        if (this.participantFemale < this.participantCount) {
          this.participantMale = 0
          if (this.participantMale + this.participantFemale < this.participantCount) {
            this.participantMale = this.participantCount - this.participantFemale
          }
        } else {
          this.participantFemale = this.participantCount
          this.participantMale = 0
        }
      }
    }
  }

  goToResultFormEvaluator() {
    this.router.navigate([]).then(result => {  window.open('activity/summary-evaluator/' + this.detailEventId); })
  }

  goToResultFormParticipant() {
    this.router.navigate([]).then(result => {  window.open('activity/summary-participant/' + this.detailEventId); })
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  getDetailLastReport() {
    this.startLoading()
    this.serviceEvent.detailEventLastReport(this.detailEventId).subscribe(
        resp => {
          if (resp.success) {
            this.detailProgressEvent = resp.data
            if (this.detailProgressEvent.id) {
              this.existOnAnggaran = this.detailProgressEvent?.budgetAvailable
              this.selectedComponentExist = this.detailProgressEvent?.event?.awpImplementation?.component
              this.selectedSubComponentExist = this.detailProgressEvent?.event?.awpImplementation?.subComponent
              this.selectedSubSubComponentExist = this.detailProgressEvent?.event?.awpImplementation?.subSubComponent
              this.codeSelected = this.detailProgressEvent?.event?.awpImplementation?.componentCode?.code
              this.kegiatanName = this.detailProgressEvent?.event?.awpImplementation?.name
              this.eventName = this.detailProgressEvent?.event?.name
              this.selectedEvent = this.detailProgressEvent?.event?.eventType
              this.existOnPok = this.detailProgressEvent?.pokEvent
              this.budgetAwp = this.detailProgressEvent?.event?.awpImplementation?.budgetAwp
              this.budgetPok = this.detailProgressEvent?.event?.budgetPok
              this.timeOtherInformation = this.detailProgressEvent?.eventOtherInformation
              this.location = this.detailProgressEvent?.location
              this.selectedContactPerson = this.detailProgressEvent?.event?.contactPersonResources
              this.selectedProvinceExist = this.detailProgressEvent?.province
              this.selectedCityExist = this.detailProgressEvent?.regency

              this.detailProgressEvent.convertStartDate = new Date(this.detailProgressEvent.startDate)
              this.detailProgressEvent.convertEndDate = new Date(this.detailProgressEvent.endDate)
              this.fromDate = new NgbDate(this.detailProgressEvent.convertStartDate.getFullYear(), this.detailProgressEvent.convertStartDate.getMonth() + 1, this.detailProgressEvent.convertStartDate.getDate())
              this.toDate = new NgbDate(this.detailProgressEvent.convertEndDate.getFullYear(), this.detailProgressEvent.convertEndDate.getMonth() + 1, this.detailProgressEvent.convertEndDate.getDate())
              this.fromDateIntroduction = parse(this.detailProgressEvent.startDate, 'yyyy-MM-dd', new Date());
              this.toDateIntroduction = parse(this.detailProgressEvent.endDate, 'yyyy-MM-dd', new Date());

              this.detailProgressEvent.event.convertStartDate = new Date(this.detailProgressEvent.event.startDate)
              this.detailProgressEvent.event.convertEndDate = new Date(this.detailProgressEvent.event.endDate)
              this.minEvent = new NgbDate(this.detailProgressEvent.event.convertStartDate.getFullYear(), this.detailProgressEvent.event.convertStartDate.getMonth() + 1, this.detailProgressEvent.event.convertStartDate.getDate())
              this.maxEvent = new NgbDate(this.detailProgressEvent.event.convertEndDate.getFullYear(), this.detailProgressEvent.event.convertEndDate.getMonth() + 1, this.detailProgressEvent.event.convertEndDate.getDate())
              if (this.detailProgressEvent?.regency) {
                this.selectedProvinceAndCity.push({
                  provinceId: this.selectedProvinceExist,
                  regencyId: this.selectedCityExist
                })
              } else {
                this.selectedProvinceAndCity.push({
                  provinceId: this.selectedProvinceExist,
                  regencyId: null
                })
              }

              this.detailProgressEvent?.staff.forEach(dataStaff => {
                this.resourcesRequest.push(dataStaff.resources)
                this.resourcesRequestIds.push(dataStaff)
              })
              if (this.detailProgressEvent?.otherStaff.length > 0) {
                this.detailProgressEvent?.otherStaff.forEach(dataOtherStaff => {
                  this.otherStaffIds.push(dataOtherStaff)
                })
              }

              this.eventOutput = this.detailProgressEvent?.eventOutput
              this.listMeqrResourceSelected = JSON.parse(JSON.stringify(this.resourcesRequest))
              this.participantCount = this.detailProgressEvent?.participantCount
              this.participantMale = this.detailProgressEvent?.participantMale
              this.participantFemale = this.detailProgressEvent?.participantFemale
              this.participantOtherInformation = this.detailProgressEvent?.participantOtherInformation
              this.summaryEventOutput = this.detailProgressEvent?.summary
              this.agendaFile = this.detailProgressEvent?.attachmentFile
              this.raaFile = this.detailProgressEvent?.rraFile
              this.selectedProjectOfficer = this.detailProgressEvent?.event.awpImplementation?.projectOfficerResources
              this.detailProgressEvent.staff.forEach(x => {
                this.listProjectOfficer.push(x.resources)
                this.listProjectOfficerMaster.push(x.resources)
              })
              let x = this.detailProgressEvent.staff.filter(staffId => staffId.resources.id === this.selectedProjectOfficer.id)
              if (x.length == 0) {
                this.listProjectOfficer.push(this.selectedProjectOfficer)
                this.listProjectOfficerMaster.push(this.selectedProjectOfficer)
              }
              this.listProjectOfficer = this.listProjectOfficer.filter(x => x.id !== this.selectedProjectOfficer.id)
              this.selectedModaActivity = this.detailProgressEvent?.activityMode
              this.tempatInformasiLainnya = this.detailProgressEvent?.locationOtherInformation
              this.listProvince()
              this.setTimeRangeEvent()
              this.listComponent()
              this.getListQuestionByEvent()
              this.serviceEvent.detailEvent(this.detailEventId).subscribe(
                  resp => {
                    this.stopLoading()
                    if (resp.success) {
                      this.dataDetailEventSpecial = resp.data
                      this.keteranganKegiatan = this.dataDetailEventSpecial?.awpImplementation?.nameOtherInformation
                      console.log(this.dataDetailEventSpecial)
                      if (this.dataDetailEventSpecial?.pok?.length > 0) {
                        this.dataDetailEventSpecial?.pok.forEach(x => {
                          this.pokRequest.push(x.pok)
                        })
                      }
                      this.targetParticipant = this.dataDetailEventSpecial?.participantTarget
                      this.listAllModa()
                      this.listPokEvent()
                    }
                  }
              )

            } else {
              this.stopLoading()
              this.getDetailEvent()
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getDetailEvent() {
    this.startLoading()
    this.serviceEvent.detailEvent(this.detailEventId).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.participantCount = 0
            this.dataDetailEvent = resp.data
            this.dataDetailEvent.convertStartDate = new Date(this.dataDetailEvent.startDate)
            this.dataDetailEvent.convertEndDate = new Date(this.dataDetailEvent.endDate)
            this.existOnAnggaran = this.dataDetailEvent?.budgetAvailable
            this.selectedComponentExist = this.dataDetailEvent?.awpImplementation?.component
            this.selectedSubComponentExist = this.dataDetailEvent?.awpImplementation?.subComponent
            this.selectedSubSubComponentExist = this.dataDetailEvent?.awpImplementation?.subSubComponent
            this.codeSelected = this.dataDetailEvent?.awpImplementation?.componentCode?.code
            this.kegiatanName = this.dataDetailEvent?.awpImplementation?.name
            this.eventName = this.dataDetailEvent?.name
            this.selectedEvent = this.dataDetailEvent?.eventType
            this.existOnPok = this.dataDetailEvent?.pokEvent
            this.budgetAwp = this.dataDetailEvent?.awpImplementation?.budgetAwp
            this.budgetPok = this.dataDetailEvent?.budgetPok
            this.timeOtherInformation = this.dataDetailEvent?.eventOtherInformation
            this.location = this.dataDetailEvent?.location
            this.locationEventParagraph3 = this.dataDetailEvent?.location
            this.selectedContactPerson = this.dataDetailEvent?.contactPersonResources
            this.selectedProvinceExist = this.dataDetailEvent?.province
            this.selectedCityExist = this.dataDetailEvent?.regency

            this.dataDetailEvent.convertStartDate = new Date(this.dataDetailEvent.startDate)
            this.dataDetailEvent.convertEndDate = new Date(this.dataDetailEvent.endDate)
            this.minEvent = new NgbDate(this.dataDetailEvent.convertStartDate.getFullYear(), this.dataDetailEvent.convertStartDate.getMonth() + 1, this.dataDetailEvent.convertStartDate.getDate())
            this.maxEvent = new NgbDate(this.dataDetailEvent.convertEndDate.getFullYear(), this.dataDetailEvent.convertEndDate.getMonth() + 1, this.dataDetailEvent.convertEndDate.getDate())
            this.fromDate = new NgbDate(this.dataDetailEvent.convertStartDate.getFullYear(), this.dataDetailEvent.convertStartDate.getMonth() + 1, this.dataDetailEvent.convertStartDate.getDate())
            this.toDate = new NgbDate(this.dataDetailEvent.convertEndDate.getFullYear(), this.dataDetailEvent.convertEndDate.getMonth() + 1, this.dataDetailEvent.convertEndDate.getDate())
            this.fromDateIntroduction = parse(this.dataDetailEvent.startDate, 'yyyy-MM-dd', new Date());
            this.toDateIntroduction = parse(this.dataDetailEvent.endDate, 'yyyy-MM-dd', new Date());

            if (this.dataDetailEvent?.regency) {
              this.selectedProvinceAndCity.push({
                provinceId: this.selectedProvinceExist,
                regencyId: this.selectedCityExist
              })
            } else {
              this.selectedProvinceAndCity.push({
                provinceId: this.selectedProvinceExist,
                regencyId: null
              })
            }

            this.dataDetailEvent?.staff.forEach(dataStaff => {
              this.resourcesRequest.push(dataStaff.resources)
              this.resourcesRequestIds.push(dataStaff)
            })
            if (this.dataDetailEvent?.otherStaff.length > 0) {
              this.dataDetailEvent?.otherStaff.forEach(dataOtherStaff => {
                this.otherStaffIds.push(dataOtherStaff)
              })
            }
            this.eventOutput = this.dataDetailEvent?.eventOutput
            this.listMeqrResourceSelected = JSON.parse(JSON.stringify(this.resourcesRequest))
            this.participantOtherInformation = this.dataDetailEvent?.participantOtherInformation
            this.selectedProjectOfficer = this.dataDetailEvent?.awpImplementation?.projectOfficerResources

            this.dataDetailEvent.staff.forEach(x => {
              this.listProjectOfficer.push(x.resources)
              this.listProjectOfficerMaster.push(x.resources)
            })
            let x = this.dataDetailEvent.staff.filter(staffId => staffId.resources.id === this.selectedProjectOfficer.id)
            if (x.length == 0) {
              this.listProjectOfficer.push(this.selectedProjectOfficer)
              this.listProjectOfficerMaster.push(this.selectedProjectOfficer)
            }
            this.listProjectOfficer = this.listProjectOfficer.filter(x => x.id !== this.selectedProjectOfficer.id)
            this.selectedModaActivity = this.dataDetailEvent?.activityMode
            this.keteranganKegiatan = this.dataDetailEvent?.awpImplementation?.nameOtherInformation
            if (this.dataDetailEvent.pok.length > 0) {
              this.dataDetailEvent.pok.forEach(x => {
                this.pokRequest.push(x.pok)
              })
            }
            this.tempatInformasiLainnya = this.dataDetailEvent?.locationOtherInformation
            this.targetParticipant = this.dataDetailEvent?.participantTarget
            this.listProvince()
            this.setTimeRangeEvent()
            this.listComponent()
            this.listAllModa()
            this.listPokEvent()
            this.getListQuestionByEvent()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * List Question */
  getListQuestionByEvent() {
    this.startLoading()
    let params = {
      enablePage: false,
      paramIs: [
        {
          field: "eventForm.eventTypeId",
          dataType: "string",
          value: this.selectedEvent?.id
        },
        {
          field: "createdFor",
          dataType: "int",
          value: 1
        }
      ],
      sort: []
    }
    this.serviceEvent.getListEventAddQuestion(params).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listAllQuestionEvent = resp.data.content
            this.listAllQuestionEvent.forEach(x => {
              x.scaleAsnwer = 0
              x.explanationAnswer = null
              x.yesOrNoAnswer = null
              if (x.questionCategory === 1) {
                this.existingQuestionCompliance.push(x)
              } else if (x.questionCategory === 2) {
                this.existingQuestionEconomy.push(x)
              } else if (x.questionCategory === 3) {
                this.existingQuestionEfective.push(x)
              } else if (x.questionCategory === 4) {
                this.existingQuestionEfisiens.push(x)
              }
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * POK Code */
  listPokEvent() {
    this.startLoading()
    this.serviceResources.listCodePok().subscribe(
        resp => {
          if (resp.success) {
            this.listCodePok = resp.data.content
            this.listCodePokMaster = resp.data.content
            this.searchPok = resp.data.content
            if (this.pokRequest.length > 0) {
              this.pokRequest.forEach(dataPokExist => {
                this.listCodePok = this.listCodePok.filter(x => x.id !== dataPokExist?.id)
                this.searchPok = this.listCodePok.filter(x => x.id !== dataPokExist?.id)
              })
            }
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addPokSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pokRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePok.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePok.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPok.push(data)
        }
        this.pokRequest.push(data)
      }
    } else {
      this.pokRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pokRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPok.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPok.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePok.push(data)
      }
    }
    this.listCodePok = this.listCodePokMaster

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.id)
      })
    }

    this.pokRequest.forEach(dataProvince => {
      this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPok = this.listCodePok
  }

  searchFilterPok(e) {
    const searchStr = e.target.value
    this.listCodePok = this.searchPok.filter((product) => {
      return (product.code.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.title.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePokExisting(data) {
    this.pokExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.pokExisting.splice(index, 1)
      }
    })
    this.listCodePok = this.listCodePokMaster
    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.pok?.id)
      })
      this.pokRequest.forEach(dataPok => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataPok.id)
      })
    } else {
      this.pokRequest.forEach(dataProvince => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPok = this.listCodePok
  }

  /** * End POK Code */

  /** * List Project Officer */
  selectProjectOfficer(data) {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster

    this.selectedProjectOfficer = data
    this.listProjectOfficer = this.listProjectOfficer.filter(dataFilter => dataFilter.id != this.selectedProjectOfficer.id)
  }

  removeProjectOfficer() {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster
  }

  searchFilterProjectOfficer(e) {
    const searchStr = e.target.value
    let filter = this.listProjectOfficerMaster
    if (this.selectedProjectOfficer) {
      filter = filter.filter(x => x.id !== this.selectedProjectOfficer.id)
    } else {
      filter = this.listProjectOfficerMaster
    }

    this.listProjectOfficer = filter.filter((product) => {
      return ((product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List Project Officer */
  dataResources() {
    let dataListResource = []
    this.loadingDataProjectOfficer = true
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.selectedUnitExist.id
        }
      ],
      paramIn: [],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.serviceResources.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDataProjectOfficer = false
            this.listResources = resp.data.content
            this.listResourcesMaster = resp.data.content
            this.searchResources = resp.data.content
            const result = this.listResources.filter(({ id: id1 }) => !this.resourcesRequest.some(({ id: id2 }) => id2 === id1));
            this.listResources = result
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Add New Unit*/

  selectedUnit(data) {
    this.selectedUnitExist = null
    this.listComponentStaff = this.componentExist

    this.selectedUnitExist = data
    this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedUnitExist.id)
    this.dataResources()
  }

  searchFilterUnit(e) {
    const searchStr = e.target.value
    let filter = this.componentExist
    if (this.selectedUnitExist) {
      filter = filter.filter(x => x.id !== this.selectedUnitExist.id)
    } else {
      filter = this.componentExist
    }

    this.listComponentStaff = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  listComponent() {
    this.serviceResources.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff = resp.data.content
            this.listComponentStaff.forEach(dataCode => {
              dataCode.code = 'Komponen ' + dataCode.code
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  setTimeRangeEvent() {
    let currentDate = new Date()
    // let newConvertDate = new NgbDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate())
    // if (currentDate >= this.dataDetailEvent.awpImplementation.convertStartDate) {
    //   this.minDate = {
    //     year: newConvertDate.year,
    //     month: newConvertDate.month,
    //     day: newConvertDate.day + 1
    //   };
    // } else {
    //   this.minDate = {
    //     year: this.minEvent.year,
    //     month: this.minEvent.month,
    //     day: this.minEvent.day
    //   };
    // }
    this.minDate = {
      year: this.minEvent.year,
      month: this.minEvent.month,
      day: this.minEvent.day
    };

    this.maxDate = {
      year: this.maxEvent.year,
      month: this.maxEvent.month,
      day: this.maxEvent.day
    };
  }


  /** * List Province */
  listProvince() {
    let dataProvinceExist = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            this.listProvinceExist = resp.data.content
            // this.listProvinceAll = this.listProvinceAll.filter(x => x.id !== this.selectedProvinceExist.id)

            this.dataDetailEvent?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
              let dataExist = this.listProvinceAll.find(x => dataProvince.province.id == x.id)
              if (dataExist) {
                dataProvinceExist.push(dataExist)
              }
            })
            this.detailProgressEvent?.event?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
              let dataExist = this.listProvinceAll.find(x => dataProvince.province.id == x.id)
              if (dataExist) {
                dataProvinceExist.push(dataExist)
              }
            })

            this.listProvinceExist = dataProvinceExist
            this.listProvinceExistMaster = dataProvinceExist
            this.listCity()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedProvinceExist = null
    this.listProvinceExist = this.listProvinceExistMaster

    this.selectedProvinceExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      this.listProvinceExist = this.listProvinceExistMaster
    }
    console.log(this.listProvinceExistMaster)
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    console.log(searchStr)
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    if (searchStr == '') {
      this.listProvinceExist = this.listProvinceExistMaster
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      return this.listProvinceExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  /** * End List Province */

  /** * List City */

  listCity() {
    let cityExistFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.serviceAwp.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            this.listCityAllExist = resp.data.content
            let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
            if (checkProvinceExist.length > 0) {
              this.dataDetailEvent?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
                if (dataProvince.regencies.length > 0) {
                  dataProvince.regencies.forEach(dataCity => {
                    let cityExist = this.listCityAll.find(x => dataCity.regency?.id == x.id)
                    if (cityExist) {
                      cityExistFromApi.push(cityExist)
                    }
                  })
                }
              })
              this.detailProgressEvent?.event?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
                if (dataProvince.regencies.length > 0) {
                  dataProvince.regencies.forEach(dataCity => {
                    let cityExist = this.listCityAll.find(x => dataCity.regency?.id == x.id)
                    if (cityExist) {
                      cityExistFromApi.push(cityExist)
                    }
                  })
                }
              })
              this.listCityAllExist = cityExistFromApi
              this.listCityAllMasterExist = JSON.parse(JSON.stringify(this.listCityAllExist))
            } else {
              this.listCityAllExist = this.listCityAll
            }

            this.listCityAllMaster = resp.data.content
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist

    this.selectedCityExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    } else {
      this.listCityAllExist = this.listCityAll
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    }
  }

  searchFilterCity(e) {
    const searchStr = e.target.value
    let filter = this.listCityAll
    if (this.selectedCityExist) {
      filter = filter.filter(x => x.id !== this.selectedCityExist.id)
    } else {
      filter = this.listCityAll
    }

    if (searchStr == '') {
      let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
      if (checkProvinceExist.length > 0) {
        this.listCityAllExist = this.listCityAllMasterExist
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      } else {
        this.listCityAllExist = this.listCityAll
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      }
    } else {
      return this.listCityAllExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  addSelectedProvinceAndCity() {
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityExist
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.selectedProvinceExist = null
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist
    this.listProvinceExist = this.listProvinceExistMaster
    console.log(this.listProvinceExist)
    console.log(this.listProvinceExistMaster)
  }

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      this.fromDateIntroduction = new Date(this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'))
      this.toDateIntroduction = new Date(this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'))
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }


  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }
  /** * end date */

  /** New Documentation Event */

  listEventDocumentType() {
    let tag = []
    this.serviceEvent.getListDocumentType().subscribe(
        resp => {
          if (resp.success) {
            this.listAllDocumentType = resp.data.content
            this.listAllDocumentType.forEach(x => {
              tag.push(x.name)
            })
          }
          this.typeDocumentation = tag
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addDocumentationEvent() {
    let startDate
    let selectedTypeDocument
    if (this.documentationEventDate) {
      startDate = this.documentationEventDate.year.toString() + '-' + this.documentationEventDate.month.toString().padStart(2, '0') + '-' + this.documentationEventDate.day.toString().padStart(2, '0')
    } else {
      startDate = null
    }

    selectedTypeDocument = this.listAllDocumentType.filter(x => x.name === this.selectedTypeDocumentation)

    this.documentationEvent.push({
      title: this.documentationEventName,
      date: startDate,
      place: this.documentationEventPlace,
      description: this.documentationEventDescription,
      fileIds: this.uploadFileDocumentationEvent,
      fileId: null,
      eventDocumentTypeId: selectedTypeDocument[0].id,
      eventDocumentTypeName: this.selectedTypeDocumentation
    })
    this.documentationEventName = ''
    this.documentationEventDate = null
    this.documentationEventPlace = ''
    this.documentationEventDescription = ''
    this.selectedTypeDocumentation = ''
    this.uploadFileDocumentationEvent = []
    this.closeModal()
  }
  removeDocumentationFile(data) {
    this.documentationEvent.forEach((check, index) => {
      if (check.title === data.title) {
        this.documentationEvent.splice(index, 1)
      }
    })
  }
  clearOldDocuments(data) {
    this.detailProgressEvent?.documents.forEach((check, index) => {
      if (check.id === data.id) {
        this.detailProgressEvent?.documents.splice(index, 1)
      }
    })
  }

  /** New Evaluation Point Event */

  addEvaluationPoint() {
    this.listPointEvaluation.push({
      id: this.listPointEvaluation.length + 1,
      newEvaluationPoint: this.evaluationTitle,
      evaluationPointId: null,
      value: this.valueEvaluationRating,
      note: this.noteEvaluation
    })
    this.closeModal()
    this.evaluationTitle = ''
    this.valueEvaluationRating = 0
    this.noteEvaluation = ''
  }
  clearEvaluationPointNew(data) {
    this.listPointEvaluation.forEach((check, index) => {
      if (data.id === check.id) {
        this.listPointEvaluation.splice(index, 1)
      }
    })
  }

  getListEventEvaluationPoint() {
    this.startLoading()
    let tag = []
    this.serviceEvent.getListEventEvaluation().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            tag = []
            this.listEvaluationPoint = resp.data.content
            this.listEvaluationPoint.forEach(x => {
              tag.push(x.name)
            })
            this.selectValue = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  CreateNew(city){
    return city
  }
  /** End Evaluation Point Event */

  /** New Participant Target Event */


  CreateNewParticipantTarget(data){
    return data
  }

  addNewParticipantTarget() {
    let filtered = this.listParticipantTargetFromApi.filter(x => x.name === this.fromParticipantTarget)
    if (filtered.length > 0) {
      this.listParticipantTarget.push({
        id: this.listParticipantTarget.length + 1,
        newParticipantTarget: this.fromParticipantTarget,
        participantTargetId: filtered[0].id,
        value: this.totalParticipantTarget
      })
    } else {
      this.listParticipantTarget.push({
        id: this.listParticipantTarget.length + 1,
        newParticipantTarget: this.fromParticipantTarget,
        participantTargetId: null,
        value: this.totalParticipantTarget
      })
    }
    this.closeModal()
    this.fromParticipantTarget = ''
    this.totalParticipantTarget = null
  }

  clearNewParticipantTarget(data) {
    this.listParticipantTarget.forEach((check, index) => {
      if (data.id === check.id) {
        this.listParticipantTarget.splice(index, 1)
      }
    })
  }

  getListParticipantTarget() {
    this.startLoading()
    let tag = []
    this.serviceEvent.getListParticipantEvent().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            tag = []
            this.listParticipantTargetFromApi = resp.data.content
            this.listParticipantTargetFromApi.forEach(x => {
              tag.push(x.name)
            })
            this.selectValueParticipant = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** End Participant Target Event */


  /** Upload File Kegiatan */
  doUploadAttachmentFile() {
    this.loadingSaveProgressEvent = true
    if (this.uploadFileExcelAgenda.length > 0) {
      this.serviceResources.uploadAttachment(this.uploadFileExcelAgenda[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdAttachmentFile = resp.data[0].id
              this.doUploadAttachmentRaa()
            } else {
              this.loadingSaveProgressEvent = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveProgressEvent = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.dataIdAttachmentFile = this.detailProgressEvent.attachmentFile.id
      this.doUploadAttachmentRaa()
    }
  }

  doUploadAttachmentRaa() {
    if (this.uploadFileRaa.length > 0) {
      this.serviceResources.uploadKegiatanRaa(this.uploadFileRaa[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdFileRaa = resp.data[0].id
              this.doUploadRab()
            } else {
              this.loadingSaveProgressEvent = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveProgressEvent = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.dataIdFileRaa = this.detailProgressEvent.rraFile.id
      this.doUploadRab()
    }
  }

  doUploadRab() {
    this.documentationEventToApi = []
    if (this.documentationEvent.length > 0) {
      this.documentationEvent.forEach(data => {
        this.serviceResources.uploadDocumentation(data.fileIds[0]).subscribe(resp => {
              if (resp.success) {
                this.fileDocumentationId = resp.data[0].id
                data.fileId = this.fileDocumentationId
                this.documentationEventToApi.push({
                  title: data?.title,
                  date: data?.date,
                  place: data?.place,
                  description: data?.description,
                  fileId: this.fileDocumentationId,
                  eventDocumentTypeId: data?.eventDocumentTypeId
                })
                if (this.documentationEvent.length == this.documentationEventToApi.length) {
                  this.createReportEvent()
                }
              } else {
                this.loadingSaveProgressEvent = false
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: resp.message,
                });
              }
            }, error => {
              this.loadingSaveProgressEvent = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: error.data[0]?.message,
              });
            }
        );
      })
    } else {
      this.createReportEvent()
    }
  }

  createReportEvent() {
    let regencyId
    let startDate
    let endDate
    let listEvaluationFiltered = JSON.parse(JSON.stringify(this.listPointEvaluation))
    let listEvaluationToApi = []
    let pokRequestApi = []
    let listParticipantTargetToApi = []
    let listParticipantFiltered = JSON.parse(JSON.stringify(this.listParticipantTarget))


    if (this.pokRequest.length > 0) {
      this.pokRequest.forEach(x => {
        pokRequestApi.push(x.id)
      })
    }

    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')

    listEvaluationFiltered.forEach(x => {
      if (x.evaluationPointId !== null) {
        x.newEvaluationPoint = null
      }
      listEvaluationToApi.push({
        newEvaluationPoint: x.newEvaluationPoint,
        evaluationPointId: x.evaluationPointId,
        value: x.value,
        note: x.note
      })
    })

    listParticipantFiltered.forEach(x => {
      if (x.participantTargetId !== null) {
        x.newParticipantTarget = null
      }
      listParticipantTargetToApi.push({
        newParticipantTarget: x.newParticipantTarget,
        participantTargetId: x.participantTargetId,
        value: x.value
      })
    })

    if (this.selectedProvinceAndCity[0]?.regencyId) {
      regencyId = this.selectedProvinceAndCity[0]?.regencyId.id
    } else {
      regencyId = null
    }

    if (this.detailProgressEvent?.documents.length > 0) {
      this.detailProgressEvent.documents.forEach(data => {
        this.documentationEventToApi.push({
          title: data?.title,
          date: data?.date,
          place: data?.place,
          description: data?.description,
          fileId: data?.file?.id,
          eventDocumentTypeId: data?.eventDocumentType?.id
        })
      })
    }

    let params = {
      eventId: this.detailEventId,
      topic: this.themeEvent,
      eventCode: this.eventCode,
      provinceId: this.selectedProvinceAndCity[0].provinceId.id,
      regencyId: regencyId,
      location: this.location,
      participantMale: this.participantMale,
      participantFemale: this.participantFemale,
      participantCount: this.participantCount,
      participantOtherInformation: this.participantOtherInformation,
      participantTarget: this.targetParticipant,
      implementationSummary: this.summaryEventOutput,
      eventEvaluationDescription: this.evaluationEventOutput,
      conclusionsAndRecommendations: this.conclutionAndAdvice,
      startDate: startDate,
      endDate: endDate,
      attachmentFileId: this.dataIdAttachmentFile,
      rraFileId: this.dataIdFileRaa,
      budgetSource: this.sourceFounds,
      budgetYear: this.dataDetailEvent?.awpImplementation?.awp?.year,
      makNumber: this.MakNumber,
      budgetCeiling: this.budgetCeiling,
      rraBudget: this.totalAnggaranRealitation,
      paragraph1: this.introductionParagraph1,
      paragraph2: this.introductionParagraph2,
      p3EventType: this.selectedEvent.id,
      p3StartDate: startDate,
      p3EndDate: endDate,
      paragraph4: this.introductionParagraph4,
      paragraph5: this.introductionParagraph5,
      technicalImplementer: this.selectedProjectOfficer.id,
      totalDuration: this.totalDurationParagraph3,
      schemaEvent: this.schemaEventParagraph3,
      newDocuments: this.documentationEventToApi,
      // newEventReportEvaluationPoints: this.listPointEvaluation,
      eventOutput: this.eventOutput,
      p3Location: this.locationEventParagraph3,
      activityModeId: this.selectedModaActivity.id,
      locationOtherInformation: this.tempatInformasiLainnya,
      newPokIds: pokRequestApi,
      newEventReportParticipantTargets: listParticipantTargetToApi,
    }

    this.serviceEvent.createEventReport(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveProgressEvent = false
            this.router.navigate(['activity/detail-event/' + this.detailEventId])
          }
        }, error => {
          this.loadingSaveProgressEvent = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


}
