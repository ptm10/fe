import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartQuestionnaireComponent } from './chart-questionnaire.component';

describe('ChartQuestionnaireComponent', () => {
  let component: ChartQuestionnaireComponent;
  let fixture: ComponentFixture<ChartQuestionnaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartQuestionnaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartQuestionnaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
