import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryQuestionnaireParticipantComponent } from './summary-questionnaire-participant.component';

describe('SummaryQuestionnaireParticipantComponent', () => {
  let component: SummaryQuestionnaireParticipantComponent;
  let fixture: ComponentFixture<SummaryQuestionnaireParticipantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryQuestionnaireParticipantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryQuestionnaireParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
