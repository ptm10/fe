import { Component, OnInit } from '@angular/core';
import {QuestionAnswers, SummaryResult} from "../../../../core/models/SummaryResult";
import {ActivatedRoute, Router} from "@angular/router";
import {EventService} from "../../../../core/services/Event/event.service";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-summary-questionnaire-evaluator',
  templateUrl: './summary-questionnaire-evaluator.component.html',
  styleUrls: ['./summary-questionnaire-evaluator.component.scss']
})
export class SummaryQuestionnaireEvaluatorComponent implements OnInit {

  //loading
  finishLoadData = true
  finishLoadDataCount = 0

  // variable
  breadCrumbItems: Array<{}>;
  idEvent: string
  listAllSummaryParticipant: SummaryResult
  resultCategoryCompliance: QuestionAnswers[] = []
  resultCategoryEconomy: QuestionAnswers[] = []
  resultCategoryEffective: QuestionAnswers[] = []
  resultCategoryEffisiens: QuestionAnswers[] = []
  resultCategoryOtherQuestion: QuestionAnswers[] = []

  constructor(
      private router: Router,
      private service: EventService,
      private activatedRoute: ActivatedRoute,
      private swalAlert: SwalAlert,
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Event' }, { label: 'Hasil Formulir Tanggapan Evaluator', active: true }]
    this.idEvent = this.activatedRoute.snapshot.paramMap.get('id')
    this.getListAllSummaryEvent()
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  goToListQuestion() {
    // this.router.navigate(['activity/event'])
    history.back();
  }

  getListAllSummaryEvent() {
    this.startLoading()
    this.service.getResultAllQuestionEvaluator(this.idEvent).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listAllSummaryParticipant = resp.data
            this.listAllSummaryParticipant?.questionCategories.forEach(x => {
              if (x.category === 1) {
                x.questionAnswers.forEach(y => {
                  this.resultCategoryCompliance.push(y)
                })
              } else if (x.category === 2) {
                x.questionAnswers.forEach(y => {
                  this.resultCategoryEconomy.push(y)
                })
              } else if (x.category === 3) {
                x.questionAnswers.forEach(y => {
                  this.resultCategoryEffective.push(y)
                })
              } else if (x.category === 4) {
                x.questionAnswers.forEach(y => {
                  this.resultCategoryEffisiens.push(y)
                })
              } else if (x.category === 5) {
                x.questionAnswers.forEach(y => {
                  this.resultCategoryOtherQuestion.push(y)
                })
              }
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
