import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryQuestionnaireEvaluatorComponent } from './summary-questionnaire-evaluator.component';

describe('SummaryQuestionnaireEvaluatorComponent', () => {
  let component: SummaryQuestionnaireEvaluatorComponent;
  let fixture: ComponentFixture<SummaryQuestionnaireEvaluatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SummaryQuestionnaireEvaluatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryQuestionnaireEvaluatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
