import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartQuestionnaireEvaluatorComponent } from './chart-questionnaire-evaluator.component';

describe('ChartQuestionnaireEvaluatorComponent', () => {
  let component: ChartQuestionnaireEvaluatorComponent;
  let fixture: ComponentFixture<ChartQuestionnaireEvaluatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartQuestionnaireEvaluatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartQuestionnaireEvaluatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
