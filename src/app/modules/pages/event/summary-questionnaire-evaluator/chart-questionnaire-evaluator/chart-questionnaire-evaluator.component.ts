import {Component, Input, OnInit} from '@angular/core';
import {ChartType} from "../../../../../core/models/apex.model";

@Component({
  selector: 'app-chart-questionnaire-evaluator',
  templateUrl: './chart-questionnaire-evaluator.component.html',
  styleUrls: ['./chart-questionnaire-evaluator.component.scss']
})
export class ChartQuestionnaireEvaluatorComponent implements OnInit {

  @Input() questionName: string
  @Input() questionAnswerYes: number
  @Input() questionAnswerNo: number
  @Input() respondentCount: number

  dataQuestionYesAndNo = []

  simplePieChart: ChartType;

  constructor() { }

  ngOnInit(): void {
    this.fetchData()
    this.dataQuestionYesAndNo.push(this.questionAnswerYes)
    this.dataQuestionYesAndNo.push(this.questionAnswerNo)
  }

  fetchData() {
    this.simplePieChart = {
      chart: {
        height: 240,
        type: 'pie',
      },
      series: this.dataQuestionYesAndNo,
      labels: ['Ya', 'Tidak'],
      colors: ['#1865E4', '#FF5B5B'],
      legend: {
        show: false,
        position: 'right',
        horizontalAlign: 'center',
        verticalAlign: 'middle',
        floating: false,
        fontSize: '14px',
        offsetX: 0,
        offsetY: -10
      },
      responsive: [{
        breakpoint: 600,
        options: {
          chart: {
            height: 240
          },
          legend: {
            show: false
          },
        }
      }]
    };
  }
}
