import {Component, OnInit, ViewChild} from '@angular/core';
import {AwpModel} from "../../../../core/models/awp.model";
import {ActivatedRoute, Router} from "@angular/router";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {PustakaService} from "../../../../core/services/Pustaka/pustaka.service";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {parse} from "date-fns";
import Swal from "sweetalert2";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {EventActivityService} from "../../../../core/services/EventActivity/event-activity.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {Pdfs} from "../../../../core/models/pdfViewer.model";
import {FileService} from "../../../../core/services/Image/file.service";
import {ProgressEventModel} from "../../../../core/models/progress-event.model";
import {ReportEventModel} from "../../../../core/models/report-event.model";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-event',
  templateUrl: './detail-event.component.html',
  styleUrls: ['./detail-event.component.scss']
})
export class DetailEventComponent implements OnInit {

  detailIdAwp: string
  dataDetailEvent: EventActivityModel
  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  loadingSavePdf = false
  currentDate: any
  awpYear: any
  loadingSaveData = true
  rejectMessage: string
  flagApproveReject: number
  existOnAnggaran: boolean
  checkedButton: number
  listProgressEvent: ProgressEventModel[] = []
  dateNow: Date
  getListEventReport: ReportEventModel[] = []
  participantPmu = []
  participantPcu = []
  unitLogin: string
  loadingExportWord = false
  //pdf
  pdfCountKegiatan: Pdfs[] = [];
  pdfCountRab: Pdfs[] = [];
  pdfSrcKegiatan: any
  pdfSrcRab: any

  showEmptyStatusProgressEvent = false
  showEmptyStatusEvent = false
  getUnit = localStorage.getItem('unit')
  getUserLogin = localStorage.getItem('user_id')


  ZOOM_STEP = 0.25;
  DEFAULT_ZOOM = 1;

  public zoomIn(pdf: Pdfs) {
    pdf.pdfZoom += this.ZOOM_STEP;
  }

  public zoomOut(pdf: Pdfs) {
    if (pdf.pdfZoom > this.DEFAULT_ZOOM) {
      pdf.pdfZoom -= this.ZOOM_STEP;
    }
  }

  public resetZoom(pdf: Pdfs) {
    pdf.pdfZoom = this.DEFAULT_ZOOM;
  }

  nextPage(pdf: Pdfs) {
    pdf.page += 1;
  }

  previousPage(pdf: Pdfs) {
    pdf.page -= 1;
  }

  afterLoadComplete(pdfData: any, pdf: Pdfs) {
    pdf.totalPages = pdfData.numPages;
  }


  @ViewChild('scrollEle') scrollEle;
  @ViewChild('scrollRef') scrollRef;
  constructor(
      private router:ActivatedRoute,
      private route: Router,
      private service: EventActivityService,
      private serviceFile: PustakaService,
      public roles: AuthfakeauthenticationService,
      private modalService: NgbModal,
      private serviceNotification: ProfileService,
      private serviceImage: FileService,
      private swalAlert: SwalAlert,
      private serviceAwp: AwpService
  ) { }

  ngOnInit(): void {
    this.unitLogin = localStorage.getItem('unit')
    this.breadCrumbItems = [{ label: 'Event' }, { label: 'Detail Event', active: true }]
    this.detailIdAwp = this.router.snapshot.paramMap.get('id')
    this.getDetailAwp()
    this.currentDate = new Date().getFullYear()
    this.updateStatusTask()
    this.checkedButton = 1
    this.getDetailEventProgress()
    this.dateNow = new Date()
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   * @param flagApproveReject
   */
  openModal(selectedMenu: any, flagApproveReject: any) {
    this.flagApproveReject = flagApproveReject
    this.modalService.open(selectedMenu, { size: 'md', centered: true });
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   * @param flagApproveReject
   */
  openModalTreasure(selectedMenu: any, flagApproveReject: any) {
    this.flagApproveReject = flagApproveReject
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  openModalHistory(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  downloadFile(id, name) {
    // this.loadingDownloadPdf = true
    this.service.exportFile(id, name).subscribe(
        resp => {
          // this.loadingDownloadPdf = false
        }, error => {
          // this.loadingDownloadPdf = false
        }
    )
  }

  openQuestionForParticipant(url: string){
    window.open(url, "_blank");
  }

  downloadWordConceptNote() {
    this.loadingExportWord = true
    this.serviceAwp.exportWordAwpConceptNote(this.dataDetailEvent.id, this.dataDetailEvent.name).subscribe(
        resp => {
          this.loadingExportWord = false
          this.closeModal()
        }, error => {
          this.loadingExportWord = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  checkConditionShowStatusProgressEvent() {
    let todayDate= new Date()
    if (todayDate <= this.dataDetailEvent.convertEndDate) {
      this.showEmptyStatusProgressEvent = true
    } else {
      this.showEmptyStatusProgressEvent = false
    }
  }

  checkConditionShowStatusEvent() {
    let todayDate = new Date()
    if (todayDate > this.dataDetailEvent.convertEndDate) {
      this.showEmptyStatusEvent = true
    } else {
      this.showEmptyStatusEvent = false
    }
    // showEmptyStatusEvent
  }

  getDetailAwp() {
    this.startLoading()
    this.service.detailEvent(this.detailIdAwp).subscribe(
        resp => {
          if (resp.success) {
            this.participantPcu = []
            this.participantPmu = []
            this.dataDetailEvent = resp.data
            this.dataDetailEvent.convertStartDate = new Date(this.dataDetailEvent.startDate)
            this.dataDetailEvent.convertEndDate = new Date(this.dataDetailEvent.endDate)
            this.existOnAnggaran = this.dataDetailEvent?.budgetAvailable
            if (this.dataDetailEvent?.staff.length > 0) {
              this.dataDetailEvent?.staff.forEach(x => {
                if (x.resources?.user?.component?.code === '-') {
                  this.participantPcu.push(x)
                } else if (x.resources?.user?.component?.code !== '-') {
                  this.participantPmu.push(x)
                }
              })
            }
            this.checkConditionShowStatusProgressEvent()
            if (this.dataDetailEvent.agendaFile) {
              if (this.dataDetailEvent?.agendaFile.fileExt === 'pdf') {
                this.getPdfKegiatanViewer(this.dataDetailEvent?.agendaFile?.id, 1)
              } else {
                this.getPdfKegiatanViewer(this.dataDetailEvent?.agendaFile?.id, 2)
              }
            }
            if (this.dataDetailEvent.rabFile) {
              if (this.dataDetailEvent?.rabFile.fileExt === 'pdf') {
                this.getPdfRabViewer(this.dataDetailEvent?.rabFile?.id, 1)
              } else {
                this.getPdfRabViewer(this.dataDetailEvent?.rabFile?.id, 2)
              }
            }
            if (this.getUnit === 'PCU') {
              if (this.participantPcu.length > 0) {
                let alloParticipantShowData = this.participantPcu.filter(x => x.resources.user.id === this.getUserLogin)
                if (alloParticipantShowData.length == 0) {
                  this.route.navigate(['404_not_found'])
                }
              } else {
                this.route.navigate(['404_not_found'])
              }
            }
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  goToListAWP() {
    this.route.navigate(['admin/awp'])
  }

  goToEditAwp() {
    this.route.navigate(['activity/edit-event/' + this.detailIdAwp])
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  goToListEvent() {
    this.route.navigate(['activity/event'])
  }

  updateFromCoordinator() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdAwp,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromCoordinator(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.getDetailAwp()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromTreasure() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdAwp,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
      budgetAvailable: this.existOnAnggaran
    }
    this.service.updateFromTreasure(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.getDetailAwp()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromHeadPmu() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdAwp,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromHeadPmu(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.getDetailAwp()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateStatusTask() {
    const params = {
      taskId: this.detailIdAwp,
      taskType: 6,
      status: 1
    }
    this.service.updateStatusTask(params).subscribe(
        resp => {
          if (resp.success) {
            this.serviceNotification.updateNotification.emit()
          }
        }
    )
  }

  getPdfKegiatanViewer(id, isPdf: number) {
    this.pdfCountKegiatan = []
    if (isPdf == 1) {
      this.serviceImage.getUrlPdf(id).subscribe(
          resp => {
            let reader = new FileReader();
            reader.addEventListener("load", () => {
              this.pdfSrcKegiatan = reader.result;
              const pdf: Pdfs = {
                src: this.pdfSrcKegiatan,
                isCollapsed: false,
                totalPages: 1,
                page: 1,
                pdfZoom: 1
              };
              this.pdfCountKegiatan.push(pdf);
            }, false);
            reader.readAsDataURL(resp);
          }
      )
    } else if (isPdf == 2) {
      this.serviceImage.getUrlWordExcel(id).subscribe(
          resp => {
            let reader = new FileReader();
            reader.addEventListener("load", () => {
              this.pdfSrcKegiatan = reader.result;
              const pdf: Pdfs = {
                src: this.pdfSrcKegiatan,
                isCollapsed: false,
                totalPages: 1,
                page: 1,
                pdfZoom: 1
              };
              this.pdfCountKegiatan.push(pdf);
            }, false);
            reader.readAsDataURL(resp);
          }
      )
    }
  }

  getPdfRabViewer(id, isPdf: number) {
    this.pdfCountRab = []
    if (isPdf == 1) {
      this.startLoading()
      this.serviceImage.getUrlPdf(id).subscribe(
          resp => {
            this.stopLoading()
            let reader = new FileReader();
            reader.addEventListener("load", () => {
              this.pdfSrcRab = reader.result;
              const pdf: Pdfs = {
                src: this.pdfSrcRab,
                isCollapsed: false,
                totalPages: 1,
                page: 1,
                pdfZoom: 1
              };
              this.pdfCountRab.push(pdf);
            }, false);
            reader.readAsDataURL(resp);
          }, error => {
            this.stopLoading()
          }
      )
    } else if (isPdf == 2) {
      this.startLoading()
      this.serviceImage.getUrlWordExcel(id).subscribe(
          resp => {
            this.stopLoading()
            let reader = new FileReader();
            reader.addEventListener("load", () => {
              this.pdfSrcRab = reader.result;
              const pdf: Pdfs = {
                src: this.pdfSrcRab,
                isCollapsed: false,
                totalPages: 1,
                page: 1,
                pdfZoom: 1
              };
              this.pdfCountRab.push(pdf);
            }, false);
            reader.readAsDataURL(resp);
          }, error => {
            this.stopLoading()
          }
      )
    }
  }

  goToCreateProgress() {
    this.route.navigate(['activity/progress-event/' + this.dataDetailEvent?.id])
  }

  goToCreateEvent() {
    this.route.navigate(['activity/add-report-event/' + this.dataDetailEvent?.id])
  }

  getDetailEventProgress() {
    const params = {
      enablePage: false,
      paramLike: [],
      paramIs: [
        {
          field: "eventId",
          dataType: "string",
          value: this.detailIdAwp
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "desc"
        }
      ],
      paramDateBetween: []
    }
    if (this.unitLogin === 'PMU') {
      this.service.getListEventProgress(params).subscribe(
          resp => {
            if (resp.success) {
              this.listProgressEvent = resp.data.content
            }
          }, error => {
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else if (this.unitLogin === 'PCU') {
      this.service.getListEventProgressPcu(params).subscribe(
          resp => {
            if (resp.success) {
              this.listProgressEvent = resp.data.content
            }
          }, error => {
            this.swalAlert.showAlertSwal(error)
          }
      )
    }
    this.service.getListEventReport(params).subscribe(
        resp => {
          if (resp.success) {
            this.getListEventReport = resp.data.content
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )

  }

  goToDetailProgressEvent(id) {
    this.route.navigate(['activity/detail-progress-event/' + id])
  }

  goToDetailReportEvent(id) {
    this.route.navigate(['activity/detail-report-event/' + id + '/2'])
  }

}
