import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddProgressEventComponent } from './add-progress-event.component';

describe('AddProgressEventComponent', () => {
  let component: AddProgressEventComponent;
  let fixture: ComponentFixture<AddProgressEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddProgressEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddProgressEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
