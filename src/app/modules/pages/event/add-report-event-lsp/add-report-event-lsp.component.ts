import { Component, OnInit } from '@angular/core';
import {ProgressEventModel} from "../../../../core/models/progress-event.model";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {EvaluationPoint} from "../../../../core/models/report-event.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {Event, EventDocumentType, Kegiatan, MangementType, PokCode, Province} from "../../../../core/models/awp.model";
import {
  NgbCalendar,
  NgbDate,
  NgbDateParserFormatter,
  NgbDateStruct,
  NgbModal,
  NgbRatingConfig
} from "@ng-bootstrap/ng-bootstrap";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {resourcesModel} from "../../../../core/models/resources.model";
import {ActivatedRoute, Router} from "@angular/router";
import {EventActivityService} from "../../../../core/services/EventActivity/event-activity.service";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import Swal from "sweetalert2";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {parse} from "date-fns";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {EventQuestion, QuestionnareEvent} from "../../../../core/models/Questionnare";

@Component({
  selector: 'app-add-report-event-lsp',
  templateUrl: './add-report-event-lsp.component.html',
  styleUrls: ['./add-report-event-lsp.component.scss']
})
export class AddReportEventLspComponent implements OnInit {

  selectValue: string[];
  selectValueParticipant: string[]

  //loading
  finishLoadData = true
  finishLoadDataCount = 0

  // detailProgressEvent: ProgressEventModel
  breadCrumbItems: Array<{}>;
  detailKegiatanId: string
  dataDetailEvent: EventActivityModel
  codeSelected: any
  selectedEventFromList: any

  public outputEvent = ClassicEditor;
  public descriptionEvent = ClassicEditor;
  public purposeEvent = ClassicEditor;
  public summaryEvent = ClassicEditor;
  public evaluationEvent = ClassicEditor;
  public paragraph1 = ClassicEditor;
  public paragraph2 = ClassicEditor;
  public paragraph3 = ClassicEditor;
  public paragraph4 = ClassicEditor;
  public paragraph5 = ClassicEditor;
  public participantTarget = ClassicEditor;

  fieldSubstantiv: number
  loadingSaveProgressEvent = false
  minDateYear: any
  maxDateYear: any
  listEvaluationPoint: EvaluationPoint[]
  dataDetailKegiatan: Kegiatan
  fromDateIntroduction: Date
  toDateIntroduction: Date
  listParticipantTargetFromApi: EvaluationPoint[]

  // data event
  selectedComponentExist: ComponentModel
  selectedSubComponentExist: ComponentModel
  selectedSubSubComponentExist: ComponentModel
  kegiatanName: string
  eventName: string
  selectedEvent: Event
  uploadFileExcelAgenda: Array<File> = [];
  uploadFileExcelIdsAgenda: Array<File> = [];
  uploadFileRaa: Array<File> = [];
  uploadFileRaaIdsAgenda: Array<File> = [];
  agendaFile: any
  raaFile: any
  dataIdAttachmentFile: string
  dataIdFileRaa: string
  fileDocumentationId: string
  eventPurpose: string = null
  eventDescription: string = null


  // create field
  existOnAnggaran: boolean
  existOnPok: boolean
  budgetAwp: number
  budgetPok: number
  timeOtherInformation: string
  location: string
  eventOutput: string
  participantCount: number
  participantMale: number
  participantFemale: number
  participantOtherInformation: string
  summaryEventOutput: string = null
  documentationEvent = []
  uploadFileDocumentationEvent: Array<File> = [];
  documentationEventName: string
  documentationEventDescription: string
  documentationEventDate: any
  documentationEventPlace: string
  documentationEventToApi = []
  themeEvent: string
  eventCode: string
  targetParticipant: string = null
  evaluationEventOutput: string = null
  conclutionAndAdvice: string = null
  sourceFounds: string
  yearBudget: any
  MakNumber: string
  totalAnggaranRealitation: number
  budgetCeiling: number
  listPointEvaluation = []
  introductionParagraph1: string = null
  introductionParagraph2: string = null
  introductionParagraph4: string = null
  introductionParagraph5: string = null
  totalDurationParagraph3: string
  schemaEventParagraph3: string
  locationEventParagraph3: string
  involveEventParagraph3: string


  // point evaluation
  evaluationTitle: string
  noteEvaluation: string
  valueEvaluationRating: number = 0
  readonly = true

  // sasaran peserta
  listParticipantTarget = []
  fromParticipantTarget: string
  totalParticipantTarget: number


  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;

  hoveredDateIntroduction: NgbDate | null = null;

  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  codeEvent: any
  minEvent: NgbDate
  maxEvent: NgbDate
  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  listCityAllExist: Province[]
  listCityAllMasterExist: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []
  listCityFromApi: string[];
  selectedCityFromList = []

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  listProvinceExist: Province[]
  listProvinceExistMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province
  provinceAndRegenciesExist = []
  searchFilterProvinces: any
  listMeqrResourceSelected = []

  // peserta
  listResources: resourcesModel[]
  listResourcesMaster: resourcesModel[]
  selectedResourcesExist: resourcesModel
  resourcesRequest = []
  resourcesRequestIds = []
  addResources = []
  removeResources = []
  searchResources = []
  otherStaffIds = []
  otherStaff = []

  //contact person
  listContactPerson: resourcesModel[]
  listContactPersonMaster: resourcesModel[]
  selectedContactPerson: resourcesModel

  //componen
  selectedUnitExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]

  // unit
  listComponentStaff: ComponentModel[]
  componentExist: ComponentModel[]

  // project officer
  listProjectOfficer = []
  listProjectOfficerMaster = []
  selectedProjectOfficer: resourcesModel
  loadingDataProjectOfficer = false

  nameNewStaff: string
  namePosition: string
  nameInstitution: string

  //Moda Kegiatan
  listModaType: MangementType[]
  listModaTypeMaster: MangementType[]
  selectedModaActivity: any

  public OtherInfoAwp = ClassicEditor;
  otherInformationAwp: string = null

  tempatInformasiLainnya: string

  typeDocumentation: string[] = []
  selectedTypeDocumentation: string
  listAllDocumentType: EventDocumentType[]

  // POK
  listCodePok: PokCode[]
  listCodePokMaster: PokCode[]
  pokRequest = []
  addPok = []
  removePok = []
  searchPok = []
  pokExisting = []

  // question evaluator
  existingQuestionCompliance: EventQuestion[]  = []
  existingQuestionEconomy: EventQuestion[]  = []
  existingQuestionEfective: EventQuestion[]  = []
  existingQuestionEfisiens: EventQuestion[]  = []
  existingQuestionParticipant: EventQuestion[]  = []
  listAllQuestionEvent: EventQuestion[]
  questionAnswerAllCategory = []

  // field form participant
  listLinkParticipant: string[] = []
  selectedLinkParticipant: string
  modelEventParticipant: EventActivityModel[]

  constructor(
      private activatedRoute: ActivatedRoute,
      private serviceEvent: EventActivityService,
      private router: Router,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private serviceAwp: AwpService,
      private modalService: NgbModal,
      private serviceResources: KegiatanService,
      private config: NgbRatingConfig,
      private serviceKegiatan: KegiatanService,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Kegiatan' }, { label: 'Detail Kegiatan'}, { label: 'Tambah Laporan Event', active: true }]
    this.fieldSubstantiv = 1
    this.detailKegiatanId = this.activatedRoute.snapshot.paramMap.get('id')
    // this.getDetailEvent()
    this.config.max = 5;
    this.minDateYear = new Date(2020, 1, 1)
    this.maxDateYear = new Date(2024, 11, 30)
    this.yearBudget = new Date()
    this.introductionParagraph1 = '<p>Event Workshop/Bimbingan Teknis/Pelatihan/Konsinyering merupakan salah satu tahapan Event yang diperlukan dalam rangka pencapaian Kerangka Hasil (Result Framework) sebagaimana tercantum dalam Project Operation Manual (POM) dan Annual Work Plan (AWP) Tahun Anggaran 2021 yang ditetapkan oleh Project Management Unit Realizing Education’s Promise – Madrasah Education Quality Reform (IBRD 8992-ID).\t</p>'
    this.introductionParagraph2 = '<p>Event ini bertujuan untuk</p>'
    this.introductionParagraph4 = '<p>Kami menyampaikan apresiasi dan terima kasih yang setinggi-tingginya kepada para pihak yang telah memberikan kontribusi besar sehingga Event dapat terlaksana dengan lancar. Penghargaan dan terima kasih juga kami sampaikan kepada para narasumber, moderator, dan seluruh panitia pelaksana yang telah berpartisipasi dalam menyukseskan Event ini.</p>'
    this.introductionParagraph5 = '<p>Akhirnya, kami berharap laporan Event ini dapat bermanfaat sebagai salah satu bentuk transparansi dan akuntabilitas pelaksanaan Event. Kami juga berharap Event ini dapat memberikan kontribusi positif sebagai necessary conditions dalam peningkatan mutu pendidikan madrasah.</p>'
    this.getListEventEvaluationPoint()
    this.getDetailEvent()
    this.listAllModa()
    this.getListParticipantTarget()
    this.listEventDocumentType()
    this.getListAllLinkForm()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  changeSelectedFormQuestion() {
    this.selectedEventFromList = null
    if (this.selectedLinkParticipant) {
      let filterSelectedEvent = this.modelEventParticipant.filter(x => x.name === this.selectedLinkParticipant)
      this.selectedEventFromList = filterSelectedEvent[0].id
    }
  }

  goToResultFormEvaluator() {
    this.router.navigate([]).then(result => {  window.open('activity/summary-evaluator/' + this.selectedEventFromList); })
  }

  goToResultFormParticipant() {
    this.router.navigate([]).then(result => {  window.open('activity/summary-participant/' + this.selectedEventFromList); })
  }

  getListAllLinkForm() {
    let tag = []
    let params = {
      enablePage: false,
      paramIs: [
        {
          field: "awpImplementationId",
          dataType: "string",
          value: this.detailKegiatanId
        }
      ],
      "sort": [
        {
          "field": "createdAt",
          "direction": "desc"
        }
      ]
    }
    this.serviceAwp.listEventFormLsp(params).subscribe(
        resp => {
          if (resp.success) {
            this.modelEventParticipant = resp.data.content
            this.modelEventParticipant.forEach(x => {
              if (x.repots?.length == 0) {
                tag.push(x.name)
              }
            })
            this.listLinkParticipant = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * POK Code */
  listPokEvent() {
    this.startLoading()
    this.serviceResources.listCodePok().subscribe(
        resp => {
          if (resp.success) {
            this.listCodePok = resp.data.content
            this.listCodePokMaster = resp.data.content
            this.searchPok = resp.data.content
            if (this.pokRequest.length > 0) {
              this.pokRequest.forEach(dataPokExist => {
                this.listCodePok = this.listCodePok.filter(x => x.id !== dataPokExist?.id)
                this.searchPok = this.listCodePok.filter(x => x.id !== dataPokExist?.id)
              })
            }
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addPokSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pokRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePok.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePok.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPok.push(data)
        }
        this.pokRequest.push(data)
      }
    } else {
      this.pokRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pokRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPok.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPok.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePok.push(data)
      }
    }
    this.listCodePok = this.listCodePokMaster

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.id)
      })
    }

    this.pokRequest.forEach(dataProvince => {
      this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPok = this.listCodePok
  }

  searchFilterPok(e) {
    const searchStr = e.target.value
    this.listCodePok = this.searchPok.filter((product) => {
      return (product.code.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.title.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePokExisting(data) {
    this.pokExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.pokExisting.splice(index, 1)
      }
    })
    this.listCodePok = this.listCodePokMaster
    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.pok?.id)
      })
      this.pokRequest.forEach(dataPok => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataPok.id)
      })
    } else {
      this.pokRequest.forEach(dataProvince => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPok = this.listCodePok
  }

  /** * End POK Code */

  /** Input date calender */
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  goToDetailEvent() {
    this.router.navigate(['implement/detail-kegiatan/' + this.detailKegiatanId])
  }

  changeMenuFieldProgress(menu: any) {
    this.fieldSubstantiv = menu
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.serviceAwp.listModa().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  calculatePs(event, from) {
    if (this.participantMale || this.participantFemale) {
      if (from == 1) {
        if (this.participantMale < this.participantCount) {
          this.participantFemale = 0
          if (this.participantMale + this.participantFemale < this.participantCount) {
            this.participantFemale = this.participantCount - this.participantMale
          }
        } else {
          this.participantMale = this.participantCount
          this.participantFemale = 0
        }
      } else {
        if (this.participantFemale < this.participantCount) {
          this.participantMale = 0
          if (this.participantMale + this.participantFemale < this.participantCount) {
            this.participantMale = this.participantCount - this.participantFemale
          }
        } else {
          this.participantFemale = this.participantCount
          this.participantMale = 0
        }
      }
    }
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  getDetailEvent() {
    this.startLoading()
    this.serviceKegiatan.detailKegiatan(this.detailKegiatanId).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.participantCount = 0
            this.dataDetailKegiatan = resp.data
            this.selectedComponentExist = this.dataDetailKegiatan?.component
            this.selectedSubComponentExist = this.dataDetailKegiatan?.subComponent
            this.selectedSubSubComponentExist = this.dataDetailKegiatan?.subSubComponent
            this.codeSelected = this.dataDetailKegiatan?.componentCode?.code
            this.kegiatanName = this.dataDetailKegiatan?.name
            this.selectedEvent = this.dataDetailKegiatan?.eventType
            this.existOnPok = this.dataDetailKegiatan?.pokEvent
            this.budgetAwp = this.dataDetailKegiatan?.budgetAwp
            this.budgetPok = this.dataDetailKegiatan?.budgetPok
            this.selectedProjectOfficer = this.dataDetailKegiatan?.projectOfficerResources
            this.dataDetailKegiatan.convertStartDate = new Date(this.dataDetailKegiatan.startDate)
            this.dataDetailKegiatan.convertEndDate = new Date(this.dataDetailKegiatan.endDate)
            this.minEvent = new NgbDate(this.dataDetailKegiatan.convertStartDate.getFullYear(), this.dataDetailKegiatan.convertStartDate.getMonth() + 1, this.dataDetailKegiatan.convertStartDate.getDate())
            this.maxEvent = new NgbDate(this.dataDetailKegiatan.convertEndDate.getFullYear(), this.dataDetailKegiatan.convertEndDate.getMonth() + 1, this.dataDetailKegiatan.convertEndDate.getDate())
            this.fromDate = new NgbDate(this.dataDetailKegiatan.convertStartDate.getFullYear(), this.dataDetailKegiatan.convertStartDate.getMonth() + 1, this.dataDetailKegiatan.convertStartDate.getDate())
            this.toDate = new NgbDate(this.dataDetailKegiatan.convertEndDate.getFullYear(), this.dataDetailKegiatan.convertEndDate.getMonth() + 1, this.dataDetailKegiatan.convertEndDate.getDate())
            this.timeOtherInformation = this.dataDetailKegiatan?.eventOtherInformation
            this.eventOutput = this.dataDetailKegiatan?.eventOutput
            this.participantOtherInformation = this.dataDetailKegiatan?.participantOtherInformation
            this.fromDateIntroduction = parse(this.dataDetailKegiatan.startDate, 'yyyy-MM-dd', new Date());
            this.toDateIntroduction = parse(this.dataDetailKegiatan.endDate, 'yyyy-MM-dd', new Date());
            this.listMeqrResourceSelected = JSON.parse(JSON.stringify(this.resourcesRequest))
            this.targetParticipant = this.dataDetailKegiatan?.participantTarget
            if (this.dataDetailKegiatan?.activityMode) {
              this.selectedModaActivity = this.dataDetailKegiatan?.activityMode
            } else {
              this.selectedModaActivity = this.dataDetailKegiatan?.awp?.activityMode
            }
            if (this.dataDetailKegiatan?.nameOtherInformation) {
              this.otherInformationAwp = this.dataDetailKegiatan?.nameOtherInformation
            } else {
              this.otherInformationAwp = this.dataDetailKegiatan?.awp?.nameOtherInformation
            }
            if (this.dataDetailKegiatan?.locationOtherInformation) {
              this.tempatInformasiLainnya = this.dataDetailKegiatan?.locationOtherInformation
            } else {
              this.tempatInformasiLainnya = this.dataDetailKegiatan?.awp?.locationOtherInformation
            }
            if (this.dataDetailKegiatan.pok.length > 0) {
              this.dataDetailKegiatan.pok.forEach(x => {
                this.pokRequest.push(x.pok)
              })
            }
            this.listPokEvent()
            this.listProvince()
            this.setTimeRangeEvent()
            this.listComponent()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  /** * List Project Officer */
  selectProjectOfficer(data) {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster

    this.selectedProjectOfficer = data
    this.listProjectOfficer = this.listProjectOfficer.filter(dataFilter => dataFilter.id != this.selectedProjectOfficer.id)
  }

  removeProjectOfficer() {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster
  }

  searchFilterProjectOfficer(e) {
    const searchStr = e.target.value
    let filter = this.listProjectOfficerMaster
    if (this.selectedProjectOfficer) {
      filter = filter.filter(x => x.id !== this.selectedProjectOfficer.id)
    } else {
      filter = this.listProjectOfficerMaster
    }

    this.listProjectOfficer = filter.filter((product) => {
      return ((product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List Project Officer */
  dataResources() {
    let dataListResource = []
    this.loadingDataProjectOfficer = true
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.selectedUnitExist.id
        }
      ],
      paramIn: [],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.serviceResources.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDataProjectOfficer = false
            this.listResources = resp.data.content
            this.listResourcesMaster = resp.data.content
            this.searchResources = resp.data.content
            const result = this.listResources.filter(({ id: id1 }) => !this.resourcesRequest.some(({ id: id2 }) => id2 === id1));
            this.listResources = result
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Add New Unit*/

  selectedUnit(data) {
    this.selectedUnitExist = null
    this.listComponentStaff = this.componentExist

    this.selectedUnitExist = data
    this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedUnitExist.id)
    this.dataResources()
  }

  searchFilterUnit(e) {
    const searchStr = e.target.value
    let filter = this.componentExist
    if (this.selectedUnitExist) {
      filter = filter.filter(x => x.id !== this.selectedUnitExist.id)
    } else {
      filter = this.componentExist
    }

    this.listComponentStaff = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  listComponent() {
    this.serviceResources.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff = resp.data.content
            this.listComponentStaff.forEach(dataCode => {
              dataCode.code = 'Komponen ' + dataCode.code
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  setTimeRangeEvent() {
    let currentDate = new Date()
    // let newConvertDate = new NgbDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate())
    // if (currentDate >= this.dataDetailEvent.awpImplementation.convertStartDate) {
    //   this.minDate = {
    //     year: newConvertDate.year,
    //     month: newConvertDate.month,
    //     day: newConvertDate.day + 1
    //   };
    // } else {
    //   this.minDate = {
    //     year: this.minEvent.year,
    //     month: this.minEvent.month,
    //     day: this.minEvent.day
    //   };
    // }
    this.minDate = {
      year: this.minEvent.year,
      month: this.minEvent.month,
      day: this.minEvent.day
    };

    this.maxDate = {
      year: this.maxEvent.year,
      month: this.maxEvent.month,
      day: this.maxEvent.day
    };
  }


  /** * List Province */
  listProvince() {
    let dataProvinceExist = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            this.listProvinceExist = resp.data.content
            // this.listProvinceAll = this.listProvinceAll.filter(x => x.id !== this.selectedProvinceExist.id)

            this.dataDetailKegiatan?.provincesRegencies.forEach(dataProvince => {
              let dataExist = this.listProvinceAll.find(x => dataProvince.province.id == x.id)
              if (dataExist) {
                dataProvinceExist.push(dataExist)
              }
            })

            this.listProvinceExist = dataProvinceExist
            this.listProvinceExistMaster = dataProvinceExist
            this.listCity()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedProvinceExist = null
    this.listProvinceExist = this.listProvinceExistMaster

    this.selectedProvinceExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      this.listProvinceExist = this.listProvinceExistMaster
    }
    console.log(this.listProvinceExistMaster)
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    console.log(searchStr)
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    if (searchStr == '') {
      this.listProvinceExist = this.listProvinceExistMaster
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      return this.listProvinceExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  /** * End List Province */

  /** * List City */

  listCity() {
    let cityExistFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.serviceAwp.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            this.listCityAllExist = resp.data.content
            let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
            if (checkProvinceExist.length > 0) {
              this.dataDetailKegiatan?.provincesRegencies.forEach(dataProvince => {
                if (dataProvince.regencies.length > 0) {
                  dataProvince.regencies.forEach(dataCity => {
                    let cityExist = this.listCityAll.find(x => dataCity.regency?.id == x.id)
                    if (cityExist) {
                      cityExistFromApi.push(cityExist)
                    }
                  })
                }
              })

              this.listCityAllExist = cityExistFromApi
              this.listCityAllMasterExist = JSON.parse(JSON.stringify(this.listCityAllExist))
            } else {
              this.listCityAllExist = this.listCityAll
            }

            this.listCityAllMaster = resp.data.content
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist

    this.selectedCityExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    } else {
      this.listCityAllExist = this.listCityAll
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    }
  }

  searchFilterCity(e) {
    const searchStr = e.target.value
    let filter = this.listCityAll
    if (this.selectedCityExist) {
      filter = filter.filter(x => x.id !== this.selectedCityExist.id)
    } else {
      filter = this.listCityAll
    }

    if (searchStr == '') {
      let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
      if (checkProvinceExist.length > 0) {
        this.listCityAllExist = this.listCityAllMasterExist
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      } else {
        this.listCityAllExist = this.listCityAll
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      }
    } else {
      return this.listCityAllExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  addSelectedProvinceAndCity() {
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityExist
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.selectedProvinceExist = null
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist
    this.listProvinceExist = this.listProvinceExistMaster
    console.log(this.listProvinceExist)
    console.log(this.listProvinceExistMaster)
  }

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }
  /** * end date */

  /** New Documentation Event */

  listEventDocumentType() {
    let tag = []
    this.serviceEvent.getListDocumentType().subscribe(
        resp => {
          if (resp.success) {
            this.listAllDocumentType = resp.data.content
            this.listAllDocumentType.forEach(x => {
              tag.push(x.name)
            })
          }
          this.typeDocumentation = tag
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addDocumentationEvent() {
    let startDate
    let selectedTypeDocument
    if (this.documentationEventDate) {
      startDate = this.documentationEventDate.year.toString() + '-' + this.documentationEventDate.month.toString().padStart(2, '0') + '-' + this.documentationEventDate.day.toString().padStart(2, '0')
    } else {
      startDate = null
    }

    selectedTypeDocument = this.listAllDocumentType.filter(x => x.name === this.selectedTypeDocumentation)

    this.documentationEvent.push({
      title: this.documentationEventName,
      date: startDate,
      place: this.documentationEventPlace,
      description: this.documentationEventDescription,
      fileIds: this.uploadFileDocumentationEvent,
      fileId: null,
      eventDocumentTypeId: selectedTypeDocument[0].id,
      eventDocumentTypeName: this.selectedTypeDocumentation
    })
    this.documentationEventName = ''
    this.documentationEventDate = null
    this.documentationEventPlace = ''
    this.documentationEventDescription = ''
    this.selectedTypeDocumentation = ''
    this.uploadFileDocumentationEvent = []
    this.closeModal()
  }
  removeDocumentationFile(data) {
    this.documentationEvent.forEach((check, index) => {
      if (check.title === data.title) {
        this.documentationEvent.splice(index, 1)
      }
    })
  }
  // clearOldDocuments(data) {
  //   this.detailProgressEvent?.documents.forEach((check, index) => {
  //     if (check.id === data.id) {
  //       this.detailProgressEvent?.documents.splice(index, 1)
  //     }
  //   })
  // }

  addEvaluationPoint() {
    this.listPointEvaluation.push({
      id: this.listPointEvaluation.length + 1,
      newEvaluationPoint: this.evaluationTitle,
      evaluationPointId: null,
      value: this.valueEvaluationRating,
      note: this.noteEvaluation
    })
    this.closeModal()
    this.evaluationTitle = ''
    this.valueEvaluationRating = 0
    this.noteEvaluation = ''
  }
  clearEvaluationPointNew(data) {
    this.listPointEvaluation.forEach((check, index) => {
      if (data.id === check.id) {
        this.listPointEvaluation.splice(index, 1)
      }
    })
  }

  /** New Participant Target Event */


  CreateNewParticipantTarget(data){
    return data
  }

  addNewParticipantTarget() {
    let filtered = this.listParticipantTargetFromApi.filter(x => x.name === this.fromParticipantTarget)
    if (filtered.length > 0) {
      this.listParticipantTarget.push({
        id: this.listParticipantTarget.length + 1,
        newParticipantTarget: this.fromParticipantTarget,
        participantTargetId: filtered[0].id,
        value: this.totalParticipantTarget
      })
    } else {
      this.listParticipantTarget.push({
        id: this.listParticipantTarget.length + 1,
        newParticipantTarget: this.fromParticipantTarget,
        participantTargetId: null,
        value: this.totalParticipantTarget
      })
    }
    this.closeModal()
    this.fromParticipantTarget = ''
    this.totalParticipantTarget = null
  }

  clearNewParticipantTarget(data) {
    this.listParticipantTarget.forEach((check, index) => {
      if (data.id === check.id) {
        this.listParticipantTarget.splice(index, 1)
      }
    })
  }

  getListParticipantTarget() {
    this.startLoading()
    let tag = []
    this.serviceEvent.getListParticipantEvent().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            tag = []
            this.listParticipantTargetFromApi = resp.data.content
            this.listParticipantTargetFromApi.forEach(x => {
              tag.push(x.name)
            })
            this.selectValueParticipant = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** End Participant Target Event */


  getListEventEvaluationPoint() {
    this.startLoading()
    let tag = []
    this.serviceEvent.getListEventEvaluation().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            tag = []
            this.listEvaluationPoint = resp.data.content
            this.listEvaluationPoint.forEach(x => {
              tag.push(x.name)
            })
            this.selectValue = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  CreateNew(city){
    return city
  }

  /** Upload File Kegiatan */
  doUploadAttachmentFile() {
    this.loadingSaveProgressEvent = true
    if (this.uploadFileExcelAgenda.length > 0) {
      this.serviceResources.uploadAttachment(this.uploadFileExcelAgenda[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdAttachmentFile = resp.data[0].id
              this.doUploadAttachmentRaa()
            } else {
              this.loadingSaveProgressEvent = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveProgressEvent = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    }
  }

  doUploadAttachmentRaa() {
    if (this.uploadFileRaa.length > 0) {
      this.serviceResources.uploadKegiatanRaa(this.uploadFileRaa[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdFileRaa = resp.data[0].id
              this.doUploadRab()
            } else {
              this.loadingSaveProgressEvent = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveProgressEvent = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    }
  }

  doUploadRab() {
    this.documentationEventToApi = []
    if (this.documentationEvent.length > 0) {
      this.documentationEvent.forEach(data => {
        this.serviceResources.uploadDocumentation(data?.fileIds[0]).subscribe(resp => {
              if (resp.success) {
                this.fileDocumentationId = resp.data[0].id
                data.fileId = this.fileDocumentationId
                this.documentationEventToApi.push({
                  title: data?.title,
                  date: data?.date,
                  place: data?.place,
                  description: data?.description,
                  fileId: this.fileDocumentationId,
                  eventDocumentTypeId: data?.eventDocumentTypeId
                })
                if (this.documentationEvent.length == this.documentationEventToApi.length) {
                  this.createReportEvent()
                }
              } else {
                this.loadingSaveProgressEvent = false
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: resp.message,
                });
              }
            }, error => {
              this.loadingSaveProgressEvent = false
              this.swalAlert.showAlertSwal(error)
            }
        );
      })
    }
  }

  createReportEvent() {
    let regencyId
    let startDate
    let endDate
    let startDateP3
    let endDateP3
    let listEvaluationFiltered = JSON.parse(JSON.stringify(this.listPointEvaluation))
    let listEvaluationToApi = []
    let pokRequestApi = []
    let listParticipantTargetToApi = []
    let listParticipantFiltered = JSON.parse(JSON.stringify(this.listParticipantTarget))
    let selectedLinkParticipant

    if (this.pokRequest.length > 0) {
      this.pokRequest.forEach(x => {
        pokRequestApi.push(x.id)
      })
    }

    listParticipantFiltered.forEach(x => {
      if (x.participantTargetId !== null) {
        x.newParticipantTarget = null
      }
      listParticipantTargetToApi.push({
        newParticipantTarget: x.newParticipantTarget,
        participantTargetId: x.participantTargetId,
        value: x.value
      })
    })


    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')

    listEvaluationFiltered.forEach(x => {
      if (x.evaluationPointId !== null) {
        x.newEvaluationPoint = null
      }
      listEvaluationToApi.push({
        newEvaluationPoint: x.newEvaluationPoint,
        evaluationPointId: x.evaluationPointId,
        value: x.value,
        note: x.note
      })
    })

    let filterSelectedEvent = this.modelEventParticipant.filter(x => x.name === this.selectedLinkParticipant)
    selectedLinkParticipant = filterSelectedEvent[0].id



    if (this.selectedProvinceAndCity[0]?.regencyId) {
      regencyId = this.selectedProvinceAndCity[0]?.regencyId.id
    } else {
      regencyId = null
    }
    /** POST save event */
    let params1 = {
      awpImplementationId: this.detailKegiatanId,
      name: this.eventName,
      startDate,
      endDate,
      eventOtherInformation: this.dataDetailKegiatan?.eventOtherInformation,
      eventTypeId: this.selectedEvent.id,
      pokEvent: this.dataDetailKegiatan?.pokEvent,
      budgetPok: this.dataDetailKegiatan?.budgetPok,
      description: this.eventDescription,
      purpose: this.eventPurpose,
      eventOutput: this.eventOutput,
      participantMale: this.participantMale,
      participantFemale: this.participantFemale,
      participantCount: this.participantCount,
      participantOtherInformation: this.participantOtherInformation,
      provinceId: this.selectedProvinceAndCity[0].provinceId.id,
      regencyId,
      location: this.location,
      agendaFileId: this.dataIdAttachmentFile
    }
    this.serviceEvent.updateEvent(selectedLinkParticipant, params1).subscribe(

        // this.serviceEvent.createEvent(params1).subscribe(
        resp => {
          if (resp.success) {
            /** Getting response id data */
            console.log(resp.data);
            // this.detailKegiatanId = resp.data.id
            /** POST save event report */
            let params2 = {
              eventId: resp.data.id,
              topic: this.themeEvent,
              eventCode: this.eventCode,
              provinceId: this.selectedProvinceAndCity[0].provinceId.id,
              regencyId: regencyId,
              location: this.location,
              participantMale: this.participantMale,
              participantFemale: this.participantFemale,
              participantCount: this.participantCount,
              participantOtherInformation: this.participantOtherInformation,
              participantTarget: this.targetParticipant,
              implementationSummary: this.summaryEventOutput,
              eventEvaluationDescription: this.evaluationEventOutput,
              conclusionsAndRecommendations: this.conclutionAndAdvice,
              startDate: startDate,
              endDate: endDate,
              attachmentFileId: this.dataIdAttachmentFile,
              rraFileId: this.dataIdFileRaa,
              budgetSource: this.sourceFounds,
              budgetYear: this.dataDetailKegiatan?.awp?.year,
              makNumber: this.MakNumber,
              budgetCeiling: this.budgetCeiling,
              rraBudget: this.totalAnggaranRealitation,
              paragraph1: this.introductionParagraph1,
              paragraph2: this.introductionParagraph2,
              p3EventType: this.selectedEvent.id,
              p3StartDate: startDateP3,
              p3EndDate: endDateP3,
              paragraph4: this.introductionParagraph4,
              paragraph5: this.introductionParagraph5,
              // technicalImplementer: this.dataDetailKegiatan?.lspPicUser.id,
              totalDuration: this.totalDurationParagraph3,
              schemaEvent: this.schemaEventParagraph3,
              newDocuments: this.documentationEventToApi,
              newEventReportEvaluationPoints: this.listPointEvaluation,
              eventOutput: this.eventOutput,
              p3Location: this.locationEventParagraph3,
              activityModeId: this.selectedModaActivity.id,
              locationOtherInformation: this.tempatInformasiLainnya,
              newPokIds: pokRequestApi,
              newEventReportParticipantTargets: listParticipantTargetToApi,
            }
            this.serviceEvent.createEventReport(params2).subscribe(
                resp => {
                  if (resp.success) {
                    this.loadingSaveProgressEvent = false
                    this.router.navigate(['implement/detail-kegiatan/' + this.detailKegiatanId])
                  }
                }, error => {
                  this.loadingSaveProgressEvent = false
                  this.swalAlert.showAlertSwal(error)
                }
            )
          }
        }, error => {
          this.loadingSaveProgressEvent = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
