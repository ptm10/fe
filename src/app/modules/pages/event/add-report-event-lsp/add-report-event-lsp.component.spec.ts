import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddReportEventLspComponent } from './add-report-event-lsp.component';

describe('AddReportEventLspComponent', () => {
  let component: AddReportEventLspComponent;
  let fixture: ComponentFixture<AddReportEventLspComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddReportEventLspComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddReportEventLspComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
