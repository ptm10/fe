import { Component, OnInit } from '@angular/core';
import {ProgressEventModel} from "../../../../core/models/progress-event.model";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {Event, EventDocumentType, MangementType, Province} from "../../../../core/models/awp.model";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {resourcesModel} from "../../../../core/models/resources.model";
import {ActivatedRoute, Router} from "@angular/router";
import {EventActivityService} from "../../../../core/services/EventActivity/event-activity.service";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import Swal from "sweetalert2";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {SwalAlert} from "../../../../core/helpers/Swal";
import {ListUnit} from "../../../../core/models/RoleUsers";
import {ProfileService} from "../../../../core/services/Profile/profile.service";

@Component({
  selector: 'app-edit-progress-event',
  templateUrl: './edit-progress-event.component.html',
  styleUrls: ['./edit-progress-event.component.scss']
})
export class EditProgressEventComponent implements OnInit {

  detailProgressEvent: ProgressEventModel
  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  detailEventId: string
  // dataDetailEvent: EventActivityModel
  codeSelected: any

  public outputEvent = ClassicEditor;
  public summaryEvent = ClassicEditor;
  fieldSubstantiv: number
  loadingSaveProgressEvent = false
  deletedDocumentIds = []

  // data event
  selectedComponentExist: ComponentModel
  selectedSubComponentExist: ComponentModel
  selectedSubSubComponentExist: ComponentModel
  kegiatanName: string
  eventName: string
  selectedEvent: Event
  uploadFileExcelAgenda: Array<File> = [];
  uploadFileExcelIdsAgenda: Array<File> = [];
  uploadFileRaa: Array<File> = [];
  uploadFileRaaIdsAgenda: Array<File> = [];
  agendaFile: any
  raaFile: any
  dataIdAttachmentFile: string
  dataIdFileRaa: string
  fileDocumentationId: string
  typeDocumentation: string[] = []
  selectedTypeDocumentation: string
  listAllDocumentType: EventDocumentType[]

  // create field
  existOnAnggaran: boolean
  existOnPok: boolean
  budgetAwp: number
  budgetPok: number
  timeOtherInformation: string
  location: string
  eventOutput: string
  participantCount: number
  participantMale: number
  participantFemale: number
  participantOtherInformation: string
  summaryEventOutput: string = null
  documentationEvent = []
  uploadFileDocumentationEvent: Array<File> = [];
  documentationEventName: string
  documentationEventDescription: string
  documentationEventDate: any
  documentationEventPlace: string
  documentationEventToApi = []
  tempatInformasiLainnya: string


// date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  codeEvent: any
  minEvent: NgbDate
  maxEvent: NgbDate
  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  listCityAllExist: Province[]
  listCityAllMasterExist: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []
  listCityFromApi: string[];
  selectedCityFromList = []

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  listProvinceExist: Province[]
  listProvinceExistMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province
  provinceAndRegenciesExist = []
  searchFilterProvinces: any
  listMeqrResourceSelected = []

  // peserta
  listResources: resourcesModel[]
  listResourcesMaster: resourcesModel[]
  selectedResourcesExist: resourcesModel
  resourcesRequest = []
  resourcesRequestIds = []
  addResources = []
  removeResources = []
  searchResources = []
  otherStaffIds = []
  otherStaff = []
  deletedOtherStaffIds = []
  deletedStaffIds = []

  //contact person
  listContactPerson: resourcesModel[]
  listContactPersonMaster: resourcesModel[]
  selectedContactPerson: resourcesModel

  //componen
  selectedUnitExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]

  // unit
  listComponentStaff: ComponentModel[]
  componentExist: ComponentModel[]

  // project officer
  listProjectOfficer: resourcesModel[]
  listProjectOfficerMaster: resourcesModel[]
  selectedProjectOfficer: resourcesModel
  loadingDataProjectOfficer = false

  //Moda Kegiatan
  listModaType: MangementType[]
  listModaTypeMaster: MangementType[]
  selectedModaActivity: any

  nameNewStaff: string
  namePosition: string
  nameInstitution: string

  listMeqrPmuSelected = []
  listMeqrPcuSelected = []

  //work unit
  selectedWorkUnit: string
  listUnit: ListUnit[]

  //province
  listProvinceAllStaff: Province[]
  listProvinceAllMasterStaff: Province[]
  listProvinceApi: string[];
  selectedProvinceStaff: string

  constructor(
      private activatedRoute: ActivatedRoute,
      private serviceEvent: EventActivityService,
      private router: Router,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private serviceAwp: AwpService,
      private modalService: NgbModal,
      private serviceResources: KegiatanService,
      private swalAlert: SwalAlert,
      private serviceProfile: ProfileService
  ) { }

  ngOnInit(): void {
    this.detailEventId = this.activatedRoute.snapshot.paramMap.get('id')
    this.breadCrumbItems = [{ label: 'Event' }, { label: 'Detail Event'}, { label: 'Edit Laporan Kemajuan', active: true }]
    this.getDetailEvent()
    this.fieldSubstantiv = 1
    this.listProvinceStaff()
    this.getListWorkUnit()
    this.listEventDocumentType()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  changeSelectedWorkUnit() {
    this.selectedUnitExist = null
    this.listComponentStaff = this.componentExist
    this.selectedProvinceStaff = null
  }

  changeUserStaffPcu() {
    this.dataResources()
  }

  getListWorkUnit() {
    this.startLoading()
    this.serviceProfile.listUnit().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listUnit = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  checkValidationAddStaff() {
    if (this.selectedWorkUnit) {
      if (this.selectedWorkUnit === 'PMU') {
        return !this.selectedUnitExist
      } else if (this.selectedWorkUnit === 'PCU') {
        return !this.selectedProvinceStaff
      }
    } else {
      return true
    }
  }
  /** * End Work Unit */

  /** * List Province */

  listProvinceStaff() {
    let tag = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAllStaff = resp.data.content
            this.listProvinceAllMasterStaff = resp.data.content
            this.listProvinceAllStaff.forEach(x => {
              tag.push(x.name)
            })
            this.listProvinceApi = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End List Province */

  changeMenuFieldProgress(menu: any) {
    this.fieldSubstantiv = menu
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  getDetailEvent() {
    this.startLoading()
    this.serviceEvent.detailEventProgress(this.detailEventId).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.detailProgressEvent = resp.data
            this.existOnAnggaran = this.detailProgressEvent?.budgetAvailable
            this.selectedComponentExist = this.detailProgressEvent?.event?.awpImplementation?.component
            this.selectedSubComponentExist = this.detailProgressEvent?.event?.awpImplementation?.subComponent
            this.selectedSubSubComponentExist = this.detailProgressEvent?.event?.awpImplementation?.subSubComponent
            this.codeSelected = this.detailProgressEvent?.event?.awpImplementation?.componentCode?.code
            this.kegiatanName = this.detailProgressEvent?.event?.name
            this.eventName = this.detailProgressEvent?.event?.name
            this.selectedEvent = this.detailProgressEvent?.event?.eventType
            this.existOnPok = this.detailProgressEvent?.pokEvent
            this.budgetAwp = this.detailProgressEvent?.event?.awpImplementation?.budgetAwp
            this.budgetPok = this.detailProgressEvent?.event?.budgetPok
            this.timeOtherInformation = this.detailProgressEvent?.eventOtherInformation
            this.location = this.detailProgressEvent?.location
            this.selectedContactPerson = this.detailProgressEvent?.event?.contactPersonResources
            this.selectedProvinceExist = this.detailProgressEvent?.province
            this.selectedCityExist = this.detailProgressEvent?.regency

            this.detailProgressEvent.event.convertStartDate = new Date(this.detailProgressEvent.event.startDate)
            this.detailProgressEvent.event.convertEndDate = new Date(this.detailProgressEvent.event.endDate)
            this.minEvent = new NgbDate(this.detailProgressEvent.event.convertStartDate.getFullYear(), this.detailProgressEvent.event.convertStartDate.getMonth() + 1, this.detailProgressEvent.event.convertStartDate.getDate())
            this.maxEvent = new NgbDate(this.detailProgressEvent.event.convertEndDate.getFullYear(), this.detailProgressEvent.event.convertEndDate.getMonth() + 1, this.detailProgressEvent.event.convertEndDate.getDate())

            this.detailProgressEvent.convertStartDate = new Date(this.detailProgressEvent.startDate)
            this.detailProgressEvent.convertEndDate = new Date(this.detailProgressEvent.endDate)
            this.fromDate = new NgbDate(this.detailProgressEvent.convertStartDate.getFullYear(), this.detailProgressEvent.convertStartDate.getMonth() + 1, this.detailProgressEvent.convertStartDate.getDate())
            this.toDate = new NgbDate(this.detailProgressEvent.convertEndDate.getFullYear(), this.detailProgressEvent.convertEndDate.getMonth() + 1, this.detailProgressEvent.convertEndDate.getDate())

            if (this.detailProgressEvent?.regency) {
              this.selectedProvinceAndCity.push({
                provinceId: this.selectedProvinceExist,
                regencyId: this.selectedCityExist
              })
            } else {
              this.selectedProvinceAndCity.push({
                provinceId: this.selectedProvinceExist,
                regencyId: null
              })
            }

            this.detailProgressEvent?.staff.forEach(dataStaff => {
              this.resourcesRequest.push(dataStaff.resources)
              this.resourcesRequestIds.push(dataStaff)
            })
            if (this.detailProgressEvent?.otherStaff.length > 0) {
              this.detailProgressEvent?.otherStaff.forEach(dataOtherStaff => {
                this.otherStaffIds.push(dataOtherStaff)
              })
            }
            this.eventOutput = this.detailProgressEvent?.eventOutput
            this.participantCount = this.detailProgressEvent?.participantCount
            this.participantMale = this.detailProgressEvent?.participantMale
            this.participantFemale = this.detailProgressEvent?.participantFemale
            this.participantOtherInformation = this.detailProgressEvent?.participantOtherInformation
            this.summaryEventOutput = this.detailProgressEvent?.summary
            this.agendaFile = this.detailProgressEvent?.attachmentFile
            this.raaFile = this.detailProgressEvent?.rraFile
            this.selectedModaActivity = this.detailProgressEvent?.activityMode
            this.tempatInformasiLainnya = this.detailProgressEvent?.locationOtherInformation
            this.listMeqrResourceSelected = JSON.parse(JSON.stringify(this.resourcesRequest))
            this.listMeqrResourceSelected.forEach(x => {
              if (x.user?.component?.code !== '-') {
                this.listMeqrPmuSelected.push(x)
              } else if (x.user?.component?.code === '-') {
                this.listMeqrPcuSelected.push(x)
              }
            })
            this.listProvince()
            this.setTimeRangeEvent()
            this.listComponent()
            this.listAllModa()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.serviceAwp.listModa().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */


  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  calculatePs(event, from) {
    if (from == 1) {
      if (this.participantMale < this.participantCount) {
        this.participantFemale = 0
        if (this.participantMale + this.participantFemale < this.participantCount) {
          this.participantFemale = this.participantCount - this.participantMale
        }
      } else {
        this.participantMale = this.participantCount
        this.participantFemale = 0
      }
    } else {
      if (this.participantFemale < this.participantCount) {
        this.participantMale = 0
        if (this.participantMale + this.participantFemale < this.participantCount) {
          this.participantMale = this.participantCount - this.participantFemale
        }
      } else {
        this.participantFemale = this.participantCount
        this.participantMale = 0
      }
    }
  }

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  setTimeRangeEvent() {
    let currentDate = new Date()
    // let newConvertDate = new NgbDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate())
    // if (currentDate >= this.dataDetailEvent.awpImplementation.convertStartDate) {
    //   this.minDate = {
    //     year: newConvertDate.year,
    //     month: newConvertDate.month,
    //     day: newConvertDate.day + 1
    //   };
    // } else {
    //   this.minDate = {
    //     year: this.minEvent.year,
    //     month: this.minEvent.month,
    //     day: this.minEvent.day
    //   };
    // }
    this.minDate = {
      year: this.minEvent.year,
      month: this.minEvent.month,
      day: this.minEvent.day
    };

    this.maxDate = {
      year: this.maxEvent.year,
      month: this.maxEvent.month,
      day: this.maxEvent.day
    };
  }

  /** * end date */

  goToDetailEvent() {
    this.router.navigate(['activity/detail-progress-event/' + this.detailEventId])
  }

  /** * List Province */
  listProvince() {
    let dataProvinceExist = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            this.listProvinceExist = resp.data.content
            // this.listProvinceAll = this.listProvinceAll.filter(x => x.id !== this.selectedProvinceExist.id)

            this.detailProgressEvent?.event?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
              let dataExist = this.listProvinceAll.find(x => dataProvince.province.id == x.id)
              if (dataExist) {
                dataProvinceExist.push(dataExist)
              }
            })
            this.listProvinceExist = dataProvinceExist
            this.listProvinceExistMaster = dataProvinceExist
            this.listCity()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedProvinceExist = null
    this.listProvinceExist = this.listProvinceExistMaster

    this.selectedProvinceExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      this.listProvinceExist = this.listProvinceExistMaster
    }
    console.log(this.listProvinceExistMaster)
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    console.log(searchStr)
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    if (searchStr == '') {
      this.listProvinceExist = this.listProvinceExistMaster
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      return this.listProvinceExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  /** * End List Province */

  /** * List City */

  listCity() {
    let cityExistFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.serviceAwp.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            this.listCityAllExist = resp.data.content
            let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
            if (checkProvinceExist.length > 0) {
              this.detailProgressEvent?.event?.awpImplementation?.provincesRegencies.forEach(dataProvince => {
                if (dataProvince.regencies.length > 0) {
                  dataProvince.regencies.forEach(dataCity => {
                    let cityExist = this.listCityAll.find(x => dataCity.regency?.id == x.id)
                    if (cityExist) {
                      cityExistFromApi.push(cityExist)
                    }
                  })
                }
              })
              this.listCityAllExist = cityExistFromApi
              this.listCityAllMasterExist = JSON.parse(JSON.stringify(this.listCityAllExist))
            } else {
              this.listCityAllExist = this.listCityAll
            }

            this.listCityAllMaster = resp.data.content
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist

    this.selectedCityExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    } else {
      this.listCityAllExist = this.listCityAll
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    }
  }

  searchFilterCity(e) {
    const searchStr = e.target.value
    let filter = this.listCityAll
    if (this.selectedCityExist) {
      filter = filter.filter(x => x.id !== this.selectedCityExist.id)
    } else {
      filter = this.listCityAll
    }

    if (searchStr == '') {
      let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
      if (checkProvinceExist.length > 0) {
        this.listCityAllExist = this.listCityAllMasterExist
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      } else {
        this.listCityAllExist = this.listCityAll
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      }
    } else {
      return this.listCityAllExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  addSelectedProvinceAndCity() {
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityExist
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.selectedProvinceExist = null
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist
    this.listProvinceExist = this.listProvinceExistMaster
    console.log(this.listProvinceExist)
    console.log(this.listProvinceExistMaster)
  }

  /** * Resources (Peserta) */

  getAllResources() {
    this.serviceResources.resourceAll().subscribe(
        resp => {
          if (resp.success) {
            this.listResources = resp.data.content
            this.listResourcesMaster = resp.data.content
            this.searchResources = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addResourcesSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.resourcesRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removeResources.forEach((check, index) => {
          if (check.id === data.id) {
            this.removeResources.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addResources.push(data)
        }
        this.resourcesRequest.push(data)
      }
    } else {
      this.resourcesRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.resourcesRequest.splice(index, 1)
          this.listMeqrResourceSelected.splice(index, 1)
        }
      })
      this.listMeqrPcuSelected = []
      this.listMeqrPmuSelected = []
      this.listMeqrResourceSelected.forEach(x => {
        if (x.user?.component?.code !== '-') {
          this.listMeqrPmuSelected.push(x)
        } else if (x.user?.component?.code === '-') {
          this.listMeqrPcuSelected.push(x)
        }
      })


      let checkAddExist = false

      this.addResources.forEach((check, index) => {
        if (check.id === data.id) {
          this.addResources.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      let x = this.resourcesRequest.filter(data => data.id === this.detailProgressEvent.event?.contactPersonResources.id)
      if (x.length == 0) {
        this.selectedContactPerson = null
      } else {
        this.selectedContactPerson = x[0]
      }

      if (!checkAddExist) {
        data.status = 0
        this.removeResources.push(data)
      }
    }
    this.listResources = this.listResourcesMaster

    this.resourcesRequest.forEach(dataProvince => {
      this.listResources = this.listResources.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })
    this.searchResources = this.listResources
    this.listContactPersonMaster = Object.assign([], this.resourcesRequest)
    this.listContactPerson = Object.assign([], this.resourcesRequest)
    let x = this.listContactPerson.filter(data => data.id === this.detailProgressEvent.event.contactPersonResources.id)
    if (x.length == 0) {
      this.selectedContactPerson = null
    } else {
      this.selectedContactPerson = x[0]
    }
  }

  searchFilterResources(e) {
    const searchStr = e.target.value
    this.listResources = this.searchResources.filter((product) => {
      return ((product.user.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.firstName !== null) || (product?.user?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.lastName !== null))
    });
  }

  addResourceSelectedMeqr() {
    this.listMeqrPmuSelected = []
    this.listMeqrPcuSelected = []
    this.closeModal()
    this.listMeqrResourceSelected = Object.assign([], this.resourcesRequest)
    this.listContactPersonMaster = Object.assign([], this.resourcesRequest)
    this.listContactPerson = Object.assign([], this.resourcesRequest)
    this.listMeqrResourceSelected.forEach(x => {
      if (x.user?.component?.code !== '-') {
        this.listMeqrPmuSelected.push(x)
      } else if (x.user?.component?.code === '-') {
        this.listMeqrPcuSelected.push(x)
      }
    })
    let x = this.listContactPerson.filter(data => data.id === this.detailProgressEvent.event.contactPersonResources.id)
    if (x.length == 0) {
      this.selectedContactPerson = null
    } else {
      this.selectedContactPerson = x[0]
    }
    this.listContactPerson = this.listContactPerson.filter(x => x.id !== this.selectedContactPerson.id)
  }
  /** * end Resources (Peserta) */

  /** Add New Unit*/

  selectedUnit(data) {
    this.selectedUnitExist = null
    this.listComponentStaff = this.componentExist

    this.selectedUnitExist = data
    this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedUnitExist.id)
    this.dataResources()
  }

  searchFilterUnit(e) {
    const searchStr = e.target.value
    let filter = this.componentExist
    if (this.selectedUnitExist) {
      filter = filter.filter(x => x.id !== this.selectedUnitExist.id)
    } else {
      filter = this.componentExist
    }

    this.listComponentStaff = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  listComponent() {
    this.serviceResources.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff = resp.data.content
            this.listComponentStaff.forEach(dataCode => {
              dataCode.code = 'Komponen ' + dataCode.code
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * List Project Officer */
  dataResources() {
    let dataListResource = []
    let selectedWorkUnitExist
    let params
    let selectedProvincePcu

    if (this.selectedWorkUnit === 'PMU') {
      selectedWorkUnitExist = [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.selectedUnitExist.id
        }
      ]
    } else if (this.selectedWorkUnit === 'PCU') {
      selectedProvincePcu = this.listProvinceAllStaff.filter(x => x.name === this.selectedProvinceStaff)
      selectedWorkUnitExist = [
        {
          field: "user.provinceId",
          dataType: "string",
          value: selectedProvincePcu[0].id
        }
      ]
    }

    this.loadingDataProjectOfficer = true
    params = {
      enablePage: false,
      paramIs: selectedWorkUnitExist,
      paramIn: [],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.serviceResources.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDataProjectOfficer = false
            this.listResources = resp.data.content
            this.listResourcesMaster = resp.data.content
            this.searchResources = resp.data.content
            const result = this.listResources.filter(({ id: id1 }) => !this.resourcesRequest.some(({ id: id2 }) => id2 === id1));
            this.listResources = result
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Add New Staff Outside */
  addNewStaffOutside() {
    this.otherStaff.push({
      name: this.nameNewStaff,
      position: this.namePosition,
      institution: this.nameInstitution,
    })
    this.closeModal()
    this.nameNewStaff = null
    this.namePosition = null
    this.nameInstitution = null
  }

  deleteNewStaffOutside(data) {
    this.otherStaff.forEach((check, index) => {
      if (check.name === data.name) {
        this.otherStaff.splice(index, 1)
      }
    })
  }
  deleteNewStaffOutsideIds(data) {
    this.otherStaffIds.forEach((check, index) => {
      if (check.name === data.name) {
        this.deletedOtherStaffIds.push(check.id)
        this.otherStaffIds.splice(index, 1)
      }
    })
  }
  /** End Add New Staff Outside */


  /** New Documentation Event */

  listEventDocumentType() {
    let tag = []
    this.serviceEvent.getListDocumentType().subscribe(
        resp => {
          if (resp.success) {
            this.listAllDocumentType = resp.data.content
            this.listAllDocumentType.forEach(x => {
              tag.push(x.name)
            })
          }
          this.typeDocumentation = tag
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addDocumentationEvent() {
    let selectedTypeDocument
    let startDate
    if (this.documentationEventDate) {
      startDate = this.documentationEventDate.year.toString() + '-' + this.documentationEventDate.month.toString().padStart(2, '0') + '-' + this.documentationEventDate.day.toString().padStart(2, '0')
    } else {
      startDate = null
    }

    selectedTypeDocument = this.listAllDocumentType.filter(x => x.name === this.selectedTypeDocumentation)

    this.documentationEvent.push({
      title: this.documentationEventName,
      date: startDate,
      place: this.documentationEventPlace,
      description: this.documentationEventDescription,
      fileIds: this.uploadFileDocumentationEvent,
      fileId: null,
      eventDocumentTypeId: selectedTypeDocument[0].id,
      eventDocumentTypeName: this.selectedTypeDocumentation
    })
    this.documentationEventName = ''
    this.documentationEventDate = null
    this.documentationEventPlace = ''
    this.documentationEventDescription = ''
    this.selectedTypeDocumentation = ''
    this.uploadFileDocumentationEvent = []
    this.closeModal()
  }

  /** Upload File Kegiatan */
  doUploadAttachmentFile() {
    this.loadingSaveProgressEvent = true
    if (this.uploadFileExcelAgenda.length > 0) {
      this.serviceResources.uploadAttachment(this.uploadFileExcelAgenda[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdAttachmentFile = resp.data[0].id
              this.doUploadAttachmentRaa()
            } else {
              this.loadingSaveProgressEvent = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            // this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.dataIdAttachmentFile = this.detailProgressEvent?.attachmentFile?.id
      this.doUploadAttachmentRaa()
    }
  }

  doUploadAttachmentRaa() {
    if (this.uploadFileRaa.length > 0) {
      this.serviceResources.uploadKegiatanRaa(this.uploadFileRaa[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdFileRaa = resp.data[0].id
              this.doUploadRab()
            } else {
              this.loadingSaveProgressEvent = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            // this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.dataIdFileRaa = this.detailProgressEvent?.rraFile?.id
      this.doUploadRab()
    }
  }

  doUploadRab() {
    if (this.documentationEvent.length > 0) {
      this.documentationEvent.forEach(data => {
        this.serviceResources.uploadDocumentation(data.fileIds[0]).subscribe(resp => {
              if (resp.success) {
                this.fileDocumentationId = resp.data[0].id
                data.fileId = this.fileDocumentationId
                this.documentationEventToApi.push({
                  title: data?.title,
                  date: data?.date,
                  place: data?.place,
                  description: data?.description,
                  fileId: this.fileDocumentationId,
                  eventDocumentTypeId: data?.eventDocumentTypeId
                })
                if (this.documentationEvent.length == this.documentationEventToApi.length) {
                  this.updateEventProgress()
                }
              } else {
                this.loadingSaveProgressEvent = false
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: resp.message,
                });
              }
            }, error => {
              // this.loadingSaveKegiatan = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: error.data[0]?.message,
              });
            }
        );
      })
    } else {
      this.updateEventProgress()
    }
  }

  removeDocumentationFile(data) {
    this.documentationEvent.forEach((check, index) => {
      if (check.title === data.title) {
        this.documentationEvent.splice(index, 1)
      }
    })
  }

  clearOldDocuments(data) {
    this.detailProgressEvent?.documents.forEach((check, index) => {
      if (check.id === data.id) {
        this.deletedDocumentIds.push(check.id)
        this.detailProgressEvent?.documents.splice(index, 1)
      }
    })
  }

  testing() {
    this.updateEventProgress()
  }

  updateEventProgress() {
    let startDate
    let endDate
    let listStaff = []
    let regencyId
    let otherStaffIds = []
    let staffIds = JSON.parse(JSON.stringify(this.listMeqrResourceSelected))
    let staffIdsToApi = []
    let staffIdsToApiFilter = []
    let dataStaffNewApi = []


    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')

    if (this.selectedProvinceAndCity[0]?.regencyId) {
      regencyId = this.selectedProvinceAndCity[0]?.regencyId.id
    } else {
      regencyId = null
    }

    // this.listMeqrResourceSelected.forEach(data => {
    //   dataStaffNewApi.push(data.id)
    // })

    if (this.addResources.length > 0) {
      this.addResources.forEach(x => {
        dataStaffNewApi.push(x.id)
      })
    }

    // this.listMeqrResourceSelected.forEach(x => {
    //   this.resourcesRequestIds.forEach(y => {
    //     if (x.id !== y.resources.id) {
    //       let existing = staffIdsToApi.filter(dataFilterExisting => dataFilterExisting === y.id)
    //       if (existing.length == 0) {
    //         staffIdsToApi.push(y.id)
    //       }
    //     }
    //   })
    // })

    if (this.removeResources.length > 0) {
      this.removeResources.forEach(x => {
        let dataFilter = this.resourcesRequestIds.filter(dataExisting => dataExisting.resources.id === x.id)
        if (dataFilter.length > 0) {
          staffIdsToApi.push(dataFilter[0].id)
        }
      })
    }


    let params = {
      eventId: this.detailProgressEvent?.eventId,
      eventOutput: this.eventOutput,
      participantMale: this.participantMale,
      participantFemale: this.participantFemale,
      participantCount: this.participantCount,
      participantOtherInformation: this.participantOtherInformation,
      startDate: startDate,
      endDate: endDate,
      provinceId: this.selectedProvinceAndCity[0].provinceId.id,
      regencyId: regencyId,
      location: this.location,
      attachmentFileId: this.dataIdAttachmentFile,
      newStaff: dataStaffNewApi,
      newOtherStaff: this.otherStaff,
      newDocuments: this.documentationEventToApi,
      budgetAvailable: this.existOnAnggaran,
      pokEvent: this.existOnPok,
      rraFileId: this.dataIdFileRaa,
      deletedStaffIds: staffIdsToApi,
      deletedOtherStaffIds: this.deletedOtherStaffIds,
      deletedDocumentIds: this.deletedDocumentIds,
      eventOtherInformation: this.timeOtherInformation,
      summary: this.summaryEventOutput,
      activityModeId: this.selectedModaActivity.id,
      locationOtherInformation: this.tempatInformasiLainnya
    }
    this.serviceEvent.updateEventProgress(this.detailEventId, params).subscribe(
        resp => {
          if (resp.success) {
            this.router.navigate(['activity/detail-progress-event/' + this.detailEventId])
            this.loadingSaveProgressEvent = false
          }
        }, error => {
          this.loadingSaveProgressEvent = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
