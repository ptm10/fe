import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProgressEventComponent } from './edit-progress-event.component';

describe('EditProgressEventComponent', () => {
  let component: EditProgressEventComponent;
  let fixture: ComponentFixture<EditProgressEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditProgressEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProgressEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
