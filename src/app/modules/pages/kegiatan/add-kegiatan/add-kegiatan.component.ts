import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {parse} from "date-fns";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {Event, EventDocumentType, Kegiatan, MangementType, PokCode, Province} from "../../../../core/models/awp.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {PDOModel} from "../../../../core/models/PDO.model";
import {ComponentService} from "../../../../core/services/Component/component.service";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {resourcesModel} from "../../../../core/models/resources.model";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {UsersModels} from "../../../../core/models/users.models";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {Monev2} from "../../../../core/models/monev";

@Component({
  selector: 'app-add-kegiatan',
  templateUrl: './add-kegiatan.component.html',
  styleUrls: ['./add-kegiatan.component.scss']
})
export class AddKegiatanComponent implements OnInit {

  detailIdKegiatan: string
  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  dataDetailKegiatan: Kegiatan
  dataIdRab: string = null
  firstNameLogged: string
  lastNameLogged: string

  public Description = ClassicEditor;
  public Purpose = ClassicEditor;
  public OutputEvent = ClassicEditor;
  public Asumtion = ClassicEditor;
  public RiskAsumtion = ClassicEditor;
  public OtherInfoAwp = ClassicEditor;
  public Institution = ClassicEditor;
  public riskMitigation = ClassicEditor;
  public participantTarget = ClassicEditor;

  // upload file
  uploadFileExcel: Array<File> = [];
  uploadFileExcelIds: Array<File> = [];

  // data update
  targetParticipant: string
  date: Date
  editNamaKegiatan: string
  editBudgetAwp: number
  editBudgetPok: number
  editTujuanKegiatan: string
  editDeskripsiKegiatan: string
  editWaktuInformasiLainnya: string
  editjumlahNasrasumber: number
  editAsalLembagaNarasumber: string
  editInformasiLainNarasumber: string
  editTotalPeserta: number
  editInformasiPeserta: string
  editOutputKegiatan: string
  editVolumeEvent: number
  selectedValueEventType: any
  existOnPok: boolean
  nomorKontrakLsp: string
  dataKontrakLsp: string
  picLsp: UsersModels
  asumsi: string
  descriptionRisk: string
  risk: string
  mitigasiResiko: string
  volumeKomponenPembiayaan: string
  hargaSatuanKomponenPembiayaan: string
  jumlahKomponenPembiayaan: string
  pesertaLaki: number
  pesertaPerempuan: number
  kemungkinanTerjadi: number;
  dampak: number;
  nsLakiLaki: number
  nsPerempuan: number
  gejalaResiko: string
  loadingSaveKegiatan = false
  otherInformationAwp: string = null
  tempatInformasiLainnya: string

  // POK
  listCodePok: PokCode[]
  listCodePokMaster: PokCode[]
  pokRequest = []
  addPok = []
  removePok = []
  searchPok = []
  pokExisting = []

  //componen
  selectedComponentExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]

  //Moda Kegiatan
  listModaType: MangementType[]
  listModaTypeMaster: MangementType[]
  selectedModaActivity: any

  // sub componen
  selectedSubComponentExist: ComponentModel
  selectedSubComponentExistMaster: ComponentModel
  selectedSubComponentExistEdit: ComponentModel

  //sub-sub componen
  selectedSubSubComponentExist: ComponentModel
  selectedSubSubComponentExistMaster: ComponentModel
  selectedSubSubComponentExistEdit: ComponentModel
  codeSelected: any = 'pilih'
  listCodeSelected = []

  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // event
  listEvent: Event[]
  listEventMaster: Event[]
  selectedEvent: Event
  selectedEventTypeIsPenyedia: boolean

  // LSP
  listLSP: UsersModels[]
  listLSPMaster: UsersModels[]
  selectedLsp: UsersModels

  // project officer
  listProjectOfficer: resourcesModel[]
  listProjectOfficerMaster: resourcesModel[]
  selectedProjectOfficer: resourcesModel
  loadingDataProjectOfficer = false

  // PDO
  listPdoAll: PDOModel[]
  listPdoAllMaster: PDOModel[]
  pdoRequest = []
  addPdo = []
  removePdo = []
  searchPdo = []
  pdoExisting = []

  // IRI
  listIriAll: PDOModel[]
  listIriAllMaster: PDOModel[]
  iriRequest = []
  addIri = []
  removeIri = []
  searchIri = []
  iriExisting = []

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []
  listCityFromApi: string[];
  selectedCityFromList = []

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province
  provinceAndRegenciesExist = []

  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  codeEvent: any

  // management type
  listManagementType: MangementType[]
  listManagementTypeMaster: MangementType[]
  selectedMangementTypeExist: MangementType

  // result chain code
  listChainCode: Monev2[]
  listChainCodeMaster: Monev2[]
  listChainCodeFilter: Monev2[]
  listChainCodeName: string[] = [];
  selectedChainCode: Monev2

  // indicator Success
  selectedIndicatorSuccess: string[] = []
  listIndicatorSuccessApi: string[];
  listSelectedChainCodeAndIndicator = []
  listChainCodeAndIndicatorExist = []

  // list Risk RIK
  listAllRisk: string[] = []
  selectedRisk: string
  dataListRisk: EventDocumentType[]
  otherRisk: string



  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private service: KegiatanService,
      private serviceComponent: ComponentService,
      private serviceAwp: AwpService,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private serviceAdministration: AdministrationService,
      private modalService: NgbModal,
      public roles: AuthfakeauthenticationService,
      private swalAlert: SwalAlert
  ) {
  }

  ngOnInit(): void {
    this.firstNameLogged = localStorage.getItem('fullName')
    this.lastNameLogged = localStorage.getItem('lastName')
    this.detailIdKegiatan = this.activatedRoute.snapshot.paramMap.get('id')
    this.breadCrumbItems = [{ label: 'Kegiatan' }, { label: 'Detail Kegiatan'}, { label: 'Rencana Implementasi Kegiatan', active: true }]
    this.getDetailKegiatan()
    this.managementType()
    this.getListHolidayManagement()
    this.listProvince()
    this.listAllModa()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  // new tag
  CreateNew(Position){
    return Position
  }

  setTimeRangeEvent() {
    const current = new Date();
    this.minDate = {
      year: this.date.getFullYear(),
      month: 1,
      day: 1
    };
    this.maxDate = {
      year: this.date.getFullYear(),
      month: 12,
      day: 31
    };
  }

  goToListKegiatan() {
    if (this.dataDetailKegiatan.status == 0) {
      this.router.navigate(['implement/detail-kegiatan/' + this.detailIdKegiatan])
    } else {
      this.router.navigate(['implement/detail-kegiatan-perencanaan/' + this.detailIdKegiatan])
    }
  }

  /** * POK Code */
  listPokEvent() {
    this.startLoading()
    this.service.listCodePok().subscribe(
        resp => {
          if (resp.success) {
            this.listCodePok = resp.data.content
            this.listCodePokMaster = resp.data.content
            this.searchPok = resp.data.content
            if (this.pokExisting.length > 0) {
              this.pokExisting.forEach(dataPokExist => {
                this.listCodePok = this.listCodePok.filter(x => x.id !== dataPokExist?.pok?.id)
                this.searchPok = this.listCodePok.filter(x => x.id !== dataPokExist?.pok?.id)
              })
            }
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addPokSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pokRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePok.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePok.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPok.push(data)
        }
        this.pokRequest.push(data)
      }
    } else {
      this.pokRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pokRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPok.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPok.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePok.push(data)
      }
    }
    this.listCodePok = this.listCodePokMaster

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.id)
      })
    }

    this.pokRequest.forEach(dataProvince => {
      this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPok = this.listCodePok
  }

  searchFilterPok(e) {
    const searchStr = e.target.value
    this.listCodePok = this.searchPok.filter((product) => {
      return (product.code.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.title.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePokExisting(data) {
    this.pokExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.pokExisting.splice(index, 1)
      }
    })
    this.listCodePok = this.listCodePokMaster
    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.pok?.id)
      })
      this.pokRequest.forEach(dataPok => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataPok.id)
      })
    } else {
      this.pokRequest.forEach(dataProvince => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPok = this.listCodePok
  }


  testing() {
    console.log(this.pokExisting)
    console.log(this.pokRequest)
  }
  /** * End POK Code */



  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.serviceAwp.listModa().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */


  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  changeEventType() {
    const x = this.listManagementType.find(data => data.id === this.selectedValueEventType.id)
    this.selectedEventTypeIsPenyedia = x.name === 'Kontrak';
  }

  getDetailKegiatan() {
    this.startLoading()
    this.service.detailKegiatan(this.detailIdKegiatan).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataDetailKegiatan = resp.data
            this.editNamaKegiatan = this.dataDetailKegiatan.name
            this.editBudgetAwp = this.dataDetailKegiatan.budgetAwp
            this.editTujuanKegiatan = this.dataDetailKegiatan.purpose
            this.editDeskripsiKegiatan = this.dataDetailKegiatan.description
            this.selectedValueEventType = this.dataDetailKegiatan.eventManagementType
            this.editjumlahNasrasumber = this.dataDetailKegiatan.nsCount
            this.editAsalLembagaNarasumber = this.dataDetailKegiatan.nsInstitution
            this.editInformasiLainNarasumber = this.dataDetailKegiatan.nsOtherInformation
            this.editTotalPeserta = this.dataDetailKegiatan.participantCount
            this.editInformasiPeserta = this.dataDetailKegiatan.participantOtherInformation
            this.editOutputKegiatan = this.dataDetailKegiatan.eventOutput
            this.editVolumeEvent = this.dataDetailKegiatan?.eventVolume
            this.nomorKontrakLsp = this.dataDetailKegiatan?.lspContractNumber
            this.dataKontrakLsp = this.dataDetailKegiatan?.lspContractData
            // this.picLsp = this.dataDetailKegiatan?.lspPic
            this.date = parse(this.dataDetailKegiatan.awp.year.toString(), 'yyyy', new Date());
            this.selectedComponentExist = this.dataDetailKegiatan?.component
            this.selectedProjectOfficer = this.dataDetailKegiatan?.projectOfficerResources
            this.dataDetailKegiatan.convertStartDate = new Date(this.dataDetailKegiatan.startDate)
            this.dataDetailKegiatan.convertEndDate = new Date(this.dataDetailKegiatan.endDate)
            this.existOnPok = this.dataDetailKegiatan?.pokEvent
            this.fromDate = new NgbDate(this.dataDetailKegiatan.convertStartDate.getFullYear(), this.dataDetailKegiatan.convertStartDate.getMonth() + 1, this.dataDetailKegiatan.convertStartDate.getDate())
            this.toDate = new NgbDate(this.dataDetailKegiatan.convertEndDate.getFullYear(), this.dataDetailKegiatan.convertEndDate.getMonth() + 1, this.dataDetailKegiatan.convertEndDate.getDate())
            this.selectedEvent = this.dataDetailKegiatan?.eventType
            this.asumsi = this.dataDetailKegiatan?.assumption
            this.descriptionRisk = this.dataDetailKegiatan?.descriptionRisk
            this.mitigasiResiko = this.dataDetailKegiatan?.mitigationRisk
            this.editBudgetPok = this.dataDetailKegiatan?.budgetPok
            this.pesertaLaki = this.dataDetailKegiatan?.tpMale
            this.nsLakiLaki = this.dataDetailKegiatan?.nsMale
            this.nsPerempuan = this.dataDetailKegiatan?.nsFemale
            this.pesertaLaki = this.dataDetailKegiatan?.participantMale
            this.pesertaPerempuan = this.dataDetailKegiatan?.participantFemale
            this.editWaktuInformasiLainnya = this.dataDetailKegiatan?.eventOtherInformation
            this.selectedProjectOfficer = this.dataDetailKegiatan?.projectOfficerResources
            this.dampak = this.dataDetailKegiatan?.impactRisk
            this.kemungkinanTerjadi = this.dataDetailKegiatan?.potentialRisk
            this.gejalaResiko = this.dataDetailKegiatan?.symptomsRisk
            this.selectedLsp = this.dataDetailKegiatan?.lspPicUser
            if (this.dataDetailKegiatan.eventManagementType.name === 'Kontrak') {
              this.selectedEventTypeIsPenyedia = true
            }
            this.dataDetailKegiatan.pdo.forEach(x => {
              this.pdoExisting.push(x)
            })
            this.dataDetailKegiatan.iri.forEach(x => {
              this.iriExisting.push(x)
            })
            this.dataDetailKegiatan.provincesRegencies.forEach(x => {
              this.provinceAndRegenciesExist.push(x)
            })
            if (this.dataDetailKegiatan?.pok.length > 0) {
              this.dataDetailKegiatan?.pok.forEach(x => {
                this.pokExisting.push(x)
              })
            }
            if (this.dataDetailKegiatan?.nameOtherInformation) {
              this.otherInformationAwp = this.dataDetailKegiatan?.nameOtherInformation
            } else {
              this.otherInformationAwp = this.dataDetailKegiatan?.awp?.nameOtherInformation
            }
            if (this.dataDetailKegiatan?.locationOtherInformation) {
              this.tempatInformasiLainnya = this.dataDetailKegiatan?.locationOtherInformation
            } else {
              this.tempatInformasiLainnya = this.dataDetailKegiatan?.awp?.locationOtherInformation
            }
            if (this.dataDetailKegiatan?.participantTarget) {
              this.targetParticipant = this.dataDetailKegiatan?.participantTarget
            } else {
              this.targetParticipant = this.dataDetailKegiatan?.awp?.participantTarget
            }
            if (this.dataDetailKegiatan?.activityMode) {
              this.selectedModaActivity = this.dataDetailKegiatan?.activityMode
            } else {
              this.selectedModaActivity = this.dataDetailKegiatan?.awp?.activityMode
            }

            if (this.dataDetailKegiatan?.monev.length > 0) {
              this.dataDetailKegiatan?.monev.forEach(x => {
                this.listChainCodeAndIndicatorExist.push(x)
              })
            }
            if (this.dataDetailKegiatan?.risk) {
              this.selectedRisk = this.dataDetailKegiatan?.risk
            }
            this.listResultChainCode()
            this.dataEvent()
            this.getListComponent()
            this.dataPDO()
            this.dataIri()
            this.dataProjectOfficer()
            this.setTimeRangeEvent()
            this.dataListLsp()
            this.listPokEvent()
            this.getListRisk()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  calculateNs(event, from) {
    if (this.nsLakiLaki || this.nsPerempuan) {
      if (from == 1) {
        if (this.nsLakiLaki < this.editjumlahNasrasumber) {
          this.nsPerempuan = 0
          if (this.nsLakiLaki + this.nsPerempuan < this.editjumlahNasrasumber) {
            this.nsPerempuan = this.editjumlahNasrasumber - this.nsLakiLaki
          }
        } else {
          this.nsLakiLaki = this.editjumlahNasrasumber
          this.nsPerempuan = 0
        }
      } else {
        if (this.nsPerempuan < this.editjumlahNasrasumber) {
          this.nsLakiLaki = 0
          if (this.nsLakiLaki + this.nsPerempuan < this.editjumlahNasrasumber) {
            this.nsLakiLaki = this.editjumlahNasrasumber - this.nsPerempuan
          }
        } else {
          this.nsPerempuan = this.editjumlahNasrasumber
          this.nsLakiLaki = 0
        }
      }
    }
  }

  calculatePs(event, from) {
    if (this.pesertaPerempuan || this.pesertaLaki) {
      if (from == 1) {
        if (this.pesertaLaki < this.editTotalPeserta) {
          this.pesertaPerempuan = 0
          if (this.pesertaLaki + this.pesertaPerempuan < this.editTotalPeserta) {
            this.pesertaPerempuan = this.editTotalPeserta - this.pesertaLaki
          }
        } else {
          this.pesertaLaki = this.editTotalPeserta
          this.pesertaPerempuan = 0
        }
      } else {
        if (this.pesertaPerempuan < this.editTotalPeserta) {
          this.pesertaLaki = 0
          if (this.pesertaLaki + this.pesertaPerempuan < this.editTotalPeserta) {
            this.pesertaLaki = this.editTotalPeserta - this.pesertaPerempuan
          }
        } else {
          this.pesertaPerempuan = this.editTotalPeserta
          this.pesertaLaki = 0
        }
      }
    }
  }

  /** * list Risk */

  getListRisk() {
    let tag = []
    this.service.listRiskInRik().subscribe(
        resp => {
          if (resp.success) {
            this.dataListRisk = resp.data.content
            this.dataListRisk.forEach(x => {
              tag.push(x.name)
            })
            this.listAllRisk = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End list Risk */


  /** * list component & selected component */
  getListComponent() {
    this.startLoading()
    this.serviceComponent.listComponent(this.dataDetailKegiatan?.awp?.year).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.component = resp.data
            this.componentMaster = resp.data
            let componentMasterEdit =JSON.parse(JSON.stringify(this.componentMaster))
            this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)

            let dataSubComponent = componentMasterEdit.find(dataFilter => dataFilter.id == this.selectedComponentExist.id)
            this.selectedSubComponentExistMaster = dataSubComponent
            this.selectedSubComponentExistEdit = Object.assign({}, dataSubComponent)
            if (this.dataDetailKegiatan?.subComponent) {
              this.selectedSubComponentExist = dataSubComponent.subComponent.find(data => data.id == this.dataDetailKegiatan?.subComponent.id)
              this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
            }

            this.selectedSubSubComponentExistMaster = this.selectedSubComponentExist
            this.selectedSubSubComponentExistEdit = Object.assign({}, this.selectedSubComponentExist)
            if (this.dataDetailKegiatan?.subSubComponent) {
              this.selectedSubSubComponentExist = this.selectedSubComponentExist.subComponent.find(data => data.id == this.dataDetailKegiatan?.subSubComponent.id)
              this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubSubComponentExist.id)
            }
            this.codeSelectedFromComponent()
            this.dataProjectOfficer()
          } else {
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null
    this.component = this.componentMaster

    this.selectedComponentExist = data
    this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)

    this.selectedSubComponentExistMaster = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExistEdit = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
    this.dataProjectOfficer()
  }

  searchFilterComponent(e) {
    const searchStr = e.target.value
    let filter = this.componentMaster
    if (this.selectedComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
    } else {
      filter = this.componentMaster
    }

    this.component = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedComponent() {
    this.codeSelected = null
    this.selectedProjectOfficer = null
    this.editNamaKegiatan = null
    this.component = this.componentMaster
    this.selectedComponentExist = null

    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null

    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.codeSelectedFromComponent()
  }

  // sub componen
  selectedSubComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent

    this.selectedSubComponentExist = data
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
    this.selectedSubSubComponentExistMaster = Object.assign({}, this.selectedSubComponentExist)
    this.selectedSubSubComponentExistEdit = Object.assign({}, this.selectedSubComponentExist)
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
  }

  searchFilterSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubComponentExistEdit.subComponent
    if (this.selectedSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubComponentExist.id)
    } else {
      filter = this.selectedSubComponentExistEdit.subComponent
    }

    this.selectedSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedSubComponent() {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
  }

  // sub sub componen
  selectedSubSubComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent

    this.selectedSubSubComponentExist = data
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubSubComponentExist.id)
    this.codeSelectedFromComponent()
  }

  searchFilterSubSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubSubComponentExistEdit.subComponent
    if (this.selectedSubSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubSubComponentExist.id)
    } else {
      filter = this.selectedSubSubComponentExistEdit.subComponent
    }

    this.selectedSubSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedSubSubComponent() {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent
    this.codeSelectedFromComponent()
  }

  codeSelectedFromComponent() {
    let selectedCodeComponent
    let filterComponent
    this.codeSelected = 'pilih'
    this.listCodeSelected = []
    if (this.selectedSubSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubSubComponentExist))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      const x = this.listCodeSelected.filter(y => y.code === this.dataDetailKegiatan?.componentCode.code)
      if (x.length > 0) {
        this.codeSelected = this.dataDetailKegiatan?.componentCode?.code
        // this.editNamaKegiatan = this.dataDetailKegiatan?.componentCode?.description
      } else {
        this.codeSelected = 'pilih'
      }
    } else if (this.selectedSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      const x = this.listCodeSelected.filter(y => y.code === this.dataDetailKegiatan?.componentCode.code)
      if (x.length > 0) {
        this.codeSelected = this.dataDetailKegiatan?.componentCode.code.toString()
        // this.editNamaKegiatan = this.dataDetailKegiatan?.componentCode?.description
      } else {
        this.codeSelected = 'pilih'
      }
    } else if (this.selectedComponentExist) {
      filterComponent = this.componentMaster.find(data => data.id == this.selectedComponentExist.id)
      selectedCodeComponent = JSON.parse(JSON.stringify(filterComponent))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      const x = this.listCodeSelected.filter(y => y.code === this.dataDetailKegiatan?.componentCode.code)
      if (x.length > 0) {
        this.codeSelected = this.dataDetailKegiatan?.componentCode.code
        // this.editNamaKegiatan = this.dataDetailKegiatan?.componentCode?.description
      } else {
        this.codeSelected = 'pilih'
      }
    }
    console.log(this.codeSelected)
  }

  changeValueSelected() {
    if (this.codeSelected === 'pilih') {
      // this.editNamaKegiatan = ''
    } else {
      let x = this.listCodeSelected.find(y => y.code === this.codeSelected)
      // this.editNamaKegiatan = x.description
    }
  }

  /** * Management Type */

  managementType() {
    this.startLoading()
    this.service.eventManagementType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listManagementType = resp.data.content
            this.listManagementTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedManagementType(data) {
    this.selectedMangementTypeExist = null
    this.listManagementType = this.listManagementTypeMaster

    this.selectedMangementTypeExist = data
    this.listManagementType = this.listManagementType.filter(dataFilter => dataFilter.id != this.selectedMangementTypeExist.id)
  }


  /** * List Province */

  listProvince() {
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            // this.searchProvince = this.listProvinceAll
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster

    this.selectedProvinceExist = data
    this.listProvinceAll = this.listProvinceAll.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    this.listProvinceAll = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List City */

  listCity() {
    let cityNameFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.serviceAwp.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            // this.listCityAll = this.listCityAll.filter(x => this.selectedProvinceAndCity.filter(y => x.id !== y.regencyId.id))
            this.listCityAll.forEach(x => {
              cityNameFromApi.push(x.name)
            })

            if (this.selectedProvinceAndCity.length > 0) {
              this.selectedProvinceAndCity.forEach(x => {
                if (x.regencyId.length > 0) {
                  x.regencyId.forEach(y => {
                    cityNameFromApi = cityNameFromApi.filter(exist => exist !== y)
                  })
                }
              })
            }

            if (this.provinceAndRegenciesExist.length > 0) {
              this.provinceAndRegenciesExist.forEach(x => {
                if (x.regencies.length > 0) {
                  x.regencies.forEach(dataRegencie => {
                    cityNameFromApi = cityNameFromApi.filter(y => dataRegencie.regency.name !== y)
                  })
                }
              })
            }
            this.listCityFromApi = cityNameFromApi

            // if (this.selectedProvinceAndCity.length > 0) {
            //   this.selectedProvinceAndCity.forEach(x => {
            //     if (x.regencyId != null) {
            //       this.listCityAll = this.listCityAll.filter(y => y.id !== x.regencyId.id)
            //     }
            //   })
            // }
            this.listCityAllMaster = resp.data.content
            // this.searchProvince = this.listProvinceAll
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAll = this.listCityAllMaster

    this.selectedCityExist = data
    this.listCityAll = this.listCityAll.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    if (this.selectedProvinceAndCity.length > 0) {
      this.selectedProvinceAndCity.forEach(x => {
        if (x.regencyId != null) {
          this.listCityAll = this.listCityAll.filter(y => y.id !== x.regencyId.id)
        }
      })
    }
  }

  addSelectedProvinceAndCity() {
    this.selectedCityFromList.forEach(x => {
      let y = this.listCityAll.find(dataSelected => dataSelected.name === x)
      if (y) {
        this.dataAllCitySelected.push(y)
      }
    })
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityFromList
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.listCityFromApi = []
  }

  removeProvinceRegenciesExist(data) {
    this.provinceAndRegenciesExist.forEach((check, index ) => {
      if (check.id === data.id) {
        this.provinceAndRegenciesExist.splice(index, 1)
      }
    })
    this.listCityFromApi = []
  }

  /** * List Target Result Chain Activity */
  listResultChainCode() {
    let tag
    let params = {
      enablePage: false,
      page: 0,
      paramLike: [],
      paramIs: [
        {
          field: "year",
          dataType: "int",
          value: this.dataDetailKegiatan?.awp?.year
        },
        {
          field: "monevTypeId",
          dataType: "string",
          value: "afedb8dc-d1ee-11ec-88cb-0ba398ed0298"
        }
      ],
      paramIn: [],
      paramNotIn: [],
      sort: [
        {
          field: "code",
          direction: "asc"
        }
      ]
    }
    this.service.listResultChainAll(params).subscribe(
        resp => {
          this.startLoading()
          if (resp.success) {
            this.stopLoading()
            this.listChainCode = resp.data.content
            this.listChainCodeMaster = resp.data.content

            this.listChainCode = this.listChainCode.filter(x => x.code.charAt(2) === this.dataDetailKegiatan?.component?.code)
            this.listChainCodeMaster = this.listChainCodeMaster.filter(x => x.code.charAt(2) === this.dataDetailKegiatan?.component?.code)

            this.listChainCode.forEach(x => {
              x.successIndicator.forEach(y => {
                y.notApplicableAllYear = false
                if (this.dataDetailKegiatan?.awp?.year === 2020) {
                  y.notApplicableAllYear = y.notApplicable2020
                } else if (this.dataDetailKegiatan?.awp?.year === 2021) {
                  y.notApplicableAllYear = y.notApplicable2021
                } else if (this.dataDetailKegiatan?.awp?.year === 2022) {
                  y.notApplicableAllYear = y.notApplicable2022
                } else if (this.dataDetailKegiatan?.awp?.year === 2023) {
                  y.notApplicableAllYear = y.notApplicable2023
                } else if (this.dataDetailKegiatan?.awp?.year === 2024) {
                  y.notApplicableAllYear = y.notApplicable2024
                }
              })
            })

            this.listChainCodeMaster.forEach(x => {
              x.successIndicator.forEach(y => {
                y.notApplicableAllYear = false
                if (this.dataDetailKegiatan?.awp?.year === 2020) {
                  y.notApplicableAllYear = y.notApplicable2020
                } else if (this.dataDetailKegiatan?.awp?.year === 2021) {
                  y.notApplicableAllYear = y.notApplicable2021
                } else if (this.dataDetailKegiatan?.awp?.year === 2022) {
                  y.notApplicableAllYear = y.notApplicable2022
                } else if (this.dataDetailKegiatan?.awp?.year === 2023) {
                  y.notApplicableAllYear = y.notApplicable2023
                } else if (this.dataDetailKegiatan?.awp?.year === 2024) {
                  y.notApplicableAllYear = y.notApplicable2024
                }
              })
            })

            if (this.listChainCodeAndIndicatorExist.length > 0) {
              this.listChainCodeAndIndicatorExist.forEach(y => {
                this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
              })
            }
            this.listChainCodeFilter = this.listChainCode
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedChainCodeFromList(data) {
    let tag = []
    this.selectedChainCode = null
    this.listIndicatorSuccessApi = []
    this.selectedIndicatorSuccess = []
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }

    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }

    this.selectedChainCode = data
    this.listChainCode = this.listChainCode.filter(dataFilter => dataFilter.id != this.selectedChainCode.id)
    // this.listIndicatorSuccess()

    this.selectedChainCode?.successIndicator?.forEach(y => {
      if (this.dataDetailKegiatan?.awp?.year === 2020) {
        y.notApplicableAllYear = y.notApplicable2020
      } else if (this.dataDetailKegiatan?.awp?.year === 2021) {
        y.notApplicableAllYear = y.notApplicable2021
      } else if (this.dataDetailKegiatan?.awp?.year === 2022) {
        y.notApplicableAllYear = y.notApplicable2022
      } else if (this.dataDetailKegiatan?.awp?.year === 2023) {
        y.notApplicableAllYear = y.notApplicable2023
      } else if (this.dataDetailKegiatan?.awp?.year === 2024) {
        y.notApplicableAllYear = y.notApplicable2024
      }
    })
    
    this.selectedChainCode.successIndicator.forEach(x => {
      if (!x.notApplicableAllYear) {
        tag.push(x.code + ' - ' + x.name)
      }
    })
    this.listIndicatorSuccessApi = tag


    this.listChainCodeFilter = this.listChainCode
  }

  searchFilterChainCode(e) {
    const searchStr = e.target.value
    let filter = this.listChainCodeFilter
    if (this.selectedChainCode) {
      filter = filter.filter(x => x.id !== this.selectedChainCode.id)
    } else {
      filter = this.listChainCodeFilter
    }

    this.listChainCode = filter.filter((product) => {
      return ((product?.result?.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product?.resultChainCode?.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * End Of List Target Result Chain */

  /** * List Indicator Success */

  addSelectedResultChainAndIndicatorSuccess() {
    let selectedIndicator = []
    let dataSelectedIndicator = []
    let selectedIndicatorToApi = []

    console.log(this.selectedIndicatorSuccess)

    this.selectedIndicatorSuccess.forEach(x => {
      const selectedIndicatorSuccessExist = x.split(" ", 1)
      selectedIndicator.push(selectedIndicatorSuccessExist[0])
    })

    console.log(this.selectedIndicatorSuccess)
    console.log(selectedIndicator)

    selectedIndicator.forEach(x => {
      dataSelectedIndicator = this.selectedChainCode?.successIndicator.filter(y => x === y.code)
      if (dataSelectedIndicator.length > 0) {
        selectedIndicatorToApi.push(dataSelectedIndicator[0])
      }
    })
    this.listSelectedChainCodeAndIndicator.push({
      resultChainId: this.selectedChainCode,
      successIndicatorIds: selectedIndicatorToApi
    })
    this.closeModal()
    this.selectedChainCode = null
    this.selectedIndicatorSuccess = []
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }

    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  removeSelectedChainCodeAndIndicator(data) {
    this.listSelectedChainCodeAndIndicator.forEach((check, index ) => {
      if (check.resultChainId.id == data.resultChainId.id) {
        this.listSelectedChainCodeAndIndicator.splice(index, 1)
      }
    })
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }
    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  removeSelectedChainCodeAndIndicatorExist(data) {
    this.listChainCodeAndIndicatorExist.forEach((check, index ) => {
      if (check.id == data.id) {
        this.listChainCodeAndIndicatorExist.splice(index, 1)
      }
    })
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }
    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  /** * End Of List Indicator Success */


  /** * List Project Officer */
  dataProjectOfficer() {
    this.loadingDataProjectOfficer = true
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.selectedComponentExist.id
        }
      ],
      paramIn: [
        {
          field: "roleId",
          dataType: "string",
          value: [
            "f51e35a2-91f8-11ec-8ad1-ef8b05e5801f"
          ]
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.service.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDataProjectOfficer = false
            this.listProjectOfficer = resp.data.content
            this.listProjectOfficerMaster = resp.data.content
            if (this.listProjectOfficer.length > 0) {
              this.listProjectOfficer = this.listProjectOfficer.filter(dataFilter => dataFilter.id != this.selectedProjectOfficer.id)
            }
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectProjectOfficer(data) {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster

    this.selectedProjectOfficer = data
    this.listProjectOfficer = this.listProjectOfficer.filter(dataFilter => dataFilter.id != this.selectedProjectOfficer.id)
  }

  removeProjectOfficer() {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster
  }

  searchFilterProjectOfficer(e) {
    const searchStr = e.target.value
    let filter = this.listProjectOfficerMaster
    if (this.selectedProjectOfficer) {
      filter = filter.filter(x => x.id !== this.selectedProjectOfficer.id)
    } else {
      filter = this.listProjectOfficerMaster
    }

    this.listProjectOfficer = filter.filter((product) => {
      return ((product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  /** * end date */

  /** * list holidays */
  getListHolidayManagement() {
    this.listHolidays = []
    this.serviceAdministration.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * end list holidays */

  /** * List Event */
  dataEvent() {
    this.startLoading()
    this.serviceAwp.eventType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEvent = resp.data.content
            this.listEventMaster = resp.data.content
            this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent.id)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedEventType(data) {
    this.selectedEvent = null
    this.listEvent = this.listEventMaster

    this.selectedEvent = data
    this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent.id)
  }

  searchFilterEvent(e) {
    const searchStr = e.target.value
    let filter = this.listEventMaster
    if (this.selectedEvent) {
      filter = filter.filter(x => x.id !== this.selectedEvent.id)
    } else {
      filter = this.listEventMaster
    }

    this.listEvent = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List LSP */
  dataListLsp() {
    const params = {
      enablePage: false,
      page: 0,
      paramIs: [
        {
          field: "componentId",
          dataType: "string",
          value: this.dataDetailKegiatan.component.id
        }
      ],
      sort: [
        {
          field: "firstName",
          direction: "asc"
        },
        {
          field: "lastName",
          direction: "asc"
        }
      ]
    }
    this.startLoading()
    this.service.listLsp(params).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listLSP = resp.data.content
            this.listLSPMaster = resp.data.content
            this.listLSP = this.listLSP.filter(x => x.id !== this.selectedLsp?.id)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedListLsp(data) {
    this.selectedLsp = null
    this.listLSP = this.listLSPMaster

    this.selectedLsp = data
    this.listLSP = this.listLSP.filter(dataFilter => dataFilter.id != this.selectedLsp?.id)
  }

  searchFilterLsp(e) {
    const searchStr = e.target.value
    let filter = this.listLSPMaster
    if (this.selectedLsp) {
      filter = filter.filter(x => x.id !== this.selectedLsp.id)
    } else {
      filter = this.listLSPMaster
    }

    this.listLSP = filter.filter((product) => {
      return ((product?.firstName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeLspSelected() {
    this.selectedLsp = null
    this.listLSP = this.listLSPMaster
  }

  /** * List PDO */

  dataPDO() {
    this.serviceAwp.pdo().subscribe(
        resp => {
          if (resp.success) {
            this.listPdoAll = resp.data.content
            this.listPdoAllMaster = resp.data.content
            this.searchPdo = resp.data.content
            if (this.pdoExisting.length > 0) {
              this.pdoExisting.forEach(dataPdoExist => {
                this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
                this.searchPdo = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
              })
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addPdoSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pdoRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePdo.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePdo.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPdo.push(data)
        }
        this.pdoRequest.push(data)
      }
    } else {
      this.pdoRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pdoRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPdo.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPdo.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePdo.push(data)
      }
    }
    this.listPdoAll = this.listPdoAllMaster

    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(dataPdoExist => {
        this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
      })
    }

    this.pdoRequest.forEach(dataProvince => {
      this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPdo = this.listPdoAll
  }

  searchFilterPdo(e) {
    const searchStr = e.target.value
    this.listPdoAll = this.searchPdo.filter((product) => {
      return (product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.description.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePdoExisting(data) {
    this.pdoExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.pdoExisting.splice(index, 1)
      }
    })
    this.listPdoAll = this.listPdoAllMaster
    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(dataPdoExist => {
        this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
      })
      this.pdoRequest.forEach(dataProvince => {
        this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    } else {
      this.pdoRequest.forEach(dataProvince => {
        this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPdo = this.listPdoAll
  }

  /** * List IRI */
  dataIri() {
    this.startLoading()
    this.serviceAwp.iri().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listIriAll = resp.data.content
            this.listIriAllMaster = resp.data.content
            this.searchIri = resp.data.content
            if (this.iriExisting.length > 0) {
              this.iriExisting.forEach(dataIriExist => {
                this.listIriAll = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
                this.searchIri = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
              })
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addIriSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.iriRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removeIri.forEach((check, index) => {
          if (check.id === data.id) {
            this.removeIri.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addIri.push(data)
        }
        this.iriRequest.push(data)
      }
    } else {
      this.iriRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.iriRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addIri.forEach((check, index) => {
        if (check.id === data.id) {
          this.addIri.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removeIri.push(data)
      }
    }
    this.listIriAll = this.listIriAllMaster

    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(dataIriExist => {
        this.listIriAll = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
      })
    }

    this.iriRequest.forEach(dataProvince => {
      this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })
    this.searchIri = this.listIriAll
  }

  searchFilterIri(e) {
    const searchStr = e.target.value
    this.listIriAll = this.searchIri.filter((product) => {
      return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
    });
  }

  removeIriExisting(data) {
    this.iriExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.iriExisting.splice(index, 1)
      }
    })
    this.listIriAll = this.listIriAllMaster
    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(dataPdoExist => {
        this.listIriAll = this.listIriAll.filter(x => x.id !== dataPdoExist?.iri?.id)
      })
      this.iriRequest.forEach(dataProvince => {
        this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    } else {
      this.iriRequest.forEach(dataProvince => {
        this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchIri = this.listIriAll
  }

  /** Upload Foto Profile */
  doUploadRab() {
    this.loadingSaveKegiatan = true
    if (this.uploadFileExcel.length > 0) {
      this.service.upload(this.uploadFileExcel[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdRab = resp.data[0].id
              this.createNewImplement()
            } else {
              this.loadingSaveKegiatan = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
        this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      if (!this.dataIdRab) {
        if (this.dataDetailKegiatan?.rabFile?.id && (this.dataDetailKegiatan?.awp?.rabFile || !this.dataDetailKegiatan?.awp?.rabFile)) {
          this.dataIdRab = this.dataDetailKegiatan?.rabFile?.id
        } else if (!this.dataDetailKegiatan?.rabFile?.id && this.dataDetailKegiatan?.awp?.rabFile) {
          this.dataIdRab = this.dataDetailKegiatan?.awp?.rabFile?.id
        } else if (!this.dataDetailKegiatan?.rabFile?.id && !this.dataDetailKegiatan?.awp?.rabFile) {
          this.dataIdRab = null
        }
      }
      this.createNewImplement()
    }
  }

  disabledForm1() {
    if (this.roles?.hasAuthority('Consultan') || this.roles?.hasAuthority('Coordinator')) {
      return !this.selectedComponentExist || !this.editNamaKegiatan || this.editBudgetAwp == null || this.editBudgetPok == null || (this.pokRequest.length == 0 && this.pokExisting.length == 0)
    } else {
      return !this.selectedComponentExist || !this.editNamaKegiatan || this.editBudgetAwp == null || !this.selectedProjectOfficer || this.editBudgetPok == null || (this.pokRequest.length == 0 && this.pokExisting.length == 0)
    }
  }

  createNewImplement() {
    let selectedSubComponentId
    let selectedSubSubComponentId
    let startDate
    let endDate
    let selectedProvinceCity = []
    let selectedProvinceCityExist = []
    let pdoRequestApi = []
    let iriRequestApi = []
    let dataRegencyIds = []
    let dataRegencyExistingIds = []
    let pdoRequestExisting = []
    let iriRequestExisting = []
    let selectedProvinceAndCityEdit = JSON.parse(JSON.stringify(this.selectedProvinceAndCity))
    let codeSelectedToApi
    let projectOfficerToApi
    let picSelected
    let pokRequestApi = []
    let PokExisting = []
    let selectedChainCodeAndIndicator = JSON.parse(JSON.stringify(this.listSelectedChainCodeAndIndicator))
    let selectedChainCodeAndIndicatorExist = JSON.parse(JSON.stringify(this.listChainCodeAndIndicatorExist))
    let selectedChainCodeAndIndicatorToApi = []
    let selectedChainCodeAndIndicatorToApiExist = []
    let selectedRiskToApi

    selectedChainCodeAndIndicator.forEach(x => {
      let dataChainCode = x.resultChainId.id
      let dataIndicator = []
      x.successIndicatorIds.forEach(indicator => {
        dataIndicator.push(indicator.id)
      })
      selectedChainCodeAndIndicatorToApi.push({
        resultChainId: dataChainCode,
        successIndicatorIds: dataIndicator
      })
    })

    if (selectedChainCodeAndIndicatorExist.length > 0) {
      selectedChainCodeAndIndicatorExist.forEach(x => {
        let dataChainCodeExist = x?.id
        let dataIndicatorExist = []
        x.successIndicator.forEach(indicatorExist => {
          dataIndicatorExist.push(indicatorExist.id)
        })
        selectedChainCodeAndIndicatorToApiExist.push({
          awpResultChainId: dataChainCodeExist,
          awpSuccessIndicatorIds: dataIndicatorExist
        })
      })
    }

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(x => {
        PokExisting.push(x.id)
      })
    } else {
      PokExisting = []
    }

    if (!this.editjumlahNasrasumber) {
      this.editjumlahNasrasumber = 0
    }

    if (!this.editTotalPeserta) {
      this.editTotalPeserta = 0
    }

    if (this.pokRequest.length > 0) {
      this.pokRequest.forEach(x => {
        pokRequestApi.push(x.id)
      })
    }

    selectedProvinceAndCityEdit.forEach(x => {
      if (x.regencyId.length > 0) {
        x.regencyId = x.regencyId.map(y => this.dataAllCitySelected.find(xz => xz.name === y))
      }
    })

    if (this.codeSelected !== 'pilih') {
      let x = this.listCodeSelected.find(x => x.code == this.codeSelected)
      codeSelectedToApi = x.id
    } else {
      codeSelectedToApi = null
    }

    if (this.roles?.hasAuthority('Consultan') || this.roles?.hasAuthority('Coordinator')) {
      projectOfficerToApi = null
    } else {
      projectOfficerToApi = this.selectedProjectOfficer.id
    }

    selectedProvinceAndCityEdit.forEach(data => {
      if (data?.regencyId) {
        dataRegencyIds = []
        data.regencyId.forEach(x => {
          dataRegencyIds.push(x.id)
        })
        selectedProvinceCity.push({
          provinceId: data?.provinceId?.id,
          regencyIds: dataRegencyIds
        })
      } else {
        selectedProvinceCity.push({
          provinceId: data?.provinceId?.id,
          regencyIds: null
        })
      }
    })

    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(data => {
        pdoRequestExisting.push(data.id)
      })
    } else {
      pdoRequestExisting = []
    }

    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(data => {
        iriRequestExisting.push(data.id)
      })
    } else {
      iriRequestExisting = []
    }

    if (!this.selectedSubComponentExist) {
      selectedSubComponentId = null
    } else {
      selectedSubComponentId = this.selectedSubComponentExist.id
    }

    if (!this.selectedSubSubComponentExist) {
      selectedSubSubComponentId = null
    } else {
      selectedSubSubComponentId = this.selectedSubSubComponentExist.id
    }

    if (this.provinceAndRegenciesExist.length > 0) {
      this.provinceAndRegenciesExist.forEach(data => {
        if (data?.regencies) {
          dataRegencyExistingIds = []
          data?.regencies.forEach(x => {
            dataRegencyExistingIds.push(x.id)
          })
          selectedProvinceCityExist.push({
            awpProvincesId: data.id,
            awpProvincesRegenciesId: dataRegencyExistingIds
          })
        } else {
          selectedProvinceCityExist.push({
            awpProvincesId: data.id,
            awpProvincesRegenciesId: null
          })
        }
      })
    } else {
      selectedProvinceCityExist = []
    }


    if (this.pdoRequest.length > 0) {
      this.pdoRequest.forEach(data => {
        pdoRequestApi.push(data.id)
      })
    } else {
      pdoRequestApi = []
    }

    if (this.iriRequest.length > 0) {
      this.iriRequest.forEach(data => {
        iriRequestApi.push(data.id)
      })
    } else {
      iriRequestApi = []
    }

    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')

    if (this.selectedLsp) {
      picSelected = this.selectedLsp.id
    } else {
      picSelected = null
    }
    selectedRiskToApi = this.selectedRisk

    const params = {
      componentId: this.selectedComponentExist.id,
      subComponentId: selectedSubComponentId,
      subSubComponentId: selectedSubSubComponentId,
      componentCodeId: codeSelectedToApi,
      name: this.editNamaKegiatan,
      // pokEvent: this.existOnPok,
      budgetPok: this.editBudgetPok,
      budgetAwp: this.editBudgetAwp,
      lspContractNumber: this.nomorKontrakLsp,
      lspContractData: this.dataKontrakLsp,
      lspPic: picSelected,
      purpose: this.editTujuanKegiatan,
      description: this.editDeskripsiKegiatan,
      eventManagementTypeId: this.selectedValueEventType.id,
      startDate: startDate,
      endDate: endDate,
      eventOtherInformation: this.editWaktuInformasiLainnya,
      provincesRegenciesIds: selectedProvinceCityExist,
      newProvincesRegencies: selectedProvinceCity,
      eventVolume: this.editVolumeEvent,
      eventTypeId: this.selectedEvent.id,
      nsMale: this.nsLakiLaki,
      nsFemale: this.nsPerempuan,
      nsCount: this.editjumlahNasrasumber,
      nsInstitution: this.editAsalLembagaNarasumber,
      nsOtherInformation: this.editInformasiLainNarasumber,
      participantMale: this.pesertaLaki,
      participantFemale: this.pesertaPerempuan,
      participantCount: this.editTotalPeserta,
      participantOtherInformation: this.editInformasiPeserta,
      assumption: this.asumsi,
      descriptionRisk: this.descriptionRisk,
      potentialRisk: this.kemungkinanTerjadi,
      impactRisk: this.dampak,
      mitigationRisk: this.mitigasiResiko,
      eventOutput: this.editOutputKegiatan,
      pdoIds: pdoRequestExisting,
      newPdoIds: pdoRequestApi,
      iriIds: iriRequestExisting,
      newIriIds: iriRequestApi,
      rabFileId: this.dataIdRab,
      projectOfficer: projectOfficerToApi,
      symptomsRisk: this.gejalaResiko,
      pokIds: PokExisting,
      newPokIds: pokRequestApi,
      nameOtherInformation: this.otherInformationAwp,
      locationOtherInformation: this.tempatInformasiLainnya,
      participantTarget: this.targetParticipant,
      activityModeId: this.selectedModaActivity.id,
      newMonev: selectedChainCodeAndIndicatorToApi,
      monevIds: selectedChainCodeAndIndicatorToApiExist,
      risk: selectedRiskToApi
    }
    this.service.saveImplementation(this.detailIdKegiatan, params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveKegiatan = false
            this.router.navigate(['implement/detail-kegiatan-perencanaan/' + this.detailIdKegiatan])
          }
        }, error => {
          this.loadingSaveKegiatan = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }



}
