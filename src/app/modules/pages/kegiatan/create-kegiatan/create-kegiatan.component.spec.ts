import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateKegiatanComponent } from './create-kegiatan.component';

describe('CreateKegiatanComponent', () => {
  let component: CreateKegiatanComponent;
  let fixture: ComponentFixture<CreateKegiatanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateKegiatanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateKegiatanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
