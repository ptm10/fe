import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbDatepickerModule, NgbRatingModule, NgbTimepickerModule, NgbAccordionModule, NgbAlertModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {KegiatanRoutingModule} from './kegiatan-routing.module'
import {DataTablesModule} from 'angular-datatables';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import {UIModule} from "../../../shared/ui/ui.module";
import {ArchwizardModule} from "angular-archwizard";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {KegiatanComponent} from './kegiatan.component';
import { DetailKegiatanComponent } from './detail-kegiatan/detail-kegiatan.component';
import { AddKegiatanComponent } from './add-kegiatan/add-kegiatan.component'
import {FileUploadModule} from "@iplab/ngx-file-upload";
import { DetailPerencanaanComponent } from './detail-perencanaan/detail-perencanaan.component';
import { AddConceptNoteComponent } from './add-concept-note/add-concept-note.component';
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import {SimplebarAngularModule} from "simplebar-angular";
import { CreateKegiatanComponent } from './create-kegiatan/create-kegiatan.component';
import { ModalComponent } from './create-kegiatan/modal/modal.component';
import { DetailReportKegiatanComponent } from './detail-report-kegiatan/detail-report-kegiatan.component';
import { EditKegiatanComponent } from './edit-kegiatan/edit-kegiatan.component';
import {SwalAlert} from "../../../core/helpers/Swal";
import {ClipboardModule} from "@angular/cdk/clipboard";
import { TableListFormComponent } from './detail-kegiatan/table-list-form/table-list-form.component';
import {FullCalendarModule} from "@fullcalendar/angular";
import {PdfViewerModule} from "ng2-pdf-viewer";

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;


@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [KegiatanComponent, DetailKegiatanComponent, AddKegiatanComponent, DetailPerencanaanComponent, AddConceptNoteComponent, CreateKegiatanComponent, ModalComponent, DetailReportKegiatanComponent, EditKegiatanComponent, TableListFormComponent],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        SimplebarAngularModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        KegiatanRoutingModule,
        DataTablesModule,
        NgbTooltipModule,
        NgxMaskModule.forRoot(),
        UIModule,
        ArchwizardModule,
        NgbDatepickerModule,
        BsDatepickerModule,
        NgbRatingModule,
        FileUploadModule,
        CKEditorModule,
        NgbTimepickerModule,
        NgbAccordionModule,
        ClipboardModule,
        NgbAlertModule,
        FullCalendarModule,
        PdfViewerModule,
    ],
  providers: [
      SwalAlert
  ]
})
export class KegiatanModule { }
