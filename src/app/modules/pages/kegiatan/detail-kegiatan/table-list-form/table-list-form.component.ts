import {Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {leangueIndoDT} from "../../../../../shared/pagination-custom";
import {DataResponse} from "../../../../../core/models/dataresponse.models";
import {environment} from "../../../../../../environments/environment";
import {CommonCons} from "../../../../../shared/CommonCons";
import {DataTableDirective} from "angular-datatables";
import {EventActivityModel} from "../../../../../core/models/event-activity.model";
import {HttpClient} from "@angular/common/http";
import {SwalAlert} from "../../../../../core/helpers/Swal";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";
import {AwpService} from "../../../../../core/services/Awp/awp.service";
import {KegiatanService} from "../../../../../core/services/kegiatan.service";
import {EventFormEmailParticipant} from "../../../../../core/models/event.model";

@Component({
  selector: 'app-table-list-form',
  templateUrl: './table-list-form.component.html',
  styleUrls: ['./table-list-form.component.scss']
})
export class TableListFormComponent implements OnInit {
  @ViewChild('fileUploadEmailParticipant') fileUploadEmailParticipant: TemplateRef<any>;

  @Input() awpImplementId: string

  // datatable list event
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  start = 1
  filterData: string
  modelEventParticipant: EventActivityModel[]
  paramsLike = []
  paramsIs = []
  showToast = false
  uploadFileExcelForm: Array<File> = [];
  selectedForFormEmail: number
  selectedEvent: EventActivityModel
  loadingExportTemplate = false
  openedModal: any

  // email participant
  loadingUploadFormParticipant: boolean
  loadingGetDataSuccessEmailParticipant = false
  idFileSelected: string
  modelSuccessEmailParticipant: EventFormEmailParticipant



  // loading
  finishLoadData = true
  finishLoadDataCount = 0


  constructor(
      private http: HttpClient,
      private swalAlert: SwalAlert,
      private modalService: NgbModal,
      private router: Router,
      private service: AwpService,
      private serviceResources: KegiatanService,
  ) { }

  ngOnInit(): void {
    this.paramsIs.push({
      field: "awpImplementationId",
      dataType: "string",
      value: this.awpImplementId
    })
    this.getDataTableParticipant()
  }

  openQuestionForParticipant(url: string){
    window.open(url, "_blank");
  }

  goToSummaryQuestionnaire(id) {
    this.router.navigate([]).then(result => {  window.open('activity/summary-participant/' + id); })
  }

  goToSummaryQuestionnaireEvaluator(id) {
    this.router.navigate([]).then(result => {  window.open('activity/summary-evaluator/' + id); })
  }

  openModalForm(selectedMenu: any, formFor: number, selectedEventForm: EventActivityModel, uploadForm: boolean) {
    this.selectedForFormEmail = null
    this.selectedEvent = null
    this.selectedEvent = selectedEventForm
    this.selectedForFormEmail = formFor
    if (!uploadForm) {
      this.checkStatusEmailStatus()
    }
    this.openedModal = this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /**
   * Download Template Excel
   */

  exportExcelTempalateForm() {
    this.loadingExportTemplate = true
    this.service.exportWordTemplateForm().subscribe(
        resp => {
          this.loadingExportTemplate = false
        }, error => {
          this.loadingExportTemplate = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  getDataTableParticipant() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramsIs,
          paramIn: [],
          paramNotIn: [],
          paramDateBetween: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'event/datatable-event-lsp', params, CommonCons.getHttpOptions()).subscribe(resp => {
          if (resp.success) {
            this.stopLoading()
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.modelEventParticipant = resp.data.content;
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'updatedAt' },{ data: 'name' },{ data: 'name' }, { data: 'name' }],
      order: [[1, 'asc']]
    };
  }


  /**
   * function refresh datatable participant
   */
  refreshDatatable(): void {
    this.paramsLike = []
    if (this.filterData) {
      const paramsFirstName = {
        field: "name",
        dataType: "string",
        value: this.filterData
      }
      this.paramsLike.push(paramsFirstName)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  /**
   * function upload form email
   */
  uploadFormEmailParticipant() {
    this.openedModal.close()
    this.filterData = null
    this.refreshDatatable()
  }

  doUploadFileEmailParticipant(uploadFrom: number) {
    this.loadingUploadFormParticipant = true
    if (this.uploadFileExcelForm.length > 0) {
      this.serviceResources.uploadFileEmailParticipant(this.uploadFileExcelForm[0]).subscribe(resp => {
            if (resp.success) {
              this.idFileSelected = resp.data[0].id
              this.doUploadFileExcelToEvent(uploadFrom)
            } else {
              this.loadingUploadFormParticipant = false
            }
          }, error => {
            this.loadingUploadFormParticipant = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    }
  }

  doUploadFileExcelToEvent(uploadFrom: number) {
    let params = {
      fileId: this.idFileSelected,
      eventId: this.selectedEvent?.id,
      createdFor: uploadFrom
    }
    this.service.uploadFormUser(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingUploadFormParticipant = false
            this.uploadFileExcelForm = []
            this.openedModal.close()
            this.refreshDatatable()
          }
        }, error => {
          this.loadingUploadFormParticipant = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  checkStatusEmailStatus() {
    this.loadingGetDataSuccessEmailParticipant = true
    this.service.resultFormUser(this.selectedEvent?.id, this.selectedForFormEmail).subscribe(
        resp => {
          if (resp.success) {
            this.loadingGetDataSuccessEmailParticipant = false
            this.modelSuccessEmailParticipant = resp.data
          }
        }, error => {
          this.loadingGetDataSuccessEmailParticipant = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
