import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableListFormComponent } from './table-list-form.component';

describe('TableListFormComponent', () => {
  let component: TableListFormComponent;
  let fixture: ComponentFixture<TableListFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableListFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableListFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
