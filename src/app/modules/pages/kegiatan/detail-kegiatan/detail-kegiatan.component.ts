import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {Kegiatan} from "../../../../core/models/awp.model";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ReportEventModel} from "../../../../core/models/report-event.model";
import {ActivityReport} from "../../../../core/models/ActivityReport";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {parse} from "date-fns";

@Component({
  selector: 'app-detail-kegiatan',
  templateUrl: './detail-kegiatan.component.html',
  styleUrls: ['./detail-kegiatan.component.scss']
})
export class DetailKegiatanComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  detailIdKegiatan: string
  dataDetailKegiatan: Kegiatan
  listAllReportKegiatan: ActivityReport[]
  dataAllEvent: ReportEventModel[]
  currentRate = 8;
  countEntryTrue: number = 0
  checkedButton: number
  loadingDetailEvent = false
  showNotificationLspCreateReport = false
  showNotificationLspFinishReport = false
  getUnit = localStorage.getItem('unit')

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private service: KegiatanService,
      public roles: AuthfakeauthenticationService,
      private modalService: NgbModal,
      private swalAlert: SwalAlert,
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Kegiatan' }, { label: 'Detail Kegiatan', active: true }]
    this.detailIdKegiatan = this.route.snapshot.paramMap.get('id')
    this.getDetailEvent()
    this.checkedButton = 1
  }

  getDetailEvent() {
    let todayDate = new Date()
    let convertStartDate
    let convertEndDate
    let startCreateReport
    let endCreateReport
    this.startLoading()
    this.service.detailKegiatan(this.detailIdKegiatan).subscribe(
        resp => {
          if (resp.success) {
            this.dataDetailKegiatan = resp.data
            this.dataDetailKegiatan.convertStartDate = new Date(this.dataDetailKegiatan.startDate)
            this.dataDetailKegiatan.convertEndDate = new Date(this.dataDetailKegiatan.endDate)
            let filteredDataCheckedTrue = this.dataDetailKegiatan?.awpImplementationQualityAtEntryAnswers.filter(data => data.answer == true)
            if (filteredDataCheckedTrue.length > 0) {
              this.countEntryTrue = filteredDataCheckedTrue.length
            } else {
              this.countEntryTrue = 0
            }

            this.dataDetailKegiatan?.pdo?.forEach(dataPdo => {
              if (this.dataDetailKegiatan.awp.year == 2020) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2020
              } else if (this.dataDetailKegiatan.awp.year == 2021) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2021
              } else if (this.dataDetailKegiatan.awp.year == 2022) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2022
              } else if (this.dataDetailKegiatan.awp.year == 2023) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2023
              } else if (this.dataDetailKegiatan.awp.year == 2024) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2024
              }
            })
            this.dataDetailKegiatan?.iri?.forEach(dataIri => {
              if (this.dataDetailKegiatan.awp.year == 2020) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2020
              } else if (this.dataDetailKegiatan.awp.year == 2021) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2021
              } else if (this.dataDetailKegiatan.awp.year == 2022) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2022
              } else if (this.dataDetailKegiatan.awp.year == 2023) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2023
              } else if (this.dataDetailKegiatan.awp.year == 2024) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2024
              }
            })
            this.detailListKegiatanEvent()
            if (this.dataDetailKegiatan?.eventManagementType?.name === 'Swakelola') {
              this.getListReportKegiatanStaff()
            } else if (this.dataDetailKegiatan?.eventManagementType?.name === 'Kontrak') {
              if (this.dataDetailKegiatan?.lspPicUser) {
                this.getListReportKegiatanLSP(this.dataDetailKegiatan?.lspPicUser?.id)
              } else {
                this.listAllReportKegiatan = []
              }
            }

            if (this.dataDetailKegiatan?.eventManagementType?.name === 'Kontrak') {
              convertStartDate = parse(this.dataDetailKegiatan.startDate, 'yyyy-MM-dd', new Date());
              convertEndDate =  parse(this.dataDetailKegiatan.endDate, 'yyyy-MM-dd', new Date());
              // dataResp.convertDateCreated = parse(dataResp.createdAt, 'dd-MM-yyyy HH:mm:ss', new Date());

              startCreateReport = parse(this.dataDetailKegiatan.endDate, 'yyyy-MM-dd', new Date());
              startCreateReport.setHours(0, 0, 0, 0);

              endCreateReport = parse(this.dataDetailKegiatan.endDate, 'yyyy-MM-dd', new Date());
              endCreateReport.setDate(endCreateReport.getDate() + 14)
              endCreateReport.setHours(0, 0, 0, 0);
              todayDate.setHours(0, 0, 0, 0);
              if (todayDate < convertStartDate) {
                this.showNotificationLspCreateReport = false
                this.showNotificationLspFinishReport = false
              } else if (todayDate > startCreateReport && todayDate <= endCreateReport) {
                this.showNotificationLspCreateReport = true
                this.showNotificationLspFinishReport = false
              } else if (todayDate > endCreateReport) {
                this.showNotificationLspCreateReport = false
                this.showNotificationLspFinishReport = true
              }
            }
            if (this.getUnit === 'PCU') {
              this.router.navigate(['404_not_found'])
            }
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          if (this.getUnit === 'PCU') {
            this.router.navigate(['404_not_found'])
          } else {
            this.swalAlert.showAlertSwal(error)
          }
        }
    )
  }

  openModalHistory(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  openModalListEvent(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'xl', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  showToolTip() {
    if (this.countEntryTrue >= 5) {
      return `Direkomendasi untuk dilaksanakan, Hanya diperlukan revisi/klarifikasi kecil terhadap RIK.`
    } else if (this.countEntryTrue == 3 || this.countEntryTrue == 4) {
      return `Direkomendasi untuk dilaksanakan, tetapi harus ada revisi/klarifikasi yang berarti terhadap RIK.`
    } else if (this.countEntryTrue <= 2) {
      return `Kegiatan TIDAK direkomendasikan untuk dilaksanakan jika tidak ada revisi mendasar terhadap RIK.`
    }
  }

  goToListKegiatan() {
    this.router.navigate(['implement/kegiatan'])
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  goToImplementKegiatan() {
    this.router.navigate(['implement/add-kegiatan/' + this.detailIdKegiatan])
  }

  /** Add routing 'create-kegiatan' */
  createKegiatan() {
    this.router.navigate(['implement/create-kegiatan/' + this.detailIdKegiatan])
  }

  createEventReportLsp() {
    this.router.navigate(['activity/add-report-event-lsp/' + this.detailIdKegiatan])
  }

  goToConceptNote() {
    this.router.navigate(['implement/add-concept-note/' + this.detailIdKegiatan + '/1'])
  }

  downloadFile(id, name) {
    this.service.exportFile(id, name).subscribe(
        resp => {
        }, error => {
        }
    )
  }

  /** Add Condition Create Laporan kegiatan */

  checkConditionCreateKegiatanReport() {
    let todayDate = new Date()
    if (this.roles?.hasAuthority('Consultan') && this.dataDetailKegiatan.status == 5) {
      if (todayDate > this.dataDetailKegiatan.convertEndDate) {
        return true
      }
    } else if (this.roles.hasAuthority('LSP') && this.dataDetailKegiatan.status == 5) {
      if (todayDate > this.dataDetailKegiatan.convertEndDate) {
        return true
      }
    }
  }

  checkConditionCreateLspEvent() {
    let todayDate = new Date()
    if (this.roles.hasAuthority('LSP') && this.dataDetailKegiatan.status == 5) {
      if (todayDate >= this.dataDetailKegiatan.convertStartDate && todayDate <= this.dataDetailKegiatan.convertEndDate) {
        return true
      }
    }
  }

  detailListKegiatanEvent() {
    this.startLoading()
    let params = {
      enablePage: false,
      page: 0,
      paramLike: [],
      paramIs: [
        {
          field: "event.awpImplementationId",
          dataType: "string",
          value: this.dataDetailKegiatan?.id
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "desc"
        }
      ]
    }
    this.service.listAllEvent(params).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataAllEvent = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  goToDetailReportEvent(data) {
    this.router.navigate(['activity/detail-report-event/' + data + '/1'])
  }

  goToDetailReportActivity(data) {
    this.router.navigate(['implement/detail-report-activity/' + data])
  }

  getListReportKegiatanStaff() {
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "awpImplementationId",
          dataType: "string",
          value: this.detailIdKegiatan
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "desc"
        }
      ]
    }
    this.startLoading()
    this.service.listReportKegiatanStaff(params).subscribe(
        resp => {
          if (resp.success) {
            this.stopLoading()
            this.listAllReportKegiatan = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListReportKegiatanLSP(id) {
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "awpImplementation.lspPic",
          dataType: "string",
          value: id
        },
        {
          field: "awpImplementationId",
          dataType: "string",
          value: this.detailIdKegiatan
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "desc"
        }
      ]
    }
    this.startLoading()
    this.service.listReportKegiatanLsp(params).subscribe(
        resp => {
          if (resp.success) {
            this.stopLoading()
            this.listAllReportKegiatan = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
