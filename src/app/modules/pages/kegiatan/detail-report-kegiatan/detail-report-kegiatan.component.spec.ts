import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailReportKegiatanComponent } from './detail-report-kegiatan.component';

describe('DetailReportKegiatanComponent', () => {
  let component: DetailReportKegiatanComponent;
  let fixture: ComponentFixture<DetailReportKegiatanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailReportKegiatanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailReportKegiatanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
