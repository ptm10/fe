import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import Swal from "sweetalert2";
import {ActivityReport} from "../../../../core/models/ActivityReport";
import {parse} from "date-fns";
import {ReportEventModel} from "../../../../core/models/report-event.model";
import {MangementType} from "../../../../core/models/awp.model";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {Monev, Monev2} from "../../../../core/models/monev";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {Pdfs} from "../../../../core/models/pdfViewer.model";
import {FileService} from "../../../../core/services/Image/file.service";

@Component({
  selector: 'app-detail-report-kegiatan',
  templateUrl: './detail-report-kegiatan.component.html',
  styleUrls: ['./detail-report-kegiatan.component.scss']
})
export class DetailReportKegiatanComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  detailIdKegiatan: string
  detailActivity: ActivityReport
  fieldSubstantiv: number
  flagApproveReject: number
  loadingSaveData = true

  // list all event
  listSelectedEvent: ReportEventModel
  dataAllEvent: ReportEventModel[]
  totalAllRealitationEvent: number
  percentageAllRealitationEvent: number
  totalAllParticipantEvent: number
  percentageAllParticipantEvent: number
  totalAllParticipantMaleEvent: number
  percentageAllParticipantMaleEvent: number
  totalAllParticipantFemaleEvent: number
  percentageAllParticipantFemaleEvent: number
  fromDateIntroduction: Date
  toDateIntroduction: Date
  selectedDocumentationEvent: ReportEventModel
  rejectMessage: string

  // management type
  listManagementType: MangementType[]
  listManagementTypeMaster: MangementType[]
  selectedMangementTypeExist: MangementType

  successIndicatorIds = []
  successIndicatorSelected = []

  // result chain code
  listChainCode: Monev2[]
  listChainCodeMaster: Monev2[]
  listChainCodeFilter: Monev2[]
  listChainCodeName: string[] = [];
  selectedChainCode: Monev2

  // indicator Success
  selectedIndicatorSuccess: string[] = []
  listIndicatorSuccessApi: string[];
  dataListIndicatorSuccess: Monev[]
  listSelectedChainCodeAndIndicator = []
  listChainCodeAndIndicatorExist = []
  listMonevExisting = []
  createNewIndicatorSuccess: boolean = null

//pdf
  pdfCountKegiatan: Pdfs[] = [];
  pdfCountRab: Pdfs[] = [];
  pdfSrcKegiatan: any
  pdfSrcRab: any

  // pdf viewer
  ZOOM_STEP = 0.25;
  DEFAULT_ZOOM = 1;

  constructor(
      private route: ActivatedRoute,
      private service: KegiatanService,
      private serviceAwp: AwpService,
      private router: Router,
      private modalService: NgbModal,
      private serviceNotification: ProfileService,
      private swalAlert: SwalAlert,
      private serviceImage: FileService,

  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Kegiatan' }, { label: 'Detail Kegiatan' }, { label: 'Detail Laporan Kegiatan', active: true }]
    this.detailIdKegiatan = this.route.snapshot.paramMap.get('id')
    this.getDetailKegiatan()
    this.fieldSubstantiv = 1
    this.updateStatusTask()
  }

  // pdf viewer
  public zoomIn(pdf: Pdfs) {
    pdf.pdfZoom += this.ZOOM_STEP;
  }
  public zoomOut(pdf: Pdfs) {
    if (pdf.pdfZoom > this.DEFAULT_ZOOM) {
      pdf.pdfZoom -= this.ZOOM_STEP;
    }
  }
  public resetZoom(pdf: Pdfs) {
    pdf.pdfZoom = this.DEFAULT_ZOOM;
  }
  nextPage(pdf: Pdfs) {
    pdf.page += 1;
  }
  previousPage(pdf: Pdfs) {
    pdf.page -= 1;
  }
  afterLoadComplete(pdfData: any, pdf: Pdfs) {
    pdf.totalPages = pdfData.numPages;
  }

  /** Add Pdf Bast */
  getPdfBast(id, isPdf: number) {
    this.pdfCountKegiatan = []
    if (isPdf == 1) {
      this.serviceImage.getUrlPdf(id).subscribe(
          resp => {
            let reader = new FileReader();
            reader.addEventListener("load", () => {
              this.pdfSrcKegiatan = reader.result;
              const pdf: Pdfs = {
                src: this.pdfSrcKegiatan,
                isCollapsed: false,
                totalPages: 1,
                page: 1,
                pdfZoom: 1
              };
              this.pdfCountKegiatan.push(pdf);
            }, false);
            reader.readAsDataURL(resp);
          }
      )
    }
  }

  /** Change Menu Type */
  changeMenuFieldProgress(menu: any) {
    this.fieldSubstantiv = menu
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  openModalHistory(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  openModalMonitoringEvaluation(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'xl', centered: true });
  }

  openModalAddIndicator(selectedMenu: any, dataSelected: Monev, createNew: boolean) {
    this.selectedIndicatorSuccess = []
    this.selectedChainCode = null
    this.createNewIndicatorSuccess = createNew
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
    if (dataSelected) {
      this.selectedChainCodeFromList(dataSelected)
    }
  }

  /** Approval Status */

  updateReportActivity() {
    this.loadingSaveData = false
    let successIndicatorExist = []
    let monevExisting = []
    let newMonev = []
    let monevIds = []
    let successIndicatorSelected = []
    let successIndicatorSelectedNewExis = []

    this.listMonevExisting.forEach(x => {
      x.successIndicator.forEach(y => {
        console.log(y)
        y.successIndicator.awpReportSuccessIndicators = y.id
        successIndicatorExist.push(y.successIndicator)
      })
    })
    this.successIndicatorIds = successIndicatorExist.map(key => ({id: key.awpReportSuccessIndicators, contribution: key.resultIndicator}));
    successIndicatorSelectedNewExis = successIndicatorExist.map(key => ({id: key.id, contribution: key.resultIndicator}));

    this.listSelectedChainCodeAndIndicator.forEach(x => {
      x.successIndicatorIds.forEach(y => {
        successIndicatorSelected.push(y)
      })
    })
    this.successIndicatorSelected = successIndicatorSelected.map(key => ({id: key.id, contribution: key.resultIndicator}));

    this.listMonevExisting.forEach(x => {
      x.successIndicator.forEach(y => {
        let dataIndicator = []
        if (y.existing) {
          this.successIndicatorIds.forEach(successIndicator => {
            if (successIndicator?.id === y.id) {
              dataIndicator.push(successIndicator)
            }
          })
          let findMonevExist = monevIds.find(z => z.awpReportResultChainId === x.successIndicator[0].awpImplementationReportResultChainId)
          if (findMonevExist) {
            findMonevExist.awpReportSuccessIndicators.push(dataIndicator[0])
          } else {
            monevIds.push({
              awpReportResultChainId: x.successIndicator[0].awpImplementationReportResultChainId,
              awpReportSuccessIndicators: dataIndicator
            })
          }
        } else {
          let dataIndicator = []
          successIndicatorSelectedNewExis.forEach(successIndicator => {
            if (successIndicator?.id === y.successIndicator.id) {
              dataIndicator.push(successIndicator)
            }
          })
          let findMonevExist = monevExisting.find(z => z.resultChainId === x.resultChain.id)
          if (findMonevExist) {
            findMonevExist.successIndicators.push(dataIndicator[0])
          } else {
            monevExisting.push({
              resultChainId: x.resultChain.id,
              successIndicators: dataIndicator
            })
          }
        }
      })
    })

    console.log(newMonev)

    this.listSelectedChainCodeAndIndicator.forEach(x => {
      x.successIndicatorIds.forEach(y => {
        let dataIndicator = []
        this.successIndicatorSelected.forEach(successIndicator => {
          if (successIndicator?.id === y.id) {
            dataIndicator.push(successIndicator)
          }
        })
        let findMonevExist = monevExisting.find(z => z.resultChainId === x.resultChainId.id)
        if (findMonevExist) {
          findMonevExist.successIndicators.push(dataIndicator[0])
        } else {
          monevExisting.push({
            resultChainId: x.resultChainId.id,
            successIndicators: dataIndicator
          })
        }
      })
    })

    const param = {
      id: this.detailIdKegiatan,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
      newMonev: monevExisting,
      monevIds: monevIds
    }
    this.service.updateLspActivityReport(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailKegiatan()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromConsultant() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdKegiatan,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromConsultantActivityReport(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailKegiatan()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromCoordinator() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdKegiatan,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromCoordinatorActivityReport(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailKegiatan()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromTreasure() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdKegiatan,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromTreasureActivityReport(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailKegiatan()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromHeadPmu() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdKegiatan,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
    }
    this.service.updateFromHeadPmuActivityReport(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.goToDetailKegiatan()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** End Approval Status */


  goToDetailKegiatan() {
    this.router.navigate(['implement/detail-kegiatan/' + this.detailActivity.awpImplementation.id])
  }

  goToEditKegiatan() {
    // if (this.roles.hasAuthority('LSP')) {
    //   this.router.navigate(['activity/edit-report-event-lsp/' + this.detailIdReportEvent])
    // } else {
    //   this.router.navigate(['activity/edit-report-event/' + this.detailIdReportEvent])
    // }
    this.router.navigate(['implement/edit-kegiatan/' + this.detailIdKegiatan])
  }

  openModalDocumentationEvent(data: ReportEventModel, selectedMenu: any) {
    this.selectedDocumentationEvent = data
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }


  openModal(selectedMenu: any, flagApproveReject: any) {
    this.flagApproveReject = flagApproveReject
    this.modalService.open(selectedMenu, { size: 'md', centered: true });
  }

  getDetailKegiatan() {
    this.startLoading()
    this.service.detailActivityReport(this.detailIdKegiatan).subscribe(
        resp => {
          if (resp.success) {
            this.stopLoading()
            this.detailActivity = resp.data
            this.detailActivity.convertCreatedAt = parse(this.detailActivity.createdAt, 'dd-MM-yyyy HH:mm:ss', new Date());
            this.detailActivity.convertStartDate = new Date(this.detailActivity.startDate)
            this.detailActivity.convertEndDate = new Date(this.detailActivity.endDate)

            if (this.detailActivity?.monev.length > 0) {
              this.detailActivity?.monev.forEach(x => {
                x.successIndicator?.forEach(x => {
                  x.existing = true
                })
                this.listMonevExisting.push(x)
                this.listChainCodeAndIndicatorExist.push(x)
                x.successIndicator = x.successIndicator.map(x => (
                    {
                      ...x,resultIndicator: ''
                    }
                ))
                this.listMonevExisting.forEach(x => {
                  x.successIndicator.forEach(y => {
                    y.successIndicator.resultIndicator = y.contribution
                  })
                })
              })
            } else {
              this.detailActivity?.awpImplementation?.monev.forEach(x => {
                x.successIndicator?.forEach(x => {
                  x.existing = true
                })
                this.listMonevExisting.push(x)
                this.listChainCodeAndIndicatorExist.push(x)
                x.successIndicator = x.successIndicator.map(x => (
                    {
                      ...x,resultIndicator: ''
                    }
                ))
              })
            }

            if (this.detailActivity?.bastFile) {
              if (this.detailActivity?.bastFile.fileExt === 'pdf') {
                this.getPdfBast(this.detailActivity?.bastFile?.id, 1)
              }
            }


            this.listResultChainCode()
            this.detailListKegiatanEvent()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  downloadFile(id, name) {
    // this.loadingDownloadPdf = true
    this.service.exportFile(id, name).subscribe(
        resp => {
          // this.loadingDownloadPdf = false
        }, error => {
          // this.loadingDownloadPdf = false
        }
    )
  }

  downloadWordReportActivity() {
    this.loadingSaveData = false
    this.serviceAwp.exportWordAwpReportActivity(this.detailActivity?.awpImplementation?.id, this.detailActivity?.awpImplementation?.name).subscribe(
        resp => {
          this.loadingSaveData = true
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  /** * List Target Result Chain Activity */
  listResultChainCode() {
    let tag
    let params = {
      enablePage: false,
      page: 0,
      paramLike: [],
      paramIs: [
        {
          field: "year",
          dataType: "int",
          value: this.detailActivity?.awpImplementation?.awp?.year
        },
        {
          field: "monevTypeId",
          dataType: "string",
          value: "afedb8dc-d1ee-11ec-88cb-0ba398ed0298"
        }
      ],
      paramIn: [],
      paramNotIn: [],
      sort: [
        {
          field: "code",
          direction: "asc"
        }
      ]
    }
    this.service.listResultChainAll(params).subscribe(
        resp => {
          this.startLoading()
          if (resp.success) {
            this.stopLoading()
            this.listChainCode = resp.data.content
            this.listChainCodeMaster = resp.data.content

            this.listChainCode = this.listChainCode.filter(x => x.code.charAt(2) === this.detailActivity?.awpImplementation?.awp?.component.code)
            this.listChainCodeMaster = this.listChainCodeMaster.filter(x => x.code.charAt(2) === this.detailActivity?.awpImplementation?.awp?.component.code)

            this.listChainCode.forEach(x => {
              x.successIndicator.forEach(y => {
                y.notApplicableAllYear = false
                if (this.detailActivity?.awpImplementation?.awp?.year === 2020) {
                  y.notApplicableAllYear = y.notApplicable2020
                } else if (this.detailActivity?.awpImplementation?.awp?.year === 2021) {
                  y.notApplicableAllYear = y.notApplicable2021
                } else if (this.detailActivity?.awpImplementation?.awp?.year === 2022) {
                  y.notApplicableAllYear = y.notApplicable2022
                } else if (this.detailActivity?.awpImplementation?.awp?.year === 2023) {
                  y.notApplicableAllYear = y.notApplicable2023
                } else if (this.detailActivity?.awpImplementation?.awp?.year === 2024) {
                  y.notApplicableAllYear = y.notApplicable2024
                }
              })
            })

            this.listChainCodeMaster.forEach(x => {
              x.successIndicator.forEach(y => {
                y.notApplicableAllYear = false
                if (this.detailActivity?.awpImplementation?.awp?.year === 2020) {
                  y.notApplicableAllYear = y.notApplicable2020
                } else if (this.detailActivity?.awpImplementation?.awp?.year === 2021) {
                  y.notApplicableAllYear = y.notApplicable2021
                } else if (this.detailActivity?.awpImplementation?.awp?.year === 2022) {
                  y.notApplicableAllYear = y.notApplicable2022
                } else if (this.detailActivity?.awpImplementation?.awp?.year === 2023) {
                  y.notApplicableAllYear = y.notApplicable2023
                } else if (this.detailActivity?.awpImplementation?.awp?.year === 2024) {
                  y.notApplicableAllYear = y.notApplicable2024
                }
              })
            })

            if (this.listChainCodeAndIndicatorExist.length > 0) {
              this.listChainCodeAndIndicatorExist.forEach(y => {
                this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
              })
            }
            this.listChainCodeFilter = this.listChainCode
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedChainCodeFromList(data) {
    this.listIndicatorSuccessApi = []
    this.selectedIndicatorSuccess = []
    let tag = []
    this.selectedChainCode = null
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }

    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }

    this.selectedChainCode = data
    this.listChainCode = this.listChainCode.filter(dataFilter => dataFilter.id != this.selectedChainCode.id)
    // this.listIndicatorSuccess()

    this.listIndicatorSuccessFromSelectedChainCode()

    this.listChainCodeFilter = this.listChainCode
  }

  listIndicatorSuccessFromSelectedChainCode() {
    let tag = []
    if (this.createNewIndicatorSuccess === true) {
      if (this.listSelectedChainCodeAndIndicator.length > 0) {
        this.listSelectedChainCodeAndIndicator.forEach(y => {
          y.successIndicatorIds.forEach(z => {
            this.selectedChainCode.successIndicator = this.selectedChainCode.successIndicator.filter(dataIndicator => z.id !== dataIndicator.id)
          })
        })
      }
    } else if (this.createNewIndicatorSuccess === false) {
      if (this.listMonevExisting.length > 0) {
        this.listMonevExisting.forEach(y => {
          y.successIndicator.forEach(z => {
            this.selectedChainCode.successIndicator = this.selectedChainCode.successIndicator.filter(dataIndicator => z.successIndicator?.id !== dataIndicator.id)
          })
        })
      }
    }

    this.selectedChainCode?.successIndicator?.forEach(y => {
      if (this.detailActivity?.awpImplementation?.awp?.year === 2020) {
        y.notApplicableAllYear = y.notApplicable2020
      } else if (this.detailActivity?.awpImplementation?.awp?.year === 2021) {
        y.notApplicableAllYear = y.notApplicable2021
      } else if (this.detailActivity?.awpImplementation?.awp?.year === 2022) {
        y.notApplicableAllYear = y.notApplicable2022
      } else if (this.detailActivity?.awpImplementation?.awp?.year === 2023) {
        y.notApplicableAllYear = y.notApplicable2023
      } else if (this.detailActivity?.awpImplementation?.awp?.year === 2024) {
        y.notApplicableAllYear = y.notApplicable2024
      }
    })

    this.selectedChainCode.successIndicator?.forEach(x => {
      if (!x.notApplicableAllYear) {
        tag.push(x.code + ' - ' + x.name)
      }
    })
    this.listIndicatorSuccessApi = tag
  }

  searchFilterChainCode(e) {
    const searchStr = e.target.value
    let filter = this.listChainCodeFilter
    if (this.selectedChainCode) {
      filter = filter.filter(x => x.id !== this.selectedChainCode.id)
    } else {
      filter = this.listChainCodeFilter
    }

    this.listChainCode = filter.filter((product) => {
      return ((product?.result?.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product?.resultChainCode?.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * End Of List Target Result Chain */

  /** * List Indicator Success */
  listIndicatorSuccess() {
    let tag = []
    const params = {
      enablePage: false,
      page: 0,
      paramLike: [],
      paramIs: [
        {
          field: "resultChainId",
          dataType: "string",
          value: this.selectedChainCode?.id
        }
      ],
      paramIn: [],
      paramNotIn: [],
      sort: [
        {
          field: "successIndicatorCode",
          direction: "asc"
        }
      ]
    }
    this.service.listIndicatorCode(params).subscribe(
        resp => {
          if (resp.success) {
            this.dataListIndicatorSuccess = resp.data.content
            this.dataListIndicatorSuccess.forEach(x => {
              if (this.createNewIndicatorSuccess === true) {
                if (this.listSelectedChainCodeAndIndicator.length > 0) {
                  this.listSelectedChainCodeAndIndicator.forEach(y => {
                    y.successIndicatorIds.forEach(z => {
                      this.dataListIndicatorSuccess = this.dataListIndicatorSuccess.filter(dataIndicator => z.id !== dataIndicator.id)
                    })
                  })
                }
              } else if (this.createNewIndicatorSuccess === false) {
                if (this.listMonevExisting.length > 0) {
                  this.listMonevExisting.forEach(y => {
                    y.successIndicator.forEach(z => {
                      console.log(z)
                      this.dataListIndicatorSuccess = this.dataListIndicatorSuccess.filter(dataIndicator => z.successIndicator?.id !== dataIndicator.id)
                    })
                  })
                }
              }
            })
            this.dataListIndicatorSuccess.forEach(x => {
              tag.push(x.successIndicatorCode + ' - ' + x.successIndicator)
            })
            this.listIndicatorSuccessApi = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addSelectedResultChainAndIndicatorSuccess() {
    let selectedIndicator = []
    let dataSelectedIndicator = []
    let selectedIndicatorToApi = []

    this.selectedIndicatorSuccess.forEach(x => {
      const selectedIndicatorSuccessExist = x.split(" ", 1)
      selectedIndicator.push(selectedIndicatorSuccessExist[0])
    })

    selectedIndicator.forEach(x => {
      dataSelectedIndicator = this.selectedChainCode?.successIndicator.filter(y => x === y.code)
      if (dataSelectedIndicator.length > 0) {
        selectedIndicatorToApi.push(dataSelectedIndicator[0])
      }
    })
    this.listSelectedChainCodeAndIndicator.push({
      resultChainId: this.selectedChainCode,
      successIndicatorIds: selectedIndicatorToApi
    })
    console.log(this.listSelectedChainCodeAndIndicator)
    this.selectedChainCode = null
    this.selectedIndicatorSuccess = []
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }

    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  addSelectedResultChainAndIndicatorSuccessExisting() {
    let selectedIndicator = []
    let dataSelectedIndicator = []
    let selectedIndicatorToApi = []
    let filterSelectedChainCodeAndIndicator = []

    this.selectedIndicatorSuccess.forEach(x => {
      const selectedIndicatorSuccessExist = x.split(" ", 1)
      selectedIndicator.push(selectedIndicatorSuccessExist[0])
    })

    selectedIndicator.forEach(x => {
      dataSelectedIndicator = this.selectedChainCode?.successIndicator.filter(y => x === y.code)
      if (dataSelectedIndicator.length > 0) {
        selectedIndicatorToApi.push(dataSelectedIndicator[0])
      }
    })

    if (this.createNewIndicatorSuccess === true) {
      this.listSelectedChainCodeAndIndicator.forEach(x => {
        filterSelectedChainCodeAndIndicator = this.listSelectedChainCodeAndIndicator.filter(x => x.resultChainId?.id === this.selectedChainCode.id)
        if (filterSelectedChainCodeAndIndicator.length > 0) {
          selectedIndicatorToApi.forEach(y => {
            filterSelectedChainCodeAndIndicator[0].successIndicatorIds.push(y)
          })
        }
      })
    }

    if (this.createNewIndicatorSuccess === false) {
      filterSelectedChainCodeAndIndicator = this.listMonevExisting.filter(x => x.resultChain?.id === this.selectedChainCode.id)
      console.log(filterSelectedChainCodeAndIndicator)
      if (filterSelectedChainCodeAndIndicator.length > 0) {
        selectedIndicatorToApi.forEach(y => {
          filterSelectedChainCodeAndIndicator[0].successIndicator.push({
            existing: false,
            successIndicator: y
          })
        })
      }
    }

    this.selectedChainCode = null
    this.selectedIndicatorSuccess = []
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }

    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  removeSelectedChainCodeAndIndicator(data) {
    this.listSelectedChainCodeAndIndicator.forEach((check, index ) => {
      if (check.resultChainId.id == data.resultChainId.id) {
        this.listSelectedChainCodeAndIndicator.splice(index, 1)
      }
    })
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }
    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  removeSelectedChainCodeAndIndicatorExist(data) {
    this.listChainCodeAndIndicatorExist.forEach((check, index ) => {
      if (check.id == data.id) {
        this.listChainCodeAndIndicatorExist.splice(index, 1)
      }
    })
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }
    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  removeNewIndicatorExist(data) {
    this.listMonevExisting.forEach(x => {
      x.successIndicator.forEach((check, index ) => {
        if (check.successIndicator?.id === data?.successIndicator?.id) {
          x.successIndicator.splice(index, 1)
        }
      })
    })
  }

  removeNewIndicatorNew(data) {
    this.listSelectedChainCodeAndIndicator.forEach((dataList, index ) => {
      dataList.successIndicatorIds.forEach((check, index ) => {
        if (check.id === data.id) {
          dataList.successIndicatorIds.splice(index, 1)
        }
      })
      if (dataList.successIndicatorIds.length == 0) {
        this.listSelectedChainCodeAndIndicator.splice(index, 1)
      }
    })
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }
    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  /** * End Of List Indicator Success */

  validationForm() {
    let success
    let successExist
    let successIndicatorExist = []
    let successIndicatorSelected = []
    this.listMonevExisting.forEach(x => {
      x.successIndicator.forEach(y => {
        successIndicatorExist.push(y.successIndicator)
      })
    })
    this.listSelectedChainCodeAndIndicator.forEach(x => {
      x.successIndicatorIds.forEach(y => {
        successIndicatorSelected.push(y)
      })
    })
    success = successIndicatorExist?.filter(x => !x.resultIndicator)
    successExist = successIndicatorSelected?.filter(x => !x.resultIndicator)

    return success?.length > 0 || successExist.length > 0
  }


  changeEventType() {
    console.log(this.listSelectedEvent.id)
    this.fromDateIntroduction = parse(this.listSelectedEvent.startDate, 'yyyy-MM-dd', new Date());
    this.toDateIntroduction = parse(this.listSelectedEvent.endDate, 'yyyy-MM-dd', new Date());
  }

  detailListKegiatanEvent() {
    this.startLoading()
    let params = {
      enablePage: false,
      page: 0,
      paramLike: [],
      paramIs: [
        {
          field: "event.awpImplementationId",
          dataType: "string",
          value: this.detailActivity?.awpImplementation?.id
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "desc"
        }
      ]
    }
    this.service.listAllEvent(params).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataAllEvent = resp.data.content
            if (this.dataAllEvent.length > 0) {
              let regenciesFromListEvent = []
              this.totalAllRealitationEvent = 0
              this.percentageAllRealitationEvent = 0
              this.totalAllParticipantEvent = 0
              this.percentageAllParticipantEvent = 0
              this.totalAllParticipantMaleEvent = 0
              this.totalAllParticipantFemaleEvent = 0
              this.percentageAllParticipantFemaleEvent = 0
              this.listSelectedEvent = this.dataAllEvent[0]

              this.fromDateIntroduction = parse(this.listSelectedEvent.startDate, 'yyyy-MM-dd', new Date());
              this.toDateIntroduction = parse(this.listSelectedEvent.endDate, 'yyyy-MM-dd', new Date());
              this.dataAllEvent.forEach(x => {
                this.totalAllRealitationEvent = this.totalAllRealitationEvent + x.rraBudget
                this.totalAllParticipantEvent = this.totalAllParticipantEvent + x.participantCount
                this.totalAllParticipantMaleEvent = this.totalAllParticipantMaleEvent + x.participantMale
                this.totalAllParticipantFemaleEvent = this.totalAllParticipantFemaleEvent + x.participantFemale
              })
              this.percentageAllRealitationEvent = this.totalAllRealitationEvent / this.detailActivity?.awpImplementation?.budgetPok
              this.percentageAllRealitationEvent = Number(this.percentageAllRealitationEvent.toFixed(2))
              this.percentageAllParticipantEvent = this.totalAllParticipantEvent / this.detailActivity?.awpImplementation?.participantCount
              this.percentageAllParticipantEvent = Number(this.percentageAllParticipantEvent.toFixed(2))
              if (this.detailActivity?.awpImplementation?.participantMale !== 0 && this.detailActivity?.awpImplementation?.participantMale != null) {
                this.percentageAllParticipantMaleEvent = (this.totalAllParticipantMaleEvent / this.detailActivity?.awpImplementation?.participantMale)
                this.percentageAllParticipantMaleEvent = Number(this.totalAllParticipantMaleEvent.toFixed(2))
              } else {
                this.percentageAllParticipantMaleEvent = 0
              }
              if (this.detailActivity?.awpImplementation?.participantFemale !== 0 && this.detailActivity?.awpImplementation?.participantFemale != null) {
                this.percentageAllParticipantFemaleEvent = this.percentageAllParticipantFemaleEvent / this.detailActivity?.awpImplementation?.participantFemale

                this.percentageAllParticipantFemaleEvent = Number(this.percentageAllParticipantFemaleEvent.toFixed(2))
              } else {
                this.percentageAllParticipantFemaleEvent = 0
              }
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateStatusTask() {
    const params = {
      taskId: this.detailIdKegiatan,
      taskType: 9,
      status: 1
    }
    this.service.updateStatusTask(params).subscribe(
        resp => {
          if (resp.success) {
            this.serviceNotification.updateNotification.emit()
          }
        }
    )
  }




}
