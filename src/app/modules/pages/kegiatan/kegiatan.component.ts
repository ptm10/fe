import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../core/services/Awp/awp.service";
import {Router} from "@angular/router";
import {leangueIndoDT} from "../../../shared/pagination-custom";
import {DataResponse} from "../../../core/models/dataresponse.models";
import {environment} from "../../../../environments/environment";
import {CommonCons} from "../../../shared/CommonCons";
import {AwpModel, Kegiatan, MangementType} from "../../../core/models/awp.model";
import Swal from "sweetalert2";
import {SwalAlert} from "../../../core/helpers/Swal";
import {AuthfakeauthenticationService} from "../../../core/services/authfake.service";
import {ComponentModel} from "../../../core/models/componentModel";
import {CalendarOptions, EventClickArg, EventInput} from "@fullcalendar/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {FullCalendarComponent} from "@fullcalendar/angular";
import {DateHelper} from "../../../core/helpers/date-helper";
import {NgbModal, NgbNavChangeEvent} from "@ng-bootstrap/ng-bootstrap";
import {KegiatanService} from "../../../core/services/kegiatan.service";

@Component({
  selector: 'app-kegiatan',
  templateUrl: './kegiatan.component.html',
  styleUrls: ['./kegiatan.component.scss']
})
export class KegiatanComponent implements OnInit {
  @ViewChild('modalShow') modalShow: TemplateRef<any>;
  @ViewChild('editmodalShow') editmodalShow: TemplateRef<any>;
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  filterData: any
  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  start = 1
  modelKegiatan: Kegiatan[]
  filterYear: number
  selectedYear: number
  selectedManagement: any
  dataListYear = []
  dataListYearExist = []
  paramsLike = []
  paramsIs = []
  componentLogin: string
  unitLogin: string
  checkedButton: number

  // unit
  checkedComponent = []
  listComponentStaff = []
  componentExist: ComponentModel[]
  checkedComponentFilter = []

  // filter
  selectComponent: string[] = [];
  selectedFilterComponent: string
  selectSubComponent: string[] = [];
  selectedFilterSubComponent: string
  filterSubComponent = []

  // loading
  loadingExportWord = false

  // calender
  dataCalender: EventInput[]
  calenderModelEvent: Kegiatan[]
  dataCalenderInput: EventInput[] = []
  tempDataCalender: EventInput[] = []
  startDate: Date
  endDate: Date
  id: string
  tittle: string
  backgroundColorEventCalender: string
  formData: FormGroup;
  formEditData: FormGroup;
  titleDate: Date
  date: Date
  existingDate: Date
  nextDate: Date
  previousDate: Date
  tittleEventSelected: string
  descriptionEventSelected: string
  staffEvent = []
  taskTypeEventSelected: number
  listDescriptionEventSelected: any
  listStaffEvent = []
  otherStaffEvent = []
  province: any
  regencies: any
  managementType:any
  startDateEvent: any
  endDateEvent: any
  checkedSelfEvent = false
  checkedPmuEventStaff = false
  checkedSelfLsp = false
  checkedStaffEvent: boolean = true
  checkedStaffSelf: boolean = true
  checkedStaffOther: boolean = false
  checkedComponentWithoutUser = []
  finishLoadDataCalender = true
  editActivity: any;
  listComponentStaffCalender = []
  listAllManagementType: MangementType[]

  // model calender
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: '',
      right: ''
    },
    initialView: "dayGridMonth",
    themeSystem: "bootstrap",
    initialEvents: [],
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    // dateClick: this.openModalCalender.bind(this),
    eventClick: this.handleEventClick.bind(this),
    displayEventTime: false,
    customButtons: {
      myCustomButton: {
        text: 'custom!',
        click: function() {
          alert('clicked the custom button!');
        }
      }
    },
  };

  constructor(
      private http: HttpClient,
      private service: AwpService,
      private router: Router,
      private swalAlert: SwalAlert,
      public roles: AuthfakeauthenticationService,
      private dateHelper: DateHelper,
      private formBuilder: FormBuilder,
      private modalService: NgbModal,
      private serviceKegiatan: KegiatanService
  ) { }

  ngOnInit(): void {
    this.titleDate = new Date()
    this.checkedButton = 1
    this.componentLogin = localStorage.getItem('code_component')
    this.unitLogin = localStorage.getItem('unit')
    this.breadCrumbItems = [{ label: 'Kegiatan', active: true }];
    this.getDataTable()
    this.getListYear()
    this.listComponent()
    this.date = new Date()
    this.existingDate = new Date()
    this.nextDate = new Date(this.existingDate.setMonth(this.existingDate.getMonth() + 1))
    this.previousDate = this.dateHelper.minusDate(new Date(this.date), 1)
    this.listManagementType()
  }

  refreshCalender(changeEvent: NgbNavChangeEvent) {
    if (changeEvent.nextId === 2) {
      this.finishLoadDataCalender = true
      this.listCalenderEvent()
    } else if (changeEvent.nextId === 1) {
      this.getDataTable()
    }
  }

  getDataTable() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramsIs,
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'awp-implementation/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.modelKegiatan = resp.data.content;
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'createdAt' },{ data: 'component.code' }, { data: 'componentCode.code' },{ data: 'name' }, { data: 'budgetAwp' },  { data: 'budgetPok' }, { data: 'budgetPok' }, { data: 'status' }, { data: 'status' }],
      order: [[1, 'asc']]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.filterYear = this.selectedYear
    this.paramsIs = []

    if (this.selectedYear) {
      const selectedYearChoosed = {
        field: "awp.year",
        dataType: "int",
        value: this.filterYear
      }
      this.paramsIs.push(selectedYearChoosed)
    }

    if (this.checkedComponentFilter.length > 0) {
      const selectedComponent = {
        field: "componentId",
        dataType: "string",
        value: this.checkedComponentFilter[0]
      }
      this.paramsIs.push(selectedComponent)
    }

    if (this.filterSubComponent.length > 0) {
      const selectedSubComponent = {
        field: "subComponentId",
        dataType: "string",
        value: this.filterSubComponent[0].id
      }
      this.paramsIs.push(selectedSubComponent)
    }

    this.paramsLike = []
    if (this.filterData) {
      const paramsFirstName = {
        field: "name",
        dataType: "string",
        value: this.filterData
      }
      const paramsBudegetAwp =  {
        field: "budgetAwp",
        dataType: "int",
        value: this.filterData
      }
      const paramsBudegetPok =  {
        field: "budgetPok",
        dataType: "int",
        value: this.filterData
      }
      const paramsComponentDescription = {
        field: "component.code",
        dataType: "string",
        value: this.filterData
      }
      const paramsSubComponentDescription = {
        field: "component.description",
        dataType: "string",
        value: this.filterData
      }
      const paramsDescription = {
        field: "description",
        dataType: "string",
        value: this.filterData
      }
      const paramsCode = {
        field: "componentCode.code",
        dataType: "string",
        value: this.filterData
      }
      this.paramsLike.push(paramsBudegetAwp)
      this.paramsLike.push(paramsFirstName)
      this.paramsLike.push(paramsComponentDescription)
      this.paramsLike.push(paramsSubComponentDescription)
      this.paramsLike.push(paramsDescription)
      this.paramsLike.push(paramsBudegetPok)
      this.paramsLike.push(paramsCode)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  selectedAwpYear(data) {
    this.selectedYear = data
    this.refreshDatatable()
  }

  clearFilterYear() {
    this.selectedYear = null
    this.refreshDatatable()
  }

  clearFilter() {
    this.selectedFilterComponent = null
    this.selectedFilterSubComponent = null
    this.selectSubComponent = []
    this.checkedComponentFilter = []
    this.filterSubComponent = []
    this.refreshDatatable()
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.dataListYear = this.dataListYearExist.filter((product) => {
      return product.toString().search(searchStr.toString().toLowerCase()) !== -1
    });
  }

  /** Management type */
  listManagementType() {
    this.startLoading()
    this.service.eventManagementType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listAllManagementType = resp.data.content
              if (this.roles.hasAuthority('LSP')) {
                this.selectedManagement = this.listAllManagementType.find(x => x?.name === 'Kontrak')
              }
            }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  clearFilterManagementType() {
    this.selectedManagement = null
    this.listCalenderEvent()
  }

  selectedManagementType(data) {
    this.selectedManagement = null
    this.selectedManagement = data
    this.listCalenderEvent()
  }

  /** Change Data Calender */

  clearFilterCalender() {
    this.checkedComponent = []
    this.listComponentStaffCalender.forEach(data => {
      data.checked = false
    })
    this.checkedComponentWithoutUser = []
    this.checkedSelfEvent = false
    this.checkedPmuEventStaff = false
    this.listCalenderEvent()
  }

  selectedComponent() {
    this.checkedComponent = this.listComponentStaffCalender.filter(data => data.checked == true)
    let filterComponentSelected = this.listComponentStaffCalender.filter(data => data.checked == true && data.id !== 1 && data.id !== 2 && data.id !== 3)
    let filterComponentUser = this.listComponentStaffCalender.filter(data => data.checked == true && data.id == 1)
    let filterComponentStaff = this.listComponentStaffCalender.filter(data => data.checked == true && data.id == 2)
    this.checkedComponentWithoutUser = filterComponentSelected.filter(data => data.checked).map(data => data.id)
    this.checkedSelfEvent = filterComponentUser.length > 0;
    this.checkedPmuEventStaff = filterComponentStaff.length > 0;
    this.listCalenderEvent()
  }

  listCalenderEvent() {
    let params
    if (this.roles.hasAuthority('LSP')) {
      params = {
        self: true,
        staff: false,
        eventManagementTypeId: this.selectedManagement?.id,
        componentIds: []
      }
    } else {
      params = {
        self: this.checkedSelfEvent,
        staff: this.checkedPmuEventStaff,
        eventManagementTypeId: this.selectedManagement?.id,
        componentIds: this.checkedComponentWithoutUser
      }
    }

    /** Change Data Calender */
    this.service.calenderActivity(params).subscribe(
        resp => {
          if (resp.success) {
            this.tempDataCalender = []
            this.calenderModelEvent = resp.data
            this.calenderModelEvent.forEach(data => {
              data.backgroundColorCalender = ''
              this.startDate = new Date(data.startDate)
              this.endDate = new Date(data.endDate)
              this.id = data.id
              this.tittle = data.name
              if (data.component.code === '1') {
                data.backgroundColorCalender = 'bg-komponen1 text-white'
              } else if (data.component.code === '2') {
                data.backgroundColorCalender = 'bg-komponen2 text-white'
              } else if (data.component.code === '3') {
                data.backgroundColorCalender = 'bg-komponen3 text-white'
              } else if (data.component.code === '4') {
                data.backgroundColorCalender = 'bg-komponen41 text-white'
              }
              this.backgroundColorEventCalender = data.backgroundColorCalender
              this.tempDataCalender.push({
                id: this.id,
                title: this.tittle,
                start: this.startDate,
                end: this.endDate,
                className: this.backgroundColorEventCalender,
                extendedProps : {
                  description: data.description,
                  province: data.provincesRegencies,
                  startDate: new Date(data.startDate),
                  endDate: new Date(data.endDate),
                  eventMangement: data?.eventManagementType
                }
              })
            })
            // this.finishLoadData = true
            setTimeout(() => {
              this.dataCalenderInput = this.tempDataCalender
              this.calendarOptions.events = this.dataCalenderInput
              this.finishLoadDataCalender = false
              this.checkedComponent = this.listComponentStaffCalender.filter(data => data.checked == true)
            }, 1000 )
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeDateCalender() {
    this.titleDate = this.date
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate(this.date);
  }

  moveDateCalender(from: number) {
    if (from === 1) {
      this.date.setMonth(this.date.getMonth() + 1)
    } else {
      this.date = this.dateHelper.minusDate(new Date(this.date), 1)
    }
    this.existingDate = new Date(this.date)
    this.nextDate = new Date(this.existingDate.setMonth(this.existingDate.getMonth() + 1))
    this.previousDate = this.dateHelper.minusDate(new Date(this.date), 1)
    this.titleDate = new Date(this.date)
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate(this.date);
  }

  /*** Event click modal show */
  handleEventClick(clickInfo: EventClickArg) {
    this.tittleEventSelected = ''
    this.descriptionEventSelected = ''
    this.tittleEventSelected = clickInfo.event.title
    this.descriptionEventSelected = clickInfo.event.extendedProps.description
    this.taskTypeEventSelected = clickInfo.event.extendedProps.taskType
    this.province = clickInfo.event.extendedProps.province
    this.startDateEvent = clickInfo.event.extendedProps.startDate
    this.endDateEvent = clickInfo.event.extendedProps.endDate
    this.managementType = clickInfo.event.extendedProps.eventMangement
    this.editActivity = clickInfo.event;
    console.log(this.province)
    this.formEditData = this.formBuilder.group({
      editTitle: clickInfo.event.title,
      dataDescription: clickInfo.event.extendedProps.description,
      start: clickInfo.event.start
    });
    this.modalService.open(this.editmodalShow, { centered: true });
  }

  /**
   * get list year
   */
  getListYear() {
    this.startLoading()
    this.service.listYear().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataListYear = []
            this.dataListYear = resp.data
            this.dataListYearExist = resp.data
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.stopLoading()
          // this.finishLoadData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  listComponent() {
    let tag = []
    this.service.listAllComponent().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data
            this.listComponentStaffCalender.push({
              id: 1,
              code: 'Saya'
            })
            this.listComponentStaffCalender.push({
              id: 2,
              code: 'Staf',
            })
            this.componentExist.forEach(dataCode => {
              dataCode.checked = false
              dataCode.code = 'Komponen ' + dataCode.code
              this.listComponentStaff.push(dataCode)
              this.listComponentStaffCalender.push(dataCode)
              tag.push(dataCode.code)
            })
            this.selectComponent = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeFilterComponent() {
    this.checkedComponentFilter = []
    let tagSubComponent = []
    let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
    let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
    this.selectedFilterSubComponent = null
    if (this.selectedFilterComponent) {
      findComponent = findComponent.replace('komponen ', '')
      filteredComponent = filteredComponent.filter(x => x.code === findComponent)
      if (filteredComponent.length > 0) {
        tagSubComponent = []
        this.checkedComponentFilter.push(filteredComponent[0]?.id)
        filteredComponent[0].subComponent.forEach(x => {
          x.code = 'Sub-Komponen ' + x.code
          tagSubComponent.push(x.code)
        })
        this.selectSubComponent = []
        this.selectSubComponent = tagSubComponent
      }
    }
    this.refreshDatatable()
  }


  changeFilterSubComponent() {
    let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
    let findSubComponent = JSON.parse(JSON.stringify(this.selectedFilterSubComponent))
    let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
    let selectedFilterSubComponent
    if (this.selectedFilterSubComponent) {
      findComponent = findComponent.replace('komponen ', '')
      filteredComponent = filteredComponent.filter(x => x.code === findComponent)
      if (filteredComponent) {
        findSubComponent = findSubComponent.replace('Sub-Komponen ', '')
        selectedFilterSubComponent = filteredComponent[0].subComponent.filter(x =>x.code === findSubComponent)
        this.filterSubComponent = selectedFilterSubComponent
      }
      this.refreshDatatable()
    }
  }


  goToDetailKegiatan(id, status) {
    if (status == 0 || status == 5 || status == 6) {
      this.router.navigate(['implement/detail-kegiatan/' + id])
    } else if (status != 0 && status != 5 && status != 6) {
      this.router.navigate(['implement/detail-kegiatan-perencanaan/' + id])
    }
  }

  downloadWordReportActivity(data) {
    this.loadingExportWord = true
    this.service.exportWordAwpReportActivity(data.id, data.name).subscribe(
        resp => {
          this.loadingExportWord = false
        }, error => {
          this.loadingExportWord = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
