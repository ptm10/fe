import { Component, OnInit } from '@angular/core';
import {Event, Kegiatan, MangementType, PokCode, Province} from "../../../../core/models/awp.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {resourcesModel} from "../../../../core/models/resources.model";
import {PDOModel} from "../../../../core/models/PDO.model";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {ComponentService} from "../../../../core/services/Component/component.service";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import Swal from "sweetalert2";
import {parse} from "date-fns";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {SwalAlert} from "../../../../core/helpers/Swal";
import {ListUnit} from "../../../../core/models/RoleUsers";
import {ProfileService} from "../../../../core/services/Profile/profile.service";

@Component({
  selector: 'app-add-concept-note',
  templateUrl: './add-concept-note.component.html',
  styleUrls: ['./add-concept-note.component.scss']
})
export class AddConceptNoteComponent implements OnInit {

  public latarBelakangEvent = ClassicEditor;
  public eventDescription = ClassicEditor;
  public purposeEvent = ClassicEditor;
  public outputEvent = ClassicEditor;
  public participantTarget = ClassicEditor;

  detailIdKegiatan: string
  fromPage: string
  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  dataDetailKegiatan: Kegiatan
  dataIdRab: string = null
  dataIdKegiatan: string = null
  firstNameLogged: string
  lastNameLogged: string
  userIdLogin: string
  userComponentLogin: string
  nameNewStaff: string
  namePosition: string
  nameInstitution: string
  listMeqrResourceSelected = []
  tempatInformasiLainnya: string
  targetParticipant: string = null
  // unit
  listComponentStaff: ComponentModel[]
  componentExist: ComponentModel[]
  selectedComponentExist: ComponentModel

  // add jadwal kegiatan
  yearNewEvent: Date
  timeNewEvent: any
  startTime = {hour: 0, minute: 0};
  endTime = {hour: 0, minute: 0};
  warningTimeOver = false
  warningTimeOverText: string
  descriptionNewEvent: string
  penanggungJawab: string
  eventAgenda = []
  acceptAddEvent: number
  listAllNewAgendas = []


  // upload file
  uploadFileExcel: Array<File> = [];
  uploadFileExcelIds: Array<File> = [];
  uploadFileExcelAgenda: Array<File> = [];
  uploadFileExcelIdsAgenda: Array<File> = [];

  // data create CN
  eventName: string
  otherStaff = []
  existOnAnggaran: boolean
  editOtherInformationTime: string
  background: string = null
  description: string = null
  purpose: string = null
  eventOutput: string = null
  participantCount: number
  participantMale: number
  participantFemale: number
  participantOtherInformation: string
  location: string


  // data update
  date: Date
  editNamaKegiatan: string
  editBudgetAwp: number
  editBudgetPok: number
  editTujuanKegiatan: string
  editDeskripsiKegiatan: string
  editWaktuInformasiLainnya: string
  editjumlahNasrasumber: number
  editAsalLembagaNarasumber: string
  editInformasiLainNarasumber: string
  editTotalPeserta: number
  editInformasiPeserta: string
  editOutputKegiatan: string
  editVolumeEvent: number
  selectedValueEventType: any
  existOnPok: boolean
  nomorKontrakLsp: string
  dataKontrakLsp: string
  picLsp: string
  asumsi: string
  descriptionRisk: string
  mitigasiResiko: string
  volumeKomponenPembiayaan: string
  hargaSatuanKomponenPembiayaan: string
  jumlahKomponenPembiayaan: string
  pesertaLaki: number
  pesertaPerempuan: number
  kemungkinanTerjadi: number;
  dampak: number;
  nsLakiLaki: number
  nsPerempuan: number
  gejalaResiko: string
  loadingSaveKegiatan = false

  //componen
  selectedUnitExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]

  // sub componen
  selectedSubComponentExist: ComponentModel
  selectedSubComponentExistMaster: ComponentModel
  selectedSubComponentExistEdit: ComponentModel

  //sub-sub componen
  selectedSubSubComponentExist: ComponentModel
  selectedSubSubComponentExistMaster: ComponentModel
  selectedSubSubComponentExistEdit: ComponentModel
  codeSelected: any = 'pilih'
  listCodeSelected = []

  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // event
  listEvent: Event[]
  listEventMaster: Event[]
  selectedEvent: Event
  selectedEventTypeIsPenyedia: boolean

  //contact person
  listContactPerson: resourcesModel[]
  listContactPersonMaster: resourcesModel[]
  selectedContactPerson: resourcesModel


  // project officer
  listProjectOfficer: resourcesModel[]
  listProjectOfficerMaster: resourcesModel[]
  selectedProjectOfficer: resourcesModel
  loadingDataProjectOfficer = false

  // PDO
  listPdoAll: PDOModel[]
  listPdoAllMaster: PDOModel[]
  pdoRequest = []
  addPdo = []
  removePdo = []
  searchPdo = []
  pdoExisting = []

  // IRI
  listIriAll: PDOModel[]
  listIriAllMaster: PDOModel[]
  iriRequest = []
  addIri = []
  removeIri = []
  searchIri = []
  iriExisting = []

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  listCityAllExist: Province[]
  listCityAllMasterExist: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []
  listCityFromApi: string[];
  selectedCityFromList = []

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  listProvinceExist: Province[]
  listProvinceExistMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province
  provinceAndRegenciesExist = []
  searchFilterProvinces: any

  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  codeEvent: any
  minEvent: NgbDate
  maxEvent: NgbDate

  // management type
  listManagementType: MangementType[]
  listManagementTypeMaster: MangementType[]
  selectedMangementTypeExist: MangementType

  // peserta
  listResources: resourcesModel[]
  listResourcesMaster: resourcesModel[]
  selectedResourcesExist: resourcesModel
  resourcesRequest = []
  addResources = []
  removeResources = []
  searchResources = []

  //Moda Kegiatan
  listModaType: MangementType[]
  listModaTypeMaster: MangementType[]
  selectedModaActivity: any

  // POK
  listCodePok: PokCode[]
  listCodePokMaster: PokCode[]
  pokRequest = []
  addPok = []
  removePok = []
  searchPok = []
  pokExisting = []

  //work unit
  selectedWorkUnit: string
  listUnit: ListUnit[]

  //province
  listProvinceAllStaff: Province[]
  listProvinceAllMasterStaff: Province[]
  listProvinceApi: string[];
  selectedProvinceStaff: string
  listMeqrPmuSelected = []
  listMeqrPcuSelected = []

  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private service: KegiatanService,
      private serviceComponent: ComponentService,
      private serviceAwp: AwpService,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private serviceAdministration: AdministrationService,
      private modalService: NgbModal,
      public roles: AuthfakeauthenticationService,
      private swalAlert: SwalAlert,
      private serviceProfile: ProfileService
  ) {
  }

  ngOnInit(): void {
    this.participantCount = 0
    this.firstNameLogged = localStorage.getItem('fullName')
    this.lastNameLogged = localStorage.getItem('lastName')
    this.userIdLogin = localStorage.getItem('user_id')
    this.userComponentLogin = localStorage.getItem('componentId')
    this.detailIdKegiatan = this.activatedRoute.snapshot.paramMap.get('id')
    this.fromPage = this.activatedRoute.snapshot.paramMap.get('from')
    this.selectedWorkUnit = 'PMU'
    this.breadCrumbItems = [{ label: 'Event' }, { label: 'Detail Event'}, { label: 'Tambah Concept Note', active: true }]
    this.getDetailKegiatan()
    this.managementType()
    this.getListHolidayManagement()
    // this.getAllResources()
    this.listComponent()
    this.listAllModa()
    this.getListWorkUnit()
    this.listProvinceStaff()
    this.addCreateCnToParticipant()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  goToListKegiatan() {
    if (this.fromPage == '1') {
      this.router.navigate(['implement/detail-kegiatan/' + this.detailIdKegiatan])
    } else {
      this.router.navigate(['activity/event'])
    }
  }

  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.serviceAwp.listModa().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */


  /** * Work Unit */

  changeSelectedWorkUnit() {
    this.selectedUnitExist = null
    this.listComponentStaff = this.componentExist
    this.selectedProvinceStaff = null
  }

  changeUserStaffPcu() {
    this.dataResources()
  }

  getListWorkUnit() {
    this.startLoading()
    this.serviceProfile.listUnit().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listUnit = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * List Province */

  listProvinceStaff() {
    let tag = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAllStaff = resp.data.content
            this.listProvinceAllMasterStaff = resp.data.content
            this.listProvinceAllStaff.forEach(x => {
              tag.push(x.name)
            })
            this.listProvinceApi = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End List Province */
  /** * End Work Unit */


  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  openModalKegiatan(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'xl', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  checkValidationAddStaff() {
    if (this.selectedWorkUnit) {
      if (this.selectedWorkUnit === 'PMU') {
        return !this.selectedUnitExist
      } else if (this.selectedWorkUnit === 'PCU') {
        return !this.selectedProvinceStaff
      }
    } else {
      return true
    }
  }

  /** Add New Unit*/

  selectedUnit(data) {
    this.selectedUnitExist = null
    this.listComponentStaff = this.componentExist

    this.selectedUnitExist = data
    this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedUnitExist.id)
    this.dataResources()
  }

  searchFilterUnit(e) {
    const searchStr = e.target.value
    let filter = this.componentExist
    if (this.selectedUnitExist) {
      filter = filter.filter(x => x.id !== this.selectedUnitExist.id)
    } else {
      filter = this.componentExist
    }

    this.listComponentStaff = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  listComponent() {
    this.service.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff = resp.data.content
            this.listComponentStaff.forEach(dataCode => {
              dataCode.code = 'Komponen ' + dataCode.code
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * POK Code */
  listPokEvent() {
    this.startLoading()
    this.service.listCodePok().subscribe(
        resp => {
          if (resp.success) {
            this.listCodePok = resp.data.content
            this.listCodePokMaster = resp.data.content
            this.searchPok = resp.data.content
            if (this.pokRequest.length > 0) {
              this.pokRequest.forEach(dataPokExist => {
                this.listCodePok = this.listCodePok.filter(x => x.id !== dataPokExist?.id)
                this.searchPok = this.listCodePok.filter(x => x.id !== dataPokExist?.id)
              })
            }
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addPokSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pokRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePok.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePok.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPok.push(data)
        }
        this.pokRequest.push(data)
      }
    } else {
      this.pokRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pokRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPok.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPok.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePok.push(data)
      }
    }
    this.listCodePok = this.listCodePokMaster

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.id)
      })
    }

    this.pokRequest.forEach(dataProvince => {
      this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPok = this.listCodePok
  }

  searchFilterPok(e) {
    const searchStr = e.target.value
    this.listCodePok = this.searchPok.filter((product) => {
      return (product.code.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.title.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePokExisting(data) {
    this.pokExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.pokExisting.splice(index, 1)
      }
    })
    this.listCodePok = this.listCodePokMaster
    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.pok?.id)
      })
      this.pokRequest.forEach(dataPok => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataPok.id)
      })
    } else {
      this.pokRequest.forEach(dataProvince => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPok = this.listCodePok
  }


  testing() {
    console.log(this.pokExisting)
    console.log(this.pokRequest)
  }
  /** * End POK Code */

  /** * List Project Officer */
  dataResources() {
    let selectedWorkUnitExist
    let params
    let selectedProvincePcu

    if (this.selectedWorkUnit === 'PMU') {
      selectedWorkUnitExist = [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.selectedUnitExist.id
        }
      ]
    } else if (this.selectedWorkUnit === 'PCU') {
      selectedProvincePcu = this.listProvinceAllStaff.filter(x => x.name === this.selectedProvinceStaff)
      selectedWorkUnitExist = [
        {
          field: "user.provinceId",
          dataType: "string",
          value: selectedProvincePcu[0].id
        }
      ]
    }

    this.loadingDataProjectOfficer = true
    params = {
      enablePage: false,
      paramIs: selectedWorkUnitExist,
      paramIn: [],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.service.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDataProjectOfficer = false
            this.listResources = resp.data.content
            this.listResourcesMaster = resp.data.content
            this.searchResources = resp.data.content
            const result = this.listResources.filter(({ id: id1 }) => !this.resourcesRequest.some(({ id: id2 }) => id2 === id1));
            this.listResources = result
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addCreateCnToParticipant() {
    let selectedWorkUnitParticipant = []
    let params

    console.log(this.userComponentLogin)
    if (this.selectedWorkUnit === 'PMU') {
      selectedWorkUnitParticipant = [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.userComponentLogin
        }
      ]
    }
    params = {
      enablePage: false,
      paramIs: selectedWorkUnitParticipant,
      paramIn: [],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.service.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.listResources = resp.data.content
            let userLogin = this.listResources.filter(x => x.user?.id === this.userIdLogin)
            if (userLogin.length > 0) {
              this.listMeqrPmuSelected.push(userLogin[0])
              this.resourcesRequest.push(userLogin[0])
            }
            this.listResources = []
            this.listContactPersonMaster = Object.assign([], this.resourcesRequest)
            this.listContactPerson = Object.assign([], this.resourcesRequest)
            this.selectedWorkUnit = null
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  /** Add New Agenda*/

  changeTime() {
    if (this.endTime.hour == this.startTime.hour) {
      if (this.endTime.minute > this.startTime.minute) {
        this.warningTimeOver = false
      } else {
        this.warningTimeOver = true
        this.warningTimeOverText = 'Waktu akhir melebihi waktu awal'
      }
    } else if (this.endTime.hour > this.startTime.hour) {
      this.warningTimeOver = false
    } else if (this.startTime.hour > this.endTime.hour) {
      this.warningTimeOver = true
      this.warningTimeOverText = 'Waktu awal melebihi waktu akhir'
    } else {
      this.warningTimeOver = true
      this.warningTimeOverText = 'Waktu akhir melebihi waktu awal'
    }
  }

  addNewAgendas() {
    this.acceptAddEvent = 0
    if (this.eventAgenda.length > 0) {
      let startTimeSelected = parse(this.startTime.hour.toString() + ':' + this.startTime.minute.toString(), 'H:m', new Date())
      let endTimeSelected = parse(this.endTime.hour.toString() + ':' + this.endTime.minute.toString(), 'H:m', new Date())

      this.eventAgenda.forEach(data => {
        let startTimeExistConvert = parse(data.startTime.hour.toString() + ':' + data.startTime.minute.toString(), 'H:m', new Date())
        let endTimeExistConvert = parse(data.endTime.hour.toString() + ':' + data.endTime.minute.toString(), 'H:m', new Date())
        if (startTimeSelected < startTimeExistConvert || startTimeSelected > endTimeExistConvert) {
          if (endTimeSelected < startTimeExistConvert || endTimeSelected > endTimeExistConvert) {
            if ((startTimeExistConvert > startTimeSelected && startTimeExistConvert < endTimeSelected)) {
              this.acceptAddEvent = this.acceptAddEvent + 1
              this.warningTimeOver = true
              this.warningTimeOverText = 'Terdapat Kegiatan diantara waktu yang dipilih'
            } else {
              return
            }
          }else {
            this.acceptAddEvent = this.acceptAddEvent + 1
            this.warningTimeOver = true
            this.warningTimeOverText = 'Waktu akhir berada diantara waktu yang sudah ada'
          }
        } else {
          this.acceptAddEvent = this.acceptAddEvent + 1
          this.warningTimeOver = true
          this.warningTimeOverText = 'Waktu mulai berada diantara waktu yang sudah ada'
        }
      })
      if (this.acceptAddEvent == 0) {
        this.eventAgenda.push({
          startTime: this.startTime,
          endTime: this.endTime,
          activity: this.descriptionNewEvent,
          pic: this.penanggungJawab
        })
        this.startTime = {hour: 0, minute: 0};
        this.endTime = {hour: 0, minute: 0};
        this.descriptionNewEvent = null
        this.penanggungJawab = null
        this.warningTimeOver = false
      }
    } else {
      this.eventAgenda.push({
        startTime: this.startTime,
        endTime: this.endTime,
        activity: this.descriptionNewEvent,
        pic: this.penanggungJawab
      })
      this.startTime = {hour: 0, minute: 0};
      this.endTime = {hour: 0, minute: 0};
      this.descriptionNewEvent = null
      this.penanggungJawab = null
    }
  }
  /** End Of new Agenda*/


  /** Add New Staff Outside */
  addNewStaffOutside() {
    this.otherStaff.push({
      name: this.nameNewStaff,
      position: this.namePosition,
      institution: this.nameInstitution,
    })
    this.closeModal()
    this.nameNewStaff = null
    this.namePosition = null
    this.nameInstitution = null
  }

  deleteNewStaffOutside(data) {
    this.otherStaff.forEach((check, index) => {
      if (check.name === data.name) {
        this.otherStaff.splice(index, 1)
      }
    })
  }
  /** End Add New Staff Outside */



  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  changeEventType() {
    const x = this.listManagementType.find(data => data.id === this.selectedValueEventType.id)
    this.selectedEventTypeIsPenyedia = x.name === 'Kontrak';
  }

  addResourceSelectedMeqr() {
    this.listMeqrPmuSelected = []
    this.listMeqrPcuSelected = []
    this.closeModal()
    this.listMeqrResourceSelected = Object.assign([], this.resourcesRequest)
    this.listContactPersonMaster = Object.assign([], this.resourcesRequest)
    this.listContactPerson = Object.assign([], this.resourcesRequest)
    this.selectedContactPerson = null
    this.listMeqrResourceSelected.forEach(x => {
      if (x.user?.component?.code !== '-') {
        this.listMeqrPmuSelected.push(x)
      } else if (x.user?.component?.code === '-') {
        this.listMeqrPcuSelected.push(x)
      }
    })
  }

  getDetailKegiatan() {
    this.startLoading()
    this.service.detailKegiatan(this.detailIdKegiatan).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataDetailKegiatan = resp.data
            this.editNamaKegiatan = this.dataDetailKegiatan.name
            this.editBudgetAwp = this.dataDetailKegiatan.budgetAwp
            this.editTujuanKegiatan = this.dataDetailKegiatan.purpose
            this.editDeskripsiKegiatan = this.dataDetailKegiatan.description
            this.selectedValueEventType = this.dataDetailKegiatan.eventManagementType
            this.editjumlahNasrasumber = this.dataDetailKegiatan.nsCount
            this.editAsalLembagaNarasumber = this.dataDetailKegiatan.nsInstitution
            this.editInformasiLainNarasumber = this.dataDetailKegiatan.nsOtherInformation
            this.editTotalPeserta = this.dataDetailKegiatan.participantCount
            this.editInformasiPeserta = this.dataDetailKegiatan.participantOtherInformation
            this.editOutputKegiatan = this.dataDetailKegiatan.eventOutput
            this.editVolumeEvent = this.dataDetailKegiatan?.eventVolume
            this.nomorKontrakLsp = this.dataDetailKegiatan?.lspContractNumber
            this.dataKontrakLsp = this.dataDetailKegiatan?.lspContractData
            this.picLsp = this.dataDetailKegiatan?.lspPic
            this.selectedModaActivity = this.dataDetailKegiatan?.activityMode
            // this.date = parse(this.dataDetailKegiatan.year.toString(), 'yyyy', new Date());
            this.selectedComponentExist = this.dataDetailKegiatan?.component
            this.selectedProjectOfficer = this.dataDetailKegiatan?.projectOfficerResources
            this.dataDetailKegiatan.convertStartDate = new Date(this.dataDetailKegiatan.startDate)
            this.dataDetailKegiatan.convertEndDate = new Date(this.dataDetailKegiatan.endDate)
            // this.existOnPok = this.dataDetailKegiatan?.pokEvent
            this.minEvent = new NgbDate(this.dataDetailKegiatan.convertStartDate.getFullYear(), this.dataDetailKegiatan.convertStartDate.getMonth() + 1, this.dataDetailKegiatan.convertStartDate.getDate())
            this.maxEvent = new NgbDate(this.dataDetailKegiatan.convertEndDate.getFullYear(), this.dataDetailKegiatan.convertEndDate.getMonth() + 1, this.dataDetailKegiatan.convertEndDate.getDate())
            this.selectedEvent = this.dataDetailKegiatan?.eventType
            this.asumsi = this.dataDetailKegiatan?.assumption
            this.descriptionRisk = this.dataDetailKegiatan?.descriptionRisk
            this.mitigasiResiko = this.dataDetailKegiatan?.mitigationRisk
            this.editBudgetPok = this.dataDetailKegiatan?.budgetPok
            this.pesertaLaki = this.dataDetailKegiatan?.tpMale
            this.nsLakiLaki = this.dataDetailKegiatan?.nsMale
            this.nsPerempuan = this.dataDetailKegiatan?.nsFemale
            this.pesertaLaki = this.dataDetailKegiatan?.participantMale
            this.pesertaPerempuan = this.dataDetailKegiatan?.participantFemale
            this.editWaktuInformasiLainnya = this.dataDetailKegiatan?.eventOtherInformation
            this.selectedProjectOfficer = this.dataDetailKegiatan?.projectOfficerResources
            this.dampak = this.dataDetailKegiatan?.impactRisk
            this.kemungkinanTerjadi = this.dataDetailKegiatan?.potentialRisk
            this.gejalaResiko = this.dataDetailKegiatan?.symptomsRisk
            this.tempatInformasiLainnya = this.dataDetailKegiatan?.locationOtherInformation
            this.targetParticipant = this.dataDetailKegiatan?.participantTarget
            if (this.dataDetailKegiatan.eventManagementType.name === 'Kontrak') {
              this.selectedEventTypeIsPenyedia = true
            }
            this.dataDetailKegiatan.pdo.forEach(x => {
              this.pdoExisting.push(x)
            })
            this.dataDetailKegiatan.iri.forEach(x => {
              this.iriExisting.push(x)
            })
            this.dataDetailKegiatan.provincesRegencies.forEach(x => {
              this.provinceAndRegenciesExist.push(x)
            })

            if (this.dataDetailKegiatan.pok.length > 0) {
              this.dataDetailKegiatan.pok.forEach(x => {
                this.pokRequest.push(x.pok)
              })
            }
            this.dataEvent()
            this.getListComponent()
            this.dataPDO()
            this.dataIri()
            this.dataProjectOfficer()
            this.listProvince()
            this.setTimeRangeEvent()
            this.listPokEvent()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  setTimeRangeEvent() {
    let currentDate = new Date()
    let newConvertDate = new NgbDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate())
    if (currentDate >= this.dataDetailKegiatan.convertStartDate && currentDate < this.dataDetailKegiatan?.convertEndDate) {
      this.minDate = {
        year: newConvertDate.year,
        month: newConvertDate.month,
        day: newConvertDate.day + 1
      };
      this.fromDate = new NgbDate(newConvertDate.year, newConvertDate.month, newConvertDate.day + 1)
    } else {
      this.minDate = {
        year: this.minEvent.year,
        month: this.minEvent.month,
        day: this.minEvent.day
      };
      this.fromDate = new NgbDate(this.minEvent.year, this.minEvent.month, this.minEvent.day)
    }

    this.maxDate = {
      year: this.maxEvent.year,
      month: this.maxEvent.month,
      day: this.maxEvent.day
    };
    this.toDate = new NgbDate(this.maxEvent.year, this.maxEvent.month, this.maxEvent.day)
  }

  /** * Resources (Peserta) */

  getAllResources() {
    this.service.resourceAll().subscribe(
        resp => {
          if (resp.success) {
            this.listResources = resp.data.content
            this.listResourcesMaster = resp.data.content
            this.searchResources = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addResourcesSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.resourcesRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removeResources.forEach((check, index) => {
          if (check.id === data.id) {
            this.removeResources.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addResources.push(data)
        }
        this.resourcesRequest.push(data)
      }
    } else {
      this.resourcesRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.resourcesRequest.splice(index, 1)
          this.listMeqrResourceSelected.splice(index, 1)
        }
      })
      this.listMeqrPcuSelected = []
      this.listMeqrPmuSelected = []
      this.listMeqrResourceSelected.forEach(x => {
        if (x.user?.component?.code !== '-') {
          this.listMeqrPmuSelected.push(x)
        } else if (x.user?.component?.code === '-') {
          this.listMeqrPcuSelected.push(x)
        }
      })

      let checkAddExist = false

      this.addResources.forEach((check, index) => {
        if (check.id === data.id) {
          this.addResources.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removeResources.push(data)
      }
    }
    this.listResources = this.listResourcesMaster

    this.resourcesRequest.forEach(dataProvince => {
      this.listResources = this.listResources.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })
    this.searchResources = this.listResources
    this.selectedContactPerson = null
  }

  searchFilterResources(e) {
    const searchStr = e.target.value
    this.listResources = this.searchResources.filter((product) => {
      return ((product.user.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.firstName !== null) || (product?.user?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.lastName !== null))
    });
  }
  /** * end Resources (Peserta) */


  calculateNs(event, from) {
    console.log(event.target.value)
    if (from == 1) {
      if (this.nsLakiLaki < this.editjumlahNasrasumber) {
        this.nsPerempuan = 0
        if (this.nsLakiLaki + this.nsPerempuan < this.editjumlahNasrasumber) {
          this.nsPerempuan = this.editjumlahNasrasumber - this.nsLakiLaki
        }
      } else {
        this.nsLakiLaki = this.editjumlahNasrasumber
        this.nsPerempuan = 0
      }
    } else {
      if (this.nsPerempuan < this.editjumlahNasrasumber) {
        this.nsLakiLaki = 0
        if (this.nsLakiLaki + this.nsPerempuan < this.editjumlahNasrasumber) {
          this.nsLakiLaki = this.editjumlahNasrasumber - this.nsPerempuan
        }
      } else {
        this.nsPerempuan = this.editjumlahNasrasumber
        this.nsLakiLaki = 0
      }
    }
  }

  calculatePs(event, from) {
    if (this.participantFemale || this.participantMale) {
      if (from == 1) {
        if (this.participantMale < this.participantCount) {
          this.participantFemale = 0
          if (this.participantMale + this.participantFemale < this.participantCount) {
            this.participantFemale = this.participantCount - this.participantMale
          }
        } else {
          this.participantMale = this.participantCount
          this.participantFemale = 0
        }
      } else {
        if (this.participantFemale < this.participantCount) {
          this.participantMale = 0
          if (this.participantMale + this.participantFemale < this.participantCount) {
            this.participantMale = this.participantCount - this.participantFemale
          }
        } else {
          this.participantFemale = this.participantCount
          this.participantMale = 0
        }
      }
    }
  }

  /** * list component & selected component */
  getListComponent() {
    this.startLoading()
    this.serviceComponent.listComponent(this.dataDetailKegiatan?.awp?.year).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.component = resp.data
            this.componentMaster = resp.data
            let componentMasterEdit =JSON.parse(JSON.stringify(this.componentMaster))
            this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)

            let dataSubComponent = componentMasterEdit.find(dataFilter => dataFilter.id == this.selectedComponentExist.id)
            this.selectedSubComponentExistMaster = dataSubComponent
            this.selectedSubComponentExistEdit = Object.assign({}, dataSubComponent)
            if (this.dataDetailKegiatan?.subComponent) {
              this.selectedSubComponentExist = dataSubComponent.subComponent.find(data => data.id == this.dataDetailKegiatan?.subComponent.id)
              this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
            }

            this.selectedSubSubComponentExistMaster = this.selectedSubComponentExist
            this.selectedSubSubComponentExistEdit = Object.assign({}, this.selectedSubComponentExist)
            if (this.dataDetailKegiatan?.subSubComponent) {
              this.selectedSubSubComponentExist = this.selectedSubComponentExist.subComponent.find(data => data.id == this.dataDetailKegiatan?.subSubComponent.id)
              this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubSubComponentExist.id)
            }
            this.codeSelectedFromComponent()
            this.dataProjectOfficer()
          } else {
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null
    this.component = this.componentMaster

    this.selectedComponentExist = data
    this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)

    this.selectedSubComponentExistMaster = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExistEdit = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
    this.dataProjectOfficer()
  }

  searchFilterComponent(e) {
    const searchStr = e.target.value
    let filter = this.componentMaster
    if (this.selectedComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
    } else {
      filter = this.componentMaster
    }

    this.component = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedComponent() {
    this.codeSelected = null
    this.selectedProjectOfficer = null
    this.editNamaKegiatan = null
    this.component = this.componentMaster
    this.selectedComponentExist = null

    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null

    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.codeSelectedFromComponent()
  }

  // sub componen
  selectedSubComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent

    this.selectedSubComponentExist = data
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
    this.selectedSubSubComponentExistMaster = Object.assign({}, this.selectedSubComponentExist)
    this.selectedSubSubComponentExistEdit = Object.assign({}, this.selectedSubComponentExist)
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
  }

  searchFilterSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubComponentExistEdit.subComponent
    if (this.selectedSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubComponentExist.id)
    } else {
      filter = this.selectedSubComponentExistEdit.subComponent
    }

    this.selectedSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedSubComponent() {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
  }

  // sub sub componen
  selectedSubSubComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent

    this.selectedSubSubComponentExist = data
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubSubComponentExist.id)
    this.codeSelectedFromComponent()
  }

  searchFilterSubSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubSubComponentExistEdit.subComponent
    if (this.selectedSubSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubSubComponentExist.id)
    } else {
      filter = this.selectedSubSubComponentExistEdit.subComponent
    }

    this.selectedSubSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedSubSubComponent() {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent
    this.codeSelectedFromComponent()
  }

  codeSelectedFromComponent() {
    let selectedCodeComponent
    let filterComponent
    this.codeSelected = 'pilih'
    this.listCodeSelected = []
    if (this.selectedSubSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubSubComponentExist))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      const x = this.listCodeSelected.filter(y => y.code === this.dataDetailKegiatan?.componentCode.code)
      if (x.length > 0) {
        this.codeSelected = this.dataDetailKegiatan?.componentCode?.code
        // this.editNamaKegiatan = this.dataDetailKegiatan?.componentCode?.description
      } else {
        this.codeSelected = 'pilih'
      }
    } else if (this.selectedSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      const x = this.listCodeSelected.filter(y => y.code === this.dataDetailKegiatan?.componentCode.code)
      if (x.length > 0) {
        this.codeSelected = this.dataDetailKegiatan?.componentCode.code.toString()
        // this.editNamaKegiatan = this.dataDetailKegiatan?.componentCode?.description
      } else {
        this.codeSelected = 'pilih'
      }
    } else if (this.selectedComponentExist) {
      filterComponent = this.componentMaster.find(data => data.id == this.selectedComponentExist.id)
      selectedCodeComponent = JSON.parse(JSON.stringify(filterComponent))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      const x = this.listCodeSelected.filter(y => y.code === this.dataDetailKegiatan?.componentCode.code)
      if (x.length > 0) {
        this.codeSelected = this.dataDetailKegiatan?.componentCode.code
        // this.editNamaKegiatan = this.dataDetailKegiatan?.componentCode?.description
      } else {
        this.codeSelected = 'pilih'
      }
    }
    console.log(this.codeSelected)
  }

  changeValueSelected() {
    if (this.codeSelected === 'pilih') {
      // this.editNamaKegiatan = ''
    } else {
      let x = this.listCodeSelected.find(y => y.code === this.codeSelected)
      // this.editNamaKegiatan = x.description
    }
  }

  /** * Management Type */

  managementType() {
    this.startLoading()
    this.service.eventManagementType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listManagementType = resp.data.content
            this.listManagementTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedManagementType(data) {
    this.selectedMangementTypeExist = null
    this.listManagementType = this.listManagementTypeMaster

    this.selectedMangementTypeExist = data
    this.listManagementType = this.listManagementType.filter(dataFilter => dataFilter.id != this.selectedMangementTypeExist.id)
  }

  testingOnly() {
    console.log(this.uploadFileExcel)
  }

  /** * List Province */

  listProvince() {
    let dataProvinceExist = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            this.listProvinceExist = resp.data.content
            this.dataDetailKegiatan?.provincesRegencies.forEach(dataProvince => {
              let dataExist = this.listProvinceAll.find(x => dataProvince.province.id == x.id)
              if (dataExist) {
                dataProvinceExist.push(dataExist)
              }
            })
            this.listProvinceExist = dataProvinceExist
            this.listProvinceExistMaster = dataProvinceExist
            console.log(this.listProvinceExist)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedProvinceExist = null
    this.listProvinceExist = this.listProvinceExistMaster

    this.selectedProvinceExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      this.listProvinceExist = []
    }
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    console.log(searchStr)
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    if (searchStr == '') {
      this.listProvinceExist = this.listProvinceExistMaster
      this.listProvinceExist = this.listProvinceExist.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    } else {
      return this.listProvinceExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  /** * List City */

  listCity() {
    let cityExistFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.serviceAwp.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            this.listCityAllExist = resp.data.content
            let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
            if (checkProvinceExist.length > 0) {
              this.dataDetailKegiatan?.provincesRegencies.forEach(dataProvince => {
                if (dataProvince.regencies.length > 0) {
                  dataProvince.regencies.forEach(dataCity => {
                    let cityExist = this.listCityAll.find(x => dataCity.regency?.id == x.id)
                    if (cityExist) {
                      cityExistFromApi.push(cityExist)
                    }
                  })
                }
              })
              this.listCityAllExist = cityExistFromApi
              this.listCityAllMasterExist = JSON.parse(JSON.stringify(this.listCityAllExist))
            } else {
              this.listCityAllExist = this.listCityAll
            }

            this.listCityAllMaster = resp.data.content
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAllExist = this.listCityAllMasterExist

    this.selectedCityExist = data
    let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
    if (checkProvinceExist.length > 0) {
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    } else {
      this.listCityAllExist = this.listCityAll
      this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    }
  }

  searchFilterCity(e) {
    const searchStr = e.target.value
    let filter = this.listCityAll
    if (this.selectedCityExist) {
      filter = filter.filter(x => x.id !== this.selectedCityExist.id)
    } else {
      filter = this.listCityAll
    }

    if (searchStr == '') {
      let checkProvinceExist = this.listProvinceExistMaster.filter(y => y.id === this.selectedProvinceExist.id)
      if (checkProvinceExist.length > 0) {
        this.listCityAllExist = this.listCityAllMasterExist
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      } else {
        this.listCityAllExist = this.listCityAll
        this.listCityAllExist = this.listCityAllExist.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
      }
    } else {
      return this.listCityAllExist = filter.filter((product) => product.name.toLowerCase() === searchStr.toLowerCase());
    }
  }

  addSelectedProvinceAndCity() {
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityExist
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.listCityAllExist = this.listCityAllMasterExist
    this.listProvinceExist = this.listProvinceExistMaster
  }

  removeProvinceRegenciesExist(data) {
    this.provinceAndRegenciesExist.forEach((check, index ) => {
      if (check.id === data.id) {
        this.provinceAndRegenciesExist.splice(index, 1)
      }
    })
    this.listCityFromApi = []
  }

  /** * List Project Officer */
  dataProjectOfficer() {
    this.loadingDataProjectOfficer = true
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.selectedComponentExist.id
        }
      ],
      paramIn: [
        {
          field: "roleId",
          dataType: "string",
          value: [
            "f51e35a2-91f8-11ec-8ad1-ef8b05e5801f"
          ]
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.service.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDataProjectOfficer = false
            this.listProjectOfficer = resp.data.content
            this.listProjectOfficerMaster = resp.data.content
            if (this.listProjectOfficer.length > 0) {
              this.listProjectOfficer = this.listProjectOfficer.filter(dataFilter => dataFilter.id != this.selectedProjectOfficer.id)
            }
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectProjectOfficer(data) {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster

    this.selectedProjectOfficer = data
    this.listProjectOfficer = this.listProjectOfficer.filter(dataFilter => dataFilter.id != this.selectedProjectOfficer.id)
  }

  removeProjectOfficer() {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster
  }

  searchFilterProjectOfficer(e) {
    const searchStr = e.target.value
    let filter = this.listProjectOfficerMaster
    if (this.selectedProjectOfficer) {
      filter = filter.filter(x => x.id !== this.selectedProjectOfficer.id)
    } else {
      filter = this.listProjectOfficerMaster
    }

    this.listProjectOfficer = filter.filter((product) => {
      return ((product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  /** * end date */

  /** * list holidays */
  getListHolidayManagement() {
    this.listHolidays = []
    this.serviceAdministration.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * end list holidays */

  /** * List Event */
  dataEvent() {
    this.startLoading()
    this.serviceAwp.eventType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEvent = resp.data.content
            this.listEventMaster = resp.data.content
            this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent.id)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedEventType(data) {
    this.selectedEvent = null
    this.listEvent = this.listEventMaster

    this.selectedEvent = data
    this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent.id)
  }

  searchFilterEvent(e) {
    const searchStr = e.target.value
    let filter = this.listEventMaster
    if (this.selectedEvent) {
      filter = filter.filter(x => x.id !== this.selectedEvent.id)
    } else {
      filter = this.listEventMaster
    }

    this.listEvent = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * Narahubung */
  selectedContactPersonExist(data) {
    this.selectedContactPerson = null
    this.listContactPerson = this.listContactPersonMaster

    this.selectedContactPerson = data
    this.listContactPerson = this.listContactPerson.filter(dataFilter => dataFilter.id != this.selectedContactPerson.id)
  }

  searchFilterContactPerson(e) {
    const searchStr = e.target.value
    let filter = this.listContactPersonMaster
    if (this.selectedContactPerson) {
      filter = filter.filter(x => x.id !== this.selectedContactPerson.id)
    } else {
      filter = this.listContactPersonMaster
    }

    this.listContactPerson = filter.filter((product) => {
      return ((product.user.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.firstName !== null) || (product?.user?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product.user.lastName !== null))
    });
  }
  /** * End Naraubung */



  /** * List PDO */

  dataPDO() {
    this.serviceAwp.pdo().subscribe(
        resp => {
          if (resp.success) {
            this.listPdoAll = resp.data.content
            this.listPdoAllMaster = resp.data.content
            this.searchPdo = resp.data.content
            if (this.pdoExisting.length > 0) {
              this.pdoExisting.forEach(dataPdoExist => {
                this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
                this.searchPdo = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
              })
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addPdoSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pdoRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePdo.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePdo.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPdo.push(data)
        }
        this.pdoRequest.push(data)
      }
    } else {
      this.pdoRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pdoRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPdo.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPdo.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePdo.push(data)
      }
    }
    this.listPdoAll = this.listPdoAllMaster

    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(dataPdoExist => {
        this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
      })
    }

    this.pdoRequest.forEach(dataProvince => {
      this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPdo = this.listPdoAll
  }

  searchFilterPdo(e) {
    const searchStr = e.target.value
    this.listPdoAll = this.searchPdo.filter((product) => {
      return (product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.description.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePdoExisting(data) {
    this.pdoExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.pdoExisting.splice(index, 1)
      }
    })
    this.listPdoAll = this.listPdoAllMaster
    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(dataPdoExist => {
        this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
      })
      this.pdoRequest.forEach(dataProvince => {
        this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    } else {
      this.pdoRequest.forEach(dataProvince => {
        this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPdo = this.listPdoAll
  }

  /** * List IRI */
  dataIri() {
    this.startLoading()
    this.serviceAwp.iri().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listIriAll = resp.data.content
            this.listIriAllMaster = resp.data.content
            this.searchIri = resp.data.content
            if (this.iriExisting.length > 0) {
              this.iriExisting.forEach(dataIriExist => {
                this.listIriAll = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
                this.searchIri = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
              })
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addIriSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.iriRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removeIri.forEach((check, index) => {
          if (check.id === data.id) {
            this.removeIri.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addIri.push(data)
        }
        this.iriRequest.push(data)
      }
    } else {
      this.iriRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.iriRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addIri.forEach((check, index) => {
        if (check.id === data.id) {
          this.addIri.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removeIri.push(data)
      }
    }
    this.listIriAll = this.listIriAllMaster

    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(dataIriExist => {
        this.listIriAll = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
      })
    }

    this.iriRequest.forEach(dataProvince => {
      this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })
    this.searchIri = this.listIriAll
  }

  searchFilterIri(e) {
    const searchStr = e.target.value
    this.listIriAll = this.searchIri.filter((product) => {
      return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
    });
  }

  removeIriExisting(data) {
    this.iriExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.iriExisting.splice(index, 1)
      }
    })
    this.listIriAll = this.listIriAllMaster
    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(dataPdoExist => {
        this.listIriAll = this.listIriAll.filter(x => x.id !== dataPdoExist?.iri?.id)
      })
      this.iriRequest.forEach(dataProvince => {
        this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    } else {
      this.iriRequest.forEach(dataProvince => {
        this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchIri = this.listIriAll
  }

  /** Upload File Rab */
  doUploadRab() {
    this.loadingSaveKegiatan = true
    if (this.uploadFileExcel.length > 0) {
      this.service.upload(this.uploadFileExcel[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdRab = resp.data[0].id
              this.doUploadKegiatan()
            } else {
              this.loadingSaveKegiatan = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.dataIdRab = null
      this.doUploadKegiatan()
    }
  }

  /** Upload File Kegiatan */
  doUploadKegiatan() {
    if (this.uploadFileExcelAgenda.length > 0) {
      this.service.uploadKegiatan(this.uploadFileExcelAgenda[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdKegiatan = resp.data[0].id
              this.createNewEvent()
            } else {
              this.loadingSaveKegiatan = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.dataIdKegiatan = null
      this.createNewEvent()
    }
  }

  createNewEvent() {
    let startDate
    let endDate
    let listStaff = []
    let RegencyId
    let pokRequestApi = []

    if (this.pokRequest.length > 0) {
      this.pokRequest.forEach(x => {
        pokRequestApi.push(x.id)
      })
    }

    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')

    this.resourcesRequest.forEach(data => {
      listStaff.push(data?.id)
    })

    if (this.selectedProvinceAndCity[0]?.regencyId) {
      RegencyId = this.selectedProvinceAndCity[0]?.regencyId.id
    } else {
      RegencyId = null
    }

    const params = {
      awpImplementationId: this.dataDetailKegiatan?.id,
      name: this.eventName,
      startDate: startDate,
      endDate: endDate,
      eventOtherInformation: this.editOtherInformationTime,
      eventTypeId: this.selectedEvent.id,
      budgetPok: this.editBudgetPok,
      // pokEvent: this.existOnPok,
      budgetAwp: this.dataDetailKegiatan?.budgetAwp,
      background: this.background,
      description: this.description,
      purpose: this.purpose,
      eventOutput: this.eventOutput,
      participantMale: this.participantMale,
      participantFemale: this.participantFemale,
      participantCount: this.participantCount,
      participantOtherInformation: this.participantOtherInformation,
      contactPerson: this.selectedContactPerson.id,
      provinceId: this.selectedProvinceAndCity[0].provinceId.id,
      regencyId: RegencyId,
      location: this.location,
      rabFileId: this.dataIdRab,
      agendaFileId: this.dataIdKegiatan,
      newStaff: listStaff,
      newOtherStaff: this.otherStaff,
      activityModeId: this.selectedModaActivity.id,
      locationOtherInformation: this.tempatInformasiLainnya,
      participantTarget: this.targetParticipant,
      newPokIds: pokRequestApi,
    }
    this.service.createCn(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveKegiatan = false
            this.router.navigate(['activity/event'])
          }
        }, error => {
          this.loadingSaveKegiatan = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
