import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddConceptNoteComponent } from './add-concept-note.component';

describe('AddConceptNoteComponent', () => {
  let component: AddConceptNoteComponent;
  let fixture: ComponentFixture<AddConceptNoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddConceptNoteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddConceptNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
