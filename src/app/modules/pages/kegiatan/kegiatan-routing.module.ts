import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {KegiatanComponent} from "./kegiatan.component";
import {DetailKegiatanComponent} from "./detail-kegiatan/detail-kegiatan.component";
import {AddKegiatanComponent} from "./add-kegiatan/add-kegiatan.component";
import {DetailPerencanaanComponent} from "./detail-perencanaan/detail-perencanaan.component";
import {AddConceptNoteComponent} from "./add-concept-note/add-concept-note.component";
import {CreateKegiatanComponent} from "./create-kegiatan/create-kegiatan.component";
import {DetailReportKegiatanComponent} from "./detail-report-kegiatan/detail-report-kegiatan.component";
import {EditKegiatanComponent} from "./edit-kegiatan/edit-kegiatan.component";
import {AuthGuard} from "../../../core/guards/auth.guard";

const routes: Routes = [
    {
        path: 'kegiatan',
        component: KegiatanComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-kegiatan/:id',
        component: DetailKegiatanComponent, canActivate: [AuthGuard]
    },
    {
        path: 'add-kegiatan/:id',
        component: AddKegiatanComponent, canActivate: [AuthGuard]
    },
    /** Add 'add-kegiatan' path */
    {
        path: 'create-kegiatan/:id',
        component: CreateKegiatanComponent, canActivate: [AuthGuard]
    },
    {
        path: 'edit-kegiatan/:id',
        component: EditKegiatanComponent
    },
    {
        path: 'detail-kegiatan-perencanaan/:id',
        component: DetailPerencanaanComponent, canActivate: [AuthGuard]
    },
    {
        path: 'add-concept-note/:id/:from',
        component: AddConceptNoteComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-report-activity/:id',
        component: DetailReportKegiatanComponent, canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class KegiatanRoutingModule {}
