import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {KegiatanService} from "../../../../../core/services/kegiatan.service";
import {FileService} from "../../../../../core/services/Image/file.service";
import Swal from "sweetalert2";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() index: any;
  @Input() listRAB: any;
  @Input() type: any;
  @Output() event: EventEmitter<any> = new EventEmitter();

  uploadFileExcel: Array<File> = [];
  loadingSaveKegiatan = false
  dataIdRab: string = null

  constructor(public modal: NgbActiveModal,
              private service: KegiatanService,
              private modalService: NgbModal,
              private serviceImage: FileService,
              private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    console.log(this.listRAB);
  }

  doUploadRab(e) {
    console.log(e)
    this.loadingSaveKegiatan = true
    let newRAB = {};
    if (this.uploadFileExcel.length > 0) {
      this.service.uploadKegiatanRaa(this.uploadFileExcel[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdRab = resp.data[0].id
              newRAB = {
                eventReportId: this.listRAB.id,
                type: this.type,
                id: this.dataIdRab,
                filename: e[0].name,
                index: this.index
              }
              console.log(newRAB);
              this.event.emit(newRAB);
              this.loadingSaveKegiatan = false
              this.modal.dismiss();
            } else {
              this.loadingSaveKegiatan = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveKegiatan = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    }
  }

  // showFileInfo(e, index){
  //   console.log(e);
  //   // console.log(this.listRAB);
  //   let newRAB = {};
  //   if (this.type === 'rab') {
  //     newRAB = {
  //       type: this.type,
  //       index,
  //       no: this.listRAB.no,
  //       name: this.listRAB.name,
  //       rab: e[0].name,
  //       anggaran: this.listRAB.anggaran
  //     }
  //   } else {
  //     newRAB = {
  //       type: this.type,
  //       index,
  //       no: this.listRAB.no,
  //       name: this.listRAB.name,
  //       rab: this.listRAB.rab,
  //       anggaran: e[0].name
  //     }
  //   }
  //   // this.listRAB[index].anggaran = e[0].name;
  //   console.log(newRAB);
  //   this.event.emit(newRAB);
  //   this.modal.dismiss();
  // }

}
