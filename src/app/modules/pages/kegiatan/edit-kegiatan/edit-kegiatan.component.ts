import { Component, OnInit } from '@angular/core';
import {ReportEventModel} from "../../../../core/models/report-event.model";
import {Event, Kegiatan, MangementType, PokCode, Province} from "../../../../core/models/awp.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {resourcesModel} from "../../../../core/models/resources.model";
import {PDOModel} from "../../../../core/models/PDO.model";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute, Router} from "@angular/router";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {ComponentService} from "../../../../core/services/Component/component.service";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {parse} from "date-fns";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import Swal from "sweetalert2";
import {ModalComponent} from "../create-kegiatan/modal/modal.component";
import {ActivityReport} from "../../../../core/models/ActivityReport";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {Monev, Monev2} from "../../../../core/models/monev";

@Component({
  selector: 'app-edit-kegiatan',
  templateUrl: './edit-kegiatan.component.html',
  styleUrls: ['./edit-kegiatan.component.scss']
})
export class EditKegiatanComponent implements OnInit {
  dataAllEvent: ReportEventModel[]
  detailIdKegiatan: string
  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  dataDetailKegiatan: ActivityReport
  dataIdRab: string = null
  firstNameLogged: string
  lastNameLogged: string
  fieldSubstantiv: number
  listSelectedEvent: ReportEventModel
  fromDateIntroduction: Date
  toDateIntroduction: Date
  totalAllRealitationEvent: number
  percentageAllRealitationEvent: number

  totalAllParticipantEvent: number
  percentageAllParticipantEvent: number

  totalAllParticipantMaleEvent: number
  percentageAllParticipantMaleEvent: number

  totalAllParticipantFemaleEvent: number
  percentageAllParticipantFemaleEvent: number
  loadingCreateKegiatan = false

  selectedDocumentationEvent: ReportEventModel

  public Description = ClassicEditor;
  public Purpose = ClassicEditor;
  public OutputEvent = ClassicEditor;
  public Asumtion = ClassicEditor;
  public RiskAsumtion = ClassicEditor;
  public summaryActivity = ClassicEditor;
  public conclusionActivity = ClassicEditor
  public OtherInfoAwp = ClassicEditor;
  public participantTarget = ClassicEditor;

  // POK
  listCodePok: PokCode[]
  listCodePokMaster: PokCode[]
  pokRequest = []
  addPok = []
  removePok = []
  searchPok = []
  pokExisting = []
  deletedPokExisting = []


  // upload file
  uploadFileExcel: Array<File> = [];
  uploadFileExcelIds: Array<File> = [];

  // data update
  activitySummary: string = null
  activityConclusion: string = null
  projectName: string
  date: Date
  editNamaKegiatan: string
  editBudgetAwp: number
  editBudgetPok: number
  editTujuanKegiatan: string
  editDeskripsiKegiatan: string
  editWaktuInformasiLainnya: string
  editjumlahNasrasumber: number
  editAsalLembagaNarasumber: string
  editInformasiLainNarasumber: string
  editTotalPeserta: number
  editInformasiPeserta: string
  editOutputKegiatan: string
  editVolumeEvent: number
  selectedValueEventType: any
  existOnPok: boolean
  nomorKontrakLsp: string
  dataKontrakLsp: string
  picLsp: string
  asumsi: string
  descriptionRisk: string
  mitigasiResiko: string
  volumeKomponenPembiayaan: string
  hargaSatuanKomponenPembiayaan: string
  jumlahKomponenPembiayaan: string
  pesertaLaki: number
  pesertaPerempuan: number
  kemungkinanTerjadi: number;
  dampak: number;
  nsLakiLaki: number
  nsPerempuan: number
  gejalaResiko: string
  loadingSaveKegiatan = false
  yearCreated;
  tempatInformasiLainnya: string
  otherInformationAwp: string = null
  targetParticipant: string = null

  //componen
  selectedComponentExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]

  // sub componen
  selectedSubComponentExist: ComponentModel
  selectedSubComponentExistMaster: ComponentModel
  selectedSubComponentExistEdit: ComponentModel

  //sub-sub componen
  selectedSubSubComponentExist: ComponentModel
  selectedSubSubComponentExistMaster: ComponentModel
  selectedSubSubComponentExistEdit: ComponentModel
  codeSelected: any = 'pilih'
  listCodeSelected = []

  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // event
  listEvent: Event[]
  listEventMaster: Event[]
  selectedEvent: Event
  selectedEventTypeIsPenyedia: boolean

  // project officer
  listProjectOfficer: resourcesModel[]
  listProjectOfficerMaster: resourcesModel[]
  selectedProjectOfficer: resourcesModel
  loadingDataProjectOfficer = false

  // PDO
  listPdoAll: PDOModel[]
  listPdoAllMaster: PDOModel[]
  pdoRequest = []
  addPdo = []
  removePdo = []
  searchPdo = []
  pdoExisting = []

  // IRI
  listIriAll: PDOModel[]
  listIriAllMaster: PDOModel[]
  iriRequest = []
  addIri = []
  removeIri = []
  searchIri = []
  iriExisting = []

  // LSP
  listLSP: Event[]
  listLSPMaster: Event[]
  selectedLsp: Event

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []
  listCityFromApi: string[];
  selectedCityFromList = []

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province
  provinceAndRegenciesExist = []
  deletedProvinceRegenciesExist = []

  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  codeEvent: any

  // management type
  listManagementType: MangementType[]
  listManagementTypeMaster: MangementType[]
  selectedMangementTypeExist: MangementType

  //Moda Kegiatan
  listModaType: MangementType[] = []
  listModaTypeMaster: MangementType[] = []
  selectedModaActivity: any = {}

  dataRAB = [];
  rraEventReports = [];
  awpImplementationId = '';

  successIndicatorIds = []
  successIndicatorSelected = []

  // result chain code
  listChainCode: Monev2[]
  listChainCodeMaster: Monev2[]
  listChainCodeFilter: Monev2[]
  listChainCodeName: string[] = [];
  selectedChainCode: Monev2

  // indicator Success
  selectedIndicatorSuccess: string[] = []
  listIndicatorSuccessApi: string[];
  dataListIndicatorSuccess: Monev2[]
  listSelectedChainCodeAndIndicator = []
  listChainCodeAndIndicatorExist = []
  listMonevExisting = []
  createNewIndicatorSuccess: boolean = null

  // upload file
  uploadFileBast: Array<File> = [];
  idFileBast: string

  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private service: KegiatanService,
      private serviceComponent: ComponentService,
      private serviceAwp: AwpService,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private serviceAdministration: AdministrationService,
      private modalService: NgbModal,
      public roles: AuthfakeauthenticationService,
      private swalAlert: SwalAlert,
      private serviceResources: KegiatanService,
  ) {
  }

  ngOnInit(): void {
    this.firstNameLogged = localStorage.getItem('fullName')
    this.lastNameLogged = localStorage.getItem('lastName')
    this.detailIdKegiatan = this.activatedRoute.snapshot.paramMap.get('id')
    this.breadCrumbItems = [{ label: 'Kegiatan' }, { label: 'Detail Kegiatan'}, { label: 'Detail Laporan Kegiatan'}, { label: 'Edit Laporan Kegiatan', active: true }]
    this.projectName = 'MEQR'
    this.listAllModa()
    this.getDetailReportKegiatan()
    this.managementType()
    this.getListHolidayManagement()
    this.listProvince()
    this.listPokEvent()
    this.fieldSubstantiv = 1
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  back(){
    history.back()
  }

  setTimeRangeEvent() {
    const current = new Date();
    this.minDate = {
      year: this.date.getFullYear(),
      month: 1,
      day: 1
    };
    this.maxDate = {
      year: this.date.getFullYear(),
      month: 12,
      day: 31
    };
  }

  goToListKegiatan() {
    this.router.navigate(['implement/detail-kegiatan/' + this.detailIdKegiatan])
    // if (this.dataDetailKegiatan.status == 0) {
    //   this.router.navigate(['implement/detail-kegiatan/' + this.detailIdKegiatan])
    // } else {
    //   this.router.navigate(['implement/detail-kegiatan-perencanaan/' + this.detailIdKegiatan])
    // }
  }

  downloadFile(id, name) {
    // this.loadingDownloadPdf = true
    this.service.exportFile(id, name).subscribe(
        resp => {
          // this.loadingDownloadPdf = false
        }, error => {
          // this.loadingDownloadPdf = false
        }
    )
  }

  openModalDocumentationEvent(data: ReportEventModel, selectedMenu: any) {
    this.selectedDocumentationEvent = data
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  openModalAddIndicator(selectedMenu: any, dataSelected: Monev, createNew: boolean) {
    this.selectedIndicatorSuccess = []
    this.selectedChainCode = null
    this.createNewIndicatorSuccess = createNew
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
    if (dataSelected) {
      this.selectedChainCodeFromList(dataSelected)
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  changeEventType() {
    console.log(this.listSelectedEvent?.id)
    const x = this.listManagementType.find(data => data.id === this.selectedValueEventType?.id)
    this.selectedEventTypeIsPenyedia = x.name === 'Kontrak';
    this.fromDateIntroduction = parse(this.listSelectedEvent?.startDate, 'yyyy-MM-dd', new Date());
    this.toDateIntroduction = parse(this.listSelectedEvent?.endDate, 'yyyy-MM-dd', new Date());
  }

  getDetailReportKegiatan() {
    this.startLoading()
    this.service.detailReportKegiatan(this.detailIdKegiatan).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataDetailKegiatan = resp.data
            this.awpImplementationId = resp.data.awpImplementationId
            this.targetParticipant = this.dataDetailKegiatan?.participantTarget
            this.editInformasiPeserta = this.dataDetailKegiatan.participantOtherInformation
            this.dataDetailKegiatan.convertStartDate = new Date(this.dataDetailKegiatan.startDate)
            this.dataDetailKegiatan.convertEndDate = new Date(this.dataDetailKegiatan.endDate)
            this.fromDate = new NgbDate(this.dataDetailKegiatan.convertStartDate.getFullYear(), this.dataDetailKegiatan.convertStartDate.getMonth() + 1, this.dataDetailKegiatan.convertStartDate.getDate())
            this.toDate = new NgbDate(this.dataDetailKegiatan.convertEndDate.getFullYear(), this.dataDetailKegiatan.convertEndDate.getMonth() + 1, this.dataDetailKegiatan.convertEndDate.getDate())
            this.editWaktuInformasiLainnya = this.dataDetailKegiatan?.eventOtherInformation
            this.tempatInformasiLainnya = this.dataDetailKegiatan?.locationOtherInformation
            this.selectedModaActivity = this.dataDetailKegiatan?.activityMode
            this.otherInformationAwp = this.dataDetailKegiatan?.nameOtherInformation
            this.activitySummary = this.dataDetailKegiatan?.summary
            this.activityConclusion = this.dataDetailKegiatan?.conclusionsAndRecommendations
            this.selectedComponentExist = this.dataDetailKegiatan?.awpImplementation?.component
            this.date = parse(this.dataDetailKegiatan?.awpImplementation?.awp.year.toString(), 'yyyy', new Date());
            this.editVolumeEvent = this.dataDetailKegiatan?.eventVolume
            if (this.dataDetailKegiatan?.awpImplementation?.eventManagementType?.name === 'Kontrak') {
              this.selectedEventTypeIsPenyedia = true
            }
            if (this.dataDetailKegiatan.pok.length > 0) {
              this.dataDetailKegiatan.pok.forEach(x => {
                this.pokExisting.push(x)
              })
            }
            this.dataDetailKegiatan.provincesRegencies.forEach(x => {
              this.provinceAndRegenciesExist.push(x)
            })

            this.dataDetailKegiatan?.monev.forEach(x => {
              x.successIndicator?.forEach(y => {
                y.existing = true
              })
              this.listMonevExisting.push(x)
              this.listChainCodeAndIndicatorExist.push(x)
              x.successIndicator = x.successIndicator.map(x => (
                  {
                    ...x,resultIndicator: ''
                  }
              ))
            })

            this.listMonevExisting.forEach(x => {
              x.successIndicator.forEach(y => {
                y.successIndicator.resultIndicator = y.contribution
              })
            })

            this.dataEvent()
            this.getListComponent()
            this.dataPDO()
            this.dataIri()
            this.dataProjectOfficer()
            this.setTimeRangeEvent()
            this.detailListKegiatanEvent()
            this.listResultChainCode()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  calculateNs(event, from) {
    if (this.nsLakiLaki || this.nsPerempuan) {
      if (from == 1) {
        if (this.nsLakiLaki < this.editjumlahNasrasumber) {
          this.nsPerempuan = 0
          if (this.nsLakiLaki + this.nsPerempuan < this.editjumlahNasrasumber) {
            this.nsPerempuan = this.editjumlahNasrasumber - this.nsLakiLaki
          }
        } else {
          this.nsLakiLaki = this.editjumlahNasrasumber
          this.nsPerempuan = 0
        }
      } else {
        if (this.nsPerempuan < this.editjumlahNasrasumber) {
          this.nsLakiLaki = 0
          if (this.nsLakiLaki + this.nsPerempuan < this.editjumlahNasrasumber) {
            this.nsLakiLaki = this.editjumlahNasrasumber - this.nsPerempuan
          }
        } else {
          this.nsPerempuan = this.editjumlahNasrasumber
          this.nsLakiLaki = 0
        }
      }
    }
  }

  calculatePs(event, from) {
    if (this.pesertaPerempuan || this.pesertaLaki) {
      if (from == 1) {
        if (this.pesertaLaki < this.editTotalPeserta) {
          this.pesertaPerempuan = 0
          if (this.pesertaLaki + this.pesertaPerempuan < this.editTotalPeserta) {
            this.pesertaPerempuan = this.editTotalPeserta - this.pesertaLaki
          }
        } else {
          this.pesertaLaki = this.editTotalPeserta
          this.pesertaPerempuan = 0
        }
      } else {
        if (this.pesertaPerempuan < this.editTotalPeserta) {
          this.pesertaLaki = 0
          if (this.pesertaLaki + this.pesertaPerempuan < this.editTotalPeserta) {
            this.pesertaLaki = this.editTotalPeserta - this.pesertaPerempuan
          }
        } else {
          this.pesertaPerempuan = this.editTotalPeserta
          this.pesertaLaki = 0
        }
      }
    }
  }

  /** * list component & selected component */
  getListComponent() {
    this.startLoading()
    this.serviceComponent.listComponent(this.dataDetailKegiatan?.awpImplementation?.awp?.year).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.component = resp.data
            this.componentMaster = resp.data
            let componentMasterEdit =JSON.parse(JSON.stringify(this.componentMaster))
            this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)

            let dataSubComponent = componentMasterEdit.find(dataFilter => dataFilter.id == this.selectedComponentExist.id)
            this.selectedSubComponentExistMaster = dataSubComponent
            this.selectedSubComponentExistEdit = Object.assign({}, dataSubComponent)
            if (this.dataDetailKegiatan?.awpImplementation?.subComponent) {
              this.selectedSubComponentExist = dataSubComponent.subComponent.find(data => data.id == this.dataDetailKegiatan?.awpImplementation?.subComponent.id)
              this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
            }

            this.selectedSubSubComponentExistMaster = this.selectedSubComponentExist
            this.selectedSubSubComponentExistEdit = Object.assign({}, this.selectedSubComponentExist)
            if (this.dataDetailKegiatan?.awpImplementation?.subSubComponent) {
              this.selectedSubSubComponentExist = this.selectedSubComponentExist.subComponent.find(data => data.id == this.dataDetailKegiatan?.awpImplementation?.subSubComponent.id)
              this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubSubComponentExist.id)
            }
            this.codeSelectedFromComponent()
            this.dataProjectOfficer()
          } else {
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null
    this.component = this.componentMaster

    this.selectedComponentExist = data
    this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)

    this.selectedSubComponentExistMaster = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExistEdit = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
    this.dataProjectOfficer()
  }

  searchFilterComponent(e) {
    const searchStr = e.target.value
    let filter = this.componentMaster
    if (this.selectedComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
    } else {
      filter = this.componentMaster
    }

    this.component = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedComponent() {
    this.codeSelected = null
    this.selectedProjectOfficer = null
    this.editNamaKegiatan = null
    this.component = this.componentMaster
    this.selectedComponentExist = null

    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null

    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.codeSelectedFromComponent()
  }

  // sub componen
  selectedSubComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent

    this.selectedSubComponentExist = data
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
    this.selectedSubSubComponentExistMaster = Object.assign({}, this.selectedSubComponentExist)
    this.selectedSubSubComponentExistEdit = Object.assign({}, this.selectedSubComponentExist)
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
  }

  searchFilterSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubComponentExistEdit.subComponent
    if (this.selectedSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubComponentExist.id)
    } else {
      filter = this.selectedSubComponentExistEdit.subComponent
    }

    this.selectedSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedSubComponent() {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
  }

  // sub sub componen
  selectedSubSubComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent

    this.selectedSubSubComponentExist = data
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubSubComponentExist.id)
    this.codeSelectedFromComponent()
  }

  searchFilterSubSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubSubComponentExistEdit.subComponent
    if (this.selectedSubSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubSubComponentExist.id)
    } else {
      filter = this.selectedSubSubComponentExistEdit.subComponent
    }

    this.selectedSubSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedSubSubComponent() {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent
    this.codeSelectedFromComponent()
  }

  codeSelectedFromComponent() {
    let selectedCodeComponent
    let filterComponent
    this.codeSelected = 'pilih'
    this.listCodeSelected = []
    if (this.selectedSubSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubSubComponentExist))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      const x = this.listCodeSelected.filter(y => y.code === this.dataDetailKegiatan?.awpImplementation?.componentCode.code)
      if (x.length > 0) {
        this.codeSelected = this.dataDetailKegiatan?.awpImplementation?.componentCode?.code
        // this.editNamaKegiatan = this.dataDetailKegiatan?.componentCode?.description
      } else {
        this.codeSelected = 'pilih'
      }
    } else if (this.selectedSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      const x = this.listCodeSelected.filter(y => y.code === this.dataDetailKegiatan?.awpImplementation?.componentCode.code)
      if (x.length > 0) {
        this.codeSelected = this.dataDetailKegiatan?.awpImplementation?.componentCode.code.toString()
        // this.editNamaKegiatan = this.dataDetailKegiatan?.componentCode?.description
      } else {
        this.codeSelected = 'pilih'
      }
    } else if (this.selectedComponentExist) {
      filterComponent = this.componentMaster.find(data => data.id == this.selectedComponentExist.id)
      selectedCodeComponent = JSON.parse(JSON.stringify(filterComponent))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      const x = this.listCodeSelected.filter(y => y.code === this.dataDetailKegiatan?.awpImplementation?.componentCode.code)
      if (x.length > 0) {
        this.codeSelected = this.dataDetailKegiatan?.awpImplementation?.componentCode.code
        // this.editNamaKegiatan = this.dataDetailKegiatan?.componentCode?.description
      } else {
        this.codeSelected = 'pilih'
      }
    }
  }

  changeValueSelected() {
    if (this.codeSelected === 'pilih') {
      // this.editNamaKegiatan = ''
    } else {
      let x = this.listCodeSelected.find(y => y.code === this.codeSelected)
      // this.editNamaKegiatan = x.description
    }
  }

  /** * Management Type */

  managementType() {
    this.startLoading()
    this.service.eventManagementType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listManagementType = resp.data.content
            this.listManagementTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedManagementType(data) {
    this.selectedMangementTypeExist = null
    this.listManagementType = this.listManagementTypeMaster

    this.selectedMangementTypeExist = data
    this.listManagementType = this.listManagementType.filter(dataFilter => dataFilter.id != this.selectedMangementTypeExist.id)
  }

  /** * List LSP */
  dataListLsp() {
    const params = {
      enablePage: false,
      page: 0,
      paramIs: [
        {
          field: "componentId",
          dataType: "string",
          value: this.dataDetailKegiatan.awpImplementation?.component.id
        }
      ],
      sort: [
        {
          field: "firstName",
          direction: "asc"
        },
        {
          field: "lastName",
          direction: "asc"
        }
      ]
    }
    this.startLoading()
    this.service.listLsp(params).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listLSP = resp.data.content
            this.listLSPMaster = resp.data.content
            this.listLSP = this.listLSP.filter(x => x.id !== this.selectedLsp.id)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedListLsp(data) {
    this.selectedLsp = null
    this.listLSP = this.listLSPMaster

    this.selectedLsp = data
    this.listLSP = this.listLSP.filter(dataFilter => dataFilter.id != this.selectedLsp.id)
  }

  searchFilterLsp(e) {
    const searchStr = e.target.value
    let filter = this.listLSPMaster
    if (this.selectedLsp) {
      filter = filter.filter(x => x.id !== this.selectedLsp.id)
    } else {
      filter = this.listLSPMaster
    }

    this.listLSP = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }


  testingOnly() {
    console.log(this.targetParticipant)
    console.log(this.otherInformationAwp)
  }

  /** * List Province */

  listProvince() {
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            // this.searchProvince = this.listProvinceAll
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster

    this.selectedProvinceExist = data
    this.listProvinceAll = this.listProvinceAll.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    this.listProvinceAll = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List City */

  listCity() {
    let cityNameFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.serviceAwp.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            // this.listCityAll = this.listCityAll.filter(x => this.selectedProvinceAndCity.filter(y => x.id !== y.regencyId.id))
            this.listCityAll.forEach(x => {
              cityNameFromApi.push(x.name)
            })

            if (this.selectedProvinceAndCity.length > 0) {
              this.selectedProvinceAndCity.forEach(x => {
                if (x.regencyId.length > 0) {
                  x.regencyId.forEach(y => {
                    cityNameFromApi = cityNameFromApi.filter(exist => exist !== y)
                  })
                }
              })
            }

            if (this.provinceAndRegenciesExist.length > 0) {
              this.provinceAndRegenciesExist.forEach(x => {
                if (x.regencies.length > 0) {
                  x.regencies.forEach(dataRegencie => {
                    cityNameFromApi = cityNameFromApi.filter(y => dataRegencie.regency.name !== y)
                  })
                }
              })
            }
            this.listCityFromApi = cityNameFromApi

            // if (this.selectedProvinceAndCity.length > 0) {
            //   this.selectedProvinceAndCity.forEach(x => {
            //     if (x.regencyId != null) {
            //       this.listCityAll = this.listCityAll.filter(y => y.id !== x.regencyId.id)
            //     }
            //   })
            // }
            this.listCityAllMaster = resp.data.content
            // this.searchProvince = this.listProvinceAll
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAll = this.listCityAllMaster

    this.selectedCityExist = data
    this.listCityAll = this.listCityAll.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    if (this.selectedProvinceAndCity.length > 0) {
      this.selectedProvinceAndCity.forEach(x => {
        if (x.regencyId != null) {
          this.listCityAll = this.listCityAll.filter(y => y.id !== x.regencyId.id)
        }
      })
    }
  }

  addSelectedProvinceAndCity() {
    this.selectedCityFromList.forEach(x => {
      let y = this.listCityAll.find(dataSelected => dataSelected.name === x)
      if (y) {
        this.dataAllCitySelected.push(y)
      }
    })
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityFromList
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.listCityFromApi = []
  }

  removeProvinceRegenciesExist(data) {
    this.provinceAndRegenciesExist.forEach((check, index ) => {
      if (check.id === data.id) {
        this.deletedProvinceRegenciesExist.push(data?.id)
        this.provinceAndRegenciesExist.splice(index, 1)
      }
    })
    this.listCityFromApi = []
  }

  /** * List Project Officer */
  dataProjectOfficer() {
    this.loadingDataProjectOfficer = true
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "user.componentId",
          dataType: "string",
          value: this.selectedComponentExist.id
        }
      ],
      paramIn: [
        {
          field: "roleId",
          dataType: "string",
          value: [
            "f51e35a2-91f8-11ec-8ad1-ef8b05e5801f"
          ]
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "asc"
        }
      ]
    }
    this.service.projectOfficer(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDataProjectOfficer = false
            this.listProjectOfficer = resp.data.content
            this.listProjectOfficerMaster = resp.data.content
            if (this.listProjectOfficer.length > 0) {
              this.listProjectOfficer = this.listProjectOfficer?.filter(dataFilter => dataFilter.id != this.selectedProjectOfficer?.id)
            }
          }
        }, error => {
          this.loadingDataProjectOfficer = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectProjectOfficer(data) {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster

    this.selectedProjectOfficer = data
    this.listProjectOfficer = this.listProjectOfficer.filter(dataFilter => dataFilter.id != this.selectedProjectOfficer.id)
  }

  removeProjectOfficer() {
    this.selectedProjectOfficer = null
    this.listProjectOfficer = this.listProjectOfficerMaster
  }

  searchFilterProjectOfficer(e) {
    const searchStr = e.target.value
    let filter = this.listProjectOfficerMaster
    if (this.selectedProjectOfficer) {
      filter = filter.filter(x => x.id !== this.selectedProjectOfficer.id)
    } else {
      filter = this.listProjectOfficerMaster
    }

    this.listProjectOfficer = filter.filter((product) => {
      return ((product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  /** * end date */

  /** * list holidays */
  getListHolidayManagement() {
    this.listHolidays = []
    this.serviceAdministration.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * end list holidays */

  /** * List Event */
  dataEvent() {
    this.startLoading()
    this.serviceAwp.eventType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEvent = resp.data.content
            this.listEventMaster = resp.data.content
            this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent?.id)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedEventType(data) {
    this.selectedEvent = null
    this.listEvent = this.listEventMaster

    this.selectedEvent = data
    this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent?.id)
  }

  searchFilterEvent(e) {
    const searchStr = e.target.value
    let filter = this.listEventMaster
    if (this.selectedEvent) {
      filter = filter.filter(x => x.id !== this.selectedEvent?.id)
    } else {
      filter = this.listEventMaster
    }

    this.listEvent = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List PDO */

  dataPDO() {
    this.serviceAwp.pdo().subscribe(
        resp => {
          if (resp.success) {
            this.listPdoAll = resp.data.content
            this.listPdoAllMaster = resp.data.content
            this.searchPdo = resp.data.content
            if (this.pdoExisting.length > 0) {
              this.pdoExisting.forEach(dataPdoExist => {
                this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
                this.searchPdo = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
              })
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addPdoSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pdoRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePdo.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePdo.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPdo.push(data)
        }
        this.pdoRequest.push(data)
      }
    } else {
      this.pdoRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pdoRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPdo.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPdo.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePdo.push(data)
      }
    }
    this.listPdoAll = this.listPdoAllMaster

    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(dataPdoExist => {
        this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
      })
    }

    this.pdoRequest.forEach(dataProvince => {
      this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPdo = this.listPdoAll
  }

  searchFilterPdo(e) {
    const searchStr = e.target.value
    this.listPdoAll = this.searchPdo.filter((product) => {
      return (product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.description.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePdoExisting(data) {
    this.pdoExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.pdoExisting.splice(index, 1)
      }
    })
    this.listPdoAll = this.listPdoAllMaster
    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(dataPdoExist => {
        this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
      })
      this.pdoRequest.forEach(dataProvince => {
        this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    } else {
      this.pdoRequest.forEach(dataProvince => {
        this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPdo = this.listPdoAll
  }

  /** * List IRI */
  dataIri() {
    this.startLoading()
    this.serviceAwp.iri().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listIriAll = resp.data.content
            this.listIriAllMaster = resp.data.content
            this.searchIri = resp.data.content
            if (this.iriExisting.length > 0) {
              this.iriExisting.forEach(dataIriExist => {
                this.listIriAll = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
                this.searchIri = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
              })
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addIriSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.iriRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removeIri.forEach((check, index) => {
          if (check.id === data.id) {
            this.removeIri.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addIri.push(data)
        }
        this.iriRequest.push(data)
      }
    } else {
      this.iriRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.iriRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addIri.forEach((check, index) => {
        if (check.id === data.id) {
          this.addIri.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removeIri.push(data)
      }
    }
    this.listIriAll = this.listIriAllMaster

    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(dataIriExist => {
        this.listIriAll = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
      })
    }

    this.iriRequest.forEach(dataProvince => {
      this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })
    this.searchIri = this.listIriAll
  }

  searchFilterIri(e) {
    const searchStr = e.target.value
    this.listIriAll = this.searchIri.filter((product) => {
      return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
    });
  }

  removeIriExisting(data) {
    this.iriExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.iriExisting.splice(index, 1)
      }
    })
    this.listIriAll = this.listIriAllMaster
    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(dataPdoExist => {
        this.listIriAll = this.listIriAll.filter(x => x.id !== dataPdoExist?.iri?.id)
      })
      this.iriRequest.forEach(dataProvince => {
        this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    } else {
      this.iriRequest.forEach(dataProvince => {
        this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchIri = this.listIriAll
  }

  disabledForm1() {
    if (this.roles?.hasAuthority('Consultan')) {
      return !this.selectedComponentExist || !this.editNamaKegiatan || !this.editBudgetAwp ||  this.existOnPok == undefined
    } else {
      return !this.selectedComponentExist || !this.editNamaKegiatan || !this.editBudgetAwp ||  this.existOnPok == undefined || !this.selectedProjectOfficer
    }
  }

  changeMenuFieldProgress(menu: any) {
    this.fieldSubstantiv = menu
  }

  detailListKegiatanEvent() {
    this.startLoading()
    let params = {
      enablePage: false,
      page: 0,
      paramLike: [],
      paramIs: [
        {
          field: "event.awpImplementationId",
          dataType: "string",
          value: this.dataDetailKegiatan?.awpImplementation?.id
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "desc"
        }
      ]
    }
    this.service.listAllEvent(params).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataAllEvent = resp.data.content
            for (let i = 0; i < this.dataAllEvent.length; i++) {
              this.rraEventReports.push({eventReportId: '', rraFileId: ''})
            }
            if (this.dataAllEvent.length > 0) {
              let regenciesFromListEvent = []
              this.totalAllRealitationEvent = 0
              this.percentageAllRealitationEvent = 0
              this.totalAllParticipantEvent = 0
              this.percentageAllParticipantEvent = 0
              this.totalAllParticipantMaleEvent = 0
              this.totalAllParticipantFemaleEvent = 0
              this.percentageAllParticipantFemaleEvent = 0
              this.listSelectedEvent = this.dataAllEvent[0]
              // this.dataRAB = this.listSelectedEvent.event.rab
              this.dataRAB = resp.data.content
              this.fromDateIntroduction = parse(this.listSelectedEvent.startDate, 'yyyy-MM-dd', new Date());
              this.toDateIntroduction = parse(this.listSelectedEvent.endDate, 'yyyy-MM-dd', new Date());
              this.dataAllEvent.forEach(x => {
                this.totalAllRealitationEvent = this.totalAllRealitationEvent + x.rraBudget
                this.totalAllParticipantEvent = this.totalAllParticipantEvent + x.participantCount
                this.totalAllParticipantMaleEvent = this.totalAllParticipantMaleEvent + x.participantMale
                this.totalAllParticipantFemaleEvent = this.totalAllParticipantFemaleEvent + x.participantFemale
              })
              this.percentageAllRealitationEvent = this.totalAllRealitationEvent / this.dataDetailKegiatan.awpImplementation?.budgetPok
              this.percentageAllRealitationEvent = Number(this.percentageAllRealitationEvent.toFixed(2))
              this.percentageAllParticipantEvent = this.totalAllParticipantEvent / this.dataDetailKegiatan.awpImplementation?.participantCount
              this.percentageAllParticipantEvent = Number(this.percentageAllParticipantEvent.toFixed(2))
              if (this.dataDetailKegiatan?.awpImplementation?.participantMale !== 0 && this.dataDetailKegiatan?.awpImplementation?.participantMale != null) {
                this.percentageAllParticipantMaleEvent = (this.totalAllParticipantMaleEvent / this.dataDetailKegiatan.awpImplementation?.participantMale)
                this.percentageAllParticipantMaleEvent = Number(this.totalAllParticipantMaleEvent.toFixed(2))
                this.percentageAllParticipantMaleEvent = this.percentageAllParticipantMaleEvent * 100
              } else {
                this.percentageAllParticipantMaleEvent = 0
              }
              if (this.dataDetailKegiatan?.awpImplementation?.participantFemale !== 0 && this.dataDetailKegiatan?.awpImplementation?.participantFemale != null) {
                this.percentageAllParticipantFemaleEvent = this.percentageAllParticipantFemaleEvent / this.dataDetailKegiatan.awpImplementation?.participantFemale
                this.percentageAllParticipantFemaleEvent = Number(this.percentageAllParticipantFemaleEvent.toFixed(2))
                this.percentageAllParticipantFemaleEvent = this.percentageAllParticipantFemaleEvent * 100
              } else {
                this.percentageAllParticipantFemaleEvent = 0
              }
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.serviceAwp.listModa().subscribe(
        resp => {
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */

  /** * POK Code */
  listPokEvent() {
    this.startLoading()
    this.service.listCodePok().subscribe(
        resp => {
          if (resp.success) {
            this.listCodePok = resp.data.content
            this.listCodePokMaster = resp.data.content
            this.searchPok = resp.data.content
            if (this.pokExisting.length > 0) {
              this.pokExisting.forEach(dataPokExist => {
                this.listCodePok = this.listCodePok.filter(x => x.id !== dataPokExist?.pok?.id)
                this.searchPok = this.listCodePok.filter(x => x.id !== dataPokExist?.pok?.id)
              })
            }
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addPokSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pokRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePok.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePok.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPok.push(data)
        }
        this.pokRequest.push(data)
      }
    } else {
      this.pokRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pokRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPok.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPok.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePok.push(data)
      }
    }
    this.listCodePok = this.listCodePokMaster

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.id)
      })
    }

    this.pokRequest.forEach(dataProvince => {
      this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPok = this.listCodePok
  }

  searchFilterPok(e) {
    const searchStr = e.target.value
    this.listCodePok = this.searchPok.filter((product) => {
      return (product.code.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.title.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePokExisting(data) {
    this.pokExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.deletedPokExisting.push(data?.id)
        this.pokExisting.splice(index, 1)
      }
    })
    this.listCodePok = this.listCodePokMaster
    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(dataPdoExist => {
        this.listCodePok = this.listCodePok.filter(x => x.id !== dataPdoExist?.pok?.id)
      })
      this.pokRequest.forEach(dataPok => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataPok.id)
      })
    } else {
      this.pokRequest.forEach(dataProvince => {
        this.listCodePok = this.listCodePok.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPok = this.listCodePok
  }
  /** * End POK Code */


  /** * List Target Result Chain Activity */
  listResultChainCode() {
    let tag
    let params = {
      enablePage: false,
      page: 0,
      paramLike: [],
      paramIs: [
        {
          field: "year",
          dataType: "int",
          value: this.dataDetailKegiatan?.awpImplementation?.awp?.year
        },
        {
          field: "monevTypeId",
          dataType: "string",
          value: "afedb8dc-d1ee-11ec-88cb-0ba398ed0298"
        }
      ],
      paramIn: [],
      paramNotIn: [],
      sort: [
        {
          field: "code",
          direction: "asc"
        }
      ]
    }
    this.service.listResultChainAll(params).subscribe(
        resp => {
          this.startLoading()
          if (resp.success) {
            this.stopLoading()
            this.listChainCode = resp.data.content
            this.listChainCodeMaster = resp.data.content

            this.listChainCode = this.listChainCode.filter(x => x.code.charAt(2) === this.dataDetailKegiatan?.awpImplementation?.awp?.component.code)
            this.listChainCodeMaster = this.listChainCodeMaster.filter(x => x.code.charAt(2) === this.dataDetailKegiatan?.awpImplementation?.awp?.component.code)

            if (this.listChainCodeAndIndicatorExist.length > 0) {
              this.listChainCodeAndIndicatorExist.forEach(y => {
                this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
              })
            }
            this.listChainCodeFilter = this.listChainCode
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedChainCodeFromList(data) {
    this.selectedChainCode = null
    this.listIndicatorSuccessApi = []
    this.selectedIndicatorSuccess = []
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }

    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }

    this.selectedChainCode = data
    this.listChainCode = this.listChainCode.filter(dataFilter => dataFilter.id != this.selectedChainCode.id)
    this.listIndicatorSuccessFromSelectedChainCode()

    this.listChainCodeFilter = this.listChainCode
  }

  listIndicatorSuccessFromSelectedChainCode() {
    let tag = []
    if (this.createNewIndicatorSuccess === true) {
      if (this.listSelectedChainCodeAndIndicator.length > 0) {
        this.listSelectedChainCodeAndIndicator.forEach(y => {
          y.successIndicatorIds.forEach(z => {
            this.selectedChainCode.successIndicator = this.selectedChainCode.successIndicator.filter(dataIndicator => z.id !== dataIndicator.id)
          })
        })
      }
    } else if (this.createNewIndicatorSuccess === false) {
      if (this.listMonevExisting.length > 0) {
        this.listMonevExisting.forEach(y => {
          y.successIndicator.forEach(z => {
            this.selectedChainCode.successIndicator = this.selectedChainCode.successIndicator.filter(dataIndicator => z.successIndicator?.id !== dataIndicator.id)
          })
        })
      }
    }
    this.selectedChainCode.successIndicator?.forEach(x => {
      tag.push(x.code + ' - ' + x.name)
    })
    this.listIndicatorSuccessApi = tag
  }


  searchFilterChainCode(e) {
    const searchStr = e.target.value
    let filter = this.listChainCodeFilter
    if (this.selectedChainCode) {
      filter = filter.filter(x => x.id !== this.selectedChainCode.id)
    } else {
      filter = this.listChainCodeFilter
    }

    this.listChainCode = filter.filter((product) => {
      return ((product?.result?.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product?.resultChainCode?.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * End Of List Target Result Chain */

  /** * List Indicator Success */
  listIndicatorSuccess() {
    let tag = []
    const params = {
      enablePage: false,
      page: 0,
      paramLike: [],
      paramIs: [
        {
          field: "resultChainId",
          dataType: "string",
          value: this.selectedChainCode?.id
        }
      ],
      paramIn: [],
      paramNotIn: [],
      sort: [
        {
          field: "successIndicatorCode",
          direction: "asc"
        }
      ]
    }
    this.service.listIndicatorCode(params).subscribe(
        resp => {
          if (resp.success) {
            this.dataListIndicatorSuccess = resp.data.content
            this.dataListIndicatorSuccess.forEach(x => {
              if (this.createNewIndicatorSuccess === true) {
                if (this.listSelectedChainCodeAndIndicator.length > 0) {
                  this.listSelectedChainCodeAndIndicator.forEach(y => {
                    y.successIndicatorIds.forEach(z => {
                      this.dataListIndicatorSuccess = this.dataListIndicatorSuccess.filter(dataIndicator => z.id !== dataIndicator.id)
                    })
                  })
                }
              } else if (this.createNewIndicatorSuccess === false) {
                if (this.listMonevExisting.length > 0) {
                  this.listMonevExisting.forEach(y => {
                    y.successIndicator.forEach(z => {
                      console.log(z)
                      this.dataListIndicatorSuccess = this.dataListIndicatorSuccess.filter(dataIndicator => z.successIndicator?.id !== dataIndicator.id)
                    })
                  })
                }
              }
            })
            this.dataListIndicatorSuccess.forEach(x => {
              tag.push(x.successIndicatorCode + ' - ' + x.successIndicator)
            })
            this.listIndicatorSuccessApi = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addSelectedResultChainAndIndicatorSuccess() {
    let selectedIndicator = []
    let dataSelectedIndicator = []
    let selectedIndicatorToApi = []

    this.selectedIndicatorSuccess.forEach(x => {
      const selectedIndicatorSuccessExist = x.split(" ", 1)
      selectedIndicator.push(selectedIndicatorSuccessExist[0])
    })

    selectedIndicator.forEach(x => {
      dataSelectedIndicator = this.selectedChainCode?.successIndicator.filter(y => x === y.code)
      if (dataSelectedIndicator.length > 0) {
        selectedIndicatorToApi.push(dataSelectedIndicator[0])
      }
    })
    this.listSelectedChainCodeAndIndicator.push({
      resultChainId: this.selectedChainCode,
      successIndicatorIds: selectedIndicatorToApi
    })
    console.log(this.listSelectedChainCodeAndIndicator)
    this.closeModal()
    this.selectedChainCode = null
    this.selectedIndicatorSuccess = []
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }

    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  addSelectedResultChainAndIndicatorSuccessExisting() {
    let selectedIndicator = []
    let dataSelectedIndicator = []
    let selectedIndicatorToApi = []
    let filterSelectedChainCodeAndIndicator = []

    this.selectedIndicatorSuccess.forEach(x => {
      const selectedIndicatorSuccessExist = x.split(" ", 1)
      selectedIndicator.push(selectedIndicatorSuccessExist[0])
    })

    selectedIndicator.forEach(x => {
      dataSelectedIndicator = this.selectedChainCode?.successIndicator.filter(y => x === y.code)
      if (dataSelectedIndicator.length > 0) {
        selectedIndicatorToApi.push(dataSelectedIndicator[0])
      }
    })

    if (this.createNewIndicatorSuccess === true) {
      this.listSelectedChainCodeAndIndicator.forEach(x => {
        filterSelectedChainCodeAndIndicator = this.listSelectedChainCodeAndIndicator.filter(x => x.resultChainId?.id === this.selectedChainCode.id)
        if (filterSelectedChainCodeAndIndicator.length > 0) {
          selectedIndicatorToApi.forEach(y => {
            filterSelectedChainCodeAndIndicator[0].successIndicatorIds.push(y)
          })
        }
      })
    }

    if (this.createNewIndicatorSuccess === false) {
      filterSelectedChainCodeAndIndicator = this.listMonevExisting.filter(x => x.resultChain?.id === this.selectedChainCode.id)
      console.log(filterSelectedChainCodeAndIndicator)
      if (filterSelectedChainCodeAndIndicator.length > 0) {
        selectedIndicatorToApi.forEach(y => {
          filterSelectedChainCodeAndIndicator[0].successIndicator.push({
            existing: false,
            successIndicator: y,
            id: y.id
          })
        })
      }
    }

    this.closeModal()
    this.selectedChainCode = null
    this.selectedIndicatorSuccess = []
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }

    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  removeNewIndicatorExist(data) {
    this.listMonevExisting.forEach((dataList, index ) => {
      dataList.successIndicator.forEach((check, index ) => {
        if (check.successIndicator?.id === data?.successIndicator?.id) {
          dataList.successIndicator.splice(index, 1)
        }
      })
      if (dataList.successIndicator.length === 0) {
        this.listMonevExisting.splice(index, 1)
      }
    })
    this.listChainCode = this.listChainCodeMaster
    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }

    if (this.listMonevExisting.length > 0) {
      this.listMonevExisting.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }
  }

  removeNewIndicatorNew(data) {
    this.listSelectedChainCodeAndIndicator.forEach((dataList, index ) => {
      dataList.successIndicatorIds.forEach((check, index ) => {
        if (check.id === data.id) {
          dataList.successIndicatorIds.splice(index, 1)
        }
      })
      if (dataList.successIndicatorIds.length == 0) {
        this.listSelectedChainCodeAndIndicator.splice(index, 1)
      }
    })
    this.listChainCode = this.listChainCodeMaster
    if (this.listChainCodeAndIndicatorExist.length > 0) {
      this.listChainCodeAndIndicatorExist.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChain.id !== x.id)
      })
    }
    if (this.listSelectedChainCodeAndIndicator.length > 0) {
      this.listSelectedChainCodeAndIndicator.forEach(y => {
        this.listChainCode = this.listChainCode.filter(x => y.resultChainId.id !== x.id)
      })
    }
    this.listChainCodeFilter = this.listChainCode
  }

  /** * End Of List Indicator Success */

  validationForm() {
    let success
    let successExist
    let successIndicatorExist = []
    let successIndicatorSelected = []
    this.listMonevExisting.forEach(x => {
      x.successIndicator.forEach(y => {
        successIndicatorExist.push(y.successIndicator)
      })
    })
    this.listSelectedChainCodeAndIndicator.forEach(x => {
      x.successIndicatorIds.forEach(y => {
        successIndicatorSelected.push(y)
      })
    })
    success = successIndicatorExist?.filter(x => !x.resultIndicator)
    successExist = successIndicatorSelected?.filter(x => !x.resultIndicator)

    return success?.length > 0 || successExist.length > 0
  }

  doUploadAttachmentFile() {
    this.loadingCreateKegiatan = true
    if (this.uploadFileBast.length > 0) {
      this.serviceResources.uploadFileBast(this.uploadFileBast[0]).subscribe(resp => {
            if (resp.success) {
              this.idFileBast = resp.data[0].id
              this.createKegiatan()
            } else {
              this.loadingCreateKegiatan = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            if (error?.status === 400) {
              if (error?.error?.data.length > 0) {
                let checkError = error?.error?.data.filter(x => x.message.includes('Mime Type'))
                if (checkError.length > 0) {
                  Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'Format File BAST harus dalam format pdf atau doc',
                  });
                  this.loadingCreateKegiatan = false
                } else {
                  this.loadingCreateKegiatan = false
                  this.swalAlert.showAlertSwal(error)
                }
              } else {
                this.loadingCreateKegiatan = false
                this.swalAlert.showAlertSwal(error)
              }
            } else {
              this.loadingCreateKegiatan = false
              this.swalAlert.showAlertSwal(error)
            }
          }
      );
    } else {
      this.idFileBast = this.dataDetailKegiatan?.bastFile?.id
      this.createKegiatan()
    }
  }

  createKegiatan() {
    let startDate
    let endDate
    let PokExisting = []
    let PokRequest = []
    let selectedProvinceAndCityEdit = JSON.parse(JSON.stringify(this.selectedProvinceAndCity))
    let dataRegencyIds = []
    let selectedProvinceCity = []
    let dataRegencyExistingIds = []
    let selectedProvinceCityExist = []
    let successIndicatorExist = []
    let monevExisting = []
    let monevIds = []
    let newMonev = []
    let successIndicatorSelected = []
    let successIndicatorSelectedNewExis = []
    let params

    this.listMonevExisting.forEach(x => {
      x.successIndicator.forEach(y => {
        console.log(y)
        y.successIndicator.awpReportSuccessIndicators = y.id
        successIndicatorExist.push(y.successIndicator)
      })
    })
    this.successIndicatorIds = successIndicatorExist.map(key => ({id: key.awpReportSuccessIndicators, contribution: key.resultIndicator}));
    successIndicatorSelectedNewExis = successIndicatorExist.map(key => ({id: key.id, contribution: key.resultIndicator}));

    this.listSelectedChainCodeAndIndicator.forEach(x => {
      x.successIndicatorIds.forEach(y => {
        successIndicatorSelected.push(y)
      })
    })
    this.successIndicatorSelected = successIndicatorSelected.map(key => ({id: key.id, contribution: key.resultIndicator}));

    this.listMonevExisting.forEach(x => {
      x.successIndicator.forEach(y => {
        let dataIndicator = []
        if (y.existing) {
          this.successIndicatorIds.forEach(successIndicator => {
            if (successIndicator?.id === y.id) {
              dataIndicator.push(successIndicator)
            }
          })
          let findMonevExist = monevIds.find(z => z.awpReportResultChainId === x.successIndicator[0].awpImplementationReportResultChainId)
          if (findMonevExist) {
            findMonevExist.awpReportSuccessIndicators.push(dataIndicator[0])
          } else {
            monevIds.push({
              awpReportResultChainId: x.successIndicator[0].awpImplementationReportResultChainId,
              awpReportSuccessIndicators: dataIndicator
            })
          }
        } else {
          let dataIndicator = []
          successIndicatorSelectedNewExis.forEach(successIndicator => {
            if (successIndicator?.id === y.successIndicator.id) {
              dataIndicator.push(successIndicator)
            }
          })
          let findMonevExist = monevExisting.find(z => z.resultChainId === x.resultChain.id)
          if (findMonevExist) {
            findMonevExist.successIndicators.push(dataIndicator[0])
          } else {
            monevExisting.push({
              resultChainId: x.resultChain.id,
              successIndicators: dataIndicator
            })
          }
        }
      })
    })
    this.listSelectedChainCodeAndIndicator.forEach(x => {
      x.successIndicatorIds.forEach(y => {
        let dataIndicator = []
        this.successIndicatorSelected.forEach(successIndicator => {
          if (successIndicator?.id === y.id) {
            dataIndicator.push(successIndicator)
          }
        })
        let findMonevExist = monevExisting.find(z => z.resultChainId === x.resultChainId.id)
        if (findMonevExist) {
          findMonevExist.successIndicators.push(dataIndicator[0])
        } else {
          monevExisting.push({
            resultChainId: x.resultChainId.id,
            successIndicators: dataIndicator
          })
        }
      })
    })

    if (this.pokExisting.length > 0) {
      this.pokExisting.forEach(x => {
        PokExisting.push(x.pok?.id)
      })
    } else {
      PokExisting = []
    }

    if (this.pokRequest.length > 0) {
      this.pokRequest.forEach(x => {
        PokRequest.push(x.id)
      })
    }

    selectedProvinceAndCityEdit.forEach(x => {
      if (x.regencyId.length > 0) {
        x.regencyId = x.regencyId.map(y => this.dataAllCitySelected.find(xz => xz.name === y))
      }
    })

    selectedProvinceAndCityEdit.forEach(data => {
      if (data?.regencyId) {
        dataRegencyIds = []
        data.regencyId.forEach(x => {
          dataRegencyIds.push(x.id)
        })
        selectedProvinceCity.push({
          provinceId: data?.provinceId?.id,
          regencyIds: dataRegencyIds
        })
      } else {
        selectedProvinceCity.push({
          provinceId: data?.provinceId?.id,
          regencyIds: null
        })
      }
    })
    if (this.provinceAndRegenciesExist.length > 0) {
      this.provinceAndRegenciesExist.forEach(data => {
        if (data?.regencies) {
          dataRegencyExistingIds = []
          data?.regencies.forEach(x => {
            dataRegencyExistingIds.push(x.id)
          })
          selectedProvinceCityExist.push({
            provinceId: data.province?.id,
            regencyIds: dataRegencyExistingIds
          })
        } else {
          selectedProvinceCityExist.push({
            provinceId: data.id,
            regencyIds: null
          })
        }
      })
    } else {
      selectedProvinceCityExist = []
    }

    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')

    if (this.roles.hasAuthority('LSP')) {
      params = {
        awpImplementationId: this.awpImplementationId,
        startDate: startDate,
        endDate: endDate,
        eventOtherInformation: this.editWaktuInformasiLainnya,
        eventVolume: this.editVolumeEvent,
        summary: this.activitySummary,
        conclusionsAndRecommendations: this.activityConclusion,
        locationOtherInformation: this.tempatInformasiLainnya,
        activityModeId: this.selectedModaActivity.id,
        participantTarget: this.targetParticipant,
        nameOtherInformation: this.otherInformationAwp,
        newPokIds: PokRequest,
        deletedPokIds: this.deletedPokExisting,
        newProvincesRegencies: selectedProvinceCity,
        deletedProvincesRegeciesIds: this.deletedProvinceRegenciesExist,
        rraEventReports: this.rraEventReports,
        monevIds: monevIds,
        bastFileId: this.idFileBast
      }
    } else {
      params = {
        awpImplementationId: this.awpImplementationId,
        startDate: startDate,
        endDate: endDate,
        eventOtherInformation: this.editWaktuInformasiLainnya,
        eventVolume: this.editVolumeEvent,
        summary: this.activitySummary,
        conclusionsAndRecommendations: this.activityConclusion,
        locationOtherInformation: this.tempatInformasiLainnya,
        activityModeId: this.selectedModaActivity.id,
        participantTarget: this.targetParticipant,
        nameOtherInformation: this.otherInformationAwp,
        newPokIds: PokRequest,
        deletedPokIds: this.deletedPokExisting,
        newProvincesRegencies: selectedProvinceCity,
        deletedProvincesRegeciesIds: this.deletedProvinceRegenciesExist,
        rraEventReports: this.rraEventReports,
        newMonev: monevExisting,
        monevIds: monevIds,
        bastFileId: this.idFileBast
      }
    }
    this.service.updateActivityReport(this.detailIdKegiatan ,params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingCreateKegiatan = false
            this.back()
          }
        }, error => {
          this.loadingCreateKegiatan = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  deleteAnggaran(index: number) {
    this.dataRAB[index].rraFileId = undefined;
  }

  deleteRAB(index: number) {
    this.dataRAB[index].event.rabFileId = undefined;
  }

  showFileInfo(e, index){
    this.dataRAB[index].anggaran = e[0].name;
    this.closeModal();
  }

  openModalComp(data, index, type){
    const modalRef = this.modalService.open(ModalComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.listRAB = data;
    modalRef.componentInstance.index = index;
    modalRef.componentInstance.type = type;
    modalRef.componentInstance.event.subscribe((newRAB) => {
      if (newRAB.type === 'rab') {
        this.dataRAB[newRAB.index].event.rabFileId = newRAB.id
        this.dataRAB[newRAB.index].event.rabFile.id = newRAB.id
        this.dataRAB[newRAB.index].event.rabFile.filename = newRAB.filename
      } else {
        this.rraEventReports.splice(newRAB.index, 1, {eventReportId: newRAB.eventReportId, rraFileId: newRAB.id});
        this.dataRAB[newRAB.index].rraFileId = newRAB.id
        this.dataRAB[newRAB.index].rraFile.id = newRAB.id
        this.dataRAB[newRAB.index].rraFile.filename = newRAB.filename
      }
    });
  }
}
