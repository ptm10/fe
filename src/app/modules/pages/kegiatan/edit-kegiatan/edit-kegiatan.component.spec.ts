import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditKegiatanComponent } from './edit-kegiatan.component';

describe('EditKegiatanComponent', () => {
  let component: EditKegiatanComponent;
  let fixture: ComponentFixture<EditKegiatanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditKegiatanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditKegiatanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
