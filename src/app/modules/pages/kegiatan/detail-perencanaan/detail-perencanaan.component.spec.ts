import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPerencanaanComponent } from './detail-perencanaan.component';

describe('DetailPerencanaanComponent', () => {
  let component: DetailPerencanaanComponent;
  let fixture: ComponentFixture<DetailPerencanaanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailPerencanaanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPerencanaanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
