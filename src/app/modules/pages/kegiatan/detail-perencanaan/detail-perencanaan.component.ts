import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Kegiatan} from "../../../../core/models/awp.model";
import Swal from "sweetalert2";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {QualityAtEntryModel} from "../../../../core/models/qualityAtEntry.model";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-perencanaan',
  templateUrl: './detail-perencanaan.component.html',
  styleUrls: ['./detail-perencanaan.component.scss']
})
export class DetailPerencanaanComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  detailIdKegiatan: string
  dataDetailKegiatan: Kegiatan
  loadingDownloadPdf = false
  idLogged: string
  rejectMessage: string
  loadingSaveData = true
  countEntryTrue: number = 0
  countEntryTrueExisting: number = 0
  countEntryFalse: number = 0
  dataQualityEntryRequest = []
  dataEditQualityEntry = []
  alreadyEditQualityEntry = []
  neverEditQualityEntry = false
  flagApproveReject: number
  checkedButton: number

  //form quality at entry
  comment: string

  // data quality entry
  dataQualityEntry: QualityAtEntryModel[]

  constructor(
      private router: Router,
      private activatedRouter: ActivatedRoute,
      private service: KegiatanService,
      private modalService: NgbModal,
      public roles: AuthfakeauthenticationService,
      private serviceNotification: ProfileService,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.idLogged = localStorage.getItem('user_id')
    this.breadCrumbItems = [{ label: 'Kegiatan' }, { label: 'Detail Kegiatan', active: true }]
    this.detailIdKegiatan = this.activatedRouter.snapshot.paramMap.get('id')
    this.checkedButton = 1
    this.getDetailEvent()
    this.updateStatusTask()
    this.listQualityEntry()
  }

  canUpdateImplement() {
    return this.idLogged === this.dataDetailKegiatan?.createdByUser?.id;
  }

  goToListKegiatan() {
    this.router.navigate(['implement/kegiatan'])
  }

  getDetailEvent(){
    this.startLoading()
    this.service.detailKegiatan(this.detailIdKegiatan).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataDetailKegiatan = resp.data
            this.dataDetailKegiatan.convertStartDate = new Date(this.dataDetailKegiatan.startDate)
            this.dataDetailKegiatan.convertEndDate = new Date(this.dataDetailKegiatan.endDate)
            this.dataDetailKegiatan.awp.convertStartDate = new Date (this.dataDetailKegiatan?.awp?.startDate)
            this.dataDetailKegiatan.awp.convertEndDate = new Date (this.dataDetailKegiatan?.awp?.endDate)
            this.dataEditQualityEntry = JSON.parse(JSON.stringify(this.dataDetailKegiatan?.awpImplementationQualityAtEntryAnswers))
            this.comment = this.dataDetailKegiatan?.qualitAtEntryNote
            let filteredDataCheckedTrue = this.dataDetailKegiatan?.awpImplementationQualityAtEntryAnswers.filter(data => data.answer == true)
            if (filteredDataCheckedTrue.length > 0) {
              this.countEntryTrueExisting = filteredDataCheckedTrue.length
            } else {
              this.countEntryTrueExisting = 0
            }
            let x = this.dataEditQualityEntry.filter(dataFilter => dataFilter.answer == null)
            this.alreadyEditQualityEntry = x
            if (x.length > 0) {
              this.neverEditQualityEntry = true
            }

            if (this.dataEditQualityEntry[0].answer != null) {
              let filteredDataCheckedTrue = this.dataEditQualityEntry.filter(data => data.answer == true)
              if (filteredDataCheckedTrue.length > 0) {
                this.countEntryTrue = filteredDataCheckedTrue.length
              } else {
                this.countEntryTrue = 0
              }
              let filteredDataCheckedFalse = this.dataEditQualityEntry.filter(data => data.answer == false)
              if (filteredDataCheckedFalse.length > 0) {
                this.countEntryFalse = filteredDataCheckedFalse.length
              } else {
                this.countEntryFalse = 0
              }

              this.dataEditQualityEntry.forEach(data => {
                if (data.answer) {
                  this.dataQualityEntryRequest.push({
                    qualityAtEntryQuestionId: data.qualityAtEntryQuestionId,
                    answer: data.answer,
                    revision: null
                  })
                } else if (data.answer == false) {
                  this.dataQualityEntryRequest.push({
                    qualityAtEntryQuestionId: data.qualityAtEntryQuestionId,
                    answer: data.answer,
                    revision: data.revision
                  })
                }
              })
            }
            this.dataDetailKegiatan?.pdo?.forEach(dataPdo => {
              if (this.dataDetailKegiatan.year == 2020) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2020
              } else if (this.dataDetailKegiatan.year == 2021) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2021
              } else if (this.dataDetailKegiatan.year == 2022) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2022
              } else if (this.dataDetailKegiatan.year == 2023) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2023
              } else if (this.dataDetailKegiatan.year == 2024) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2024
              }
            })
            this.dataDetailKegiatan?.iri?.forEach(dataIri => {
              if (this.dataDetailKegiatan.year == 2020) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2020
              } else if (this.dataDetailKegiatan.year == 2021) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2021
              } else if (this.dataDetailKegiatan.year == 2022) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2022
              } else if (this.dataDetailKegiatan.year == 2023) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2023
              } else if (this.dataDetailKegiatan.year == 2024) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2024
              }
            })
          }
          this.canUpdateImplement()
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'md', centered: true });
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModalQuality(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'xl', centered: true });
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   * @param flagApproveReject
   */
  openModalApproveReject(selectedMenu: any, flagApproveReject: any) {
    this.flagApproveReject = flagApproveReject
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  openModalHistory(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  downloadFile(id, name) {
    this.loadingDownloadPdf = true
    this.service.exportFile(id, name).subscribe(
        resp => {
          this.loadingDownloadPdf = false
        }, error => {
          this.loadingDownloadPdf = false
        }
    )
  }

  goToImplementKegiatan() {
    this.router.navigate(['implement/add-kegiatan/' + this.detailIdKegiatan])
  }

  updateFromCoordinator() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdKegiatan,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage,
      answers: this.dataQualityEntryRequest,
    }
    this.service.updateFromCoordinator(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.router.navigate(['implement/kegiatan'])
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateFromPmu() {
    this.loadingSaveData = false
    const param = {
      id: this.detailIdKegiatan,
      approveStatus: this.flagApproveReject,
      note: this.rejectMessage
    }
    this.service.updateFromPmu(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.router.navigate(['implement/kegiatan'])
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateStatusTask() {
    const params = {
      taskId: this.detailIdKegiatan,
      taskType: 5,
      status: 1
    }
    this.service.updateStatusTask(params).subscribe(
        resp => {
          if (resp.success) {
            this.serviceNotification.updateNotification.emit()
          }
        }
    )
  }

  listQualityEntry() {
    this.service.qualityAtEntry().subscribe(
        resp => {
          if (resp.success) {
            this.dataQualityEntry = resp.data.content
            this.dataQualityEntry.forEach(dataEntry => {
              dataEntry.checked = undefined
              dataEntry.perbaikan = null
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeValueEntry() {
    this.countEntryTrue = 0
    this.countEntryFalse = 0

    let filteredDataCheckedTrue = this.dataEditQualityEntry.filter(data => data.answer == true)
    if (filteredDataCheckedTrue.length > 0) {
      this.countEntryTrue = filteredDataCheckedTrue.length
    } else {
      this.countEntryTrue = 0
    }
    let filteredDataCheckedFalse = this.dataEditQualityEntry.filter(data => data.answer == false)
    if (filteredDataCheckedFalse.length > 0) {
      this.countEntryFalse = filteredDataCheckedFalse.length
    } else {
      this.countEntryFalse = 0
    }
    console.log(this.countEntryTrue)
  }

  disableButtonSaveQuality() {
    if (this.countEntryFalse + this.countEntryTrue != 7) {
      return true
    } else {
      const dataFilteredCheckedFalse = this.dataEditQualityEntry.filter(data => data.answer == false)
      if (dataFilteredCheckedFalse.length > 0) {
        const check = dataFilteredCheckedFalse.filter(data => (data.revision == null || data.revision == ''))
        console.log(check)
        return check.length > 0;
      } else {
        return false
      }
    }
  }

  checkValue() {
    console.log(this.dataQualityEntry)
  }

  isDisabled() {
    return !this.dataDetailKegiatan?.coordinatorAllowedApprove
  }

  saveDataQuality() {
    this.dataQualityEntryRequest = []
    this.dataEditQualityEntry.forEach(data => {
      if (data.answer) {
        this.dataQualityEntryRequest.push({
          qualityAtEntryQuestionId: data.qualityAtEntryQuestionId,
          answer: data.answer,
          revision: null
        })
      } else if (data.answer == false) {
        this.dataQualityEntryRequest.push({
          qualityAtEntryQuestionId: data.qualityAtEntryQuestionId,
          answer: data.answer,
          revision: data.revision
        })
      }
    })
    let x = this.dataEditQualityEntry.filter(dataFilter => dataFilter.answer == null)
    this.alreadyEditQualityEntry = x
    this.neverEditQualityEntry = x.length > 0;
    this.closeModal()
  }

  showToolTip() {
    if (this.countEntryTrue >= 5) {
      return `(Point = ${this.countEntryTrue}) Direkomendasi untuk dilaksanakan, Hanya diperlukan revisi/klarifikasi kecil terhadap RIK.`
    } else if (this.countEntryTrue == 3 || this.countEntryTrue == 4) {
      return `(Point = ${this.countEntryTrue}) Direkomendasi untuk dilaksanakan, tetapi harus ada revisi/klarifikasi yang berarti terhadap RIK.`
    } else if (this.countEntryTrue <= 2) {
      return `(Point = ${this.countEntryTrue}) Kegiatan TIDAK direkomendasikan untuk dilaksanakan jika tidak ada revisi mendasar terhadap RIK.`
    }
  }

  showToolTipStatus() {
    if (this.countEntryTrueExisting >= 5) {
      return `Direkomendasi untuk dilaksanakan, Hanya diperlukan revisi/klarifikasi kecil terhadap RIK.`
    } else if (this.countEntryTrueExisting == 3 || this.countEntryTrueExisting == 4) {
      return `Direkomendasi untuk dilaksanakan, tetapi harus ada revisi/klarifikasi yang berarti terhadap RIK.`
    } else if (this.countEntryTrueExisting <= 2) {
      return `Kegiatan TIDAK direkomendasikan untuk dilaksanakan jika tidak ada revisi mendasar terhadap RIK.`
    }
  }

  showToolTipStatusViewer() {
    if (this.countEntryTrue >= 5) {
      return `Direkomendasi untuk dilaksanakan, Hanya diperlukan revisi/klarifikasi kecil terhadap RIK.`
    } else if (this.countEntryTrue == 3 || this.countEntryTrue == 4) {
      return `Direkomendasi untuk dilaksanakan, tetapi harus ada revisi/klarifikasi yang berarti terhadap RIK.`
    } else if (this.countEntryTrue <= 2) {
      return `Kegiatan TIDAK direkomendasikan untuk dilaksanakan jika tidak ada revisi mendasar terhadap RIK.`
    }
  }
}
