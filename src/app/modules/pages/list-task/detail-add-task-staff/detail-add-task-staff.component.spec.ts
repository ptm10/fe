import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAddTaskStaffComponent } from './detail-add-task-staff.component';

describe('DetailAddTaskStaffComponent', () => {
  let component: DetailAddTaskStaffComponent;
  let fixture: ComponentFixture<DetailAddTaskStaffComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailAddTaskStaffComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAddTaskStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
