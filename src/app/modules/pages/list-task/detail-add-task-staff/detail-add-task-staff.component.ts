import {Component, OnInit, ViewChild} from '@angular/core';
import {DetailTaskModel} from "../../../../core/models/detail-task.model";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {FileService} from "../../../../core/services/Image/file.service";
import {NgbDate, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploadControl} from "@iplab/ngx-file-upload";
import {parse} from "date-fns";
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-add-task-staff',
  templateUrl: './detail-add-task-staff.component.html',
  styleUrls: ['./detail-add-task-staff.component.scss']
})
export class DetailAddTaskStaffComponent implements OnInit {

    @ViewChild('scrollEle') scrollEle;
    @ViewChild('scrollRef') scrollRef;

    public fileUploadControl = new FileUploadControl({ listVisible: false, accept: ['.pdf'], multiple: false});
    breadCrumbItems: Array<{}>;
  detailTask: DetailTaskModel
  loadingSavePdf = false
  idDetailTask: string
    finishLoadData = true
    finishLoadDataCount = 0
    rejectMessage: string;
  loadingActionTask = false
    changeStatus: boolean = false
    addNewCommentChat: string
    loadingComment = false
    uploadedFileEvidenceIds = []
    filesEdit = []
    lastUpdateFile = []
    lastUpdateTimeFile: any

    constructor(
      private service: AdministrationService,
      private serviceImage: FileService,
      private route: ActivatedRoute,
      private modalService: NgbModal,
      private router: Router,
      private serviceNotification: ProfileService,
      private swalAlert: SwalAlert
    ) {
        this.route.paramMap.subscribe(params => {
            this.ngOnInit()
        })
    }

  ngOnInit(): void {
    this.idDetailTask = this.route.snapshot.paramMap.get('id')
    this.breadCrumbItems = [{ label: 'Profil' }, { label: 'Tugas' },{ label: 'Penugasan' }, { label: 'Detail Tugas', active: true }];
    this.getDetailAddTask()
  }

    onListScroll() {
        if (this.scrollRef !== undefined) {
            setTimeout(() => {
                this.scrollRef.SimpleBar.getScrollElement().scrollTop =
                    this.scrollRef.SimpleBar.getScrollElement().scrollHeight + 1500;
            }, 500);
        }
    }

  downloadPdf(id) {
    this.loadingSavePdf = true
    this.service.exportPdf(id).subscribe(
        resp => {
          this.loadingSavePdf = false
        }, error => {
          this.loadingSavePdf = false
        }
    )
  }

    removeFileExist(data) {
        this.filesEdit.forEach((check, index) => {
            if (check.id == data.id) {
                this.filesEdit.splice(index, 1)
            }
        })
    }

  getDetailAddTask() {
      this.startLoading()
    this.service.detailAddTask(this.idDetailTask).subscribe(
        resp => {
            this.stopLoading()
          if (resp.success) {
              this.filesEdit = []
              this.detailTask = resp.data
              if (this.detailTask.status == 0) {
                  this.changeStatusReadTask()
              }
              this.changeStatus = this.detailTask.doneByReceiver
              this.detailTask.files.forEach(dataFile => {
                  if (dataFile.uploadedFrom == 2) {
                      this.filesEdit.push(dataFile)
                  }
              })
              if (this.filesEdit.length > 0) {
                  this.lastUpdateFile = Object.assign([], this.filesEdit.reverse())
                  this.lastUpdateTimeFile = parse(this.lastUpdateFile[0].updatedAt, 'dd-MM-yyyy HH:mm:ss', new Date())
              }
              this.detailTask.convertUpdatedAt = parse(this.detailTask.updatedAt, 'dd-MM-yyyy HH:mm:ss', new Date())
              if (this.detailTask?.createdForResources?.user?.profilePicture?.id) {
                  this.serviceImage.getUrlPhoto(this.detailTask?.createdForResources?.user?.profilePicture?.id).subscribe(
                      resp => {
                          let reader = new FileReader();
                          reader.addEventListener("load", () => {
                              this.detailTask.filePhoto = reader.result;
                          }, false);
                          reader.readAsDataURL(resp);
                      }
                  )
              } else {
                  this.detailTask.filePhoto = null
              }
              if (this.detailTask?.createdByResources?.user?.profilePicture?.id) {
                  this.serviceImage.getUrlPhoto(this.detailTask?.createdByResources?.user?.profilePicture?.id).subscribe(
                      resp => {
                          let reader = new FileReader();
                          reader.addEventListener("load", () => {
                              this.detailTask.filePhotoCreatedTask = reader.result;
                          }, false);
                          reader.readAsDataURL(resp);
                      }
                  )
              } else {
                  this.detailTask.filePhotoCreatedTask = null
              }
          }
        }, error => {
            this.stopLoading()
            this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeStatusReadTask() {
      const params = {
          taskId: this.idDetailTask,
          taskType: 4
      }
      this.service.changeStatusReadTask(params).subscribe(
          resp => {
              if (resp.success) {
                  this.serviceNotification.updateNotification.emit()
              }
          }, error => {
              this.swalAlert.showAlertSwal(error)
          }
      )
  }

    openModal(selectedMenu: any) {
        this.modalService.open(selectedMenu, { size: 'lg', centered: true });
    }

    /** * close modal */
    closeModal(){
        this.modalService.dismissAll()
    }

    doUpload(index, doneTask) {
        this.loadingActionTask = true
        if (this.fileUploadControl.value.length != 0) {
            if (index === 0) {
                this.uploadedFileEvidenceIds = [];
            }
            this.service.upload(this.fileUploadControl.value[index], 7).subscribe(data => {
                this.uploadedFileEvidenceIds.push(data.data[0].id);
                if (this.fileUploadControl.value.length > (index+1)) {
                    this.doUpload(index+1, doneTask);
                } else {
                    this.nextActDetailTask(3, doneTask);
                }
            }, error => {
                this.loadingActionTask = false
                this.swalAlert.showAlertSwal(error)
            });
        } else {
            this.uploadedFileEvidenceIds = []
            this.nextActDetailTask(3, doneTask)
        }
    }

  nextActDetailTask(id, doneTask) {
        this.loadingActionTask = true
      let params
      if (this.filesEdit.length > 0) {
          this.filesEdit.forEach(x => {
              this.uploadedFileEvidenceIds.push(x.filesId)
          })
      }
      if (id == 2) {
          params = {
              fileIds: [],
              done: false,
              reject: true,
              rejectedMessage: this.rejectMessage,
          }
      } else if (id == 1) {
          params = {
              fileIds: [],
              done: false,
              reject: false,
              rejectedMessage: null,
          }
      } else if (id == 3) {
          if (doneTask) {
              params = {
                  fileIds: this.uploadedFileEvidenceIds,
                  done: doneTask,
                  reject: false,
                  rejectedMessage: null,
              }
          } else if (doneTask == false) {
              params = {
                  fileIds: this.uploadedFileEvidenceIds,
                  done: doneTask,
                  reject: false,
                  rejectedMessage: null,
              }
          } else {
              params = {
                  fileIds: this.uploadedFileEvidenceIds,
                  done: this.changeStatus,
                  reject: false,
                  rejectedMessage: null,
              }
          }

      }
      this.service.editTask(this.idDetailTask, params).subscribe(
          resp => {
              if (resp.success) {
                  this.loadingActionTask = false
                  this.closeModal()
                  this.getDetailAddTask()
                  this.fileUploadControl.clear()
              }
          }, error => {
              this.loadingActionTask = false
              this.swalAlert.showAlertSwal(error)
          }
      )
  }

    /** Function Start Loading */
    startLoading() {
        if (this.finishLoadDataCount == 0) {
            this.finishLoadDataCount ++
            this.finishLoadData = false
        } else {
            this.finishLoadDataCount ++
        }
    }

    /** Function Stop Loading */
    stopLoading() {
        this.finishLoadDataCount --
        if (this.finishLoadDataCount == 0) {
            this.finishLoadData = true

        }
    }

    changeStatusActive() {
        this.changeStatus = !this.changeStatus
        console.log(this.changeStatus)
    }

    goToListTask() {
        this.router.navigate(['task/list-task'])
    }

    addNewCommentForStaff() {
        this.loadingComment = true
        const params = {
            resourcesTaskId: this.idDetailTask,
            comment: this.addNewCommentChat,
            createdFrom: 2
        }
        this.service.addNewComment(params).subscribe(
            resp => {
                if (resp.success) {
                    this.loadingComment = false
                    this.getDetailAddTask()
                    this.addNewCommentChat = ''
                }
            }, error => {
                this.loadingComment = false
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

}


