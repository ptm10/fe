import {Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {AwpModel} from "../../../core/models/awp.model";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../core/services/Awp/awp.service";
import {leangueIndoDT} from "../../../shared/pagination-custom";
import {DataResponse} from "../../../core/models/dataresponse.models";
import {environment} from "../../../../environments/environment";
import {CommonCons} from "../../../shared/CommonCons";
import Swal from "sweetalert2";
import {ApprovalModel} from "../../../core/models/approval.model";
import {Router} from "@angular/router";
import {ResourceTaskModel} from "../../../core/models/resource-task.model";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import {DetailTaskModel} from "../../../core/models/detail-task.model";
import {AuthfakeauthenticationService} from "../../../core/services/authfake.service";
import {parse} from "date-fns";
import {AdministrationService} from "../../../core/services/Administration/administration.service";
import {ManagementHolidays} from "../../../core/models/permission.model";
import {SwalAlert} from "../../../core/helpers/Swal";

@Component({
  selector: 'app-list-task',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.scss']
})
export class ListTaskComponent implements OnInit {

  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;


  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtOptions2: DataTables.Settings = {};

  /**
   * variable
   */
  filterYear: number
  selectedYear: number
  start = 1
  finishLoadData = true
  finishLoadDataCount = 0
  listApprove: ResourceTaskModel[]
  listApprove2: DetailTaskModel[]
  listApproveDoing = []
  listApproveDone = []
  paramsLike = []
  filterTable: string
  filterName: string
  checkedButton: number
  selectedTask: any
  selectedStatus: any
  breadCrumbItems: Array<{}>;
  paramIs = []
  paramDateBetween = []

  paramIsPenugasan = []
  paramLikePenugasan = []
  jenisTugasConsultant = [
      {
        id: 4,
        name: 'Pesan'
      },
    {
      id: 5,
      name: 'Kegiatan'
    },
    {
      id: 6,
      name: 'Event'
    },
    {
      id: 7,
      name: 'Draft Laporan Kemajuan Event'
    },
    {
      id: 8,
      name: 'Draft Laporan Event'
    },
    {
      id: 9,
      name: 'Laporan Kegiatan'
    },
  ]
  jenisTugasStaff = [
      {
        id: 4,
        name: 'Pesan'
      },
      {
        id: 7,
        name: 'Draft Laporan Kemajuan Event'
      },
      {
        id: 8,
        name: 'Draft Laporan Event'
      },
      {
        id: 9,
        name: 'Laporan Kegiatan'
      },
  ]
  jenisTugasAdmin = [
    {
      id: 4,
      name: 'Pesan'
    },
    {
      id: 1,
      name: 'Pemindahan Role'
    },
    {
      id: 5,
      name: 'Kegiatan'
    },
    {
      id: 6,
      name: 'Event'
    },
    {
      id: 7,
      name: 'Draft Laporan Kemajuan Event'
    },
    {
      id: 8,
      name: 'Draft Laporan Event'
    },
    {
      id: 9,
      name: 'Laporan Kegiatan'
    },
  ]
  JenisTugasExist = []
  JenisTugas = []

  statusTugas = [
    {
      id: 0,
      name: 'Tugas Baru'
    },
    {
      id: 1,
      name: 'Diproses'
    },
    {
      id: 2,
      name: 'Disetujui / Selesai'
    },
    {
      id: 3,
      name: 'Ditolak / Direvisi'
    },
    {
      id: 4,
      name: 'Telah Dikerjakan'
    }
  ]

  statusTugasExist = []

  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []

  holidayManagement: ManagementHolidays[]
  listHolidays = []

  constructor(
      private http: HttpClient,
      private service: AwpService,
      private router: Router,
      private calendar: NgbCalendar,
      public roles: AuthfakeauthenticationService,
      public formatter: NgbDateParserFormatter,
      private serviceAdministration: AdministrationService,
      private swalAlert: SwalAlert
  ) {}

  ngOnInit(): void {
    if (this.roles.hasAuthority('Administrator') ) {
      this.JenisTugas = this.jenisTugasAdmin
    } else if (this.roles?.hasAuthority('Consultan') || this.roles?.hasAuthority('Coordinator')) {
      this.JenisTugas = this.jenisTugasConsultant
    } else {
      this.JenisTugas = this.jenisTugasStaff
    }
    this.JenisTugasExist = this.JenisTugas
    this.statusTugasExist = this.statusTugas
    this.breadCrumbItems = [{ label: 'Profil' }, { label: 'Tugas' }, { label: 'Daftar Tugas', active: true }];
    this.getDataTable()
    this.checkedButton = 1
    this.dataTableTask()
    this.getListHolidayManagement()
  }

  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  getDataTable() {
    // this.startLoading()
    this.dtOptions2 = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramLikePenugasan,
          paramIs: [],
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverResource + 'resources-task/datatable-supervisior', params, CommonCons.getHttpOptions()).subscribe(resp => {
          // this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.listApprove2 = resp.data.content
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'id' }, { data: 'createdByResources.user.firstName'}, { data: 'createdForResources.user.firstName'}, {data: 'title'}, {data: 'description'}, {data: 'tags'}, {data: 'status'}, {data: 'createdAt'}, { data: 'id' }],
      order: [[7, 'desc']]
    };
  }

  dataTableTask() {
    // this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramIs,
          paramIn: [],
          paramNotIn: [],
          paramDateBetween: this.paramDateBetween,
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverResource + 'task/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          // this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.listApprove = resp.data.content
          } else {
            // this.finishLoadData = true
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{data: 'id'}, { data: 'createdByUser.firstName'}, {data: 'taskType'}, {data: 'description'}, {data: 'status'}, {data: 'createdAt'}, {data: 'id'}],
      order: [[5, 'desc']]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    if (this.checkedButton == 1) {
      this.paramsLike = []
      this.paramIs = []
      this.paramDateBetween = []
      if (this.selectedTask) {
        const selectedTaskType = {
          field: "taskType",
          dataType: "int",
          value: this.selectedTask.id
        }
        this.paramIs.push(selectedTaskType)
      }

      if (this.selectedStatus) {
        const selectedStatus = {
          field: "status",
          dataType: "int",
          value: this.selectedStatus.id
        }
        this.paramIs.push(selectedStatus)
      }

      if (this.fromDate && this.toDate) {
        let startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
        let endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')
        const selectedDate = {
          field: "createdAt",
          startDate: startDate,
          endDate: endDate
        }
        this.paramDateBetween.push(selectedDate)
      }

      if (this.filterName) {
        const paramsFirstName = {
          field: "createdByUser.firstName",
          dataType: "string",
          value: this.filterName
        }
        const paramsLastName =  {
          field: "createdByUser.lastName",
          dataType: "string",
          value: this.filterName
        }

        const paramsDescription =  {
          field: "description",
          dataType: "string",
          value: this.filterName
        }

        this.paramsLike.push(paramsLastName)
        this.paramsLike.push(paramsFirstName)
        this.paramsLike.push(paramsDescription)
      } else {
        this.paramsLike = []
      }
    } else if (this.checkedButton == 2) {
      this.paramIsPenugasan = []
      this.paramLikePenugasan = []

      if (this.filterName) {
        const paramsFirstName = {
          field: "createdByResources.user.firstName",
          dataType: "string",
          value: this.filterName
        }

        const paramsLastNameCreated = {
          field: "createdByResources.user.lastName",
          dataType: "string",
          value: this.filterName
        }

        const paramsFirstNameReciever = {
          field: "createdForResources.user.firstName",
          dataType: "string",
          value: this.filterName
        }

        const paramsLastNameReciever = {
          field: "createdForResources.user.lastName",
          dataType: "string",
          value: this.filterName
        }

        const paramsLastName =  {
          field: "title",
          dataType: "string",
          value: this.filterName
        }

        const paramsDescription =  {
          field: "description",
          dataType: "string",
          value: this.filterName
        }

        this.paramLikePenugasan.push(paramsLastName)
        this.paramLikePenugasan.push(paramsFirstName)
        this.paramLikePenugasan.push(paramsDescription)
        this.paramLikePenugasan.push(paramsLastNameCreated)
        this.paramLikePenugasan.push(paramsFirstNameReciever)
        this.paramLikePenugasan.push(paramsLastNameReciever)
      } else {
        this.paramLikePenugasan = []
      }
    }

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  goToDetailTask(data) {
    if (data.taskType == 1) {
      this.router.navigate(['task/detail-task/' + data.taskId])
    } else if (data.taskType == 2) {
      this.router.navigate(['administrasi/detail-approve-permission/' + data.taskId])
    } else if (data.taskType == 3) {
      this.router.navigate(['administrasi/detail-application-event/' + data.taskId])
    } else if (data.taskType == 4) {
      this.router.navigate(['task/detail-add-task-staff/' + data.taskId])
    } else if (data.taskType == 5) {
      this.router.navigate(['implement/detail-kegiatan-perencanaan/' + data.taskId])
    } else if (data.taskType == 6) {
      this.router.navigate(['activity/detail-event/' + data.taskId])
    } else if (data?.taskType == 7) {
      this.router.navigate(['activity/detail-progress-event/' + data.taskId])
    } else if (data?.taskType == 8) {
      this.router.navigate(['activity/detail-report-event/' + data.taskId + '/-'])
    } else if (data?.taskType == 9) {
      this.router.navigate(['implement/detail-report-activity/' + data.taskId])
    }
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  changeTask(data) {
    this.selectedTask = data
    this.refreshDatatable()
  }

  clearChangeTask() {
    this.selectedTask = null
    this.refreshDatatable()
  }

  changeStatus(data) {
    this.selectedStatus = data
    this.refreshDatatable()
  }

  clearChangeStatus() {
    this.selectedStatus = null
    this.refreshDatatable()
  }

  clearFilterDate() {
    this.fromDate = null
    this.toDate = null
    this.refreshDatatable()
  }

  goToAddTask() {
    this.router.navigate(['task/add-task'])
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.JenisTugas = this.JenisTugasExist.filter((product) => {
      return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
    });
  }

  searchFilterStatus(e) {
    const searchStr = e.target.value
    this.statusTugas = this.statusTugasExist.filter((product) => {
      return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
    });
  }

  goToDetailAddTask(id) {
    this.router.navigate(['task/detail-add-task-supervisor/' + id])
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      this.refreshDatatable()
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  getListHolidayManagement() {
    this.listHolidays = []
    this.serviceAdministration.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
