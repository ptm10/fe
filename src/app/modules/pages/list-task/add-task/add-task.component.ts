import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FileUploadControl} from "@iplab/ngx-file-upload";
import {Subject} from "rxjs";
import Swal from "sweetalert2";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {LoadImageService} from "ngx-image-cropper/lib/services/load-image.service";
import {FileService} from "../../../../core/services/Image/file.service";
import {resourcesModel} from "../../../../core/models/resources.model";
import {PDOModel} from "../../../../core/models/PDO.model";
import {NgbCalendar, NgbDate, NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import {parse} from "date-fns";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {UploadFileService} from "../../../../core/services/Upload-File/upload-file.service";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent implements OnInit {

  public fileUploadControl = new FileUploadControl({ listVisible: false, accept: ['.pdf'], multiple: false});
  breadCrumbItems: Array<{}>;
  selectedCity = []
  selectValue: string[];
  finishLoadData = true
  finishLoadDataCount = 0
  resourcesReplacement: resourcesModel[]
  listResourceReplacementExist: resourcesModel[]
  checkedUser = []
  selectedResourceModel: resourcesModel
  listTag: PDOModel[]
  index = 0;
  selectedTag = []
  newSelectedTag = []
  loadingSaveTask = false

  pdoModel: PDOModel[]
  pdoModelExist: PDOModel[]
  selectPdo: PDOModel

  iriModel: PDOModel[]
  iriModelExist: PDOModel[]
  selectIri: PDOModel
  disabledDates:NgbDateStruct[] = []
  holidayManagement: ManagementHolidays[]
  listHolidays = []
  uploadedFileEvidenceIds = []

  minDate = undefined;
  selectedFinishTaskDate: any
  tagName = []

  // save form
  tittleTask: string
  descriptionTask: string

  constructor(
      private router: Router,
      private service: AdministrationService,
      private serviceImage: FileService,
      private calendar: NgbCalendar,
      private swalAlert: SwalAlert
  ) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate()
    };
  }


  ngOnInit(): void {
    // this.cities = this.citiesx
    this.breadCrumbItems = [{ label: 'Profil' }, { label: 'Tugas' },{ label: 'Daftar Tugas' }, { label: 'Tambah Tugas', active: true }];
    // this.selectValue = ['Alaska', 'Hawaii', 'California', 'Nevada', 'Oregon', 'Washington', 'Arizona', 'Colorado', 'Idaho', 'Montana', 'Nebraska', 'New Mexico', 'North Dakota', 'Utah', 'Wyoming', 'Alabama', 'Arkansas', 'Illinois', 'Iowa'];
    this.listReplacementResources()
    this.getListPdo()
    this.getListIri()
    this.getListTag()
    this.getListHolidayManagement()
  }

  goToListTask() {
    this.router.navigate(['task/list-task'])
  }


  CreateNew(city){
    return city
  }

  doUpload(index) {
    this.loadingSaveTask = true
    if (this.fileUploadControl.value.length != 0) {
      if (index === 0) {
        this.uploadedFileEvidenceIds = [];
      }
      this.service.upload(this.fileUploadControl.value[index], 7).subscribe(data => {
        console.log(data)
        this.uploadedFileEvidenceIds.push(data.data[0].id);
        if (this.fileUploadControl.value.length > (index+1)) {
          this.doUpload(index+1);
        } else {
          this.saveTask();
        }
      }, error => {
        this.loadingSaveTask = false
        this.swalAlert.showAlertSwal(error)
      });
    } else {
      this.uploadedFileEvidenceIds = []
      this.saveTask()
    }
  }

  saveTask() {
    let tagIds = []
    let newTagIds = []
    const results = this.listTag.filter(({ name: id1 }) => this.selectedCity.some(id2 => id2 === id1));
    const results2 = this.selectedCity.filter(id1 => !this.listTag.some(({name: id2}) => id2 === id1));

    if (results) {
      results.forEach(data => {
        tagIds.push(data.id)
      })
    } else {
      tagIds = []
    }

    let startDate = this.selectedFinishTaskDate.year.toString() + '-' + this.selectedFinishTaskDate.month.toString().padStart(2, '0') + '-' + this.selectedFinishTaskDate.day.toString().padStart(2, '0')

    const params = {
      title: this.tittleTask,
      createdFor: this.selectedResourceModel.id,
      deadlineDate: startDate,
      description: this.descriptionTask,
      pdoId: this.selectPdo.id,
      iriId: this.selectIri.id,
      fileIds: this.uploadedFileEvidenceIds,
      tagIds: tagIds,
      newTagNames: results2
    }
    this.service.saveTask(params).subscribe(
        resp => {
          if (resp.success) {
            console.log(resp)
            this.loadingSaveTask = false
            this.router.navigate(['task/list-task'])
          }
        }, error => {
          this.loadingSaveTask = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }
  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6


  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }
  searchFilter(e) {
    const searchStr = e.target.value
    let filter = this.listResourceReplacementExist
    if (this.selectedResourceModel) {
      filter = filter.filter(data => data.id != this.selectedResourceModel.id)
    } else {
      filter = this.listResourceReplacementExist
    }
    this.resourcesReplacement = filter.filter((product) => {
      return ((product?.user?.firstName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product?.user?.firstName !== null) || (product?.user?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product?.user?.lastName !== null))
    });
    this.checkedUser = this.resourcesReplacement
  }

  selectResourcePlacement(data) {
    this.selectedResourceModel = null
    this.resourcesReplacement = this.listResourceReplacementExist

    this.selectedResourceModel = data
    this.resourcesReplacement = this.resourcesReplacement.filter(dataFilter => dataFilter.id != this.selectedResourceModel.id )
    this.checkedUser = this.resourcesReplacement
  }

  listReplacementResources() {
    this.startLoading()
    this.service.resourcesReplacement().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.resourcesReplacement = resp.data.content
            this.listResourceReplacementExist = resp.data.content
            this.checkedUser = resp.data.content
            this.resourcesReplacement.forEach(dataResource => {
              if (dataResource?.user?.profilePicture?.id) {
                this.serviceImage.getUrlPhoto(dataResource?.user?.profilePicture?.id).subscribe(
                    resp => {
                      let reader = new FileReader();
                      reader.addEventListener("load", () => {
                        dataResource.filePhoto = reader.result;
                      }, false);
                      reader.readAsDataURL(resp);
                    }
                )
              } else {
                dataResource.filePhoto = null
              }
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  searchFilterPdo(e) {
    const searchStr = e.target.value
    let filter = this.pdoModelExist
    if (this.selectPdo) {
      filter = filter.filter(data => data.id != this.selectPdo.id)
    } else {
      filter = this.pdoModelExist
    }
    this.pdoModel = filter.filter((product) => {
      return (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 )
    });
  }

  selectedPdo(data) {
    this.selectPdo = null
    this.pdoModel = this.pdoModelExist

    this.selectPdo = data
    this.pdoModel = this.pdoModelExist.filter(dataFilter => dataFilter.id != this.selectPdo.id )
  }

  getListPdo() {
    this.startLoading()
    this.service.listPdo().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.pdoModel = resp.data.content
            this.pdoModelExist = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  searchFilterIri(e) {
    const searchStr = e.target.value
    let filter = this.iriModelExist
    if (this.selectIri) {
      filter = filter.filter(data => data.id != this.selectIri.id)
    } else {
      filter = this.iriModelExist
    }
    this.iriModel = filter.filter((product) => {
      return (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 )
    });
  }

  selectedIri(data) {
    this.selectIri = null
    this.iriModel = this.iriModelExist

    this.selectIri = data
    this.iriModel = this.iriModelExist.filter(dataFilter => dataFilter.id != this.selectIri.id )
  }

  getListIri() {
    this.startLoading()
    this.service.listIri().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.iriModel = resp.data.content
            this.iriModelExist = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListTag() {
    this.startLoading()
    let tag = []
    this.service.listTag().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            tag = []
            this.listTag = resp.data.content
            this.listTag.forEach(x => {
              tag.push(x.name)
            })
            this.tagName = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListHolidayManagement() {
    this.listHolidays = []
    this.service.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
