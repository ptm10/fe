import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import Swal from "sweetalert2";
import {DetailTaskModel} from "../../../../core/models/detail-task.model";
import {FileService} from "../../../../core/services/Image/file.service";
import {resourcesModel} from "../../../../core/models/resources.model";
import {NgbCalendar, NgbDate, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {parse} from "date-fns";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {PDOModel} from "../../../../core/models/PDO.model";
import {FileUploadControl} from "@iplab/ngx-file-upload";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-add-task-supervisor',
  templateUrl: './detail-add-task.component.html',
  styleUrls: ['./detail-add-task.component.scss']
})
export class DetailAddTaskComponent implements OnInit {
    @ViewChild('scrollEle') scrollEle;
    @ViewChild('scrollRef') scrollRef;

    public fileUploadControl = new FileUploadControl({ listVisible: false, accept: ['.pdf'], multiple: false});
    idDetailAddTask: string
  detailTask: DetailTaskModel
  detailPage: number
  breadCrumbItems: Array<{}>;
  selectedResourceModel: resourcesModel
    editModelDetailTask: DetailTaskModel
    finishLoadData = true
    finishLoadDataCount = 0
    resourcesReplacement: resourcesModel[]
    listResourceReplacementExist: resourcesModel[]
    checkedUser = []
    minDate = undefined;
    disabledDates:NgbDateStruct[] = []
    editDate: any
    listTag: PDOModel[]
    holidayManagement: ManagementHolidays[]
    listHolidays = []
    tagName = []
    loadingEdit = false

    pdoModel: PDOModel[]
    pdoModelExist: PDOModel[]
    selectPdo: PDOModel
    changeStatus: boolean

    iriModel: PDOModel[]
    iriModelExist: PDOModel[]
    selectIri: PDOModel
    selectedCity = []
    uploadedFileEvidenceIds = []
    loadingSavePdf = false
    filesEdit = []

    addNewCommentChat: string
    loadingComment = false
    loadingUpdateTask = false
    loadingDeletedTask = false

    constructor(
      private route: ActivatedRoute,
      private service: AdministrationService,
      private serviceImage: FileService,
      private calendar: NgbCalendar,
      private router: Router,
      private modalService: NgbModal,
      private swalAlert: SwalAlert
    ) {
        const current = new Date();
        this.minDate = {
            year: current.getFullYear(),
            month: current.getMonth() + 1,
            day: current.getDate()
        };
    }

  ngOnInit(): void {
        this.getListHolidayManagement()
        this.detailPage = 1
        this.breadCrumbItems = [{ label: 'Profil' }, { label: 'Tugas' },{ label: 'Penugasan' }, { label: 'Detail Tugas', active: true }];
        this.idDetailAddTask = this.route.snapshot.paramMap.get('id')
        this.getDetailAddTask()
        this.getListTag()
      this.onListScroll()
  }

    isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

    myClass(date:NgbDateStruct) {
        let isSelected=this.disabledDates
            .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
        return isSelected?'classSelected':'classNormal'
    }

  goToEditPage() {
      this.detailPage = 2
  }

    CreateNew(city){
        return city
    }

    /**
     * Open extra large modal
     * @param selectedMenu extra large modal data
     */
    openModal(selectedMenu: any) {
        this.modalService.open(selectedMenu, { size: 'lg', centered: true });
    }

    /** * close modal */
    closeModal(){
        this.modalService.dismissAll()
    }


    getDetailAddTask() {
        this.startLoading()
      this.service.detailAddTask(this.idDetailAddTask).subscribe(
        resp => {
            this.stopLoading()
          if (resp.success) {
              this.selectedCity = []
              this.uploadedFileEvidenceIds = []
              this.filesEdit = []
              this.detailTask = resp.data
              this.changeStatus = this.detailTask.doneByReceiver
              // this.listTaskModel.convertDateUpdate = parse(this.listTaskModel.createdAt, 'dd-MM-yyyy HH:mm:ss', new Date());

              this.detailTask.convertUpdatedAt = parse(this.detailTask.updatedAt, 'dd-MM-yyyy HH:mm:ss', new Date())
              this.editModelDetailTask = Object.assign({}, this.detailTask)
              this.editModelDetailTask.files.forEach(dataFiles => {
                  if (dataFiles.uploadedFrom == 1) {
                      this.filesEdit.push(dataFiles)
                  }
              })
              this.editModelDetailTask.convertDate = new Date(this.editModelDetailTask.deadlineDate)
              this.editDate = new NgbDate(this.editModelDetailTask.convertDate.getFullYear(), this.editModelDetailTask.convertDate.getMonth() + 1, this.editModelDetailTask.convertDate.getDate())
              this.editModelDetailTask.tags.forEach(x => {
                  this.selectedCity.push(x.taskResourcesTag.name)
              })
              if (this.detailTask?.createdForResources?.user?.profilePicture?.id) {
                  this.serviceImage.getUrlPhoto(this.detailTask?.createdForResources?.user?.profilePicture?.id).subscribe(
                      resp => {
                          let reader = new FileReader();
                          reader.addEventListener("load", () => {
                              this.detailTask.filePhoto = reader.result;
                              this.editModelDetailTask.filePhoto = reader.result;
                          }, false);
                          reader.readAsDataURL(resp);
                      }
                  )
              } else {
                  this.detailTask.filePhoto = null
                  this.editModelDetailTask.filePhoto = null
              }

              if (this.detailTask?.createdByResources?.user?.profilePicture?.id) {
                  this.serviceImage.getUrlPhoto(this.detailTask?.createdByResources?.user?.profilePicture?.id).subscribe(
                      resp => {
                          let reader = new FileReader();
                          reader.addEventListener("load", () => {
                              this.detailTask.filePhotoCreatedTask = reader.result;
                          }, false);
                          reader.readAsDataURL(resp);
                      }
                  )
              } else {
                  this.detailTask.filePhotoCreatedTask = null
              }


              this.selectedResourceModel = this.editModelDetailTask.createdForResources
              console.log(this.selectedResourceModel)
              this.selectPdo = this.editModelDetailTask.pdo
              this.selectIri = this.editModelDetailTask.iri
              this.getListPdo()
              this.listReplacementResources()
              this.getListIri()
          }
        }, error => {
              this.stopLoading()
              this.swalAlert.showAlertSwal(error)
        }
    )
  }

    searchFilter(e) {
        const searchStr = e.target.value
        let filter = this.listResourceReplacementExist
        if (this.selectedResourceModel) {
            filter = filter.filter(data => data.id != this.selectedResourceModel.id)
        } else {
            filter = this.listResourceReplacementExist
        }
        this.resourcesReplacement = filter.filter((product) => {
            return (product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1)
        });
        this.checkedUser = this.resourcesReplacement
    }

    selectResourcePlacement(data) {
        this.selectedResourceModel = null
        this.resourcesReplacement = this.listResourceReplacementExist

        this.selectedResourceModel = data
        this.resourcesReplacement = this.resourcesReplacement.filter(dataFilter => dataFilter.id != this.selectedResourceModel.id )
        this.checkedUser = this.resourcesReplacement
    }

    listReplacementResources() {
        this.startLoading()
        this.service.resourcesReplacement().subscribe(
            resp => {
                this.stopLoading()
                if (resp.success) {
                    this.resourcesReplacement = resp.data.content
                    this.listResourceReplacementExist = resp.data.content
                    this.checkedUser = resp.data.content
                    this.resourcesReplacement.forEach(dataResource => {
                        if (dataResource?.user?.profilePicture?.id) {
                            this.serviceImage.getUrlPhoto(dataResource?.user?.profilePicture?.id).subscribe(
                                resp => {
                                    let reader = new FileReader();
                                    reader.addEventListener("load", () => {
                                        dataResource.filePhoto = reader.result;
                                        this.selectedResourceModel.filePhoto = reader.result;
                                    }, false);
                                    reader.readAsDataURL(resp);
                                }
                            )
                        } else {
                            this.selectedResourceModel.filePhoto = null
                        }

                    })
                    this.resourcesReplacement = this.resourcesReplacement.filter(dataFilter => dataFilter.id != this.selectedResourceModel.id)
                }
            }, error => {
                this.stopLoading()
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    getListHolidayManagement() {
        this.listHolidays = []
        this.disabledDates = []
        this.startLoading()
        this.service.listHolidayManagement().subscribe(
            resp => {
                this.stopLoading()
                if (resp.success) {
                    this.holidayManagement = resp.data.content
                    this.holidayManagement.forEach(respHolidays => {
                        respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
                        this.disabledDates.push(
                            {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
                        )
                    })
                }
            }, error => {
                this.stopLoading()

                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    searchFilterPdo(e) {
        const searchStr = e.target.value
        let filter = this.pdoModelExist
        if (this.selectPdo) {
            filter = filter.filter(data => data.id != this.selectPdo.id)
        } else {
            filter = this.pdoModelExist
        }
        this.pdoModel = filter.filter((product) => {
            return (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 )
        });
    }

    selectedPdo(data) {
        this.selectPdo = null
        this.pdoModel = this.pdoModelExist

        this.selectPdo = data
        this.pdoModel = this.pdoModelExist.filter(dataFilter => dataFilter.id != this.selectPdo.id )
    }

    getListPdo() {
        this.startLoading()
        this.service.listPdo().subscribe(
            resp => {
                this.stopLoading()
                if (resp.success) {
                    this.pdoModel = resp.data.content
                    this.pdoModel = this.pdoModel.filter(dataPdo => dataPdo.id !== this.selectPdo.id)
                    this.pdoModelExist = resp.data.content
                }
            }, error => {
                this.stopLoading()
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    searchFilterIri(e) {
        const searchStr = e.target.value
        let filter = this.iriModelExist
        if (this.selectIri) {
            filter = filter.filter(data => data.id != this.selectIri.id)
        } else {
            filter = this.iriModelExist
        }
        this.iriModel = filter.filter((product) => {
            return (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 )
        });
    }

    selectedIri(data) {
        this.selectIri = null
        this.iriModel = this.iriModelExist

        this.selectIri = data
        this.iriModel = this.iriModelExist.filter(dataFilter => dataFilter.id != this.selectIri.id )
    }

    getListIri() {
        this.startLoading()
        this.service.listIri().subscribe(
            resp => {
                this.stopLoading()
                if (resp.success) {
                    this.iriModel = resp.data.content
                    this.iriModel = this.iriModel.filter(dataIri => dataIri.id !== this.selectIri.id)
                    this.iriModelExist = resp.data.content
                }
            }, error => {
                this.stopLoading()
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    getListTag() {
        this.startLoading()
        let tag = []
        this.service.listTag().subscribe(
            resp => {
                this.stopLoading()
                if (resp.success) {
                    tag = []
                    this.listTag = resp.data.content
                    this.listTag.forEach(x => {
                        tag.push(x.name)
                    })
                    this.tagName = tag
                }
            }, error => {
                this.stopLoading()
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    /** Function Start Loading */
    startLoading() {
        if (this.finishLoadDataCount == 0) {
            this.finishLoadDataCount ++
            this.finishLoadData = false
        } else {
            this.finishLoadDataCount ++
        }
    }

    /** Function Stop Loading */
    stopLoading() {
        this.finishLoadDataCount --
        if (this.finishLoadDataCount == 0) {
            this.finishLoadData = true

        }
    }

    doUpload(index) {
        this.loadingEdit = true
        if (this.fileUploadControl.value.length != 0) {
            if (index === 0) {
                this.uploadedFileEvidenceIds = [];
            }
            this.service.upload(this.fileUploadControl.value[index], 7).subscribe(data => {
                console.log(data)
                this.uploadedFileEvidenceIds.push(data.data[0].id);
                if (this.fileUploadControl.value.length > (index+1)) {
                    this.doUpload(index+1);
                } else {
                    this.updateTask();
                }
            }, error => {
                this.loadingEdit = false
                this.swalAlert.showAlertSwal(error)
            });
        } else {
            this.uploadedFileEvidenceIds = []
            this.updateTask()
        }
    }

    updateTask() {
        let startDate = this.editDate.year.toString() + '-' + this.editDate.month.toString().padStart(2, '0') + '-' + this.editDate.day.toString().padStart(2, '0')
        let tagIds = []
        let newTagIds = []
        const results = this.listTag.filter(({ name: id1 }) => this.selectedCity.some(id2 => id2 === id1));
        const results2 = this.selectedCity.filter(id1 => !this.listTag.some(({name: id2}) => id2 === id1));

        if (results) {
            results.forEach(data => {
                tagIds.push(data.id)
            })
        } else {
            tagIds = []
        }

        if (this.filesEdit.length > 0) {
            this.filesEdit.forEach(x => {
                this.uploadedFileEvidenceIds.push(x.filesId)
            })
        }

        const params = {
            title: this.editModelDetailTask.title,
            createdFor: this.selectedResourceModel.id,
            deadlineDate: startDate,
            description: this.editModelDetailTask.description,
            pdoId: this.editModelDetailTask.pdo.id,
            iriId: this.editModelDetailTask.iri.id,
            fileIds: this.uploadedFileEvidenceIds,
            tagIds: tagIds,
            newTagNames: results2,
            done: false,
        }
        console.log(params)
        this.service.editTask(this.idDetailAddTask, params).subscribe(
            resp => {
                if (resp.success) {
                    this.loadingEdit = false
                    this.fileUploadControl.clear()
                    this.getDetailAddTask()
                    this.detailPage = 1
                }
            }, error => {
                this.loadingEdit = false
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    updateTaskFormReciever(statusTask) {
        let params
        let doneTask
        this.loadingUpdateTask = true
        let fileExisting = []
        let fileTags = []
        this.detailTask.files.forEach(data => {
            if (data.uploadedFrom == 1) {
                fileExisting.push(data.filesId)
            }
        })
        this.detailTask.tags.forEach(dataTags => {
            fileTags.push(dataTags.taskResourcesTagId)
        })
        if (statusTask == 1) {
            doneTask = true
        } else if (statusTask == 2) {
            doneTask = false
        }
        params = {
            title: this.detailTask.title,
            createdFor: this.detailTask.createdFor,
            deadlineDate: this.detailTask.deadlineDate,
            description: this.detailTask.description,
            pdoId: this.detailTask.pdo.id,
            iriId: this.detailTask.iri.id,
            fileIds: fileExisting,
            tagIds: fileTags,
            newTagNames: [],
            done: doneTask
        }
        this.service.editTask(this.idDetailAddTask, params).subscribe(
            resp => {
                if (resp.success) {
                    this.loadingUpdateTask = false
                    fileTags = []
                    fileExisting = []
                    this.loadingEdit = false
                    this.fileUploadControl.clear()
                    this.getDetailAddTask()
                    this.detailPage = 1
                }
            }, error => {
                this.loadingUpdateTask = false
                this.swalAlert.showAlertSwal(error)
            }
        )    }

    removeFileExist(data) {
        this.filesEdit.forEach((check, index) => {
            if (check.id == data.id) {
                this.filesEdit.splice(index, 1)
            }
        })
    }

    backToPage() {
        if (this.detailPage == 1) {
            this.router.navigate(['task/list-task'])
        } else if (this.detailPage == 2){
            this.detailPage = 1
        }
    }

    downloadPdf(id) {
        this.loadingSavePdf = true
        this.service.exportPdf(id).subscribe(
            resp => {
                this.loadingSavePdf = false
            }, error => {
                this.loadingSavePdf = false
            }
        )
    }

    changeStatusActive() {
        this.changeStatus = !this.changeStatus
        console.log(this.changeStatus)
    }

    addNewCommentForSupervisor() {
        this.loadingComment = true
        const params = {
            resourcesTaskId: this.idDetailAddTask,
            comment: this.addNewCommentChat,
            createdFrom: 1
        }
        this.service.addNewComment(params).subscribe(
            resp => {
                if (resp.success) {
                    this.loadingComment = false
                    this.getDetailAddTask()
                    this.addNewCommentChat = ''
                }
            }, error => {
                this.loadingComment = false
                this.swalAlert.showAlertSwal(error)
            }
        )
    }
    onListScroll() {
        if (this.scrollRef !== undefined) {
            setTimeout(() => {
                this.scrollRef.SimpleBar.getScrollElement().scrollTop =
                    this.scrollRef.SimpleBar.getScrollElement().scrollHeight + 1500;
            }, 500);
        }
    }

    goToListTask() {
        this.router.navigate(['task/list-task'])
    }
    deleteTaskGiven() {
        this.loadingDeletedTask = true
        this.service.deleteTask(this.idDetailAddTask).subscribe(
            resp => {
                if (resp.success) {
                    this.loadingDeletedTask = false
                    this.closeModal()
                    this.router.navigate(['task/list-task'])
                }
            }, error => {
                this.loadingDeletedTask = false
                this.swalAlert.showAlertSwal(error)
            }
        )
    }


}

