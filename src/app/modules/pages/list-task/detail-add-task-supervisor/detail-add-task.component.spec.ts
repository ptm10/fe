import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAddTaskComponent } from './detail-add-task.component';

describe('DetailAddTaskComponent', () => {
  let component: DetailAddTaskComponent;
  let fixture: ComponentFixture<DetailAddTaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailAddTaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAddTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
