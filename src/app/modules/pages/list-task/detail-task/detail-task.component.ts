import { Component, OnInit } from '@angular/core';
import {UsersModels} from "../../../../core/models/users.models";
import {ListTaskService} from "../../../../core/services/List-Task/list-task.service";
import {ActivatedRoute, Router} from "@angular/router";
import Swal from "sweetalert2";
import {environment} from "../../../../../environments/environment";
import {ApprovalModel} from "../../../../core/models/approval.model";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FileService} from "../../../../core/services/Image/file.service";
import {moment} from "ngx-bootstrap/chronos/testing/chain";
import {parse} from "date-fns";
import {HorizontaltopbarComponent} from "../../../../layouts/horizontaltopbar/horizontaltopbar.component";
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-task',
  templateUrl: './detail-task.component.html',
  styleUrls: ['./detail-task.component.scss']
})
export class DetailTaskComponent implements OnInit {

  urlPhotoProfile: any
  listTaskModel: ApprovalModel
  idDetailTask: string
  urlFiles = environment.serverFile
  loadingData = true
  rejectMessage: string
  loadingSaveData = true
  changeDate: any
  breadCrumbItems: Array<{}>;
  listRoleApprove = []

  constructor(
      private service: ListTaskService,
      private route: ActivatedRoute,
      private modalService: NgbModal,
      private router: Router,
      private serviceImage: FileService,
      private notification: HorizontaltopbarComponent,
      private serviceNotification: ProfileService,
      private swalAlert: SwalAlert
  ) {
    this.route.paramMap.subscribe(params => {
      this.ngOnInit()
    })
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Konfigurasi' },{ label: 'Role' }, { label: 'Approval Perubahan Role', active: true }];
    this.idDetailTask = this.route.snapshot.paramMap.get('id')
    this.getDetailTask()
  }

  getDetailTask() {
    this.loadingData = true
    this.listRoleApprove = []
    this.service.detailTask(this.idDetailTask).subscribe(
        resp => {
          if (resp.success) {
            this.listTaskModel = resp.data
            if (this.listTaskModel.approveStatus == 0) {
              this.updateStatusTask()
            }
            if (this.listTaskModel.approveStatus == 1) {
              const results = this.listTaskModel.user.roles.filter(({ roleId: id1 }) => !this.listTaskModel.detail.some(({ roleId: id2 }) => id2 === id1));
              this.listRoleApprove = results
            }

            this.listTaskModel.convertDateUpdate = parse(this.listTaskModel.createdAt, 'dd-MM-yyyy HH:mm:ss', new Date());

            if (this.listTaskModel?.user?.profilePicture?.id) {
              this.getFilePhotos()
            }
          this.loadingData = false
          } else {
            this.loadingData = false
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.loadingData = false
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Terjadi Kesalahan Pada Server',
          });
        }
    )
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'md', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  sendDataListTask(status) {
    this.loadingSaveData = false
    if (status == 1) {
      this.rejectMessage = null
    }
    const param = {
      approvalId: this.idDetailTask,
      type: 1,
      approveStatus: status,
      rejectedMessage: this.rejectMessage
    }
    this.service.acceptTask(param).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.getDetailTask()
            this.closeModal()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Terjadi Kesalahan Pada Server',
          });
        }
    )
  }

  getFilePhotos() {
    this.urlPhotoProfile = null
    this.serviceImage.getUrlPhoto(this.listTaskModel?.user?.profilePicture?.id).subscribe(
        resp => {
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.urlPhotoProfile = reader.result;
          }, false);
          reader.readAsDataURL(resp);
        }
    )
  }

  goToListTask() {
    this.router.navigate(['task/list-task'])
  }

  updateStatusTask() {
    const params = {
      taskId: this.idDetailTask,
      taskType: 1,
      status: 1
    }
    this.service.updateStatusTask(params).subscribe(
        resp => {
          if (resp.success) {
            this.notification.getListNotification()
            this.serviceNotification.updateNotification.emit()
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
