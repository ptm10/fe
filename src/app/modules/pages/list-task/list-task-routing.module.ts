import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {ListTaskComponent} from "./list-task.component";
import {DetailTaskComponent} from "./detail-task/detail-task.component";
import {AddTaskComponent} from "./add-task/add-task.component";
import {DetailAddTaskComponent} from "./detail-add-task-supervisor/detail-add-task.component";
import {DetailAddTaskStaffComponent} from "./detail-add-task-staff/detail-add-task-staff.component";

const routes: Routes = [
    {
        path: 'list-task',
        component: ListTaskComponent
    },
    {
        path: 'detail-task/:id',
        component: DetailTaskComponent
    },
    {
        path: 'add-task',
        component: AddTaskComponent
    },
    {
        path: 'detail-add-task-supervisor/:id',
        component: DetailAddTaskComponent
    },
    {
        path: 'detail-add-task-staff/:id',
        component: DetailAddTaskStaffComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ListTaskRoutingModule {}
