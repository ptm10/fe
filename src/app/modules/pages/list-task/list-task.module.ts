import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbDatepickerModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {ListTaskRoutingModule} from './list-task-routing.module'
import {DataTablesModule} from 'angular-datatables';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import {ListTaskComponent} from "./list-task.component";
import { DetailTaskComponent } from './detail-task/detail-task.component';
import {UIModule} from "../../../shared/ui/ui.module";
import {HorizontaltopbarComponent} from "../../../layouts/horizontaltopbar/horizontaltopbar.component";
import { AddTaskComponent } from './add-task/add-task.component';
import {FileUploadModule} from "@iplab/ngx-file-upload";
import { DetailAddTaskComponent } from './detail-add-task-supervisor/detail-add-task.component';
import { DetailAddTaskStaffComponent } from './detail-add-task-staff/detail-add-task-staff.component';
import {SimplebarAngularModule} from "simplebar-angular";
import {SwalAlert} from "../../../core/helpers/Swal";

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;


@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [ListTaskComponent, DetailTaskComponent, AddTaskComponent, DetailAddTaskComponent, DetailAddTaskStaffComponent],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        ListTaskRoutingModule,
        DataTablesModule,
        NgbTooltipModule,
        NgxMaskModule.forRoot(),
        NgbDatepickerModule,
        UIModule,
        FileUploadModule,
        SimplebarAngularModule,
    ],
  providers: [
      HorizontaltopbarComponent,
      SwalAlert
  ]
})
export class ListTaskModule { }
