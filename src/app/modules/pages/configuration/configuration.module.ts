import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common'
import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbAlertModule, NgbDatepickerModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {ConfigurationRoutingModule} from './configuration-routing.module'
import {DataTablesModule} from "angular-datatables";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {FileUploadModule} from "@iplab/ngx-file-upload";
import {DetailPenggunaComponent} from "./detail-pengguna/detail-pengguna.component";
import {AddPenggunaComponent} from "./add-pengguna/add-pengguna.component";
import {ConfigurationComponent} from "./configuration.component";
import {UIModule} from "../../../shared/ui/ui.module";
import { ModalComponent } from './add-pengguna/modal/modal.component';
import {SwalAlert} from "../../../core/helpers/Swal";
import { FormEventComponent } from './form-event/form-event.component';
import { CreateFormEventComponent } from './form-event/create-form-event/create-form-event.component';
import { EditFormEventComponent } from './form-event/edit-form-event/edit-form-event.component';
import {ArchwizardModule} from "angular-archwizard";

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [DetailPenggunaComponent, AddPenggunaComponent, ConfigurationComponent, ModalComponent, FormEventComponent, CreateFormEventComponent, EditFormEventComponent],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        ConfigurationRoutingModule,
        NgbTooltipModule,
        DataTablesModule,
        PdfViewerModule,
        FileUploadModule,
        NgbAlertModule,
        UIModule,
        NgbDatepickerModule,
        ArchwizardModule
    ],
  providers: [
      DatePipe,
      SwalAlert
  ]
})
export class ConfigurationModule { }
