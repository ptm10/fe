import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {environment} from "../../../../../environments/environment";
import {UsersModels} from "../../../../core/models/users.models";
import {RoleUsers} from "../../../../core/models/RoleUsers";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ConfigurationService} from "../../../../core/services/Configuration/configuration.service";
import {FileService} from "../../../../core/services/Image/file.service";
import {DatePipe} from "@angular/common";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {leangueIndoDT} from "../../../../shared/pagination-custom";
import {DataResponse} from "../../../../core/models/dataresponse.models";
import {CommonCons} from "../../../../shared/CommonCons";
import {parse} from "date-fns";
import {PDOModel} from "../../../../core/models/PDO.model";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {Event} from "../../../../core/models/awp.model";
import {FormQuestionEvent} from "../../../../core/models/event.model";

@Component({
  selector: 'app-form-event',
  templateUrl: './form-event.component.html',
  styleUrls: ['./form-event.component.scss']
})
export class FormEventComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  paramsLike = []
  paramsIn = []
  finishLoadDataCount = 0
  finishLoadData = true
  filterName: string
  start = 1
  pdfSrc: any
  loadingSavePdf = false
  breadCrumbItems: Array<{}>;
  checkedRoles = []
  listEvent: Event[]
  listEventForm: FormQuestionEvent[]
  listAllEventForm: FormQuestionEvent[]
  selectedEventType: FormQuestionEvent
  selectedDetailEventType: FormQuestionEvent
  checkedButton: number
  // loading delete
  loadingDeleteEventForm = false

  // tag
  tagName = []
  selectedTag: string
  listTag: []

  existingQuestionCompliance = []
  existingQuestionEconomy = []
  existingQuestionEfective = []
  existingQuestionEfisiens = []
  existingQuestionParticipantCompliance = []
  existingQuestionParticipantEconomy = []
  existingQuestionParticipantEffective = []
  existingQuestionParticipantEffisiens = []
  existingQuestionParticipantOtherQuestion = []

  constructor(
      private http: HttpClient,
      private router: Router,
      private modalService: NgbModal,
      private service: ConfigurationService,
      private serviceImage: FileService,
      private datepipe: DatePipe,
      private swalAlert: SwalAlert,
      private serviceAwp: AwpService
  ) { }

  ngOnInit(): void {
    this.checkedButton = 1
    this.breadCrumbItems = [{ label: 'Konfigurasi' }, { label: 'Formulir', active: true }];
    // this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: [],
          paramIn: this.paramsIn,
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'event-form/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          // this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.listEventForm = resp.data.content
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'eventType.name' }, { data: 'eventType.name'}, {data: 'id'}]
    };
    this.getAllForm()
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.paramsLike = []
    this.paramsIn = []

    if (this.filterName) {
      const paramsFirstName = {
        field: "eventType.name",
        dataType: "string",
        value: this.filterName
      }
      this.paramsLike.push(paramsFirstName)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  openModalDelete(selectedMenu: any, data: FormQuestionEvent) {
    this.selectedEventType = null
    this.selectedEventType = data
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  openModalDetail(selectedMenu: any, data: FormQuestionEvent) {
    this.existingQuestionCompliance = []
    this.existingQuestionEconomy = []
    this.existingQuestionEfective = []
    this.existingQuestionEfisiens = []
    this.existingQuestionParticipantCompliance = []
    this.existingQuestionParticipantEconomy = []
    this.existingQuestionParticipantEffective = []
    this.existingQuestionParticipantEffisiens = []
    this.existingQuestionParticipantOtherQuestion = []
    this.selectedDetailEventType = null
    this.selectedDetailEventType = data
    this.selectedDetailEventType?.questions.forEach(x => {
      if (x.createdFor === 1) {
        if (x.questionCategory === 1) {
          this.existingQuestionCompliance.push(x)
        } else if (x.questionCategory === 2) {
          this.existingQuestionEconomy.push(x)
        } else if (x.questionCategory === 3) {
          this.existingQuestionEfective.push(x)
        } else if (x.questionCategory === 4) {
          this.existingQuestionEfisiens.push(x)
        }
      } else {
        if (x.questionCategory === 1) {
          this.existingQuestionParticipantCompliance.push(x)
        } else if (x.questionCategory === 2) {
          this.existingQuestionParticipantEconomy.push(x)
        } else if (x.questionCategory === 3) {
          this.existingQuestionParticipantEffective.push(x)
        } else if (x.questionCategory === 4) {
          this.existingQuestionParticipantEffisiens.push(x)
        } else if (x.questionCategory === 5) {
          this.existingQuestionParticipantOtherQuestion.push(x)
        }
      }

    })
    this.modalService.open(selectedMenu, { size: 'xl', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.selectedEventType = null
    this.modalService.dismissAll()
  }

  goToAddForm(data) {
    let filterEventType
    filterEventType = this.listEvent.filter(x => x.name === data)
    this.closeModal()
    this.router.navigate(['configuration/add-event/' + filterEventType[0].id])
  }

  goToEditForm(data) {
    this.closeModal()
    this.router.navigate(['configuration/edit/' + data?.id])
  }

  deleteForm() {
    this.loadingDeleteEventForm = true
    this.serviceAwp.deleteQuestionEvent(this.selectedEventType?.id).subscribe(
        resp => {
          if (resp.success) {
            this.closeModal()
            this.loadingDeleteEventForm = false
            this.refreshDatatable()
            this.getAllForm()
          }
        }, error => {
          this.loadingDeleteEventForm = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * Get List Formulir */

  getAllForm() {
    this.startLoading()
    this.serviceAwp.listAllForm().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listAllEventForm = resp.data.content
            this.dataEvent()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End Get List Formulir */


  /** * List Event */
  dataEvent() {
    let tag = []
    this.startLoading()
    this.serviceAwp.eventType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEvent = resp.data.content
            let data = this.listEvent.filter(({ id: id1 }) => !this.listAllEventForm.some(({ eventType: id2 }) => id2.id === id1))
            data.forEach(x => {
              tag.push(x.name)
            })
            this.tagName = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End List Event */

}
