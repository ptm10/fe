import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFormEventComponent } from './create-form-event.component';

describe('CreateFormEventComponent', () => {
  let component: CreateFormEventComponent;
  let fixture: ComponentFixture<CreateFormEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateFormEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFormEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
