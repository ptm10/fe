import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AwpService} from "../../../../../core/services/Awp/awp.service";
import {SwalAlert} from "../../../../../core/helpers/Swal";
import {QuestionEvent} from "../../../../../core/models/event.model";
import {Event} from "../../../../../core/models/awp.model";

@Component({
  selector: 'app-create-form-event',
  templateUrl: './create-form-event.component.html',
  styleUrls: ['./create-form-event.component.scss']
})
export class CreateFormEventComponent implements OnInit {

  // loading
  finishLoadDataCount = 0
  finishLoadData = true

  breadCrumbItems: Array<{}>;
  newQuestionCompliance = []
  newQuestionEconomy = []
  newQuestionEfective = []
  newQuestionEfisiens = []
  newQuestionParticipantCompliance = []
  newQuestionParticipantEconomy = []
  newQuestionParticipantEfective = []
  newQuestionParticipantEfisiens = []
  newQuestionParticipantOtherQuestion = []
  selectValueQustion: string[] = [];
  idEventType: string
  listEvent: Event[]
  eventTypeName: string
  loadingCreateForm = false

  // question event
  listQuestion: [] = []

  constructor(
      private router: Router,
      private service: AwpService,
      private swalAlert: SwalAlert,
      private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Konfigurasi' }, { label: 'Formulir' }, { label: 'Tambah', active: true }];
    this.idEventType = this.activatedRoute.snapshot.paramMap.get('id')
    this.getListQuestionExisting()
    this.dataEvent()
  }

  goToListQuestion() {
    this.router.navigate(['configuration/list-event'])
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  /** Question */

  getListQuestionExisting() {
    this.startLoading()
    let tag = []
    this.service.listQuestionMonevAll().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listQuestion = resp.data
            this.listQuestion.forEach(x => {
              tag.push(x)
            })
            this.selectValueQustion = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addNewQuestions(from: number ,QuestionType: number, category: number) {
    if (from === 1) {
      if (category === 1) {
        this.newQuestionCompliance.push({
          questionType: QuestionType,
          createdFor: 1,
          questionCategory: category,
          question: null
        })
      } else if (category === 2) {
        this.newQuestionEconomy.push({
          questionType: QuestionType,
          createdFor: 1,
          questionCategory: category,
          question: null
        })
      } else if (category === 3) {
        this.newQuestionEfective.push({
          questionType: QuestionType,
          createdFor: 1,
          questionCategory: category,
          question: null
        })
      } else if (category === 4) {
        this.newQuestionEfisiens.push({
          questionType: QuestionType,
          createdFor: 1,
          questionCategory: category,
          question: null
        })
      }
    } else if (from === 2) {
      if (category === 1) {
       this.newQuestionParticipantCompliance.push({
         questionType: QuestionType,
         createdFor: 2,
         questionCategory: category,
         question: null
       })
     } else if (category === 2) {
       this.newQuestionParticipantEconomy.push({
         questionType: QuestionType,
         createdFor: 2,
         questionCategory: category,
         question: null
       })
     } else if (category === 3) {
       this.newQuestionParticipantEfective.push({
         questionType: QuestionType,
         createdFor: 2,
         questionCategory: category,
         question: null
       })
     } else if (category === 4) {
       this.newQuestionParticipantEfisiens.push({
         questionType: QuestionType,
         createdFor: 2,
         questionCategory: category,
         question: null
       })
     } else if (category === 5) {
        this.newQuestionParticipantOtherQuestion.push({
          questionType: QuestionType,
          createdFor: 2,
          questionCategory: category,
          question: null
        })
      }
    }
  }

  removeNewQuestion(category: number, index: number) {
    if (category === 1) {
      this.newQuestionCompliance.splice(index, 1)
    } else if (category === 2) {
      this.newQuestionEconomy.splice(index, 1)
    } else if (category === 3) {
      this.newQuestionEfective.splice(index, 1)
    } else if (category === 4) {
    this.newQuestionEfisiens.splice(index, 1)
    } else if (category === 5) {
      this.newQuestionParticipantCompliance.splice(index, 1)
    } else if (category === 6) {
      this.newQuestionParticipantEconomy.splice(index, 1)
    } else if (category === 7) {
      this.newQuestionParticipantEfective.splice(index, 1)
    } else if (category === 8) {
      this.newQuestionParticipantEfisiens.splice(index, 1)
    } else if (category === 9) {
      this.newQuestionParticipantOtherQuestion.splice(index, 1)
    }

  }

  /** * List Event */
  dataEvent() {
    this.startLoading()
    this.service.eventType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEvent = resp.data.content
            let findNameEventType = this.listEvent?.find(x => x.id === this.idEventType)
            this.eventTypeName = findNameEventType?.name
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  disableStep1() {
    let checkCategoryCompliance = false
    let checkCategoryEconomy = false
    let checkCategoryEfective = false
    let checkCategoryEfesiens = false


    if (this.newQuestionCompliance.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionCompliance.filter(x => !x.question)
      if (findEmptyQuestionCompliance.length > 0) {
        checkCategoryCompliance = true
      } else {
        checkCategoryCompliance = false
      }
    }
    if (this.newQuestionEconomy.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionEconomy.filter(x => !x.question)
      if (findEmptyQuestionCompliance.length > 0) {
        checkCategoryEconomy = true
      } else {
        checkCategoryEconomy = false
      }
    }

    if (this.newQuestionEfisiens.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionEfisiens.filter(x => !x.question)
      if (findEmptyQuestionCompliance.length > 0) {
        checkCategoryEfesiens = true
      } else {
        checkCategoryEfesiens = false
      }
    }

    if (this.newQuestionEfective.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionEfective.filter(x => !x.question)
      if (findEmptyQuestionCompliance.length > 0) {
        checkCategoryEfective = true
      } else {
        checkCategoryEfective = false
      }
    }
    return checkCategoryCompliance || checkCategoryEconomy || checkCategoryEfective || checkCategoryEfesiens || (this.newQuestionEfective.length + this.newQuestionEfisiens.length + this.newQuestionCompliance.length + this.newQuestionEconomy.length == 0)
  }

  disableStep2() {
    let checkQuestionParticipant = false
    let checkQuestionEconomy = false
    let checkQuestionEfective = false
    let checkQuestionEfisiens = false
    let checkQuestionOtherInformation = false

    if (this.newQuestionParticipantCompliance.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantCompliance.filter(x => !x.question)
      if (findEmptyQuestionCompliance.length > 0) {
        checkQuestionParticipant = true
      } else {
        checkQuestionParticipant = false
      }
    }
    if (this.newQuestionParticipantEconomy.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantEconomy.filter(x => !x.question)
      if (findEmptyQuestionCompliance.length > 0) {
        checkQuestionEconomy = true
      } else {
        checkQuestionEconomy = false
      }
    }
    if (this.newQuestionParticipantEfisiens.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantEfisiens.filter(x => !x.question)
      if (findEmptyQuestionCompliance.length > 0) {
        checkQuestionEfisiens = true
      } else {
        checkQuestionEfisiens = false
      }
    }
    if (this.newQuestionParticipantEfective.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantEfective.filter(x => !x.question)
      if (findEmptyQuestionCompliance.length > 0) {
        checkQuestionEfective = true
      } else {
        checkQuestionEfective = false
      }
    }
    if (this.newQuestionParticipantOtherQuestion.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantOtherQuestion.filter(x => !x.question)
      if (findEmptyQuestionCompliance.length > 0) {
        checkQuestionOtherInformation = true
      } else {
        checkQuestionOtherInformation = false
      }
    }

    return checkQuestionParticipant || checkQuestionEconomy || checkQuestionEfisiens || checkQuestionEfective || checkQuestionOtherInformation || (this.newQuestionParticipantCompliance.length + this.newQuestionParticipantEconomy.length + this.newQuestionParticipantEfective.length + this.newQuestionParticipantOtherQuestion.length + this.newQuestionParticipantEfisiens.length == 0)
  }

  // new tag
  CreateNew(question){
    return question
  }

  saveNewQuestion() {
    this.loadingCreateForm = true
    let params
    let allQuestions = []

    if (this.newQuestionCompliance?.length > 0) {
      this.newQuestionCompliance.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionEfisiens?.length > 0) {
      this.newQuestionEfisiens.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionEfective?.length > 0) {
      this.newQuestionEfective.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionEconomy?.length > 0) {
      this.newQuestionEconomy.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionParticipantCompliance?.length > 0) {
      this.newQuestionParticipantCompliance.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionParticipantEconomy?.length > 0) {
      this.newQuestionParticipantEconomy.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionParticipantEfective?.length > 0) {
      this.newQuestionParticipantEfective.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionParticipantEfisiens?.length > 0) {
      this.newQuestionParticipantEfisiens.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionParticipantOtherQuestion?.length > 0) {
      this.newQuestionParticipantOtherQuestion.forEach(x => {
        allQuestions.push(x)
      })
    }

    params = {
      eventTypeId: this.idEventType,
      newQuestions: allQuestions
    }

    console.log(params)

    this.service.createQuestionEvent(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingCreateForm = false
            this.goToListQuestion()
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
