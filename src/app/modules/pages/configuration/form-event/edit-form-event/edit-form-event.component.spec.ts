import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFormEventComponent } from './edit-form-event.component';

describe('EditFormEventComponent', () => {
  let component: EditFormEventComponent;
  let fixture: ComponentFixture<EditFormEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditFormEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFormEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
