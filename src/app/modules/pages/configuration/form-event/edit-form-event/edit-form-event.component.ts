import { Component, OnInit } from '@angular/core';
import {Event} from "../../../../../core/models/awp.model";
import {FormQuestionEvent, QuestionEvent} from "../../../../../core/models/event.model";
import {ActivatedRoute, Router} from "@angular/router";
import {AwpService} from "../../../../../core/services/Awp/awp.service";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-edit-form-event',
  templateUrl: './edit-form-event.component.html',
  styleUrls: ['./edit-form-event.component.scss']
})
export class EditFormEventComponent implements OnInit {

// loading
  finishLoadDataCount = 0
  finishLoadData = true

  breadCrumbItems: Array<{}>;
  selectValueQustion: string[] = [];
  idEventType: string
  listEvent: Event[]
  eventTypeName: string
  loadingCreateForm = false
  detailFormQuestion: FormQuestionEvent

  // question event
  listQuestion: [] = []
  newQuestionCompliance = []
  newQuestionEconomy = []
  newQuestionEfective = []
  newQuestionEfisiens = []
  newQuestionParticipantCompliance = []
  newQuestionParticipantEconomy = []
  newQuestionParticipantEfective = []
  newQuestionParticipantEfisiens = []
  newQuestionParticipantOtherQuestion = []

  existingQuestionCompliance = []
  existingQuestionEconomy = []
  existingQuestionEfective = []
  existingQuestionEfisiens = []
  existingQuestionParticipantCompliance = []
  existingQuestionParticipantEconomy = []
  existingQuestionParticipantEfective = []
  existingQuestionParticipantEfisiens = []
  existingQuestionParticipantOtherQuestion = []

  deletedQuestionExisting = []

  constructor(
      private router: Router,
      private service: AwpService,
      private swalAlert: SwalAlert,
      private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Konfigurasi' }, { label: 'Formulir' }, { label: 'Edit', active: true }];
    this.idEventType = this.activatedRoute.snapshot.paramMap.get('id')
    this.getDetailEventForm()
  }

  goToListQuestion() {
    this.router.navigate(['configuration/list-event'])
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }


  /** Detail Form */
  getDetailEventForm() {
    this.startLoading()
    this.service.detailFormQuestionEvent(this.idEventType).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.detailFormQuestion = resp.data
            this.detailFormQuestion?.questions.forEach(x => {
              if (x.createdFor === 1) {
                if (x.questionCategory === 1) {
                  this.existingQuestionCompliance.push(x)
                } else if (x.questionCategory === 2) {
                  this.existingQuestionEconomy.push(x)
                } else if (x.questionCategory === 3) {
                  this.existingQuestionEfective.push(x)
                } else if (x.questionCategory === 4) {
                  this.existingQuestionEfisiens.push(x)
                }
              } else if (x.createdFor === 2) {
                if (x.questionCategory === 1) {
                  this.existingQuestionParticipantCompliance.push(x)
                } else if (x.questionCategory === 2) {
                  this.existingQuestionParticipantEconomy.push(x)
                } else if (x.questionCategory === 3) {
                  this.existingQuestionParticipantEfective.push(x)
                } else if (x.questionCategory === 4) {
                  this.existingQuestionParticipantEfisiens.push(x)
                } else if (x.questionCategory == 5) {
                  this.existingQuestionParticipantOtherQuestion.push(x)
                }
              }
            })
            this.getListQuestionExisting()
            // this.dataEvent()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }




  /** End Detail Form */


  /** Question */

  getListQuestionExisting() {
    this.startLoading()
    let tag = []
    this.service.listQuestionMonevAll().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listQuestion = resp.data
            if (this.detailFormQuestion.questions.length > 0) {
              let data = this.listQuestion.filter(id1 => !this.detailFormQuestion.questions.some(({ question: id2 }) => id2 === id1))
              data.forEach(x => {
                tag.push(x)
              })
            } else {
              this.listQuestion.forEach(x => {
                tag.push(x)
              })
            }
            this.selectValueQustion = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addNewQuestions(from: number ,QuestionType: number, category: number) {
    if (from === 1) {
      if (category === 1) {
        this.newQuestionCompliance.push({
          questionType: QuestionType,
          createdFor: 1,
          questionCategory: category,
          question: null
        })
      } else if (category === 2) {
        this.newQuestionEconomy.push({
          questionType: QuestionType,
          createdFor: 1,
          questionCategory: category,
          question: null
        })
      } else if (category === 3) {
        this.newQuestionEfective.push({
          questionType: QuestionType,
          createdFor: 1,
          questionCategory: category,
          question: null
        })
      } else if (category === 4) {
        this.newQuestionEfisiens.push({
          questionType: QuestionType,
          createdFor: 1,
          questionCategory: category,
          question: null
        })
      }
    } else {
      if (category === 1) {
        this.newQuestionParticipantCompliance.push({
          questionType: QuestionType,
          createdFor: 2,
          questionCategory: category,
          question: null
        })
      } else if (category === 2) {
        this.newQuestionParticipantEconomy.push({
          questionType: QuestionType,
          createdFor: 2,
          questionCategory: category,
          question: null
        })
      } else if (category === 3) {
        this.newQuestionParticipantEfective.push({
          questionType: QuestionType,
          createdFor: 2,
          questionCategory: category,
          question: null
        })
      } else if (category === 4) {
        this.newQuestionParticipantEfisiens.push({
          questionType: QuestionType,
          createdFor: 2,
          questionCategory: category,
          question: null
        })
      } else if (category === 5) {
        this.newQuestionParticipantOtherQuestion.push({
          questionType: QuestionType,
          createdFor: 2,
          questionCategory: category,
          question: null
        })
      }
    }

  }

  removeNewQuestion(category: number, index: number) {
    if (category === 1) {
      this.newQuestionCompliance.splice(index, 1)
    } else if (category === 2) {
      this.newQuestionEconomy.splice(index, 1)
    } else if (category === 3) {
      this.newQuestionEfective.splice(index, 1)
    } else if (category === 4) {
      this.newQuestionEfisiens.splice(index, 1)
    } else if (category === 5) {
      this.newQuestionParticipantCompliance.splice(index, 1)
    } else if (category === 6) {
      this.newQuestionParticipantEconomy.splice(index, 1)
    } else if (category === 7) {
      this.newQuestionParticipantEfective.splice(index, 1)
    } else if (category === 8) {
      this.newQuestionParticipantEfisiens.splice(index, 1)
    } else if (category === 9) {
      this.newQuestionParticipantOtherQuestion.splice(index, 1)
    }
  }

  removeQuestionExisting(category: number, index: number, data) {
    this.deletedQuestionExisting.push(data)
    if (category === 1) {
      this.existingQuestionCompliance.splice(index, 1)
    } else if (category === 2) {
      this.existingQuestionEconomy.splice(index, 1)
    } else if (category === 3) {
      this.existingQuestionEfective.splice(index, 1)
    } else if (category === 4) {
      this.existingQuestionEfisiens.splice(index, 1)
    } else if (category === 5) {
      this.existingQuestionParticipantCompliance.splice(index, 1)
    } else if (category === 6) {
      this.existingQuestionParticipantEconomy.splice(index, 1)
    } else if (category === 7) {
      this.existingQuestionParticipantEfective.splice(index, 1)
    } else if (category === 8) {
      this.existingQuestionParticipantEfisiens.splice(index, 1)
    } else if (category === 9) {
      this.existingQuestionParticipantOtherQuestion.splice(index, 1)
    }
  }

  /** End Question */


  /** * List Event */
  dataEvent() {
    this.startLoading()
    this.service.eventType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEvent = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  disableStep1() {
    let checkCategoryCompliance = false
    let checkCategoryEconomy = false
    let checkCategoryEfective = false
    let checkCategoryEfesiens = false


    if (this.newQuestionCompliance.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionCompliance.filter(x => !x.question)
      checkCategoryCompliance = findEmptyQuestionCompliance.length > 0;
    }
    if (this.newQuestionEconomy.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionEconomy.filter(x => !x.question)
      checkCategoryEconomy = findEmptyQuestionCompliance.length > 0;
    }

    if (this.newQuestionEfisiens.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionEfisiens.filter(x => !x.question)
      checkCategoryEfesiens = findEmptyQuestionCompliance.length > 0;
    }

    if (this.newQuestionEfective.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionEfective.filter(x => !x.question)
      checkCategoryEfective = findEmptyQuestionCompliance.length > 0;
    }
    return checkCategoryCompliance || checkCategoryEconomy || checkCategoryEfective || checkCategoryEfesiens || (this.newQuestionEfective.length + this.newQuestionEfisiens.length + this.newQuestionCompliance.length + this.newQuestionEconomy.length == 0 && this.existingQuestionEfisiens.length + this.existingQuestionCompliance.length + this.existingQuestionEfective.length + this.existingQuestionEfisiens.length == 0)
  }

  disableStep2() {
    let checkQuestionParticipantCompliance = false
    let checkQuestionParticipantEconomy = false
    let checkQuestionParticipantEffective = false
    let checkQuestionParticipantEffisiens = false
    let checkQuestionParticipantOtherQuestion = false

    if (this.newQuestionParticipantCompliance.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantCompliance.filter(x => !x.question)
      checkQuestionParticipantCompliance = findEmptyQuestionCompliance.length > 0;
    }

    if (this.newQuestionParticipantEconomy.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantEconomy.filter(x => !x.question)
      checkQuestionParticipantEconomy = findEmptyQuestionCompliance.length > 0;
    }

    if (this.newQuestionParticipantEfective.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantEfective.filter(x => !x.question)
      checkQuestionParticipantEffective = findEmptyQuestionCompliance.length > 0;
    }

    if (this.newQuestionParticipantEfisiens.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantEfisiens.filter(x => !x.question)
      checkQuestionParticipantEffisiens = findEmptyQuestionCompliance.length > 0;
    }
    if (this.newQuestionParticipantOtherQuestion.length > 0) {
      let findEmptyQuestionCompliance = this.newQuestionParticipantOtherQuestion.filter(x => !x.question)
      checkQuestionParticipantOtherQuestion = findEmptyQuestionCompliance.length > 0;
    }

    return checkQuestionParticipantCompliance || checkQuestionParticipantEconomy || checkQuestionParticipantEffective || checkQuestionParticipantEffisiens || checkQuestionParticipantOtherQuestion || (this.newQuestionParticipantCompliance.length + this.existingQuestionParticipantCompliance.length + this.newQuestionParticipantEconomy.length + this.existingQuestionParticipantEconomy.length + this.newQuestionParticipantEfective.length + this.existingQuestionParticipantEfective.length + this.newQuestionParticipantEfisiens.length + this.existingQuestionParticipantEfisiens.length + this.existingQuestionParticipantOtherQuestion.length + this.newQuestionParticipantOtherQuestion.length == 0)
  }

  // new tag
  CreateNew(question){
    return question
  }

  saveNewQuestion() {
    this.loadingCreateForm = true
    let params
    let allQuestions = []

    if (this.newQuestionCompliance?.length > 0) {
      this.newQuestionCompliance.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionEfisiens?.length > 0) {
      this.newQuestionEfisiens.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionEfective?.length > 0) {
      this.newQuestionEfective.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionEconomy?.length > 0) {
      this.newQuestionEconomy.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionParticipantCompliance?.length > 0) {
      this.newQuestionParticipantCompliance.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionParticipantEconomy?.length > 0) {
      this.newQuestionParticipantEconomy.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionParticipantEfective?.length > 0) {
      this.newQuestionParticipantEfective.forEach(x => {
        allQuestions.push(x)
      })
    }

    if (this.newQuestionParticipantEfisiens?.length > 0) {
      this.newQuestionParticipantEfisiens.forEach(x => {
        allQuestions.push(x)
      })
    }
    if (this.newQuestionParticipantOtherQuestion?.length > 0) {
      this.newQuestionParticipantOtherQuestion.forEach(x => {
        allQuestions.push(x)
      })
    }
    params = {
      newQuestions: allQuestions,
      deletedQuestionIds: this.deletedQuestionExisting
    }

    console.log(params)

    this.service.updateQuestionEvent(params, this.idEventType).subscribe(
        resp => {
          if (resp.success) {
            this.loadingCreateForm = false
            this.goToListQuestion()
          }
        }, error => {
          this.loadingCreateForm = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
