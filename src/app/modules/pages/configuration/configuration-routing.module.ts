import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ConfigurationComponent} from "./configuration.component";
import {DetailPenggunaComponent} from "./detail-pengguna/detail-pengguna.component";
import {AddPenggunaComponent} from "./add-pengguna/add-pengguna.component";
import {FormEventComponent} from "./form-event/form-event.component";
import {EditFormEventComponent} from "./form-event/edit-form-event/edit-form-event.component";
import {CreateFormEventComponent} from "./form-event/create-form-event/create-form-event.component";
import {AuthGuard} from "../../../core/guards/auth.guard";


const routes: Routes = [
    {
        path: 'list',
        component: ConfigurationComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail/:id',
        component: DetailPenggunaComponent, canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: AddPenggunaComponent, canActivate: [AuthGuard]
    },
    {
        path: 'list-event',
        component: FormEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'edit/:id',
        component: EditFormEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'add-event/:id',
        component: CreateFormEventComponent, canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConfigurationRoutingModule {}
