import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {environment} from "../../../../environments/environment";
import {PustakaModel} from "../../../core/models/pustaka.model";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {PustakaService} from "../../../core/services/Pustaka/pustaka.service";
import {FileService} from "../../../core/services/Image/file.service";
import {leangueIndoDT} from "../../../shared/pagination-custom";
import {DataResponse} from "../../../core/models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {UsersModels} from "../../../core/models/users.models";
import { DatePipe } from '@angular/common'
import { parse } from 'date-fns';
import {ConfigurationService} from "../../../core/services/Configuration/configuration.service";
import {RoleUsers} from "../../../core/models/RoleUsers";
import {SwalAlert} from "../../../core/helpers/Swal";

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss']
})
export class ConfigurationComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  private prefixUrl = environment.serverFile

  paramsLike = []
  paramsIn = []
  finishLoadDataCount = 0
  finishLoadData = true
  filterName: string
  start = 1
  listPengguna: UsersModels[]
  pdfSrc: any
  loadingSavePdf = false
  breadCrumbItems: Array<{}>;
  filterRole : RoleUsers[]
  filterRoleExist : RoleUsers[]
  checkedRoles = []

  constructor(
      private http: HttpClient,
      private router: Router,
      private modalService: NgbModal,
      private service: ConfigurationService,
      private serviceImage: FileService,
      private datepipe: DatePipe,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Konfigurasi' }, { label: 'Daftar Pengguna', active: true }];
    this.startLoading()
    this.getListRoles()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: [],
          paramIn: this.paramsIn,
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAuth + 'user/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.listPengguna = resp.data.content
            this.listPengguna.forEach(dataList => {
              if (dataList.lastLogin != null) {
                dataList.convertDateLastLogin = parse(dataList.lastLogin, 'dd-MM-yyyy HH:mm:ss', new Date());
              }
            })
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'id' }, { data: 'firstName'}, {data: 'email'}, {data: 'roles?.role?.name'}, {data: 'lastLogin'}, {data: 'active'}]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.paramsLike = []
    this.paramsIn = []
    const dataSelectedrole = this.filterRole.filter(data => data.checked).map(data => data.id)

    if (dataSelectedrole.length > 0) {
      const roleName = {
        field: "roleIds",
        dataType: "string",
        value: dataSelectedrole
      }
      this.paramsIn.push(roleName)
    } else {
      this.paramsIn = []
    }
    if (this.filterName) {
      const paramsFirstName = {
        field: "firstName",
        dataType: "string",
        value: this.filterName
      }
      const paramsLastName = {
        field: "lastName",
        dataType: "string",
        value: this.filterName
      }
      const paramsEmail = {
        field: "email",
        dataType: "string",
        value: this.filterName
      }
      this.paramsLike.push(paramsFirstName)
      this.paramsLike.push(paramsLastName)
      this.paramsLike.push(paramsEmail)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  goToUploadFile() {
    this.router.navigate(['file/upload'])
  }

  goToDetailConfiguration(id) {
    this.router.navigate(['configuration/detail/' + id])
  }

  goToAddConfiguration() {
    this.router.navigate(['configuration/add'])
  }

  getListRoles() {
    this.startLoading()
    this.service.listRole().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.filterRole = resp.data.content
            this.filterRoleExist = resp.data.content
            this.filterRole.forEach(x => {
              x.checked = false
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.filterRole = this.filterRoleExist.filter((product) => {
      return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
    });
  }

  selectedRoles() {
    const x = this.filterRole.filter(data => data.checked == true)
    this.checkedRoles = x
    this.refreshDatatable()
  }

  clearFilter() {
    this.checkedRoles = []
    this.filterRole.forEach(data => {
      data.checked = false
    })
    this.refreshDatatable()
  }
}
