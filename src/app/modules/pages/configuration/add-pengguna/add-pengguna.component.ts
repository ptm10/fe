import {Component, Input, OnInit} from '@angular/core';
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {FileService} from "../../../../core/services/Image/file.service";
import {ListUnit, Position, RoleUsers} from "../../../../core/models/RoleUsers";
import Swal from "sweetalert2";
import {FileModel} from "../../../../core/models/file.model";
import {Router} from "@angular/router";
import {ComponentModel} from "../../../../core/models/componentModel";
import {parse} from "date-fns";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {resourcesModel} from "../../../../core/models/resources.model";
import {ModalComponent} from "./modal/modal.component";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {Province} from "../../../../core/models/awp.model";

@Component({
  selector: 'app-add-pengguna',
  templateUrl: './add-pengguna.component.html',
  styleUrls: ['./add-pengguna.component.scss']
})

export class AddPenggunaComponent implements OnInit {

  @Input() selectedEvent: string[]
  @Input() selectedComponent: any

  firstName: string
  lastName: string
  email: string
  uploadFilePhotoProfile: Array<File> = [];
  roleRequest = []
  removeRolesSelected = []
  addRolesSelected = []
  listRolesSelectedExist = []
  filterRole : RoleUsers[]
  oListRolesSelectedExist = []
  loadingEditProfile = true
  fileUploadPhoto: FileModel[]
  roleSelected = []
  openModalRoles = false
  addSelectedAdminRoles = false
  deleteSelectedAdminRoles = false
  roleRequestToApi = []
  idNewPhoto: string
  breadCrumbItems: Array<{}>;
  listComponentStaff: ComponentModel[]
  componentExist: ComponentModel[]
  selectedComponentExist: ComponentModel
  phoneNumber: string
  position: string

  // holidays
  holidayManagement: ManagementHolidays[]
  listHolidays = []
  selectedFinishTaskDate: any

  roleLspExist = []
  lspRoleSelected = false

  // supervisor
  listSupervisor: resourcesModel[]
  listSupervisorExist: resourcesModel[]
  selectedSupervisorExist: resourcesModel

  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;


  loadingGetListSuperVisor = false
  listKegiatan = []
  selectedWorkUnit: string
  selectPositionForPcu: string

  // loading
  finishLoadData = true
  finishLoadDataCount = 0

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  listProvinceApi: string[];
  selectedProvince: string

  //Unit
  listUnit: ListUnit[]

  //position
  listPosition: Position[]
  listPositionApi: string[]
  selectedPosition: string

  constructor(
      private service: ProfileService,
      private modalService: NgbModal,
      private formBuilder: FormBuilder,
      private http: HttpClient,
      private serviceImage: FileService,
      private router: Router,
      private calendar: NgbCalendar,
      private serviceAdministration: AdministrationService,
      public formatter: NgbDateParserFormatter,
      private swalAlert: SwalAlert,
      private serviceAwp: AwpService
  ) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: 1,
      day: 1
    };
    this.maxDate = {
      year: current.getFullYear(),
      month: 12,
      day: 31
    };
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Konfigurasi' },{ label: 'Daftar Pengguna' }, { label: 'Tambah Pengguna', active: true }];
    this.getListRoles()
    this.listAwp()
    this.getListHolidayManagement()
    this.listProvince()
    this.getListWorkUnit()
  }

  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }
  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  testing() {
    console.log(this.selectedWorkUnit)
  }

  // new tag
  CreateNew(Position){
    return Position
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * Open modal kegiatan */
  openModalKegiatan(){
    const modalRef = this.modalService.open(ModalComponent, { size: 'lg' ,centered: true });
    modalRef.componentInstance.selectedEvent = this.listKegiatan;
    modalRef.componentInstance.componentId = this.selectedComponentExist.id
    modalRef.componentInstance.event.subscribe((data: any) => {
      /**
       * Pass data from modal
       */
      this.listKegiatan.push(data)
    });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  keyPressAlphaNumeric(event) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  addDeleteRolesSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.roleRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removeRolesSelected.forEach((check, index) => {
          if (check.id === data.id) {
            this.removeRolesSelected.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addRolesSelected.push(data)
        }
        this.roleRequest.push(data)
      }

      this.roleRequest.filter(data => {
        if (data.name === 'Administrator') {
          this.addSelectedAdminRoles = true
        } else {
          this.addSelectedAdminRoles = false
        }
      })
    } else {
      this.roleRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.roleRequest.splice(index, 1)

        }
      })
      let checkAddExist = false

      this.addRolesSelected.forEach((check, index) => {
        if (check.id === data.id) {
          this.addRolesSelected.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removeRolesSelected.push(data)
      }
    }

    this.listRolesSelectedExist = this.filterRole

    if (this.selectedWorkUnit === 'PMU') {
      this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name !== 'PCU')
    } else if (this.selectedWorkUnit === 'PCU') {
      this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name === 'PCU')
    }

    this.roleRequest.forEach(dataSelected => {
      if (dataSelected.name === 'LSP') {
        this.listRolesSelectedExist = []
        return
      }
      this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter =>  dataFilter.name !== 'LSP')
      this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => dataFilter.id !== dataSelected.id)
      if (dataSelected.supervisiorId !== null) {
        this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => (dataFilter.supervisiorId === dataSelected.supervisiorId || dataFilter.supervisiorId == null))
      }
    })

    this.oListRolesSelectedExist = this.listRolesSelectedExist
    this.roleLspExist = this.addRolesSelected.filter(filterLsp => filterLsp.name === 'LSP')


    if (this.roleLspExist.length > 0) {
      this.lspRoleSelected = true
    } else {
      this.lspRoleSelected = false
    }
    this.getListSuperVisor()
  }


  changeSelectedWorkUnit() {
    let selectedUnitId = this.listUnit.filter(x => x.name === this.selectedWorkUnit)
    this.position = null
    this.roleRequest = []
    this.addRolesSelected = []
    this.roleLspExist = []
    this.listRolesSelectedExist = this.filterRole
    if (this.selectedWorkUnit === 'PMU') {
      this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name !== 'PCU')
      this.oListRolesSelectedExist = this.listRolesSelectedExist
      this.getListPosition(selectedUnitId[0].id)
    } else if (this.selectedWorkUnit === 'PCU') {
      this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name === 'PCU')
      this.oListRolesSelectedExist = this.listRolesSelectedExist
      this.getListPosition(selectedUnitId[0].id)
    }
  }

  /** * List Province */

  listProvince() {
    let tag = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            this.listProvinceAll.forEach(x => {
              tag.push(x.name)
            })
            this.listProvinceApi = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End List Province */

  /** * Delete kegiatan */
  deleteKegiatan(i: any) {
    this.listKegiatan.splice(i, 1)
    console.log(this.listKegiatan);
  }

  openFilterRoles() {
    this.openModalRoles = !this.openModalRoles
  }

  getListRoles() {
    this.startLoading()
    this.service.listRoleDt().subscribe(
        resp => {
          if (resp.success) {
            this.filterRole = resp.data.content
            this.listRolesSelectedExist = this.filterRole.filter(({ supervisiorId: sp1, id:id1 }) => !this.roleRequest.some(({  supervisiorId: sp2, id: id2 }) => ((sp1 !== sp2 || id1 === id2))));
            this.oListRolesSelectedExist = this.listRolesSelectedExist
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  doUploadPhotoProfile() {
    this.loadingEditProfile = false
    if (this.uploadFilePhotoProfile.length > 0) {
      this.service.upload(this.uploadFilePhotoProfile[0]).subscribe(resp => {
            if (resp.success) {
              this.fileUploadPhoto = resp.data
              this.saveEditProfile()
            } else {
              this.loadingEditProfile = true
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingEditProfile = true
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.saveEditProfile()
    }
  }

  getListWorkUnit() {
    this.startLoading()
    this.service.listUnit().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listUnit = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListPosition(data) {
    this.listPosition = []
    let tag = []
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "unitId",
          dataType: "string",
          value: data
        }
      ],
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ]
    }
    this.service.listPosition(params).subscribe(
        resp => {
          if (resp.success) {
            this.listPosition = resp.data.content
            this.listPosition.forEach(x => {
              tag.push(x.name)
            })
            this.listPositionApi = tag
          }
        }, error =>  {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  saveEditProfile() {
    let params
    let startDate
    let endDate
    let successSave
    let dataSelectedKegiatan = []
    let selectedUnitId
    let selectedProvince


    if (this.listKegiatan.length > 0) {
      this.listKegiatan.forEach(x => {
        dataSelectedKegiatan.push(x.id)
      })
    }

    this.roleRequestToApi = []
    this.roleSelected = []
    if (!this.fileUploadPhoto) {
      this.idNewPhoto = null
    } else {
      this.idNewPhoto = this.fileUploadPhoto[0]?.id
    }

    this.addRolesSelected.forEach(data => {
      this.roleSelected.push(data)
    })

    this.removeRolesSelected.forEach(data => {
      this.roleSelected.push(data)
    })


    if (this.roleSelected.length > 0) {
      this.roleSelected.forEach(data => {
        this.roleRequestToApi.push(data.id)
      })
    }


    if (this.selectedProvince) {
      selectedProvince = this.listProvinceAll.filter(x => x.name === this.selectedProvince)
    }

    if (this.selectedWorkUnit === 'PMU') {
      if (this.roleLspExist.length > 0) {
        let withoutUnit
        if (!this.selectedComponentExist) {
          withoutUnit = null
        } else {
          withoutUnit = this.selectedComponentExist.id
        }
        startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
        endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')
        params = {
          firstName: this.firstName,
          lastName: this.lastName,
          email: this.email + '@madrasah.kemenag.go.id',
          profilePictureId: this.idNewPhoto,
          roles: this.roleRequestToApi,
          componentId: withoutUnit,
          lspStartDate: startDate,
          lspEndDate: endDate,
          lspAwpImplementationIds: dataSelectedKegiatan,
          provinceId: null
        }
      } else {
        params = {
          firstName: this.firstName,
          lastName: this.lastName,
          email: this.email + '@madrasah.kemenag.go.id',
          profilePictureId: this.idNewPhoto,
          roles: this.roleRequestToApi,
          componentId: this.selectedComponentExist.id,
          provinceId: null
        }
      }
    } else if (this.selectedWorkUnit === 'PCU') {
      params = {
        firstName: this.firstName,
        lastName: this.lastName,
        email: this.email + '@madrasah.kemenag.go.id',
        profilePictureId: this.idNewPhoto,
        roles: this.roleRequestToApi,
        componentId: null,
        provinceId: selectedProvince[0].id
      }
    }


    this.service.saveUsers(params).subscribe(
        resp => {
          if (resp.success) {
            successSave = resp.data
            if (this.roleLspExist.length == 0) {
              this.createResources(successSave.id)
            } else {
              this.loadingEditProfile = true
              this.router.navigate(['configuration/list'])
            }
          }
        }, error => {
          this.loadingEditProfile = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  createResources(id) {
    const selectedUnitId = this.listUnit.filter(x => x.name === this.selectedWorkUnit)
    let superVisor
    if (this.selectedSupervisorExist) {
      superVisor = this.selectedSupervisorExist?.id
    } else {
      superVisor = null
    }
    const params = {
      userId: id,
      position: this.position,
      phoneNumber: '+62'+this.phoneNumber,
      supervisiorId: superVisor,
      unitId: selectedUnitId[0].id,
    }
    this.serviceAdministration.createResources(params).subscribe(
        resp => {
          if (resp.success) {
            this.idNewPhoto = ''
            this.firstName = ''
            this.lastName = ''
            this.email = ''
            this.listRolesSelectedExist = []
            this.oListRolesSelectedExist = []
            this.uploadFilePhotoProfile = []
            this.roleRequestToApi = []
            this.roleSelected = []
            this.removeRolesSelected = []
            this.addRolesSelected = []
            this.loadingEditProfile = true
            this.router.navigate(['configuration/list'])
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedUnit(data) {
    this.selectedComponentExist = null
    this.listComponentStaff = this.componentExist

    this.selectedComponentExist = data
    this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)
    this.getListSuperVisor()
  }

  searchFilterUnit(e) {
    const searchStr = e.target.value
    let filter = this.componentExist
    if (this.selectedComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
    } else {
      filter = this.componentExist
    }

    this.listComponentStaff = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  listAwp() {
    this.service.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff = resp.data.content
            this.listComponentStaff.forEach(dataCode => {
              dataCode.code = 'Komponen ' + dataCode.code
            })
            this.listComponentStaff = this.listComponentStaff.filter(dataStaff => dataStaff.id != this.selectedComponentExist?.id)
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.listRolesSelectedExist = this.oListRolesSelectedExist.filter((product) => {
      return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
    });
  }

  goToListConfiguration() {
    this.router.navigate(['configuration/list'])
  }

  getListHolidayManagement() {
    this.startLoading()
    this.listHolidays = []
    this.serviceAdministration.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedSupervisor(data) {
    this.selectedSupervisorExist = null
    this.listSupervisor = this.listSupervisorExist

    this.selectedSupervisorExist = data
    this.listSupervisor = this.listSupervisor.filter(dataFilter => dataFilter.id != this.selectedSupervisorExist.id)
  }

  searchFilterSuperVisor(e) {
    const searchStr = e.target.value
    //
    let filter = this.listSupervisorExist
    if (this.selectedSupervisorExist) {
      filter = filter.filter(x => x.id !== this.selectedSupervisorExist.id)
    } else {
      filter = this.listSupervisorExist
    }

    this.listSupervisor = filter.filter((product) => {
      return ((product?.user?.firstName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product?.user?.firstName != null) || (product?.user?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product?.user?.lastName != null))
    });
  }

  isDisabledButton() {
    if (this.selectedWorkUnit === 'PMU') {
      if (this.lspRoleSelected) {
        return !this.firstName || !this.email || this.roleRequest.length == 0 || !this.fromDate || !this.toDate || !this.selectedComponentExist || !this.selectedWorkUnit
      } else {
        return !this.firstName || !this.email || this.roleRequest.length == 0  || !this.selectedComponentExist || !this.phoneNumber || !this.position || !this.selectedWorkUnit
      }
    } else if (this.selectedWorkUnit === 'PCU') {
      return !this.firstName || !this.email || this.roleRequest.length == 0 || !this.phoneNumber || !this.position || !this.selectedWorkUnit || !this.selectedProvince
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  changeSuperVisorPcu() {
    this.getListSuperVisor()
  }

  disabledSelectSupervisor() {
    if (this.selectedWorkUnit === 'PMU') {
      return this.roleRequest.length === 0 || !this.selectedComponentExist
    } else if (this.selectedWorkUnit === 'PCU') {
      return !this.selectedProvince || this.roleRequest.length === 0 || !this.position
    } else {
      return true
    }
  }

  getListSuperVisor() {
    this.selectedSupervisorExist = null
    this.loadingGetListSuperVisor = true
    let roleIdsSelected = []
    let params
    let selectedProvince
    if (this.selectedWorkUnit === 'PMU') {
      if (this.selectedComponentExist && this.roleRequest.length > 0) {
        this.roleRequest.forEach(data => {
          roleIdsSelected.push(data.id)
        })
        params = {
          componentId: this.selectedComponentExist.id,
          roleIds: roleIdsSelected
        }
        this.service.listSupervisorByComponent(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingGetListSuperVisor = false
                this.listSupervisor = []
                this.listSupervisorExist = []
                this.listSupervisor = resp.data.content
                this.listSupervisorExist = resp.data.content
              }
            }, error => {
              this.loadingGetListSuperVisor = false
              this.swalAlert.showAlertSwal(error)
            }
        )
      } else {
        this.loadingGetListSuperVisor = false
      }
    } else if (this.selectedWorkUnit === 'PCU') {
      if (this.selectedProvince && this.position) {
        selectedProvince = this.listProvinceAll.filter(x => x.name === this.selectedProvince)
        params = {
          provinceId: selectedProvince[0].id,
          userId: null,
          position: this.position
        }
        this.service.listSupervisorByRolePcu(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingGetListSuperVisor = false
                this.listSupervisor = []
                this.listSupervisorExist = []
                this.listSupervisor = resp.data.content
                this.listSupervisorExist = resp.data.content
              }
            }, error => {
              this.loadingGetListSuperVisor = false
              this.swalAlert.showAlertSwal(error)
            }
        )
      } else {
        this.loadingGetListSuperVisor = false
      }

    }

  }
}
