import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPenggunaComponent } from './add-pengguna.component';

describe('AddPenggunaComponent', () => {
  let component: AddPenggunaComponent;
  let fixture: ComponentFixture<AddPenggunaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddPenggunaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPenggunaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
