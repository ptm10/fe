import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {Kegiatan} from "../../../../../core/models/awp.model";
import Swal from "sweetalert2";
import {EventActivityService} from "../../../../../core/services/EventActivity/event-activity.service";
import {KegiatanService} from "../../../../../core/services/kegiatan.service";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Output() event: EventEmitter<any[]> = new EventEmitter();
  @Input() selectedEvent: any[]
  @Input() deletedEvent: any[]
  @Input() componentId: string

  //code
  selectedCode: any = null
  selectedCodeExisting: any
  listCode: any
  listCodeExisting: any
  kegiatan: string
  modelKegiatan: Kegiatan[]
  modelKegiatanMaster: Kegiatan[]
  finishLoadDataCount = 0
  finishLoadData = true

  constructor(public modal: NgbActiveModal, private serviceEvent: KegiatanService, private swalAlert: SwalAlert) { }

  ngOnInit(): void {
    this.getDataActivity();
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  getDataActivity() {
    this.startLoading()
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "componentId",
          dataType: "string",
          value: this.componentId
        },
        {
          field: "status",
          dataType: "int",
          value: 5
        }
      ],
      sort: [
        {
          field: "createdAt",
          direction: "desc"
        }
      ]
    }
    this.serviceEvent.listEventLsp(params).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.modelKegiatan = resp.data.content
            this.modelKegiatanMaster = resp.data.content
            if (this.deletedEvent?.length > 0) {
              this.deletedEvent.forEach(x => {
                this.modelKegiatan.push(x)
              })
            }
            if (this.selectedEvent.length > 0) {
              this.selectedEvent.forEach(x => {
                this.modelKegiatan = this.modelKegiatan.filter(kegiatanSelected => kegiatanSelected.id !== x.id)
                this.modelKegiatanMaster = this.modelKegiatanMaster.filter(kegiatanSelected => kegiatanSelected.id !== x.id)
              })
            }

          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  selectedCodeFromList(data) {
    this.selectedCode = null
    this.modelKegiatan = this.modelKegiatanMaster

    this.selectedCode = data
    this.modelKegiatan = this.modelKegiatan.filter(data => data.componentCode.id !== this.selectedCode.componentCode.id)
    this.kegiatan = this.selectedCode.name
  }

  clearSelectedCode() {
    this.selectedCode = null
    this.modelKegiatan = this.modelKegiatanMaster
    this.kegiatan = ''
  }

  add(){
    this.event.emit(this.selectedCode);
    this.modal.dismiss();
  }

}
