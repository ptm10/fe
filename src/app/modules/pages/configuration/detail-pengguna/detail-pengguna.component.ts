import { Component, OnInit } from '@angular/core';
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {FileService} from "../../../../core/services/Image/file.service";
import {ConfigurationService} from "../../../../core/services/Configuration/configuration.service";
import {UsersModels} from "../../../../core/models/users.models";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {ListUnit, Position, RoleUsers} from "../../../../core/models/RoleUsers";
import Swal from "sweetalert2";
import {FileModel} from "../../../../core/models/file.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {parse} from "date-fns";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {resourcesModel} from "../../../../core/models/resources.model";
import {ModalComponent} from "../add-pengguna/modal/modal.component";
import {Kegiatan, Province} from "../../../../core/models/awp.model";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {AwpService} from "../../../../core/services/Awp/awp.service";

@Component({
  selector: 'app-detail-pengguna',
  templateUrl: './detail-pengguna.component.html',
  styleUrls: ['./detail-pengguna.component.scss']
})
export class DetailPenggunaComponent implements OnInit {

  changePageUser: number
  detailConfiguration: string
  detailConfigurationModel: UsersModels
  roleLspExisting = false
  urlPhotoProfile: any
  changeStatus: boolean
  uploadFilePhotoProfile: Array<File> = [];
  profileModel: UsersModels
  editDate: any
  userLoginId: string
  allowEditUser: boolean
  roleRequest = []
  removeRolesSelected = []
  addRolesSelected = []
  listRolesSelectedExist = []
  filterRole : RoleUsers[]
  oListRolesSelectedExist = []
  openModalRoles = false
    listComponentStaff: ComponentModel[]
    componentExist: ComponentModel[]
    selectedComponentExist: ComponentModel
    loadingEditProfile = true
    fileUploadPhoto: FileModel[]
    roleSelected = []
    roleRequestToApi = []
    idNewPhoto: string
    breadCrumbItems: Array<{}>;
    minDate = undefined;
    maxDate = undefined;
    holidayManagement: ManagementHolidays[]
    listHolidays = []
    disabledDates:NgbDateStruct[] = []
    selectedFinishTaskDate: any
    roleLspExist = []
    lspRoleSelected = false
    listSupervisor: resourcesModel[]
    listSupervisorExist: resourcesModel[]
    selectedSupervisorExist: resourcesModel
    selectedSupervisorExistMaster: resourcesModel
    listAllSupervisor:resourcesModel[]

    editFirstName: string
    editLastName: string
    editEmail: string
    editPhoneNumber: string
    editPosition: string

    fromDate: NgbDate | null;
    toDate: NgbDate | null;
    hoveredDate: NgbDate | null = null;
    loadingGetListSuperVisor = false

    listKegiatan = []
    modelKegiatan: Kegiatan[]
    modelKegiataEdit: Kegiatan[]
    modelKegiatanDeletedIds = []
    modelKegiatanDeletedExisting = []

    selectedWorkUnit: string
    selectPositionForPcu: string

    // loading
    finishLoadData = true
    finishLoadDataCount = 0

    //province
    listProvinceAll: Province[]
    listProvinceAllMaster: Province[]
    listProvinceApi: string[];
    selectedProvince: string

    //Unit
    listUnit: ListUnit[]

    //position
    listPosition: Position[]
    listPositionApi: string[]
    selectedPosition: string

    constructor(
      private service: ConfigurationService,
      private serviceProfile: ProfileService,
      private modalService: NgbModal,
      private formBuilder: FormBuilder,
      private http: HttpClient,
      private serviceImage: FileService,
      private route: ActivatedRoute,
      private router: Router,
      private calendar: NgbCalendar,
      private serviceAdministration: AdministrationService,
      public formatter: NgbDateParserFormatter,
      public roles: AuthfakeauthenticationService,
      private swalAlert: SwalAlert,
      private serviceAwp: AwpService
    ) {
      const current = new Date();
      this.minDate = {
          year: current.getFullYear(),
          month: 1,
          day: 1
      };
      this.maxDate = {
          year: current.getFullYear(),
          month: 12,
          day: 31
      };
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Konfigurasi' },{ label: 'Daftar Pengguna' }, { label: 'Detail Pengguna', active: true }];
    this.changePageUser = 1
    this.detailConfiguration = this.route.snapshot.paramMap.get('id')
    this.getDetailConfiguration()
    this.getListHolidayManagement()
    this.getListWorkUnit()
    this.userLoginId = localStorage.getItem('user_id')
  }

    /** Function Start Loading */
    startLoading() {
        if (this.finishLoadDataCount == 0) {
            this.finishLoadDataCount ++
            this.finishLoadData = false
        } else {
            this.finishLoadDataCount ++
        }
    }

    /** Function Stop Loading */
    stopLoading() {
        this.finishLoadDataCount --
        if (this.finishLoadDataCount == 0) {
            this.finishLoadData = true

        }
    }

    myClass(date:NgbDateStruct) {
        let isSelected=this.disabledDates
            .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
        return isSelected?'classSelected':'classNormal'
    }
    isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

    keyPressAlphaNumeric(event) {

        var inp = String.fromCharCode(event.keyCode);

        if (/[a-zA-Z0-9]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    changeStatusActive() {
        this.changeStatus = !this.changeStatus
        console.log(this.changeStatus)
    }

    goToListConfiguration() {
      if (this.changePageUser == 1) {
          this.router.navigate(['configuration/list'])
      } else if (this.changePageUser == 2) {
          this.changePageUser = 1
      }
    }

    editPageUser() {
        this.changePageUser = 2
    }

    detailPageUser() {
        this.changePageUser = 1
    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        return !(charCode > 31 && (charCode < 48 || charCode > 57));
    }

    /**
     * Open extra large modal
     * @param selectedMenu extra large modal data
     */
    openModal(selectedMenu: any) {
        this.modalService.open(selectedMenu, { size: 'lg', centered: true });
    }

    /** * close modal */
    closeModal(){
        this.modalService.dismissAll()
    }

    searchFilter(e) {
        const searchStr = e.target.value
        this.listRolesSelectedExist = this.oListRolesSelectedExist.filter((product) => {
            return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
        });
    }

    openFilterRoles() {
        this.openModalRoles = !this.openModalRoles
    }

    // new tag
    CreateNew(Position){
        return Position
    }

  getDetailConfiguration() {
      this.startLoading()
      this.editFirstName = ''
      this.editLastName = ''
      this.roleRequest = []
      this.selectedFinishTaskDate = null
    this.service.detailConfiguration(this.detailConfiguration).subscribe(
        resp => {
          if (resp.success) {
            this.detailConfigurationModel = resp.data
              this.profileModel = resp.data
              if (this.profileModel?.id === this.userLoginId) {
                  this.allowEditUser = false
              } else {
                  this.allowEditUser = true
              }
              if (this.profileModel.resources) {
                  this.selectedWorkUnit = this.profileModel?.resources?.position?.unit?.name
              } else {
                  this.selectedWorkUnit = 'PMU'
              }
              this.selectedProvince = this.profileModel?.province?.name
              this.editFirstName = this.profileModel.firstName
              this.editLastName = this.profileModel.lastName
              this.selectedPosition = this.profileModel.resources?.position?.name
              if (this.profileModel.lsp) {
                  this.profileModel.convertExpiredEndDate = new Date(this.profileModel.lsp.endDate)
                  this.profileModel.convertExpiredStartDate = new Date(this.profileModel.lsp.startDate)
                  this.fromDate = new NgbDate(this.profileModel.convertExpiredStartDate.getFullYear(), this.profileModel.convertExpiredStartDate.getMonth() + 1, this.profileModel.convertExpiredStartDate.getDate())
                  this.toDate = new NgbDate(this.profileModel.convertExpiredEndDate.getFullYear(), this.profileModel.convertExpiredEndDate.getMonth() + 1, this.profileModel.convertExpiredEndDate.getDate())
              }
              this.profileModel.editEmailExist = this.profileModel.email.split('@')
              if (this.profileModel.component) {
                  this.selectedComponentExist = this.profileModel.component
                  this.selectedComponentExist.code = 'Komponen ' + this.selectedComponentExist.code
              }
              this.editEmail = this.profileModel.editEmailExist[0]
              this.getListRoles()
              this.listAwp()
              this.getListPosition(this.profileModel?.resources?.position?.unit?.id)
              this.changeStatus = this.detailConfigurationModel.active
              this.detailConfigurationModel.roles.forEach(dataRoles => {
                  this.roleRequest.push(dataRoles?.role)
              })
              this.roleLspExist = this.roleRequest.filter(filterLsp => filterLsp.name === 'LSP')
              if (this.roleLspExist.length > 0) {
                  this.lspRoleSelected = true
              } else {
                  this.lspRoleSelected = false
              }

              if (this.selectedWorkUnit === 'PMU') {
                  if (this.profileModel.resources) {
                      this.editPhoneNumber = this.profileModel.resources.phoneNumber.substr(3)
                      this.editPosition = this.profileModel.resources.position.name
                      this.getListSuperVisor()
                      this.getAllSuperVisor()
                  }
              } else if (this.selectedWorkUnit === 'PCU') {
                  this.listProvince()
              }
            if (this.detailConfigurationModel.profilePicture) {
              this.getFilePhotos()
            }
            let x = this.detailConfigurationModel.roles.filter(x => x.role.name === 'LSP')
            this.roleLspExisting = x.length > 0;
            this.listEventLsp()
              this.stopLoading()
          } else {
              this.stopLoading()
          }
        }, error => {
            this.stopLoading()
            this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getFilePhotos() {
    this.startLoading()
    this.serviceImage.getUrlPhoto(this.detailConfigurationModel?.profilePicture?.id).subscribe(
        resp => {
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.urlPhotoProfile = reader.result;
            this.stopLoading()
          }, false);
          reader.readAsDataURL(resp);
        }, error => {
            this.stopLoading()
        }
    )
  }

    addDeleteRolesSelected(data, check) {
        if (check == 1) {
            let checkExist = false
            this.roleRequest.filter(exist => {
                if (exist.id === data.id) {
                    checkExist = true
                    return
                }
            })
            if (!checkExist) {
                let checkRemoveExist = false
                this.removeRolesSelected.forEach((check, index) => {
                    if (check.id === data.id) {
                        this.removeRolesSelected.splice(index, 1)
                        checkRemoveExist = true
                        return
                    }
                })
                if (!checkRemoveExist) {
                    data.status = 1
                    this.addRolesSelected.push(data)
                }
                this.roleRequest.push(data)
            }
        } else {
            this.roleRequest.forEach((existing, index) => {
                if (existing.id === data.id) {
                    data.checked = false
                    this.roleRequest.splice(index, 1)

                }
            })
            let checkAddExist = false

            this.addRolesSelected.forEach((check, index) => {
                if (check.id === data.id) {
                    this.addRolesSelected.splice(index, 1)
                    checkAddExist = true
                    return
                }
            })

            if (!checkAddExist) {
                data.status = 0
                this.removeRolesSelected.push(data)
            }
        }

        this.listRolesSelectedExist = this.filterRole

        if (this.selectedWorkUnit === 'PMU') {
            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name !== 'PCU')
        } else if (this.selectedWorkUnit === 'PCU') {
            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name === 'PCU')
        }

        this.roleRequest.forEach(dataSelected => {
            if (dataSelected.name === 'LSP') {
                this.listRolesSelectedExist = []
                return
            }
            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter =>  dataFilter.name !== 'LSP')
            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => dataFilter.id !== dataSelected.id)
            if (dataSelected.supervisiorId !== null) {
                this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => (dataFilter.supervisiorId === dataSelected.supervisiorId || dataFilter.supervisiorId == null))
            }
        })

        this.oListRolesSelectedExist = this.listRolesSelectedExist

        this.roleLspExist = this.roleRequest.filter(filterLsp => filterLsp.name === 'LSP')

        if (this.roleLspExist.length > 0) {
            this.lspRoleSelected = true
        } else {
            this.lspRoleSelected = false
        }
        this.getListSuperVisor()
    }

    changeSelectedWorkUnit() {
        let selectedUnitId = this.listUnit.filter(x => x.name === this.selectedWorkUnit)
        this.roleRequest = []
        this.addRolesSelected = []
        this.roleLspExist = []
        this.editPosition = null
        this.selectedSupervisorExist = null
        this.editPhoneNumber = null
        this.listRolesSelectedExist = this.filterRole
        if (this.selectedWorkUnit === 'PMU') {
            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name !== 'PCU')
            this.oListRolesSelectedExist = this.listRolesSelectedExist
            this.getListPosition(selectedUnitId[0].id)
        } else if (this.selectedWorkUnit === 'PCU') {
            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name === 'PCU')
            this.oListRolesSelectedExist = this.listRolesSelectedExist
            this.getListPosition(selectedUnitId[0].id)
        }
    }

    /** * List Province */

    listProvince() {
        let tag = []
        // this.startLoading()
        this.serviceAwp.province().subscribe(
            resp => {
                // this.stopLoading()
                if (resp.success) {
                    this.listProvinceAll = resp.data.content
                    this.listProvinceAllMaster = resp.data.content
                    this.listProvinceAll.forEach(x => {
                        tag.push(x.name)
                    })
                    this.listProvinceApi = tag
                    if (this.profileModel.resources) {
                        this.editPhoneNumber = this.profileModel.resources.phoneNumber.substr(3)
                        this.editPosition = this.profileModel.resources.position.name
                        this.getListSuperVisor()
                        this.getAllSuperVisor()
                    }
                }
            }, error => {
                // this.stopLoading()
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    /** * End List Province */

    getListWorkUnit() {
        this.startLoading()
        this.serviceProfile.listUnit().subscribe(
            resp => {
                this.stopLoading()
                if (resp.success) {
                    this.listUnit = resp.data.content
                }
            }, error => {
                this.stopLoading()
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    getListRoles() {
        this.startLoading()
        this.service.listRoleDt().subscribe(
            resp => {
                if (resp.success) {
                    this.filterRole = resp.data.content
                    this.listRolesSelectedExist = this.filterRole

                    this.roleRequest.forEach(dataSelected => {
                        if (dataSelected.name === 'LSP') {
                            this.listRolesSelectedExist = []
                            return
                        }
                        this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter =>  dataFilter.name !== 'LSP')
                        this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => dataFilter.id !== dataSelected.id)
                        if (dataSelected.supervisiorId !== null) {
                            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => (dataFilter.supervisiorId === dataSelected.supervisiorId || dataFilter.supervisiorId == null))
                        }
                    })

                    if (this.selectedWorkUnit === 'PMU') {
                        this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name !== 'PCU')
                    } else if (this.selectedWorkUnit === 'PCU') {
                        this.listRolesSelectedExist = this.listRolesSelectedExist.filter(x => x.name === 'PCU')
                    }
                    this.oListRolesSelectedExist = this.listRolesSelectedExist
                    this.stopLoading()
                }
            }, error => {
                this.stopLoading()
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    doUploadPhotoProfile() {
        this.loadingEditProfile = false
        if (this.uploadFilePhotoProfile.length > 0) {
            this.service.upload(this.uploadFilePhotoProfile[0]).subscribe(resp => {
                    if (resp.success) {
                        this.fileUploadPhoto = resp.data
                        this.saveEditProfile()
                    } else {
                        this.loadingEditProfile = true
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: resp.message,
                        });
                    }
                }, error => {
                    this.loadingEditProfile = true
                    this.swalAlert.showAlertSwal(error)
                }
            );
        } else {
            this.saveEditProfile()
        }
    }

    listEventLsp() {
        const params = {
            enablePage: false,
            paramIs: [
                {
                    field: "lspPic",
                    dataType: "string",
                    value: this.detailConfigurationModel.id
                }
            ],
            page: 0,
            sort: [
                {
                    field: "createdAt",
                    direction: "desc"
                }
            ]
        }
        this.service.listKegiatanLsp(params).subscribe(
            resp => {
                if (resp.success) {
                    this.modelKegiatan = []
                    this.modelKegiataEdit = []
                    this.modelKegiatan = resp.data.content
                    this.modelKegiataEdit = resp.data.content
                    console.log(this.modelKegiataEdit)
                }
            }, error => {
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    getListPosition(data) {
        this.listPosition = []
        let tag = []
        const params = {
            enablePage: false,
            paramIs: [
                {
                    field: "unitId",
                    dataType: "string",
                    value: data
                }
            ],
            sort: [
                {
                    field: "name",
                    direction: "asc"
                }
            ]
        }
        this.serviceProfile.listPosition(params).subscribe(
            resp => {
                if (resp.success) {
                    this.listPosition = resp.data.content
                    this.listPosition.forEach(x => {
                        tag.push(x.name)
                    })
                    this.listPositionApi = tag
                }
            }, error =>  {
                this.swalAlert.showAlertSwal(error)
            }
        )
    }


    saveEditProfile() {
        let params
        let finishDuration
        let startDate
        let endDate
        let dataSelectedKegiatan = []
        let selectedUnitId
        let selectedProvince
        this.roleRequestToApi = []
        this.roleSelected = []
        if (!this.fileUploadPhoto) {
            this.idNewPhoto = null
        } else {
            this.idNewPhoto = this.fileUploadPhoto[0]?.id
        }

        this.addRolesSelected.forEach(data => {
            this.roleSelected.push(data)
        })

        this.removeRolesSelected.forEach(data => {
            this.roleSelected.push(data)
        })


        if (this.roleSelected.length > 0) {
            this.roleSelected.forEach(data => {
                this.roleRequestToApi.push({
                    roleId: data.id,
                    status: data.status
                })
            })
        }


        if (this.selectedFinishTaskDate) {
            finishDuration = this.selectedFinishTaskDate.year.toString() + '-' + this.selectedFinishTaskDate.month.toString().padStart(2, '0') + '-' + this.selectedFinishTaskDate.day.toString().padStart(2, '0')
        }
        if (this.listKegiatan.length > 0) {
            this.listKegiatan.forEach(x => {
                dataSelectedKegiatan.push(x.id)
            })
        }

        if (this.selectedProvince) {
            selectedProvince = this.listProvinceAll?.filter(x => x.name === this.selectedProvince)
        }

        if (this.selectedWorkUnit === 'PMU') {
            if (this.roleLspExist.length > 0) {
                let withoutUnit
                if (!this.selectedComponentExist) {
                    withoutUnit = null
                } else {
                    withoutUnit = this.selectedComponentExist.id
                }
                startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
                endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')
                params = {
                    firstName: this.editFirstName,
                    lastName: this.editLastName,
                    email: this.editEmail + '@madrasah.kemenag.go.id',
                    profilePictureId: this.idNewPhoto,
                    roles: this.roleRequestToApi,
                    active: this.changeStatus,
                    componentId: withoutUnit,
                    lspStartDate: startDate,
                    lspEndDate: endDate,
                    lspAwpImplementationIds: dataSelectedKegiatan,
                    removeLspAwpImplementationIds: this.modelKegiatanDeletedIds,
                    // unitId: selectedUnitId[0].id,
                }
            } else {
                params = {
                    firstName: this.editFirstName,
                    lastName: this.editLastName,
                    email: this.editEmail + '@madrasah.kemenag.go.id',
                    profilePictureId: this.idNewPhoto,
                    roles: this.roleRequestToApi,
                    active: this.changeStatus,
                    componentId: this.selectedComponentExist.id,
                    // unitId: selectedUnitId[0].id,
                }
            }
        } else if (this.selectedWorkUnit === 'PCU') {
            params = {
                firstName: this.editFirstName,
                lastName: this.editLastName,
                email: this.editEmail + '@madrasah.kemenag.go.id',
                profilePictureId: this.idNewPhoto,
                roles: this.roleRequestToApi,
                active: this.changeStatus,
                // unitId: selectedUnitId[0].id,
                provinceId: selectedProvince[0].id
            }
        }

        this.service.updateConfiguration(this.detailConfiguration ,params).subscribe(
            resp => {
                if (resp.success) {
                    if (this.roleLspExist.length == 0) {
                        this.updateResources()
                    } else {
                        this.idNewPhoto = ''
                        this.editFirstName = ''
                        this.editLastName = ''
                        this.editEmail = ''
                        this.loadingEditProfile = true
                        this.listRolesSelectedExist = []
                        this.oListRolesSelectedExist = []
                        this.uploadFilePhotoProfile = []
                        this.roleRequestToApi = []
                        this.roleSelected = []
                        this.removeRolesSelected = []
                        this.addRolesSelected = []
                        this.listKegiatan = []
                        this.modelKegiatanDeletedExisting = []
                        this.modelKegiatanDeletedIds = []
                        this.finishLoadData = false
                        this.getDetailConfiguration()
                        this.closeModal()
                        this.changePageUser = 1
                    }
                }
            }, error => {
                this.loadingEditProfile = true
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    updateResources() {
        const selectedUnitId = this.listUnit.filter(x => x.name === this.selectedWorkUnit)
        let superVisorSelected

        if (this.selectedSupervisorExist) {
            superVisorSelected = this.selectedSupervisorExist.id
        } else  {
            superVisorSelected = null
        }
        const params = {
            userId: this.profileModel.id,
            position: this.editPosition,
            phoneNumber: '+62'+this.editPhoneNumber,
            supervisiorId: superVisorSelected,
            unitId: selectedUnitId[0].id,
        }
        this.serviceAdministration.updateResources(this.profileModel.resources.id ,params).subscribe(
            resp => {
                if (resp.success) {
                    this.idNewPhoto = ''
                    this.editFirstName = ''
                    this.editLastName = ''
                    this.editEmail = ''
                    this.loadingEditProfile = true
                    this.listRolesSelectedExist = []
                    this.oListRolesSelectedExist = []
                    this.uploadFilePhotoProfile = []
                    this.roleRequestToApi = []
                    this.roleSelected = []
                    this.removeRolesSelected = []
                    this.addRolesSelected = []
                    this.finishLoadData = false
                    this.getDetailConfiguration()
                    this.changePageUser = 1
                }
            }, error => {
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    selectedUnit(data) {
        this.selectedComponentExist = null
        this.listComponentStaff = this.componentExist

        this.selectedComponentExist = data
        this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)
        this.getListSuperVisor()
    }

    searchFilterUnit(e) {
        const searchStr = e.target.value
        //
        let filter = this.componentExist
        if (this.selectedComponentExist) {
            filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
        } else {
            filter = this.componentExist
        }

        this.listComponentStaff = filter.filter((product) => {
            return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
        });
    }

    listAwp() {
        this.service.listAwp().subscribe(
            resp => {
                if (resp.success) {
                    this.componentExist = resp.data.content
                    this.listComponentStaff = resp.data.content
                    this.listComponentStaff.forEach(dataCode => {
                        dataCode.code = 'Komponen ' + dataCode.code
                    })
                    this.listComponentStaff = this.listComponentStaff.filter(dataStaff => dataStaff.id != this.selectedComponentExist.id)
                }
            }, error => {
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    getListHolidayManagement() {
        this.listHolidays = []
        this.serviceAdministration.listHolidayManagement().subscribe(
            resp => {
                if (resp.success) {
                    this.holidayManagement = resp.data.content
                    this.holidayManagement.forEach(respHolidays => {
                        respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
                        this.disabledDates.push(
                            {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
                        )
                    })
                }
            }, error => {
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    changeSuperVisorPcu() {
        this.getListSuperVisor()
    }

    getListSuperVisor() {
        this.loadingGetListSuperVisor = true
        this.selectedSupervisorExist = null
        let roleIdsSelected = []
        let params
        let selectedProvince
        if (this.selectedWorkUnit === 'PMU') {
            if (this.selectedComponentExist && this.roleRequest.length > 0) {
                this.roleRequest.forEach(data => {
                    roleIdsSelected.push(data.id)
                })
                params = {
                    componentId: this.selectedComponentExist.id,
                    roleIds: roleIdsSelected,
                    userId: this.detailConfigurationModel.id
                }
                this.service.listSupervisorByComponent(params).subscribe(
                    resp => {
                        if (resp.success) {
                            this.loadingGetListSuperVisor = false
                            this.listSupervisor = []
                            this.listSupervisorExist = []
                            this.listSupervisor = resp.data.content
                            this.listSupervisorExist = resp.data.content
                            const x = this.listSupervisor.filter(dataSP => dataSP.id == this.profileModel.resources.supervisiorId)
                            this.selectedSupervisorExist = x[0]
                            this.listSupervisor = this.listSupervisor.filter(listData => listData.id !== this.profileModel.resources.supervisiorId)
                            if (!this.selectedSupervisorExist) {
                                this.listSupervisor = this.listSupervisorExist
                                console.log(this.listSupervisorExist)
                            }
                        }
                    }, error => {
                        this.loadingGetListSuperVisor = false
                        this.swalAlert.showAlertSwal(error)
                    }
                )
            } else {
                this.loadingGetListSuperVisor = false
            }
        } else if (this.selectedWorkUnit === 'PCU') {
            if (this.selectedProvince && this.editPosition) {
                selectedProvince = this.listProvinceAll.filter(x => x.name === this.selectedProvince)
                params = {
                    provinceId: selectedProvince[0].id,
                    userId: this.profileModel?.id,
                    position: this.editPosition
                }
                this.serviceProfile.listSupervisorByRolePcu(params).subscribe(
                    resp => {
                        if (resp.success) {
                            this.loadingGetListSuperVisor = false
                            this.listSupervisor = []
                            this.listSupervisorExist = []
                            this.listSupervisor = resp.data.content
                            this.listSupervisorExist = resp.data.content
                            const x = this.listSupervisor.filter(dataSP => dataSP.id == this.profileModel.resources.supervisiorId)
                            this.selectedSupervisorExist = x[0]
                            this.listSupervisor = this.listSupervisor.filter(listData => listData.id !== this.profileModel.resources.supervisiorId)
                            if (!this.selectedSupervisorExist) {
                                this.listSupervisor = this.listSupervisorExist
                                console.log(this.listSupervisorExist)
                            }
                        }
                    }, error => {
                        this.loadingGetListSuperVisor = false
                        this.swalAlert.showAlertSwal(error)
                    }
                )
            } else {
                this.loadingGetListSuperVisor = false
            }
        }

    }

    getAllSuperVisor() {
        this.serviceAdministration.resourceAll().subscribe(
            resp => {
                if (resp.success) {
                    this.listAllSupervisor = resp.data.content
                    const x = this.listAllSupervisor.filter(dataSP => dataSP.id == this.profileModel.resources.supervisiorId)
                    this.selectedSupervisorExistMaster = x[0]
                }
            }, error => {
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    selectedSupervisor(data) {
        this.selectedSupervisorExist = null
        this.listSupervisor = this.listSupervisorExist

        this.selectedSupervisorExist = data
        this.listSupervisor = this.listSupervisor.filter(dataFilter => dataFilter.id != this.selectedSupervisorExist.id)
    }

    searchFilterSuperVisor(e) {
        const searchStr = e.target.value
        //
        let filter = this.listSupervisorExist
        if (this.selectedSupervisorExist) {
            filter = filter.filter(x => x.id !== this.selectedSupervisorExist.id)
        } else {
            filter = this.listSupervisorExist
        }

        this.listSupervisor = filter.filter((product) => {
            return ((product?.user?.firstName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product?.user?.firstName != null) || (product?.user?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product?.user?.lastName != null))
        });
    }

    isDisabledButton() {
        if (this.selectedWorkUnit === 'PMU') {
            if (this.lspRoleSelected) {
                return !this.editFirstName || !this.editEmail || this.roleRequest.length == 0 || !this.fromDate || !this.toDate || !this.selectedComponentExist || !this.selectedWorkUnit
            } else {
                return !this.editFirstName || !this.editEmail || this.roleRequest.length == 0 || !this.selectedComponentExist || !this.editPosition || !this.editPhoneNumber || !this.selectedWorkUnit
            }
        } else if (this.selectedWorkUnit === 'PCU') {
            return !this.editFirstName || !this.editEmail || this.roleRequest.length == 0 || !this.editPosition || !this.editPhoneNumber || !this.selectedWorkUnit || !this.selectedProvince
        }
    }

    disabledSelectSupervisor() {
        if (this.selectedWorkUnit === 'PMU') {
            return this.roleRequest.length === 0 || !this.selectedComponentExist
        } else if (this.selectedWorkUnit === 'PCU') {
            return !this.selectedProvince || this.roleRequest.length === 0 || !this.editPosition
        } else {
            return true
        }
    }

    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
    }

    onDateSelection(date: NgbDate, datepicker: any) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
            datepicker.close();
        } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
            this.toDate = date;
            datepicker.close();
        } else {
            this.toDate = null;
            this.fromDate = date;
            datepicker.close();
        }
    }

    validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
        const parsed = this.formatter.parse(input);
        return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
    }

    /** * Delete kegiatan */
    deleteKegiatan(i: any) {
        this.listKegiatan.splice(i, 1)
        console.log('testing')
    }

    deleteKegiatanExist(data, i) {
        this.modelKegiatanDeletedExisting.push(data)
        this.modelKegiatanDeletedIds.push(data.id)

        // if (this.modelKegiatanDeletedIds.length == 0) {
        //     this.modelKegiatanDeletedIds.push(i.id)
        // } else {
        //     let x = this.modelKegiatanDeletedIds.filter(x => x.id === i.id)
        //     if (x.length == 0) {
        //         this.modelKegiatanDeletedIds.push(i.id)
        //     }
        // }
        this.modelKegiataEdit.splice(i, 1)
        console.log(this.modelKegiataEdit)
    }

    /** * Open modal kegiatan */
    openModalKegiatan(){
        const modalRef = this.modalService.open(ModalComponent, { size: 'lg' ,centered: true });
        modalRef.componentInstance.selectedEvent = this.listKegiatan;
        modalRef.componentInstance.deletedEvent = this.modelKegiatanDeletedExisting;
        modalRef.componentInstance.componentId = this.selectedComponentExist.id
        modalRef.componentInstance.event.subscribe((data: any) => {
            /**
             * Pass data from modal
             */
            this.listKegiatan.push(data)
        });
    }

    /** * check condition notification */
    checkNotification() {
        if (this.profileModel.unit?.name === 'PMU') {
            if (this.selectedComponentExist?.id !== this.profileModel?.component?.id) {
                return true
            }
        }
    }
}
