import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPenggunaComponent } from './detail-pengguna.component';

describe('DetailPenggunaComponent', () => {
  let component: DetailPenggunaComponent;
  let fixture: ComponentFixture<DetailPenggunaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailPenggunaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPenggunaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
