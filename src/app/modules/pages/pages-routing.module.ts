import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { DefaultComponent } from "./dashboards/default/default.component";
import { ProfileComponent } from "./profile/profile.component";
import { ConfigurationComponent } from "./configuration/configuration.component";
import { DetailPenggunaComponent } from "./configuration/detail-pengguna/detail-pengguna.component";
import { EventModule } from "./event/event.module";
import { MonevComponent } from "./monev/monev.component";
import { AuthGuard } from "../../core/guards/auth.guard";

const routes: Routes = [
  { path: "", redirectTo: "dashboard" },
  { path: "dashboard", component: DefaultComponent, canActivate: [AuthGuard] },
  {
    path: "dashboards",
    loadChildren: () =>
      import("./dashboards/dashboards.module").then((m) => m.DashboardsModule),
    canActivate: [AuthGuard],
  },
  {
    path: "admin",
    loadChildren: () => import("./awp/awp.module").then((m) => m.AwpModule),
    canActivate: [AuthGuard],
  },
  {
    path: "dashboard-component",
    loadChildren: () =>
      import("./project-component/project-component.module").then(
        (m) => m.ProjectComponentModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: "monev",
    loadChildren: () =>
      import("./monev/monev.module").then((m) => m.MonevModule),
    canActivate: [AuthGuard],
  },
  { path: "profile", component: ProfileComponent, canActivate: [AuthGuard] },
  {
    path: "task",
    loadChildren: () =>
      import("./list-task/list-task.module").then((m) => m.ListTaskModule),
    canActivate: [AuthGuard],
  },
  {
    path: "file",
    loadChildren: () =>
      import("./daftar-pustaka/daftar-pustaka.module").then(
        (m) => m.DaftarPustakaModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: "administrasi",
    loadChildren: () =>
      import("./administrasi/administrasi.module").then(
        (m) => m.AdministrasiModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: "configuration",
    loadChildren: () =>
      import("./configuration/configuration.module").then(
        (m) => m.ConfigurationModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: "implement",
    loadChildren: () =>
      import("./kegiatan/kegiatan.module").then((m) => m.KegiatanModule),
    canActivate: [AuthGuard],
  },
  {
    path: "activity",
    loadChildren: () =>
      import("./event/event.module").then((m) => m.EventModule),
    canActivate: [AuthGuard],
  },
  {
    path: "surat",
    loadChildren: () =>
      import("./surat/surat.module").then((m) => m.SuratModule),
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
