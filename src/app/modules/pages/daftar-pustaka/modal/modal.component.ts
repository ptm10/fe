import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FileService} from "../../../../core/services/Image/file.service";
import {PustakaService} from "../../../../core/services/Pustaka/pustaka.service";
import * as moment from "moment";
import Swal from "sweetalert2";
import {Kegiatan} from "../../../../core/models/awp.model";
import {PustakaModel} from "../../../../core/models/pustaka.model";
import {parse} from "date-fns";
import {SwalAlert} from "../../../../core/helpers/Swal";
moment.locale('id')

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() dataPass: any;
  pdfSrc: any
  loadingSavePdf = false
  time: any;
  date: any;
  listFiles: PustakaModel[]
  loadingFiles = false


  constructor(public modal: NgbActiveModal,
              private modalService: NgbModal,
              private serviceImage: FileService,
              private service: PustakaService,
              private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
      /** Split date and time */
      let splitted = this.dataPass.createdAt.split(" ", 2);
      /** Reformatting date to YYYY MM DD */
      let convertedDate = splitted[0].split("-").reverse().join("-");
      /** Concat formatted date with time */
      let convertedFormat = convertedDate + " " + splitted[1];
      this.date = moment(convertedFormat).format('DD MMMM YYYY');
      this.time = moment(convertedFormat).format('H:mm');
      this.getListGeneralDocument()
  }

  downloadPdf(id) {
    this.loadingSavePdf = true
    this.service.exportPdf(id).subscribe(
        resp => {
          this.loadingSavePdf = false
        }, error => {
          this.loadingSavePdf = false
        }
    )
  }

    delete(id) {
        alert("Delete file with " + id + ' id');
    }

    openModal(selectedMenu: any, data) {
        this.pdfSrc = null
        this.getPdfViewer(data)
        this.modalService.open(selectedMenu, { size: 'lg', centered: true });
    }

  getPdfViewer(id) {
    this.serviceImage.getUrlPdf(id).subscribe(
        resp => {
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.pdfSrc = reader.result;
          }, false);
          reader.readAsDataURL(resp);
        }
    )
  }

  getListGeneralDocument() {
        this.loadingFiles = true
    const params = {
        enablePage: false,
        paramIs: [
            {
                field: "generalDocumentsId",
                dataType: "string",
                value: this.dataPass.id
            }
        ],
        sort: [
            {
                field: "createdAt",
                direction: "desc"
            }
        ]
    }
    this.service.listFile(params).subscribe(
        resp => {
            if (resp.success) {
                this.loadingFiles = false
                this.listFiles = resp.data.content
                this.listFiles.push(this.dataPass)
                this.listFiles.forEach(x => {
                    x.convertCreatedAt = parse(x.createdAt, 'dd-MM-yyyy HH:mm:ss', new Date());
                })
            }
        }, error => {
            this.loadingFiles = false
            this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
