import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadPustakaComponent } from './upload-pustaka.component';

describe('UploadPustakaComponent', () => {
  let component: UploadPustakaComponent;
  let fixture: ComponentFixture<UploadPustakaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UploadPustakaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadPustakaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
