import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {FileUploadControl, FileUploadValidators} from "@iplab/ngx-file-upload";
import {HttpClient, HttpEvent, HttpEventType} from "@angular/common/http";
import {CustomValidators} from "../../../../shared/custom-validators/custom-validators";
import Swal from "sweetalert2";
import {UploadFileService} from "../../../../core/services/Upload-File/upload-file.service";
import {AttachmentType} from "../../../../core/models/file.model";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {ComponentModel} from "../../../../core/models/componentModel";
import {parse} from "date-fns";
import {NgbCalendar, NgbDate, NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {PDOModel} from "../../../../core/models/PDO.model";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-upload-pustaka',
  templateUrl: './upload-pustaka.component.html',
  styleUrls: ['./upload-pustaka.component.scss']
})
export class UploadPustakaComponent implements OnInit {

  public fileUploadControl = new FileUploadControl({ listVisible: false, accept: ['.pdf'], multiple: false});
  uploadFile: Array<File> = [];
  fileUpload: FormGroup;

  validateFileName = false
  validateVersion = false
  validateFileType = false
  validateDocumentType = false
  validateDocumentClassification = false
  loadingSaveFile = true

  listFileType: AttachmentType[]
  fileTypeSelected: AttachmentType
  fileIdUpload: string
  errorFile = ''
  successSaveUploadFile = false
  respSaveFileUpload = ''
  dataListDocumentClassification: any[]
  breadCrumbItems: Array<{}>;

  // tag
  listTag: PDOModel[]
  tagName = []
  selectedTag = []

  // unit
  listComponentStaff: ComponentModel[]
  componentExist: ComponentModel[]
  selectedComponentExist: ComponentModel

  disabledDates:NgbDateStruct[] = []
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  finishLoadData = true
  finishLoadDataCount = 0
  selectedTypeFile: string

  meetingDate: any

  constructor(
      private http: HttpClient,
      private formBuilder: FormBuilder,
      private service: UploadFileService,
      private router: Router,
      private calendar: NgbCalendar,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Pustaka' }, { label: 'Dokumen Umum' }, { label: 'Upload File', active: true }];
    this.listUnitComponent()
    this.getListFileType()
    this.getListHolidayManagement()
    this.getListTag()
    this.fileUpload = this.formBuilder.group({
      fileName: ['', [Validators.required]],
      version: ['', [Validators.required]],
      fileDocumentType: ['', [Validators.required]],
      documentClassification: [this.selectedComponentExist, [Validators.required]]
    })
    this.getListDocumentClassification()
  }

  get f() { return this.fileUpload.controls; }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  changeTypeFile(e) {
    this.selectedTypeFile = e
  }

  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }
  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6


  checkValidationChangePassword(): boolean {
    this.validateFileName = false
    this.validateVersion = false
    this.validateDocumentType = false
    this.validateDocumentClassification = false

    if (this.f.fileName.invalid) {
      this.validateFileName = true
    }

    if (this.f.version.invalid) {
      this.validateVersion = true
    }

    if (this.f.fileDocumentType.invalid) {
      this.validateDocumentType = true
    }

    // if (this.selectedComponentExist) {
    //   this.validateDocumentClassification = true
    // }

    return this.f.fileName.valid && this.f.version.valid && this.f.fileDocumentType.valid
  }
  progress: number = 0;
  doUploadPhotoProfile() {
    this.loadingSaveFile = false
    this.validateFileType = false
    this.successSaveUploadFile = false
    this.respSaveFileUpload = ''
    let fileTypeNumber: string

    this.listFileType.forEach(data => {
      if (data.fileType === this.f.fileDocumentType.value) {
        fileTypeNumber = data.type.toString()
      }
    })
    this.validateFileType = false
    if (this.checkValidationChangePassword()) {
      if (this.fileUploadControl.value.length > 0 && this.fileUploadControl.value[0].type == 'application/pdf') {
        if (this.fileUploadControl.value[0].size <= 10000000) {
          this.service.upload(this.fileUploadControl.value[0], fileTypeNumber).subscribe(event => {
            switch (event.type) {
              case HttpEventType.Sent:
                console.log('Request has been made!');
                break;
              case HttpEventType.ResponseHeader:
                console.log('Response header has been received!');
                break;
              case HttpEventType.UploadProgress:
                this.progress = Math.round(event.loaded / event.total * 100);
                console.log(`Uploaded! ${this.progress}%`);
                break;
              case HttpEventType.Response:
                console.log('User successfully created!', event.body);
                if (event.body.success) {
                  this.fileIdUpload = event.body.data[0].id
                  this.saveUploadFile()
                } else {
                  this.loadingSaveFile = true
                  Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: event.body.message,
                  });
                }
            }
          });
        } else {
          this.loadingSaveFile = true
          this.validateFileType = true
          this.errorFile = 'File PDF tidak boleh lebih dari 10 MB'
        }
      } else {
        this.loadingSaveFile = true
        if (this.fileUploadControl.value?.length == 0 || this.fileUploadControl?.value[0]?.type == 'application/pdf') {
          this.validateFileType = true
          this.errorFile = 'File Tidak boleh kosong'
        } else if (this.fileUploadControl.value?.length > 0 || this.fileUploadControl?.value[0]?.type != 'application/pdf') {
          this.validateFileType = true
          this.errorFile = 'File harus dalam tipe PDF'
        }
      }
    } else {
      this.loadingSaveFile = true
      if (this.fileUploadControl.value.length == 0) {
        this.validateFileType = true
        this.errorFile = 'File Tidak boleh kosong'
      }
    }
  }

  saveUploadFile() {
    let tagIds = []
    let newTagIds = []
    let typeFileId
    let params
    let componentId
    let startDate

    const results = this.listTag.filter(({ name: id1 }) => this.selectedTag.some(id2 => id2 === id1));
    const results2 = this.selectedTag.filter(id1 => !this.listTag.some(({name: id2}) => id2 === id1));

    if (results) {
      results.forEach(data => {
        tagIds.push(data.id)
      })
    } else {
      tagIds = []
    }

    this.listFileType.forEach(data => {
      if (data.fileType === this.f.fileDocumentType.value) {
        typeFileId = data.id
      }
    })

    if (this.selectedComponentExist) {
      componentId = this.selectedComponentExist.id
    } else {
      componentId = null
    }

    if (this.meetingDate) {
      startDate = this.meetingDate.year.toString() + '-' + this.meetingDate.month.toString().padStart(2, '0') + '-' + this.meetingDate.day.toString().padStart(2, '0')
    }

    if (this.selectedTypeFile === 'MoM') {
      params = {
        name: this.f.fileName.value,
        version: this.f.version.value,
        filesId: this.fileIdUpload,
        fileType: typeFileId,
        componentId: componentId,
        meetingDate: startDate,
        newTags: results2,
        tagIds: tagIds,
      }
    } else {
      params = {
        name: this.f.fileName.value,
        version: this.f.version.value,
        filesId: this.fileIdUpload,
        fileType: typeFileId,
        componentId: this.selectedComponentExist.id,
        newTags: results2,
        tagIds: tagIds,
      }
    }


    this.service.saveUploadFile(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveFile = true
            this.router.navigate(['file/pustaka'])
          } else {
            this.loadingSaveFile = true
          }
        }, error => {
          this.loadingSaveFile = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListFileType() {
    this.startLoading()
    this.service.listDocumentType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listFileType = resp.data.content
          } else {
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  goToListPustaka() {
    this.router.navigate(['file/pustaka'])
  }

  getListDocumentClassification() {
    this.service.listDocumentClassification().subscribe(
        resp => {
          if (resp.success) {
            this.dataListDocumentClassification = resp.data.content
          } else {

          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  // unit

  listUnitComponent() {
    this.startLoading()
    this.service.listUnit().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff = resp.data.content
            this.listComponentStaff.forEach(dataCode => {
              dataCode.code = 'Komponen ' + dataCode.code
            })
            this.listComponentStaff = this.listComponentStaff.filter(dataStaff => dataStaff.id != this.selectedComponentExist.id)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedUnit(data) {
    this.selectedComponentExist = null
    this.listComponentStaff = this.componentExist

    this.selectedComponentExist = data
    this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)
  }

  searchFilterUnit(e) {
    const searchStr = e.target.value
    let filter = this.componentExist
    if (this.selectedComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
    } else {
      filter = this.componentExist
    }

    this.listComponentStaff = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }


  clearSelectedUnit() {
    this.selectedComponentExist = null
    this.listComponentStaff = this.componentExist
  }

  // holiday

  getListHolidayManagement() {
    this.listHolidays = []
    this.service.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  // new tag
  CreateNew(city){
    return city
  }

  getListTag() {
    this.startLoading()
    let tag = []
    this.service.listTag().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            tag = []
            this.listTag = resp.data.content
            this.listTag.forEach(x => {
              tag.push(x.name)
            })
            this.tagName = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  disableButtons() {
    if (this.selectedTypeFile === 'MoM') {
      return !this.meetingDate || !this.loadingSaveFile
    } else {
      return !this.loadingSaveFile
    }
  }

}
