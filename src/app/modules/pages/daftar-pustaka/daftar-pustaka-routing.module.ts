import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {DaftarPustakaComponent} from "./daftar-pustaka.component";
import {UploadPustakaComponent} from "./upload-pustaka/upload-pustaka.component";
import {AuthGuard} from "../../../core/guards/auth.guard";

const routes: Routes = [
    {
        path: 'pustaka',
        component: DaftarPustakaComponent, canActivate: [AuthGuard]
    },
    {
        path: 'upload',
        component: UploadPustakaComponent, canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DaftarPustakaRoutingModule {}
