import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DaftarPustakaComponent } from './daftar-pustaka.component';

describe('DaftarPustakaComponent', () => {
  let component: DaftarPustakaComponent;
  let fixture: ComponentFixture<DaftarPustakaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DaftarPustakaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DaftarPustakaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
