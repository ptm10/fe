import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../core/services/Awp/awp.service";
import {Router} from "@angular/router";
import {leangueIndoDT} from "../../../shared/pagination-custom";
import {DataResponse} from "../../../core/models/dataresponse.models";
import {environment} from "../../../../environments/environment";
import {CommonCons} from "../../../shared/CommonCons";
import {PustakaModel} from "../../../core/models/pustaka.model";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {PustakaService} from "../../../core/services/Pustaka/pustaka.service";
import {FileService} from "../../../core/services/Image/file.service";
import Swal from "sweetalert2";
import {UploadFileService} from "../../../core/services/Upload-File/upload-file.service";
import {PDOModel} from "../../../core/models/PDO.model";
import {ModalComponent} from "./modal/modal.component";
import {SwalAlert} from "../../../core/helpers/Swal";

@Component({
  selector: 'app-daftar-pustaka',
  templateUrl: './daftar-pustaka.component.html',
  styleUrls: ['./daftar-pustaka.component.scss']
})
export class DaftarPustakaComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};

  private prefixUrl = environment.serverFile

  paramsLike = []
  finishLoadDataCount = 0
  finishLoadData = true
  filterName: string
  start = 1
  listFilePustaka: PustakaModel[]
  pdfSrc: any
  loadingSavePdf = false
  breadCrumbItems: Array<{}>;

  //date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  paramDateBetween = []

  // tag
  tagName = []
  selectedTag = []
  listTag: PDOModel[]

  paramsIn = []

  @Input() dataPass: any;

  constructor(
      private http: HttpClient,
      private router: Router,
      private modalService: NgbModal,
      private service: PustakaService,
      private serviceUpload: UploadFileService,
      private serviceImage: FileService,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.getListTag()
    this.breadCrumbItems = [{ label: 'Pustaka' }, { label: 'Dokumen Umum', active: true }];
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: [],
          paramIn: this.paramsIn,
          paramNotIn: [],
          paramDateBetween: this.paramDateBetween,
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverKnowledge + 'general-documents/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.listFilePustaka = resp.data.content
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)

        });
      },
      columns: [{ data: 'id' }, { data: 'name'}, {data: 'files.attachmentType.fileType'}, {data: 'version'}, {data: 'component.code'},{data: 'createdAt'}, {data: 'version'}],
      order: [[5, 'desc']]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.paramsLike = []
    this.paramDateBetween = []

    if (this.fromDate && this.toDate) {
      let startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
      let endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')
      const selectedDateStartDate = {
        field: "createdAt",
        startDate: startDate,
        endDate: endDate,
      }
      this.paramDateBetween.push(selectedDateStartDate)
    }

    if (this.filterName) {
      const paramsFirstName = {
        field: "name",
        dataType: "string",
        value: this.filterName
      }
      const paramsFileType = {
        field: "files.attachmentType.fileType",
        dataType: "string",
        value: this.filterName
      }
      const paramsVersion = {
        field: "version",
        dataType: "string",
        value: this.filterName
      }
      const paramsComponentCode = {
        field: "component.code",
        dataType: "string",
        value: this.filterName
      }

      this.paramsLike.push(paramsFirstName)
      this.paramsLike.push(paramsFileType)
      this.paramsLike.push(paramsVersion)
      this.paramsLike.push(paramsComponentCode)
    } else {
      this.paramsLike = []
    }

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }


  filterTagExist() {
    let tagFiltered = []
    this.paramsIn = []
    if (this.selectedTag.length > 0) {
      this.selectedTag.forEach(filteredName => {
        let x = this.listTag.find(x => x.name === filteredName)
        tagFiltered.push(x.id)
      })
      this.paramsIn.push({
        field: "tagId",
        dataType: "string",
        value: tagFiltered
      })
    } else {
      this.paramsIn = []
    }
    this.refreshDatatable()
  }

  goToUploadFile() {
    this.router.navigate(['file/upload'])
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any, data) {
    this.pdfSrc = null
    this.getPdfViewer(data.filesId)
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /**
   * Open modal with options
   */
  openModalList(data){
    const modalRef = this.modalService.open(ModalComponent, { size: 'lg', centered: true });
    modalRef.componentInstance.dataPass = data;
  }

  downloadPdf(id) {
    this.loadingSavePdf = true
    this.service.exportPdf(id).subscribe(
        resp => {
          this.loadingSavePdf = false
        }, error => {
          this.loadingSavePdf = false
        }
    )
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  getPdfViewer(id) {
    this.serviceImage.getUrlPdf(id).subscribe(
        resp => {
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.pdfSrc = reader.result;
          }, false);
          reader.readAsDataURL(resp);
        }
    )
  }

  // date

  clearFilterDate() {
    this.fromDate = null
    this.toDate = null
    this.refreshDatatable()
  }

  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      this.refreshDatatable()
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  getListTag() {
    this.startLoading()
    let tag = []
    this.serviceUpload.listTag().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            tag = []
            this.listTag = resp.data.content
            this.listTag.forEach(x => {
              tag.push(x.name)
            })
            this.tagName = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
