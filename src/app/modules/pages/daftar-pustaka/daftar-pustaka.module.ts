import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbAlertModule, NgbDatepickerModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {DaftarPustakaRoutingModule} from './daftar-pustaka-routing.module'
import {DaftarPustakaComponent} from "./daftar-pustaka.component";
import {DataTablesModule} from "angular-datatables";
import {PdfViewerModule} from "ng2-pdf-viewer";
import { UploadPustakaComponent } from './upload-pustaka/upload-pustaka.component';
import {FileUploadModule} from "@iplab/ngx-file-upload";
import {UIModule} from "../../../shared/ui/ui.module";
import { ModalComponent } from './modal/modal.component';
import {SwalAlert} from "../../../core/helpers/Swal";

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [DaftarPustakaComponent, UploadPustakaComponent, ModalComponent],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        DaftarPustakaRoutingModule,
        NgbTooltipModule,
        DataTablesModule,
        PdfViewerModule,
        FileUploadModule,
        NgbAlertModule,
        UIModule,
        NgbDatepickerModule

    ],
  providers: [
      SwalAlert
  ]
})
export class DaftarPustakaModule { }
