import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAwpComponent } from './detail-awp.component';

describe('DetailAwpComponent', () => {
  let component: DetailAwpComponent;
  let fixture: ComponentFixture<DetailAwpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailAwpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAwpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
