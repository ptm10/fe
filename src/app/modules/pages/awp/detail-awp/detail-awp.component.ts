import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import Swal from "sweetalert2";
import {AwpModel} from "../../../../core/models/awp.model";
import {FileService} from "../../../../core/services/Image/file.service";
import {PustakaService} from "../../../../core/services/Pustaka/pustaka.service";
import {parse} from "date-fns";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-detail-awp',
  templateUrl: './detail-awp.component.html',
  styleUrls: ['./detail-awp.component.scss']
})
export class DetailAwpComponent implements OnInit {

  detailIdAwp: string
  dataDetailAwp: AwpModel
  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  loadingSavePdf = false
  currentDate: any
  awpYear: any
  constructor(
      private router:ActivatedRoute,
      private route: Router,
      private service: AwpService,
      private serviceFile: PustakaService,
      public roles: AuthfakeauthenticationService,
      private serviceKegiatan: KegiatanService,
      private swalAlert: SwalAlert,
      private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'AWP' }, { label: 'Detail AWP', active: true }]
    this.detailIdAwp = this.router.snapshot.paramMap.get('id')
    this.getDetailAwp()
    this.currentDate = new Date().getFullYear()
  }

  getDetailAwp() {
    this.startLoading()
    this.service.detailAwp(this.detailIdAwp).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataDetailAwp = resp.data
            this.dataDetailAwp.convertStartDate = new Date(this.dataDetailAwp.startDate)
            this.dataDetailAwp.convertEndDate = new Date(this.dataDetailAwp.endDate)
            let x = parse(this.dataDetailAwp?.year.toString(), 'yyyy', new Date());
            this.awpYear = x.getFullYear()
            this.dataDetailAwp?.pdo?.forEach(dataPdo => {
              if (this.dataDetailAwp.year == 2020) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2020
              } else if (this.dataDetailAwp.year == 2021) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2021
              } else if (this.dataDetailAwp.year == 2022) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2022
              } else if (this.dataDetailAwp.year == 2023) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2023
              } else if (this.dataDetailAwp.year == 2024) {
                dataPdo.pdo.selectedYearInformation = dataPdo?.pdo?.y_2024
              }
            })
            this.dataDetailAwp?.iri?.forEach(dataIri => {
              if (this.dataDetailAwp.year == 2020) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2020
              } else if (this.dataDetailAwp.year == 2021) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2021
              } else if (this.dataDetailAwp.year == 2022) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2022
              } else if (this.dataDetailAwp.year == 2023) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2023
              } else if (this.dataDetailAwp.year == 2024) {
                dataIri.iri.selectedYearInformation = dataIri?.iri?.y_2024
              }
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  downloadFile(id, name) {
    this.serviceKegiatan.exportFile(id, name).subscribe(
        resp => {
        }, error => {
        }
    )
  }

  goToListAWP() {
    this.route.navigate(['admin/awp'])
  }

  goToEditAwp() {
    this.route.navigate(['admin/edit-awp/' + this.detailIdAwp])
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  downloadPdf(id, nameAwp: any) {
    this.loadingSavePdf = true
    this.serviceFile.exportPdfAwp(id, nameAwp).subscribe(
        resp => {
          this.loadingSavePdf = false
        }, error => {
          this.loadingSavePdf = false
        }
    )
  }

  deleteAwpExisting() {
    this.loadingSavePdf = true
    this.service.deleteAwp(this.dataDetailAwp?.id).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSavePdf = false
            this.closeModal()
            this.goToListAWP()
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

}
