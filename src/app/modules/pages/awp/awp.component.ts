import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {leangueIndoDT} from "../../../shared/pagination-custom";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {CommonCons} from "../../../shared/CommonCons";
import {AwpModel, InformationAwpDoc} from "../../../core/models/awp.model";
import {DataResponse} from "../../../core/models/dataresponse.models";
import {AwpService} from "../../../core/services/Awp/awp.service";
import {Router} from "@angular/router";
import {AuthfakeauthenticationService} from "../../../core/services/authfake.service";
import {SwalAlert} from "../../../core/helpers/Swal";
import {KegiatanService} from "../../../core/services/kegiatan.service";
import {ComponentModel} from "../../../core/models/componentModel";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-awp',
  templateUrl: './awp.component.html',
  styleUrls: ['./awp.component.scss']
})
export class AWPComponent implements OnInit {

  @ViewChild('notificationInformationAwp') notificationInformationAwp: TemplateRef<any>;

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};

  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;

  /**
   * variable
   */
  modelAwp: AwpModel[]
  filterYear: number
  selectedYear: number
  selectedYearRetain;
  selectedComponent;
  selectedSubComponent;
  start = 1
  dataListYear = []
  dataListYearExist = []
  finishLoadData = true
  finishLoadDataCount = 0
  breadCrumbItems: Array<{}>;
  filterData: any
  paramsLike = []
  paramsIs = []
  paramsIn = []
  loadingExportWord = false

  // unit
  checkedComponent = []
  listComponentStaff = []
  componentExist: ComponentModel[]
  checkedComponentWithoutUser = []

  // filter
  selectComponent: string[] = [];
  selectedFilterComponent: string
  selectedFilterComponentForExportReport: string[]
  selectSubComponent: string[] = [];
  selectedFilterSubComponent: string
  filterSubComponent = []
  testing = true
  detailInformationAwp: InformationAwpDoc
  yearSelected: number


  constructor(
      private http: HttpClient,
      private service: AwpService,
      private router: Router,
      public roles: AuthfakeauthenticationService,
      private swalAlert: SwalAlert,
      private serviceResources: KegiatanService,
      private modalService: NgbModal,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'AWP', active: true }];
    this.getDataTable()
    this.selectedFilterComponent = localStorage.getItem('selected_component')
    this.listComponent()
    this.getListYear()
    this.selectedYear = new Date().getFullYear()
    this.selectedYearRetain =  localStorage.getItem('selectedYear');
    if (!this.selectedYearRetain) {
      localStorage.setItem('selectedYear', String(2022));
      this.selectedYearRetain = localStorage.getItem('selectedYear');
    }
    const selectedYearChoosed = {
      field: "year",
      dataType: "int",
      value: this.selectedYearRetain
    }
    this.paramsIs.push(selectedYearChoosed)
    this.minDate = {
      year: 2020,
      month: 1,
      day: 1
    };
    this.maxDate = {
      year: 2024,
      month: 12,
      day: 31
    };
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }


  getDataTable() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramsIs,
          paramIn: this.paramsIn,
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'awp/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.modelAwp = resp.data.content;
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'createdAt' }, { data: 'component.code' }, { data: 'componentCode.code' }, { data: 'name' }, { data: 'budgetAwp' }, { data: 'description' }, { data: 'purpose' },  { data: 'id' }],
      order: [[2, 'asc']]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.filterYear = this.selectedYear
    this.paramsIs = []
    this.paramsIn = []
    if (this.checkedComponentWithoutUser.length > 0) {
      const selectedComponent = {
        field: "componentId",
        dataType: "string",
        value: this.checkedComponentWithoutUser[0]
      }
      this.paramsIs.push(selectedComponent)
    }

    if (this.filterSubComponent.length > 0) {
      const selectedSubComponent = {
        field: "subComponentId",
        dataType: "string",
        value: this.filterSubComponent[0].id
      }
      this.paramsIs.push(selectedSubComponent)
    }

    if (this.selectedYear) {
      const selectedYearChoosed = {
        field: "year",
        dataType: "int",
        value: this.filterYear
      }
      this.paramsIs.push(selectedYearChoosed)
    }

    this.paramsLike = []
    if (this.filterData) {
      const paramsFirstName = {
        field: "name",
        dataType: "string",
        value: this.filterData
      }
      const paramsBudegetAwp =  {
        field: "budgetAwp",
        dataType: "int",
        value: this.filterData
      }
      const paramsComponentCode = {
        field: "component.code",
        dataType: "string",
        value: this.filterData
      }
      const paramsComponentDescription = {
        field: "component.description",
        dataType: "string",
        value: this.filterData
      }
      const paramsSubComponentDescription = {
        field: "subComponent.description",
        dataType: "string",
        value: this.filterData
      }
      const paramsDescription = {
        field: "description",
        dataType: "string",
        value: this.filterData
      }
      const paramsPurpose = {
        field: "purpose",
        dataType: "string",
        value: this.filterData
      }
      const paramsCode = {
        field: "componentCode.code",
        dataType: "string",
        value: this.filterData
      }
      this.paramsLike.push(paramsBudegetAwp)
      this.paramsLike.push(paramsFirstName)
      this.paramsLike.push(paramsComponentDescription)
      this.paramsLike.push(paramsSubComponentDescription)
      this.paramsLike.push(paramsDescription)
      this.paramsLike.push(paramsPurpose)
      this.paramsLike.push(paramsCode)
      this.paramsLike.push(paramsComponentCode)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  /**
   * get list year
   */
  getListYear() {
    this.startLoading()
    this.service.listYear().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataListYear = []
            this.dataListYear = resp.data
            this.dataListYearExist = resp.data
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
          }
        }, error => {
          // this.finishLoadData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  selectedAwpYear(data) {
    this.selectedYear = data
    localStorage.setItem('selectedYear', data);
    this.selectedYearRetain =  localStorage.getItem('selectedYear');
    this.refreshDatatable()
  }

  clearFilterYear() {
    this.selectedYear = null
    this.selectedYearRetain = null
    this.refreshDatatable()
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.dataListYear = this.dataListYearExist.filter((product) => {
      return product.toString().search(searchStr.toString().toLowerCase()) !== -1
    });
  }

  goToDetailAwp(id) {
    this.router.navigate(['admin/detail-awp/' + id])
  }

  goToCreateAwp() {
    this.router.navigate(['admin/add-awp'])
  }

  goToExportExcel() {
    this.router.navigate(['admin/export-excel'])
  }

  listComponent() {
    this.startLoading()
    let tag = []
    this.service.listAllComponent().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data
            this.componentExist.forEach(dataCode => {
              dataCode.checked = false
              dataCode.code = 'Komponen ' + dataCode.code
              this.listComponentStaff.push(dataCode)
              tag.push(dataCode.code)
            })
            this.selectComponent = tag
            if (this.selectedFilterComponent) {
              this.checkComponentExist()
            } else {
              this.stopLoading()
            }
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )

  }

  changeFilterComponent() {
    localStorage.setItem('selected_component', this.selectedFilterComponent)
      this.checkedComponentWithoutUser = []
      let tagSubComponent = []
      let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
      let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
      this.selectedFilterSubComponent = null
      if (this.selectedFilterComponent) {
        findComponent = findComponent.replace('komponen ', '')
        filteredComponent = filteredComponent.filter(x => x.code === findComponent)
        if (filteredComponent.length > 0) {
          tagSubComponent = []
          this.checkedComponentWithoutUser.push(filteredComponent[0]?.id)
          filteredComponent[0].subComponent.forEach(x => {
            x.code = 'Sub-Komponen ' + x.code
            tagSubComponent.push(x.code)
          })
          this.selectSubComponent = []
          this.selectSubComponent = tagSubComponent
        }
      }
      localStorage.removeItem('selected_subcomponent')
      this.filterSubComponent = []
      this.refreshDatatable()
  }

  changeFilterSubComponent() {
    localStorage.setItem('selected_subcomponent', this.selectedFilterSubComponent)
    let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
    let findSubComponent = JSON.parse(JSON.stringify(this.selectedFilterSubComponent))
    let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
    let selectedFilterSubComponent
    if (this.selectedFilterSubComponent) {
      findComponent = findComponent.replace('komponen ', '')
      filteredComponent = filteredComponent.filter(x => x.code === findComponent)
      if (filteredComponent) {
        findSubComponent = findSubComponent.replace('Sub-Komponen ', '')
        selectedFilterSubComponent = filteredComponent[0].subComponent.filter(x =>x.code === findSubComponent)
        this.filterSubComponent = selectedFilterSubComponent
      }
      this.refreshDatatable()
    }
  }

  clearFilter() {
    this.selectedFilterComponent = null
    this.selectedFilterSubComponent = null
    this.selectSubComponent = []
    this.checkedComponentWithoutUser = []
    this.filterSubComponent = []
    localStorage.removeItem('selected_component')
    localStorage.removeItem('selected_subcomponent')
    this.refreshDatatable()
  }

  downloadWordAwp() {
    this.loadingExportWord = true
    this.service.exportWordAwp(this.yearSelected, `Final AWP ${this.yearSelected}`).subscribe(
        resp => {
          this.loadingExportWord = false
          this.closeModal()
        }, error => {
          this.loadingExportWord = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  downloadWordReportProject() {
    let componentId: any
    let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponentForExportReport))
    let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
    let allSelectedComponent = []
    findComponent.forEach(y => {
      componentId = filteredComponent.filter(x => x.code === y)
      allSelectedComponent.push(componentId[0].id)
    })
    this.loadingExportWord = true
    let startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    let endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')
    let params = {
      startDate: startDate,
      endDate: endDate,
      componentIds: allSelectedComponent,
    }
    this.service.exportWordReportProject(params).subscribe(
        resp => {
          this.loadingExportWord = false
          this.fromDate = null
          this.toDate = null
          this.selectedFilterComponentForExportReport = []
          this.closeModal()
        }, error => {
          this.loadingExportWord = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  goToAddOtherInformationAwp(id, name) {
    this.yearSelected = null
    this.yearSelected = id
    let yearSelectedFromExport = id - 1
    this.service.listAwpInformation(yearSelectedFromExport).subscribe(
        resp => {
          if (resp.success) {
            this.detailInformationAwp = resp.data
            if (this.detailInformationAwp?.keteranganKontribusiKomponent) {
              this.modalService.open(this.notificationInformationAwp, { size: 'lg', centered: true });
            } else {
              this.router.navigate(['admin/other-information/' + id])
            }
          }
        }
    )
  }

  goToAddOtherInformationAwpConfirmation() {
    this.closeModal()
    this.router.navigate(['admin/other-information/' + this.yearSelected])
  }

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  checkComponentExist() {
    if (this.selectedFilterComponent) {
      this.checkedComponentWithoutUser = []
      let tagSubComponent = []
      let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
      let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
      this.selectedFilterSubComponent = null
      if (this.selectedFilterComponent) {
        findComponent = findComponent.replace('komponen ', '')
        filteredComponent = filteredComponent.filter(x => x.code === findComponent)
        if (filteredComponent.length > 0) {
          tagSubComponent = []
          this.checkedComponentWithoutUser.push(filteredComponent[0]?.id)
          filteredComponent[0].subComponent.forEach(x => {
            x.code = 'Sub-Komponen ' + x.code
            tagSubComponent.push(x.code)
          })
          this.selectSubComponent = []
          this.selectSubComponent = tagSubComponent
          this.selectedFilterSubComponent = localStorage.getItem('selected_subcomponent')
        }
      }
      if (this.selectedFilterSubComponent) {
        let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
        let findSubComponent = JSON.parse(JSON.stringify(this.selectedFilterSubComponent))
        let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
        let selectedFilterSubComponent
        if (this.selectedFilterSubComponent) {
          findComponent = findComponent.replace('komponen ', '')
          filteredComponent = filteredComponent.filter(x => x.code === findComponent)
          if (filteredComponent) {
            findSubComponent = findSubComponent.replace('Sub-Komponen ', '')
            selectedFilterSubComponent = filteredComponent[0].subComponent.filter(x =>x.code === findSubComponent)
            this.filterSubComponent = selectedFilterSubComponent
          }
          this.stopLoading()
          this.refreshDatatable()
        }
      } else {
        this.stopLoading()
        this.refreshDatatable()
      }
    }
  }

  /** * end date */

  // downloadPdf(id, nameAwp: any) {
  //   this.loadingSavePdf = true
  //   this.serviceFile.exportPdfAwp(id, nameAwp).subscribe(
  //       resp => {
  //         this.loadingSavePdf = false
  //       }, error => {
  //         this.loadingSavePdf = false
  //       }
  //   )
  // }

}
