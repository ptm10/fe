import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherInformationAwpComponent } from './other-information-awp.component';

describe('OtherInformationAwpComponent', () => {
  let component: OtherInformationAwpComponent;
  let fixture: ComponentFixture<OtherInformationAwpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OtherInformationAwpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherInformationAwpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
