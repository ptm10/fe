import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {PdoIriModel, PDOModel} from "../../../../core/models/PDO.model";
import {InformationAwpDoc, Kegiatan} from "../../../../core/models/awp.model";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {KegiatanService} from "../../../../core/services/kegiatan.service";

@Component({
  selector: 'app-other-information-awp',
  templateUrl: './other-information-awp.component.html',
  styleUrls: ['./other-information-awp.component.scss']
})
export class OtherInformationAwpComponent implements OnInit {

  public wysiwygEditor = ClassicEditor;
  fileUpload = []
  fileUploadInformation = []


  // upload information
  filesExistInformationComponentContribution = []
  filesExistInformationProgressProgram = []
  filesExistInformationConstraint = []
  filesExistInformationLearning = []
  filesExistInformationWorkPlan = []
  filesExistInformationActivityProposal = []
  flagUploadInformationComponentContribution = false
  flagUploadInformationProgressProgram = false
  flagUploadInformationConstraint = false
  flagUploadInformationLearning = false
  flagUploadInformationWorkPlan = false
  flagUploadInformationActivityProposal = false
  uploadFileInformationComponentContribution: Array<File> = [];
  uploadFileInformationProgressProgram: Array<File> = [];
  uploadFileInformationConstraint: Array<File> = [];
  uploadFileInformationLearning: Array<File> = [];
  uploadFileInformationWorkPlan: Array<File> = [];
  uploadFileInformationActivityProposal: Array<File> = [];


  // upload component
  filesExistComponent1 = []
  filesExistComponent2 = []
  filesExistComponent3 = []
  filesExistComponent4 = []
  flagUploadPhotoComponent1 = false
  flagUploadPhotoComponent2 = false
  flagUploadPhotoComponent3 = false
  flagUploadPhotoComponent4 = false
  uploadFileComponent1: Array<File> = [];
  uploadFileComponent2: Array<File> = [];
  uploadFileComponent3: Array<File> = [];
  uploadFileComponent4: Array<File> = [];

  breadCrumbItems: Array<{}>;
  detailYear: any
  yearInformationAwp: number
  yearInformationPdoIri: number
  //loading
  finishLoadData = true
  finishLoadDataCount = 0
  pdoIriModel: PdoIriModel[] = []
  detailInformationAwp: InformationAwpDoc
  timeout: any = null;
  timeOutTotal: any = null;


  //field update
  component1FirstDipa: any = null
  component1RevisionDipa: any = null
  component1Realization: any = null
  component1Percentage: number = 0

  component2FirstDipa: any = null
  component2RevisionDipa: any = null
  component2Realization: any = null
  component2Percentage: number = 0

  component3FirstDipa: any = null
  component3RevisionDipa: any = null
  component3Realization: any = null
  component3Percentage: number = 0

  component4FirstDipa: any = null
  component4RevisionDipa: any = null
  component4Realization: any = null
  component4Percentage: number = 0

  pureValueFirstDipa: any = null
  pureValueRevisionDipa: any = null
  pureValueRealization: any = null
  pureValuePercentage: number = 0

  totalValueFirstDipa: any = null
  totalValueRevisionDipa: any = null
  totalValueRealization: any = null
  totalValuePercentage: number = 0
  obstacleComponent1: string = null
  obstacleComponent2: string = null
  obstacleComponent3: string = null
  obstacleComponent4: string = null
  informationComponentContribution: string = null
  informationProgressProgram: string = null
  informationConstraint: string = null
  informationLearning: string = null
  informationWorkPlan: string = null
  informationActivityProposal: string = null
  componentFocus1: string = null
  componentFocus2: string = null
  componentFocus3: string = null
  componentFocus4: string = null
  deletedPhotoComponentIds = []
  deletedDocumentsIds = []
  loadingUploadData = false

  constructor(
      private router: Router,
      private activatedRouter: ActivatedRoute,
      private service: AwpService,
      private swalAlert: SwalAlert,
      private activityService: KegiatanService
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'AWP' }, { label: 'Tambah Informasi AWP', active: true }]
    this.detailYear = Number(this.activatedRouter.snapshot.paramMap.get('year'))
    this.changeYearSelected()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  openUploadPhotoComponent(component: number) {
    if (component === 1) {
      this.flagUploadPhotoComponent1 = !this.flagUploadPhotoComponent1
    } else if (component === 2) {
      this.flagUploadPhotoComponent2 = !this.flagUploadPhotoComponent2
    } else if (component === 3) {
      this.flagUploadPhotoComponent3 = !this.flagUploadPhotoComponent3
    } else if (component === 4) {
      this.flagUploadPhotoComponent4 = !this.flagUploadPhotoComponent4
    }
  }


  changeYearSelected() {
    let selectedYear = Number(this.detailYear)
    this.yearInformationAwp = selectedYear - 1
    this.yearInformationPdoIri = (selectedYear - 2020) + 1
    this.getListAwpByYear(this.yearInformationAwp)
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  goToListAWP() {
    this.router.navigate(['admin/awp'])
  }

  changeValueComponent(component: number) {
    let percentage
    let sumValue
    var that = this;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(function () {
      if (component === 1) {
        if (that.component1RevisionDipa && that.component1RevisionDipa !== '0' && that.component1Realization && that.component1Realization !== '0') {
          sumValue = that.component1Realization / that.component1RevisionDipa
          percentage = Number(sumValue.toFixed(2))
          percentage = sumValue * 100
          percentage = Number(percentage.toFixed(2))
          that.component1Percentage = percentage
        } else {
          that.component1Percentage = 0
        }
      } else if (component === 2) {
        if (that.component2RevisionDipa && that.component2RevisionDipa !== '0' && that.component2Realization && that.component2Realization !== '0') {
          sumValue = that.component2Realization / that.component2RevisionDipa
          percentage = sumValue.toFixed(2)
          percentage = percentage * 100
          percentage = Number(percentage.toFixed(2))
          that.component2Percentage = percentage
        } else {
          that.component2Percentage = 0
        }
      } else if (component === 3) {
        if (that.component3RevisionDipa && that.component3RevisionDipa !== '0' && that.component3Realization && that.component3Realization !== '0') {
          sumValue = that.component3Realization / that.component3RevisionDipa
          percentage = sumValue.toFixed(2)
          percentage = percentage * 100
          percentage = Number(percentage.toFixed(2))
          that.component3Percentage = percentage
        } else {
          that.component3Percentage = 0
        }
      } else if (component === 4) {
        if (that.component4RevisionDipa && that.component4RevisionDipa !== '0' && that.component4Realization && that.component4Realization !== '0') {
          sumValue = that.component4Realization / that.component4RevisionDipa
          percentage = sumValue.toFixed(2)
          percentage = percentage * 100
          percentage = Number(percentage.toFixed(2))
          that.component4Percentage = percentage
        } else {
          that.component4Percentage = 0
        }
      } else if (component === 5) {
        if (that.pureValueRevisionDipa && that.pureValueRevisionDipa !== '0' && that.pureValueRealization && that.pureValueRealization !== '0') {
          sumValue = that.pureValueRealization / that.pureValueRevisionDipa
          percentage = sumValue.toFixed(2)
          percentage = percentage * 100
          percentage = Number(percentage.toFixed(2))
          that.pureValuePercentage = percentage
        } else {
          that.pureValuePercentage = 0
        }
      }
    }, 500);
    this.totalAllTableInput()
  }


  totalAllTableInput() {
    let percentage
    let sumValue
    var that = this;
    this.totalValueFirstDipa = 0
    this.totalValueRevisionDipa = 0
    this.totalValueRealization = 0
    this.totalValueFirstDipa = Number(this.component1FirstDipa) + Number(this.component2FirstDipa) + Number(this.component3FirstDipa) + Number(this.component4FirstDipa) + Number(this.pureValueFirstDipa)
    this.totalValueRevisionDipa = Number(this.component1RevisionDipa) + Number(this.component2RevisionDipa) + Number(this.component3RevisionDipa) + Number(this.component4RevisionDipa) + Number(this.pureValueRevisionDipa)
    this.totalValueRealization = Number(this.component1Realization) + Number(this.component2Realization) + Number(this.component3Realization) + Number(this.component4Realization) + Number(this.pureValueRealization)
    clearTimeout(this.timeOutTotal);
    this.timeOutTotal = setTimeout(function () {
      if (that.totalValueRevisionDipa && that.totalValueRevisionDipa !== '0' && that.totalValueRealization && that.totalValueRealization !== '0') {
        sumValue = that.totalValueRealization / that.totalValueRevisionDipa
        percentage = Number(sumValue.toFixed(2))
        percentage = sumValue * 100
        percentage = Number(percentage.toFixed(2))
        that.totalValuePercentage = percentage
      } else {
        that.totalValuePercentage = 0
      }
    });
  }

  validationStep1() {
    let inputComponent1
    let inputComponent2
    let inputComponent3
    let inputComponent4
    let inputPure
    inputComponent1 = this.component1FirstDipa !== null && this.component1Realization !== null && this.component1RevisionDipa !== null;
    inputComponent2 = this.component2FirstDipa !== null && this.component2Realization !== null && this.component2RevisionDipa !== null;
    inputComponent3 = this.component3FirstDipa !== null && this.component3Realization !== null && this.component3RevisionDipa !== null;
    inputComponent4 = this.component4FirstDipa !== null && this.component4Realization !== null && this.component4RevisionDipa !== null;
    inputPure = this.pureValueFirstDipa !== null && this.pureValueRealization !== null && this.pureValueRevisionDipa !== null;
    return !inputComponent1 || !inputComponent2 || !inputComponent3 || !inputComponent4 || !inputPure
  }

  validationStep2() {
    let findPdoTargetEmpty
    let findIriTargetEmpty
    let existEmptyIriTarget = []
    findPdoTargetEmpty = this.pdoIriModel.filter(x => !x.target)
    if (findPdoTargetEmpty.length > 0) {
      return true
    } else {
      existEmptyIriTarget = []
      this.pdoIriModel.filter(iri => {
        findIriTargetEmpty = iri.iri.filter(x => !x.target)
        if (findIriTargetEmpty.length > 0) {
          existEmptyIriTarget.push(iri)
        }
      })
      return existEmptyIriTarget.length > 0;
    }
  }

  validateStep3() {
    return !this.obstacleComponent1 || !this.obstacleComponent2 || !this.obstacleComponent3 || !this.obstacleComponent4
  }

  validateStep4() {
    return !this.informationComponentContribution || !this.informationProgressProgram || !this.informationConstraint
        || !this.informationLearning || !this.informationWorkPlan || !this.informationActivityProposal
  }

  validateStep5() {
    return !this.componentFocus1 || !this.componentFocus2 || !this.componentFocus3 || !this.componentFocus4
  }

  openUploadPhotoInformation(information: number) {
    if (information === 1) {
      this.flagUploadInformationComponentContribution = !this.flagUploadInformationComponentContribution
    } else if (information === 2) {
      this.flagUploadInformationProgressProgram = !this.flagUploadInformationProgressProgram
    } else if (information === 3) {
      this.flagUploadInformationConstraint = !this.flagUploadInformationConstraint
    } else if (information === 4) {
      this.flagUploadInformationLearning = !this.flagUploadInformationLearning
    } else if (information === 5) {
      this.flagUploadInformationWorkPlan = !this.flagUploadInformationWorkPlan
    } else if (information === 6) {
      this.flagUploadInformationActivityProposal = !this.flagUploadInformationActivityProposal
    }
  }


  deleteInformationExist(data, information: number) {
    if (information === 1) {
      this.filesExistInformationComponentContribution.forEach((check, index) => {
        if (check.id === data?.id) {
          this.deletedDocumentsIds.push(data?.id)
          this.filesExistInformationComponentContribution.splice(index, 1)
        }
      })
    } else if (information === 2) {
      this.filesExistInformationProgressProgram.forEach((check, index) => {
        if (check.id === data?.id) {
          this.deletedDocumentsIds.push(data?.id)
          this.filesExistInformationProgressProgram.splice(index, 1)
        }
      })
    } else if (information === 3) {
      this.filesExistInformationConstraint.forEach((check, index) => {
        if (check.id === data?.id) {
          this.deletedDocumentsIds.push(data?.id)
          this.filesExistInformationConstraint.splice(index, 1)
        }
      })
    } else if (information === 4) {
      this.filesExistInformationLearning.forEach((check, index) => {
        if (check.id === data?.id) {
          this.deletedDocumentsIds.push(data?.id)
          this.filesExistInformationLearning.splice(index, 1)
        }
      })
    } else if (information === 5) {
      this.filesExistInformationWorkPlan.forEach((check, index) => {
        if (check.id === data?.id) {
          this.deletedDocumentsIds.push(data?.id)
          this.filesExistInformationWorkPlan.splice(index, 1)
        }
      })
    } else if (information === 6) {
      this.filesExistInformationActivityProposal.forEach((check, index) => {
        if (check.id === data?.id) {
          this.deletedDocumentsIds.push(data?.id)
          this.filesExistInformationActivityProposal.splice(index, 1)
        }
      })
    }
  }

  deletePhotoExisting(data, component: number) {
    if (component === 1) {
      this.filesExistComponent1.forEach((check, index) => {
        if (check.id === data.id) {
          this.deletedPhotoComponentIds.push(data.id)
          this.filesExistComponent1.splice(index, 1)
        }
      })
    } else if (component === 2) {
      this.filesExistComponent2.forEach((check, index) => {
        if (check.id === data.id) {
          this.deletedPhotoComponentIds.push(data.id)
          this.filesExistComponent2.splice(index, 1)
        }
      })
    } else if (component === 3) {
      this.filesExistComponent3.forEach((check, index) => {
        if (check.id === data.id) {
          this.deletedPhotoComponentIds.push(data.id)
          this.filesExistComponent3.splice(index, 1)
        }
      })
    } else if (component === 4) {
      this.filesExistComponent4.forEach((check, index) => {
        if (check.id === data.id) {
          this.deletedPhotoComponentIds.push(data.id)
          this.filesExistComponent4.splice(index, 1)
        }
      })
    }
  }

  getListAwpByYear(year) {
    this.startLoading()
    this.service.listAwpInformation(year).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.detailInformationAwp = resp.data
            this.component1FirstDipa = this.detailInformationAwp?.dipaAwalKomponen1
            this.component2FirstDipa = this.detailInformationAwp?.dipaAwalKomponen2
            this.component3FirstDipa = this.detailInformationAwp?.dipaAwalKomponen3
            this.component4FirstDipa = this.detailInformationAwp?.dipaAwalKomponen4
            this.component1RevisionDipa = this.detailInformationAwp?.dipaRevisiKomponen1
            this.component2RevisionDipa = this.detailInformationAwp?.dipaRevisiKomponen2
            this.component3RevisionDipa = this.detailInformationAwp?.dipaRevisiKomponen3
            this.component4RevisionDipa = this.detailInformationAwp?.dipaRevisiKomponen4
            this.component1Realization = this.detailInformationAwp?.dipaRealisasiKomponen1
            this.component2Realization = this.detailInformationAwp?.dipaRealisasiKomponen2
            this.component3Realization = this.detailInformationAwp?.dipaRealisasiKomponen3
            this.component4Realization = this.detailInformationAwp?.dipaRealisasiKomponen4
            this.pureValueFirstDipa = this.detailInformationAwp?.dipaAwalRupiahMurni
            this.pureValueRevisionDipa = this.detailInformationAwp?.dipaRevisiRupiahMurni
            this.pureValueRealization = this.detailInformationAwp?.dipaRealisasiRupiahMurni
            this.obstacleComponent1 = this.detailInformationAwp?.hambatanKomponen1
            this.obstacleComponent2 = this.detailInformationAwp?.hambatanKomponen2
            this.obstacleComponent3 = this.detailInformationAwp?.hambatanKomponen3
            this.obstacleComponent4 = this.detailInformationAwp?.hambatanKomponen4
            this.informationComponentContribution = this.detailInformationAwp?.keteranganKontribusiKomponent
            this.informationActivityProposal = this.detailInformationAwp?.keteranganUsulanKegiatan
            this.informationLearning = this.detailInformationAwp?.keteranganPembelajaran
            this.informationConstraint = this.detailInformationAwp?.keteranganKendala
            this.informationProgressProgram = this.detailInformationAwp?.keteranganKemajuanProgram
            this.informationWorkPlan = this.detailInformationAwp?.keteranganRencanaKerja
            this.componentFocus1 = this.detailInformationAwp?.fokusKomponen1
            this.componentFocus2 = this.detailInformationAwp?.fokusKomponen2
            this.componentFocus3 = this.detailInformationAwp?.fokusKomponen3
            this.componentFocus4 = this.detailInformationAwp?.fokusKomponen4
            this.detailInformationAwp?.pdo.forEach(x => {
              if (this.yearInformationAwp == 2020) {
                x.pdo.selectedYearInformation = x.pdo?.y_2020
                x.iri.forEach(y => {
                  y.iri.selectedYearInformation = y?.iri?.y_2020
                })
              } else if (this.yearInformationAwp == 2021) {
                x.pdo.selectedYearInformation = x.pdo?.y_2021
                x.iri.forEach(y => {
                  y.iri.selectedYearInformation = y?.iri?.y_2021
                })
              } else if (this.yearInformationAwp == 2022) {
                x.pdo.selectedYearInformation = x.pdo?.y_2022
                x.iri.forEach(y => {
                  y.iri.selectedYearInformation = y?.iri?.y_2022
                })
              } else if (this.yearInformationAwp == 2023) {
                x.pdo.selectedYearInformation = x.pdo?.y_2023
                x.iri.forEach(y => {
                  y.iri.selectedYearInformation = y?.iri?.y_2023
                })
              } else if (this.yearInformationAwp == 2024) {
                x.pdo.selectedYearInformation = x.pdo?.y_2024
                x.iri.forEach(y => {
                  y.iri.selectedYearInformation = y?.iri?.y_2024
                })
              }
            })
            this.pdoIriModel = JSON.parse(JSON.stringify(this.detailInformationAwp?.pdo))
            if (this.component1RevisionDipa && this.component1RevisionDipa !== '0' && this.component1Realization && this.component1Realization !== '0') {
              this.component1Percentage = this.component1Realization / this.component1RevisionDipa
              this.component1Percentage = Number(this.component1Percentage.toFixed(2))
              this.component1Percentage = this.component1Percentage * 100
              this.component1Percentage = Number(this.component1Percentage.toFixed(2))
            }
            if (this.component2RevisionDipa && this.component2RevisionDipa !== '0' && this.component2Realization && this.component2Realization !== '0') {
              this.component2Percentage = this.component2Realization / this.component2RevisionDipa
              this.component2Percentage = Number(this.component2Percentage.toFixed(2))
              this.component2Percentage = this.component2Percentage * 100
              this.component2Percentage = Number(this.component2Percentage.toFixed(2))
            }
            if (this.component3RevisionDipa && this.component3RevisionDipa !== '0' && this.component3Realization && this.component3Realization !== '0') {
              this.component3Percentage = this.component3Realization / this.component3RevisionDipa
              this.component3Percentage = Number(this.component3Percentage.toFixed(2))
              this.component3Percentage = this.component3Percentage * 100
              this.component3Percentage = Number(this.component3Percentage.toFixed(2))
            }
            if (this.component4RevisionDipa && this.component4RevisionDipa !== '0' && this.component4Realization && this.component4Realization !== '0') {
              this.component4Percentage = this.component4Realization / this.component4RevisionDipa
              this.component4Percentage = Number(this.component4Percentage.toFixed(2))
              this.component4Percentage = this.component4Percentage * 100
              this.component4Percentage = Number(this.component4Percentage.toFixed(2))
            }
            if (this.pureValueRevisionDipa && this.pureValueRevisionDipa !== '0' && this.pureValueRealization && this.pureValueRealization !== '0') {
              this.pureValuePercentage = this.pureValueRealization / this.pureValueRevisionDipa
              this.pureValuePercentage = Number(this.pureValuePercentage.toFixed(2))
              this.pureValuePercentage = this.pureValuePercentage * 100
              this.pureValuePercentage = Number(this.pureValuePercentage.toFixed(2))
            }

            if (this.detailInformationAwp?.files.length > 0) {
              this.detailInformationAwp?.files?.forEach(x => {
                if (x.component === '1') {
                  this.filesExistComponent1.push(x)
                } else if (x.component === '2') {
                  this.filesExistComponent2.push(x)
                } else if (x.component === '3') {
                  this.filesExistComponent3.push(x)
                } else if (x.component === '4') {
                  this.filesExistComponent4.push(x)
                }
              })
            }

            if (this.detailInformationAwp?.otherFiles?.length > 0) {
              this.detailInformationAwp?.otherFiles?.forEach(x => {
                if (x.key === 'keteranganKontribusiKomponent') {
                  this.filesExistInformationComponentContribution.push(x)
                } else if (x.key === 'keteranganKemajuanProgram') {
                  this.filesExistInformationProgressProgram.push(x)
                } else if (x.key === 'keteranganKendala') {
                  this.filesExistInformationConstraint.push(x)
                } else if (x.key === 'keteranganPembelajaran') {
                  this.filesExistInformationLearning.push(x)
                } else if (x.key === 'keteranganRencanaKerja') {
                  this.filesExistInformationWorkPlan.push(x)
                } else if (x.key === 'keteranganUsulanKegiatan') {
                  this.filesExistInformationActivityProposal.push(x)
                }
              })
            }
            this.totalAllTableInput()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  uploadDocumentContributionComponent(index) {
    this.fileUploadInformation = []
    this.loadingUploadData = true
    if (this.uploadFileInformationComponentContribution.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileInformationComponentContribution[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUploadInformation.push({
                fileId: resp.data[0].id,
                key: 'keteranganKontribusiKomponent'
              })
              index = index + 1
              if (index < this.uploadFileInformationComponentContribution.length) {
                this.uploadDocumentContributionComponent(index)
              } else {
                this.uploadDocumentProgressProgram(0)
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.uploadDocumentProgressProgram(0)
    }
  }

  uploadDocumentProgressProgram(index) {
    if (this.uploadFileInformationProgressProgram.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileInformationProgressProgram[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUploadInformation.push({
                fileId: resp.data[0].id,
                key: 'keteranganKemajuanProgram'
              })
              index = index + 1
              if (index < this.uploadFileInformationProgressProgram.length) {
                this.uploadDocumentProgressProgram(index)
              } else {
                this.uploadDocumentConstraint(0)
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.uploadDocumentConstraint(0)
    }
  }

  uploadDocumentConstraint(index) {
    if (this.uploadFileInformationConstraint.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileInformationConstraint[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUploadInformation.push({
                fileId: resp.data[0].id,
                key: 'keteranganKendala'
              })
              index = index + 1
              if (index < this.uploadFileInformationConstraint.length) {
                this.uploadDocumentConstraint(index)
              } else {
                this.uploadDocumentLearning(0)
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.uploadDocumentLearning(0)
    }
  }

  uploadDocumentLearning(index) {
    if (this.uploadFileInformationLearning.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileInformationLearning[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUploadInformation.push({
                fileId: resp.data[0].id,
                key: 'keteranganPembelajaran'
              })
              index = index + 1
              if (index < this.uploadFileInformationLearning.length) {
                this.uploadDocumentLearning(index)
              } else {
                this.uploadDocumentWorkPlan(0)
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.uploadDocumentWorkPlan(0)
    }
  }

  uploadDocumentWorkPlan(index) {
    if (this.uploadFileInformationWorkPlan.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileInformationWorkPlan[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUploadInformation.push({
                fileId: resp.data[0].id,
                key: 'keteranganRencanaKerja'
              })
              index = index + 1
              if (index < this.uploadFileInformationWorkPlan.length) {
                this.uploadDocumentWorkPlan(index)
              } else {
                this.uploadDocumentActivityProposal(0)
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.uploadDocumentActivityProposal(0)
    }
  }

  uploadDocumentActivityProposal(index) {
    if (this.uploadFileInformationActivityProposal.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileInformationActivityProposal[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUploadInformation.push({
                fileId: resp.data[0].id,
                key: 'keteranganUsulanKegiatan'
              })
              index = index + 1
              if (index < this.uploadFileInformationActivityProposal.length) {
                this.uploadDocumentActivityProposal(index)
              } else {
                this.uploadDocumentComponent1(0)
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.uploadDocumentComponent1(0)
    }
  }

  uploadDocumentComponent1(index) {
    this.fileUpload = []
    if (this.uploadFileComponent1.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileComponent1[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUpload.push({
                fileId: resp.data[0].id,
                component: '1'
              })
              index = index + 1
              if (index < this.uploadFileComponent1.length) {
                this.uploadDocumentComponent1(index)
              } else {
                this.uploadDocumentComponent2(0)
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.uploadDocumentComponent2(0)
    }
  }

  uploadDocumentComponent2(index) {
    if (this.uploadFileComponent2.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileComponent2[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUpload.push({
                fileId: resp.data[0].id,
                component: '2'
              })
              index = index + 1
              if (index < this.uploadFileComponent2.length) {
                this.uploadDocumentComponent2(index)
              } else {
                this.uploadDocumentComponent3(0)
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.uploadDocumentComponent3(0)
    }
  }

  uploadDocumentComponent3(index) {
    if (this.uploadFileComponent3.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileComponent3[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUpload.push({
                fileId: resp.data[0].id,
                component: '3'
              })
              index = index + 1
              if (index < this.uploadFileComponent3.length) {
                this.uploadDocumentComponent3(index)
              } else {
                this.uploadDocumentComponent4(0)
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.uploadDocumentComponent4(0)
    }
  }

  uploadDocumentComponent4(index) {
    if (this.uploadFileComponent4.length > 0) {
      this.activityService.uploadInformationAwp(this.uploadFileComponent4[index]).subscribe(
          resp => {
            if (resp.success) {
              this.fileUpload.push({
                fileId: resp.data[0].id,
                component: '4'
              })
              index = index + 1
              if (index < this.uploadFileComponent4.length) {
                this.uploadDocumentComponent4(index)
              } else {
                this.updateDataInformationAwp()
              }
            }
          },error => {
            this.loadingUploadData = false
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.updateDataInformationAwp()
    }
  }

  updateDataInformationAwp() {
    let pdoToApi = []
    let dataIri = []

    this.pdoIriModel.forEach(pdo => {
      dataIri = []
      pdo?.iri?.forEach(iri => {
        dataIri.push({
          iriId: iri?.iri?.id,
          target: iri?.target
        })
      })
      pdoToApi.push({
        pdoId: pdo?.pdo?.id,
        target: pdo?.target,
        iri: dataIri
      })
    })

    let params = {
      year: this.yearInformationAwp,
      keteranganKontribusiKomponent: this.informationComponentContribution,
      keteranganKemajuanProgram: this.informationProgressProgram,
      keteranganKendala: this.informationConstraint,
      keteranganPembelajaran: this.informationLearning,
      keteranganRencanaKerja: this.informationWorkPlan,
      keteranganUsulanKegiatan: this.informationActivityProposal,
      dipaAwalKomponen1: Number(this.component1FirstDipa),
      dipaAwalKomponen2: Number(this.component2FirstDipa),
      dipaAwalKomponen3: Number(this.component3FirstDipa),
      dipaAwalKomponen4: Number(this.component4FirstDipa),
      dipaRevisiKomponen1: Number(this.component1RevisionDipa),
      dipaRevisiKomponen2: Number(this.component2RevisionDipa),
      dipaRevisiKomponen3: Number(this.component3RevisionDipa),
      dipaRevisiKomponen4: Number(this.component4RevisionDipa),
      dipaRealisasiKomponen1: Number(this.component1Realization),
      dipaRealisasiKomponen2: Number(this.component2Realization),
      dipaRealisasiKomponen3: Number(this.component3Realization),
      dipaRealisasiKomponen4: Number(this.component4Realization),
      dipaAwalRupiahMurni: Number(this.pureValueFirstDipa),
      dipaRevisiRupiahMurni: Number(this.pureValueRevisionDipa),
      dipaRealisasiRupiahMurni: Number(this.pureValueRealization),
      hambatanKomponen1: this.obstacleComponent1,
      hambatanKomponen2: this.obstacleComponent2,
      hambatanKomponen3: this.obstacleComponent2,
      hambatanKomponen4: this.obstacleComponent3,
      fokusKomponen1: this.componentFocus1,
      fokusKomponen2: this.componentFocus2,
      fokusKomponen3: this.componentFocus3,
      fokusKomponen4: this.componentFocus4,
      pdo: pdoToApi,
      files: this.fileUpload,
      deletedFilesIds: this.deletedPhotoComponentIds,
      otherFiles: this.fileUploadInformation,
      deletedOtherFilesIds: this.deletedDocumentsIds
    }
    this.service.editInformationAwp(this.yearInformationAwp, params).subscribe(
        resp => {
          if (resp.success) {
            this.downloadWordAwp(this.detailYear, `Final AWP ${this.detailYear}`)
          }
        }, error => {
          this.loadingUploadData = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  downloadWordAwp(id, name) {
    this.service.exportWordAwp(id, name).subscribe(
        resp => {
          this.goToListAWP()
          this.loadingUploadData = false
        }, error => {
          this.loadingUploadData = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }




}
