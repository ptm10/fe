import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AWPComponent } from './awp.component';

describe('AWPComponent', () => {
  let component: AWPComponent;
  let fixture: ComponentFixture<AWPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AWPComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AWPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
