import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAwpComponent } from './edit-awp.component';

describe('EditAwpComponent', () => {
  let component: EditAwpComponent;
  let fixture: ComponentFixture<EditAwpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditAwpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAwpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
