import {Component, OnInit} from '@angular/core';
import {AwpModel, Event, EventDocumentType, MangementType, Province} from "../../../../core/models/awp.model";
import {ActivatedRoute, Router} from "@angular/router";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import Swal from "sweetalert2";
import {parse} from "date-fns";
import {ComponentModel} from "../../../../core/models/componentModel";
import {ComponentService} from "../../../../core/services/Component/component.service";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {PDOModel} from "../../../../core/models/PDO.model";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-edit-awp',
  templateUrl: './edit-awp.component.html',
  styleUrls: ['./edit-awp.component.scss']
})
export class EditAwpComponent implements OnInit {

  public Description = ClassicEditor;
  public Purpose = ClassicEditor;
  public OutputEvent = ClassicEditor;
  public OtherInfoAwp = ClassicEditor;
  public narasumber = ClassicEditor;
  public riskMitigation = ClassicEditor;
  public riskPotential = ClassicEditor;
  public participantTarget = ClassicEditor;
  dataIdRab: string = null
  detailIdAwp: string
  dataDetailAwp: AwpModel
  breadCrumbItems: Array<{}>;
  finishLoadData = true
  finishLoadDataCount = 0
  listManagementType: MangementType[]
  selectedValueEventType: any
  loadingUpdateAwp = false
  namakegiatan: string
  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  codeEvent: any
  // data edit
  date: Date
  editNamaKegiatan: string
  editBudget: number
  editTujuanKegiatan: string
  editDeskripsiKegiatan: string
  editWaktuInformasiLainnya: string
  editjumlahNasrasumber: number
  editAsalLembagaNarasumber: string
  editInformasiLainNarasumber: string
  editTotalPeserta: number
  editInformasiPeserta: string
  editOutputKegiatan: string
  editVolumeEvent: number
  otherInformationAwp: string
  tempatInformasiLainnya: string
  minDateYear: any
  maxDateYear: any
  potentialRisk: string = null
  mitigationRisk: string = null
  targetParticipant: string = null
  inputCodeManual: string
  inputCodeManualKegiatan: string

  //componen
  selectedComponentExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]
  selectedModaActivity: any

  //Moda Kegiatan
  listModaType: MangementType[]
  listModaTypeMaster: MangementType[]

  // sub componen
  selectedSubComponentExist: ComponentModel
  selectedSubComponentExistMaster: ComponentModel
  selectedSubComponentExistEdit: ComponentModel

  //sub-sub componen
  selectedSubSubComponentExist: ComponentModel
  selectedSubSubComponentExistMaster: ComponentModel
  selectedSubSubComponentExistEdit: ComponentModel
  codeSelected: any = 'pilih'
  listCodeSelected = []


  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // event
  listEvent: Event[]
  listEventMaster: Event[]
  selectedEvent: Event

  // PDO
  listPdoAll: PDOModel[]
  listPdoAllMaster: PDOModel[]
  pdoRequest = []
  addPdo = []
  removePdo = []
  searchPdo = []
  pdoExisting = []

  // IRI
  listIriAll: PDOModel[]
  listIriAllMaster: PDOModel[]
  iriRequest = []
  addIri = []
  removeIri = []
  searchIri = []
  iriExisting = []

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []
  listCityFromApi: string[];
  selectedCityFromList = []

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province
  provinceAndRegenciesExist = []

  //code
  selectedCode: any = null
  selectedCodeExisting: any
  listCode: any
  listCodeExisting: any
  loadingListComponent = false

  //code sub sub component
  selectedCodeSubSubComponent: any = null
  selectedCodeSubSubComponentExisting: any
  listCodeSelectedSubSubComponent: any
  listCodeSelectedSubSubComponentExisting: any
  listCodeSelectedSubSub = []
  subSubComponentName: string

  // rab
  uploadFileRab: Array<File> = [];
  uploadFileRabIds: Array<File> = [];

  selectedManualCodeExist = false
  selectedManualCodeKegiatanExist = false

  // list keterangan risiko
  listAllRisk: string[] = []
  selectedRisk: string
  dataListRisk: EventDocumentType[]
  otherRisk: string
  kemungkinanTerjadi: number;
  dampak: number;
  gejalaResiko: string

  constructor(
      private router:ActivatedRoute,
      private route: Router,
      private service: AwpService,
      private serviceComponent: ComponentService,
      public formatter: NgbDateParserFormatter,
      private serviceAdministration: AdministrationService,
      private calendar: NgbCalendar,
      private modalService: NgbModal,
      private serviceKegiatan: KegiatanService,
      private swalAlert: SwalAlert
  ) {}

  ngOnInit(): void {
    this.minDateYear = new Date(2020, 1, 1)
    this.maxDateYear = new Date(2024, 11, 30)
    this.breadCrumbItems = [{ label: 'AWP' }, { label: 'Detail AWP' }, { label: 'Edit AWP', active: true }]
    this.detailIdAwp = this.router.snapshot.paramMap.get('id')
    this.getDetailAwp()
    this.getListHolidayManagement()
    this.managementType()
    this.listProvince()
    this.listAllModa()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  getDetailAwp() {
    this.startLoading()
    this.service.detailAwp(this.detailIdAwp).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataDetailAwp = resp.data
            this.editNamaKegiatan = this.dataDetailAwp.name
            this.editBudget = this.dataDetailAwp.budgetAwp
            this.editTujuanKegiatan = this.dataDetailAwp.purpose
            this.editDeskripsiKegiatan = this.dataDetailAwp.description
            this.selectedValueEventType = this.dataDetailAwp.eventManagementType
            this.editjumlahNasrasumber = this.dataDetailAwp.nsCount
            this.editAsalLembagaNarasumber = this.dataDetailAwp.nsInstitution
            this.editInformasiLainNarasumber = this.dataDetailAwp.nsOtherInformation
            this.editTotalPeserta = this.dataDetailAwp.participantCount
            this.editInformasiPeserta = this.dataDetailAwp.participantOtherInformation
            this.editOutputKegiatan = this.dataDetailAwp.eventOutput
            this.editVolumeEvent = this.dataDetailAwp?.eventVolume
            this.namakegiatan = this.dataDetailAwp?.name
            this.codeEvent = this.dataDetailAwp?.code
            this.date = parse(this.dataDetailAwp.year.toString(), 'yyyy', new Date());
            this.selectedComponentExist = this.dataDetailAwp?.component
            this.dataDetailAwp.convertStartDate = new Date(this.dataDetailAwp.startDate)
            this.editWaktuInformasiLainnya = this.dataDetailAwp?.eventOtherInformation
            this.dataDetailAwp.convertEndDate = new Date(this.dataDetailAwp.endDate)
            this.fromDate = new NgbDate(this.dataDetailAwp.convertStartDate.getFullYear(), this.dataDetailAwp.convertStartDate.getMonth() + 1, this.dataDetailAwp.convertStartDate.getDate())
            this.toDate = new NgbDate(this.dataDetailAwp.convertEndDate.getFullYear(), this.dataDetailAwp.convertEndDate.getMonth() + 1, this.dataDetailAwp.convertEndDate.getDate())
            this.selectedEvent = this.dataDetailAwp?.eventType
            this.otherInformationAwp = this.dataDetailAwp?.nameOtherInformation
            this.tempatInformasiLainnya = this.dataDetailAwp?.locationOtherInformation
            if (this.dataDetailAwp?.subSubComponent) {
              this.subSubComponentName = this.dataDetailAwp?.subSubComponentName
            }
            this.dataDetailAwp.pdo.forEach(x => {
              this.pdoExisting.push(x)
            })
            this.dataDetailAwp.iri.forEach(x => {
              this.iriExisting.push(x)
            })
            this.dataDetailAwp.provincesRegencies.forEach(x => {
              this.provinceAndRegenciesExist.push(x)
            })
            this.selectedModaActivity = this.dataDetailAwp?.activityMode
            this.potentialRisk = this.dataDetailAwp?.descriptionRisk
            this.mitigationRisk = this.dataDetailAwp?.mitigationRisk
            this.targetParticipant = this.dataDetailAwp?.participantTarget
            this.selectedRisk = this.dataDetailAwp?.risk
            this.kemungkinanTerjadi = this.dataDetailAwp?.potentialRisk
            this.dampak = this.dataDetailAwp?.impactRisk
            this.gejalaResiko = this.dataDetailAwp?.symptomsRisk
            this.dataEvent()
            this.getListComponent()
            this.dataPDO()
            this.dataIri()
            this.getListRisk()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  withoutDots(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode == 190 || charCode == 110 || charCode == 188);
  }

  validateNextButtonForSubComponent() {
    if (this.selectedCodeSubSubComponent) {
      if (this.selectedCodeSubSubComponent?.id === '1') {
        if (this.subSubComponentName) {
          return false
        } else {
          return true
        }
      } else {
        if (this.subSubComponentName) {
          return false
        } else {
          return true
        }
      }
    } else {
      return false
    }
  }

  validateButtonKegiatan() {
    if (this.selectedSubComponentExist) {
      if (this.selectedCodeSubSubComponent) {
        if (this.selectedCodeSubSubComponent?.id === '1') {
          if (this.inputCodeManual) {
            if (this.selectedManualCodeExist) {
              return true
            } else {
              return false
            }
          } else {
            return true
          }
        } else {
          return false
        }
      }
    } else {
      return true
    }
  }

  validateName() {
    if (this.selectedCode) {
      if (this.selectedCode?.id === '2') {
        if (this.inputCodeManualKegiatan) {
          if (this.selectedManualCodeKegiatanExist) {
            return true
          } else {
            return false
          }
        } else {
          return true
        }
      } else {
        return false
      }
    } else {
      return true
    }
  }

  validateNameSubSubComponent() {
    if (this.selectedSubComponentExist) {
      if (this.selectedCodeSubSubComponent) {
        if (this.selectedCodeSubSubComponent?.id === '1') {
          if (this.inputCodeManual) {
            if (this.selectedManualCodeExist) {
              return true
            } else {
              return false
            }
          } else {
            return true
          }
        } else {
          return false
        }
      } else {
        return true
      }
    } else {
      return true
    }
  }

  checkTotalNarasumber() {
    if (this.editjumlahNasrasumber < 0 || !this.editjumlahNasrasumber) {
      this.editjumlahNasrasumber = 0
    }
  }

  checkTotalParticipant() {
    if (this.editTotalPeserta < 0 || !this.editTotalPeserta) {
      this.editTotalPeserta = 0
    }
  }

  resetTimeRangeEvent() {
    // this.fromDate = null
    // this.toDate = null
    this.minDate = {
      year: this.date.getFullYear(),
      month: 1,
      day: 1
    };
    this.maxDate = {
      year: this.date.getFullYear(),
      month: 12,
      day: 31
    };
    this.removeSelectedComponent()
    this.getListComponent()
  }

  setTimeRangeEvent() {
    const current = new Date();
    this.minDate = {
      year: this.date.getFullYear(),
      month: 1,
      day: 1
    };
    this.maxDate = {
      year: this.date.getFullYear(),
      month: 12,
      day: 31
    };
  }



  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  goToListAWP() {
    this.route.navigate(['admin/detail-awp/' + this.detailIdAwp])
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  // calender
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  // componen

  /** * list component & selected component */
  getListComponent() {
    this.loadingListComponent = true
    this.serviceComponent.listComponent(this.date.getFullYear()).subscribe(
        resp => {
          this.loadingListComponent = false
          if (resp.success) {
            this.component = resp.data
            this.componentMaster = resp.data
            let componentMasterEdit =JSON.parse(JSON.stringify(this.componentMaster))
            this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)

            let dataSubComponent = componentMasterEdit.find(dataFilter => dataFilter.id == this.selectedComponentExist.id)
            this.selectedSubComponentExistMaster = JSON.parse(JSON.stringify(dataSubComponent))
            this.selectedSubComponentExistEdit = JSON.parse(JSON.stringify(dataSubComponent))

            if (this.dataDetailAwp?.subComponent) {
              this.selectedSubComponentExist = dataSubComponent.subComponent.find(data => data.id == this.dataDetailAwp?.subComponent.id)
              this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
            }
            this.selectedSubSubComponentExistMaster = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
            this.selectedSubSubComponentExistEdit = JSON.parse(JSON.stringify(this.selectedSubComponentExist))

            if (this.dataDetailAwp?.subSubComponent) {
              let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
              selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen', '')
              selectedSubSubComponent.code = selectedSubSubComponent.code + '._'
              let codeManualInput = {
                id: '1',
                code: selectedSubSubComponent?.code,
                subComponent: []
              }
              this.selectedCodeSubSubComponent = this.selectedSubComponentExist.subComponent.find(data => data.id == this.dataDetailAwp?.subSubComponent.id)
              this.listCodeSelectedSubSubComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedCodeSubSubComponent.id)
              this.listCodeSelectedSubSubComponentExisting = this.selectedSubSubComponentExistMaster.subComponent

              this.selectedSubSubComponentExist = this.selectedSubComponentExist.subComponent.find(data => data.id == this.dataDetailAwp?.subSubComponent.id)
              this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubSubComponentExist.id)
              this.listCodeSelectedSubSubComponent.push(codeManualInput)
              this.listCodeSelectedSubSubComponentExisting.push(codeManualInput)
            } else {
              let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
              selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen', '')
              selectedSubSubComponent.code = selectedSubSubComponent.code + '._'
              let codeManualInput = {
                id: '1',
                code: selectedSubSubComponent?.code,
                subComponent: []
              }
              this.listCodeSelectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubSubComponentExistMaster?.subComponent))
              this.listCodeSelectedSubSubComponentExisting = JSON.parse(JSON.stringify(this.selectedSubSubComponentExistMaster?.subComponent))
              this.listCodeSelectedSubSubComponent.push(codeManualInput)
              this.listCodeSelectedSubSubComponentExisting.push(codeManualInput)

            }
            this.codeSelectedFromComponent()
          } else {
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.loadingListComponent = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedComponent(data) {
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null
    this.component = this.componentMaster

    this.selectedComponentExist = data
    this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)
    this.selectedSubComponentExistMaster = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExistEdit = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
    this.codeSelectedFromSubSubComponent()
  }

  searchFilterComponent(e) {
    const searchStr = e.target.value
    let filter = this.componentMaster
    if (this.selectedComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
    } else {
      filter = this.componentMaster
    }

    this.component = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedComponent() {
    this.selectedCode = null
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.component = this.componentMaster
    this.selectedComponentExist = null

    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null

    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.codeSelectedFromComponent()
    this.clearSelectedCodeSubSubComponent()
    this.inputCodeManual = ''
  }

  // sub componen
  selectedSubComponent(data) {
    this.selectedCode = null
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent

    this.selectedSubComponentExist = data
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
    this.selectedSubSubComponentExistMaster = Object.assign({}, this.selectedSubComponentExist)
    this.selectedSubSubComponentExistEdit = Object.assign({}, this.selectedSubComponentExist)

    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
    this.codeSelectedFromSubSubComponent()
    this.validateName()
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
  }

  searchFilterSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubComponentExistEdit.subComponent
    if (this.selectedSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubComponentExist.id)
    } else {
      filter = this.selectedSubComponentExistEdit.subComponent
    }

    this.selectedSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedSubComponent() {
    this.selectedCode = null
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
    this.clearSelectedCodeSubSubComponent()
    this.inputCodeManual = ''
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
    // this.codeSelectedFromSubSubComponent()
  }


  // sub sub componen
  selectedSubSubComponent(data) {
    this.selectedCode = null
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent

    this.selectedSubSubComponentExist = data
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubSubComponentExist.id)
    this.codeSelectedFromComponent()
    this.validateName()
    this.validateButtonKegiatan()
  }

  searchFilterSubSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubSubComponentExistEdit.subComponent
    if (this.selectedSubSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubSubComponentExist.id)
    } else {
      filter = this.selectedSubSubComponentExistEdit.subComponent
    }

    this.selectedSubSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedSubSubComponent() {
    this.selectedCode = null
    this.codeSelected = null
    this.editNamaKegiatan = null
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent
    this.codeSelectedFromComponent()
    this.inputCodeManual = ''
    this.validateName()
  }

  changeManualCodeSubSubComponent() {
    this.selectedManualCodeExist = false
    let filterInputCode
    let selectedInputCode
    let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))

    selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
    selectedInputCode = selectedSubSubComponent.code + '.' + this.inputCodeManual

    selectedSubSubComponent.subComponent.forEach(x => {
      x.code = x.code.replace('Komponen ', '')
    })
    filterInputCode = selectedSubSubComponent?.subComponent.filter(x => x.code === selectedInputCode)
    if (filterInputCode.length > 0) {
      this.selectedManualCodeExist = true
    }
    this.selectedCode = null
    this.editNamaKegiatan = ''
    this.codeSelectedFromComponent()
    this.validateName()
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
  }

  changeManualCodekegiatan() {
    this.selectedManualCodeKegiatanExist = false
    let filterInputCode
    let selectedInputCode
    let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
    if (this.selectedCodeSubSubComponent) {
      let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedCodeSubSubComponent))
      selectedSubSubComponent.code = selectedSubSubComponent.code.replace(' ', '')
      selectedInputCode = selectedSubSubComponent.code + '.' + this.inputCodeManualKegiatan
      filterInputCode = selectedSubSubComponent.subComponent.filter(x => x.code === selectedInputCode)
      if (filterInputCode.length > 0) {
        this.selectedManualCodeKegiatanExist = true
      }
    } else if (this.selectedSubComponentExist){
      selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
      selectedInputCode = selectedSubSubComponent.code + '.' + this.inputCodeManualKegiatan

      selectedSubSubComponent.subComponent.forEach(x => {
        x.code = x.code.replace('Komponen ', '')
      })
      filterInputCode = selectedSubSubComponent?.subComponent.filter(x => x.code === selectedInputCode)
      if (filterInputCode.length > 0) {
        this.selectedManualCodeKegiatanExist = true
      }
    }
  }

  codeSelectedFromComponent() {
    let selectedCodeComponent
    let filterComponent
    this.codeSelected = 'pilih'
    this.listCodeSelected = []
    this.listCodeExisting = []
    this.listCodeSelected = []
    let selectedSubSubComponent
    let codeManualInput
    if (this.selectedCodeSubSubComponent) {
      selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedCodeSubSubComponent))
      selectedSubSubComponent.code = selectedSubSubComponent?.code.replace('_', '')
      if (this.inputCodeManual) {
        selectedSubSubComponent.code = selectedSubSubComponent?.code + this.inputCodeManual + '._'
      } else {
        selectedSubSubComponent.code = selectedSubSubComponent.code + '._'
      }
      codeManualInput = {
        id: '2',
        code: selectedSubSubComponent?.code,
        subComponent: []
      }
    } else if (this.selectedSubComponentExist){
      let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
      selectedSubSubComponent.code = selectedSubSubComponent.code + '._'
      codeManualInput = {
        id: '2',
        code: selectedSubSubComponent?.code,
        subComponent: []
      }
    }
    if (this.selectedCodeSubSubComponent) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedCodeSubSubComponent))
      selectedCodeComponent.subComponent.forEach(data => {
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      this.listCodeSelected.push(codeManualInput)
    } else if (this.selectedSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      console.log(selectedCodeComponent)
      selectedCodeComponent.subComponent.forEach(data => {
        this.listCodeSelected.push(data)
      })
      this.listCodeSelected.push(codeManualInput)
    } else if (this.selectedComponentExist) {
      filterComponent = this.componentMaster.find(data => data.id == this.selectedComponentExist.id)
      selectedCodeComponent = JSON.parse(JSON.stringify(filterComponent))
      selectedCodeComponent.subComponent.forEach(data => {
        this.listCodeSelected.push(data)
      })
    }
    this.listCodeExisting = JSON.parse(JSON.stringify(this.listCodeSelected))
    this.listCodeSelected = JSON.parse(JSON.stringify(this.listCodeSelected))

    const x = this.listCodeSelected.filter(y => y.code === this.dataDetailAwp?.componentCode?.code)
    if (x.length > 0) {
      this.selectedCode = this.dataDetailAwp?.componentCode
      this.listCodeSelected = this.listCodeSelected.filter(data => data.id !== this.selectedCode.id)
      this.editNamaKegiatan = this.namakegiatan
    } else {
      this.selectedCode = null
      this.listCodeSelected = this.listCodeExisting
    }
  }

  changeValueSelected() {
    if (this.codeSelected === 'pilih') {
      this.editNamaKegiatan = ''
    } else {
      let x = this.listCodeSelected.find(y => y.code === this.codeSelected)
      this.editNamaKegiatan = x.description
    }
  }

  selectedCodeFromList(data) {
    this.selectedCode = null
    this.listCodeSelected = this.listCodeExisting

    this.selectedCode = data
    this.listCodeSelected = this.listCodeSelected.filter(data => data.id !== this.selectedCode.id)
    if (this.selectedCode.code === this.dataDetailAwp?.componentCode.code) {
      this.editNamaKegiatan = this.namakegiatan
    } else {
      this.editNamaKegiatan = this.selectedCode.description
    }
  }

  clearSelectedCode() {
    this.selectedCode = null
    this.listCodeSelected = this.listCodeExisting
    this.editNamaKegiatan = ''
    this.inputCodeManualKegiatan = ''
  }

  /** * selected code sub sub component */

  codeSelectedFromSubSubComponent() {
    let codeManualInput
    if (this.selectedSubComponentExist) {
      let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
      selectedSubSubComponent.code = selectedSubSubComponent.code + '._'
      codeManualInput = {
        id: '1',
        code: selectedSubSubComponent?.code,
        subComponent: []
      }
    }
    let selectedCodeComponent
    this.codeSelected = 'pilih'
    this.listCodeSelectedSubSub = []
    this.selectedCodeSubSubComponentExisting = []
    this.listCodeSelectedSubSubComponent = []
    console.log(this.selectedSubComponentExist)
    if (this.selectedSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      selectedCodeComponent.subComponent.forEach(data => {
        data.code = data.code.replace('Komponen', '')
        this.listCodeSelectedSubSub.push(data)
      })
      this.listCodeSelectedSubSub.push(codeManualInput)
    }
    this.listCodeSelectedSubSubComponent = JSON.parse(JSON.stringify(this.listCodeSelectedSubSub))
    this.listCodeSelectedSubSubComponentExisting = JSON.parse(JSON.stringify(this.listCodeSelectedSubSub))
    console.log(this.listCodeSelectedSubSub)
    this.codeSelectedFromComponent()
  }

  selectedCodeFromListSubSubComponent(data) {
    this.selectedCodeSubSubComponent = null
    this.listCodeSelectedSubSubComponent = this.listCodeSelectedSubSubComponentExisting

    this.selectedCodeSubSubComponent = data
    this.listCodeSelectedSubSubComponent = this.listCodeSelectedSubSubComponent.filter(data => data.id !== this.selectedCodeSubSubComponent.id)
    this.subSubComponentName = this.selectedCodeSubSubComponent.description
    this.clearSelectedCode()
    this.codeSelectedFromComponent()
    this.validateName()
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
  }

  clearSelectedCodeSubSubComponent() {
    this.selectedCodeSubSubComponent = null
    this.listCodeSelectedSubSubComponent = this.listCodeSelectedSubSubComponentExisting
    this.subSubComponentName = ''
    this.codeSelectedFromComponent()
    this.clearSelectedCode()
    this.inputCodeManual = ''
    this.validateName()
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
  }


  changeValueSelectedSubSubComponent() {
    if (this.codeSelected === 'pilih') {
      this.subSubComponentName = ''
    } else {
      let x = this.listCodeSelected.find(y => y.code === this.codeSelected)
      this.subSubComponentName = x.description
    }
  }

  testing() {
    console.log(this.selectedSubComponentExistMaster)
    console.log(this.selectedSubComponentExistEdit)
  }


  /** * end of sub sub component */

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  /** * end date */

  /** * list holidays */
  getListHolidayManagement() {
    this.listHolidays = []
    this.serviceAdministration.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * end list holidays */

  /** * Management Type */

  managementType() {
    this.startLoading()
    this.service.eventManagementType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listManagementType = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  /** * end Management Type */


  /** * List Event */
  dataEvent() {
    this.startLoading()
    this.service.eventType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEvent = resp.data.content
            this.listEventMaster = resp.data.content
            this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent.id)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedEventType(data) {
    this.selectedEvent = null
    this.listEvent = this.listEventMaster

    this.selectedEvent = data
    this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent.id)
  }

  searchFilterEvent(e) {
    const searchStr = e.target.value
    let filter = this.listEventMaster
    if (this.selectedEvent) {
      filter = filter.filter(x => x.id !== this.selectedEvent.id)
    } else {
      filter = this.listEventMaster
    }

    this.listEvent = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List PDO */

  dataPDO() {
    this.service.pdo().subscribe(
        resp => {
          if (resp.success) {
            this.listPdoAll = resp.data.content
            this.listPdoAllMaster = resp.data.content
            this.searchPdo = resp.data.content
            if (this.pdoExisting.length > 0) {
              this.pdoExisting.forEach(dataPdoExist => {
                this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
                this.searchPdo = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
              })
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addPdoSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pdoRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePdo.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePdo.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPdo.push(data)
        }
        this.pdoRequest.push(data)
      }
    } else {
      this.pdoRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pdoRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPdo.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPdo.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePdo.push(data)
      }
    }
    this.listPdoAll = this.listPdoAllMaster

    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(dataPdoExist => {
        this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
      })
    }

    this.pdoRequest.forEach(dataProvince => {
      this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPdo = this.listPdoAll
  }

  searchFilterPdo(e) {
    const searchStr = e.target.value
    this.listPdoAll = this.searchPdo.filter((product) => {
      return (product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.description.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  removePdoExisting(data) {
    this.pdoExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.pdoExisting.splice(index, 1)
      }
    })
    this.listPdoAll = this.listPdoAllMaster
    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(dataPdoExist => {
        this.listPdoAll = this.listPdoAll.filter(x => x.id !== dataPdoExist?.pdo?.id)
      })
      this.pdoRequest.forEach(dataProvince => {
        this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    } else {
      this.pdoRequest.forEach(dataProvince => {
        this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchPdo = this.listPdoAll
  }

  /** * List IRI */
  dataIri() {
    this.startLoading()
    this.service.iri().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listIriAll = resp.data.content
            this.listIriAllMaster = resp.data.content
            this.searchIri = resp.data.content
            if (this.iriExisting.length > 0) {
              this.iriExisting.forEach(dataIriExist => {
                this.listIriAll = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
                this.searchIri = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
              })
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addIriSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.iriRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removeIri.forEach((check, index) => {
          if (check.id === data.id) {
            this.removeIri.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addIri.push(data)
        }
        this.iriRequest.push(data)
      }
    } else {
      this.iriRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.iriRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addIri.forEach((check, index) => {
        if (check.id === data.id) {
          this.addIri.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removeIri.push(data)
      }
    }
    this.listIriAll = this.listIriAllMaster

    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(dataIriExist => {
        this.listIriAll = this.listIriAll.filter(x => x.id !== dataIriExist?.iri?.id)
      })
    }

    this.iriRequest.forEach(dataProvince => {
      this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })
    this.searchIri = this.listIriAll
  }

  searchFilterIri(e) {
    const searchStr = e.target.value
    this.listIriAll = this.searchIri.filter((product) => {
      return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
    });
  }

  removeIriExisting(data) {
    this.iriExisting.forEach((check, index) => {
      if (data.id === check.id) {
        this.iriExisting.splice(index, 1)
      }
    })
    this.listIriAll = this.listIriAllMaster
    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(dataPdoExist => {
        this.listIriAll = this.listIriAll.filter(x => x.id !== dataPdoExist?.iri?.id)
      })
      this.iriRequest.forEach(dataProvince => {
        this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    } else {
      this.iriRequest.forEach(dataProvince => {
        this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
      })
    }
    this.searchIri = this.listIriAll
  }

  /** * List Province */

  listProvince() {
    this.startLoading()
    this.service.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            // this.searchProvince = this.listProvinceAll
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedCityFromList = []
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster

    this.selectedProvinceExist = data
    this.listProvinceAll = this.listProvinceAll.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    this.listProvinceAll = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List City */

  listCity() {
    let cityNameFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.service.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            this.listCityAll.forEach(x => {
              cityNameFromApi.push(x.name)
            })

            if (this.selectedProvinceAndCity.length > 0) {
              this.selectedProvinceAndCity.forEach(x => {
                if (x.regencyId.length > 0) {
                  x.regencyId.forEach(y => {
                    cityNameFromApi = cityNameFromApi.filter(exist => exist !== y)
                  })
                }
              })
            }

            if (this.provinceAndRegenciesExist.length > 0) {
              this.provinceAndRegenciesExist.forEach(x => {
                if (x.regencies.length > 0) {
                  x.regencies.forEach(dataRegencie => {
                    cityNameFromApi = cityNameFromApi.filter(y => dataRegencie.regency.name !== y)
                  })
                }
              })
            }
            this.listCityFromApi = cityNameFromApi
            this.listCityAllMaster = resp.data.content
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAll = this.listCityAllMaster

    this.selectedCityExist = data
    this.listCityAll = this.listCityAll.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    if (this.selectedProvinceAndCity.length > 0) {
      this.selectedProvinceAndCity.forEach(x => {
        if (x.regencyId != null) {
          this.listCityAll = this.listCityAll.filter(y => y.id !== x.regencyId.id)
        }
      })
    }
  }

  addSelectedProvinceAndCity() {
    this.selectedCityFromList.forEach(x => {
      let y = this.listCityAll.find(dataSelected => dataSelected.name === x)
      if (y) {
        this.dataAllCitySelected.push(y)
      }
    })
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityFromList
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.listCityFromApi = []
  }

  removeProvinceRegenciesExist(data) {
    this.provinceAndRegenciesExist.forEach((check, index ) => {
      if (check.id === data.id) {
        this.provinceAndRegenciesExist.splice(index, 1)
      }
    })
    this.listCityFromApi = []
  }

  // new tag
  CreateNew(Position){
    return Position
  }

  // get list risk
  getListRisk() {
    let tag = []
    this.serviceKegiatan.listRiskInRik().subscribe(
        resp => {
          if (resp.success) {
            this.dataListRisk = resp.data.content
            this.dataListRisk.forEach(x => {
              tag.push(x.name)
            })
            this.listAllRisk = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End list Risk */

  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.service.listModa().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */

  /** Upload File Rab */
  doUploadRab() {
    this.loadingUpdateAwp = true
    if (this.uploadFileRab.length > 0) {
      this.serviceKegiatan.upload(this.uploadFileRab[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdRab = resp.data[0].id
              this.updateAwp()
            } else {
              this.loadingUpdateAwp = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingUpdateAwp = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      if (this.dataDetailAwp?.rabFile?.id) {
        this.dataIdRab = this.dataDetailAwp?.rabFile?.id
      } else {
        this.dataIdRab = null
      }
      this.updateAwp()
    }
  }

  updateAwp() {
    let selectedSubComponentId
    let selectedSubSubComponentId
    let startDate
    let endDate
    let selectedProvinceCity = []
    let selectedProvinceCityExist = []
    let pdoRequestApi = []
    let iriRequestApi = []
    let dataRegencyIds = []
    let dataRegencyExistingIds = []
    let pdoRequestExisting = []
    let iriRequestExisting = []
    let selectedProvinceAndCityEdit = JSON.parse(JSON.stringify(this.selectedProvinceAndCity))
    let codeSelectedToApi
    let subSubComponentCode
    let componentCode
    let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
    let selectedRiskToApi
    selectedRiskToApi = this.selectedRisk

    if (!this.editTotalPeserta) {
      this.editTotalPeserta = 0
    }

    if (!this.editjumlahNasrasumber) {
      this.editjumlahNasrasumber = 0
    }

    selectedProvinceAndCityEdit.forEach(x => {
      if (x.regencyId.length > 0) {
        x.regencyId = x.regencyId.map(y => this.dataAllCitySelected.find(xz => xz.name === y))
      }
    })

    if (this.selectedCode) {
      if (this.selectedCode.id === '2') {
        if (this.selectedCodeSubSubComponent) {
          if (this.selectedCodeSubSubComponent?.id === '1') {
            let x = selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
            x = x + '.' + this.inputCodeManual
            componentCode = x + '.' + this.inputCodeManualKegiatan
            codeSelectedToApi = null
          } else {
            componentCode = this.selectedCodeSubSubComponent?.code + '.' + this.inputCodeManualKegiatan
            codeSelectedToApi = null
          }
        } else {
          let x = selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
          componentCode = x + '.' + this.inputCodeManualKegiatan
          codeSelectedToApi = null
        }
      } else {
        codeSelectedToApi = this.selectedCode.id
        componentCode = null
      }
    } else {
      codeSelectedToApi = null
      componentCode = null
    }


    selectedProvinceAndCityEdit.forEach(data => {
      if (data?.regencyId) {
        dataRegencyIds = []
        data.regencyId.forEach(x => {
          dataRegencyIds.push(x.id)
        })
        selectedProvinceCity.push({
          provinceId: data?.provinceId?.id,
          regencyIds: dataRegencyIds
        })
      } else {
        selectedProvinceCity.push({
          provinceId: data?.provinceId?.id,
          regencyIds: null
        })
      }
    })

    if (this.pdoExisting.length > 0) {
      this.pdoExisting.forEach(data => {
        pdoRequestExisting.push(data.id)
      })
    } else {
      pdoRequestExisting = []
    }

    if (this.iriExisting.length > 0) {
      this.iriExisting.forEach(data => {
        iriRequestExisting.push(data.id)
      })
    } else {
      iriRequestExisting = []
    }

    if (!this.selectedSubComponentExist) {
      selectedSubComponentId = null
    } else {
      selectedSubComponentId = this.selectedSubComponentExist.id
    }

    if (!this.selectedCodeSubSubComponent) {
      selectedSubSubComponentId = null
      subSubComponentCode = null
    } else {
      if (this.selectedCodeSubSubComponent) {
        if (this.selectedCodeSubSubComponent?.id === '1') {
          selectedSubSubComponentId = null
          selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
          subSubComponentCode = selectedSubSubComponent.code + '.' + this.inputCodeManual
        } else {
          selectedSubSubComponentId = this.selectedCodeSubSubComponent.id
          subSubComponentCode = null
        }
      } else {
        selectedSubSubComponentId = null
        subSubComponentCode = null
      }
    }

    if (this.provinceAndRegenciesExist.length > 0) {
      this.provinceAndRegenciesExist.forEach(data => {
        if (data?.regencies) {
          dataRegencyExistingIds = []
          data?.regencies.forEach(x => {
            dataRegencyExistingIds.push(x.id)
          })
          selectedProvinceCityExist.push({
            awpProvincesId: data.id,
            awpProvincesRegenciesId: dataRegencyExistingIds
          })
        } else {
          selectedProvinceCityExist.push({
            awpProvincesId: data.id,
            awpProvincesRegenciesId: null
          })
        }
      })
    } else {
      selectedProvinceCityExist = []
    }


    if (this.pdoRequest.length > 0) {
      this.pdoRequest.forEach(data => {
        pdoRequestApi.push(data.id)
      })
    } else {
      pdoRequestApi = []
    }

    if (this.iriRequest.length > 0) {
      this.iriRequest.forEach(data => {
        iriRequestApi.push(data.id)
      })
    } else {
      iriRequestApi = []
    }

    if (!this.subSubComponentName) {
      this.subSubComponentName = null
    }

    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')


    const params = {
      year: this.date.getFullYear(),
      componentCodeId: codeSelectedToApi,
      componentId: this.selectedComponentExist.id,
      subComponentId: selectedSubComponentId,
      subSubComponentId: selectedSubSubComponentId,
      name: this.editNamaKegiatan,
      budgetAwp: this.editBudget,
      purpose: this.editTujuanKegiatan,
      description: this.editDeskripsiKegiatan,
      eventManagementTypeId: this.selectedValueEventType.id,
      startDate: startDate,
      endDate: endDate,
      eventOtherInformation: this.editWaktuInformasiLainnya,
      provincesRegenciesIds: selectedProvinceCityExist,
      newProvincesRegencies: selectedProvinceCity,
      eventVolume: this.editVolumeEvent,
      eventTypeId: this.selectedEvent.id,
      nsCount: this.editjumlahNasrasumber,
      nsInstitution: this.editAsalLembagaNarasumber,
      nsOtherInformation: this.editInformasiLainNarasumber,
      participantCount: this.editTotalPeserta,
      participantOtherInformation: this.editInformasiPeserta,
      eventOutput: this.editOutputKegiatan,
      pdoIds: pdoRequestExisting,
      newPdoIds: pdoRequestApi,
      iriIds: iriRequestExisting,
      newIriIds: iriRequestApi,
      nameOtherInformation: this.otherInformationAwp,
      locationOtherInformation: this.tempatInformasiLainnya,
      subSubComponentName: this.subSubComponentName,
      rabFileId: this.dataIdRab,
      activityModeId: this.selectedModaActivity.id,
      participantTarget: this.targetParticipant,
      descriptionRisk: this.potentialRisk,
      mitigationRisk : this.mitigationRisk,
      subSubComponentCode: subSubComponentCode,
      componentCode: componentCode,
      risk: selectedRiskToApi,
      potentialRisk: this.kemungkinanTerjadi,
      impactRisk: this.dampak,
      symptomsRisk: this.gejalaResiko,
    }
    this.service.updateAwp(this.detailIdAwp, params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingUpdateAwp = false
            this.goToListAWP()
          }
        }, error => {
          this.loadingUpdateAwp = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  disableUpdateButton() {
    return !this.editNamaKegiatan || !this.editTujuanKegiatan || !this.editDeskripsiKegiatan || !this.fromDate || !this.toDate || !this.editVolumeEvent || !this.editOutputKegiatan || (this.selectedProvinceAndCity.length == 0 && this.provinceAndRegenciesExist.length == 0) || !this.selectedSubComponentExist ||  this.validateNextButtonForSubComponent()
  }
}
