import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AWPComponent} from './awp.component'
import {DetailAwpComponent} from "./detail-awp/detail-awp.component";
import {AddAwpComponent} from "./add-awp/add-awp.component";
import {EditAwpComponent} from "./edit-awp/edit-awp.component";
import {ExportAwpExcelComponent} from "./export-awp-excel/export-awp-excel.component";
import {OtherInformationAwpComponent} from "./other-information-awp/other-information-awp.component";
import {AuthGuard} from "../../../core/guards/auth.guard";

const routes: Routes = [
    {
        path: 'awp',
        component: AWPComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-awp/:id',
        component: DetailAwpComponent, canActivate: [AuthGuard]
    },
    {
        path: 'add-awp',
        component: AddAwpComponent, canActivate: [AuthGuard]
    },
    {
        path: 'edit-awp/:id',
        component: EditAwpComponent, canActivate: [AuthGuard]
    },
    {
        path: 'export-excel',
        component: ExportAwpExcelComponent, canActivate: [AuthGuard]
    },
    {
        path: 'other-information/:year',
        component: OtherInformationAwpComponent, canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AwpRoutingModule {}
