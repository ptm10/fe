import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {parse} from "date-fns";
import Swal from "sweetalert2";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import {ManagementHolidays} from "../../../../core/models/permission.model";
import {ComponentService} from "../../../../core/services/Component/component.service";
import {ComponentModel} from "../../../../core/models/componentModel";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {Event, EventDocumentType, MangementType, Province} from "../../../../core/models/awp.model";
import {PDOModel} from "../../../../core/models/PDO.model";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-add-awp',
  templateUrl: './add-awp.component.html',
  styleUrls: ['./add-awp.component.scss']
})
export class AddAwpComponent implements OnInit {

  public Description = ClassicEditor;
  public Purpose = ClassicEditor;
  public OutputEvent = ClassicEditor;
  public OtherInfoAwp = ClassicEditor;
  public Institution = ClassicEditor;
  public participantTarget = ClassicEditor;
  public riskMitigation = ClassicEditor;
  public riskPotential = ClassicEditor;
  breadCrumbItems: Array<{}>;
  component: ComponentModel[]
  componentMaster: ComponentModel[]
  finishLoadData = true
  finishLoadDataCount = 0
  date: Date
  minDateYear: any
  maxDateYear: any
  selectedValueEventType: any
  selectedModaActivity: any
  loadingSaveData = false
  // date
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  hoveredDate: NgbDate | null = null;
  disabledDates:NgbDateStruct[] = []
  minDate = undefined;
  maxDate = undefined;
  loadingListComponent = false
  dataIdRab: string = null
  selectedManualCodeExist = false
  selectedManualCodeKegiatanExist = false

  // holiday
  holidayManagement: ManagementHolidays[]
  listHolidays = []

  // component
  selectedComponentExist: ComponentModel

  selectedSubComponentExist: ComponentModel
  selectedSubComponentExistMaster: ComponentModel
  selectedSubComponentExistEdit: ComponentModel

  selectedSubSubComponentExist: ComponentModel
  selectedSubSubComponentExistMaster: ComponentModel
  selectedSubSubComponentExistEdit: ComponentModel

  codeSelected: any = 'pilih'

  listCodeSelected = []

  // data input
  inputCodeManual: string
  inputCodeManualKegiatan: string
  subSubComponentName: string
  kegiatan: string
  budgetAwp: number
  tujuanKegiatan: string = null
  deskripsiKegiatan: string = null
  waktuInformasiLainnya: string
  tempatInformasiLainnya: string
  jumlahNasrasumber: number
  asalLembagaNarasumber: string = null
  potentialRisk: string = null
  mitigationRisk: string = null
  targetParticipant: string = null
  informasiLainNarasumber: string
  totalPeserta: number
  informasiPeserta: string
  outputKegiatan: string = null
  otherInformationAwp: string = null
  volumeEvent: number
  selectedCityFromList = []

  // management type
  listManagementType: MangementType[]
  listManagementTypeMaster: MangementType[]
  selectedMangementTypeExist: MangementType

  //Moda Kegiatan
  listModaType: MangementType[]
  listModaTypeMaster: MangementType[]

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  provinceRequest = []
  selectedProvinceExist: Province

  // city
  listCityAll: Province[]
  listCityAllMaster: Province[]
  selectedCityExist: Province
  loadingListCity = false
  selectedProvinceAndCity = []
  dataAllCitySelected = []

  // event
  listEvent: Event[]
  listEventMaster: Event[]
  selectedEvent: Event

  // PDO
  listPdoAll: PDOModel[]
  listPdoAllMaster: PDOModel[]
  pdoRequest = []
  addPdo = []
  removePdo = []
  searchPdo = []

  // IRI
  listIriAll: PDOModel[]
  listIriAllMaster: PDOModel[]
  iriRequest = []
  addIri = []
  removeIri = []
  searchIri = []

  //code
  selectedCode: any = null
  selectedCodeExisting: any
  listCode: any
  listCodeExisting: any

  //code sub sub component
  selectedCodeSubSubComponent: any = null
  selectedCodeSubSubComponentExisting: any
  listCodeSelectedSubSubComponent: any
  listCodeSelectedSubSubComponentExisting: any
  listCodeSelectedSubSub = []
  listCityFromApi: string[];

  // rab
  uploadFileRab: Array<File> = [];
  uploadFileRabIds: Array<File> = [];

  // list keterangan risiko
  listAllRisk: string[] = []
  selectedRisk: string
  dataListRisk: EventDocumentType[]
  otherRisk: string
  kemungkinanTerjadi: number;
  dampak: number;
  gejalaResiko: string

  constructor(
      private router: Router,
      public formatter: NgbDateParserFormatter,
      private calendar: NgbCalendar,
      private serviceAdministration: AdministrationService,
      private serviceComponent: ComponentService,
      private service: AwpService,
      private modalService: NgbModal,
      private serviceKegiatan: KegiatanService,
      private swalAlert: SwalAlert
  ) {}

  ngOnInit(): void {
    this.minDateYear = new Date(2020, 1, 1)
    this.maxDateYear = new Date(2024, 11, 30)
    this.breadCrumbItems = [{ label: 'AWP' }, { label: 'Tambah AWP', active: true }]
    this.getListHolidayManagement()
    this.managementType()
    this.listProvince()
    this.dataEvent()
    this.dataPDO()
    this.dataIri()
    this.listAllModa()
    this.validateButtonKegiatan()
    this.getListRisk()
    this.jumlahNasrasumber = 0
    this.totalPeserta = 0
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  scrollToTop() {
    window.scrollTo(0, 0);
  }

  setTimeRangeEvent() {
    const current = new Date();
    this.minDate = {
      year: this.date.getFullYear(),
      month: 1,
      day: 1
    };
    this.maxDate = {
      year: this.date.getFullYear(),
      month: 12,
      day: 31
    };
  }

  changeYearAwp() {
    this.removeSelectedComponent()
    this.getListComponent(this.date.getFullYear())
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  /** Input date calender */
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  withoutDots(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode == 190 || charCode == 110 || charCode == 188);
  }

  checkTotalNarasumber() {
    if (this.jumlahNasrasumber < 0 || !this.jumlahNasrasumber) {
      this.jumlahNasrasumber = 0
    }
  }

  checkTotalParticipant() {
    if (this.totalPeserta < 0 || !this.totalPeserta) {
      this.totalPeserta = 0
    }
  }

  /** * date */
  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  onDateSelection(date: NgbDate, datepicker: any) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      datepicker.close();
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      datepicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
      datepicker.close();
    }
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  /** * end date */

  /** * list Risk */

  // new tag
  CreateNew(Position){
    return Position
  }

  getListRisk() {
    let tag = []
    this.serviceKegiatan.listRiskInRik().subscribe(
        resp => {
          if (resp.success) {
            this.dataListRisk = resp.data.content
            this.dataListRisk.forEach(x => {
              tag.push(x.name)
            })
            this.listAllRisk = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End list Risk */


  /** * list holidays */
  getListHolidayManagement() {
    this.listHolidays = []
    this.serviceAdministration.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * end list holidays */

  /** * list component & selected component */

  getListComponent(data) {
    this.loadingListComponent = true
    this.serviceComponent.listComponent(data).subscribe(
        resp => {
          this.loadingListComponent = false
          if (resp.success) {
            this.component = resp.data
            this.componentMaster = resp.data
            this.component.forEach(dataCode => {
              dataCode.code = 'Komponen '+ dataCode.code
            })
          } else {
            this.loadingListComponent = false
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.loadingListComponent = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedComponent(data) {
    this.kegiatan = ''
    this.selectedCode = null
    this.selectedComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null
    this.component = this.componentMaster

    this.selectedComponentExist = data
    this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)

    this.selectedSubComponentExistMaster = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExistMaster.subComponent.forEach(dataSub => {
      if (!dataSub.code.includes('Sub-Komponen')) {
        dataSub.code = 'Sub-Komponen '+dataSub.code
      }
    })
    this.selectedSubComponentExistEdit = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
  }

  searchFilterComponent(e) {
    const searchStr = e.target.value
    let filter = this.componentMaster
    if (this.selectedComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
    } else {
      filter = this.componentMaster
    }

    this.component = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedComponent() {
    this.selectedCode = null
    this.component = this.componentMaster
    this.selectedComponentExist = null

    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null

    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.codeSelectedFromComponent()
    this.clearSelectedCodeSubSubComponent()
    this.kegiatan = ''
    this.inputCodeManual = ''
  }

  selectedSubComponent(data) {
    this.selectedCode = null
    this.kegiatan = ''
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent

    this.selectedSubComponentExist = data
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
    this.selectedSubSubComponentExistMaster = Object.assign({}, this.selectedSubComponentExist)
    if (this.selectedSubSubComponentExistMaster.subComponent.length > 0) {
      this.selectedSubSubComponentExistMaster.subComponent.forEach(dataSub => {
        if (!dataSub.code.includes('Komponen')) {
          dataSub.code = 'Komponen '+dataSub.code
        }
      })
    }
    this.selectedSubSubComponentExistEdit = Object.assign({}, this.selectedSubComponentExist)
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
    this.codeSelectedFromSubSubComponent()
    this.validateName()
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
  }

  searchFilterSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubComponentExistEdit.subComponent
    if (this.selectedSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubComponentExist.id)
    } else {
      filter = this.selectedSubComponentExistEdit.subComponent
    }

    this.selectedSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }
  removeSelectedSubComponent() {
    this.selectedCode = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubSubComponentExist = null
    this.codeSelectedFromComponent()
    this.kegiatan = ''
    this.clearSelectedCodeSubSubComponent()
    this.inputCodeManual = ''
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
  }

  selectedSubSubComponent(data) {
    this.selectedCode = null
    this.kegiatan = ''
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent

    this.selectedSubSubComponentExist = data
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubSubComponentExist.id)
    this.codeSelectedFromComponent()
    this.validateName()
    this.validateButtonKegiatan()
  }

  searchFilterSubSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubSubComponentExistEdit.subComponent
    if (this.selectedSubSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubSubComponentExist.id)
    } else {
      filter = this.selectedSubSubComponentExistEdit.subComponent
    }

    this.selectedSubSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedSubSubComponent() {
    this.selectedCode = null
    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster.subComponent = this.selectedSubSubComponentExistEdit.subComponent
    this.codeSelectedFromComponent()
    this.kegiatan = ''
    this.inputCodeManual = ''
    this.validateName()
  }

  changeManualCodeSubSubComponent() {
    console.log(this.inputCodeManual.length)
    this.selectedManualCodeExist = false
    let filterInputCode
    let selectedInputCode
    let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))

    selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
    selectedInputCode = selectedSubSubComponent.code + '.' + this.inputCodeManual

    selectedSubSubComponent.subComponent.forEach(x => {
      x.code = x.code.replace('Komponen ', '')
    })
    filterInputCode = selectedSubSubComponent?.subComponent.filter(x => x.code === selectedInputCode)
    if (filterInputCode.length > 0) {
      this.selectedManualCodeExist = true
    }
    this.selectedCode = null
    this.inputCodeManualKegiatan = ''
    this.kegiatan = ''
    this.codeSelectedFromComponent()
    this.validateName()
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
  }

  changeManualCodekegiatan() {
    this.selectedManualCodeKegiatanExist = false
    let filterInputCode
    let selectedInputCode
    let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
    if (this.selectedCodeSubSubComponent) {
      let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedCodeSubSubComponent))
      selectedSubSubComponent.code = selectedSubSubComponent.code.replace(' ', '')
      selectedInputCode = selectedSubSubComponent.code + '.' + this.inputCodeManualKegiatan
      filterInputCode = selectedSubSubComponent.subComponent.filter(x => x.code === selectedInputCode)
      if (filterInputCode.length > 0) {
        this.selectedManualCodeKegiatanExist = true
      }
    } else if (this.selectedSubComponentExist){
      selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
      selectedInputCode = selectedSubSubComponent.code + '.' + this.inputCodeManualKegiatan

      selectedSubSubComponent.subComponent.forEach(x => {
        x.code = x.code.replace('Komponen ', '')
      })
      filterInputCode = selectedSubSubComponent?.subComponent.filter(x => x.code === selectedInputCode)
      if (filterInputCode.length > 0) {
        this.selectedManualCodeKegiatanExist = true
      }
    }
  }

  codeSelectedFromComponent() {
    let selectedSubSubComponent
    let codeManualInput
    if (this.selectedCodeSubSubComponent) {
      selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedCodeSubSubComponent))
      selectedSubSubComponent.code = selectedSubSubComponent?.code.replace('_', '')
      if (this.inputCodeManual) {
        selectedSubSubComponent.code = selectedSubSubComponent?.code + this.inputCodeManual + '._'
      } else {
        selectedSubSubComponent.code = selectedSubSubComponent.code + '._'
      }
      codeManualInput = {
        id: '2',
        code: selectedSubSubComponent?.code,
        subComponent: []
      }
    } else if (this.selectedSubComponentExist){
      let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
      selectedSubSubComponent.code = selectedSubSubComponent.code + '._'
      codeManualInput = {
        id: '2',
        code: selectedSubSubComponent?.code,
        subComponent: []
      }
    }
    let selectedCodeComponent
    this.codeSelected = 'pilih'
    this.listCodeSelected = []
    this.listCodeExisting = []
    this.listCodeSelected = []
    if (this.selectedCodeSubSubComponent) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedCodeSubSubComponent))
      selectedCodeComponent.subComponent.forEach(data => {
        data.code = data.code.replace('Komponen', '')
        if (data.subComponent.length == 0) {
          this.listCodeSelected.push(data)
        }
      })
      this.listCodeSelected.push(codeManualInput)
    } else if (this.selectedSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      selectedCodeComponent.subComponent.forEach(data => {
        data.code = data.code.replace('Komponen', '')
        this.listCodeSelected.push(data)
      })
      this.listCodeSelected.push(codeManualInput)
    }
    this.listCodeExisting = JSON.parse(JSON.stringify(this.listCodeSelected))
    this.listCodeSelected = JSON.parse(JSON.stringify(this.listCodeSelected))
  }

  changeValueSelected() {
    if (this.codeSelected === 'pilih') {
      this.kegiatan = ''
    } else {
      this.kegiatan = this.codeSelected
    }
  }

  /** * end list component & selected component */

  /** * selected code */

  selectedCodeFromList(data) {
    this.selectedCode = null
    this.listCodeSelected = this.listCodeExisting

    this.selectedCode = data
    this.listCodeSelected = this.listCodeSelected.filter(data => data.id !== this.selectedCode.id)
    this.kegiatan = this.selectedCode.description
    console.log(this.kegiatan)
  }

  clearSelectedCode() {
    this.selectedCode = null
    this.listCodeSelected = this.listCodeExisting
    this.kegiatan = ''
    this.inputCodeManualKegiatan = ''
  }


  /** * end of selected code */

  /** * selected code sub sub component */

  codeSelectedFromSubSubComponent() {
    let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
    console.log(selectedSubSubComponent)
    selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen', '')
    selectedSubSubComponent.code = selectedSubSubComponent.code + '._'
    let codeManualInput = {
      id: '1',
      code: selectedSubSubComponent?.code,
      subComponent: []
    }
    let selectedCodeComponent
    this.codeSelected = 'pilih'
    this.listCodeSelectedSubSub = []
    this.listCodeSelectedSubSubComponent = []
    this.listCodeSelectedSubSubComponentExisting = []
    if (this.selectedSubComponentExist) {
      selectedCodeComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
      selectedCodeComponent.subComponent.forEach(data => {
        data.code = data.code.replace('Komponen', '')
          this.listCodeSelectedSubSub.push(data)
      })
      this.listCodeSelectedSubSub.push(codeManualInput)
      console.log(this.listCodeSelectedSubSub)
    }
    this.listCodeSelectedSubSubComponent = JSON.parse(JSON.stringify(this.listCodeSelectedSubSub))
    this.listCodeSelectedSubSubComponentExisting = JSON.parse(JSON.stringify(this.listCodeSelectedSubSub))
    this.codeSelectedFromComponent()
  }

  selectedCodeFromListSubSubComponent(data) {
    this.selectedCodeSubSubComponent = null
    this.listCodeSelectedSubSubComponent = this.listCodeSelectedSubSubComponentExisting

    this.selectedCodeSubSubComponent = data
    this.listCodeSelectedSubSubComponent = this.listCodeSelectedSubSubComponent.filter(data => data.id !== this.selectedCodeSubSubComponent.id)
    this.subSubComponentName = this.selectedCodeSubSubComponent.description
    this.codeSelectedFromComponent()
    this.clearSelectedCode()
    this.validateName()
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
  }

  clearSelectedCodeSubSubComponent() {
    this.selectedCodeSubSubComponent = null
    this.listCodeSelectedSubSubComponent = this.listCodeSelectedSubSubComponentExisting
    this.subSubComponentName = ''
    this.codeSelectedFromComponent()
    this.clearSelectedCode()
    this.inputCodeManual = ''
    this.validateName()
    this.validateButtonKegiatan()
    this.validateNameSubSubComponent()
  }

  validateNextButtonForSubComponent() {
    if (this.selectedCodeSubSubComponent) {
      if (this.selectedCodeSubSubComponent?.id === '1') {
        if (this.subSubComponentName) {
          return false
        } else {
          return true
        }
      } else {
        if (this.subSubComponentName) {
          return false
        } else {
          return true
        }
      }
    } else {
      return false
    }
  }

  validateButtonKegiatan() {
    if (this.selectedSubComponentExist) {
      if (this.selectedCodeSubSubComponent) {
        if (this.selectedCodeSubSubComponent?.id === '1') {
          if (this.inputCodeManual) {
            if (this.selectedManualCodeExist) {
              return true
            } else {
              return false
            }
          } else {
            return true
          }
        } else {
          return false
        }
      }
    } else {
      return true
    }
  }

  validateName() {
    if (this.selectedCode) {
      if (this.selectedCode?.id === '2') {
        if (this.inputCodeManualKegiatan) {
          if (this.selectedManualCodeKegiatanExist) {
            return true
          } else {
            return false
          }
        } else {
          return true
        }
      } else {
        return false
      }
    } else {
      return true
    }
  }

  validateNameSubSubComponent() {
    if (this.selectedSubComponentExist) {
      if (this.selectedCodeSubSubComponent) {
        if (this.selectedCodeSubSubComponent?.id === '1') {
          if (this.inputCodeManual) {
            if (this.selectedManualCodeExist) {
              return true
            } else {
              return false
            }
          } else {
            return true
          }
        } else {
          return false
        }
      } else {
        return true
      }
    } else {
      return true
    }
  }

  testing() {
    console.log(this.selectedCodeSubSubComponent)
    if (!this.subSubComponentName) {
      this.subSubComponentName = ''
    }
    console.log(this.subSubComponentName)
  }
  /** * end of sub sub component */

  /** * Moda Kegiatan */
  listAllModa() {
    this.startLoading()
    this.service.listModa().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listModaType = resp.data.content
            this.listModaTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * End Of Moda Kegiatan */

  goToListAWP() {
    this.router.navigate(['admin/awp'])
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  /** * Management Type */

  managementType() {
    this.startLoading()
    this.service.eventManagementType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listManagementType = resp.data.content
            this.listManagementTypeMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedManagementType(data) {
    this.selectedMangementTypeExist = null
    this.listManagementType = this.listManagementTypeMaster

    this.selectedMangementTypeExist = data
    this.listManagementType = this.listManagementType.filter(dataFilter => dataFilter.id != this.selectedMangementTypeExist.id)
  }

  searchFilterManagementType(e) {
    const searchStr = e.target.value
    let filter = this.listManagementTypeMaster
    if (this.selectedMangementTypeExist) {
      filter = filter.filter(x => x.id !== this.selectedMangementTypeExist.id)
    } else {
      filter = this.listManagementTypeMaster
    }

    this.listManagementType = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List Province */

  listProvince() {
    this.startLoading()
    this.service.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            // this.searchProvince = this.listProvinceAll
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedProvince(data) {
    this.selectedCityFromList = []
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster

    this.selectedProvinceExist = data
    this.listProvinceAll = this.listProvinceAll.filter(dataFilter => dataFilter.id != this.selectedProvinceExist.id)
    this.listCity()
    this.selectedCityExist = null
  }

  searchFilterProvince(e) {
    const searchStr = e.target.value
    let filter = this.listProvinceAllMaster
    if (this.selectedProvinceExist) {
      filter = filter.filter(x => x.id !== this.selectedProvinceExist.id)
    } else {
      filter = this.listProvinceAllMaster
    }

    this.listProvinceAll = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List City */

  listCity() {
    let cityNameFromApi = []
    this.loadingListCity= true
    const paramsCityDt = {
      enablePage: false,
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ],
      paramIs: [
        {
          field: "provinceId",
          dataType: "string",
          value: this.selectedProvinceExist.id
        }
      ]
    }
    this.service.city(paramsCityDt).subscribe(
        resp => {
          if (resp.success) {
            this.loadingListCity = false
            this.listCityAll = resp.data.content
            // this.listCityAll = this.listCityAll.filter(x => this.selectedProvinceAndCity.filter(y => x.id !== y.regencyId.id))
            this.listCityAll.forEach(x => {
              cityNameFromApi.push(x.name)
            })

            if (this.selectedProvinceAndCity.length > 0) {
              this.selectedProvinceAndCity.forEach(x => {
                if (x.regencyId.length > 0) {
                  x.regencyId.forEach(y => {
                    cityNameFromApi = cityNameFromApi.filter(exist => exist !== y)
                  })
                }
              })
            }
            this.listCityFromApi = cityNameFromApi

            if (this.selectedProvinceAndCity.length > 0) {
              this.selectedProvinceAndCity.forEach(x => {
                if (x.regencyId != null) {
                  this.listCityAll = this.listCityAll.filter(y => y.id !== x.regencyId.id)
                }
              })
            }
            this.listCityAllMaster = resp.data.content
            // this.searchProvince = this.listProvinceAll
          }
        }, error => {
          this.loadingListCity = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedCity(data) {
    this.selectedCityExist = null
    this.listCityAll = this.listCityAllMaster

    this.selectedCityExist = data
    this.listCityAll = this.listCityAll.filter(dataFilter => dataFilter.id != this.selectedCityExist.id)
    if (this.selectedProvinceAndCity.length > 0) {
      this.selectedProvinceAndCity.forEach(x => {
        if (x.regencyId != null) {
          this.listCityAll = this.listCityAll.filter(y => y.id !== x.regencyId.id)
        }
      })
    }
  }

  searchFilterCity(e) {
    const searchStr = e.target.value
    let filter = this.listCityAllMaster
    if (this.selectedProvinceAndCity.length > 0) {
      this.selectedProvinceAndCity.forEach(x => {
        if (x.regencyId != null) {
          this.listCityAllMaster = this.listCityAllMaster.filter(y => y.id !== x.regencyId.id)
        }
      })
    }
    if (this.selectedCityExist) {
      filter = filter.filter(x => x.id !== this.selectedCityExist.id)
    } else {
      filter = this.listCityAllMaster
    }

    this.listCityAll = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  addSelectedProvinceAndCity() {
    console.log(this.selectedCityFromList)

    this.selectedCityFromList.forEach(x => {
      let y = this.listCityAll.find(dataSelected => dataSelected.name === x)
      if (y) {
        this.dataAllCitySelected.push(y)
      }
    })
    this.selectedProvinceAndCity.push({
      provinceId: this.selectedProvinceExist,
      regencyId: this.selectedCityFromList
    })
    this.selectedCityExist = null
    this.selectedProvinceExist = null
    this.listProvinceAll = this.listProvinceAllMaster
    this.selectedCityFromList = []
    this.closeModal()
  }

  removeSelectedProvinceAndCity(data) {
    this.selectedProvinceAndCity.forEach((check, index ) => {
      if (check.provinceId.id == data.provinceId.id) {
        this.selectedProvinceAndCity.splice(index, 1)
      }
    })
    this.listCityFromApi = []
  }


  /** * List Event */
  dataEvent() {
    this.startLoading()
    this.service.eventType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEvent = resp.data.content
            this.listEventMaster = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedEventType(data) {
    this.selectedEvent = null
    this.listEvent = this.listEventMaster

    this.selectedEvent = data
    this.listEvent = this.listEvent.filter(dataFilter => dataFilter.id != this.selectedEvent.id)
  }

  searchFilterEvent(e) {
    const searchStr = e.target.value
    let filter = this.listEventMaster
    if (this.selectedEvent) {
      filter = filter.filter(x => x.id !== this.selectedEvent.id)
    } else {
      filter = this.listEventMaster
    }

    this.listEvent = filter.filter((product) => {
      return ((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** * List PDO */

  dataPDO() {
    this.service.pdo().subscribe(
        resp => {
          if (resp.success) {
            this.listPdoAll = resp.data.content
            this.listPdoAllMaster = resp.data.content
            this.searchPdo = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  addPdoSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.pdoRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removePdo.forEach((check, index) => {
          if (check.id === data.id) {
            this.removePdo.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addPdo.push(data)
        }
        this.pdoRequest.push(data)
      }
    } else {
      this.pdoRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.pdoRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addPdo.forEach((check, index) => {
        if (check.id === data.id) {
          this.addPdo.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removePdo.push(data)
      }
    }
    this.listPdoAll = this.listPdoAllMaster

    this.pdoRequest.forEach(dataProvince => {
      this.listPdoAll = this.listPdoAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchPdo = this.listPdoAll
  }

  searchFilterPdo(e) {
    const searchStr = e.target.value
    this.listPdoAll = this.searchPdo.filter((product) => {
      return (product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.description.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  /** * List IRI */
  dataIri() {
    this.startLoading()
    this.service.iri().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listIriAll = resp.data.content
            this.listIriAllMaster = resp.data.content
            this.searchIri = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addIriSelected(data, check) {
    if (check == 1) {
      let checkExist = false
      this.iriRequest.filter(exist => {
        if (exist.id === data.id) {
          checkExist = true
          return
        }
      })
      if (!checkExist) {
        let checkRemoveExist = false
        this.removeIri.forEach((check, index) => {
          if (check.id === data.id) {
            this.removeIri.splice(index, 1)
            checkRemoveExist = true
            return
          }
        })
        if (!checkRemoveExist) {
          data.status = 1
          this.addIri.push(data)
        }
        this.iriRequest.push(data)
      }
    } else {
      this.iriRequest.forEach((existing, index) => {
        if (existing.id === data.id) {
          data.checked = false
          this.iriRequest.splice(index, 1)
        }
      })
      let checkAddExist = false

      this.addIri.forEach((check, index) => {
        if (check.id === data.id) {
          this.addIri.splice(index, 1)
          checkAddExist = true
          return
        }
      })

      if (!checkAddExist) {
        data.status = 0
        this.removeIri.push(data)
      }
    }
    this.listIriAll = this.listIriAllMaster

    this.iriRequest.forEach(dataProvince => {
      this.listIriAll = this.listIriAll.filter(dataFilter => dataFilter.id !== dataProvince.id)
    })

    this.searchIri = this.listIriAll
  }

  searchFilterIri(e) {
    const searchStr = e.target.value
    this.listIriAll = this.searchIri.filter((product) => {
      return (product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product.description.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  /** Upload File Rab */
  doUploadRab() {
    this.loadingSaveData = true
    if (this.uploadFileRab.length > 0) {
      this.serviceKegiatan.upload(this.uploadFileRab[0]).subscribe(resp => {
            if (resp.success) {
              this.dataIdRab = resp.data[0].id
              this.saveAwp()
            } else {
              this.loadingSaveData = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: resp.message,
              });
            }
          }, error => {
            this.loadingSaveData = false
            this.swalAlert.showAlertSwal(error)
          }
      );
    } else {
      this.dataIdRab = null
      this.saveAwp()
    }
  }

  saveAwp() {
    let selectedSubComponentId
    let selectedSubSubComponentId
    let startDate
    let endDate
    let selectedProvinceCity = []
    let pdoRequestApi = []
    let iriRequestApi = []
    let dataRegencyIds = []
    let selectedProvinceAndCityEdit = JSON.parse(JSON.stringify(this.selectedProvinceAndCity))
    let codeSelectedToApi
    let subSubComponentCode
    let componentCode
    let selectedSubSubComponent = JSON.parse(JSON.stringify(this.selectedSubComponentExist))
    let selectedRiskToApi
    selectedRiskToApi = this.selectedRisk

    if (!this.totalPeserta) {
      this.totalPeserta = 0
    }
    if (!this.jumlahNasrasumber) {
      this.jumlahNasrasumber = 0
    }

    if (this.selectedCode) {
      if (this.selectedCode.id === '2') {
        if (this.selectedCodeSubSubComponent) {
          if (this.selectedCodeSubSubComponent?.id === '1') {
            let x = selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
            x = x + '.' + this.inputCodeManual
            componentCode = x + '.' + this.inputCodeManualKegiatan
            console.log(componentCode)
            codeSelectedToApi = null
          } else {
            componentCode = this.selectedCodeSubSubComponent?.code.trim() + '.' + this.inputCodeManualKegiatan
            console.log(this.selectedCodeSubSubComponent?.code)
            codeSelectedToApi = null
          }
        } else {
          let x = selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
          componentCode = x + '.' + this.inputCodeManualKegiatan
          codeSelectedToApi = null
        }
      } else {
        codeSelectedToApi = this.selectedCode.id
        componentCode = null
      }
    } else {
      codeSelectedToApi = null
      componentCode = null
    }
    selectedProvinceAndCityEdit.forEach(x => {
      if (x.regencyId.length > 0) {
        x.regencyId = x.regencyId.map(y => this.dataAllCitySelected.find(xz => xz.name === y))
      }
    })

    selectedProvinceAndCityEdit.forEach(data => {
      if (data?.regencyId) {
        dataRegencyIds = []
        data.regencyId.forEach(x => {
          dataRegencyIds.push(x.id)
        })
        selectedProvinceCity.push({
          provinceId: data?.provinceId?.id,
          regencyIds: dataRegencyIds
        })
      } else {
        selectedProvinceCity.push({
          provinceId: data?.provinceId?.id,
          regencyIds: null
        })
      }
    })

    if (!this.selectedSubComponentExist) {
      selectedSubComponentId = null
    } else {
      selectedSubComponentId = this.selectedSubComponentExist.id
    }

    if (!this.selectedCodeSubSubComponent) {
      selectedSubSubComponentId = null
      subSubComponentCode = null
    } else {
      if (this.selectedCodeSubSubComponent) {
        if (this.selectedCodeSubSubComponent?.id === '1') {
          selectedSubSubComponentId = null
          selectedSubSubComponent.code = selectedSubSubComponent.code.replace('Sub-Komponen ', '')
          subSubComponentCode = selectedSubSubComponent.code + '.' + this.inputCodeManual
        } else {
          selectedSubSubComponentId = this.selectedCodeSubSubComponent.id
          subSubComponentCode = null
        }
      } else {
        selectedSubSubComponentId = null
        subSubComponentCode = null
      }
    }

    if (this.pdoRequest.length > 0) {
      this.pdoRequest.forEach(data => {
        pdoRequestApi.push(data.id)
      })
    } else {
      pdoRequestApi = []
    }

    if (this.iriRequest.length > 0) {
      this.iriRequest.forEach(data => {
        iriRequestApi.push(data.id)
      })
    } else {
      iriRequestApi = []
    }

    if (!this.subSubComponentName) {
      this.subSubComponentName = null
    }

    startDate = this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0')
    endDate = this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0')

    const params = {
      year: this.date.getFullYear(),
      componentId: this.selectedComponentExist.id,
      subComponentId: selectedSubComponentId,
      subSubComponentId: selectedSubSubComponentId,
      name: this.kegiatan,
      componentCodeId: codeSelectedToApi,
      budgetAwp: this.budgetAwp,
      purpose: this.tujuanKegiatan,
      description: this.deskripsiKegiatan,
      eventManagementTypeId: this.selectedValueEventType.id,
      startDate: startDate,
      endDate: endDate,
      eventOtherInformation: this.waktuInformasiLainnya,
      newProvincesRegencies: selectedProvinceCity,
      eventVolume: this.volumeEvent,
      eventTypeId: this.selectedEvent.id,
      nsCount: this.jumlahNasrasumber,
      nsInstitution: this.asalLembagaNarasumber,
      nsOtherInformation: this.informasiLainNarasumber,
      participantCount: this.totalPeserta,
      participantOtherInformation: this.informasiPeserta,
      eventOutput: this.outputKegiatan,
      newPdoIds: pdoRequestApi,
      newIriIds: iriRequestApi,
      nameOtherInformation: this.otherInformationAwp,
      locationOtherInformation: this.tempatInformasiLainnya,
      subSubComponentName: this.subSubComponentName,
      rabFileId: this.dataIdRab,
      activityModeId: this.selectedModaActivity.id,
      participantTarget: this.targetParticipant,
      descriptionRisk: this.potentialRisk,
      mitigationRisk : this.mitigationRisk,
      subSubComponentCode: subSubComponentCode,
      componentCode: componentCode,
      risk: selectedRiskToApi,
      potentialRisk: this.kemungkinanTerjadi,
      impactRisk: this.dampak,
      symptomsRisk: this.gejalaResiko,
    }
    this.service.saveAwp(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveData = false
            this.router.navigate(['admin/awp'])
          }
        }, error => {
          this.loadingSaveData = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
