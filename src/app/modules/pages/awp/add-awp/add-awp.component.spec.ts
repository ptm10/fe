import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAwpComponent } from './add-awp.component';

describe('AddAwpComponent', () => {
  let component: AddAwpComponent;
  let fixture: ComponentFixture<AddAwpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAwpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAwpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
