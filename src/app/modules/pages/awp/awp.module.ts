import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbDatepickerModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {AwpRoutingModule} from './awp-routing.module'
import {AWPComponent} from './awp.component'
import {DataTablesModule} from 'angular-datatables';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import {UIModule} from "../../../shared/ui/ui.module";
import { DetailAwpComponent } from './detail-awp/detail-awp.component';
import { AddAwpComponent } from './add-awp/add-awp.component';
import {ArchwizardModule} from "angular-archwizard";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import { EditAwpComponent } from './edit-awp/edit-awp.component';
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import {FileUploadModule} from "@iplab/ngx-file-upload";
import {SwalAlert} from "../../../core/helpers/Swal";
import { ExportAwpExcelComponent } from './export-awp-excel/export-awp-excel.component';
import { OtherInformationAwpComponent } from './other-information-awp/other-information-awp.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;


@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [AWPComponent, DetailAwpComponent, AddAwpComponent, EditAwpComponent, ExportAwpExcelComponent, OtherInformationAwpComponent],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        AwpRoutingModule,
        DataTablesModule,
        NgbTooltipModule,
        NgxMaskModule.forRoot(),
        UIModule,
        ArchwizardModule,
        NgbDatepickerModule,
        BsDatepickerModule,
        CKEditorModule,
        FileUploadModule,
    ],
  providers: [
      SwalAlert
  ]
})
export class AwpModule { }
