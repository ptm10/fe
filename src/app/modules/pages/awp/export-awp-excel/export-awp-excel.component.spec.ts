import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExportAwpExcelComponent } from './export-awp-excel.component';

describe('ExportAwpExcelComponent', () => {
  let component: ExportAwpExcelComponent;
  let fixture: ComponentFixture<ExportAwpExcelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExportAwpExcelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExportAwpExcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
