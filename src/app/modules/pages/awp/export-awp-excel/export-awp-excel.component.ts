import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {AwpModel} from "../../../../core/models/awp.model";
import {ComponentModel} from "../../../../core/models/componentModel";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {Router} from "@angular/router";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {leangueIndoDT} from "../../../../shared/pagination-custom";
import {DataResponse} from "../../../../core/models/dataresponse.models";
import {environment} from "../../../../../environments/environment";
import {CommonCons} from "../../../../shared/CommonCons";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-export-awp-excel',
  templateUrl: './export-awp-excel.component.html',
  styleUrls: ['./export-awp-excel.component.scss']
})
export class ExportAwpExcelComponent implements OnInit {


  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};

  /**
   * variable
   */
  modelAwp: AwpModel[]
  filterYear: number
  selectedYear: number
  selectedYearRetain;
  start = 1
  dataListYear = []
  dataListYearExist = []
  finishLoadData = true
  finishLoadDataCount = 0
  breadCrumbItems: Array<{}>;
  filterData: any
  paramsLike = []
  paramsIs = []
  paramsIn = []
  idAwpExport = []
  loadingExportExcel = false
  // unit
  checkedComponent = []
  listComponentStaff = []
  componentExist: ComponentModel[]
  checkedComponentWithoutUser = []

  checkedAllAwp = false

  // filter
  selectComponent: string[] = [];
  selectedFilterComponent: string
  selectSubComponent: string[] = [];
  selectedFilterSubComponent: string
  filterSubComponent = []

  constructor(
      private http: HttpClient,
      private service: AwpService,
      private router: Router,
      public roles: AuthfakeauthenticationService,
      private swalAlert: SwalAlert,
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'AWP', active: true }];
    this.getListYear()
    this.getDataTable()
    this.listComponent()
    this.selectedYear = new Date().getFullYear()
    this.listComponent()
  }

  getDataTable() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: false,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: false,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramsIs,
          paramIn: this.paramsIn,
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'awp/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.modelAwp = resp.data.content;
            this.modelAwp.forEach(x => {
              x.checked = false
              if (this.idAwpExport.length > 0) {
                this.idAwpExport.forEach(y => {
                  if (y === x.id) {
                    x.checked = true
                  }
                })
              }
            })
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'createdAt' }, { data: 'component.code' }, { data: 'componentCode.code' }, { data: 'name' }, { data: 'budgetAwp' }, { data: 'description' }, { data: 'purpose' }],
      order: [[2, 'asc']]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.filterYear = this.selectedYear
    this.paramsIs = []
    this.paramsIn = []

    if (this.checkedComponentWithoutUser.length > 0) {
      const selectedComponent = {
        field: "componentId",
        dataType: "string",
        value: this.checkedComponentWithoutUser[0]
      }
      this.paramsIs.push(selectedComponent)
    }

    if (this.filterSubComponent.length > 0) {
      const selectedSubComponent = {
        field: "subComponentId",
        dataType: "string",
        value: this.filterSubComponent[0].id
      }
      this.paramsIs.push(selectedSubComponent)
    }

    if (this.selectedYear) {
      const selectedYearChoosed = {
        field: "year",
        dataType: "int",
        value: this.filterYear
      }
      this.paramsIs.push(selectedYearChoosed)
    } else {
      this.paramsIs = []
    }

    this.paramsLike = []
    if (this.filterData) {
      const paramsFirstName = {
        field: "name",
        dataType: "string",
        value: this.filterData
      }
      const paramsBudegetAwp =  {
        field: "budgetAwp",
        dataType: "int",
        value: this.filterData
      }
      const paramsComponentCode = {
        field: "component.code",
        dataType: "string",
        value: this.filterData
      }
      const paramsComponentDescription = {
        field: "component.description",
        dataType: "string",
        value: this.filterData
      }
      const paramsSubComponentDescription = {
        field: "subComponent.description",
        dataType: "string",
        value: this.filterData
      }
      const paramsDescription = {
        field: "description",
        dataType: "string",
        value: this.filterData
      }
      const paramsPurpose = {
        field: "purpose",
        dataType: "string",
        value: this.filterData
      }
      const paramsCode = {
        field: "componentCode.code",
        dataType: "string",
        value: this.filterData
      }
      this.paramsLike.push(paramsBudegetAwp)
      this.paramsLike.push(paramsFirstName)
      this.paramsLike.push(paramsComponentDescription)
      this.paramsLike.push(paramsSubComponentDescription)
      this.paramsLike.push(paramsDescription)
      this.paramsLike.push(paramsPurpose)
      this.paramsLike.push(paramsCode)
      this.paramsLike.push(paramsComponentCode)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  /**
   * get list year
   */
  getListYear() {
    this.startLoading()
    this.service.listYear().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.dataListYear = []
            this.dataListYear = resp.data
            this.dataListYearExist = resp.data
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
          }
        }, error => {
          // this.finishLoadData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  selectedAwpYear(data) {
    this.selectedYear = data
    this.selectedYearRetain =  data
    this.refreshDatatable()
  }

  clearFilterYear() {
    this.selectedYear = null
    this.selectedYearRetain = null
    this.refreshDatatable()
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.dataListYear = this.dataListYearExist.filter((product) => {
      return product.toString().search(searchStr.toString().toLowerCase()) !== -1
    });
  }

  goToListAWP() {
    this.router.navigate(['admin/awp'])
  }

  selectAllExportAwp() {
    let filteredAllAwp
    if (this.checkedAllAwp) {
      if (this.idAwpExport.length > 0) {
        this.modelAwp.forEach(x => {
          x.checked = true
        })
        filteredAllAwp = this.modelAwp.filter(({ id: id1 }) => !this.idAwpExport.some(id2 => id2 === id1))
        if (filteredAllAwp.length > 0) {
          filteredAllAwp.forEach(x => {
            this.idAwpExport.push(x.id)
          })
        }
      } else {
        this.modelAwp.forEach(x => {
          x.checked = true
          this.idAwpExport.push(x.id)
        })
      }
    } else {
      this.modelAwp.forEach(x => {
        x.checked = false
      })
      this.idAwpExport = []
    }
  }

  exportExcelAwp() {
    this.loadingExportExcel = true
    this.service.exportExcelAwp(this.idAwpExport).subscribe(
        resp => {
          this.loadingExportExcel = false
        }, error => {
          this.loadingExportExcel = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addAwpExportExcel(data) {
    if (data.checked) {
      this.idAwpExport.push(data?.id)
    } else {
      this.idAwpExport.forEach((x, index) => {
        if (x === data.id) {
          this.idAwpExport.splice(index, 1)
          data.checked = false
        }
      })
    }

    console.log(this.idAwpExport)
  }

  listComponent(){
    let tag = []
    this.service.listAllComponent().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data
            this.componentExist.forEach(dataCode => {
              dataCode.checked = false
              dataCode.code = 'Komponen ' + dataCode.code
              this.listComponentStaff.push(dataCode)
              tag.push(dataCode.code)
            })
            this.selectComponent = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeFilterComponent() {
    this.checkedComponentWithoutUser = []
    let tagSubComponent = []
    let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
    let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
    this.selectedFilterSubComponent = null
    if (this.selectedFilterComponent) {
      findComponent = findComponent.replace('komponen ', '')
      filteredComponent = filteredComponent.filter(x => x.code === findComponent)
      if (filteredComponent.length > 0) {
        tagSubComponent = []
        this.checkedComponentWithoutUser.push(filteredComponent[0]?.id)
        filteredComponent[0].subComponent.forEach(x => {
          x.code = 'Sub-Komponen ' + x.code
          tagSubComponent.push(x.code)
        })
        this.selectSubComponent = []
        this.selectSubComponent = tagSubComponent
      }
    }
    this.refreshDatatable()
  }

  changeFilterSubComponent() {
    let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
    let findSubComponent = JSON.parse(JSON.stringify(this.selectedFilterSubComponent))
    let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
    let selectedFilterSubComponent
    if (this.selectedFilterSubComponent) {
      findComponent = findComponent.replace('komponen ', '')
      filteredComponent = filteredComponent.filter(x => x.code === findComponent)
      if (filteredComponent) {
        findSubComponent = findSubComponent.replace('Sub-Komponen ', '')
        selectedFilterSubComponent = filteredComponent[0].subComponent.filter(x =>x.code === findSubComponent)
        this.filterSubComponent = selectedFilterSubComponent
      }
      this.refreshDatatable()
    }
  }

  clearFilter() {
    this.selectedFilterComponent = null
    this.selectedFilterSubComponent = null
    this.selectSubComponent = []
    this.checkedComponentWithoutUser = []
    this.filterSubComponent = []
    this.refreshDatatable()
  }

}
