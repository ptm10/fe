import { Component, OnInit } from '@angular/core';
import {parse} from "date-fns";
import Swal from "sweetalert2";
import {EventModel} from "../../../../../core/models/event.model";
import {EventService} from "../../../../../core/services/Event/event.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {HorizontaltopbarComponent} from "../../../../../layouts/horizontaltopbar/horizontaltopbar.component";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-application-event',
  templateUrl: './detail-application-event.component.html',
  styleUrls: ['./detail-application-event.component.scss']
})
export class DetailApplicationEventComponent implements OnInit {

  idDetailEvent: string
  detailEventModel: EventModel
  firstNameLogged: string
  lastNameLogged: string
  finishLoadDataCount = 0
  finishLoadData = true
  rejectMessage: string
  loadingSaveData = true
  breadCrumbItems: Array<{}>;

  constructor(
      private service: EventService,
      private notification: HorizontaltopbarComponent,
      private route: ActivatedRoute,
      private router: Router,
      private modalService: NgbModal,
      private swalAlert: SwalAlert
  ) {
    this.route.paramMap.subscribe(params => {
      this.ngOnInit()
    })
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Dashboard' }, { label: 'Administrasi' }, { label: 'Persetujuan Kegiatan Luar Kota' }, { label: 'Lihat Detail Persetujuan Kegiatan Luar Kota', active: true}];
    this.idDetailEvent = this.route.snapshot.paramMap.get('id')
    this.firstNameLogged = localStorage.getItem('fullName')
    this.lastNameLogged = localStorage.getItem('lastName')
    this.detailEvent()
  }

  detailEvent() {
    this.startLoading()
    this.service.getDetailEvent(this.idDetailEvent).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.detailEventModel = resp.data
            this.detailEventModel.convertStartActivity = parse(this.detailEventModel.startDate, 'yyyy-MM-dd', new Date());
            this.detailEventModel.convertEndActivity = parse(this.detailEventModel.endDate, 'yyyy-MM-dd', new Date());
            this.detailEventModel.convertUpdatedAt = parse(this.detailEventModel.updatedAt, 'dd-MM-yyyy HH:mm:ss', new Date());
            if (this.detailEventModel.approveStatus == 0) {
              this.updateStatusTask()
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changePageView() {
    this.router.navigate(['administrasi/application-event'])
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'md', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  sendDataListTask(status) {
    let param
    this.loadingSaveData = false
    if (status == 1) {
      param = {
        id: this.idDetailEvent,
        approveStatus: 1,
      }
    } else {
      param = {
        id: this.idDetailEvent,
        approveStatus: 2,
        rejectedMessage: this.rejectMessage
      }
    }
    this.service.approveRejectEvent(param).subscribe (
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.detailEvent()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Terjadi Kesalahan Pada Server',
          });
        }
    )
  }
  updateStatusTask() {
    const params = {
      taskId: this.idDetailEvent,
      taskType: 3,
      status: 1
    }
    this.service.updateStatusTask(params).subscribe(
        resp => {
          if (resp.success) {
            this.notification.getListNotification()
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


}
