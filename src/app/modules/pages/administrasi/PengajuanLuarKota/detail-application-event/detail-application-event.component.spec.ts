import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailApplicationEventComponent } from './detail-application-event.component';

describe('DetailApplicationEventComponent', () => {
  let component: DetailApplicationEventComponent;
  let fixture: ComponentFixture<DetailApplicationEventComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailApplicationEventComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailApplicationEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
