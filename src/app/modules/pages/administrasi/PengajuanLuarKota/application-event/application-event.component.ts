import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {EventModel} from "../../../../../core/models/event.model";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {leangueIndoDT} from "../../../../../shared/pagination-custom";
import {DataResponse} from "../../../../../core/models/dataresponse.models";
import {environment} from "../../../../../../environments/environment";
import {CommonCons} from "../../../../../shared/CommonCons";

@Component({
  selector: 'app-application-event',
  templateUrl: './application-event.component.html',
  styleUrls: ['./application-event.component.scss']
})
export class ApplicationEventComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  firstName: string;
  lastName: string
  filterEvent: string

  start = 1
  finishLoadData = true
  finishLoadDataCount = 0
  listFilter = []
  eventModel: EventModel[]
  breadCrumbItems: Array<{}>;

  constructor(
      private router: Router,
      private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Dashboard' }, { label: 'Administrasi' }, { label: 'Persetujuan Kegiatan Luar Kota', active: true}];
    this.getDataTable()
  }

  getDataTable() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.listFilter,
          paramIs: [],
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverResource + 'out-of-town-event/datatable-supervisior', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.eventModel = resp.data.content

          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
        });
      },
      columns: [{ data: 'id' }, { data: 'event.name' }, { data: 'createdAt' }, { data: 'resources.user.firstName' }, { data: 'daysLength' }, { data: 'approveStatus' }]
    };
  }

  refreshDatatable(): void {
    this.listFilter = []

    const paramsLeaveRequest = {
      field: "event.name",
      dataType: "string",
      value: this.filterEvent
    }

    const paramsFirstName = {
      field: "resources.user.firstName",
      dataType: "string",
      value: this.filterEvent
    }

    const paramsLastName = {
      field: "resources.user.lastName",
      dataType: "string",
      value: this.filterEvent
    }

    if (this.filterEvent) {
      this.listFilter.push(paramsLeaveRequest)
      this.listFilter.push(paramsFirstName)
      this.listFilter.push(paramsLastName)
    } else {
      this.listFilter = []
    }

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  goToDetailApplicationEvent(id) {
    this.router.navigate(['administrasi/detail-application-event/' + id])
  }


  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

}
