import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {leangueIndoDT} from "../../../../../shared/pagination-custom";
import {DataResponse} from "../../../../../core/models/dataresponse.models";
import {environment} from "../../../../../../environments/environment";
import {CommonCons} from "../../../../../shared/CommonCons";
import {DataTableDirective} from "angular-datatables";
import {DetailPermissionModel} from "../../../../../core/models/detail-permission.model";
import {parse} from "date-fns";
import {Router} from "@angular/router";

@Component({
  selector: 'app-application-permission',
  templateUrl: './application-permission.component.html',
  styleUrls: ['./application-permission.component.scss']
})
export class ApplicationPermissionComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  listPermission: DetailPermissionModel[]
  listFilter = []
  start = 1
  finishLoadData = true
  finishLoadDataCount = 0
  filterIzin: string

  breadCrumbItems: Array<{}>;


  constructor(
      private http: HttpClient,
      private router: Router
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Administrasi' }, { label: 'Persetujuan Izin/Cuti', active: true }];
    this.getDataTable()
  }

  getDataTable() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.listFilter,
          paramIs: [],
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverResource + 'leave-request/datatable-supervisior', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.listPermission = resp.data.content;
            this.listPermission.forEach(dataResp => {
              const diffDays = (date, otherDate) => Math.ceil(Math.abs(date - otherDate) / (1000 * 60 * 60 * 24));
              dataResp.convertCreatedAt = parse(dataResp.createdAt, 'dd-MM-yyyy HH:mm:ss', new Date());
              dataResp.convertStartDate = parse(dataResp.startDate, 'yyyy-MM-dd', new Date());
              dataResp.convertEndDate = parse(dataResp.endDate, 'yyyy-MM-dd', new Date());
              dataResp.countDays = diffDays(dataResp.convertStartDate, dataResp.convertEndDate) + 1
            })

          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
        });
      },
      columns: [{ data: 'id' }, { data: 'resources.user.firstName' }, { data: 'startDate' },  { data: 'daysLength' }, { data: 'leaveRequestType.name' }, {data: 'approveStatus'}]
    };
  }

  goToDetailApprovePermission(id) {
    this.router.navigate(['administrasi/detail-approve-permission/' + id])
  }


  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.listFilter = []

    const paramsLeaveRequest = {
      field: "leaveRequestType.name",
      dataType: "string",
      value: this.filterIzin
    }
    const paramsFirstName = {
      field: "resources.user.firstName",
      dataType: "string",
      value: this.filterIzin
    }

    const paramsLastName = {
      field: "resources.user.lastName",
      dataType: "string",
      value: this.filterIzin
    }

    if (this.filterIzin) {
      this.listFilter.push(paramsLeaveRequest)
      this.listFilter.push(paramsFirstName)
      this.listFilter.push(paramsLastName)
    } else {
      this.listFilter = []
    }

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }


  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

}
