import { Component, OnInit } from '@angular/core';
import {NgbDate, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import {DetailPermissionModel} from "../../../../../core/models/detail-permission.model";
import {
  ApplicationPermissionService
} from "../../../../../core/services/Application-permission/application-permission.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FileService} from "../../../../../core/services/Image/file.service";
import {parse} from "date-fns";
import {HorizontaltopbarComponent} from "../../../../../layouts/horizontaltopbar/horizontaltopbar.component";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-approve-permission',
  templateUrl: './detail-approve-permission.component.html',
  styleUrls: ['./detail-approve-permission.component.scss']
})
export class DetailApprovePermissionComponent implements OnInit {

  detailPermissionModel: DetailPermissionModel
  replaceResource: DetailPermissionModel
  finishLoadDataCount = 0
  idPermission: string
  finishLoadData = true
  countDateCalenderWorkDays: number
  countDateCalenderHolidays: number
  countTotalCalender: number
  rejectMessage: string
  loadingSaveData = true
  breadCrumbItems: Array<{}>;


  constructor(
      private service: ApplicationPermissionService,
      private router: ActivatedRoute,
      private serviceImage: FileService,
      private route: Router,
      private modalService: NgbModal,
      private notification: HorizontaltopbarComponent,
      private swalAlert: SwalAlert
  ) {
    this.router.paramMap.subscribe(params => {
      this.ngOnInit()
    })
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Administrasi' }, { label: 'Persetujuan Izin/Cuti'}, { label: 'Lihat Detail Persetujuan Izin/Cuti', active: true}];
    this.idPermission = this.router.snapshot.paramMap.get('id')
    this.detailPermission()
    this.resourceReplacement()
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'md', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  detailPermission() {
    this.startLoading()
    this.service.detailApprovePermission(this.idPermission).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.detailPermissionModel = resp.data
            this.detailPermissionModel.convertStartDate = new Date(this.detailPermissionModel.startDate)
            this.detailPermissionModel.convertEndDate = new Date(this.detailPermissionModel.endDate)
            // this.detailPermissionModel.convertUpdatedAt = new Date(this.detailPermissionModel.updatedAt)
            // 10-02-2022 21:23:42
            this.detailPermissionModel.convertUpdatedAt = parse(this.detailPermissionModel.updatedAt, 'dd-MM-yyyy HH:mm:ss', new Date());
            console.log(this.detailPermissionModel.updatedAt)
            this.countDateCalenderWorkDays = this.detailPermissionModel.worksDays
            this.countDateCalenderHolidays = this.detailPermissionModel.holidays
            this.countTotalCalender = this.detailPermissionModel.daysLength
            if (this.detailPermissionModel.approveStatus == 0) {
              this.updateStatusTask()
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  resourceReplacement() {
    this.startLoading()
    this.service.detailResourceReplacement(this.idPermission).subscribe(
        resp => {
          if (resp.success) {
            this.replaceResource = resp.data
            this.serviceImage.getUrlPhoto(this.replaceResource?.resources?.user?.profilePicture?.id).subscribe(
                resp => {
                  let reader = new FileReader();
                  reader.addEventListener("load", () => {
                    this.replaceResource.filePhoto = reader.result;
                    this.replaceResource.resources.filePhoto = reader.result
                  }, false);
                  reader.readAsDataURL(resp);
                }
            )
          }
          this.stopLoading()
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  sendDataListTask(status) {
    let param
    this.loadingSaveData = false
    if (status == 1) {
      param = {
        id: this.idPermission,
        approveStatus: 1,
      }
    } else {
      param = {
        id: this.idPermission,
        approveStatus: 2,
        rejectedMessage: this.rejectMessage
      }
    }
    this.service.approveRejectPermission(param).subscribe (
        resp => {
          if (resp.success) {
            this.loadingSaveData = true
            this.closeModal()
            this.detailPermission()
            this.resourceReplacement()
          } else {
            this.loadingSaveData = true
          }
        }, error => {
          this.loadingSaveData = true
          Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Terjadi Kesalahan Pada Server',
          });
        }
    )
  }

  updateStatusTask() {
    const params = {
      taskId: this.idPermission,
      taskType: 2,
      status: 1
    }
    this.service.updateStatusTask(params).subscribe(
        resp => {
          if (resp.success) {
            this.notification.getListNotification()
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changePageStatus() {
    this.route.navigate(['administrasi/application'])
  }

}
