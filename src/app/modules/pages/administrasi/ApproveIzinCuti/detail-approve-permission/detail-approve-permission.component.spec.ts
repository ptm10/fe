import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailApprovePermissionComponent } from './detail-approve-permission.component';

describe('DetailApprovePermissionComponent', () => {
  let component: DetailApprovePermissionComponent;
  let fixture: ComponentFixture<DetailApprovePermissionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailApprovePermissionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailApprovePermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
