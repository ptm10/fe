import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import {parse} from "date-fns";
import Swal from "sweetalert2";
import {EventService} from "../../../../../core/services/Event/event.service";
import {ManagementHolidays} from "../../../../../core/models/permission.model";
import {EventListActivityModel} from "../../../../../core/models/event-list-activity.model";
import {EventModel} from "../../../../../core/models/event.model";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-event',
  templateUrl: './detail-event.component.html',
  styleUrls: ['./detail-event.component.scss']
})
export class DetailEventComponent implements OnInit {

  changePage: number

  disabledDates:NgbDateStruct[] = []
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  startDate: Date
  endDate: Date
  hoveredDate: NgbDate | null = null;
  minDate = undefined;
  listHolidays = []
  holidayManagement: ManagementHolidays[]
  listEventActivity: EventListActivityModel[]
  listEventActivityExist: EventListActivityModel[]
  selectedEventActivity: any
  countDateCalenderWorkDays: number
  countTotalCalender: number
  idDetailEvent: string
  detailEventModel: EventModel
  firstNameLogged: string
  lastNameLogged: string
  finishLoadDataCount = 0
  finishLoadData = true
  model: NgbDateStruct;
  date: {year: number, month: number};
  breadCrumbItems: Array<{}>;

  time = {hour: 13, minute: 30};
  public selectedStartDate = new Date();
  public selectedEndDate = new Date();
  public validateDate = new Date()

  constructor(
      private router: Router,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private service: EventService,
      private route: ActivatedRoute,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Dashboard' }, { label: 'Administrasi' }, { label: 'Kegiatan Luar Kota' }, { label: 'Lihat Detail Kegiatan Luar Kota', active: true }];
    this.idDetailEvent = this.route.snapshot.paramMap.get('id')
    this.firstNameLogged = localStorage.getItem('fullName')
    this.lastNameLogged = localStorage.getItem('lastName')
    this.changePage = 1
    this.fromDate = null
    this.toDate = null;
    this.getListHolidayManagement()
    this.getListEventActivity()
    this.detailEvent()
  }

  changePageView() {
    if (this.changePage == 1) {
      this.router.navigate(['administrasi/event'])
    } else if (this.changePage == 2) {
      this.changePage = 1
    }
  }

  selectedWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      this.startDate = new Date(this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'))
      this.endDate = new Date(this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'))
      // this.countDatesRangeWithoutWeekends(this.startDate, this.endDate)
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  getListHolidayManagement() {
    this.listHolidays = []
    this.service.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListEventActivity() {
    this.service.listEventActivity().subscribe(
        resp => {
          if (resp.success) {
            this.listEventActivity = resp.data.content
            this.listEventActivityExist = resp.data.content
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeEventActivity(data) {
    this.selectedEventActivity = null
    this.listEventActivity = this.listEventActivityExist

    this.selectedEventActivity = data
    this.listEventActivity = this.listEventActivity.filter(dataFilter => dataFilter.id != this.selectedEventActivity.id )
  }

  searchFilterActivityEvent(e) {
    const searchStr = e.target.value
    this.listEventActivity = this.listEventActivityExist.filter((product) => {
      console.log((product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
      return (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  saveEventActivity() {
    console.log(this.selectedEndDate)
    console.log(this.selectedStartDate)
  }

  goToAdministrasiEvent() {
    this.router.navigate(['administrasi/event'])
  }

  goToDetailUpdate() {
    this.changePage = 2
  }

  detailEvent() {
    this.startLoading()
    this.service.getDetailEvent(this.idDetailEvent).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.detailEventModel = resp.data
            this.detailEventModel.convertStartActivity = parse(this.detailEventModel.startDate, 'yyyy-MM-dd', new Date());
            this.detailEventModel.convertEndActivity = parse(this.detailEventModel.endDate, 'yyyy-MM-dd', new Date());
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }
}
