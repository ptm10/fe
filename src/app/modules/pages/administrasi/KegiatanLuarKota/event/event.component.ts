import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DataTableDirective} from "angular-datatables";
import {leangueIndoDT} from "../../../../../shared/pagination-custom";
import {DataResponse} from "../../../../../core/models/dataresponse.models";
import {environment} from "../../../../../../environments/environment";
import {CommonCons} from "../../../../../shared/CommonCons";
import {HttpClient} from "@angular/common/http";
import {EventModel} from "../../../../../core/models/event.model";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  firstName: string;
  lastName: string
  filterEvent: string
  breadCrumbItems: Array<{}>;

  start = 1
  finishLoadData = true
  finishLoadDataCount = 0
  listFilter = []
  eventModel: EventModel[]

  constructor(
      private router: Router,
      private http: HttpClient,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Dashboard' }, { label: 'Administrasi' }, { label: 'Kegiatan Luar Kota', active: true}];
    this.getDataTable()
  }

  getDataTable() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.listFilter,
          paramIs: [],
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverResource + 'out-of-town-event/datatable-staff', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.eventModel = resp.data.content

          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
        });
      },
      columns: [{ data: 'id' }, { data: 'event?.name' }, { data: 'createdAt' }, { data: 'resources?.user?.firstName' }, { data: 'daysLength' }, { data: 'approveStatus' }]
    };
  }

  goToAddEvent() {
    this.router.navigate(['administrasi/add-event'])
  }

  goToDetailEvent(data) {
    this.router.navigate(['administrasi/detail-event/' + data])
  }

  refreshDatatable(): void {
    this.listFilter = []

    const paramsLeaveRequest = {
      field: "event.name",
      dataType: "string",
      value: this.filterEvent
    }

    const paramsFirstName = {
      field: "resources.user.firstName",
      dataType: "string",
      value: this.filterEvent
    }

    const paramsLastName = {
      field: "resources.user.lastName",
      dataType: "string",
      value: this.filterEvent
    }

    if (this.filterEvent) {
      this.listFilter.push(paramsLeaveRequest)
      this.listFilter.push(paramsLastName)
      this.listFilter.push(paramsFirstName)
    } else {
      this.listFilter = []
    }

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }


  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

}
