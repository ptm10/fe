import { Component, OnInit } from '@angular/core';
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
import Swal from "sweetalert2";
import {parse} from "date-fns";
import {EventService} from "../../../../../core/services/Event/event.service";
import {ManagementHolidays} from "../../../../../core/models/permission.model";
import {EventListActivityModel, Transportation} from "../../../../../core/models/event-list-activity.model";
import {Router} from "@angular/router";
import {resourcesModel} from "../../../../../core/models/resources.model";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnInit {

  disabledDates:NgbDateStruct[] = []
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  startDate: Date
  endDate: Date
  hoveredDate: NgbDate | null = null;
  minDate = undefined;
  listHolidays = []
  holidayManagement: ManagementHolidays[]
  startTransportation: Date
  endTransportation: Date

  listEventActivity: EventListActivityModel[]
  listEventActivityExist: EventListActivityModel[]
  listEventSelected = []
  selectedEventActivity: any

  listAllUserResources: resourcesModel[]
  listAllUserResourcesExist: resourcesModel[]
  listUserSelected = []
  selectedUsersResources = []

  listTransportation: Transportation[]
  listTransportationExist: Transportation[]
  selectedlistTransportation: any

  countDateCalenderWorkDays: number
  countTotalCalender: number
  firstNameLogged: string
  lastNameLogged: string
  originTransportation: string
  destinationTransportation: string
  descriptionDestination: string
  addressDestination: string
  transportationCode: string
  diffDays = (date, otherDate) => Math.ceil(Math.abs(date - otherDate) / (1000 * 60 * 60 * 24));

  finishLoadDataCount = 0
  finishLoadData = true
  successSaveNotif = false

  model: NgbDateStruct;
  date: {year: number, month: number};

  time = {hour: 13, minute: 30};
  //form add event
  selectedStartDate: Date
  selectedEndDate: Date
  public validateDate = new Date()
  validateDateStartTransportation: Date
  validateDateActivity: Date
  public endValidateDate = new Date()
  countDates: any
  loadingSave = false
  breadCrumbItems: Array<{}>;


  constructor(
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private service: EventService,
      private router: Router,
      private swalAlert: SwalAlert
  ) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate()
    };
  }

  selectToday() {
    this.model = this.calendar.getToday();
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Administrasi' }, { label: 'Kegiatan Luar kota' }, { label: 'Ajukan Kegiatan Luar kota', active: true }];
    this.successSaveNotif = false
    this.firstNameLogged = localStorage.getItem('fullName')
    this.lastNameLogged = localStorage.getItem('lastName')
    this.fromDate = null
    this.toDate = null;
    this.getListHolidayManagement()
    this.getListEventActivity()
    this.getListUsersResources()
    this.addListTransportation()
  }

  selectedWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.selectedStartDate = null
      this.selectedEndDate = null
      this.toDate = date;
      this.startDate = new Date(this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'))
      this.startDate.setHours(23)
      this.startDate.setMinutes(59);
      this.startDate.setSeconds(0);
      this.validateDateActivity = new Date(this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'))
      this.validateDateActivity.setHours(23)
      this.validateDateActivity.setMinutes(59);
      this.validateDateActivity.setSeconds(0);
      this.endDate = new Date(this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'))
      this.endDate.setHours(23)
      this.endDate.setMinutes(59);
      this.endDate.setSeconds(0);

      this.countDates = this.diffDays(this.startDate, this.endDate) + 1
      this.startTransportation = this.startDate
      this.endTransportation = this.endDate
      if (this.validateDateActivity.getDate() - 3 > this.validateDate.getDate()) {
        this.validateDateStartTransportation = new Date (this.validateDateActivity.setDate(this.validateDateActivity.getDate() - 3))
      } else {
        this.validateDateStartTransportation = new Date()
      }
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  goToAdministrasiEvent() {
    this.router.navigate(['administrasi/event'])
  }

  getListHolidayManagement() {
    this.startLoading()
    this.listHolidays = []
    this.service.listHolidayManagement().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListEventActivity() {
    this.startLoading()
    this.service.listEventActivity().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listEventActivity = resp.data.content
            this.listEventActivityExist = resp.data.content
            this.listEventSelected = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListUsersResources() {
    this.startLoading()
    this.service.listAllUserResources().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listAllUserResources = resp.data.content
            this.listAllUserResourcesExist = resp.data.content
            this.listUserSelected = resp.data.content
            console.log(this.listAllUserResources)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  addListTransportation() {
    this.startLoading()
    this.service.listTransportation().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listTransportation = resp.data.content
            this.listTransportationExist = resp.data.content
            console.log(this.listTransportation)
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeEventActivity(data) {
    this.selectedEventActivity = null
    this.listEventActivity = this.listEventActivityExist

    this.selectedEventActivity = data
    this.listEventActivity = this.listEventActivityExist.filter(dataFilter => dataFilter.id != this.selectedEventActivity.id )
    this.listEventSelected = this.listEventActivity
  }

  searchFilterActivityEvent(e) {
    const searchStr = e.target.value

    let filter = this.listEventActivityExist
    if (this.selectedEventActivity) {
      filter = filter.filter(x => x.id !== this.selectedEventActivity.id)
    } else {
      filter = this.listEventActivityExist
    }

    this.listEventActivity = filter.filter((product) => {
      return (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }

  changeUserResources(data) {
    this.selectedUsersResources.push(data)
    this.selectedUsersResources.forEach(dataSelected => {
      this.listAllUserResourcesExist = this.listAllUserResourcesExist.filter(dataFilter => dataFilter.id !== dataSelected.id)
    })
    this.listUserSelected = this.listAllUserResourcesExist
  }

  deleteChangeUserResources(data) {
    let filter = this.listAllUserResources
    this.selectedUsersResources.forEach((existing, index) => {
      if (existing.id == data.id) {
        this.selectedUsersResources.splice(index, 1)
      }
    })
    this.selectedUsersResources.forEach(data => {
      filter = filter.filter(x => x.id != data.id)
    })
    this.listAllUserResourcesExist = filter
  }

  searchFilterUserResources(e) {
    let filter = this.listAllUserResources
    const searchStr = e.target.value
    this.selectedUsersResources.forEach(data => {
      filter = filter.filter(x => x.id !== data.id)
    })
    this.listAllUserResourcesExist = filter.filter((product) => {
      return (product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
    console.log(this.listAllUserResourcesExist)
  }


  changeTransportation(data) {
    this.selectedlistTransportation = null
    this.listTransportation = this.listTransportationExist

    this.selectedlistTransportation = data
    this.listTransportation = this.listTransportation.filter(dataFilter => dataFilter.id != this.selectedlistTransportation.id )
  }

  searchFilterTransportation(e) {
    const searchStr = e.target.value

    let filter = this.listTransportationExist
    if (this.selectedlistTransportation) {
      filter = filter.filter(x => x.id !== this.selectedlistTransportation.id)
    } else {
      filter = this.listTransportationExist
    }

    this.listTransportation = filter.filter((product) => {
      return (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
  }


  saveEventActivity() {
    this.loadingSave = true
    let resourceSelected = []

    if (this.selectedUsersResources) {
      this.selectedUsersResources.forEach(data => {
        resourceSelected.push(data.id)
      })
    } else {
      resourceSelected = []
    }

    if (!this.transportationCode) {
      this.transportationCode = null
    }

    const monthStartTransportation = this.selectedStartDate.getMonth() + 1
    const monthEndTransportation = this.selectedEndDate.getMonth() + 1
    const monthStartDate = this.startDate.getMonth() + 1
    const monthEndDate = this.endDate.getMonth() + 1

    const startTransportation = this.selectedStartDate.getFullYear().toString() + '-' + monthStartTransportation.toString().padStart(2, '0') + '-' + this.selectedStartDate.getDate().toString().padStart(2, '0')+ ' ' + this.selectedStartDate.getHours().toString().padStart(2, '0') + ':' + this.selectedStartDate.getMinutes().toString().padStart(2,'0')
    const endTransportation = this.selectedEndDate.getFullYear().toString() + '-' + monthEndTransportation.toString().padStart(2, '0') + '-' + this.selectedEndDate.getDate().toString().padStart(2, '0') + ' ' + this.selectedEndDate.getHours().toString().padStart(2, '0') + ':' + this.selectedEndDate.getMinutes().toString().padStart(2,'0')
    const startEventActivity = this.startDate.getFullYear().toString() + '-' + monthStartDate.toString().padStart(2, '0') + '-' + this.startDate.getDate().toString().padStart(2, '0')
    const endEventActivity = this.endDate.getFullYear().toString() + '-' + monthEndDate.toString().padStart(2, '0') + '-' + this.endDate.getDate().toString().padStart(2, '0')

    const saveEvent = {
      startDate: startEventActivity,
      endDate: endEventActivity,
      eventId: this.selectedEventActivity.id,
      description: this.descriptionDestination,
      address: this.addressDestination,
      transportationId: this.selectedlistTransportation.id,
      origin: this.originTransportation,
      destination: this.destinationTransportation,
      transportationStartTime: startTransportation,
      transportationEndTime: endTransportation,
      resourcesIds: resourceSelected,
      transportationCode: this.transportationCode
    }
    this.service.saveEvent(saveEvent).subscribe(
        resp => {
          if (resp.success) {
            this.successSaveNotif = true
            this.loadingSave = false
            this.holidayManagement = []
            this.listHolidays = []
            this.selectedUsersResources=[]
            this.router.navigate(['administrasi/event'])
          }
        }, error => {
          this.loadingSave = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }
}
