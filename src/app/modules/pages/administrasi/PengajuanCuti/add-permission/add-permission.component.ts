import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AdministrationService} from "../../../../../core/services/Administration/administration.service";
import Swal from "sweetalert2";
import {CheckDate, LeaveRequestType, ManagementHolidays} from "../../../../../core/models/permission.model";
import {resourcesModel} from "../../../../../core/models/resources.model";
import {
  NgbDate,
  NgbCalendar,
  NgbDateParserFormatter,
  NgbDatepickerConfig,
  NgbDateStruct
} from '@ng-bootstrap/ng-bootstrap';
import {FileService} from "../../../../../core/services/Image/file.service";
import {parse} from "date-fns";
import {SwalAlert} from "../../../../../core/helpers/Swal";
@Component({
  selector: 'app-add-permission',
  templateUrl: './add-permission.component.html',
  styleUrls: ['./add-permission.component.scss']
})
export class AddPermissionComponent implements OnInit {

  leaveRequestType: LeaveRequestType[]
  specialLeave = []
  notSpecialLeave = []
  checkedLeaveRequest: LeaveRequestType = null
  checkedSpecialLeave: LeaveRequestType = null
  resourcesReplacement: resourcesModel[]
  listResourceReplacementExist: resourcesModel[]
  descriptionPermission: string
  addressPermission: string
  validateDate = false
  finishLoadData = true
  finishLoadDataCount = 0
  checkValidateDate: CheckDate
  hoveredDate: NgbDate | null = null;
  successPermission = false
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  diffDays = (date, otherDate) => Math.ceil(Math.abs(date - otherDate) / (1000 * 60 * 60 * 24));

  startDate: Date
  endDate: Date
  countDate: any
  countDateCalenderWorkDays: number
  countDateCalenderHolidays: number
  countTotalCalender: number
  selectedResourceModel: resourcesModel
  loadingSubmitPermission = false
  minDate = undefined;
  holidayManagement: ManagementHolidays[]
  listHolidays = []
  disabledDates:NgbDateStruct[] = []
  loadingGetListHolidays = false
  checkedUser = []

  breadCrumbItems: Array<{}>;


  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6

  constructor(
      private router: Router,
      private service: AdministrationService,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private serviceImage: FileService,
      private swalAlert: SwalAlert
  ) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate()
    };
  }


  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Dashboard' }, { label: 'Administrasi' }, { label: 'Izin/Cuti' }, { label: 'Ajukan Izin/Cuti', active: true }];
    this.successPermission = false
    this.getListRequestType()
    this.listReplacementResources()
    this.fromDate = null
    this.toDate = null;
    this.getListHolidayManagement()
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      this.startDate = new Date(this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'))
      this.endDate = new Date(this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'))
      this.countDate = this.diffDays(this.startDate, this.endDate)
      this.loadingGetListHolidays = true

      if (this.checkedLeaveRequest.name == 'Cuti/Izin Khusus') {
        const params = {
          startDate: this.startDate,
          endDate: this.endDate,
          leaveRequestTypeId: this.checkedSpecialLeave.id,
        }
        this.service.checkValidateDate(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingGetListHolidays = false
                this.checkValidateDate = resp.data
                this.countDateCalenderWorkDays = this.checkValidateDate.worksDaysCount
                this.countDateCalenderHolidays = this.checkValidateDate.holidaysCount
                this.countTotalCalender = this.checkValidateDate.totalLeaveDays
              }
            }, error => {
              this.loadingGetListHolidays = false
              if (error?.error?.data) {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.data[0]?.message,
                });
              } else {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.message,
                });
              }
            }
        )
      } else {
        const params = {
          startDate: this.startDate,
          endDate: this.endDate,
          leaveRequestTypeId: this.checkedLeaveRequest.id,
        }
        this.service.checkValidateDate(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingGetListHolidays = false
                this.checkValidateDate = resp.data
                this.countDateCalenderWorkDays = this.checkValidateDate.worksDaysCount
                this.countDateCalenderHolidays = this.checkValidateDate.holidaysCount
                this.countTotalCalender = this.checkValidateDate.totalLeaveDays
              }
            }, error => {
              this.loadingGetListHolidays = false
              Swal.fire({
                position: 'center',
                icon: 'error',
                title: error.data[0]?.message,
              });
            }
        )
      }
      // this.f(this.startDate, this.endDate)
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  goToListPermission() {
    this.router.navigate(['administrasi/permission'])
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  getListRequestType() {
    this.startLoading()
    this.specialLeave = []
    this.notSpecialLeave = []
    this.service.leaveRequestType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.leaveRequestType = resp.data.content
            this.leaveRequestType.forEach(leaveRequest => {
              if (leaveRequest.special) {
                this.specialLeave.push(leaveRequest)
              } else if (!leaveRequest.special) {
                this.notSpecialLeave.push(leaveRequest)
              }
            })
            this.notSpecialLeave.push({
              id: '1',
              name: 'Cuti/Izin Khusus',
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  listReplacementResources() {
    this.startLoading()
    this.service.resourcesReplacement().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.resourcesReplacement = resp.data.content
            this.listResourceReplacementExist = resp.data.content
            this.checkedUser = resp.data.content
            console.log(this.resourcesReplacement)
            this.resourcesReplacement.forEach(dataResource => {
              this.serviceImage.getUrlPhoto(dataResource?.user?.profilePicture?.id).subscribe(
                  resp => {
                    let reader = new FileReader();
                    reader.addEventListener("load", () => {
                      dataResource.filePhoto = reader.result;
                    }, false);
                    reader.readAsDataURL(resp);
                  }
              )
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  savePermission() {
    this.loadingSubmitPermission = true
    this.validateDate = false
    let requestType: string
    let longDatePermission: number = this.countTotalCalender
    if (this.checkedLeaveRequest.name == 'Cuti/Izin Khusus') {
      const params = {
        startDate: this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'),
        endDate: this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'),
        leaveRequestTypeId: this.checkedSpecialLeave.id,
        description: this.descriptionPermission,
        address: this.addressPermission,
        resourcesReplacement: this.selectedResourceModel.id
      }
      if (longDatePermission <=  this.checkedSpecialLeave.maxDayLength || this.checkedSpecialLeave.maxDayLength == null) {
        this.service.submitLeaveRequest(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingSubmitPermission = false
                this.successPermission = true
              } else {
                this.loadingSubmitPermission = false
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: 'terjadi kesalahan',
                });
              }
            }, error => {
              this.loadingSubmitPermission = false
              if (error?.data) {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.data[0]?.message,
                });
              } else {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.message,
                });
              }
            }
        )
      } else {
        this.loadingSubmitPermission = false
        this.validateDate = true
      }
    } else {
      const params = {
        startDate: this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'),
        endDate: this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'),
        leaveRequestTypeId: this.checkedLeaveRequest.id,
        description: this.descriptionPermission,
        address: this.addressPermission,
        resourcesReplacement: this.selectedResourceModel.id
      }
      if ( longDatePermission <= this.checkedLeaveRequest.maxDayLength || this.checkedLeaveRequest.maxDayLength == null) {
        this.service.submitLeaveRequest(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingSubmitPermission = false
                this.successPermission = true
              } else {
                this.loadingSubmitPermission = false
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: 'terjadi kesalahan',
                });
              }
            }, error => {
              this.loadingSubmitPermission = false
              if (error?.data) {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.data[0]?.message,
                });
              } else {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.message,
                });
              }
            }
        )
      } else {
        this.loadingSubmitPermission = false
        this.validateDate = true
      }
    }
  }

  changeDateFromNumberDates(e) {
    // console.log(e.target.value)
    let changeDate = Number(e.target.value)
    if (changeDate == 0) {
      this.toDate = this.calendar.getNext(this.fromDate, 'd', 1);
    } else {
      this.toDate = this.calendar.getNext(this.fromDate, 'd', 0);
      this.toDate = this.calendar.getNext(this.toDate, 'd', changeDate);
    }
  }

  searchFilter(e) {
    const searchStr = e.target.value

    let filter = this.listResourceReplacementExist
    if (this.selectedResourceModel) {
      filter = filter.filter(data => data.id != this.selectedResourceModel.id)
    } else {
      filter = this.listResourceReplacementExist
    }

    this.resourcesReplacement = filter.filter((product) => {
      return (product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
    this.checkedUser = this.resourcesReplacement
  }

  selectResourcePlacement(data) {
    this.selectedResourceModel = null
    this.resourcesReplacement = this.listResourceReplacementExist

    this.selectedResourceModel = data
    this.resourcesReplacement = this.resourcesReplacement.filter(dataFilter => dataFilter.id != this.selectedResourceModel.id )
    this.checkedUser = this.resourcesReplacement
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  getListHolidayManagement() {
    this.listHolidays = []
    this.service.listHolidayManagement().subscribe(
        resp => {
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }



}
