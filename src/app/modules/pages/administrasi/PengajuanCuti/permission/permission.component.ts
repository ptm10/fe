import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {leangueIndoDT} from "../../../../../shared/pagination-custom";
import {DataResponse} from "../../../../../core/models/dataresponse.models";
import {environment} from "../../../../../../environments/environment";
import {CommonCons} from "../../../../../shared/CommonCons";
import {Route, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {PermissionModel} from "../../../../../core/models/permission.model";

@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss']
})
export class PermissionComponent implements OnInit {


  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  firstName: string;
  lastName: string

  start = 1
  finishLoadData = true
  finishLoadDataCount = 0
  permissionModel: PermissionModel[]
  listFilter = []
  filterIzin: string
  breadCrumbItems: Array<{}>;

  constructor(
      private route: Router,
      private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Dashboard' }, { label: 'Administrasi' }, { label: 'Izin/Cuti' , active: true}];
    this.getDataTable()
  }

  getDataTable() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.listFilter,
          paramIs: [],
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverResource + 'leave-request/datatable-staff', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.permissionModel = resp.data.content;
            this.permissionModel.forEach(dataResp => {
              dataResp.convertStartDate = new Date (dataResp.startDate)
              const diffDays = (date, otherDate) => Math.ceil(Math.abs(date - otherDate) / (1000 * 60 * 60 * 24));
              dataResp.countDays = diffDays(new Date(dataResp.startDate), new Date (dataResp.endDate))
            })
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
        });
      },
      columns: [{ data: 'id' }, { data: 'startDate' }, { data: 'daysLength' }, { data: 'leaveRequestType.name' }, {data: 'approveStatus'}]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.listFilter = []

    const paramsLeaveRequest = {
      field: "leaveRequestType.name",
      dataType: "string",
      value: this.filterIzin
    }

    if (this.filterIzin) {
      this.listFilter.push(paramsLeaveRequest)
    } else {
      this.listFilter = []
    }

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }


  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  goToAddPermission() {
    this.route.navigate(['administrasi/add'])
  }

  goToDetailPermission(id) {
    console.log(id)
    this.route.navigate(['administrasi/detail-permission/'+ id])
  }

}
