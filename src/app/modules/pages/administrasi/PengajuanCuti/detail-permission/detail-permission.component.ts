import { Component, OnInit } from '@angular/core';
import {DetailPermissionModel} from "../../../../../core/models/detail-permission.model";
import {AdministrationService} from "../../../../../core/services/Administration/administration.service";
import {ActivatedRoute, Router} from "@angular/router";
import Swal from "sweetalert2";
import {LoadImageService} from "ngx-image-cropper/lib/services/load-image.service";
import {FileService} from "../../../../../core/services/Image/file.service";
import {NgbCalendar, NgbDate, NgbDateParserFormatter, NgbDateStruct, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {parse} from "date-fns";
import {CheckDate, LeaveRequestType, ManagementHolidays} from "../../../../../core/models/permission.model";
import {resourcesModel} from "../../../../../core/models/resources.model";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-detail-permission',
  templateUrl: './detail-permission.component.html',
  styleUrls: ['./detail-permission.component.scss']
})
export class DetailPermissionComponent implements OnInit {

  detailPermissionModel: DetailPermissionModel
  idPermission: string
  loadingDetailPermission = false
  replaceResource: DetailPermissionModel
  changePage: number
  checkedLeaveRequest: any
  checkedSpecialLeave: any
  editPermissionModel: DetailPermissionModel
  listResourceReplacementExist: resourcesModel[]
  leaveRequestType: LeaveRequestType[]
  loadingSubmitPermission = false

  specialLeave = []
  notSpecialLeave = []
  selectedResourceModel: resourcesModel
  finishLoadDataCount = 0
  validateDate = false
  loadingDeletePermission = false
  breadCrumbItems: Array<{}>;


  // calender
  isWeekend = (date: NgbDate) => this.calendar.getWeekday(date) >= 6
  minDate = undefined;
  disabledDates:NgbDateStruct[] = []
  listHolidays = []
  holidayManagement: ManagementHolidays[]
  hoveredDate: NgbDate | null = null;
  successPermission = false
  fromDate: NgbDate | null;
  toDate: NgbDate | null;
  startDate: Date
  endDate: Date
  loadingGetListHolidays = false
  finishLoadData = true


  checkValidateDate: CheckDate
  countDateCalenderWorkDays: number
  countDateCalenderHolidays: number
  countTotalCalender: number
  resourcesReplacement: resourcesModel[]
  selectedResources = []

  constructor(
      private service: AdministrationService,
      private route: ActivatedRoute,
      private router: Router,
      private serviceImage: FileService,
      private calendar: NgbCalendar,
      public formatter: NgbDateParserFormatter,
      private modalService: NgbModal,
      private swalAlert: SwalAlert
  ) {
    const current = new Date();
    this.minDate = {
      year: current.getFullYear(),
      month: current.getMonth() + 1,
      day: current.getDate()
    };
  }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Dashboard' }, { label: 'Administrasi' }, { label: 'Izin/Cuti' }, { label: 'Lihat Detail Izin/Cuti', active: true }];
    this.changePage = 1
    this.idPermission = this.route.snapshot.paramMap.get('id')
    this.detailPermission()
    this.resourceReplacement()
    this.getListHolidayManagement()
    this.getListRequestType()
    this.listReplacementResources()
  }

  myClass(date:NgbDateStruct) {
    let isSelected=this.disabledDates
        .find(d=>d.year==date.year && d.month==date.month && d.day==date.day)
    return isSelected?'classSelected':'classNormal'
  }

  goToListPermission() {
    this.router.navigate(['administrasi/permission'])
  }

  changePageView() {
    this.changePage = 2
  }


  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate) || date.equals(this.fromDate)) {
      this.toDate = date;
      this.startDate = new Date(this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'))
      this.endDate = new Date(this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'))
      this.loadingGetListHolidays = true

      if (this.checkedLeaveRequest === '1') {
        const params = {
          startDate: this.startDate,
          endDate: this.endDate,
          leaveRequestTypeId: this.checkedSpecialLeave,
        }
        this.service.checkValidateDate(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingGetListHolidays = false
                this.checkValidateDate = resp.data
                this.countDateCalenderWorkDays = this.checkValidateDate.worksDaysCount
                this.countDateCalenderHolidays = this.checkValidateDate.holidaysCount
                this.countTotalCalender = this.checkValidateDate.totalLeaveDays
              }
            }, error => {
              this.loadingGetListHolidays = false
              if (error?.data) {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.data[0]?.message,
                });
              } else {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.message,
                });
              }
            }
        )
      } else {
        const params = {
          startDate: this.startDate,
          endDate: this.endDate,
          leaveRequestTypeId: this.checkedLeaveRequest,
        }
        this.service.checkValidateDate(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingGetListHolidays = false
                this.checkValidateDate = resp.data
                this.countDateCalenderWorkDays = this.checkValidateDate.worksDaysCount
                this.countDateCalenderHolidays = this.checkValidateDate.holidaysCount
                this.countTotalCalender = this.checkValidateDate.totalLeaveDays
              }
            }, error => {
              this.loadingGetListHolidays = false
              if (error?.data) {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.data[0]?.message,
                });
              } else {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.message,
                });
              }
            }
        )
      }
      // this.countDatesRangeWithoutWeekends(this.startDate, this.endDate)
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }


  detailPermission() {
    this.startLoading()
    this.service.detailPermission(this.idPermission).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.detailPermissionModel = resp.data
            this.editPermissionModel = resp.data
            this.detailPermissionModel.convertStartDate = new Date(this.detailPermissionModel.startDate)
            this.detailPermissionModel.convertEndDate = new Date(this.detailPermissionModel.endDate)
            // this.detailPermissionModel.convertUpdatedAt = new Date(this.detailPermissionModel.convertUpdatedAt)
            this.detailPermissionModel.convertUpdatedAt = parse(this.detailPermissionModel.updatedAt, 'dd-MM-yyyy HH:mm:ss', new Date());
            this.fromDate = new NgbDate(this.detailPermissionModel.convertStartDate.getFullYear(), this.detailPermissionModel.convertStartDate.getMonth() + 1, this.detailPermissionModel.convertStartDate.getDate())
            this.toDate = new NgbDate(this.detailPermissionModel.convertEndDate.getFullYear(), this.detailPermissionModel.convertEndDate.getMonth() + 1, this.detailPermissionModel.convertEndDate.getDate())
            if (this.detailPermissionModel?.leaveRequestType?.special) {
              this.checkedLeaveRequest = '1'
              this.checkedSpecialLeave = this.detailPermissionModel.leaveRequestType.id
            } else {
              this.checkedLeaveRequest = this.detailPermissionModel.leaveRequestType.id
              this.checkedSpecialLeave = null
            }
            this.countDateCalenderWorkDays = this.detailPermissionModel.worksDays
            this.countDateCalenderHolidays = this.detailPermissionModel.holidays
            this.countTotalCalender = this.detailPermissionModel.daysLength
          }
        }, error => {
          this.loadingDetailPermission = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  resourceReplacement() {
    this.startLoading()
    this.service.detailResourceReplacement(this.idPermission).subscribe(
        resp => {
          if (resp.success) {
            this.replaceResource = resp.data
            this.selectedResources = resp.data
            this.serviceImage.getUrlPhoto(this.replaceResource?.resources?.user?.profilePicture?.id).subscribe(
                resp => {
                  let reader = new FileReader();
                  reader.addEventListener("load", () => {
                    this.replaceResource.filePhoto = reader.result;
                    this.replaceResource.resources.filePhoto = reader.result
                  }, false);
                  reader.readAsDataURL(resp);
                }
            )
            this.selectedResourceModel = this.replaceResource.resources
          }
          this.stopLoading()
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListHolidayManagement() {
    this.startLoading()
    this.listHolidays = []
    this.service.listHolidayManagement().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.holidayManagement = resp.data.content
            this.holidayManagement.forEach(respHolidays => {
              respHolidays.convertDate =  parse(respHolidays.holidayDate, 'yyyy-MM-dd', new Date());
              this.disabledDates.push(
                  {year:respHolidays.convertDate.getFullYear(),month:respHolidays.convertDate.getMonth() + 1,day:respHolidays.convertDate.getDate()}
              )
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectResourcePlacement(data) {
    this.selectedResourceModel = null
    this.resourcesReplacement = this.listResourceReplacementExist

    this.selectedResourceModel = data
    this.resourcesReplacement = this.resourcesReplacement.filter(dataFilter => dataFilter.id != this.selectedResourceModel.id )
    this.selectedResources = this.resourcesReplacement
  }

  listReplacementResources() {
    this.startLoading()
    this.service.resourcesReplacement().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.resourcesReplacement = resp.data.content
            this.resourcesReplacement = this.resourcesReplacement.filter(dataFilter => dataFilter.id != this.selectedResourceModel.id )
            console.log(this.resourcesReplacement)
            this.listResourceReplacementExist = resp.data.content
            this.resourcesReplacement.forEach(dataResource => {
              this.serviceImage.getUrlPhoto(dataResource?.user?.profilePicture?.id).subscribe(
                  resp => {
                    let reader = new FileReader();
                    reader.addEventListener("load", () => {
                      dataResource.filePhoto = reader.result;
                    }, false);
                    reader.readAsDataURL(resp);
                  }
              )
            })
            this.listResourceReplacementExist.forEach(dataResource => {
              this.serviceImage.getUrlPhoto(dataResource?.user?.profilePicture?.id).subscribe(
                  resp => {
                    let reader = new FileReader();
                    reader.addEventListener("load", () => {
                      dataResource.filePhoto = reader.result;
                    }, false);
                    reader.readAsDataURL(resp);
                  }
              )
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getListRequestType() {
    this.startLoading()
    this.specialLeave = []
    this.notSpecialLeave = []
    this.service.leaveRequestType().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.leaveRequestType = resp.data.content
            this.leaveRequestType.forEach(leaveRequest => {
              if (leaveRequest.special) {
                this.specialLeave.push(leaveRequest)
              } else if (!leaveRequest.special) {
                this.notSpecialLeave.push(leaveRequest)
              }
            })
            this.notSpecialLeave.push({
              id: '1',
              name: 'Cuti/Izin Khusus',
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  searchFilter(e) {
    const searchStr = e.target.value

    let filter = this.listResourceReplacementExist
    if (this.selectedResourceModel) {
      filter = filter.filter(data => data.id != this.selectedResourceModel.id)
    } else {
      filter = this.listResourceReplacementExist
    }

    this.resourcesReplacement = filter.filter((product) => {
      return (product?.user?.firstName.toLowerCase().search(searchStr.toLowerCase()) !== -1 || product?.user?.lastName.toLowerCase().search(searchStr.toLowerCase()) !== -1)
    });
    this.selectedResources = this.resourcesReplacement
  }

  savePermission() {
    this.loadingSubmitPermission = true
    this.validateDate = false
    let requestType: string
    let longDatePermission: number = this.countTotalCalender
    if (this.checkedLeaveRequest === '1') {
      const filterChecked = this.leaveRequestType.filter(data => data.id === this.checkedSpecialLeave)
      const params = {
        startDate: this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'),
        endDate: this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'),
        leaveRequestTypeId: this.checkedSpecialLeave,
        description: this.detailPermissionModel.description,
        address: this.detailPermissionModel.address,
        resourcesReplacement: this.selectedResourceModel.id
      }
      if (longDatePermission <=  filterChecked[0].maxDayLength || filterChecked[0].maxDayLength == null) {
        this.service.updatePermission(this.idPermission ,params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingSubmitPermission = false
                this.successPermission = true
                this.detailPermission()
                this.resourceReplacement()
                this.changePage = 1
              } else {
                this.loadingSubmitPermission = false
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: 'terjadi kesalahan',
                });
              }
            }, error => {
              this.loadingSubmitPermission = false
              if (error?.data) {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.data[0]?.message,
                });
              } else {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.message,
                });
              }
            }
        )
      } else {
        this.loadingSubmitPermission = false
        this.validateDate = true
      }
    } else {
      const filterChecked = this.leaveRequestType.filter(data => data.id === this.checkedLeaveRequest)
      const params = {
        startDate: this.fromDate.year.toString() + '-' + this.fromDate.month.toString().padStart(2, '0') + '-' + this.fromDate.day.toString().padStart(2, '0'),
        endDate: this.toDate.year.toString() + '-' + this.toDate.month.toString().padStart(2, '0') + '-' + this.toDate.day.toString().padStart(2, '0'),
        leaveRequestTypeId: this.checkedLeaveRequest,
        description: this.detailPermissionModel.description,
        address: this.detailPermissionModel.address,
        resourcesReplacement: this.selectedResourceModel.id
      }
      if ( longDatePermission <= filterChecked[0].maxDayLength || filterChecked[0].maxDayLength == null) {
        this.service.updatePermission(this.idPermission ,params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingSubmitPermission = false
                this.successPermission = true
                this.detailPermission()
                this.resourceReplacement()
                this.changePage = 1
              } else {
                this.loadingSubmitPermission = false
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: 'terjadi kesalahan',
                });
              }
            }, error => {
              this.loadingSubmitPermission = false
              if (error?.data) {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.data[0]?.message,
                });
              } else {
                Swal.fire({
                  position: 'center',
                  icon: 'error',
                  title: error.message,
                });
              }

            }
        )
      } else {
        this.loadingSubmitPermission = false
        this.validateDate = true
      }
    }
  }

  changePageStatus() {
    if (this.changePage == 1) {
      this.router.navigate(['administrasi/permission'])
    } else if (this.changePage == 2) {
      this.changePage = 1
    }
  }

  deletePermission() {
    this.loadingDeletePermission = true
    this.service.deletePermission(this.idPermission).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDeletePermission = false
            this.closeModal()
            this.router.navigate(['administrasi/permission'])
          }
        }, error => {
          this.loadingDeletePermission = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }



}
