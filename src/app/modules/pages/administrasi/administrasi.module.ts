import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbDatepickerModule, NgbAlertModule, NgbTimepickerModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {AdministrasiRoutingModule} from './administrasi-routing.module';
import { PermissionComponent } from './PengajuanCuti/permission/permission.component'
import {DataTablesModule} from "angular-datatables";
import { AddPermissionComponent } from './PengajuanCuti/add-permission/add-permission.component';
import { ApplicationPermissionComponent } from './ApproveIzinCuti/application-permission/application-permission.component';
import { DetailPermissionComponent } from './PengajuanCuti/detail-permission/detail-permission.component';
import { DetailApprovePermissionComponent } from './ApproveIzinCuti/detail-approve-permission/detail-approve-permission.component';
import { EventComponent } from './KegiatanLuarKota/event/event.component';
import { AddEventComponent } from './KegiatanLuarKota/add-event/add-event.component';
import { DetailEventComponent } from './KegiatanLuarKota/detail-event/detail-event.component';
import { ApplicationEventComponent } from './PengajuanLuarKota/application-event/application-event.component';
import { DetailApplicationEventComponent } from './PengajuanLuarKota/detail-application-event/detail-application-event.component';
import {UIModule} from "../../../shared/ui/ui.module";
import {HorizontaltopbarComponent} from "../../../layouts/horizontaltopbar/horizontaltopbar.component";
import { EditStaffComponent } from './staf/edit-staff/edit-staff.component';
import {DateHelper} from "../../../core/helpers/date-helper";
import {SwalAlert} from "../../../core/helpers/Swal";

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [PermissionComponent,AddPermissionComponent, ApplicationPermissionComponent, DetailPermissionComponent, DetailApprovePermissionComponent, EventComponent, AddEventComponent, DetailEventComponent, ApplicationEventComponent, DetailApplicationEventComponent, EditStaffComponent],
    imports: [
        NgbDatepickerModule,
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        AdministrasiRoutingModule,
        NgbTooltipModule,
        BsDatepickerModule.forRoot(),
        DataTablesModule,
        NgbAlertModule,
        NgbTimepickerModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        UIModule
    ],
  providers: [
      HorizontaltopbarComponent, DateHelper,
      SwalAlert
  ]
})
export class AdministrasiModule { }
