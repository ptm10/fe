import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {AwpModel} from "../../../core/models/awp.model";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../core/services/Awp/awp.service";
import {leangueIndoDT} from "../../../shared/pagination-custom";
import {DataResponse} from "../../../core/models/dataresponse.models";
import {environment} from "../../../../environments/environment";
import {CommonCons} from "../../../shared/CommonCons";
import Swal from "sweetalert2";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {resourcesModel} from "../../../core/models/resources.model";
import {SwalAlert} from "../../../core/helpers/Swal";
import {AuthfakeauthenticationService} from "../../../core/services/authfake.service";
import {ComponentModel} from "../../../core/models/componentModel";
import {ProfileService} from "../../../core/services/Profile/profile.service";

@Component({
  selector: 'app-administrasi',
  templateUrl: './administrasi.component.html',
  styleUrls: ['./administrasi.component.scss']
})
export class AdministrasiComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  firstName: string;
  lastName: string

  /**
   * variable
   */
  resourcesModel: resourcesModel[]
  filterYear: number
  selectedYear: number
  start = 1
  listFilter = []
  paramsIs = []
  finishLoadData = true
  finishLoadDataCount = 0
  filterStaf: string
  breadCrumbItems: Array<{}>;

  // filter
  selectComponent: string[] = [];
  selectedFilterComponent: string
  selectSubComponent: string[] = [];
  selectedFilterSubComponent: string
  filterSubComponent = []

  // unit
  checkedComponent = []
  listComponentStaff = []
  componentExist: ComponentModel[]
  checkedComponentFilter = []

  constructor(
      private http: HttpClient,
      private service: AwpService,
      private route: Router,
      private swalAlert: SwalAlert,
      public roles: AuthfakeauthenticationService,
      private serviceProfile: ProfileService
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Administrasi' }, { label: 'Direktori Staf', active: true }];
    this.firstName = localStorage.getItem('fullName')
    this.lastName = localStorage.getItem('lastName')
    this.getDataTable()
    this.listComponent()
  }

  getDataTable() {
    this.startLoading()
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.listFilter,
          paramIs: this.paramsIs,
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverResource + 'resources/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.resourcesModel = resp.data.content;
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'id' }, { data: 'user.firstName' },{ data: 'position.unit.name' }, { data: 'user.component.code' }, { data: 'user.province.name' }, { data: 'position' }],
      order: [[3, 'desc']]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.paramsIs = []

    if (this.checkedComponentFilter.length > 0) {
      const selectedComponent = {
        field: "user.component.id",
        dataType: "string",
        value: this.checkedComponentFilter[0]
      }
      this.paramsIs.push(selectedComponent)
    }

    if (this.filterSubComponent.length > 0) {
      const selectedSubComponent = {
        field: "subComponentId",
        dataType: "string",
        value: this.filterSubComponent[0].id
      }
      this.paramsIs.push(selectedSubComponent)
    }


    this.listFilter = []

    const paramsProvince = {
      field: "user.province.name",
      dataType: "string",
      value: this.filterStaf
    }

    const paramsWorkUnit = {
      field: "position.unit.name",
      dataType: "string",
      value: this.filterStaf
    }
    const paramsFirstName = {
      field: "user.firstName",
      dataType: "string",
      value: this.filterStaf
    }

    const paramsLastName = {
      field: "user.lastName",
      dataType: "string",
      value: this.filterStaf
    }

    const paramsUnit = {
      field: "user.component.description",
      dataType: "string",
      value: this.filterStaf
    }

    const paramsPosition = {
      field: "position",
      dataType: "string",
      value: this.filterStaf
    }

    if (this.filterStaf) {
      this.listFilter.push(paramsProvince)
      this.listFilter.push(paramsWorkUnit)
      this.listFilter.push(paramsFirstName)
      this.listFilter.push(paramsLastName)
      this.listFilter.push(paramsUnit)
      this.listFilter.push(paramsPosition)
    } else {
      this.listFilter = []
    }

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  /** Move To Detail Staf */
  goToDetailPageStaf(id) {
    this.route.navigate(['administrasi/staf/' + id])
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  listComponent() {
    let tag = []
    this.serviceProfile.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.componentExist.forEach(dataCode => {
              dataCode.checked = false
              dataCode.code = 'Komponen ' + dataCode.code
              this.listComponentStaff.push(dataCode)
              tag.push(dataCode.code)
            })
            this.selectComponent = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeFilterComponent() {
    this.checkedComponentFilter = []
    let tagSubComponent = []
    let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
    let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
    this.selectedFilterSubComponent = null
    if (this.selectedFilterComponent) {
      findComponent = findComponent.replace('komponen ', '')
      filteredComponent = filteredComponent.filter(x => x.code === findComponent)
      if (filteredComponent.length > 0) {
        this.checkedComponentFilter.push(filteredComponent[0]?.id)
      }
    }
    this.refreshDatatable()
  }


  // changeFilterSubComponent() {
  //   let findComponent = JSON.parse(JSON.stringify(this.selectedFilterComponent))
  //   let findSubComponent = JSON.parse(JSON.stringify(this.selectedFilterSubComponent))
  //   let filteredComponent = JSON.parse(JSON.stringify(this.componentExist))
  //   let selectedFilterSubComponent
  //   if (this.selectedFilterSubComponent) {
  //     findComponent = findComponent.replace('komponen ', '')
  //     filteredComponent = filteredComponent.filter(x => x.code === findComponent)
  //     if (filteredComponent) {
  //       findSubComponent = findSubComponent.replace('Sub-Komponen ', '')
  //       selectedFilterSubComponent = filteredComponent[0].subComponent.filter(x =>x.code === findSubComponent)
  //       this.filterSubComponent = selectedFilterSubComponent
  //     }
  //     this.refreshDatatable()
  //   }
  // }

  clearFilter() {
    this.selectedFilterComponent = null
    this.selectedFilterSubComponent = null
    this.selectSubComponent = []
    this.checkedComponentFilter = []
    this.filterSubComponent = []
    this.refreshDatatable()
  }

}
