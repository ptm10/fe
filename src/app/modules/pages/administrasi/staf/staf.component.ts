
import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AdministrationService} from "../../../../core/services/Administration/administration.service";
import Swal from "sweetalert2";
import {resourcesModel} from "../../../../core/models/resources.model";
import {CalendarOptions, EventClickArg, EventInput} from "@fullcalendar/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AddCalender, CalenderModel} from "../../../../core/models/calender.model";
import * as events from "events";
import {FullCalendarComponent} from "@fullcalendar/angular";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {FileService} from "../../../../core/services/Image/file.service";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {DateHelper} from "../../../../core/helpers/date-helper";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-staf',
  templateUrl: './staf.component.html',
  styleUrls: ['./staf.component.scss']
})
export class StafComponent implements OnInit {

  @ViewChild('modalShow') modalShow: TemplateRef<any>;
  @ViewChild('editmodalShow') editmodalShow: TemplateRef<any>;
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;

  /** Variable */
  listSupervisor: resourcesModel
  idStaf: string
  resourcesDetailModel: resourcesModel
  selectedSupervisorExist: resourcesModel
  dataDetailEvent: EventActivityModel
  loadingDetailEvent = false
  finishLoadData = false
  userIdLogin: string
  allowEdit: boolean
  editEvent: any;
  formData: FormGroup;
  formEditData: FormGroup;
  newEventDate: any;
  dataCalender: EventInput[]
  dataCalenderModel: CalenderModel[]
  startDate: Date
  endDate: Date
  id: string
  tittle: string
  dataCalenderInput: EventInput[] = []
  tempDataCalender: EventInput[] = []
  tittleEventSelected: string
  descriptionEventSelected: string
  taskTypeEventSelected: number
  listDescriptionEventSelected: any
  date: Date
  existingDate: Date
  nextDate: Date
  previousDate: Date
  listEvent = []
  titleDate: Date
  showCalender = false
  formatDatePicker="{dateInputFormat: 'MMMM YYYY'}"
  breadCrumbItems: Array<{}>;
  totalLeave: number
  remainingLeave: number
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: '',
      right: ''
    },
    initialView: "dayGridMonth",
    themeSystem: "bootstrap",
    initialEvents: [],
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    dateClick: this.openModal.bind(this),
    eventClick: this.handleEventClick.bind(this),
    displayEventTime: false,
    customButtons: {
      myCustomButton: {
        text: 'custom!',
        click: function() {
          alert('clicked the custom button!');
        }
      }
    },
  };
  urlPhotoProfile: any
  finishLoadDataCount = 0

  constructor(
      private route: Router,
      private router: ActivatedRoute,
      private service: AdministrationService,
      private modalService: NgbModal,
      private formBuilder: FormBuilder,
      public roles: AuthfakeauthenticationService,
      private serviceImage: FileService,
      private dateHelper: DateHelper,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.userIdLogin = localStorage.getItem('user_id')
    this.breadCrumbItems = [{ label: 'Administrasi' }, { label: 'Direktori Staf' }, { label: 'Jadwal Staf', active: true}];
    this.titleDate = new Date()
    this.date = new Date()
    this.existingDate = new Date()
    this.idStaf = this.router.snapshot.paramMap.get('id')
    this.getDataCalender()
    this.getDetailStaf()
    this.nextDate = new Date(this.existingDate.setMonth(this.existingDate.getMonth() + 1))
    this.previousDate = this.dateHelper.minusDate(new Date(this.date), 1)
  }

  /** Input date calender */
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  /** Move To List Staf */
  goToListStaf() {
    this.route.navigate(['administrasi/detail'])
  }

  /** Get Detail Staf */
  getDetailStaf() {
    this.service.detailStaf(this.idStaf).subscribe(
        resp => {
          if (resp.success) {
            // this.finishLoadData = true
            this.resourcesDetailModel = resp.data
            if (this.resourcesDetailModel?.user?.id === this.userIdLogin) {
              this.allowEdit = false
            } else {
              this.allowEdit = true
            }
            console.log(this.resourcesDetailModel)
            if (this.resourcesDetailModel?.user?.profilePicture?.id) {
              this.getFilePhotos()
            }
            if (this.resourcesDetailModel.supervisiorId) {
              this.getAllSuperVisor()
            }
          } else {
            // this.finishLoadData = true
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getFilePhotos() {
    this.startLoading()
    this.serviceImage.getUrlPhoto(this.resourcesDetailModel?.user?.profilePicture?.id).subscribe(
        resp => {
          this.stopLoading()
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.urlPhotoProfile = reader.result;
          }, false);
          reader.readAsDataURL(resp);
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  /** Get Data Calender */
  getDataCalender() {
    const dataTableModel = {
      enablePage: false,
      paramLike: [],
      paramIs: [
        {
          field: "resourcesId",
          dataType: "string",
          value: this.idStaf
        },
        {
          field: "approveStatus",
          dataType: "int",
          value: 1
        }
      ],
      paramIn: [],
      paramNotIn: [],
      sort: [
        {
          field: "createdAt",
          direction: "desc"
        }
      ]
    }
    this.service.getListCalender(dataTableModel).subscribe(
        resp => {
          if (resp.success) {
            this.resourcesDetailModel = resp.data.resources
            this.totalLeave = resp.data.leaveDaysCount
            this.remainingLeave = resp.data.leaveDaysRemaining
            this.dataCalenderModel = resp.data.content
            this.dataCalenderModel.forEach(data => {
              if (data.taskType == 3) {
                data.listDescription = data.description.split(',')
              }
              this.startDate = new Date(data.startDate)
              this.endDate = new Date(data.endDate)
              this.id = data.id
              this.tittle = data.title
              this.tempDataCalender.push({
                id: this.id,
                title: this.tittle,
                start: this.startDate,
                end: this.endDate,
                className: 'bg-warning text-white',
                extendedProps : {
                  description: data.description,
                  taskType: data.taskType,
                  splitDescription: data.listDescription,
                  taskId: data.taskId
                }
              })
            })
            // this.finishLoadData = true
            setTimeout(() => {
              this.dataCalenderInput = this.tempDataCalender
              this.calendarOptions.events = this.dataCalenderInput
              this.finishLoadData = true
            }, 1000 )
          } else {
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Change Data Calender */
  changeDateCalender() {
    this.titleDate = this.date
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate(this.date);
  }

  moveDateCalender(from: number) {
    if (from === 1) {
      this.date.setMonth(this.date.getMonth() + 1)
    } else {
      this.date = this.dateHelper.minusDate(new Date(this.date), 1)
    }
    this.existingDate = new Date(this.date)
    this.nextDate = new Date(this.existingDate.setMonth(this.existingDate.getMonth() + 1))
    this.previousDate = this.dateHelper.minusDate(new Date(this.date), 1)
    this.titleDate = new Date(this.date)
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate(this.date);
  }

  /** Open Modal For Event */
  openModal(event: any) {
    this.listEvent = []
    const calendar = this.calendarComponent.getApi();
    const dayEvents = calendar.getEvents()
    const dateSelected = new Date(event.dateStr)
    console.log(dayEvents)
    const todayEvents = dayEvents.filter(dataEvent => dataEvent.start.getDate() == dateSelected.getDate() && dataEvent.start.getMonth() == dateSelected.getMonth() && dataEvent.start.getFullYear() == dateSelected.getFullYear())
    if (todayEvents.length > 0) {
      this.modalService.open(this.modalShow, { centered: true });
      this.listEvent = todayEvents
      this.listEvent = this.listEvent.reverse()
      console.log(this.listEvent[0].extendedProps)
      if (this.listEvent[0].extendedProps.taskType == 6) {
        this.getDetailEvent(this.listEvent[0].extendedProps.taskId)
      }
    }
  }

  /*** Event click modal show */
  handleEventClick(clickInfo: EventClickArg) {
    this.tittleEventSelected = ''
    this.descriptionEventSelected = ''
    this.tittleEventSelected = clickInfo.event.title
    this.descriptionEventSelected = clickInfo.event.extendedProps.description
    this.taskTypeEventSelected = clickInfo.event.extendedProps.taskType
    console.log(clickInfo.event.extendedProps.taskType)
    console.log(clickInfo.event.extendedProps.taskId)
    if (clickInfo.event.extendedProps.taskType == 3) {
      this.listDescriptionEventSelected = this.descriptionEventSelected.split(',')
    }
    if (clickInfo.event.extendedProps.taskType == 6) {
      this.getDetailEvent(clickInfo.event.extendedProps.taskId)
    }
    this.editEvent = clickInfo.event;
    this.formEditData = this.formBuilder.group({
      editTitle: clickInfo.event.title,
      dataDescription: clickInfo.event.extendedProps.description,
      start: clickInfo.event.start
    });
    this.modalService.open(this.editmodalShow, { centered: true });
  }

  /** Close Modal */
  closeEventModal() {
    this.formData = this.formBuilder.group({
      title: '',
      category: '',
    });
    this.modalService.dismissAll();
  }

  goToEditStaf() {
    this.route.navigate(['administrasi/edit-staf/' + this.idStaf])
  }

  getAllSuperVisor() {
    this.service.detailStaf(this.resourcesDetailModel.supervisiorId).subscribe(
        resp => {
          if (resp.success) {
            this.listSupervisor = resp.data
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getDetailEvent(data) {
    this.loadingDetailEvent = true
    this.service.detailEvent(data).subscribe(
        resp => {
          if (resp.success) {
            this.dataDetailEvent = resp.data
            this.dataDetailEvent.convertStartDate = new Date(this.dataDetailEvent.startDate)
            this.dataDetailEvent.convertEndDate = new Date(this.dataDetailEvent.endDate)
            this.loadingDetailEvent = false
          }
        }, error => {
          this.loadingDetailEvent = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}



