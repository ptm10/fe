import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import Swal from "sweetalert2";
import {AdministrationService} from "../../../../../core/services/Administration/administration.service";
import {resourcesModel} from "../../../../../core/models/resources.model";
import {AuthfakeauthenticationService} from "../../../../../core/services/authfake.service";
import {AwpModel, Province} from "../../../../../core/models/awp.model";
import {ComponentModel} from "../../../../../core/models/componentModel";
import {SwalAlert} from "../../../../../core/helpers/Swal";
import {ListUnit, Position} from "../../../../../core/models/RoleUsers";
import {ProfileService} from "../../../../../core/services/Profile/profile.service";
import {AwpService} from "../../../../../core/services/Awp/awp.service";

@Component({
  selector: 'app-edit-staff',
  templateUrl: './edit-staff.component.html',
  styleUrls: ['./edit-staff.component.scss']
})
export class EditStaffComponent implements OnInit {

  idResourceStaff: string
  breadCrumbItems: Array<{}>;
  resourcesDetailModel: resourcesModel
  resourcesDetailModelEdit: resourcesModel
  listSupervisor: resourcesModel[]
  listSupervisorExist: resourcesModel[]
  selectedSupervisorExist: resourcesModel
  listComponentStaff: ComponentModel[]
  componentExist: ComponentModel[]
  selectedComponentExist: ComponentModel
  finishLoadData = true
  finishLoadDataCount = 0
  loadingUpdateStaff = false
  editEmail: string
  firstName: string
  lastName: string
  loadingGetListSuperVisor = false
  selectedWorkUnit: string

  //Unit
  listUnit: ListUnit[]

  //province
  listProvinceAll: Province[]
  listProvinceAllMaster: Province[]
  listProvinceApi: string[];
  selectedProvince: string

  //position
  listPosition: Position[]
  listPositionApi: string[]
  selectedPosition: string

  constructor(
      private router: Router,
      private route: ActivatedRoute,
      private service: AdministrationService,
      private swalAlert: SwalAlert,
      private serviceProfile: ProfileService,
      private serviceAwp: AwpService
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Administrasi' }, { label: 'Direktori Staf' }, { label: 'Detail Staf', active: true}];
    this.idResourceStaff = this.route.snapshot.paramMap.get('id')
    this.getDetailStaf()
    this.getListWorkUnit()
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    return !(charCode > 31 && (charCode < 48 || charCode > 57));
  }

  goToListStaf() {
    this.router.navigate(['administrasi/staf/' + this.idResourceStaff])
  }

  changeSelectedWorkUnit() {
    let selectedUnitId = this.listUnit.filter(x => x.name === this.selectedWorkUnit)
    this.selectedSupervisorExist = null
    this.resourcesDetailModelEdit.position.name = null
    if (this.selectedWorkUnit === 'PMU') {
      this.getListSuperVisor()
      this.getListPosition(selectedUnitId[0].id)
    } else if (this.selectedWorkUnit === 'PCU') {
      this.getListSuperVisor()
      this.getListPosition(selectedUnitId[0].id)
    }
  }

  getListPosition(data) {
    this.listPosition = []
    let tag = []
    const params = {
      enablePage: false,
      paramIs: [
        {
          field: "unitId",
          dataType: "string",
          value: data
        }
      ],
      sort: [
        {
          field: "name",
          direction: "asc"
        }
      ]
    }
    this.serviceProfile.listPosition(params).subscribe(
        resp => {
          if (resp.success) {
            this.listPosition = resp.data.content
            this.listPosition.forEach(x => {
              tag.push(x.name)
            })
            this.listPositionApi = tag
          }
        }, error =>  {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  getDetailStaf() {
    this.startLoading()
    this.service.detailStaf(this.idResourceStaff).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            // this.finishLoadData = true
            this.resourcesDetailModel = resp.data
            this.firstName = this.resourcesDetailModel.user?.firstName
            this.lastName = this.resourcesDetailModel.user?.lastName
            this.selectedWorkUnit = this.resourcesDetailModel?.position?.unit?.name
            this.resourcesDetailModelEdit = JSON.parse(JSON.stringify(this.resourcesDetailModel))
            const editedEmail = this.resourcesDetailModelEdit?.user?.email.split('@')
            this.selectedComponentExist = this.resourcesDetailModelEdit?.user?.component
            if (this.resourcesDetailModel?.position?.unit?.name === 'PMU') {
              this.selectedComponentExist.code = 'Komponen ' + this.selectedComponentExist.code
            }
            this.editEmail = editedEmail[0]
            this.resourcesDetailModelEdit.phoneNumber = this.resourcesDetailModelEdit?.phoneNumber.substr(3)
            if (this.selectedWorkUnit === 'PCU') {
              this.selectedProvince = this.resourcesDetailModelEdit?.user?.province?.name
              this.listProvince()
            } else {
              this.getListSuperVisor()
            }
            this.listAwp()
            this.getListPosition(this.resourcesDetailModel?.position?.unit?.id)
          } else {
            // this.finishLoadData = true
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  // new tag
  CreateNew(Position){
    return Position
  }

  getListWorkUnit() {
    this.startLoading()
    this.serviceProfile.listUnit().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listUnit = resp.data.content
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  listProvince() {
    let tag = []
    this.startLoading()
    this.serviceAwp.province().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listProvinceAll = resp.data.content
            this.listProvinceAllMaster = resp.data.content
            this.listProvinceAll.forEach(x => {
              tag.push(x.name)
            })
            this.getListSuperVisor()
            this.listProvinceApi = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeSuperVisorPcu() {
    this.getListSuperVisor()
  }

  updateDataUser() {
    let selectedComponent
    let params
    let selectedProvince

    if (this.selectedProvince) {
      selectedProvince = this.listProvinceAll.filter(x => x.name === this.selectedProvince)
    }

    if (this.selectedComponentExist) {
      selectedComponent = this.selectedComponentExist.id
    } else {
      selectedComponent = null
    }
    this.loadingUpdateStaff = true
    if (this.selectedWorkUnit === 'PMU') {
      params = {
        firstName: this.resourcesDetailModelEdit?.user?.firstName,
        lastName: this.resourcesDetailModelEdit?.user?.lastName,
        email:  this.editEmail +"@madrasah.kemenag.go.id",
        componentId : this.selectedComponentExist.id,
        active: true
      }
    } else if (this.selectedWorkUnit === 'PCU') {
      params = {
        firstName: this.resourcesDetailModelEdit?.user?.firstName,
        lastName: this.resourcesDetailModelEdit?.user?.lastName,
        email:  this.editEmail +"@madrasah.kemenag.go.id",
        provinceId: selectedProvince[0].id,
        active: true
      }
    }
    this.service.updateDataUserStaf(this.resourcesDetailModel?.user?.id, params).subscribe(
        resp => {
          if (resp.success) {
            this.updateResources()
          }
        }, error => {
          this.loadingUpdateStaff = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  updateResources() {
    let superVisorSelected
    const selectedUnitId = this.listUnit.filter(x => x.name === this.selectedWorkUnit)

    if (this.selectedSupervisorExist) {
      superVisorSelected = this.selectedSupervisorExist.id
    } else  {
      superVisorSelected = null
    }
    const params = {
      userId: this.resourcesDetailModel?.user?.id,
      position: this.resourcesDetailModelEdit?.position.name,
      phoneNumber: '+62'+this.resourcesDetailModelEdit.phoneNumber,
      supervisiorId: superVisorSelected,
      unitId: selectedUnitId[0].id,
    }
    this.service.updateDataResourceStaf(this.idResourceStaff, params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingUpdateStaff = false
            this.router.navigate(['administrasi/staf/' + this.idResourceStaff])
          }
        }, error => {
          this.loadingUpdateStaff = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  disabledSelectSupervisor() {
    if (this.selectedWorkUnit === 'PMU') {
      return !this.selectedComponentExist
    } else if (this.selectedWorkUnit === 'PCU') {
      return !this.selectedProvince || !this.resourcesDetailModelEdit?.position
    } else {
      return true
    }
  }

  getListSuperVisor() {
    let params
    let selectedProvince
    this.loadingGetListSuperVisor = true
    this.selectedSupervisorExist = null
    this.listSupervisor = []
    this.listSupervisorExist = []
    let roleIdsSelected = []
    if (this.selectedWorkUnit === 'PMU') {
      if (this.selectedComponentExist) {
        this.resourcesDetailModel?.user?.roles.forEach(data => {
          roleIdsSelected.push(data.roleId)
        })
        params = {
          componentId: this.selectedComponentExist.id,
          roleIds: roleIdsSelected,
          userId: this.resourcesDetailModel?.user?.id
        }
        this.service.listSupervisorByComponent(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingGetListSuperVisor = false
                this.listSupervisor = []
                this.listSupervisorExist = []
                this.listSupervisor = resp.data.content
                this.listSupervisorExist = resp.data.content
                const x = this.listSupervisor.filter(dataSP => dataSP.id == this.resourcesDetailModel.supervisiorId)
                this.selectedSupervisorExist = x[0]
                this.listSupervisor = this.listSupervisor.filter(listData => listData.id !== this.resourcesDetailModel.supervisiorId)
                if (!this.selectedSupervisorExist) {
                  this.listSupervisor = this.listSupervisorExist
                  console.log(this.listSupervisorExist)
                }
              }
            }, error => {
              this.loadingGetListSuperVisor = false
              this.swalAlert.showAlertSwal(error)
            }
        )
      } else {
        this.loadingGetListSuperVisor = false
      }
    } else if (this.selectedWorkUnit === 'PCU') {
      if (this.selectedProvince && this.resourcesDetailModelEdit?.position?.name) {
        selectedProvince = this.listProvinceAll.filter(x => x.name === this.selectedProvince)
        params = {
          provinceId: selectedProvince[0].id,
          userId: this.resourcesDetailModel?.user?.id,
          position: this.resourcesDetailModelEdit?.position?.name
        }
        this.serviceProfile.listSupervisorByRolePcu(params).subscribe(
            resp => {
              if (resp.success) {
                this.loadingGetListSuperVisor = false
                this.listSupervisor = []
                this.listSupervisorExist = []
                this.listSupervisor = resp.data.content
                this.listSupervisorExist = resp.data.content
                const x = this.listSupervisor.filter(dataSP => dataSP.id == this.resourcesDetailModel?.supervisiorId)
                this.selectedSupervisorExist = x[0]
                this.listSupervisor = this.listSupervisor.filter(listData => listData.id !== this.resourcesDetailModel?.supervisiorId)
                if (!this.selectedSupervisorExist) {
                  this.listSupervisor = this.listSupervisorExist
                  console.log(this.listSupervisorExist)
                }
              }
            }, error => {
              this.loadingGetListSuperVisor = false
              this.swalAlert.showAlertSwal(error)
            }
        )
      } else {
        this.loadingGetListSuperVisor = false
      }
    } else {
      this.loadingGetListSuperVisor = false
    }

  }

  selectedSupervisor(data) {
    this.selectedSupervisorExist = null
    this.listSupervisor = this.listSupervisorExist

    this.selectedSupervisorExist = data
    this.listSupervisor = this.listSupervisor.filter(dataFilter => dataFilter.id != this.selectedSupervisorExist.id)
  }

  searchFilterSuperVisor(e) {
    const searchStr = e.target.value
    //
    let filter = this.listSupervisorExist
    if (this.selectedSupervisorExist) {
      filter = filter.filter(x => x.id !== this.selectedSupervisorExist.id)
    } else {
      filter = this.listSupervisorExist
    }

    this.listSupervisor = filter.filter((product) => {
      return ((product?.user?.firstName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product?.user?.firstName != null) || (product?.user?.lastName?.toLowerCase().search(searchStr.toLowerCase()) !== -1 && product?.user?.lastName != null))
    });
  }

  selectedUnit(data) {
    this.selectedComponentExist = null
    this.listComponentStaff = this.componentExist

    this.selectedComponentExist = data
    this.listComponentStaff = this.listComponentStaff.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)
    this.getListSuperVisor()
  }

  searchFilterUnit(e) {
    const searchStr = e.target.value
    //
    let filter = this.componentExist
    if (this.selectedComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
    } else {
      filter = this.componentExist
    }

    this.listComponentStaff = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  listAwp() {
    this.service.listAwp().subscribe(
        resp => {
          if (resp.success) {
            this.componentExist = resp.data.content
            this.listComponentStaff = resp.data.content
            this.listComponentStaff.forEach(dataCode => {
              dataCode.code = 'Komponen ' + dataCode.code
            })
            this.listComponentStaff = this.listComponentStaff.filter(dataStaff => dataStaff.id != this.selectedComponentExist.id)
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }


  keyPressAlphaNumeric(event) {
    var inp = String.fromCharCode(event.keyCode);
    if (/[a-zA-Z0-9]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

}
