import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdministrasiComponent} from './administrasi.component'
import {StafComponent} from './staf/staf.component'
import {PermissionComponent} from "./PengajuanCuti/permission/permission.component";
import {AddPermissionComponent} from "./PengajuanCuti/add-permission/add-permission.component";
import {ApplicationPermissionComponent} from "./ApproveIzinCuti/application-permission/application-permission.component";
import {DetailPermissionComponent} from "./PengajuanCuti/detail-permission/detail-permission.component";
import {DetailApprovePermissionComponent} from "./ApproveIzinCuti/detail-approve-permission/detail-approve-permission.component";
import {EventComponent} from "./KegiatanLuarKota/event/event.component";
import {AddEventComponent} from "./KegiatanLuarKota/add-event/add-event.component";
import {DetailEventComponent} from "./KegiatanLuarKota/detail-event/detail-event.component";
import {ApplicationEventComponent} from "./PengajuanLuarKota/application-event/application-event.component";
import {
    DetailApplicationEventComponent
} from "./PengajuanLuarKota/detail-application-event/detail-application-event.component";
import {EditStaffComponent} from "./staf/edit-staff/edit-staff.component";
import {AuthGuard} from "../../../core/guards/auth.guard";

const routes: Routes = [
    {
        path: 'detail',
        component: AdministrasiComponent, canActivate: [AuthGuard]
    },
    {
        path: 'staf/:id',
        component: StafComponent, canActivate: [AuthGuard]
    },
    {
        path: 'permission',
        component: PermissionComponent, canActivate: [AuthGuard]
    },
    {
        path: 'add',
        component: AddPermissionComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-permission/:id',
        component: DetailPermissionComponent, canActivate: [AuthGuard]
    },
    {
        path: 'application',
        component: ApplicationPermissionComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-approve-permission/:id',
        component: DetailApprovePermissionComponent, canActivate: [AuthGuard]
    },
    {
        path: 'event',
        component: EventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'add-event',
        component: AddEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-event/:id',
        component: DetailEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'application-event',
        component: ApplicationEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'detail-application-event/:id',
        component: DetailApplicationEventComponent, canActivate: [AuthGuard]
    },
    {
        path: 'edit-staf/:id',
        component: EditStaffComponent, canActivate: [AuthGuard]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdministrasiRoutingModule {}
