import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministrasiComponent } from './administrasi.component';

describe('AdministrasiComponent', () => {
  let component: AdministrasiComponent;
  let fixture: ComponentFixture<AdministrasiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdministrasiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdministrasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
