import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {
    NgbNavModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbTooltipModule,
    NgbCollapseModule,
    NgbAlertModule, NgbDatepickerModule, NgbButtonsModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgApexchartsModule } from 'ng-apexcharts';
import { FullCalendarModule } from '@fullcalendar/angular';
import { SimplebarAngularModule } from 'simplebar-angular';
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction'; // a plugin
import bootstrapPlugin from "@fullcalendar/bootstrap";
import { LightboxModule } from 'ngx-lightbox';

import { WidgetModule } from '../../shared/widget/widget.module';
import { UIModule } from '../../shared/ui/ui.module';

import { PagesRoutingModule } from './pages-routing.module';

import { DashboardsModule } from './dashboards/dashboards.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProfileComponent } from './profile/profile.component';
import {FileUploadModule} from "@iplab/ngx-file-upload";
import { AdministrasiComponent } from './administrasi/administrasi.component';
import { StafComponent } from './administrasi/staf/staf.component';
import {DataTablesModule} from "angular-datatables";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {DROPZONE_CONFIG} from "ngx-dropzone-wrapper";
import {SwalAlert} from "../../core/helpers/Swal";
import {NgSelectModule} from "@ng-select/ng-select";

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  bootstrapPlugin
]);

@NgModule({
  declarations: [
    ProfileComponent,
    ProfileComponent,
    AdministrasiComponent,
    StafComponent,
  ],
    imports: [
        CommonModule,
        FormsModule,
        NgbDropdownModule,
        NgbModalModule,
        PagesRoutingModule,
        NgApexchartsModule,
        ReactiveFormsModule,
        DashboardsModule,
        HttpClientModule,
        UIModule,
        WidgetModule,
        FullCalendarModule,
        NgbNavModule,
        NgbTooltipModule,
        NgbCollapseModule,
        SimplebarAngularModule,
        LightboxModule,
        FileUploadModule,
        NgbAlertModule,
        DataTablesModule,
        NgbDatepickerModule,
        NgbButtonsModule,
        BsDatepickerModule,
        NgSelectModule
    ],
    providers: [
        SwalAlert
    ]
})
export class PagesModule { }
