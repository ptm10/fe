import { Component, OnInit } from '@angular/core';
import {ProfileService} from "../../../core/services/Profile/profile.service";
import Swal from 'sweetalert2';
import {ChangeEmailModels, UsersModels} from "../../../core/models/users.models";
import {NgbDate, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomValidators} from "../../../shared/custom-validators/custom-validators";
import {FileModel} from "../../../core/models/file.model";
import {environment} from "../../../../environments/environment";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {RoleUsers} from "../../../core/models/RoleUsers";
import {ApprovalModel} from "../../../core/models/approval.model";
import {CommonCons} from "../../../shared/CommonCons";
import {FileService} from "../../../core/services/Image/file.service";
import {AdministrationService} from "../../../core/services/Administration/administration.service";
import {resourcesModel} from "../../../core/models/resources.model";
import {SwalAlert} from "../../../core/helpers/Swal";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    /** Variable In Profile */
    changePasswordForm: FormGroup;
    validationUploadFileEmpty = false
    profileModel: UsersModels
    changeEmailModel: ChangeEmailModels[]
    showOldPasswordInput = false
    showPasswordInputUsers = false
    showRetypePasswordInputUsers = false
    urlPhotoProfile: any
    urlFiles = environment.serverFile
    showWarningFormatEmail = false

    filePhotoModel: FileModel
    uploadPhotoProfileLoading = false
    uploadFilePhotoProfile: Array<File> = [];
    uploadFilePhotoProfileIds: Array<File> = [];

    validateOldPassword = false
    validateNewPassword = false
    validateRetypePassword = false

    changePageProfile: number
    loadingChangePassword = true
    successChangePassword = false

    listRolesExist: RoleUsers[]
    removeRolesSelected = []
    editFirstName: string
    editLastName: string
    roleRequest = []
    fileUploadPhoto: FileModel[]
    idNewPhoto: string
    loadingEditProfile = true
    validationModel: ApprovalModel
    checkValidationWaiting = false
    filterRole : RoleUsers[]
    openModalRoles = false
    addRolesSelected = []
    beforeSelected = []
    roleSelected = []
    roleRequestToApi = []
    errorRespChangePassword = ''
    listRolesSelectedExist = []
    oListRolesSelectedExist = []
    accessToken = localStorage.getItem('accessToken')
    breadCrumbItems: Array<{}>;
    finishLoadData = true
    finishLoadDataCount = 0
    loadingDeleteRoles = false
    editEmailUser = false
    editEmail: string
    loadingDeleteChangeEmail = false
    includesRoleLsp = false
    includesRolePcu = false
    listSupervisor: resourcesModel[]
    selectedSupervisorExist: resourcesModel
    rolesLspExist = false

    getToken() {
        return 'Bearer ' + localStorage.getItem('accessToken');
    }

    constructor(
      private service: ProfileService,
      private modalService: NgbModal,
      private formBuilder: FormBuilder,
      private http: HttpClient,
      private serviceImage: FileService,
      private serviceAdministration: AdministrationService,
      private swalAlert: SwalAlert
    ) {}

    ngOnInit(): void {
        this.breadCrumbItems = [{ label: 'Profil', active: true }];
        this.getPendingUpdate()
        this.changePageProfile = 1
        this.getDetailProfile()
        this.checkValidateRequestEmail()
        this.changePasswordForm = this.formBuilder.group({
            passwordLast: ['', [Validators.compose([
                Validators.required,
                CustomValidators.patternValidator(/\d/, { hasNumber: true }),
                CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
                CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
                CustomValidators.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, { hasSpecialCharacters: true }),
                Validators.minLength(8)
            ])]],
            password: ['', [Validators.compose([
                Validators.required,
                CustomValidators.patternValidator(/\d/, { hasNumber: true }),
                CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
                CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
                CustomValidators.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, { hasSpecialCharacters: true }),
                Validators.minLength(8)
            ])]],
            reTypePassword: ['', [Validators.required]],
        },
        {
            // check whether our password and confirm password match
            validator: CustomValidators.passwordMatchValidator
        });
    }
    get f() { return this.changePasswordForm.controls; }

    changeAccessEditEmail() {
        this.editEmailUser = !this.editEmailUser
    }

    keyPressAlphaNumeric(event) {
        var inp = String.fromCharCode(event.keyCode);

        if (/[a-zA-Z0-9]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    /**
     * Open extra large modal
     * @param selectedMenu extra large modal data
     */
    openModal(selectedMenu: any) {
        this.modalService.open(selectedMenu, { size: 'lg', centered: true });
    }

    /** * close modal */
    closeModal(){
        this.modalService.dismissAll()
    }


    /** * Show input Old Password, New Password & Retype Password */
    showOldPasswordInputUser() {
        this.showOldPasswordInput = !this.showOldPasswordInput
    }

    showPasswordInput() {
        this.showPasswordInputUsers = !this.showPasswordInputUsers
    }

    showRetypePasswordInput() {
        this.showRetypePasswordInputUsers = !this.showRetypePasswordInputUsers
    }

    goToEditPage() {
        this.changePageProfile = 2
    }

    goToDetailPage() {
        this.successChangePassword = false
        this.changePageProfile = 1
    }

    goToChangePassword() {
        this.changePageProfile = 3
    }

    /** Function Start Loading */
    startLoading() {
        if (this.finishLoadDataCount == 0) {
            this.finishLoadDataCount ++
            this.finishLoadData = false
        } else {
            this.finishLoadDataCount ++
        }
    }

    /** Function Stop Loading */
    stopLoading() {
        this.finishLoadDataCount --
        if (this.finishLoadDataCount == 0) {
            this.finishLoadData = true

        }
    }

    addDeleteRolesSelected(data, check) {
        if (check == 1) {
            let checkExist = false
            this.roleRequest.filter(exist => {
                if (exist.id === data.id) {
                    checkExist = true
                    return
                }
            })
            if (!checkExist) {
                let checkRemoveExist = false
                this.removeRolesSelected.forEach((check, index) => {
                    if (check.id === data.id) {
                        this.removeRolesSelected.splice(index, 1)
                        checkRemoveExist = true
                        return
                    }
                })
                if (!checkRemoveExist) {
                    data.status = 1
                    this.addRolesSelected.push(data)
                }
                this.roleRequest.push(data)
            }
        } else {
            this.roleRequest.forEach((existing, index) => {
                if (existing.id === data.id) {
                    data.checked = false
                    this.roleRequest.splice(index, 1)

                }
            })
            let checkAddExist = false

            this.addRolesSelected.forEach((check, index) => {
                if (check.id === data.id) {
                    this.addRolesSelected.splice(index, 1)
                    checkAddExist = true
                    return
                }
            })

            if (!checkAddExist) {
                data.status = 0
                this.removeRolesSelected.push(data)
            }
        }

        this.listRolesSelectedExist = this.filterRole

        this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => dataFilter.name !== 'PCU')

        this.roleRequest.forEach(dataSelected => {
            if (dataSelected.name === 'Ketua PMU') {
                this.listRolesSelectedExist = []
                return
            }
            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter =>  dataFilter.name !== 'Ketua PMU')
            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => dataFilter.id !== dataSelected.id)
            if (dataSelected.supervisiorId !== null) {
                this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => (dataFilter.supervisiorId === dataSelected.supervisiorId || dataFilter.supervisiorId == null))
            }
        })

        this.oListRolesSelectedExist = this.listRolesSelectedExist
    }

    
    openFilterRoles() {
        this.openModalRoles = !this.openModalRoles
    }

    /** Get Detail Profile */
    getDetailProfile() {
        this.startLoading()
        this.service.detailProfile().subscribe(
            resp => {
                this.stopLoading()
              if (resp.success) {
                  this.roleRequest = []
                  this.profileModel = resp.data
                  this.profileModel.editEmailExist = this.profileModel.email.split('@')
                  this.editEmail =  this.profileModel.editEmailExist[0]
                  if (this.profileModel.resources) {
                      this.getAllSuperVisor()
                  }
                  this.profileModel.roles.forEach(data => {
                      data.role.checked = true
                      this.roleRequest.push(data.role)
                      this.beforeSelected.push(data.role)
                      if (data.role.name == 'LSP') {
                          this.includesRoleLsp = true
                      }
                      if (data.role?.name == 'PCU') {
                          this.includesRolePcu = true
                      }
                  })
                  if (this.profileModel?.profilePicture?.id) {
                      // this.urlPhotoProfile =  this.urlFiles+'/'+this.profileModel?.profilePicture?.id
                      this.getFilePhotos()
                  }
                  this.editFirstName = this.profileModel.firstName
                  this.editLastName = this.profileModel.lastName
                  if (this.profileModel.lsp) {
                      this.profileModel.convertExpiredEndDate = new Date(this.profileModel.lsp.endDate)
                      this.profileModel.convertExpiredStartDate = new Date(this.profileModel.lsp.startDate)
                  }
                  let rolesLspExist = this.profileModel.roles.filter(x => x.role.name === 'LSP')
                  this.rolesLspExist = rolesLspExist.length > 0;

                  this.getListRoles()
              } else {
                  Swal.fire({
                      position: 'center',
                      icon: 'error',
                      title: 'Terjadi Kesalahan Pada Server',
                  });
              }
            }, error => {
                this.stopLoading()
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    /** Save Change Password */
    saveChangePassword() {
        this.successChangePassword = false
        this.loadingChangePassword = false
        if (this.checkValidationChangePassword()) {
            const params = {
                oldPassword: this.f.passwordLast.value,
                newPassword: this.f.password.value,
                confirmPassword: this.f.reTypePassword.value
            }
            this.service.changePassword(params).subscribe(
                resp => {
                    if (resp.success) {
                        this.loadingChangePassword = true
                        this.successChangePassword = true
                    } else {
                    }
                }, error =>  {
                    if (error?.error?.data) {
                        if (error?.error?.data[0]?.key === 'oldPassword') {
                            this.validateOldPassword = true
                            this.errorRespChangePassword = error?.error?.data[0].message
                        } else {
                            this.validateOldPassword = true
                            this.errorRespChangePassword = error?.error?.data[0].message
                        }
                    } else {
                        this.validateOldPassword = true
                        this.errorRespChangePassword = error?.error?.data[0].message
                    }

                    this.loadingChangePassword = true
                }
            )
        } else {
            this.loadingChangePassword = true
        }
    }

    /** Validation Change Password Password */
    checkValidationChangePassword(): boolean {
        this.validateOldPassword = false
        this.validateNewPassword = false
        this.validateRetypePassword = false

        if (this.f.passwordLast.invalid) {
            this.validateOldPassword = true
        }

        if (this.f.password.invalid) {
            this.validateNewPassword = true
        }

        if (this.f.reTypePassword.invalid) {
            this.validateRetypePassword = true
        }

        return this.f.passwordLast.valid && this.f.password.valid && this.f.reTypePassword.valid;
    }

    /** Upload Foto Profile */
    doUploadPhotoProfile() {
        this.loadingEditProfile = false
        if (this.uploadFilePhotoProfile.length > 0) {
            this.validationUploadFileEmpty = false
            this.service.upload(this.uploadFilePhotoProfile[0]).subscribe(resp => {
                    if (resp.success) {
                        this.fileUploadPhoto = resp.data
                        this.saveEditProfile()
                    } else {
                        this.loadingEditProfile = true
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: resp.message,
                        });
                    }
                }, error => {
                    this.loadingEditProfile = true
                    this.swalAlert.showAlertSwal(error)
                }
            );
        } else {
            this.saveEditProfile()
        }
    }

    getListRoles() {
        this.startLoading()
        this.service.listRole().subscribe(
            resp => {
                this.stopLoading()
                if (resp.success) {
                    this.listRolesExist = resp.data.content
                    this.filterRole = resp.data.content

                    this.filterRole = this.filterRole.filter(dataFilterLsp => dataFilterLsp.name != 'LSP' && dataFilterLsp.name != 'PCU')
                    this.listRolesExist = this.listRolesExist.filter(dataFilterLsp => dataFilterLsp.name != 'LSP' && dataFilterLsp.name != 'PCU')
                    this.listRolesSelectedExist = this.filterRole

                    this.roleRequest.forEach(dataSelected => {
                        if (dataSelected.name === 'Ketua PMU') {
                            this.listRolesSelectedExist = []
                            return
                        }
                        this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter =>  dataFilter.name !== 'Ketua PMU')
                        this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => dataFilter.id !== dataSelected.id)
                        if (dataSelected.supervisiorId !== null) {
                            this.listRolesSelectedExist = this.listRolesSelectedExist.filter(dataFilter => (dataFilter.supervisiorId === dataSelected.supervisiorId || dataFilter.supervisiorId == null))
                        }
                    })

                    this.oListRolesSelectedExist = this.listRolesSelectedExist
                    this.listRolesExist.forEach(listExist => {
                        if (this.profileModel) {
                            this.profileModel.roles.forEach(hasRole => {
                                if (listExist.id === hasRole.roleId) {
                                    listExist.checked = true
                                }
                            })
                        }
                    })
                }
            }, error => {
                this.stopLoading()
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    saveEditProfile() {
        let params
        this.roleRequestToApi = []
        // this.roleRequest = []
        if (!this.fileUploadPhoto) {
            this.idNewPhoto = null
        } else {
            this.idNewPhoto = this.fileUploadPhoto[0]?.id
        }

        this.addRolesSelected.forEach(data => {
            this.roleSelected.push(data)
        })

        this.removeRolesSelected.forEach(data => {
            this.roleSelected.push(data)
        })


        if (this.roleSelected.length > 0) {
            this.roleSelected.forEach(data => {
                this.roleRequestToApi.push({
                    roleId: data.id,
                    status: data.status
                })
            })
        }

        if (this.editEmailUser) {
            params = {
                firstName: this.editFirstName,
                lastName: this.editLastName,
                profilePictureId: this.idNewPhoto,
                roleRequest: this.roleRequestToApi,
                email: this.editEmail + '@madrasah.kemenag.go.id'
            }
        } else {
            params = {
                firstName: this.editFirstName,
                lastName: this.editLastName,
                profilePictureId: this.idNewPhoto,
                roleRequest: this.roleRequestToApi,
                email: this.profileModel.email
            }
        }
        this.service.updateProfile(params).subscribe(
            resp => {
                if (resp.success) {
                    this.loadingEditProfile = true
                    this.getDetailProfile()
                    this.getPendingUpdate()
                    this.checkValidateRequestEmail()
                    this.editEmailUser = false
                    this.addRolesSelected = []
                    this.removeRolesSelected = []
                    this.roleRequestToApi = []
                    this.roleSelected = []
                    this.listRolesSelectedExist = []
                    this.oListRolesSelectedExist = []
                    this.changePageProfile = 1
                }
            }, error => {
                this.loadingEditProfile = true
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    getPendingUpdate() {
        this.service.pendingApproval().subscribe(
            resp => {
                if (resp.success) {
                    this.validationModel = resp.data
                    if (this.validationModel != null) {
                        this.checkValidationWaiting = true
                    }
                } else {
                }
            }, error => {
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    validationSelectedRoles() {
        const check = this.listRolesExist.filter(data => {
            return data.checked == false
        })
    }

    searchFilter(e) {
        const searchStr = e.target.value
        this.listRolesSelectedExist = this.oListRolesSelectedExist.filter((product) => {
            return product.name.toLowerCase().search(searchStr.toLowerCase()) !== -1
        });
    }

    getFilePhotos() {
        this.startLoading()
        this.serviceImage.getUrlPhoto(this.profileModel?.profilePicture?.id).subscribe(
            resp => {
                this.stopLoading()
                let reader = new FileReader();
                reader.addEventListener("load", () => {
                    this.urlPhotoProfile = reader.result;
                }, false);
                reader.readAsDataURL(resp);
            }, error => {
                this.stopLoading()
            }
        )
    }

    cancelPermissionRole() {
        this.loadingDeleteRoles = true
        this.service.removeRolesChoosed().subscribe(
            resp => {
                if (resp.success) {
                    this.loadingDeleteRoles = false
                    this.closeModal()
                    this.getPendingUpdate()
                    this.getDetailProfile()
                }
            }, error => {
                this.loadingDeleteRoles = false
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    checkValidateRequestEmail() {
        this.service.validateEmail().subscribe(
            resp => {
                if (resp.success) {
                    this.changeEmailModel = resp.data
                }
            }, error => {
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    deleteChangeEmail() {
        this.loadingDeleteChangeEmail = true
        this.service.deleteRequestChangeEmail().subscribe(
            resp => {
                if (resp.success) {
                    this.loadingDeleteChangeEmail = false
                    this.closeModal()
                    this.getDetailProfile()
                    this.getPendingUpdate()
                    this.checkValidateRequestEmail()
                    this.editEmailUser = false
                    this.changePageProfile = 1
                }
            }, error => {
                this.loadingDeleteChangeEmail = false
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

    getAllSuperVisor() {
        this.serviceAdministration.resourceAll().subscribe(
            resp => {
                if (resp.success) {
                    this.listSupervisor = resp.data.content
                    const x = this.listSupervisor.filter(dataSP => dataSP.id == this.profileModel.resources.supervisiorId)
                    this.selectedSupervisorExist = x[0]
                }
            }, error => {
                this.swalAlert.showAlertSwal(error)
            }
        )
    }

}

