import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {parse} from "date-fns";
import {Answer, ChildMonev, Monev, Monev2} from "../../../../core/models/monev";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {MonevService} from "../../../../core/services/Monev/monev.service";
import {NgbNavChangeEvent} from "@ng-bootstrap/ng-bootstrap";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {ComponentService} from "../../../../core/services/Component/component.service";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {EventActivityService} from "../../../../core/services/EventActivity/event-activity.service";

@Component({
  selector: 'app-detail-result-monev',
  templateUrl: './detail-result-monev.component.html',
  styleUrls: ['./detail-result-monev.component.scss']
})
export class DetailResultMonevComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  checkedButton: number
  detailIdResultMonev: string
  from: string
  date: Date
  carouselListIndicator: string
  evaluationDetail: string
  separatorDetail: string
  selectedMonevTypeId: string

  // loading
  finishLoadData = true
  finishLoadDataCount = 0
  listMonev: Monev2
  loadingUpdateMonev = false
  listSuccessIndicator: Monev[]
  resultStatus: number
  resultAnswer: Answer[] = []
  childMonev: Monev2[]
  validationEditMonev: boolean
  yearSelected: any
  showMoreDocumentation = false


  constructor(
      private activatedRoute: ActivatedRoute,
      private swalAlert: SwalAlert,
      private serviceMonev: MonevService,
      private serviceKegiatan: KegiatanService,
      private serviceComponent: ComponentService,
      private service: AwpService,
      private router: Router,
      public roles: AuthfakeauthenticationService,
      private serviceEvent: EventActivityService,
  ) { }

  ngOnInit(): void {
    this.date = new Date()
    this.detailIdResultMonev = this.activatedRoute.snapshot.paramMap.get('id')
    this.from = this.activatedRoute.snapshot.paramMap.get('from')
    this.yearSelected = this.activatedRoute.snapshot.paramMap.get('year')
    this.yearSelected = Number(this.yearSelected)
    if (this.yearSelected === 1) {
      this.date = new Date()
    } else {
      this.date = new Date(this.yearSelected, 1, 1)
    }
    if (this.from === 'output') {
      this.selectedMonevTypeId = 'afedb8dc-d1ee-11ec-88cb-0ba398ed0298'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Output'}, { label: 'Hasil'}, { label: 'Detail Hasil Output', active: true }]
    } else if (this.from === 'intermediate') {
      this.selectedMonevTypeId = 'df8ab4b0-d1ed-11ec-88ca-6f259751c604'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Intermediate Outcome'},{ label: 'Hasil'}, { label: 'Detail Hasil Intermediate Outcome', active: true }]
    } else if (this.from === 'Midterm') {
      this.selectedMonevTypeId = 'd1607d84-d1ed-11ec-88c9-937d18396f6b'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Midterm Outcome'},{ label: 'Hasil'}, { label: 'Detail Hasil Midterm Outcome', active: true }]
    } else if (this.from === 'Longterm') {
      this.selectedMonevTypeId = 'bb34f0da-d1ed-11ec-88c8-432a3415376c'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Longterm Outcome'},{ label: 'Hasil'}, { label: 'Detail Hasil Longterm Outcome', active: true }]
    }
    this.detailMonev()
    this.stepperNavbarName()
    this.validationEditDetailMonev(this.date.getFullYear())
    this.checkedButton = this.date.getFullYear()
  }

  stepperNavbarName() {
    if (this.from === 'output') {
      this.carouselListIndicator = 'Indikator Output Kegiatan'
      this.separatorDetail = 'Output Kegiatan'
      this.evaluationDetail = 'Penilaian Output Kegiatan'
    } else if (this.from === 'intermediate') {
      this.carouselListIndicator = 'Indikator Intermediate Outcome'
      this.separatorDetail = 'Intermediate Outcome'
      this.evaluationDetail = 'Penilaian Intermediate Outcome'
    } else if (this.from === 'Midterm') {
      this.carouselListIndicator = 'Indikator Midterm Outcome'
      this.separatorDetail = 'Midterm Outcome'
      this.evaluationDetail = 'Penilaian Midterm Outcome'
    } else if (this.from === 'Longterm') {
      this.carouselListIndicator = 'Indikator Longterm Outcome'
      this.separatorDetail = 'Longterm Outcome'
      this.evaluationDetail = 'Penilaian Longterm Outcome'
    }
  }

  showMoreFileDocumentation(showMore: boolean) {
    this.showMoreDocumentation = showMore;
  }

  refreshCalender(changeEvent: NgbNavChangeEvent) {
    this.date.setFullYear(changeEvent.nextId, 1, 1)
    this.resultAnswer = []
    this.validationEditDetailMonev(changeEvent.nextId)
    this.detailMonev()
    if (this.from !== 'output') {
      this.getListMonevChild()
    }
  }

  getListMonevChild() {
    let selectedYear = this.date.getFullYear()
    let params = {
      enablePage: false,
      page: 0,
      paramIs: [
        {
          field: "parrentId",
          dataType: "string",
          value: this.listMonev?.id
        }
      ],
      sort: [
        {
          field: "code",
          direction: "asc"
        }
      ]
    }

    this.serviceKegiatan.listResultChainAll(params).subscribe(
        resp => {
          if (resp.success) {
            this.childMonev = resp.data.content
            this.childMonev.forEach(x => {
              if (selectedYear === 2020) {
                x.resultStatusSelected = x.status2020
              } else if (selectedYear === 2021) {
                x.resultStatusSelected = x.status2021
              } else if (selectedYear === 2022) {
                x.resultStatusSelected = x.status2022
              } else if (selectedYear === 2023) {
                x.resultStatusSelected = x.status2023
              } else if (selectedYear === 2024) {
                x.resultStatusSelected = x.status2024
              }
            })
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  detailMonev() {
    let filesExistEveryYear = []
    let yearSelected = this.date.getFullYear()
    this.startLoading()
    this.serviceMonev.detailMonevResultChain(this.detailIdResultMonev).subscribe(
        resp => {
          if (resp.success) {
            this.listMonev = resp.data
            this.listMonev?.successIndicator?.forEach(x => {
              filesExistEveryYear = []
              if (yearSelected === 2020) {
                x.filesExist = []
                x.targetYearNow = x.cumulativeTarget2020
                x.resultIndicator = x.result2020
                x.notApplicableAllYear = x.notApplicable2020
                x.finalTarget = x.cumulativeTargetDummy2020
                if (x.files?.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2020) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
                this.resultStatus = this.listMonev?.status2020
              } else if (yearSelected === 2021) {
                x.targetYearNow = x.cumulativeTarget2021
                x.resultIndicator = x.result2021
                x.notApplicableAllYear = x.notApplicable2021
                x.finalTarget = x.cumulativeTargetDummy2021
                x.filesExist = []
                this.resultStatus = this.listMonev?.status2021
                if (x.files?.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2021) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
              } else if (yearSelected === 2022) {
                x.targetYearNow = x.cumulativeTarget2022
                x.resultIndicator = x.result2022
                x.notApplicableAllYear = x.notApplicable2022
                x.finalTarget = x.cumulativeTargetDummy2022
                x.filesExist = []
                this.resultStatus = this.listMonev?.status2022
                if (x.files?.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2022) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
              } else if (yearSelected === 2023) {
                x.filesExist = []
                x.targetYearNow = x.cumulativeTarget2023
                x.finalTarget = x.cumulativeTargetDummy2023
                x.notApplicableAllYear = x.notApplicable2023
                x.resultIndicator = x.result2023
                if (x.files?.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2023) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
                this.resultStatus = this.listMonev?.status2023
              } else if (yearSelected === 2024) {
                x.filesExist = []
                x.targetYearNow = x.cumulativeTarget2024
                x.resultIndicator = x.result2024
                x.finalTarget = x.cumulativeTargetDummy2024
                x.notApplicableAllYear = x.notApplicable2024
                if (x.files?.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2024) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
                this.resultStatus = this.listMonev?.status2024
              }
              const params  = {
                enablePage: false,
                page: 0,
                paramIs: [
                  {
                    field: "successIndicatorId",
                    dataType: "string",
                    value: x.id
                  },
                  {
                    field: "year",
                    dataType: "int",
                    value: this.date.getFullYear()
                  }
                ],
                sort: [
                  {
                    field: "componentCode",
                    direction: "asc"
                  }
                ]
              }
              this.service.listResultContribution(params).subscribe(
                  resp => {
                    if (resp.success) {
                      x.rekaptulationActivity = resp.data.content
                    }
                  }, error => {
                    this.swalAlert.showAlertSwal(error)
                  }
              )
            })
            this.listMonev.questions.forEach(x => {
              if (yearSelected === 2020) {
                this.resultStatus = this.listMonev?.status2020
                if (x.year === 2020) {
                  if (x.number === 1) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 2) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 3) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  } else if (x.number === 4) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  }
                }
              } else if (yearSelected === 2021) {
                this.resultStatus = this.listMonev?.status2021
                if (x.year === 2021) {
                  if (x.number === 1) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 2) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 3) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  } else if (x.number === 4) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  }
                }
              } else if (yearSelected === 2022) {
                this.resultStatus = this.listMonev?.status2022
                if (x.year === 2022) {
                  if (x.number === 1) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 2) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 3) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  } else if (x.number === 4) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  }
                }
              } else if (yearSelected === 2023) {
                this.resultStatus = this.listMonev?.status2023
                if (x.year === 2023) {
                  if (x.number === 1) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 2) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 3) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  } else if (x.number === 4) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  }
                }
              } else if (yearSelected === 2024) {
                this.resultStatus = this.listMonev?.status2024
                if (x.year === 2024) {
                  if (x.number === 1) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 2) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: x.yesOrNoAnswer,
                      answer: x.answer
                    })
                  } else if (x.number === 3) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  } else if (x.number === 4) {
                    this.resultAnswer.push({
                      questionId: x.monevQuestion?.id,
                      number: x.number,
                      yesOrNo: x.monevQuestion?.yesOrNo,
                      questionName: x.monevQuestion?.question,
                      yesOrNoAnswer: null,
                      answer: x.answer
                    })
                  }
                }
              }
            })

            this.stopLoading()
            if (this.from !== 'output') {
              this.getListMonevChild()
            }
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  downloadFile(id, name) {
    // this.loadingDownloadPdf = true
    this.serviceEvent.exportFile(id, name).subscribe(
        resp => {
          // this.loadingDownloadPdf = false
        }, error => {
          // this.loadingDownloadPdf = false
        }
    )
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  validationEditDetailMonev(data) {
    let yearNow = new Date().getFullYear()
    if (this.roles.hasAuthority('Consultan')) {
      if (data <= yearNow) {
        this.validationEditMonev = false
      } else {
        this.validationEditMonev = true
      }
    } else {
      this.validationEditMonev = true
    }
  }

  changeYearResult() {
    let yearSelected = this.date.getFullYear()
    this.listSuccessIndicator.forEach(x => {

    })
  }

  goToListMonev() {
    if (this.from === 'output') {
      this.router.navigate(['monev/monev-result-output/' + this.from])
    } else if (this.from === 'intermediate') {
      this.router.navigate(['monev/monev-result-output/intermediate'])
    } else if (this.from === 'Midterm') {
      this.router.navigate(['monev/monev-result-output/Midterm'])
    } else if (this.from === 'Longterm') {
      this.router.navigate(['monev/monev-result-output/Longterm'])
    }
  }

  goToEditMonev() {
    this.router.navigate(['monev/monev-result/' + this.detailIdResultMonev +'/' + this.from + '/' + this.date.getFullYear()])
  }

  // /** * List Indicator Success */
  // listIndicatorSuccess() {
  //   this.startLoading()
  //   let yearSelected = this.date.getFullYear()
  //   const params = {
  //     enablePage: false,
  //     page: 0,
  //     paramLike: [],
  //     paramIs: [
  //       {
  //         field: "resultChainId",
  //         dataType: "string",
  //         value: this.detailIdResultMonev
  //       }
  //     ],
  //     paramIn: [],
  //     paramNotIn: [],
  //     sort: [
  //       {
  //         field: "successIndicatorCode",
  //         direction: "asc"
  //       }
  //     ]
  //   }
  //   this.serviceKegiatan.listIndicatorCode(params).subscribe(
  //       resp => {
  //         this.stopLoading()
  //         if (resp.success) {
  //           this.listSuccessIndicator = resp.data.content
  //           this.listSuccessIndicator.forEach(x => {
  //             if (yearSelected === 2020) {
  //               x.resultIndicator = x.result2020
  //             } else if (yearSelected === 2021) {
  //               x.resultIndicator = x.result2021
  //             } else if (yearSelected === 2022) {
  //               x.resultIndicator = x.result2022
  //             } else if (yearSelected === 2023) {
  //               x.resultIndicator = x.result2023
  //             } else if (yearSelected === 2024) {
  //               x.resultIndicator = x.result2024
  //             }
  //             this.startLoading()
  //             const params  = {
  //               enablePage: false,
  //               page: 0,
  //               paramIs: [
  //                 {
  //                   field: "successIndicatorId",
  //                   dataType: "string",
  //                   value: x.id
  //                 },
  //                 {
  //                   field: "year",
  //                   dataType: "int",
  //                   value: this.date.getFullYear()
  //                 }
  //               ],
  //               sort: [
  //                 {
  //                   field: "componentCode",
  //                   direction: "asc"
  //                 }
  //               ]
  //             }
  //             this.service.listResultContribution(params).subscribe(
  //                 resp => {
  //                   if (resp.success) {
  //                     this.stopLoading()
  //                     x.rekaptulationActivity = resp.data.content
  //                   }
  //                 }, error => {
  //                   this.stopLoading()
  //                   this.swalAlert.showAlertSwal(error)
  //                 }
  //             )
  //           })
  //           this.changeYearResult()
  //         }
  //       }, error => {
  //         this.stopLoading()
  //         this.swalAlert.showAlertSwal(error)
  //       }
  //   )
  // }

}
