import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailResultMonevComponent } from './detail-result-monev.component';

describe('DetailResultMonevComponent', () => {
  let component: DetailResultMonevComponent;
  let fixture: ComponentFixture<DetailResultMonevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailResultMonevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailResultMonevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
