import {Component, Input, OnInit} from '@angular/core';
import {ComponentModel} from "../../../../core/models/componentModel";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {ComponentService} from "../../../../core/services/Component/component.service";
import Swal from "sweetalert2";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Answer, Monev, Monev2} from "../../../../core/models/monev";
import {MonevService} from "../../../../core/services/Monev/monev.service";

@Component({
  selector: 'app-detail-monev',
  templateUrl: './detail-monev.component.html',
  styleUrls: ['./detail-monev.component.scss']
})
export class DetailMonevComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  todayDate: Date
  // loading
  finishLoadData = true
  finishLoadDataCount = 0
  listMonev: Monev2
  //componen
  selectedComponentExist: ComponentModel
  component: ComponentModel[]
  componentMaster: ComponentModel[]
  carouselListIndicator: string
  indicatorName: string
  checkedButton: number
  // sub componen
  selectedSubComponentExist: ComponentModel
  selectedSubComponentExistMaster: ComponentModel
  selectedSubComponentExistEdit: ComponentModel
  resultAnswer: Answer[] = []

  //data input
  target2020: boolean = false
  target2021: boolean = false
  target2022: boolean = false
  target2023: boolean = false
  target2024: boolean = false

  testing = 'testing'

  @Input() title: String;
  @Input() id: String;

  constructor(
      public modal: NgbActiveModal,
      public service: MonevService,
      private swalAlert: SwalAlert,
      private router: Router,
      private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.checkedButton = 1
    this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Output'}, { label: 'Target', active: true }]
    this.todayDate = new Date()
    // this.title = this.capitalizeFirstLetter(this.title);
    this.detailMonev()
    if (this.title === 'Target Output Kegiatan') {
      this.carouselListIndicator = 'Indikator Output Kegiatan'
      this.indicatorName = 'Output Kegiatan'
    } else if (this.title === 'Target Intermediate Outcome') {
      this.carouselListIndicator = 'Indikator Intermediate Outcome'
      this.indicatorName = 'Intermediate Outcome'
    } else if (this.title === 'Target Midterm Outcome') {
      this.carouselListIndicator = 'Indikator Midterm Outcome'
      this.indicatorName = 'Midterm Outcome'
    } else if (this.title === 'Target Longterm Outcome') {
      this.carouselListIndicator = 'Indikator Longterm Outcome'
      this.indicatorName = 'Longterm Outcome'
    }
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  detailMonev() {
    this.startLoading()
    this.service.detailMonevResultChain(this.id).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.resultAnswer = []
            this.listMonev = resp.data
            this.listMonev.questions.forEach(x => {
              if (this.listMonev?.year === 2020) {
                if (x.year === 2020) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              } else if (this.listMonev?.year === 2021) {
                if (x.year === 2021) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              } else if (this.listMonev?.year === 2022) {
                if (x.year === 2022) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              } else if (this.listMonev?.year === 2023) {
                if (x.year === 2023) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              } else if (this.listMonev?.year === 2024) {
                if (x.year === 2024) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              }
            })
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  goToEditMonev() {
    let detailName
    if (this.title === 'Target Output Kegiatan') {
      detailName = 'output'
    } else if (this.title === 'Target Intermediate Outcome') {
      detailName = 'intermediate'
    } else if (this.title === 'Target Midterm Outcome') {
      detailName = 'Midterm'
    } else if (this.title === 'Target Longterm Outcome') {
      detailName = 'Longterm'
    }
    this.closeModal()
    this.router.navigate(['monev/edit-monev/' + this.listMonev?.id +'/' + detailName])
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }


}
