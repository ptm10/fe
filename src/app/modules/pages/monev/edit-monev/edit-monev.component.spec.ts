import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditMonevComponent } from './edit-monev.component';

describe('EditMonevComponent', () => {
  let component: EditMonevComponent;
  let fixture: ComponentFixture<EditMonevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditMonevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditMonevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
