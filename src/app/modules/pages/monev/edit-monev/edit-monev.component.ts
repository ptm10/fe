import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SwalAlert} from "../../../../core/helpers/Swal";
import Swal from "sweetalert2";
import {ComponentModel} from "../../../../core/models/componentModel";
import {ComponentService} from "../../../../core/services/Component/component.service";
import {PDOModel} from "../../../../core/models/PDO.model";
import {MonevService} from "../../../../core/services/Monev/monev.service";
import {Monev, Monev2, MonevQuestion, Question, SuccessIndicator} from "../../../../core/models/monev";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-edit-monev',
  templateUrl: './edit-monev.component.html',
  styleUrls: ['./edit-monev.component.scss']
})
export class EditMonevComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  todayDate: Date
  detailIdTarget: string
  from: string
  selectedMonevTypeId: string
  parentMonevTypeId: string
  loadingListComponent = false
  validateYear = false
  parentName: string
  nameIndicator: string
  fieldSubstantiv: number
  listQuestionFromSelectedChainCode: Question[] = []

  // loading
  finishLoadData = true
  finishLoadDataCount = 0
  listMonev: Monev2
  loadingUpdateMonev = false
  // PDO
  listPdoAll: PDOModel[]
  listPdoAllMaster: PDOModel[]
  pdoRequest = []

  // IRI
  listIriAll: PDOModel[]
  listIriAllMaster: PDOModel[]
  iriRequest = []

  //data input
  outputIndicatorName: string
  target2020: boolean = false
  target2021: boolean = false
  target2022: boolean = false
  target2023: boolean = false
  target2024: boolean = false
  selectValuePdo: string[] = [];
  selectValueIri: string[] = [];
  listPDO: string[] = [];
  selectedPdo: string
  selectedIri: string
  baseLineValue: string
  totalTarget: string
  targetValue2020: string
  targetValue2021: string
  targetValue2022: string
  targetValue2023: string
  targetValue2024: string
  notApplicable2020: boolean = false
  notApplicable2021: boolean = false
  notApplicable2022: boolean = false
  notApplicable2023: boolean = false
  notApplicable2024: boolean = false
  // parent result chain code
  listSuccessIndicator: SuccessIndicator[]
  listSuccessIndicatorMaster: SuccessIndicator[]
  selectedSuccessIndicator: SuccessIndicator

  // parent result chain code
  listChainCodeParent: Monev2[]
  listChainCodeParentMaster: Monev2[]
  selectedChainCodeParent: Monev2

  // question
  newQuestion = []
  questionExisting = []
  selectNewQuestion: string
  selectValueQustion: string[] = [];
  withOptionInQuestion: boolean
  listQuestion: MonevQuestion[]
  listQuestionMaster: MonevQuestion[]
  deleteQuestionExsiting = []

  // disable form target
  disableFormTarget1 = false
  disableFormTarget2 = false
  disableFormTarget3 = false
  disableFormTarget4 = false
  disableFormTarget5 = false

  constructor(
      private http: HttpClient,
      private service: AwpService,
      private router: Router,
      private swalAlert: SwalAlert,
      private serviceComponent: ComponentService,
      private activatedRoute: ActivatedRoute,
      private serviceMonev: MonevService,
      private serviceKegiatan: KegiatanService,
      private modalService: NgbModal,

  ) { }

  ngOnInit(): void {
    this.todayDate = new Date()
    this.detailIdTarget = this.activatedRoute.snapshot.paramMap.get('id')
    this.from = this.activatedRoute.snapshot.paramMap.get('from')
    if (this.from === 'output') {
      this.selectedMonevTypeId = 'afedb8dc-d1ee-11ec-88cb-0ba398ed0298'
      this.parentMonevTypeId = 'df8ab4b0-d1ed-11ec-88ca-6f259751c604'
      this.parentName = 'Intermediate Outcome'
      this.nameIndicator = 'Output Kegiatan'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Output'}, { label: 'Target'}, { label: 'Edit Output', active: true }]
    } else if (this.from === 'intermediate') {
      this.selectedMonevTypeId = 'df8ab4b0-d1ed-11ec-88ca-6f259751c604'
      this.parentMonevTypeId = 'd1607d84-d1ed-11ec-88c9-937d18396f6b'
      this.parentName = 'Midterm Outcome'
      this.nameIndicator = 'Intermediate Outcome'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Intermediate Outcome'},{ label: 'Target'}, { label: 'Edit Intermediate Outcome', active: true }]
    } else if (this.from === 'Midterm') {
      this.selectedMonevTypeId = 'd1607d84-d1ed-11ec-88c9-937d18396f6b'
      this.parentMonevTypeId = 'bb34f0da-d1ed-11ec-88c8-432a3415376c'
      this.parentName = 'Longterm Outcome'
      this.nameIndicator = 'Midterm Outcome'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Midterm Outcome'},{ label: 'Target'}, { label: 'Edit Midterm Outcome', active: true }]
    } else if (this.from === 'Longterm') {
      this.parentMonevTypeId = null
      this.selectedMonevTypeId = 'bb34f0da-d1ed-11ec-88c8-432a3415376c'
      this.nameIndicator = 'Longterm Outcome'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Longterm Outcome'},{ label: 'Target'}, { label: 'Edit Longterm Outcome', active: true }]
    }
    this.fieldSubstantiv = 1
    this.detailMonev()
    this.dataPDO()
    this.dataIri()
  }

  goToListMonev() {
    if (this.from === 'output') {
      this.router.navigate(['monev/list-monev/' + this.from])
    } else if (this.from === 'intermediate') {
      this.router.navigate(['monev/monev-intermediate/intermediate'])
    } else if (this.from === 'Midterm') {
      this.router.navigate(['monev/monev-midterm/Midterm'])
    } else if (this.from === 'Longterm') {
      this.router.navigate(['monev/monev-longterm/Longterm'])
    }
  }

  changeMenuFieldProgress(menu: any) {
    this.fieldSubstantiv = menu
  }

  disabledStep1() {
    return (!this.targetValue2020 && !this.notApplicable2020) || (!this.targetValue2021 && !this.notApplicable2021) || (!this.targetValue2022 && !this.notApplicable2022) || (!this.targetValue2023 && !this.notApplicable2023)
        || (!this.targetValue2024 && !this.notApplicable2024)
  }

  disableInputTargetByApplicable(from: any) {
    if (from === 2020) {
      if (this.notApplicable2020) {
        this.target2020 = true
        this.disableFormTarget1 = true
        this.targetValue2020 = null
      } else {
        this.target2020 = false
        this.disableFormTarget1 = false
      }
    } else if (from === 2021) {
      if (this.notApplicable2021) {
        this.target2021 = true
        this.disableFormTarget2 = true
        this.targetValue2021 = null
      } else {
        this.target2021 = false
        this.disableFormTarget2 = false
      }
    } else if (from === 2022) {
      if (this.notApplicable2022) {
        this.target2022 = true
        this.disableFormTarget3 = true
        this.targetValue2022 = null
      } else {
        this.target2022 = false
        this.disableFormTarget3 = false
      }
    } else if (from === 2023) {
      if (this.notApplicable2023) {
        this.target2023 = true
        this.disableFormTarget4 = true
        this.targetValue2023 = null
      } else {
        this.target2023 = false
        this.disableFormTarget4 = false
      }
    } else if (from === 2024) {
      if (this.notApplicable2024) {
        this.target2024 = true
        this.disableFormTarget5 = true
        this.targetValue2024 = null
      } else {
        this.target2024 = false
        this.disableFormTarget5 = false
      }
    }
  }

  detailMonev() {
    this.startLoading()
    this.serviceMonev.detailMonevResultChain(this.detailIdTarget).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listMonev = resp.data
            this.listSuccessIndicator = JSON.parse(JSON.stringify(this.listMonev?.successIndicator))
            this.listSuccessIndicatorMaster = JSON.parse(JSON.stringify(this.listMonev?.successIndicator))
            this.outputIndicatorName = this.listMonev?.name
            if (this.listMonev?.questions?.length > 0) {
              this.listMonev?.questions?.forEach(x => {
                if (this.listMonev?.year === 2020) {
                  if (x.year === 2020) {
                    this.listQuestionFromSelectedChainCode.push(x)
                  }
                } else if (this.listMonev?.year === 2021) {
                  if (x.year === 2021) {
                    this.listQuestionFromSelectedChainCode.push(x)
                  }
                } else if (this.listMonev?.year === 2022) {
                  if (x.year === 2022) {
                    this.listQuestionFromSelectedChainCode.push(x)
                  }
                } else if (this.listMonev?.year === 2023) {
                  if (x.year === 2023) {
                    this.listQuestionFromSelectedChainCode.push(x)
                  }
                } else if (this.listMonev?.year === 2024) {
                  if (x.year === 2024) {
                    this.listQuestionFromSelectedChainCode.push(x)
                  }
                }
              })
            }
            this.checkValidationYear()
            if (this.from !== 'Longterm') {
              this.getListResultChainParent()
            }
            this.getListQuestion()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  checkValidationYear() {
    this.validateYear = this.listMonev?.year > this.todayDate.getFullYear();
  }

  disableInputTarget(from: any) {
    if (from === 2020) {
      if (!this.target2020) {
        this.notApplicable2020 = false
        this.disableFormTarget1 = false
      }
    } else if (from === 2021) {
      if (!this.target2021) {
        this.notApplicable2021 = false
        this.disableFormTarget2 = false
      }
    } else if (from === 2022) {
      if (!this.target2022) {
        this.notApplicable2022 = false
        this.disableFormTarget3 = false
      }
    } else if (from === 2023) {
      if (!this.target2023) {
        this.notApplicable2023 = false
        this.disableFormTarget4 = false
      }
    } else if (from === 2024) {
      if (!this.target2024) {
        this.notApplicable2024 = false
        this.disableFormTarget5 = false
      }
    }
  }

  disabledStep2() {
    let filteredNewQuestion
    let disable
    if (this.listQuestionFromSelectedChainCode?.length > 0) {
      if (this.newQuestion.length > 0) {
        filteredNewQuestion = this.newQuestion.filter(x => !x.question)
        if (filteredNewQuestion.length > 0) {
          disable = false
        } else {
          disable = true
        }
      } else {
        disable = true
      }
    } else if (this.newQuestion.length > 0) {
      filteredNewQuestion = this.newQuestion.filter(x => !x.question)
      if (filteredNewQuestion.length > 0) {
        disable = false
      } else {
        disable = true
      }
      // return filteredNewQuestion.length > 0;
    } else {
      disable = false
    }
    return disable
  }

  /** Question */
  addNewQuestions(withQuestion: number) {
    if (withQuestion === 1) {
      this.newQuestion.push({
        question: null,
        yesOrNo: true
      })
    } else {
      this.newQuestion.push({
        question: null,
        yesOrNo: false
      })
    }

  }

  // new tag
  CreateNew(question){
    return question
  }

  deleteNewQuestions(i) {
    this.newQuestion.splice(i, 1)
  }

  deleteNewQuestionsExisting(i, data) {
    let tag = []
    this.deleteQuestionExsiting.push(data.id)
    this.listQuestionFromSelectedChainCode.splice(i, 1)
    if (this.listQuestionFromSelectedChainCode.length > 0) {
      let data = this.listQuestion.filter(({ question: id1 }) => !this.listQuestionFromSelectedChainCode.some(({ monevQuestion: id2 }) => id2.question === id1))
      data.forEach(x => {
        tag.push(x.question)
      })
    } else {
      this.listQuestion.forEach(x => {
        tag.push(x.question)
      })
    }
    this.selectValueQustion = tag
  }

  getListQuestion() {
    let tag = []
    this.serviceMonev.listAllQuestion().subscribe(
        resp => {
          if (resp.success) {
            this.listQuestion = resp.data.content
            this.listQuestionMaster = resp.data.content
            if (this.listQuestionFromSelectedChainCode.length > 0) {
              let data = this.listQuestion.filter(({ question: id1 }) => !this.listQuestionFromSelectedChainCode.some(({ monevQuestion: id2 }) => id2.question === id1))
              data.forEach(x => {
                tag.push(x.question)
              })
            } else {
              this.listQuestion.forEach(x => {
                tag.push(x.question)
              })
            }
            this.selectValueQustion = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** End OfQuestion */

  /** List IndicatorSuccess */
  selectedListIndicator(data) {
    this.disableFormTarget1 = false
    this.disableFormTarget2 = false
    this.disableFormTarget3 = false
    this.disableFormTarget4 = false
    this.disableFormTarget5 = false
    this.selectedSuccessIndicator = null
    this.listSuccessIndicator = this.listSuccessIndicatorMaster
    this.selectedSuccessIndicator = data
    this.listSuccessIndicator = this.listSuccessIndicator.filter(x => x.id !== data.id)
    this.baseLineValue = this.selectedSuccessIndicator?.baselineValue
    this.totalTarget = this.selectedSuccessIndicator?.targetTotal
    this.targetValue2020 = this.selectedSuccessIndicator?.cumulativeTarget2020
    this.targetValue2021 = this.selectedSuccessIndicator?.cumulativeTarget2021
    this.targetValue2022 = this.selectedSuccessIndicator?.cumulativeTarget2022
    this.targetValue2023 = this.selectedSuccessIndicator?.cumulativeTarget2023
    this.targetValue2024 = this.selectedSuccessIndicator?.cumulativeTarget2024
    this.target2020 = this.selectedSuccessIndicator?.cumulativeTargetDummy2020
    this.target2021 = this.selectedSuccessIndicator?.cumulativeTargetDummy2021
    this.target2022 = this.selectedSuccessIndicator?.cumulativeTargetDummy2022
    this.target2023 = this.selectedSuccessIndicator?.cumulativeTargetDummy2023
    this.target2024 = this.selectedSuccessIndicator?.cumulativeTargetDummy2024
    this.notApplicable2020 = this.selectedSuccessIndicator?.notApplicable2020
    this.notApplicable2021 = this.selectedSuccessIndicator?.notApplicable2021
    this.notApplicable2022 = this.selectedSuccessIndicator?.notApplicable2022
    this.notApplicable2023 = this.selectedSuccessIndicator?.notApplicable2023
    this.notApplicable2024 = this.selectedSuccessIndicator?.notApplicable2024
    if (this.selectedSuccessIndicator?.pdo) {
      this.selectedPdo = this.selectedSuccessIndicator?.pdo?.name + ' - ' + this.selectedSuccessIndicator?.pdo?.description
    }
    if (this.selectedSuccessIndicator?.iri) {
      this.selectedIri = this.selectedSuccessIndicator?.iri?.name + ' - ' + this.selectedSuccessIndicator?.iri?.description
    }
    if (this.notApplicable2020) {
      this.target2020 = true
      this.disableFormTarget1 = true
      this.targetValue2020 = null
    }

    if (this.notApplicable2021) {
      this.target2021 = true
      this.disableFormTarget2 = true
      this.targetValue2021 = null
    }

    if (this.notApplicable2022) {
      this.target2022 = true
      this.disableFormTarget3 = true
      this.targetValue2022 = null
    }

    if (this.notApplicable2023) {
      this.target2023 = true
      this.disableFormTarget4 = true
      this.targetValue2023 = null
    }

    if (this.notApplicable2024) {
      this.target2024 = true
      this.disableFormTarget5 = true
      this.targetValue2024 = null
    }
  }


  removeSelectedIndicator() {
    this.selectedSuccessIndicator = null
    this.listSuccessIndicator = this.listSuccessIndicatorMaster
  }

  searchFilterSuccessIndicator(e) {
    const searchStr = e.target.value
    let filter = this.listSuccessIndicatorMaster
    if (this.selectedSuccessIndicator) {
      filter = filter.filter(x => x.id !== this.selectedSuccessIndicator.id)
    } else {
      filter = this.listSuccessIndicatorMaster
    }

    this.listSuccessIndicator = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  /** End List IndicatorSuccess */

  /** Parent Result Chain */
  getListResultChainParent() {
    this.loadingListComponent = true
    let params = {
      enablePage: false,
      page: 0,
      paramIs: [
        {
          field: "year",
          dataType: "int",
          value: this.listMonev?.year
        },
        {
          field: "monevTypeId",
          dataType: "string",
          value: this.parentMonevTypeId
        }
      ],
      sort: [
        {
          field: "code",
          direction: "asc"
        }
      ]
    }
    this.serviceKegiatan.listResultChainAll(params).subscribe(
        resp => {
          if (resp.success) {
            this.listChainCodeParent = resp.data.content
            this.listChainCodeParentMaster = resp.data.content
            this.loadingListComponent = false
            if (this.listMonev?.parrentId) {
              this.selectedChainCodeParent = this.listChainCodeParent.find(x => x.id === this.listMonev?.parrentId)
              this.listChainCodeParent = this.listChainCodeParent.filter(x => x.id !== this.selectedChainCodeParent?.id)
            }
          }
        }, error => {
          this.loadingListComponent = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedParent(data) {
    this.listChainCodeParent = this.listChainCodeParentMaster
    this.selectedChainCodeParent = null

    this.selectedChainCodeParent = data
    this.listChainCodeParent = this.listChainCodeParent.filter(x => x.id !== this.selectedChainCodeParent?.id)
  }

  searchFilterParent(e) {
    const searchStr = e.target.value
    let filter = this.listChainCodeParentMaster
    if (this.selectedChainCodeParent) {
      filter = filter.filter(x => x.id !== this.selectedChainCodeParent.id)
    } else {
      filter = this.listChainCodeParentMaster
    }

    this.listChainCodeParent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedParent() {
    this.selectedChainCodeParent = null
    this.listChainCodeParent = this.listChainCodeParentMaster
  }
  /** End Parent Result Chain */


  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  /** * Change model target */

  changeValueTargetYear(from: any) {
    if (from === 2020) {
      if (!this.targetValue2020) {
        this.target2020 = false
      }
    } else if (from === 2021) {
      if (!this.targetValue2020) {
        this.target2021 = false
      }
    } else if (from === 2022) {
      if (!this.targetValue2020) {
        this.target2022 = false
      }
    } else if (from === 2023) {
      if (!this.targetValue2020) {
        this.target2023 = false
      }
    } else if (from === 2024) {
      if (!this.targetValue2020) {
        this.target2024 = false
      }
    }
  }

  /** * List PDO */
  dataPDO() {
    let tag = []
    this.startLoading()
    this.service.pdo().subscribe(
        resp => {
          if (resp.success) {
            this.stopLoading()
            this.listPdoAll = resp.data.content
            this.listPdoAllMaster = resp.data.content
            this.listPdoAll.forEach(x => {
              let combineCodeName = x.name + ' - ' + x.description
              tag.push(combineCodeName)
            })
            this.selectValuePdo = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End List PDO */

  /** * List IRI */
  dataIri() {
    let tag = []
    this.startLoading()
    this.service.iri().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listPDO.forEach(x => {
              tag.push(x)
            })
            this.listIriAll = resp.data.content
            this.listIriAllMaster = resp.data.content
            this.listIriAll.forEach(x => {
              let combineCodeName = x.name + ' - ' + x.description
              tag.push(combineCodeName)
            })
            this.selectValueIri = tag
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End List IRI */

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }


  saveEditTarget(withDelete: number) {
    this.loadingUpdateMonev = true
    let findPdoExist
    let findIriExist
    let filterPdo
    let filterIri
    let selectedPdoToApi
    let selectedIriToApi
    let findPdo = JSON.parse(JSON.stringify(this.listPdoAllMaster))
    let findIri = JSON.parse(JSON.stringify(this.listIriAllMaster))
    let indicatorSuccessDelete = []
    let newQuestions = []
    let parentId

    if (withDelete === 1) {
      indicatorSuccessDelete.push(this.selectedSuccessIndicator.id)
    } else {
      indicatorSuccessDelete = []
    }

    if (this.selectedPdo) {
      findPdoExist = this.selectedPdo.split(' ')
      filterPdo = findPdo.filter(x => x.name === findPdoExist[0])
      if (filterPdo.length > 0) {
        selectedPdoToApi = filterPdo[0].id
      } else {
        selectedPdoToApi = null
      }
    } else {
      selectedPdoToApi = null
    }

    if (this.selectedIri) {
      findIriExist = this.selectedIri.split(' ')
      filterIri = findIri.filter(x => x.name === findIriExist[0])
      if (filterIri.length > 0) {
        selectedIriToApi = filterIri[0].id
      } else {
        selectedIriToApi = null
      }
    } else {
      selectedIriToApi = null
    }

    if (this.newQuestion.length > 0) {
      newQuestions = this.newQuestion
    } else {
      newQuestions = []
    }

    if (this.validateYear && this.from !== 'Longterm') {
      if (this.selectedChainCodeParent) {
        parentId = this.selectedChainCodeParent?.id
      } else {
        parentId = null
      }
    } else if (!this.validateYear && this.from !== 'Longterm'){
      parentId = this.listMonev?.parrentId
    } else {
      parentId = null
    }

    let params2 = {
      year: this.listMonev?.year,
      monevTypeId: this.selectedMonevTypeId,
      componentId: this.listMonev?.component?.id,
      subComponentId: this.listMonev?.subComponent?.id,
      code: this.listMonev?.code,
      name: this.outputIndicatorName,
      parrentId: parentId,
      newSuccessIndicators: [
        {
          code: this.selectedSuccessIndicator.code,
          name: this.selectedSuccessIndicator?.name,
          pdoId: selectedPdoToApi,
          iriId: selectedIriToApi,
          baselineValue: this.baseLineValue,
          targetDescription: null,
          targetTotal: this.totalTarget,
          cumulativeTarget2020: this.targetValue2020,
          cumulativeTargetDummy2020: this.target2020,
          notApplicable2020: this.notApplicable2020,
          cumulativeTarget2021: this.targetValue2021,
          cumulativeTargetDummy2021: this.target2021,
          notApplicable2021: this.notApplicable2021,
          cumulativeTarget2022: this.targetValue2022,
          cumulativeTargetDummy2022: this.target2022,
          notApplicable2022: this.notApplicable2022,
          cumulativeTarget2023: this.targetValue2023,
          cumulativeTargetDummy2023: this.target2023,
          notApplicable2023: this.notApplicable2023,
          cumulativeTarget2024: this.targetValue2024,
          cumulativeTargetDummy2024: this.target2024,
          notApplicable2024: this.notApplicable2024
        }
      ],
      deletedSuccessIndicatorsIds: indicatorSuccessDelete,
      newQuestions: newQuestions,
      deletedQuestionIds: this.deleteQuestionExsiting
    }
     this.serviceMonev.updateMonev(this.detailIdTarget, params2).subscribe(
        resp => {
          if (resp.success) {
            this.loadingUpdateMonev = false
            this.closeModal()
            this.goToListMonev()
          }
        }, error => {
          this.loadingUpdateMonev = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

}
