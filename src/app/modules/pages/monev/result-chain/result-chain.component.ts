import { Component, OnInit } from '@angular/core';
import {MonevService} from "../../../../core/services/Monev/monev.service";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {AllMonev, AllMonevMap, Answer, ChildMonev, Monev, Monev2} from "../../../../core/models/monev";
import {MessageService, TreeNode} from "primeng/api";
import {NgbModal, NgbNavChangeEvent} from "@ng-bootstrap/ng-bootstrap";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {Router} from "@angular/router";
import screenfull from 'screenfull'
@Component({
  selector: 'app-result-chain',
  templateUrl: './result-chain.component.html',
  styleUrls: ['./result-chain.component.scss']
})
export class ResultChainComponent implements OnInit {

  breadCrumbItems: Array<{}>;

  /**
   * Loading Variables
   */
  finishLoadData = true
  finishLoadDataCount = 0

  finishLoadDataDetail = true
  finishLoadDataCountDetail = 0

  listAllMonev: AllMonev[] = []
  listAllMonevMap: AllMonevMap[] = []

  // testing
  data1: TreeNode[];

  data2: TreeNode[];
  data3: TreeNode[];
  fullScreenMode: HTMLElement

  selectedNode: TreeNode;
  selectedNodeDetail: any
  checkedButton: number
  todayDate: Date
  date: Date

  // detail Monev
  listMonev: Monev2
  loadingUpdateMonev = false
  listSuccessIndicator: Monev[]
  resultStatus: number
  resultAnswer: Answer[] = []
  childMonev: ChildMonev[]
  validationEditMonev: boolean
  carouselListIndicator: string
  evaluationDetail: string
  separatorDetail: string
  from: string
  longTerm = 'bb34f0da-d1ed-11ec-88c8-432a3415376c'
  midTerm = 'd1607d84-d1ed-11ec-88c9-937d18396f6b'
  intermediate = 'df8ab4b0-d1ed-11ec-88ca-6f259751c604'
  output = 'afedb8dc-d1ee-11ec-88cb-0ba398ed0298'
  onFullScreen: boolean = false

  constructor(
      private serviceMonev: MonevService,
      private swalAlert: SwalAlert,
      private messageService: MessageService,
      private modalService: NgbModal,
      private service: AwpService,
      private serviceKegiatan: KegiatanService,
      private router: Router
  ) { }

  ngOnInit(): void {
    this.todayDate = new Date()
    this.date = new Date()
    this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Result Chain Diagram', active: true }];
    this.checkedButton = this.todayDate.getFullYear()
    this.getListAllMonev()
  }

  toggleFullScreen(codePart: HTMLElement) {
    if (screenfull.isEnabled) {
      screenfull.toggle(codePart);
    }
  }

  testing() {
    if (screenfull.isFullscreen) {
      return true
    } else {
      return false
    }
  }


  refreshCalender(changeEvent: NgbNavChangeEvent) {
    console.log(changeEvent.nextId)
    this.date.setFullYear(changeEvent.nextId, 1, 1)
    this.getListAllMonev()
  }

  getListAllMonev() {
    this.data3 = []
    let selectedYear = this.date.getFullYear()
    this.startLoading()
    this.serviceMonev.listAllMonev(selectedYear).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listAllMonev = resp.data
            this.recursiveAllListMonev(this.listAllMonev)
            this.listAllMonev = JSON.parse(JSON.stringify(this.listAllMonev))
            this.data3 = this.listAllMonev
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  recursiveAllListMonev(data: AllMonev[]) {
    let selectedYear = this.date.getFullYear()
    if (data.length > 0) {
      data?.forEach(x => {
        x.type = 'person'
        if (selectedYear === 2020) {
          x.resultStatusSelected = x.resultStatus2020
          if (x.resultStatusSelected === 1) {
            x.styleClass = 'notApplicable'
            x.statusSelected = 'Not Applicable'
          } else if (x.resultStatusSelected === 2) {
            x.styleClass = 'offTrack'
            x.statusSelected = 'Off-Track'
          } else if (x.resultStatusSelected === 3) {
            x.styleClass = 'partialyOnTrack'
            x.statusSelected = 'Partially On-Track'
          } else if (x.resultStatusSelected === 4) {
            x.styleClass = 'onTrack'
            x.statusSelected = 'On-Track'
          } else if (x.resultStatusSelected === 5) {
            x.styleClass = 'achived'
            x.statusSelected = 'Achieved'
          } else if (x.resultStatusSelected === null) {
            x.styleClass = null
            x.statusSelected = '-'
          }
        } else if (selectedYear === 2021) {
          x.resultStatusSelected = x.resultStatus2021
          if (x.resultStatusSelected === 1) {
            x.styleClass = 'notApplicable'
            x.statusSelected = 'Not Applicable'
          } else if (x.resultStatusSelected === 2) {
            x.styleClass = 'offTrack'
            x.statusSelected = 'Off-Track'
          } else if (x.resultStatusSelected === 3) {
            x.styleClass = 'partialyOnTrack'
            x.statusSelected = 'Partially On-Track'
          } else if (x.resultStatusSelected === 4) {
            x.styleClass = 'onTrack'
            x.statusSelected = 'On-Track'
          } else if (x.resultStatusSelected === 5) {
            x.styleClass = 'achived'
            x.statusSelected = 'Achieved'
          } else if (x.resultStatusSelected === null) {
            x.styleClass = null
            x.statusSelected = '-'
          }
        } else if (selectedYear === 2022) {
          x.resultStatusSelected = x.resultStatus2022
          if (x.resultStatusSelected === 1) {
            x.styleClass = 'notApplicable'
            x.statusSelected = 'Not Applicable'
          } else if (x.resultStatusSelected === 2) {
            x.styleClass = 'offTrack'
            x.statusSelected = 'Off-Track'
          } else if (x.resultStatusSelected === 3) {
            x.styleClass = 'partialyOnTrack'
            x.statusSelected = 'Partially On-Track'
          } else if (x.resultStatusSelected === 4) {
            x.styleClass = 'onTrack'
            x.statusSelected = 'On-Track'
          } else if (x.resultStatusSelected === 5) {
            x.styleClass = 'achived'
            x.statusSelected = 'Achieved'
          } else if (x.resultStatusSelected === null) {
            x.styleClass = null
            x.statusSelected = '-'
          }
        } else if (selectedYear === 2023) {
          x.resultStatusSelected = x.resultStatus2023
          if (x.resultStatusSelected === 1) {
            x.styleClass = 'notApplicable'
            x.statusSelected = 'Not Applicable'
          } else if (x.resultStatusSelected === 2) {
            x.styleClass = 'offTrack'
            x.statusSelected = 'Off-Track'
          } else if (x.resultStatusSelected === 3) {
            x.styleClass = 'partialyOnTrack'
            x.statusSelected = 'Partially On-Track'
          } else if (x.resultStatusSelected === 4) {
            x.styleClass = 'onTrack'
            x.statusSelected = 'On-Track'
          } else if (x.resultStatusSelected === 5) {
            x.styleClass = 'achived'
            x.statusSelected = 'Achieved'
          } else if (x.resultStatusSelected === null) {
            x.styleClass = null
            x.statusSelected = '-'
          }
        } else if (selectedYear === 2024) {
          x.resultStatusSelected = x.resultStatus2024
          if (x.resultStatusSelected === 1) {
            x.styleClass = 'notApplicable'
            x.statusSelected = 'Not Applicable'
          } else if (x.resultStatusSelected === 2) {
            x.styleClass = 'offTrack'
            x.statusSelected = 'Off-Track'
          } else if (x.resultStatusSelected === 3) {
            x.styleClass = 'partialyOnTrack'
            x.statusSelected = 'Partially On-Track'
          } else if (x.resultStatusSelected === 4) {
            x.styleClass = 'onTrack'
            x.statusSelected = 'On-Track'
          } else if (x.resultStatusSelected === 5) {
            x.styleClass = 'achived'
            x.statusSelected = 'Achieved'
          } else if (x.resultStatusSelected === null) {
            x.styleClass = null
            x.statusSelected = '-'
          }
        }
        if (x.children.length > 0) {
          this.recursiveAllListMonev(x.children)
        }
      })
    }
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  startLoadingDetail() {
    if (this.finishLoadDataCountDetail == 0) {
      this.finishLoadDataCountDetail ++
      this.finishLoadDataDetail = false
    } else {
      this.finishLoadDataCountDetail ++
    }
  }

  stopLoadingDetail() {
    this.finishLoadDataCountDetail --
    if (this.finishLoadDataCountDetail == 0) {
      this.finishLoadDataDetail = true
    }
  }

  openModal(selectedMenu: any, data: any) {
    this.listMonev = null
    this.resultAnswer = []
    this.listMonev = null
    this.childMonev = []
    this.listSuccessIndicator = []
    this.selectedNodeDetail = data
    console.log(data)
    this.modalService.open(selectedMenu, { size: 'xl', centered: true });
    this.detailMonev(data?.node?.id)
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  onNodeSelect(event) {
    console.log(event)
    this.messageService.add({severity: 'success', summary: 'Node Selected', detail: event.node.label});
  }

  /** * Detail Monev */

  stepperNavbarName() {
    if (this.from === 'output') {
      this.carouselListIndicator = 'Indikator Output Kegiatan'
      this.separatorDetail = 'Output Kegiatan'
      this.evaluationDetail = 'Penilaian Output Kegiatan'
    } else if (this.from === 'intermediate') {
      this.carouselListIndicator = 'Indikator Intermediate Outcome'
      this.separatorDetail = 'Intermediate Outcome'
      this.evaluationDetail = 'Penilaian Intermediate Outcome'
    } else if (this.from === 'Midterm') {
      this.carouselListIndicator = 'Indikator Midterm Outcome'
      this.separatorDetail = 'Midterm Outcome'
      this.evaluationDetail = 'Penilaian Midterm Outcome'
    } else if (this.from === 'Longterm') {
      this.carouselListIndicator = 'Indikator Longterm Outcome'
      this.separatorDetail = 'Longterm Outcome'
      this.evaluationDetail = 'Penilaian Longterm Outcome'
    }
  }

  detailMonev(data) {
    let yearSelected = this.date.getFullYear()
    this.startLoadingDetail()
    this.serviceMonev.detailMonevResultChain(data).subscribe(
        resp => {
          if (resp.success) {
            this.listMonev = resp.data
            if (this.listMonev?.monevTypeId === this.longTerm) {
              this.from = 'Longterm'
            } else if (this.listMonev?.monevTypeId === this.midTerm) {
              this.from = 'Midterm'
            } else if (this.listMonev?.monevTypeId === this.intermediate) {
              this.from = 'intermediate'
            } else if (this.listMonev?.monevTypeId === this.output) {
              this.from = 'output'
            }
            this.stepperNavbarName()

            this.listMonev?.successIndicator?.forEach(x => {
              if (yearSelected === 2020) {
                x.filesExist = []
                x.targetYearNow = x.cumulativeTarget2020
                x.resultIndicator = x.result2020
                x.notApplicableAllYear = x.notApplicable2020
                x.finalTarget = x.cumulativeTargetDummy2020
                this.resultStatus = this.listMonev?.status2020
              } else if (yearSelected === 2021) {
                x.targetYearNow = x.cumulativeTarget2021
                x.resultIndicator = x.result2021
                x.notApplicableAllYear = x.notApplicable2021
                x.finalTarget = x.cumulativeTargetDummy2021
                x.filesExist = []
                this.resultStatus = this.listMonev?.status2021
              } else if (yearSelected === 2022) {
                x.targetYearNow = x.cumulativeTarget2022
                x.resultIndicator = x.result2022
                x.notApplicableAllYear = x.notApplicable2022
                x.finalTarget = x.cumulativeTargetDummy2022
                x.filesExist = []
                this.resultStatus = this.listMonev?.status2022
              } else if (yearSelected === 2023) {
                x.filesExist = []
                x.targetYearNow = x.cumulativeTarget2023
                x.finalTarget = x.cumulativeTargetDummy2023
                x.notApplicableAllYear = x.notApplicable2023
                x.resultIndicator = x.result2023
                this.resultStatus = this.listMonev?.status2023
              } else if (yearSelected === 2024) {
                x.filesExist = []
                x.targetYearNow = x.cumulativeTarget2024
                x.resultIndicator = x.result2024
                x.finalTarget = x.cumulativeTargetDummy2024
                x.notApplicableAllYear = x.notApplicable2024
                this.resultStatus = this.listMonev?.status2024
              }
              const params  = {
                enablePage: false,
                page: 0,
                paramIs: [
                  {
                    field: "successIndicatorId",
                    dataType: "string",
                    value: x.id
                  },
                  {
                    field: "year",
                    dataType: "int",
                    value: this.date.getFullYear()
                  }
                ],
                sort: [
                  {
                    field: "componentCode",
                    direction: "asc"
                  }
                ]
              }
              this.service.listResultContribution(params).subscribe(
                  resp => {
                    if (resp.success) {
                      x.rekaptulationActivity = resp.data.content
                    }
                  }, error => {
                    this.swalAlert.showAlertSwal(error)
                  }
              )
            })
            // this.listIndicatorSuccess(data)
            this.stopLoadingDetail()
          }
        }, error => {
          this.stopLoadingDetail()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** * List Indicator Success */
  listIndicatorSuccess(data) {
    this.startLoadingDetail()
    let yearSelected = this.date.getFullYear()
    const params = {
      enablePage: false,
      page: 0,
      paramLike: [],
      paramIs: [
        {
          field: "resultChainId",
          dataType: "string",
          value: data
        }
      ],
      paramIn: [],
      paramNotIn: [],
      sort: [
        {
          field: "successIndicatorCode",
          direction: "asc"
        }
      ]
    }
    this.serviceKegiatan.listIndicatorCode(params).subscribe(
        resp => {
          this.stopLoadingDetail()
          if (resp.success) {
            this.listSuccessIndicator = resp.data.content
            this.listSuccessIndicator.forEach(x => {
              if (yearSelected === 2020) {
                x.resultIndicator = x.result2020
              } else if (yearSelected === 2021) {
                x.resultIndicator = x.result2021
              } else if (yearSelected === 2022) {
                x.resultIndicator = x.result2022
              } else if (yearSelected === 2023) {
                x.resultIndicator = x.result2023
              } else if (yearSelected === 2024) {
                x.resultIndicator = x.result2024
              }
              this.startLoadingDetail()
              const params  = {
                enablePage: false,
                page: 0,
                paramIs: [
                  {
                    field: "successIndicatorId",
                    dataType: "string",
                    value: x.id
                  },
                  {
                    field: "year",
                    dataType: "int",
                    value: this.date.getFullYear()
                  }
                ],
                sort: [
                  {
                    field: "componentCode",
                    direction: "asc"
                  }
                ]
              }
              this.service.listResultContribution(params).subscribe(
                  resp => {
                    if (resp.success) {
                      this.stopLoadingDetail()
                      x.rekaptulationActivity = resp.data.content
                    }
                  }, error => {
                    this.stopLoadingDetail()
                    this.swalAlert.showAlertSwal(error)
                  }
              )
            })
            this.changeYearResult()
          }
        }, error => {
          this.stopLoadingDetail()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeYearResult() {
    let yearSelected = this.date.getFullYear()
    console.log(yearSelected)
    this.listSuccessIndicator.forEach(x => {
      if (yearSelected === 2020) {
        x.targetYearNow = x.cumulativeTarget2020
        x.resultIndicator = x.result2020
      } else if (yearSelected === 2021) {
        x.targetYearNow = x.cumulativeTarget2021
        x.resultIndicator = x.result2021
      } else if (yearSelected === 2022) {
        x.targetYearNow = x.cumulativeTarget2022
        x.resultIndicator = x.result2022
        console.log(x.resultIndicator)
      } else if (yearSelected === 2023) {
        x.targetYearNow = x.cumulativeTarget2023
        x.resultIndicator = x.result2023
      } else if (yearSelected === 2024) {
        x.targetYearNow = x.cumulativeTarget2024
        x.resultIndicator = x.result2024
      }
    })
  }

  goToDetailMonev() {
    this.closeModal()
    let detailName
    if (this.listMonev?.monevTypeId === this.longTerm) {
      detailName = 'Longterm'
    } else if (this.listMonev?.monevTypeId === this.midTerm) {
      detailName = 'Midterm'
    } else if (this.listMonev?.monevTypeId === this.intermediate) {
      detailName = 'intermediate'
    } else if (this.listMonev?.monevTypeId === this.output) {
      detailName = 'output'
    }
    this.router.navigate(['monev/monev-detail-result/' + this.selectedNodeDetail?.node?.id +'/' + detailName + '/' + this.date.getFullYear()])
  }
}

