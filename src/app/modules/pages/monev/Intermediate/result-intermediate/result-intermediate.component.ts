import {Component, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {Monev, Monev2} from "../../../../../core/models/monev";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../../../core/services/Awp/awp.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SwalAlert} from "../../../../../core/helpers/Swal";
import {NgbModal, NgbNavChangeEvent} from "@ng-bootstrap/ng-bootstrap";
import {AuthfakeauthenticationService} from "../../../../../core/services/authfake.service";
import {leangueIndoDT} from "../../../../../shared/pagination-custom";
import {DataResponse} from "../../../../../core/models/dataresponse.models";
import {environment} from "../../../../../../environments/environment";
import {CommonCons} from "../../../../../shared/CommonCons";
import {DetailMonevComponent} from "../../detail-monev/detail-monev.component";

@Component({
  selector: 'app-result-intermediate',
  templateUrl: './result-intermediate.component.html',
  styleUrls: ['./result-intermediate.component.scss']
})
export class ResultIntermediateComponent implements OnInit {
  /**
   * Table Variables
   */
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  listMonev: Monev2[]
  filterYear: number
  selectedYear: number
  dataListYear = []
  dataListYearExist = []
  paramsLike = []
  paramsIs = []
  filterData: any
  from: any
  monevType: string
  selectedMonevType: any
  selectedMonevTypeId: any
  finishLoading = false
  date: Date
  checkedButton: number
  /**
   * Loading Variables
   */
  finishLoadData = true
  finishLoadDataCount = 0

  /**
   * Other Variables
   */
  start = 1
  modelMonev = []
  componentLogin: string
  breadCrumbItems: Array<{}>;

  constructor(private http: HttpClient,
              private service: AwpService,
              private router: Router,
              private route: ActivatedRoute,
              private swalAlert: SwalAlert,
              private activateRouter: ActivatedRoute,
              private modalService: NgbModal,
              public roles: AuthfakeauthenticationService,
  ) {

  }

  ngOnInit(): void {
    this.date = new Date()
    this.checkedButton = this.date.getFullYear()
    this.componentLogin = localStorage.getItem('code_component')
    this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Intermediate Outcome' }, { label: 'Hasil', active: true }];
    this.from = this.activateRouter.snapshot.paramMap.get('from')
    if (this.from === 'output') {
      this.monevType = 'Output Kegiatan'
      this.selectedMonevTypeId = 'afedb8dc-d1ee-11ec-88cb-0ba398ed0298'
    } else if (this.from === 'intermediate') {
      this.monevType = 'Intermediate Outcome'
      this.selectedMonevTypeId = 'df8ab4b0-d1ed-11ec-88ca-6f259751c604'
    } else if (this.from === 'Midterm') {
      this.monevType = 'Midterm Outcome'
      this.selectedMonevTypeId = 'd1607d84-d1ed-11ec-88c9-937d18396f6b'
    } else if (this.from === 'Longterm') {
      this.monevType = 'Longterm Outcome'
      this.selectedMonevTypeId = 'bb34f0da-d1ed-11ec-88c8-432a3415376c'
    }
    this.getDataTable()
  }

  refreshCalender(changeEvent: NgbNavChangeEvent) {
    this.date.setFullYear(changeEvent.nextId, 1, 1)
    this.getDataTable()
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.dataListYear = this.dataListYearExist.filter((product) => {
      return product.toString().search(searchStr.toString().toLowerCase()) !== -1
    });
  }

  getDataTable() {
    // this.finishLoadData = false
    this.paramsIs = []
    let filterDate  = {
      field: "year",
      dataType: "int",
      value: this.date.getFullYear()
    }
    this.selectedMonevType = {
      field: "monevTypeId",
      dataType: "string",
      value: this.selectedMonevTypeId
    }
    this.startLoading()
    this.paramsIs.push(this.selectedMonevType)
    this.paramsIs.push(filterDate)
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramsIs,
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'monev-result-chain/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.listMonev = resp.data.content;
            // if (this.listMonev) {
            //   this.finishLoadData = true
            // }
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.finishLoading = false
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{data: 'code'},{ data: 'code'},{ data: 'name'},{ data: 'status2020'},{ data: 'status2021'},{ data: 'status2022'},{ data: 'status2023'},{ data: 'status2024'},{ data: 'result'}]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.filterYear = this.selectedYear
    this.paramsLike = []
    if (this.filterData) {
      const resultChainCode = {
        field: "code",
        dataType: "string",
        value: this.filterData
      }
      const result =  {
        field: "name",
        dataType: "string",
        value: this.filterData
      }
      this.paramsLike.push(resultChainCode)
      this.paramsLike.push(result)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  goToEditMonev(data) {
    let detailName
    if (this.monevType === 'Output Kegiatan') {
      detailName = 'output'
    } else if (this.monevType === 'Intermediate Outcome') {
      detailName = 'intermediate'
    } else if (this.monevType === 'Midterm Outcome') {
      detailName = 'Midterm'
    } else if (this.monevType === 'Longterm Outcome') {
      detailName = 'Longterm'
    }
    this.router.navigate(['monev/monev-result/' + data +'/' + detailName + '/' + 1])
  }

  goToDetailMonev(id) {
    let detailName
    if (this.monevType === 'Output Kegiatan') {
      detailName = 'output'
    } else if (this.monevType === 'Intermediate Outcome') {
      detailName = 'intermediate'
    } else if (this.monevType === 'Midterm Outcome') {
      detailName = 'Midterm'
    } else if (this.monevType === 'Longterm Outcome') {
      detailName = 'Longterm'
    }
    this.router.navigate(['monev/monev-detail-result/' + id +'/' + detailName + '/' + 1])
  }
}
