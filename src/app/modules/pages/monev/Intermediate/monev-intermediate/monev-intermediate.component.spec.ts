import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonevIntermediateComponent } from './monev-intermediate.component';

describe('MonevIntermediateComponent', () => {
  let component: MonevIntermediateComponent;
  let fixture: ComponentFixture<MonevIntermediateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonevIntermediateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonevIntermediateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
