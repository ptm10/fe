import { Component, OnInit } from '@angular/core';
import {Answer, ChildMonev, Monev, Monev2} from "../../../../core/models/monev";
import {PDOModel} from "../../../../core/models/PDO.model";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {ComponentService} from "../../../../core/services/Component/component.service";
import {MonevService} from "../../../../core/services/Monev/monev.service";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {parse} from "date-fns";
import {Location} from '@angular/common';

@Component({
  selector: 'app-result-output',
  templateUrl: './result-output.component.html',
  styleUrls: ['./result-output.component.scss']
})
export class ResultOutputComponent implements OnInit {

  public ResultIndicator = ClassicEditor;

  breadCrumbItems: Array<{}>;
  todayDate: Date
  detailIdTarget: string
  from: string
  minDateYear: any
  maxDateYear: any
  date: Date
  listSuccessIndicator: Monev[]
  childMonev: Monev2[]
  // loading
  finishLoadData = true
  finishLoadDataCount = 0
  listMonev: Monev2
  loadingUpdateMonev = false
  navBarNameFirst: string
  navBarNameLast: string
  carouselListIndicator: string
  loadingGetChild = false

  // upload file
  filesIndex = 0
  filesUploadSuccess = []

  // PDO
  listPdoAll: PDOModel[]
  listPdoAllMaster: PDOModel[]
  pdoRequest = []
  successIndicatorIds = []

  // IRI
  listIriAll: PDOModel[]
  listIriAllMaster: PDOModel[]
  iriRequest = []
  resultAnswer: Answer[] = []

  //data input
  target2020: boolean = false
  target2021: boolean = false
  target2022: boolean = false
  target2023: boolean = false
  target2024: boolean = false
  selectValuePdo: string[] = [];
  selectValueIri: string[] = [];
  listPDO: string[] = [];
  selectedPdo: string
  selectedIri: string
  baseLineValue: string
  totalTarget: string
  targetValue2020: string
  targetValue2021: string
  targetValue2022: string
  targetValue2023: string
  targetValue2024: string

  resultQuestion1: boolean = null
  resultQuestion2: boolean = null
  resultQuestionReason1: string
  resultQuestionReason2: string
  resultQuestionReason3: string
  resultQuestionReason4: string

  resultStatus: number
  selectedYearFromDetail: any

  constructor(
      private http: HttpClient,
      private service: AwpService,
      private router: Router,
      private swalAlert: SwalAlert,
      private serviceComponent: ComponentService,
      private activatedRoute: ActivatedRoute,
      private serviceMonev: MonevService,
      private serviceKegiatan: KegiatanService,
      private location: Location
  ) { }

  ngOnInit(): void {
    this.minDateYear = new Date(2020, 1, 1)
    this.date = new Date()
    this.todayDate = new Date()
    this.maxDateYear = new Date()
    this.detailIdTarget = this.activatedRoute.snapshot.paramMap.get('id')
    this.from = this.activatedRoute.snapshot.paramMap.get('from')
    this.selectedYearFromDetail = this.activatedRoute.snapshot.paramMap.get('year')
    this.selectedYearFromDetail = Number(this.selectedYearFromDetail)
    if (this.selectedYearFromDetail === 1) {
      this.date = new Date()
    } else {
      this.date = new Date(this.selectedYearFromDetail, 1, 1)
    }
    if (this.from === 'output') {
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Output'}, { label: 'Hasil'}, { label: 'Edit', active: true }]
    } else if (this.from === 'intermediate') {
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Intermediate Outcome'},{ label: 'Hasil'}, { label: 'Edit', active: true }]
    } else if (this.from === 'Midterm') {
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Midterm Outcome'},{ label: 'Hasil'}, { label: 'Edit', active: true }]
    } else if (this.from === 'Longterm') {
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Longterm Outcome'},{ label: 'Hasil'}, { label: 'Edit', active: true }]
    }
    this.detailMonev()
    // this.listIndicatorSuccess()
    // if (this.from !== 'output') {
    //   this.getListMonevChild()
    // }
    this.stepperNavbarName()
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
      },
      language: 'en'
    };
  }

  stepperNavbarName() {
    if (this.from === 'output') {
      this.navBarNameFirst = 'Output Kegiatan'
      this.navBarNameLast = 'Penilaian Output Kegiatan'
      this.carouselListIndicator = 'Indikator Output Kegiatan'
    } else if (this.from === 'intermediate') {
      this.navBarNameFirst = 'Intermediate Outcome'
      this.navBarNameLast = 'Penilaian Intermediate Outcome'
      this.carouselListIndicator = 'Indikator Intermediate Outcome'
    } else if (this.from === 'Midterm') {
      this.navBarNameFirst = 'Midterm Outcome'
      this.navBarNameLast = 'Penilaian Midterm Outcome'
      this.carouselListIndicator = 'Indikator Midterm Outcome'
    } else if (this.from === 'Longterm') {
      this.navBarNameFirst = 'Longterm Outcome'
      this.navBarNameLast = 'Penilaian Longterm Outcome'
      this.carouselListIndicator = 'Indikator Longterm Outcome'
    }
  }

  goToListMonev() {
    this.location.back();
  }

  testing() {
    console.log(this.listMonev?.successIndicator)
  }

  validationForm() {
    let success
    let filterNotApplicable
    success = this.listMonev?.successIndicator.filter(x => !x.resultIndicator)
    if (success?.length > 0) {
      filterNotApplicable = success.filter(x => !x.notApplicableAllYear)
      console.log(filterNotApplicable)
      return filterNotApplicable.length > 0;
    } else {
      return false
    }
    // return success?.length > 0;
  }

  /** Input date calender */
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }


  detailMonev() {
    let filesExistEveryYear = []
    let yearSelected = this.date.getFullYear()
    this.startLoading()
    this.serviceMonev.detailMonevResultChain(this.detailIdTarget).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listMonev = resp.data
            this.listMonev?.successIndicator.forEach(x => {
              filesExistEveryYear = []
              if (yearSelected === 2020) {
                x.targetYearNow = x.cumulativeTarget2020
                x.finalTarget = x.cumulativeTargetDummy2020
                x.notApplicableAllYear = x.notApplicable2020
                x.resultIndicator = x.result2020
                x.deletedFilesUploadExist = []
                x.filesExist = []
                if (x.files?.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2020) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
                this.resultStatus = this.listMonev?.status2020
              } else if (yearSelected === 2021) {
                x.targetYearNow = x.cumulativeTarget2021
                x.finalTarget = x.cumulativeTargetDummy2021
                x.notApplicableAllYear = x.notApplicable2021
                x.resultIndicator = x.result2021
                x.deletedFilesUploadExist = []
                x.filesExist = []
                if (x.files?.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2021) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
                this.resultStatus = this.listMonev?.status2021
              } else if (yearSelected === 2022) {
                x.targetYearNow = x.cumulativeTarget2022
                x.resultIndicator = x.result2022
                x.finalTarget = x.cumulativeTargetDummy2022
                x.notApplicableAllYear = x.notApplicable2022
                x.deletedFilesUploadExist = []
                x.filesExist = []
                if (x.files?.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2022) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
                this.resultStatus = this.listMonev?.status2022
              } else if (yearSelected === 2023) {
                x.targetYearNow = x.cumulativeTarget2023
                x.resultIndicator = x.result2023
                x.finalTarget = x.cumulativeTargetDummy2023
                x.notApplicableAllYear = x.notApplicable2023
                x.deletedFilesUploadExist = []
                x.filesExist = []
                if (x.files?.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2023) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
                this.resultStatus = this.listMonev?.status2023
              } else if (yearSelected === 2024) {
                x.targetYearNow = x.cumulativeTarget2024
                x.resultIndicator = x.result2024
                x.finalTarget = x.cumulativeTargetDummy2024
                x.notApplicableAllYear = x.notApplicable2024
                x.deletedFilesUploadExist = []
                x.filesExist = []
                if (x.files.length > 0) {
                  x.files?.forEach(listFiles => {
                    if (listFiles.year === 2024) {
                      filesExistEveryYear.push(listFiles)
                    }
                  })
                  x.filesExist = filesExistEveryYear
                }
                this.resultStatus = this.listMonev?.status2024
              }

              const params  = {
                enablePage: false,
                page: 0,
                paramIs: [
                  {
                    field: "successIndicatorId",
                    dataType: "string",
                    value: x.id
                  },
                  {
                    field: "year",
                    dataType: "int",
                    value: this.date.getFullYear()
                  }
                ],
                sort: [
                  {
                    field: "componentCode",
                    direction: "asc"
                  }
                ]
              }
              this.service.listResultContribution(params).subscribe(
                  resp => {
                    if (resp.success) {
                      x.rekaptulationActivity = resp.data.content
                    }
                  }, error => {
                    this.swalAlert.showAlertSwal(error)
                  }
              )
            })
            this.listMonev.convertUpdated = parse(this.listMonev?.updatedAt, 'dd-MM-yyyy HH:mm:ss', new Date());
            this.listMonev.questions.forEach(x => {
              if (yearSelected === 2020) {
                this.resultStatus = this.listMonev?.status2020
                if (x.year === 2020) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              } else if (yearSelected === 2021) {
                this.resultStatus = this.listMonev?.status2021
                if (x.year === 2021) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              } else if (yearSelected === 2022) {
                this.resultStatus = this.listMonev?.status2022
                if (x.year === 2022) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              } else if (yearSelected === 2023) {
                this.resultStatus = this.listMonev?.status2023
                if (x.year === 2023) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              } else if (yearSelected === 2024) {
                this.resultStatus = this.listMonev?.status2024
                if (x.year === 2024) {
                  this.resultAnswer.push({
                    questionId: x.id,
                    number: x.number,
                    yesOrNo: x.monevQuestion?.yesOrNo,
                    questionName: x.monevQuestion?.question,
                    yesOrNoAnswer: x.yesOrNoAnswer,
                    answer: x.answer
                  })
                }
              }
            })
            this.getListMonevChild()
            this.dataPDO()
            this.dataIri()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  /** * Change model target */

  changeValueTargetYear(from: any) {
    if (from === 2020) {
      if (!this.targetValue2020) {
        this.target2020 = false
      }
    } else if (from === 2021) {
      if (!this.targetValue2020) {
        this.target2021 = false
      }
    } else if (from === 2022) {
      if (!this.targetValue2020) {
        this.target2022 = false
      }
    } else if (from === 2023) {
      if (!this.targetValue2020) {
        this.target2023 = false
      }
    } else if (from === 2024) {
      if (!this.targetValue2020) {
        this.target2024 = false
      }
    }
  }


  /** * List PDO */
  dataPDO() {
    let tag = []
    this.startLoading()
    this.service.pdo().subscribe(
        resp => {
          if (resp.success) {
            this.stopLoading()
            this.listPdoAll = resp.data.content
            this.listPdoAllMaster = resp.data.content
            this.listPdoAll.forEach(x => {
              let combineCodeName = x.name + ' - ' + x.description
              tag.push(combineCodeName)
            })
            this.selectValuePdo = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End List PDO */

  /** * List IRI */
  dataIri() {
    let tag = []
    this.startLoading()
    this.service.iri().subscribe(
        resp => {
          if (resp.success) {
            this.listPDO.forEach(x => {
              tag.push(x)
            })
            this.listIriAll = resp.data.content
            this.listIriAllMaster = resp.data.content
            this.listIriAll.forEach(x => {
              let combineCodeName = x.name + ' - ' + x.description
              tag.push(combineCodeName)
            })
            this.selectValueIri = tag
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End List IRI */

  /** * List Indicator Success */
  // listIndicatorSuccess() {
  //   let yearSelected = this.date.getFullYear()
  //   const params = {
  //     enablePage: false,
  //     page: 0,
  //     paramLike: [],
  //     paramIs: [
  //       {
  //         field: "resultChainId",
  //         dataType: "string",
  //         value: this.detailIdTarget
  //       }
  //     ],
  //     paramIn: [],
  //     paramNotIn: [],
  //     sort: [
  //       {
  //         field: "successIndicatorCode",
  //         direction: "asc"
  //       }
  //     ]
  //   }
  //   this.serviceKegiatan.listIndicatorCode(params).subscribe(
  //       resp => {
  //         if (resp.success) {
  //           this.listSuccessIndicator = resp.data.content
  //           this.listSuccessIndicator = this.listSuccessIndicator.map(x => (
  //               {
  //                 ...x,resultIndicator: ''
  //               }
  //           ))
  //
  //           this.listSuccessIndicator.forEach(x => {
  //
  //           })
  //           // this.changeYearResult()
  //         }
  //       }, error => {
  //         this.swalAlert.showAlertSwal(error)
  //       }
  //   )
  // }

  changeYearResult() {
    this.resultAnswer = []
    let filesExistEveryYear = []
    let yearSelected = this.date.getFullYear()
    this.listMonev.questions.forEach(x => {
      if (yearSelected === 2020) {
        this.resultStatus = this.listMonev?.resultStatus2020
        if (x.year === 2020) {
          this.resultAnswer.push({
            questionId: x.id,
            number: x.number,
            yesOrNo: x.monevQuestion?.yesOrNo,
            questionName: x.monevQuestion?.question,
            yesOrNoAnswer: x.yesOrNoAnswer,
            answer: x.answer
          })
        }
      } else if (yearSelected === 2021) {
        this.resultStatus = this.listMonev?.resultStatus2021
        if (x.year === 2021) {
          this.resultAnswer.push({
            questionId: x.id,
            number: x.number,
            yesOrNo: x.monevQuestion?.yesOrNo,
            questionName: x.monevQuestion?.question,
            yesOrNoAnswer: x.yesOrNoAnswer,
            answer: x.answer
          })
        }
      } else if (yearSelected === 2022) {
        this.resultStatus = this.listMonev?.resultStatus2022
        if (x.year === 2022) {
          this.resultAnswer.push({
            questionId: x.id,
            number: x.number,
            yesOrNo: x.monevQuestion?.yesOrNo,
            questionName: x.monevQuestion?.question,
            yesOrNoAnswer: x.yesOrNoAnswer,
            answer: x.answer
          })
        }
      } else if (yearSelected === 2023) {
        this.resultStatus = this.listMonev?.resultStatus2023
        if (x.year === 2023) {
          this.resultAnswer.push({
            questionId: x.id,
            number: x.number,
            yesOrNo: x.monevQuestion?.yesOrNo,
            questionName: x.monevQuestion?.question,
            yesOrNoAnswer: x.yesOrNoAnswer,
            answer: x.answer
          })
        }
      } else if (yearSelected === 2024) {
        this.resultStatus = this.listMonev?.resultStatus2024
        if (x.year === 2024) {
          this.resultAnswer.push({
            questionId: x.id,
            number: x.number,
            yesOrNo: x.monevQuestion?.yesOrNo,
            questionName: x.monevQuestion?.question,
            yesOrNoAnswer: x.yesOrNoAnswer,
            answer: x.answer
          })
        }
      }
    })

    this.childMonev.forEach(x => {
      if (yearSelected === 2020) {
        x.resultIndicator = x.result2020
        x.resultStatusSelected = x.status2020
      } else if (yearSelected === 2021) {
        x.resultIndicator = x.result2021
        x.resultStatusSelected = x.status2021
      } else if (yearSelected === 2022) {
        x.resultIndicator = x.result2022
        x.resultStatusSelected = x.status2022
      } else if (yearSelected === 2023) {
        x.resultIndicator = x.result2023
        x.resultStatusSelected = x.status2023
      } else if (yearSelected === 2024) {
        x.resultIndicator = x.result2024
        x.resultStatusSelected = x.status2024
      }
    })

    this.listMonev?.successIndicator.forEach(x => {
      filesExistEveryYear = []
      if (yearSelected === 2020) {
        x.targetYearNow = x.cumulativeTarget2020
        x.resultIndicator = x.result2020
        x.finalTarget = x.cumulativeTargetDummy2020
        x.notApplicableAllYear = x.notApplicable2020
        x.deletedFilesUploadExist = []
        x.filesExist = []
        if (x.files?.length > 0) {
          x.files?.forEach(listFiles => {
            if (listFiles.year === 2020) {
              filesExistEveryYear.push(listFiles)
            }
          })
          x.filesExist = filesExistEveryYear
        }
        this.resultStatus = this.listMonev?.status2020
      } else if (yearSelected === 2021) {
        x.targetYearNow = x.cumulativeTarget2021
        x.resultIndicator = x.result2021
        x.finalTarget = x.cumulativeTargetDummy2021
        x.notApplicableAllYear = x.notApplicable2021
        x.deletedFilesUploadExist = []
        x.filesExist = []
        if (x.files?.length > 0) {
          x.files?.forEach(listFiles => {
            if (listFiles.year === 2021) {
              filesExistEveryYear.push(listFiles)
            }
          })
          x.filesExist = filesExistEveryYear
        }
        this.resultStatus = this.listMonev?.status2021
      } else if (yearSelected === 2022) {
        x.targetYearNow = x.cumulativeTarget2022
        x.resultIndicator = x.result2022
        x.finalTarget = x.cumulativeTargetDummy2022
        x.notApplicableAllYear = x.notApplicable2022
        x.deletedFilesUploadExist = []
        x.filesExist = []
        if (x.files?.length > 0) {
          x.files?.forEach(listFiles => {
            if (listFiles.year === 2022) {
              filesExistEveryYear.push(listFiles)
            }
          })
          x.filesExist = filesExistEveryYear
        }
        this.resultStatus = this.listMonev?.status2022
      } else if (yearSelected === 2023) {
        x.targetYearNow = x.cumulativeTarget2023
        x.resultIndicator = x.result2023
        x.finalTarget = x.cumulativeTargetDummy2023
        x.notApplicableAllYear = x.notApplicable2023
        x.deletedFilesUploadExist = []
        x.filesExist = []
        if (x.files?.length > 0) {
          x.files?.forEach(listFiles => {
            if (listFiles.year === 2023) {
              filesExistEveryYear.push(listFiles)
            }
          })
          x.filesExist = filesExistEveryYear
        }
        this.resultStatus = this.listMonev?.status2023
      } else if (yearSelected === 2024) {
        x.targetYearNow = x.cumulativeTarget2024
        x.resultIndicator = x.result2024
        x.finalTarget = x.cumulativeTargetDummy2024
        x.notApplicableAllYear = x.notApplicable2024
        x.deletedFilesUploadExist = []
        x.filesExist = []
        if (x.files?.length > 0) {
          x.files?.forEach(listFiles => {
            if (listFiles.year === 2024) {
              filesExistEveryYear.push(listFiles)
            }
          })
          x.filesExist = filesExistEveryYear
        }
        this.resultStatus = this.listMonev?.status2024
      }

      const params  = {
        enablePage: false,
        page: 0,
        paramIs: [
          {
            field: "successIndicatorId",
            dataType: "string",
            value: x.id
          },
          {
            field: "year",
            dataType: "int",
            value: this.date.getFullYear()
          }
        ],
        sort: [
          {
            field: "componentCode",
            direction: "asc"
          }
        ]
      }
      this.service.listResultContribution(params).subscribe(
          resp => {
            if (resp.success) {
              x.rekaptulationActivity = resp.data.content
            }
          }, error => {
            this.swalAlert.showAlertSwal(error)
          }
      )
    })
    // this.listIndicatorSuccess()
    // this.detailMonev()
    this.getListMonevChild()
  }

  removeFilesExisting(data) {
    let deletedDataExisting = []
    console.log(data)
    this.listMonev?.successIndicator?.forEach(x => {
      x.filesExist.forEach((check, index) => {
        if (data?.id === check.id) {
          deletedDataExisting.push(check.id)
          x.filesExist.splice(index, 1)
          if (x.deletedFilesUploadExist.length == 0) {
            x.deletedFilesUploadExist = deletedDataExisting
          } else {
            x.deletedFilesUploadExist.forEach(dataExist => {
              deletedDataExisting.push(dataExist)
            })
            x.deletedFilesUploadExist = deletedDataExisting
          }
        }
      })
      deletedDataExisting = []
    })

  }

  validationSaveResultTarget() {
    let resultQuestion1Check
    let resultQuestion2Check
    let resultQuestion3Check
    let resultQuestion4Check

    this.resultAnswer.forEach(x => {
      if (x.number === 1) {
        if (x.yesOrNo) {
          if (x.yesOrNoAnswer === true) {
            resultQuestion1Check = true
          } else if (x.yesOrNoAnswer === false){
            if (x.answer) {
              resultQuestion1Check = true
            } else {
              resultQuestion1Check = false
            }
          }
        } else {
          if (x.answer) {
            resultQuestion1Check = true
          } else {
            resultQuestion1Check = false
          }
        }

      }
      if (x.number === 2) {
        if (x.yesOrNo) {
          if (x.yesOrNoAnswer === true) {
            resultQuestion2Check = true
          } else if (x.yesOrNoAnswer === false){
            if (x.answer) {
              resultQuestion2Check = true
            } else {
              resultQuestion2Check = false
            }
          }
        } else {
          if (x.answer) {
            resultQuestion2Check = true
          } else {
            resultQuestion2Check = false
          }
        }
      }
      if (x.number === 3) {
        if (x.answer) {
          resultQuestion3Check = true
        } else {
          resultQuestion3Check = false
        }
      }
    })
    return !this.resultStatus
  }

  // getListMonevChild() {
  //   this.loadingGetChild = true
  //   this.childMonev = []
  //   let selectedYear = this.date.getFullYear()
  //
  //   this.serviceKegiatan.listResultChainAll(this.detailIdTarget).subscribe(
  //       resp => {
  //         if (resp.success) {
  //           this.childMonev = resp.data
  //           this.childMonev.forEach(x => {
  //             this.loadingGetChild = false
  //             if (selectedYear === 2020) {
  //               x.resultStatusSelected = x.resultStatus2020
  //             } else if (selectedYear === 2021) {
  //               x.resultStatusSelected = x.resultStatus2021
  //             } else if (selectedYear === 2022) {
  //               x.resultStatusSelected = x.resultStatus2022
  //             } else if (selectedYear === 2023) {
  //               x.resultStatusSelected = x.resultStatus2023
  //             } else if (selectedYear === 2024) {
  //               x.resultStatusSelected = x.resultStatus2024
  //             }
  //           })
  //         }
  //       }, error => {
  //         this.loadingGetChild = false
  //         this.swalAlert.showAlertSwal(error)
  //       }
  //   )
  // }

  getListMonevChild() {
    this.loadingGetChild = true
    let selectedYear = this.date.getFullYear()
    let params = {
      enablePage: false,
      page: 0,
      paramIs: [
        {
          field: "parrentId",
          dataType: "string",
          value: this.listMonev?.id
        }
      ],
      sort: [
        {
          field: "code",
          direction: "desc"
        }
      ]
    }
    this.serviceKegiatan.listResultChainAll(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingGetChild = false
            this.childMonev = resp.data.content
            this.childMonev.forEach(x => {
              if (selectedYear === 2020) {
                x.resultStatusSelected = x.status2020
              } else if (selectedYear === 2021) {
                x.resultStatusSelected = x.status2021
              } else if (selectedYear === 2022) {
                x.resultStatusSelected = x.status2022
              } else if (selectedYear === 2023) {
                x.resultStatusSelected = x.status2023
              } else if (selectedYear === 2024) {
                x.resultStatusSelected = x.status2024
              }
            })
          }
        }, error => {
          this.loadingGetChild = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  uploadResultIndicatorFiles(index) {
    this.loadingUpdateMonev = true
    if (this.listMonev?.successIndicator[index]?.filesUpload?.length > 0) {
      this.serviceMonev.uploadResultMonev(this.listMonev?.successIndicator[index]?.filesUpload[this.filesIndex]).subscribe(
          resp => {
            if (resp.success) {
              this.filesUploadSuccess.push(resp.data[0].id)
              this.filesIndex = this.filesIndex + 1
              if (this.filesIndex < this.listMonev?.successIndicator[index]?.filesUpload?.length) {
                this.uploadResultIndicatorFiles(index)
              } else {
                this.listMonev.successIndicator[index].filesUploadId = this.filesUploadSuccess
                index = index + 1
                this.filesIndex = 0
                if (index < this.listMonev?.successIndicator?.length) {
                  this.filesUploadSuccess = []
                  this.uploadResultIndicatorFiles(index)
                } else {
                  this.saveResultTarget()
                }
              }
            }
          }, error => {
            this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      index = index + 1
      if (index < this.listMonev?.successIndicator.length) {
        this.uploadResultIndicatorFiles(index)
      } else {
        this.saveResultTarget()
      }
    }
  }


  saveResultTarget() {
    let answersToApi = []
    this.successIndicatorIds = []
    this.successIndicatorIds = this.listMonev?.successIndicator.map(key => ({id: key.id, result: key.resultIndicator, newFilesIds: key.filesUploadId, deletedFilesIds: key.deletedFilesUploadExist}));
    if (this.resultQuestion1 === true) {
      this.resultQuestionReason1 = null
    }

    if (this.resultQuestion2 === true) {
      this.resultQuestionReason2 = null
    }

    if (!this.resultQuestionReason4) {
      this.resultQuestionReason4 = null
    }

    this.resultAnswer.forEach(x => {
      answersToApi.push({
        id: x.questionId,
        yesOrNoAnswer: x.yesOrNoAnswer,
        answer: x.answer
      })
    })

    const params = {
      year: this.date.getFullYear(),
      status: this.resultStatus,
      successIndicatorResult: this.successIndicatorIds,
      answers: answersToApi
    }
    this.serviceMonev.updateResult(this.detailIdTarget, params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingUpdateMonev = false
            this.goToListMonev()
          }
        }, error => {
          this.loadingUpdateMonev = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
