import {Component, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../core/services/Awp/awp.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DataTableDirective} from "angular-datatables";
import {SwalAlert} from "../../../core/helpers/Swal";
import {NgbModal, NgbNavChangeEvent} from "@ng-bootstrap/ng-bootstrap";
import {Monev, Monev2} from "../../../core/models/monev";
import {leangueIndoDT} from "../../../shared/pagination-custom";
import {DataResponse} from "../../../core/models/dataresponse.models";
import {environment} from "../../../../environments/environment";
import {CommonCons} from "../../../shared/CommonCons";
import {DetailMonevComponent} from "./detail-monev/detail-monev.component";
import {AuthfakeauthenticationService} from "../../../core/services/authfake.service";
import {KegiatanService} from "../../../core/services/kegiatan.service";
import {MonevService} from "../../../core/services/Monev/monev.service";

@Component({
  selector: 'app-monev',
  templateUrl: './monev.component.html',
  styleUrls: ['./monev.component.scss']
})
export class MonevComponent implements OnInit {

  /**
   * Table Variables
   */
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  listMonev: Monev2[]
  filterYear: number
  selectedYear: number
  dataListYear = []
  dataListYearExist = []
  paramsLike = []
  paramsIs = []
  filterData: any
  from: any
  monevType: string
  selectedMonevType: any
  selectedMonevTypeId: any
  finishLoading = false
  checkedButton: number
  date: Date
  selectedResultChainToDelete: Monev2
  allowDelete = false
  indicatorName: string
  /**
   * Loading Variables
   */
  finishLoadData = true
  finishLoadDataCount = 0

  /**
   * Other Variables
   */
  start = 1
  modelMonev = []
  componentLogin: string
  breadCrumbItems: Array<{}>;
  loadingDeleteResultChain = false

  constructor(private http: HttpClient,
              private service: AwpService,
              private router: Router,
              private route: ActivatedRoute,
              private swalAlert: SwalAlert,
              private activateRouter: ActivatedRoute,
              private modalService: NgbModal,
              public roles: AuthfakeauthenticationService,
              private serviceMonev: MonevService
  ) {

  }

  ngOnInit(): void {
    this.date = new Date()
    this.checkedButton = this.date.getFullYear()
    this.componentLogin = localStorage.getItem('code_component')
    this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Output' }, { label: 'Target', active: true }];
    this.from = this.activateRouter.snapshot.paramMap.get('from')
    if (this.from === 'output') {
      this.monevType = 'Target Output Kegiatan'
      this.indicatorName = 'Output Kegiatan'
      this.selectedMonevTypeId = 'afedb8dc-d1ee-11ec-88cb-0ba398ed0298'
    } else if (this.from === 'intermediate') {
      this.indicatorName = 'Intermediate Outcome'
      this.monevType = 'Target Intermediate Outcome'
      this.selectedMonevTypeId = 'df8ab4b0-d1ed-11ec-88ca-6f259751c604'
    } else if (this.from === 'Midterm') {
      this.monevType = 'Target Midterm Outcome'
      this.indicatorName = 'Midterm Outcome'
      this.selectedMonevTypeId = 'd1607d84-d1ed-11ec-88c9-937d18396f6b'
    } else if (this.from === 'Longterm') {
      this.indicatorName = 'Longterm Outcome'
      this.monevType = 'Target Longterm Outcome'
      this.selectedMonevTypeId = 'bb34f0da-d1ed-11ec-88c8-432a3415376c'
    }
    this.getDataTable()
  }

  refreshCalender(changeEvent: NgbNavChangeEvent) {
    this.date.setFullYear(changeEvent.nextId, 1, 1)
    this.validationDeleteYear()
    this.getDataTable()
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.dataListYear = this.dataListYearExist.filter((product) => {
      return product.toString().search(searchStr.toString().toLowerCase()) !== -1
    });
  }

  getDataTable() {
    this.startLoading()
    this.paramsIs = []
    this.selectedMonevType = {
      field: "monevTypeId",
      dataType: "string",
      value: this.selectedMonevTypeId
    }
    let filterDate  = {
      field: "year",
      dataType: "int",
      value: this.date.getFullYear()
    }
    console.log(filterDate)
    this.paramsIs.push(this.selectedMonevType)
    this.paramsIs.push(filterDate)
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramsIs,
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'monev-result-chain/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          if (resp.success) {
            this.stopLoading()
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.listMonev = resp.data.content;
            this.validationDeleteYear()
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.finishLoading = false
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{data: 'code'}, { data: 'code'}, { data: 'result'},{ data: 'result'},{ data: 'result'},]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.filterYear = this.selectedYear
    this.paramsIs = []
    this.selectedMonevType = {
      field: "monevTypeId",
      dataType: "string",
      value: this.selectedMonevTypeId
    }
    let filterDate  = {
      field: "year",
      dataType: "int",
      value: this.date.getFullYear()
    }
    this.paramsIs.push(this.selectedMonevType)
    this.paramsIs.push(filterDate)
    this.paramsLike = []
    if (this.filterData) {
      const resultChainCode = {
        field: "code",
        dataType: "string",
        value: this.filterData
      }
      const result =  {
        field: "name",
        dataType: "string",
        value: this.filterData
      }
      this.paramsLike.push(resultChainCode)
      this.paramsLike.push(result)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  goToEditMonev(data) {
    let detailName
    if (this.monevType === 'Target Output Kegiatan') {
      detailName = 'output'
    } else if (this.monevType === 'Target Intermediate Outcome') {
      detailName = 'intermediate'
    } else if (this.monevType === 'Target Midterm Outcome') {
      detailName = 'Midterm'
    } else if (this.monevType === 'Target Longterm Outcome') {
      detailName = 'Longterm'
    }
    this.router.navigate(['monev/edit-monev/' + data +'/' + detailName])
  }

  goToDetailMonev(id) {
    // this.router.navigate(['monev/detail-monev/test/' + this.from])
    const modalRef = this.modalService.open(DetailMonevComponent, {size: 'xl', centered: true });
    modalRef.componentInstance.title = this.monevType;
    modalRef.componentInstance.id = id;
  }

  goToCreateMonev() {
    let detailName
    if (this.monevType === 'Target Output Kegiatan') {
      detailName = 'output'
    } else if (this.monevType === 'Target Intermediate Outcome') {
      detailName = 'intermediate'
    } else if (this.monevType === 'Target Midterm Outcome') {
      detailName = 'Midterm'
    } else if (this.monevType === 'Target Longterm Outcome') {
      detailName = 'Longterm'
    }
    this.router.navigate(['monev/create-target-monev/' + detailName])
  }

  validationDeleteYear() {
    let todayDate = new Date().getFullYear()
    if (this.date.getFullYear() > todayDate) {
      this.allowDelete = true
    } else {
      this.allowDelete = false
    }
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any, data) {
    this.selectedResultChainToDelete = null
    this.selectedResultChainToDelete = data
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  deleteResultChain(data) {
    this.loadingDeleteResultChain = true
    this.serviceMonev.deleteResultChain(data).subscribe(
        resp => {
          if (resp.success) {
            this.loadingDeleteResultChain = false
            this.closeModal()
            this.refreshDatatable()
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
          this.loadingDeleteResultChain = false
        }
    )
  }
}
