import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {ModalComponent} from "./edit-monev/modal/modal.component";
import {
    NgbAccordionModule,
    NgbDatepickerModule,
    NgbDropdownModule,
    NgbModalModule,
    NgbNavModule,
    NgbPaginationModule, NgbRatingModule, NgbTimepickerModule,
    NgbTooltipModule
} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {Ng2SearchPipeModule} from "ng2-search-filter";
import {SimplebarAngularModule} from "simplebar-angular";
import {Ng5SliderModule} from "ng5-slider";
import {NgSelectModule} from "@ng-select/ng-select";
import {DataTablesModule} from "angular-datatables";
import {IConfig, NgxMaskModule} from "ngx-mask";
import {UIModule} from "../../../shared/ui/ui.module";
import {ArchwizardModule} from "angular-archwizard";
import {BsDatepickerModule} from "ngx-bootstrap/datepicker";
import {FileUploadModule} from "@iplab/ngx-file-upload";
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";
import {MonevComponent} from "./monev.component";
import {EditMonevComponent} from "./edit-monev/edit-monev.component";
import {DetailMonevComponent} from "./detail-monev/detail-monev.component";
import {MonevRoutingModule} from "./monev-routing.module";
import {SwalAlert} from "../../../core/helpers/Swal";
import { MonevTableComponent } from './OutputKegiatan/monev-table/monev-table.component';
import { MonevIntermediateComponent } from './Intermediate/monev-intermediate/monev-intermediate.component';
import { MonevMidtermComponent } from './Midterm/monev-midterm/monev-midterm.component';
import { MonevLongtermComponent } from './Longterm/monev-longterm/monev-longterm.component';
import { ResultOutputComponent } from './result-output/result-output.component';
import { ResultMidtermComponent } from './Midterm/result-midterm/result-midterm.component';
import { MonevResultOutputComponent } from './OutputKegiatan/monev-result-output/monev-result-output.component';
import { ResultIntermediateComponent } from './Intermediate/result-intermediate/result-intermediate.component';
import { ResultLongtermComponent } from './Longterm/result-longterm/result-longterm.component';
import { DetailResultMonevComponent } from './detail-result-monev/detail-result-monev.component';
import {OrganizationChartModule} from 'primeng/organizationchart';
import {ToastModule} from 'primeng/toast';
import {PanelModule} from 'primeng/panel';
import { ResultChainComponent } from './result-chain/result-chain.component';
import {MessageService} from "primeng/api";
import { CreateTargetMonevComponent } from './create-target-monev/create-target-monev.component';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [MonevComponent, EditMonevComponent, DetailMonevComponent, ModalComponent, MonevTableComponent, MonevIntermediateComponent, MonevMidtermComponent, MonevLongtermComponent, ResultOutputComponent, ResultMidtermComponent, MonevResultOutputComponent, ResultIntermediateComponent, ResultLongtermComponent, DetailResultMonevComponent, ResultChainComponent, CreateTargetMonevComponent],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        SimplebarAngularModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        MonevRoutingModule,
        DataTablesModule,
        NgbTooltipModule,
        NgxMaskModule.forRoot(),
        UIModule,
        ArchwizardModule,
        NgbDatepickerModule,
        BsDatepickerModule,
        NgbRatingModule,
        FileUploadModule,
        CKEditorModule,
        NgbTimepickerModule,
        NgbAccordionModule,
        OrganizationChartModule,
        ToastModule,
        PanelModule
    ],
  providers: [
      MessageService,
    SwalAlert
  ]
})
export class MonevModule { }
