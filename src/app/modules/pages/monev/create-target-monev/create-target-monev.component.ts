import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../../core/services/Awp/awp.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SwalAlert} from "../../../../core/helpers/Swal";
import {ComponentService} from "../../../../core/services/Component/component.service";
import {MonevService} from "../../../../core/services/Monev/monev.service";
import {PDOModel} from "../../../../core/models/PDO.model";
import Swal from "sweetalert2";
import {ComponentModel} from "../../../../core/models/componentModel";
import {KegiatanService} from "../../../../core/services/kegiatan.service";
import {Monev, Monev2, MonevQuestion, Question} from "../../../../core/models/monev";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-create-target-monev',
  templateUrl: './create-target-monev.component.html',
  styleUrls: ['./create-target-monev.component.scss']
})
export class CreateTargetMonevComponent implements OnInit {

  breadCrumbItems: Array<{}>;
  todayDate: Date
  from: string
  minDateYear: any
  maxDateYear: any
  loadingListComponent = false
  selectedCode: any = null
  selectedCodeIndicator: any = null
  validateYear = false
  indicatorName: string
  parentName: string
  parentId: string
  form1 = false
  form2 = false

  // loading
  finishLoadData = true
  finishLoadDataCount = 0
  loadingCreateIndicator = false

  // PDO
  listPdoAll: PDOModel[]
  listPdoAllMaster: PDOModel[]
  pdoRequest = []
  selectValuePdo: string[] = [];

  // IRI
  listIriAll: PDOModel[]
  listIriAllMaster: PDOModel[]
  iriRequest = []
  selectValueIri: string[] = [];

  // variable create
  baseLineValue: string
  totalTarget: string
  selectedPdo: string
  selectedIri: string
  successIndicator: string
  date: Date
  inputCodeManualOutput: string
  inputCodeManualIndicator: string
  outputDescription: string
  outputIndicatorDescription: string
  dataOutputKegiatan: any
  listDataSuccessIndicatorNew = []
  targetValue2020: string
  targetValue2021: string
  targetValue2022: string
  targetValue2023: string
  targetValue2024: string
  target2020: boolean = false
  target2021: boolean = false
  target2022: boolean = false
  target2023: boolean = false
  target2024: boolean = false
  notApplicable2020: boolean = false
  notApplicable2021: boolean = false
  notApplicable2022: boolean = false
  notApplicable2023: boolean = false
  notApplicable2024: boolean = false

  // list component
  component: ComponentModel[]
  componentMaster: ComponentModel[]

  // component
  selectedComponentExist: ComponentModel

  selectedSubComponentExist: ComponentModel
  selectedSubComponentExistMaster: ComponentModel
  selectedSubComponentExistEdit: ComponentModel

  selectedSubSubComponentExist: ComponentModel
  selectedSubSubComponentExistMaster: ComponentModel
  selectedSubSubComponentExistEdit: ComponentModel

  monevType: string
  selectedMonevTypeId: string
  codeInputSame = false
  codeIndicatorInputSame = false
  dataExistSuccessCode = []

  // result chain code
  listChainCode: Monev2[]
  listChainCodeMaster: Monev2[]
  listChainCodeFilter: Monev2[]
  listCodeSelected = []
  dataListIndicatorSuccess: Monev2[]
  newCodeSuccessIndicator: string
  newNameSuccessIndicator: string

  // parent result chain code
  listChainCodeParent: Monev2[]
  listChainCodeParentMaster: Monev2[]
  selectedChainCodeParent: Monev2
  listQuestionFromSelectedChainCode: Question[]

  // question
  newQuestion = []
  questionExisting = []
  selectNewQuestion: string
  selectValueQustion: string[] = [];
  withOptionInQuestion: boolean
  listQuestion: MonevQuestion[]

  // disable form target
  disableFormTarget1 = false
  disableFormTarget2 = false
  disableFormTarget3 = false
  disableFormTarget4 = false
  disableFormTarget5 = false


  constructor(
      private http: HttpClient,
      private service: AwpService,
      private router: Router,
      private swalAlert: SwalAlert,
      private serviceComponent: ComponentService,
      private activatedRoute: ActivatedRoute,
      private serviceMonev: MonevService,
      private serviceKegiatan: KegiatanService,
      private modalService: NgbModal,

  ) { }

  ngOnInit(): void {
    this.minDateYear = new Date(2020, 1, 1)
    this.maxDateYear = new Date(2024, 11, 30)
    this.todayDate = new Date()
    this.date = new Date()
    this.from = this.activatedRoute.snapshot.paramMap.get('from')
    this.changeNameIndicator()
    this.dataPDO()
    this.dataIri()
    this.allowCreateQuestion()
    this.getListComponent(this.date.getFullYear())
    this.getListResultChain()
  }

  changeNameIndicator() {
    if (this.from === 'output') {
      this.monevType = 'Output Kegiatan'
      this.selectedMonevTypeId = 'afedb8dc-d1ee-11ec-88cb-0ba398ed0298'
      this.indicatorName = 'Output Kegiatan'
      this.parentName = 'Intermediate Outcome'
      this.parentId = 'df8ab4b0-d1ed-11ec-88ca-6f259751c604'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Output'}, { label: 'Target'}, { label: 'Tambah Output', active: true }]
    } else if (this.from === 'intermediate') {
      this.monevType = 'Intermediate Outcome'
      this.selectedMonevTypeId = 'df8ab4b0-d1ed-11ec-88ca-6f259751c604'
      this.indicatorName = 'Intermediate Outcome'
      this.parentName = 'Midterm Outcome'
      this.parentId = 'd1607d84-d1ed-11ec-88c9-937d18396f6b'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Intermediate Outcome'},{ label: 'Target'}, { label: 'Tambah Intermediate Outcome', active: true }]
    } else if (this.from === 'Midterm') {
      this.monevType = 'Midterm Outcome'
      this.selectedMonevTypeId = 'd1607d84-d1ed-11ec-88c9-937d18396f6b'
      this.indicatorName = 'Midterm Outcome'
      this.parentName = 'Longterm Outcome'
      this.parentId = 'bb34f0da-d1ed-11ec-88c8-432a3415376c'
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Midterm Outcome'},{ label: 'Target'}, { label: 'Tambah Midterm Outcome', active: true }]
    } else if (this.from === 'Longterm') {
      this.monevType = 'Longterm Outcome'
      this.selectedMonevTypeId = 'bb34f0da-d1ed-11ec-88c8-432a3415376c'
      this.indicatorName = 'Longterm Outcome'
      this.parentName = null
      this.breadCrumbItems = [{ label: 'Monev' }, { label: 'Longterm Outcome'},{ label: 'Target'}, { label: 'Tambah Longterm Outcome', active: true }]
    }
  }

  validationCreateIndicatorWithoutComponent() {
    if (this.from === 'output') {
      return !this.selectedComponentExist
    } else if (this.from === 'intermediate') {
      return !this.selectedComponentExist
    } else if (this.from === 'Midterm') {
     return !this.date
    } else if (this.from === 'Longterm') {
      return !this.date
    }
  }

  changeValueTargetYear(from: any) {
    if (from === 2020) {
      if (!this.targetValue2020) {
        this.target2020 = false
      }
    } else if (from === 2021) {
      if (!this.targetValue2020) {
        this.target2021 = false
      }
    } else if (from === 2022) {
      if (!this.targetValue2020) {
        this.target2022 = false
      }
    } else if (from === 2023) {
      if (!this.targetValue2020) {
        this.target2023 = false
      }
    } else if (from === 2024) {
      if (!this.targetValue2020) {
        this.target2024 = false
      }
    }
  }

  disableInputTarget(from: any) {
    if (from === 2020) {
      if (!this.target2020) {
        this.notApplicable2020 = false
        this.disableFormTarget1 = false
      }
    } else if (from === 2021) {
      if (!this.target2021) {
        this.notApplicable2021 = false
        this.disableFormTarget2 = false
      }
    } else if (from === 2022) {
      if (!this.target2022) {
        this.notApplicable2022 = false
        this.disableFormTarget3 = false
      }
    } else if (from === 2023) {
      if (!this.target2023) {
        this.notApplicable2023 = false
        this.disableFormTarget4 = false
      }
    } else if (from === 2024) {
      if (!this.target2024) {
        this.notApplicable2024 = false
        this.disableFormTarget5 = false
      }
    }
  }

  disableInputTargetByApplicable(from: any) {
    if (from === 2020) {
      if (this.notApplicable2020) {
        this.target2020 = true
        this.disableFormTarget1 = true
        this.targetValue2020 = null
      } else {
        this.target2020 = false
        this.disableFormTarget1 = false
      }
    } else if (from === 2021) {
      if (this.notApplicable2021) {
        this.target2021 = true
        this.disableFormTarget2 = true
        this.targetValue2021 = null
      } else {
        this.target2021 = false
        this.disableFormTarget2 = false
      }
    } else if (from === 2022) {
      if (this.notApplicable2022) {
        this.target2022 = true
        this.disableFormTarget3 = true
        this.targetValue2022 = null
      } else {
        this.target2022 = false
        this.disableFormTarget3 = false
      }
    } else if (from === 2023) {
      if (this.notApplicable2023) {
        this.target2023 = true
        this.disableFormTarget4 = true
        this.targetValue2023 = null
      } else {
        this.target2023 = false
        this.disableFormTarget4 = false
      }
    } else if (from === 2024) {
      if (this.notApplicable2024) {
        this.target2024 = true
        this.disableFormTarget5 = true
        this.targetValue2024 = null
      } else {
        this.target2024 = false
        this.disableFormTarget5 = false
      }
    }
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   */
  openModal(selectedMenu: any) {
    this.modalService.open(selectedMenu, { size: 'lg', centered: true });
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }


  changeYearAwp() {
    this.allowCreateQuestion()
    this.newQuestion = []
    this.listQuestionFromSelectedChainCode = []
    this.codeIndicatorInputSame = false
    this.removeSelectedComponent()
    this.getListComponent(this.date.getFullYear())
    this.getListResultChain()
    this.clearDataIndicatorAndSuccess()
    if (this.from !== 'Longterm Outcome') {
      this.getListResultChainParent()
    }
  }

  clearDataIndicatorAndSuccess() {
    this.listQuestionFromSelectedChainCode = []
    this.listDataSuccessIndicatorNew = []
    this.dataExistSuccessCode = []
    this.selectedCodeIndicator = null
    this.inputCodeManualIndicator = null
    this.outputIndicatorDescription = null
    this.dataOutputKegiatan = null
    this.listCodeSelected = []
    this.selectedCode = null
    this.outputDescription = null
    this.inputCodeManualOutput = null
  }

  /** List Component */

  getListComponent(data) {
    this.loadingListComponent = true
    this.serviceComponent.listComponent(data).subscribe(
        resp => {
          this.loadingListComponent = false
          if (resp.success) {
            this.component = resp.data
            this.componentMaster = resp.data
            this.component.forEach(dataCode => {
              dataCode.code = 'Komponen '+ dataCode.code
            })
          } else {
            this.loadingListComponent = false
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.loadingListComponent = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  removeSelectedComponent() {
    this.component = this.componentMaster
    this.selectedComponentExist = null

    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null

    this.selectedSubSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.clearDataIndicatorAndSuccess()
  }

  searchFilterComponent(e) {
    const searchStr = e.target.value
    let filter = this.componentMaster
    if (this.selectedComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedComponentExist.id)
    } else {
      filter = this.componentMaster
    }

    this.component = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  selectedComponent(data) {
    this.clearDataIndicatorAndSuccess()
    this.selectedComponentExist = null
    this.selectedSubComponentExistMaster = null
    this.selectedSubComponentExistEdit = null
    this.component = this.componentMaster

    this.selectedComponentExist = data
    this.component = this.component.filter(dataFilter => dataFilter.id != this.selectedComponentExist.id)

    this.selectedSubComponentExistMaster = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExistMaster.subComponent.forEach(dataSub => {
      if (!dataSub.code.includes('Sub-Komponen')) {
        dataSub.code = 'Sub-Komponen '+dataSub.code
      }
    })
    this.selectedSubComponentExistEdit = Object.assign({}, this.selectedComponentExist)
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExist = null
    this.checkValidationYear()
    // this.codeSelectedFromComponent()
  }
  /** End List Component */


  /** Start Sub Component */
  removeSelectedSubComponent() {
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent
    this.selectedSubComponentExist = null
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubSubComponentExist = null
    this.clearSelectedCodeSubSubComponent()
  }

  clearSelectedCodeSubSubComponent() {
  }

  searchFilterSubComponent(e) {
    const searchStr = e.target.value
    let filter = this.selectedSubComponentExistEdit.subComponent
    if (this.selectedSubComponentExist) {
      filter = filter.filter(x => x.id !== this.selectedSubComponentExist.id)
    } else {
      filter = this.selectedSubComponentExistEdit.subComponent
    }

    this.selectedSubComponentExistMaster.subComponent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.description.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  selectedSubComponent(data) {
    this.selectedSubSubComponentExistMaster = null
    this.selectedSubSubComponentExistEdit = null
    this.selectedSubComponentExist = null
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistEdit.subComponent

    this.selectedSubComponentExist = data
    this.selectedSubComponentExistMaster.subComponent = this.selectedSubComponentExistMaster.subComponent.filter(dataFilter => dataFilter.id != this.selectedSubComponentExist.id)
    this.selectedSubSubComponentExistMaster = Object.assign({}, this.selectedSubComponentExist)
    if (this.selectedSubSubComponentExistMaster.subComponent.length > 0) {
      this.selectedSubSubComponentExistMaster.subComponent.forEach(dataSub => {
        if (!dataSub.code.includes('Komponen')) {
          dataSub.code = 'Komponen '+dataSub.code
        }
      })
    }
    this.selectedSubSubComponentExistEdit = Object.assign({}, this.selectedSubComponentExist)
    this.selectedSubSubComponentExist = null
  }
  /** End Sub Component */


  /** List Result Chain */
  getListResultChain() {
    let params = {
      enablePage: false,
      page: 0,
      paramIs: [
        {
          field: "year",
          dataType: "int",
          value: this.date.getFullYear()
        },
        {
          field: "monevTypeId",
          dataType: "string",
          value: this.selectedMonevTypeId
        }
      ],
      sort: [
        {
          field: "code",
          direction: "asc"
        }
      ]
    }
    this.serviceKegiatan.listResultChainAll(params).subscribe(
        resp => {
          if (resp.success) {
            this.listChainCode = resp.data.content
            this.listChainCodeMaster = resp.data.content
            this.listChainCodeFilter = resp.data.content
            if (this.from === 'Midterm' || this.from === 'Longterm') {
              this.checkValidationYear()
            }
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  changeListCodeIndicator() {
    console.log('masuk')
    this.listCodeSelected = []
    let codeFromComponentSelected
    let filterListResultChain

    if (this.from === 'output' || this.from === 'intermediate') {
      let codeComponentSelected = JSON.parse(JSON.stringify(this.selectedComponentExist))
      codeFromComponentSelected  = codeComponentSelected.code.replace('Komponen ', '')
      this.listChainCode.forEach(x => {
        if (x.code !== null) {
          if (x.code.charAt(2) === codeFromComponentSelected) {
            this.listCodeSelected.push(x)
          }
        }
      })
    } else {
      this.listChainCode.forEach(x => {
        if (x.code !== null) {
          this.listCodeSelected.push(x)
        }
      })
    }


    if (this.validateYear) {
      if (this.indicatorName === 'Output Kegiatan') {
        this.listCodeSelected.push({
          id: 1,
          code: 'O.'+ codeFromComponentSelected + '._',
          name: null
        })
      } else if (this.indicatorName === 'Intermediate Outcome') {
        this.listCodeSelected.push({
          id: 1,
          code: 'I.'+ codeFromComponentSelected + '._',
          name: null
        })
      } else if (this.indicatorName === 'Midterm Outcome') {
        this.listCodeSelected.push({
          id: 1,
          code: 'M'+ '._',
          name: null
        })
      } else if (this.indicatorName === 'Longterm Outcome') {
        this.listCodeSelected.push({
          id: 1,
          code: 'L' + '._',
          name: null
        })
      }
    }

    // if (this.indicatorName === 'Output Kegiatan') {
    //   this.listCodeSelected.push({
    //     id: 1,
    //     code: 'O.'+ codeFromComponentSelected + '._',
    //     name: null
    //   })
    // } else if (this.indicatorName === 'Intermediate Outcome') {
    //   this.listCodeSelected.push({
    //     id: 1,
    //     code: 'I.'+ codeFromComponentSelected + '._',
    //     name: null
    //   })
    // } else if (this.indicatorName === 'Midterm Outcome') {
    //   this.listCodeSelected.push({
    //     id: 1,
    //     code: 'M'+ '._',
    //     name: null
    //   })
    // } else if (this.indicatorName === 'Longterm Outcome') {
    //   this.listCodeSelected.push({
    //     id: 1,
    //     code: 'L' + '._',
    //     name: null
    //   })
    // }
    console.log(this.listCodeSelected)
  }

  checkValidationYear() {
    this.validateYear = this.date.getFullYear() > this.todayDate.getFullYear();
    this.changeListCodeIndicator()
  }

  allowCreateQuestion() {
    if (this.from === 'output' || this.from === 'Longterm') {
      this.form1 = false
      this.form2 = true
    } else {
      if (this.date.getFullYear() > this.todayDate.getFullYear()) {
        this.form1 = true
        this.form2 = false
      } else {
        this.form1 = false
        this.form2 = true
      }
    }
  }

  clearSelectedCode() {
    this.newQuestion = []
    this.listQuestionFromSelectedChainCode = []
    this.selectedCode = null
    this.outputDescription = null
    this.selectedCodeIndicator = null
    this.outputIndicatorDescription = null
    this.codeIndicatorInputSame = false
    this.codeInputSame = false
    this.inputCodeManualIndicator = null
  }

  clearSelectedCodeIndicator() {
    this.selectedCodeIndicator = null
    this.outputIndicatorDescription = null
  }

  selectedCodeFromList(data) {
    this.listQuestionFromSelectedChainCode = []
    this.selectedCode = null
    // this.listCodeSelected = this.listCodeExisting

    this.selectedCode = data
    console.log(this.selectedCode)
    this.outputDescription = this.selectedCode.name
    if (this.selectedCode?.questions) {
      this.selectedCode?.questions.forEach(x => {
        if (this.date?.getFullYear() === 2020) {
          if (x.year === 2020) {
            this.listQuestionFromSelectedChainCode.push(x)
          }
        } else if (this.date?.getFullYear() === 2021) {
          if (x.year === 2021) {
            this.listQuestionFromSelectedChainCode.push(x)
          }
        } else if (this.date?.getFullYear() === 2022) {
          if (x.year === 2022) {
            this.listQuestionFromSelectedChainCode.push(x)
          }
        } else if (this.date?.getFullYear() === 2023) {
          if (x.year === 2023) {
            this.listQuestionFromSelectedChainCode.push(x)
          }
        } else if (this.date?.getFullYear() === 2024) {
          if (x.year === 2024) {
            this.listQuestionFromSelectedChainCode.push(x)
          }
        }
      })
    }
    this.addOutputIndicator()
    // this.listCodeSelected = this.listCodeSelected.filter(data => data.id !== this.selectedCode.id)
  }

  addNewIndicatorList() {
    this.listDataSuccessIndicatorNew.push({
      code: this.dataOutputKegiatan?.codeIndicator + '.' + this.newCodeSuccessIndicator,
      name: this.newNameSuccessIndicator
    })
    this.newCodeSuccessIndicator = null
    this.newNameSuccessIndicator = null
    this.closeModal()
  }

  checkValidationCodeExist() {
    let filteredDataExist
    let newCode = this.selectedCode?.code + '.' + this.inputCodeManualIndicator

    if (this.selectedCode?.id !== 1) {
      console.log(newCode)
      let filteredDataExist = this.selectedCode?.successIndicator.filter(x => x.code.toLowerCase() === newCode.toLowerCase())
      if (filteredDataExist.length > 0) {
        this.codeIndicatorInputSame = true
      } else {
        this.codeIndicatorInputSame = false
      }
    }
  }

  /** End List Result Chain */

  /** Parent Result Chain */
  getListResultChainParent() {
    let params = {
      enablePage: false,
      page: 0,
      paramIs: [
        {
          field: "year",
          dataType: "int",
          value: this.date.getFullYear()
        },
        {
          field: "monevTypeId",
          dataType: "string",
          value: this.parentId
        }
      ],
      sort: [
        {
          field: "code",
          direction: "asc"
        }
      ]
    }
    this.serviceKegiatan.listResultChainAll(params).subscribe(
        resp => {
          if (resp.success) {
            this.listChainCodeParent = resp.data.content
            this.listChainCodeParentMaster = resp.data.content
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  selectedParent(data) {
    this.listChainCodeParent = this.listChainCodeParentMaster
    this.selectedChainCodeParent = null

    this.selectedChainCodeParent = data
    this.listChainCodeParent = this.listChainCodeParent.filter(x => x.id !== this.selectedChainCodeParent?.id)
  }

  searchFilterParent(e) {
    const searchStr = e.target.value
    let filter = this.listChainCodeParentMaster
    if (this.selectedChainCodeParent) {
      filter = filter.filter(x => x.id !== this.selectedChainCodeParent.id)
    } else {
      filter = this.listChainCodeParentMaster
    }

    this.listChainCodeParent = filter.filter((product) => {
      return ((product?.code.toLowerCase().search(searchStr.toLowerCase()) !== -1) || (product?.name.toLowerCase().search(searchStr.toLowerCase()) !== -1))
    });
  }

  removeSelectedParent() {
    this.selectedChainCodeParent = null
    this.listChainCodeParent = this.listChainCodeParentMaster
  }
  /** End Parent Result Chain */


  /** Output Result Chain */
  addOutputIndicator() {
    this.dataExistSuccessCode = []
    let selectedCodeResultChain = JSON.parse(JSON.stringify(this.selectedCode))
    if (this.selectedCode.id === 1) {
      let changeResultChain = selectedCodeResultChain.code.replace('_', '')
      this.dataOutputKegiatan = {
        codeIndicator: changeResultChain + this.inputCodeManualOutput,
        value: this.outputDescription
      }
      let newCode = {
        id: 1,
        code: changeResultChain + this.inputCodeManualOutput + '._',
        name: null
      }
      this.dataExistSuccessCode.push(newCode)

    } else {
      this.dataOutputKegiatan = {
        codeIndicator: this.selectedCode.code,
        value: this.outputDescription
      }
      if (this.selectedCode?.successIndicator.length > 0) {
        this.selectedCode?.successIndicator?.forEach(x => {
          this.dataExistSuccessCode.push(x)
        })
      }
      let newCode = {
        id: 1,
        code: this.selectedCode?.code + '._',
        name: null
      }
      this.dataExistSuccessCode.push(newCode)
      if (this.selectedCode?.questions.length > 0) {
        this.selectedCode?.questions.forEach(x => {
          this.questionExisting.push(x)
        })
      }
    }
    this.getListQuestion()
  }

  checkSameCodeOutputKegiatan() {
    let selectedCodeResultChain = JSON.parse(JSON.stringify(this.selectedCode))
    let changeResultChain = selectedCodeResultChain?.code.replace('_', '')
    let dataInputCode = changeResultChain + this.inputCodeManualOutput
    let findCodeExistSame = this.listCodeSelected.filter(x => x.code.toLowerCase() === dataInputCode.toLowerCase())
    if (findCodeExistSame.length > 0) {
      this.codeInputSame = true
    } else {
      this.codeInputSame = false
    }
    this.addOutputIndicator()
  }

  removeListIndicatorExisting(data) {
    this.dataExistSuccessCode.forEach((check, index) => {
      if (check.id === data.id) {}
      this.dataExistSuccessCode.splice(index, 1)
    })
  }

  removeListIndicatorNew(data) {
    this.listDataSuccessIndicatorNew.forEach((check, index) => {
      if (check.code === data.code) {
        this.listDataSuccessIndicatorNew.splice(index, 1)
      }
    })
  }

  validateNameOutputIndicator() {
    if (this.selectedCode) {
      if (this.selectedCode.id === 1) {
        if (this.inputCodeManualOutput) {
          if (this.codeInputSame) {
            return true
          } else {
            return false
          }
        } else {
          return true
        }
      } else {
        return false
      }
    } else {
      return true
    }
  }

  validateNameOutputIndicatorSuccess() {
    if (this.selectedCodeIndicator) {
      if (this.selectedCodeIndicator?.id === 1) {
        if (this.inputCodeManualIndicator) {
          if (this.codeIndicatorInputSame) {
            return true
          } else {
            return false
          }
        } else {
          return true
        }
      } else {
        return false
      }
    } else {
      return true
    }
  }

  selectedCodeIndicatorFromList(data) {
    if (data?.id === 1) {
      this.selectedCodeIndicator = null

      this.selectedCodeIndicator = data
      this.outputIndicatorDescription = this.selectedCodeIndicator?.name
    }
  }


  /** EndOutput Result Chain */


  /** Question */
  addNewQuestions(withQuestion: number) {
    if (withQuestion === 1) {
      this.newQuestion.push({
        question: null,
        yesOrNo: true
      })
    } else {
      this.newQuestion.push({
        question: null,
        yesOrNo: false
      })
    }

  }

  getListQuestion() {
    let tag = []
    this.serviceMonev.listAllQuestion().subscribe(
        resp => {
          if (resp.success) {
            this.listQuestion = resp.data.content
            if (this.questionExisting.length > 0) {
              let data = this.listQuestion.filter(({ question: id1 }) => !this.questionExisting.some(({ monevQuestion: id2 }) => id2.question === id1))
              data.forEach(x => {
                tag.push(x.question)
              })
            } else {
              this.listQuestion.forEach(x => {
                tag.push(x.question)
              })
            }
            this.selectValueQustion = tag
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  // new tag
  CreateNew(question){
    return question
  }

  deleteNewQuestions(i) {
    this.newQuestion.splice(i, 1)
  }
  /** End OfQuestion */


  /** Input date calender */
  onOpenCalendar(container) {
    container.monthSelectHandler = (event: any): void => {
      container._store.dispatch(container._actions.select(event.date));
    };
    container.setViewMode('month');
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  /** * List PDO */
  dataPDO() {
    let tag = []
    this.startLoading()
    this.service.pdo().subscribe(
        resp => {
          if (resp.success) {
            this.stopLoading()
            this.listPdoAll = resp.data.content
            this.listPdoAllMaster = resp.data.content
            this.listPdoAll.forEach(x => {
              let combineCodeName = x.name + ' - ' + x.description
              tag.push(combineCodeName)
            })
            this.selectValuePdo = tag
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End List PDO */

  /** * List IRI */
  dataIri() {
    let tag = []
    this.startLoading()
    this.service.iri().subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.listIriAll = resp.data.content
            this.listIriAllMaster = resp.data.content
            this.listIriAll.forEach(x => {
              let combineCodeName = x.name + ' - ' + x.description
              tag.push(combineCodeName)
            })
            this.selectValueIri = tag
            this.stopLoading()
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
  /** * End List IRI */

  goToListMonev() {
    if (this.from === 'output') {
      this.router.navigate(['monev/list-monev/' + this.from])
    } else if (this.from === 'intermediate') {
      this.router.navigate(['monev/monev-intermediate/intermediate'])
    } else if (this.from === 'Midterm') {
      this.router.navigate(['monev/monev-midterm/Midterm'])
    } else if (this.from === 'Longterm') {
      this.router.navigate(['monev/monev-longterm/Longterm'])
    }
  }

  disabledStep1() {
    if (this.from === 'output' || this.from === 'intermediate') {
      return !this.selectedComponentExist || !this.selectedSubComponentExist || !this.date || !this.selectedCode
          || !this.outputDescription || !this.selectedCodeIndicator  || !this.outputIndicatorDescription
          || (!this.targetValue2020 && !this.notApplicable2020) || (!this.targetValue2021 && !this.notApplicable2021) || (!this.targetValue2022 && !this.notApplicable2022) || (!this.targetValue2023 && !this.notApplicable2023)
          || (!this.targetValue2024 && !this.notApplicable2024)
    } else if (this.from === 'Midterm' || this.from === 'Longterm') {
      return !this.date || !this.selectedCode
          || !this.outputDescription || !this.selectedCodeIndicator  || !this.outputIndicatorDescription
          || (!this.targetValue2020 && !this.notApplicable2020) || (!this.targetValue2021 && !this.notApplicable2021) || (!this.targetValue2022 && !this.notApplicable2022) || (!this.targetValue2023 && !this.notApplicable2023)
          || (!this.targetValue2024 && !this.notApplicable2024)
    }
  }

  disabledStep2() {
    let filteredNewQuestion
    let disable
    if (this.listQuestionFromSelectedChainCode?.length > 0) {
      if (this.newQuestion.length > 0) {
        filteredNewQuestion = this.newQuestion.filter(x => !x.question)
        if (filteredNewQuestion.length > 0) {
          disable = false
        } else {
          disable = true
        }
      } else {
        disable = true
      }
    } else if (this.newQuestion.length > 0) {
      filteredNewQuestion = this.newQuestion.filter(x => !x.question)
      if (filteredNewQuestion.length > 0) {
        disable = false
      } else {
        disable = true
      }
      // return filteredNewQuestion.length > 0;
    } else {
      disable = false
    }
    return disable
  }

  createNewTarget() {
    this.loadingCreateIndicator = true
    let selectedPdoToApi
    let selectedIriToApi
    let findPdoExist
    let findIriExist
    let filterPdo
    let filterIri
    let findPdo = JSON.parse(JSON.stringify(this.listPdoAllMaster))
    let findIri = JSON.parse(JSON.stringify(this.listIriAllMaster))
    let dataSuccessIds = []
    let newIndicator = []
    let subComponentSelected
    let parentId
    let selectedComponent
    let newQuestions = []

    if (this.newQuestion.length > 0) {
      newQuestions = this.newQuestion
    } else {
      newQuestions = []
    }

    if (this.selectedPdo) {
      findPdoExist = this.selectedPdo.split(' ')
      filterPdo = findPdo.filter(x => x.name === findPdoExist[0])
      if (filterPdo.length > 0) {
        selectedPdoToApi = filterPdo[0].id
      } else {
        selectedPdoToApi = null
      }
    } else {
      selectedPdoToApi = null
    }

    if (this.selectedIri) {
      findIriExist = this.selectedIri.split(' ')
      filterIri = findIri.filter(x => x.name === findIriExist[0])
      if (filterIri.length > 0) {
        selectedIriToApi = filterIri[0].id
      } else {
        selectedIriToApi = null
      }
    } else {
      selectedIriToApi = null
    }

    if (this.selectedSubComponentExist) {
      subComponentSelected = this.selectedSubComponentExist.id
    } else {
      subComponentSelected = null
    }

    if (this.selectedComponentExist) {
      selectedComponent = this.selectedComponentExist.id
    } else {
      selectedComponent = null
    }

    if (this.selectedCodeIndicator?.id === 1) {
      if (this.selectedCode?.id === 1) {
        let selectedCodeResultChain = JSON.parse(JSON.stringify(this.selectedCode))
        let changeResultChain = selectedCodeResultChain?.code.replace('_', '')
        let dataInputCode = changeResultChain + this.inputCodeManualOutput + '.' + this.inputCodeManualIndicator
        newIndicator.push({
          code: dataInputCode,
          name: this.outputIndicatorDescription,
          baselineValue: this.baseLineValue,
          targetDescription: null,
          targetTotal: this.totalTarget,
          cumulativeTarget2020: this.targetValue2020,
          cumulativeTargetDummy2020: this.target2020,
          cumulativeTarget2021: this.targetValue2021,
          cumulativeTargetDummy2021: this.target2021,
          cumulativeTarget2022: this.targetValue2022,
          cumulativeTargetDummy2022: this.target2022,
          cumulativeTarget2023: this.targetValue2023,
          cumulativeTargetDummy2023: this.target2023,
          cumulativeTarget2024: this.targetValue2024,
          cumulativeTargetDummy2024: this.target2024,
          notApplicable2020: this.notApplicable2020,
          notApplicable2021: this.notApplicable2021,
          notApplicable2022: this.notApplicable2022,
          notApplicable2023: this.notApplicable2023,
          notApplicable2024: this.notApplicable2024,
          pdoId: selectedPdoToApi,
          iriId: selectedIriToApi,
        })
      }  else {
        let dataInputCode = this.selectedCode?.code + '.' + this.inputCodeManualIndicator
        newIndicator.push({
          code: dataInputCode,
          name: this.outputIndicatorDescription,
          baselineValue: this.baseLineValue,
          targetDescription: null,
          targetTotal: this.totalTarget,
          cumulativeTarget2020: this.targetValue2020,
          cumulativeTargetDummy2020: this.target2020,
          cumulativeTarget2021: this.targetValue2021,
          cumulativeTargetDummy2021: this.target2021,
          cumulativeTarget2022: this.targetValue2022,
          cumulativeTargetDummy2022: this.target2022,
          cumulativeTarget2023: this.targetValue2023,
          cumulativeTargetDummy2023: this.target2023,
          cumulativeTarget2024: this.targetValue2024,
          cumulativeTargetDummy2024: this.target2024,
          notApplicable2020: this.notApplicable2020,
          notApplicable2021: this.notApplicable2021,
          notApplicable2022: this.notApplicable2022,
          notApplicable2023: this.notApplicable2023,
          notApplicable2024: this.notApplicable2024,
          pdoId: selectedPdoToApi,
          iriId: selectedIriToApi,
        })
      }
    } else {
      newIndicator = []
    }

    if (this.selectedCode?.id === 1) {
      if (this.selectedChainCodeParent) {
        parentId = this.selectedChainCodeParent.id
      } else {
        parentId = null
      }
    } else {
      if (this.selectedCode?.parrentId) {
        parentId = this.selectedCode?.parrentId
      } else {
        parentId = null
      }
    }


    let params = {
      year: this.date.getFullYear(),
      monevTypeId: this.selectedMonevTypeId,
      componentId: selectedComponent,
      subComponentId: subComponentSelected,
      code: this.dataOutputKegiatan?.codeIndicator,
      name: this.outputDescription,
      newSuccessIndicators: newIndicator,
      newQuestions: newQuestions,
      parrentId: parentId
    }
    this.serviceMonev.createOutput(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingCreateIndicator = false
            this.goToListMonev()
          }
        }, error => {
          this.loadingCreateIndicator = false
          this.swalAlert?.showAlertSwal(error)
        }
    )
  }
}
