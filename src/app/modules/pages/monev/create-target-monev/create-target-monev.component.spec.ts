import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTargetMonevComponent } from './create-target-monev.component';

describe('CreateTargetMonevComponent', () => {
  let component: CreateTargetMonevComponent;
  let fixture: ComponentFixture<CreateTargetMonevComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateTargetMonevComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTargetMonevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
