import {Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {Monev} from "../../../../../core/models/monev";
import {HttpClient} from "@angular/common/http";
import {AwpService} from "../../../../../core/services/Awp/awp.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SwalAlert} from "../../../../../core/helpers/Swal";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {leangueIndoDT} from "../../../../../shared/pagination-custom";
import {DataResponse} from "../../../../../core/models/dataresponse.models";
import {environment} from "../../../../../../environments/environment";
import {CommonCons} from "../../../../../shared/CommonCons";
import {DetailMonevComponent} from "../../detail-monev/detail-monev.component";

@Component({
  selector: 'app-monev-table',
  templateUrl: './monev-table.component.html',
  styleUrls: ['./monev-table.component.scss']
})
export class MonevTableComponent implements OnInit, OnChanges {

  /**
   * Table Variables
   */

  @Input() selectedMonevTypeId: string
  @Input() monevType: string

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  listMonev: Monev[]
  filterYear: number
  selectedYear: number
  dataListYear = []
  dataListYearExist = []
  paramsLike = []
  paramsIs = []
  filterData: any
  from: any
  selectedMonevType: any
  finishLoading = true

  /**
   * Loading Variables
   */
  finishLoadData = false
  finishLoadDataCount = 0

  /**
   * Other Variables
   */
  start = 1
  modelMonev = []
  componentLogin: string
  breadCrumbItems: Array<{}>;

  constructor(private http: HttpClient,
              private service: AwpService,
              private router: Router,
              private route: ActivatedRoute,
              private swalAlert: SwalAlert,
              private activateRouter: ActivatedRoute,
              private modalService: NgbModal
  ) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.getDataTable()
    // this.refreshDataTable2()
  }

  ngOnInit(): void {
    this.getDataTable()
  }

  getDataTable() {
    this.finishLoadData = false
    this.paramsIs = []
    this.selectedMonevType = {
      field: "monevTypeId",
      dataType: "string",
      value: this.selectedMonevTypeId
    }
    this.startLoading()
    this.paramsIs.push(this.selectedMonevType)
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramsIs,
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'monev/datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            });
            this.listMonev = resp.data.content;
            if (this.listMonev) {
              this.finishLoadData = true
            }
            // this.finishLoadData = true
          } else {
            // this.finishLoadData = true
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.finishLoading = false
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{data: 'resultChainCode'},{ data: 'resultChainCode'},{ data: 'result'},{ data: 'successIndicatorCode'},{ data: 'successIndicator'},{ data: 'targetTotal'}]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.filterYear = this.selectedYear
    this.paramsLike = []
    if (this.filterData) {
      const resultChainCode = {
        field: "resultChainCode",
        dataType: "string",
        value: this.filterData
      }
      const result =  {
        field: "result",
        dataType: "string",
        value: this.filterData
      }
      const successIndicatorCode = {
        field: "successIndicatorCode",
        dataType: "string",
        value: this.filterData
      }
      const successIndicator = {
        field: "successIndicator",
        dataType: "string",
        value: this.filterData
      }
      const targetTotal = {
        field: "targetTotal",
        dataType: "string",
        value: this.filterData
      }
      this.paramsLike.push(resultChainCode)
      this.paramsLike.push(result)
      this.paramsLike.push(successIndicatorCode)
      this.paramsLike.push(successIndicator)
      this.paramsLike.push(targetTotal)
    } else {
      this.paramsLike = []
    }
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.dataListYear = this.dataListYearExist.filter((product) => {
      return product.toString().search(searchStr.toString().toLowerCase()) !== -1
    });
  }


  goToDetailMonev(id) {
    // this.router.navigate(['monev/detail-monev/test/' + this.from])
    const modalRef = this.modalService.open(DetailMonevComponent, {size: 'lg', centered: true});
    modalRef.componentInstance.title = this.monevType;
    modalRef.componentInstance.id = id;
  }

  goToEditMonev(data) {
    let detailName
    if (this.monevType === 'Target Output Kegiatan') {
      detailName = 'output'
    } else if (this.monevType === 'Target Intermediate Outcome') {
      detailName = 'intermediate'
    } else if (this.monevType === 'Target Midterm Outcome') {
      detailName = 'Midterm'
    } else if (this.monevType === 'Target Longterm Outcome') {
      detailName = 'Longterm'
    }
    this.router.navigate(['monev/edit-monev/' + data +'/' + detailName])
  }

  refreshDataTable2() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

}
