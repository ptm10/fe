import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonevTableComponent } from './monev-table.component';

describe('MonevTableComponent', () => {
  let component: MonevTableComponent;
  let fixture: ComponentFixture<MonevTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonevTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonevTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
