import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonevResultOutputComponent } from './monev-result-output.component';

describe('MonevResultOutputComponent', () => {
  let component: MonevResultOutputComponent;
  let fixture: ComponentFixture<MonevResultOutputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonevResultOutputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonevResultOutputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
