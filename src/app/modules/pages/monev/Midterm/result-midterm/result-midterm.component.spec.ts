import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultMidtermComponent } from './result-midterm.component';

describe('ResultMidtermComponent', () => {
  let component: ResultMidtermComponent;
  let fixture: ComponentFixture<ResultMidtermComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultMidtermComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultMidtermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
