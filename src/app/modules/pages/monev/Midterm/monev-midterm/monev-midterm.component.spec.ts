import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonevMidtermComponent } from './monev-midterm.component';

describe('MonevMidtermComponent', () => {
  let component: MonevMidtermComponent;
  let fixture: ComponentFixture<MonevMidtermComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonevMidtermComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonevMidtermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
