import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonevLongtermComponent } from './monev-longterm.component';

describe('MonevLongtermComponent', () => {
  let component: MonevLongtermComponent;
  let fixture: ComponentFixture<MonevLongtermComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonevLongtermComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonevLongtermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
