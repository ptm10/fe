import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultLongtermComponent } from './result-longterm.component';

describe('ResultLongtermComponent', () => {
  let component: ResultLongtermComponent;
  let fixture: ComponentFixture<ResultLongtermComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultLongtermComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultLongtermComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
