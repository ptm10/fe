import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MonevComponent} from "./monev.component";
import {DetailMonevComponent} from "./detail-monev/detail-monev.component";
import {EditMonevComponent} from "./edit-monev/edit-monev.component";
import {MonevIntermediateComponent} from "./Intermediate/monev-intermediate/monev-intermediate.component";
import {MonevMidtermComponent} from "./Midterm/monev-midterm/monev-midterm.component";
import {MonevLongtermComponent} from "./Longterm/monev-longterm/monev-longterm.component";
import {ResultOutputComponent} from "./result-output/result-output.component";
import {ResultMidtermComponent} from "./Midterm/result-midterm/result-midterm.component";
import {MonevResultOutputComponent} from "./OutputKegiatan/monev-result-output/monev-result-output.component";
import {ResultLongtermComponent} from "./Longterm/result-longterm/result-longterm.component";
import {ResultIntermediateComponent} from "./Intermediate/result-intermediate/result-intermediate.component";
import {DetailResultMonevComponent} from "./detail-result-monev/detail-result-monev.component";
import {ResultChainComponent} from "./result-chain/result-chain.component";
import {CreateTargetMonevComponent} from "./create-target-monev/create-target-monev.component";
import {AuthGuard} from "../../../core/guards/auth.guard";

const routes: Routes = [
  {
    path: 'list-monev/:from',
    component: MonevComponent, canActivate: [AuthGuard]
  },
  {
    path: 'detail-monev/:id/:from',
    component: DetailMonevComponent, canActivate: [AuthGuard]
  },
  {
    path: 'edit-monev/:id/:from',
    component: EditMonevComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-intermediate/:from',
    component: MonevIntermediateComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-midterm/:from',
    component: MonevMidtermComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-longterm/:from',
    component: MonevLongtermComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-result/:id/:from/:year',
    component: ResultOutputComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-result-midterm/:from',
    component: ResultMidtermComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-result-output/:from',
    component: MonevResultOutputComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-result-intermediate/:from',
    component: ResultIntermediateComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-result-longterm/:from',
    component: ResultLongtermComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-detail-result/:id/:from/:year',
    component: DetailResultMonevComponent, canActivate: [AuthGuard]
  },
  {
    path: 'monev-result-chain',
    component: ResultChainComponent, canActivate: [AuthGuard]
  },
  {
    path: 'create-target-monev/:from',
    component: CreateTargetMonevComponent, canActivate: [AuthGuard]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonevRoutingModule { }
