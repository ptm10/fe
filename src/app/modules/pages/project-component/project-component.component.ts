import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../core/services/Component/component.service";
import {ComponentModel} from "../../../core/models/componentModel";
import Swal from 'sweetalert2';
import {SwalAlert} from "../../../core/helpers/Swal";
import {NgbNavChangeEvent} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-project-component',
  templateUrl: './project-component.component.html',
  styleUrls: ['./project-component.component.scss']
})
export class ProjectComponentComponent implements OnInit {

  /**
   * variable declaration
   */
  openChildComponent = false
  openChildSubComponent = false
  openChildSubSubComponent = false
  openChildDetailSubSubComponent = false
  finishLoadData = false
  component: [ComponentModel]
  selectedComponent: ComponentModel
  selectedSubComponent: ComponentModel
  selectedSubSubComponent: ComponentModel
  selectedDetailSubSubComponent: ComponentModel


  openDetailChildComponent = false
  openDetailChildSubComponent = false
  openDetailChildSubSubComponent = false
  openDetailChildSubSubDetailComponent = false
  selectedDetailComponent: ComponentModel
  selectedDetailSubComponent: ComponentModel
  selectedSubSubDetailComponent: ComponentModel
  selectedViewDetailComponent: ComponentModel
  breadCrumbItems: Array<{}>;
  checkedButton: number
  todayDate: Date

  constructor(
      private service: ComponentService,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.breadCrumbItems = [{ label: 'Project Component', active: true }];
    this.todayDate = new Date()
    this.getListComponent(this.todayDate.getFullYear())
    this.checkedButton = this.todayDate.getFullYear()
  }

  refreshCalender(changeEvent: NgbNavChangeEvent) {
    this.selectedComponent = null
    this.selectedSubComponent = null
    this.selectedSubSubComponent = null
   this.getListComponent(changeEvent.nextId)
  }

  /**
   * list sub component
   */
  // open modal tree
  openModalChildComponent(data: any) {
    if (data.id != this.selectedComponent?.id) {
      data.open = true
      this.selectedComponent = data
    } else {
      this.selectedSubComponent = null
      this.selectedSubSubComponent = null
      data.open = !data.open
      this.selectedComponent = data
      this.openChildComponent = !this.openChildComponent
      if (!this.selectedComponent.open) {
        this.selectedComponent = null
      }
    }
  }
  /**
   * list child from sub component
   */
  openModalChildSubComponent(data: any) {
    if (data.id != this.selectedSubComponent?.id) {
      data.open = true
      this.selectedSubComponent = data
    } else {
      this.selectedSubSubComponent = null
      this.openChildSubComponent = !this.openChildSubComponent
      data.open = !data.open
      this.selectedSubComponent = data
      if (!this.selectedSubComponent.open) {
        this.selectedSubComponent = null
      }
    }
  }
  /**
   * list child from child sub component
   */
  openModalChildSubSubComponent(data: any) {
    if (data.id != this.selectedSubSubComponent?.id) {
      data.open = true
      this.selectedSubSubComponent = data
    } else {
      data.open = !data.open
      this.selectedDetailSubSubComponent = null
      this.selectedSubSubComponent = data
      if (!this.selectedSubSubComponent.open) {
        this.selectedSubSubComponent = null
      }
    }
  }

  openModalChildDetailSubSubComponent(data) {
    this.selectedDetailComponent = data
    this.openDetailChildComponent = true
    if (data.id != this.selectedDetailSubSubComponent?.id) {
      data.open = true
      this.selectedDetailSubSubComponent = data
    } else {
      data.open = !data.open
      this.selectedDetailSubSubComponent = data
      if (!this.selectedDetailSubSubComponent) {
        this.selectedDetailSubSubComponent = null
      }
    }
  }
  // -------


  showDetailComponent(data) {
    this.selectedDetailComponent = null
    this.openDetailChildComponent = true
    this.selectedDetailComponent = data
  }


  /**
   * get list component
   */
  getListComponent(year: any) {
    this.finishLoadData = false
    this.service.listComponentProjectComponent(year).subscribe(
        resp => {
          if (resp.success) {
            this.component = null
            this.component = resp.data
            this.finishLoadData = true
          } else {
            this.finishLoadData = true
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'Terjadi Kesalahan Pada Server',
            });
          }
        }, error => {
          this.finishLoadData = true
          this.swalAlert.showAlertSwal(error)
        }
    )
  }
}
