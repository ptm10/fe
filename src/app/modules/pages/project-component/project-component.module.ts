import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
  NgbNavModule,
  NgbDropdownModule,
  NgbPaginationModule,
  NgbModalModule,
  NgbTooltipModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {ProjectComponentRoutingModule} from './project-component-routing.module'
import {ProjectComponentComponent} from './project-component.component'
import {UIModule} from "../../../shared/ui/ui.module";
import {SwalAlert} from "../../../core/helpers/Swal";

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [ProjectComponentComponent],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        ProjectComponentRoutingModule,
        NgbTooltipModule,
        UIModule
    ],
  providers: [
      SwalAlert
  ]
})
export class ProjectComponentModule { }
