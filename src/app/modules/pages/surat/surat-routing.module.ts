import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SuratComponent } from "./surat.component";

const routes: Routes = [
  {
    path: "",
    component: SuratComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuratRoutingModule {}
