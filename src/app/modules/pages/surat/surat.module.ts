import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SuratRoutingModule } from "./surat-routing.module";
import { SuratComponent } from "./surat.component";
import { UIModule } from "src/app/shared/ui/ui.module";
import { NgbDropdownModule } from "@ng-bootstrap/ng-bootstrap";
import { SafePipe } from "./safe.pipe";

@NgModule({
  declarations: [SuratComponent, SafePipe],
  imports: [CommonModule, SuratRoutingModule, UIModule, NgbDropdownModule],
})
export class SuratModule {}
