import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from "@angular/core";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-surat",
  templateUrl: "./surat.component.html",
  styleUrls: ["./surat.component.scss"],
})
export class SuratComponent implements OnInit, AfterViewInit, AfterViewChecked {
  @ViewChild("iframePMRMSPenomoran") iframePMRMSPenomoran: ElementRef;

  prefixUrl: string = environment.serverScrumProject;
  link: string;
  elPMRMSPenomoran: HTMLIFrameElement;
  retrySendInfoSession: number;

  constructor() {}

  ngOnInit(): void {
    this.link = `${this.prefixUrl}login`;
    this.retrySendInfoSession = 1;
  }

  ngAfterViewInit() {
    this.elPMRMSPenomoran = this.iframePMRMSPenomoran.nativeElement;
  }

  ngAfterViewChecked() {
    if (this.retrySendInfoSession > 0) {
      let url = this.prefixUrl;

      if (url.substring(url.length - 1, url.length) === "/") {
        url = url.substring(0, url.length - 1);
      }

      window.addEventListener("message", (event) => {
        if (event.origin === url && event.data === "PAGE IS READY") {
          try {
            this.elPMRMSPenomoran.contentWindow.postMessage(
              JSON.stringify({
                token: localStorage.getItem("accessToken"),
                expired: localStorage.getItem("expired"),
                tokenType: localStorage.getItem("tokenType"),
                user_id: localStorage.getItem("user_id"),
                fullName: localStorage.getItem("fullName"),
                lastName: localStorage.getItem("lastName"),
                unit: localStorage.getItem("unit"),
                role_name: localStorage.getItem("role_name"),
                picture_id: localStorage.getItem("picture_id"),
                selectedYear: localStorage.getItem("selectedYear"),
                code_component: localStorage.getItem("code_component"),
                route: "surat",
              }),
              "*"
            );

            console.log("Send information session to child");
          } catch (error) {}
        }
      });

      this.retrySendInfoSession--;
    }
  }
}
