import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomValidators} from "../../../../../shared/custom-validators/custom-validators";
import {ProfileService} from "../../../../../core/services/Profile/profile.service";
import {AuthfakeauthenticationService} from "../../../../../core/services/authfake.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-change-confirm-password',
  templateUrl: './change-confirm-password.component.html',
  styleUrls: ['./change-confirm-password.component.scss']
})
export class ChangeConfirmPasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  showOldPasswordInput = false
  showPasswordInputUsers = false
  showRetypePasswordInputUsers = false
  loadingChangePassword = true
  successChangePassword = false
  validateOldPassword = false
  validateNewPassword = false
  validateRetypePassword = false
  errorRespChangePassword: string
  constructor(
      private formBuilder: FormBuilder,
      private service: ProfileService,
      private authFackservice: AuthfakeauthenticationService,
      private router: Router

  ) { }

  ngOnInit(): void {
    this.changePasswordForm = this.formBuilder.group({
          passwordLast: ['', [Validators.compose([
            Validators.required,
            CustomValidators.patternValidator(/\d/, { hasNumber: true }),
            CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
            CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
            CustomValidators.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, { hasSpecialCharacters: true }),
            Validators.minLength(8)
          ])]],
          password: ['', [Validators.compose([
            Validators.required,
            CustomValidators.patternValidator(/\d/, { hasNumber: true }),
            CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
            CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
            CustomValidators.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, { hasSpecialCharacters: true }),
            Validators.minLength(8)
          ])]],
          reTypePassword: ['', [Validators.required]],
        },
        {
          // check whether our password and confirm password match
          validator: CustomValidators.passwordMatchValidator
        });
  }
  get f() { return this.changePasswordForm.controls; }


  showOldPasswordInputUser() {
    this.showOldPasswordInput = !this.showOldPasswordInput
  }

  showPasswordInput() {
    this.showPasswordInputUsers = !this.showPasswordInputUsers
  }

  showRetypePasswordInput() {
    this.showRetypePasswordInputUsers = !this.showRetypePasswordInputUsers
  }

  saveChangePassword() {
    this.successChangePassword = false
    this.loadingChangePassword = false
    if (this.checkValidationChangePassword()) {
      const params = {
        oldPassword: this.f.passwordLast.value,
        newPassword: this.f.password.value,
        confirmPassword: this.f.reTypePassword.value
      }
      this.service.changePassword(params).subscribe(
          resp => {
            if (resp.success) {
              console.log(resp)
              this.loadingChangePassword = true
              this.successChangePassword = true
              this.authFackservice.logout();
              this.router.navigate(['/admin/login']);
            } else {
            }
          }, error =>  {
            console.log(error?.error?.data[0]?.key)
            if (error?.error?.data) {
              if (error?.error?.data[0]?.key === 'oldPassword') {
                this.validateOldPassword = true
                this.errorRespChangePassword = error?.error?.data[0].message
              } else {
                this.validateOldPassword = true
                this.errorRespChangePassword = error?.error?.data[0].message
              }
            } else {
              this.validateOldPassword = true
              this.errorRespChangePassword = error?.error?.data[0].message
            }

            this.loadingChangePassword = true
          }
      )
    } else {
      this.loadingChangePassword = true
    }
  }

  checkValidationChangePassword(): boolean {
    this.validateOldPassword = false
    this.validateNewPassword = false
    this.validateRetypePassword = false

    if (this.f.passwordLast.invalid) {
      this.validateOldPassword = true
    }

    if (this.f.password.invalid) {
      this.validateNewPassword = true
    }

    if (this.f.reTypePassword.invalid) {
      this.validateRetypePassword = true
    }

    return this.f.passwordLast.valid && this.f.password.valid && this.f.reTypePassword.valid;
  }

}
