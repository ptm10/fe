import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PasswordresetconfirmationComponent } from './passwordresetconfirmation.component';

describe('PasswordresetconfirmationComponent', () => {
  let component: PasswordresetconfirmationComponent;
  let fixture: ComponentFixture<PasswordresetconfirmationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PasswordresetconfirmationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordresetconfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
