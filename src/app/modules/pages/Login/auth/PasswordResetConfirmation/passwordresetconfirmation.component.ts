import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-passwordresetconfirmation',
  templateUrl: './passwordresetconfirmation.component.html',
  styleUrls: ['./passwordresetconfirmation.component.scss']
})
export class PasswordresetconfirmationComponent implements OnInit {

  dateExpired: string

  constructor(
    private route: Router,
    private router: ActivatedRoute
) { }

  ngOnInit(): void {
    this.dateExpired = this.router.snapshot.paramMap.get('id');
  }

  GoToPageLogin() {
    this.route.navigate(['admin/login'])
  }
}
