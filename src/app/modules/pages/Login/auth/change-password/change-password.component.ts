import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../../../../core/services/auth.service";
import {AuthfakeauthenticationService} from "../../../../../core/services/authfake.service";
import {CustomValidators} from "../../../../../shared/custom-validators/custom-validators";
import {ChangePasswordService} from "../../../../../core/services/Change-Password/change-password.service";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  error = '';
  returnUrl: string;
  successResetPassword = false
  failedResetPassword = false
  changePasswordId: string
  loadingChangePassword = false
  textResponse: string
  showPasswordInputUsers = false
  showRetypePasswordInputUsers = false
  disableButton = true
  responSuccess = false

  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authenticationService: AuthenticationService,
              private authFackservice: AuthfakeauthenticationService, private service: ChangePasswordService) { }


  ngOnInit() {
    this.changePasswordId = this.route.snapshot.paramMap.get('id');
    this.loginForm = this.formBuilder.group({
      password: ['', [Validators.compose([
        Validators.required,
        CustomValidators.patternValidator(/\d/, { hasNumber: true }),
        CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
        CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
        CustomValidators.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, { hasSpecialCharacters: true }),
        Validators.minLength(8)
      ])]],
      reTypePassword: ['', [Validators.required]],

    },
    {
      // check whether our password and confirm password match
      validator: CustomValidators.passwordMatchValidator
    });

    // reset login status
    // this.authenticationService.logout();
    // get return url from route parameters or default to '/'
    // tslint:disable-next-line: no-string-literal
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.checkValidForgetPassword()
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  /**
   * Form submit
   */
  onSubmit() {
    this.disableButton = false
    this.loadingChangePassword = true
    this.failedResetPassword = false
    this.successResetPassword = false
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
    } else {
      const body = {
        requestId: this.changePasswordId,
        password: this.f.password.value,
        confirmPassword: this.f.reTypePassword.value
      }
      this.service.changePassword(body).subscribe(
          resp => {
            if (resp.success) {
              this.loadingChangePassword = false
              this.textResponse = resp.message
              this.successResetPassword = true
              this.failedResetPassword = false
              this.responSuccess = true
            } else {
              this.loadingChangePassword = false
              this.textResponse = resp.data.message
              this.successResetPassword = false
              this.failedResetPassword = true
            }
            this.disableButton = true
          }, (error) => {
            this.failedResetPassword = true
            this.textResponse = error?.error?.data[0].message
            this.loadingChangePassword = false
            this.disableButton = true
          }
      )
    }
  }
  showPasswordInput() {
    this.showPasswordInputUsers = !this.showPasswordInputUsers
  }

  showRetypePasswordInput() {
    this.showRetypePasswordInputUsers = !this.showRetypePasswordInputUsers
  }

  checkValidForgetPassword() {
    this.service.checkForgotPassword(this.changePasswordId).subscribe(
        resp => {
          if (resp.success) {
            if (!resp.data) {
              this.router.navigate(['admin/reset-password/2'])
            }
          }
        }
    )
  }
}
