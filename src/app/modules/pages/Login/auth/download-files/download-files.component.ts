import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {EventService} from "../../../../../core/services/Event/event.service";
import {EventActivityService} from "../../../../../core/services/EventActivity/event-activity.service";

@Component({
  selector: 'app-download-files',
  templateUrl: './download-files.component.html',
  styleUrls: ['./download-files.component.scss']
})
export class DownloadFilesComponent implements OnInit {

  // loading
  loadingSaveQuestion = false
  finishLoadData = true
  finishLoadDataCount = 0
  idFiles: string
  hasAuthorizeFiles = false


  constructor(
      private router: Router,
      private activatedRoute: ActivatedRoute,
      private service: EventService,
      private serviceEvent: EventActivityService,

  ) {
  }

  ngOnInit(): void {
    this.idFiles = this.activatedRoute.snapshot.paramMap.get('id')
    this.getDetailFilesRequest()
  }

  getDetailFilesRequest() {
    this.startLoading()
    this.service.getDetailsFile(this.idFiles).subscribe(
        resp => {
          if (resp.success) {
            this.hasAuthorizeFiles = false
            this.stopLoading()
            this.downloadFiles(resp?.data?.filename, resp?.data?.fileExt)
          }
        }, error => {
          this.stopLoading()
          if (error?.status === 401) {
            this.hasAuthorizeFiles = true
          }
          console.log(error)
        }
    )
  }

  downloadFiles(nameFiles: string, extFiles: string) {
    this.serviceEvent.exportFilesFromOpenDoc(this.idFiles, nameFiles, extFiles).subscribe(
        resp => {
        }, error => {
        }
    )
  }



  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount--
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  goToHomePage() {
    this.router.navigate([''])
  }
}
