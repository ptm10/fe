import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthenticationService } from '../../../../../core/services/auth.service';
import { environment } from '../../../../../../environments/environment';
import {ResetPasswordService} from "../../../../../core/services/Forgot-Password/reset-password.service";
import {ForgotPassword} from "../../../../../core/models/auth.models";
import {CustomValidators} from "../../../../../shared/custom-validators/custom-validators";

@Component({
  selector: 'app-passwordreset',
  templateUrl: './passwordreset.component.html',
  styleUrls: ['./passwordreset.component.scss']
})

/**
 * Reset-password component
 */
export class PasswordresetComponent implements OnInit, AfterViewInit {

  resetForm: FormGroup;
  submitted = false;
  error = '';
  loading = false;
  forgetPassword: ForgotPassword
  finishForgetPassword = false;
  // set the currenr year
  year: number = new Date().getFullYear();
  passwordExpired: string
  loadingResetPassword = true

  // tslint:disable-next-line: max-line-length
  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService,
      private service: ResetPasswordService) { }

  ngOnInit() {
    this.passwordExpired = this.route.snapshot.paramMap.get('id');
    this.resetForm = this.formBuilder.group({
      email: ['', [Validators.compose([
            CustomValidators.patternValidator(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/, { hasEmailValidate: true }),
          ]
      )]],
    });
    this.checkExpired()
  }

  ngAfterViewInit() {
  }
  // convenience getter for easy access to form fields
  get f() { return this.resetForm.controls; }

  checkExpired() {
    if (this.passwordExpired === '2') {
      this.error = 'Link setel ulang password telah kedaluwarsa'
    }
  }

  goToLogin() {
    this.router.navigate(['admin/login'])
  }
  /** * On submit form */
  onSubmit() {
    this.loadingResetPassword = false
    this.error = ''
    this.submitted = true;
    this.finishForgetPassword = true

    // stop here if form is invalid
    if (this.f.email.value != null && this.f.email.value != '') {
      if (this.resetForm.valid) {
        if (this.checkValidationEmailKemenag(this.f.email.value)) {
          const params = {
            email: this.f.email.value
          }
          this.service.resetPassword(params).subscribe(
              resp => {
                if (resp.success) {
                  this.error = ''
                  this.forgetPassword = resp.data
                  this.router.navigate(['admin/reset-password-confirmation', this.forgetPassword.expired])
                  this.finishForgetPassword = false
                  this.loadingResetPassword = true
                } else {
                  this.loadingResetPassword = true
                  this.error = resp.message
                  this.finishForgetPassword = false
                }
              }, error => {
                this.loadingResetPassword = true
                this.error = error?.error ? error?.error?.message : '';
                this.finishForgetPassword = false
              }
          )
        } else {
          this.loadingResetPassword = true
          this.finishForgetPassword = false
          this.error = 'Pengguna tidak diketahui'
        }
      } else {
        this.loadingResetPassword = true
        this.finishForgetPassword = false
        this.error = 'Email tidak valid'
      }
    } else {
      this.loadingResetPassword = true
      this.finishForgetPassword = false
      this.error = 'Form tidak boleh kosong'
    }
  }

  checkValidationEmailKemenag(emailInput) {
    if (emailInput.includes("madrasah.kemenag.go.id")) {
      return true
    } else {
      return false
    }
  }
}
