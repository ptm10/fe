import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../../../../core/services/Auth/auth.service";
import Swal from "sweetalert2";
import {SwalAlert} from "../../../../../core/helpers/Swal";

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent implements OnInit {

  idEmailForgot: string
  identity:string
  emailValidatePosition: dataResponseEmail
  loadingSaveReset = false
  finishLoadData = true
  finishLoadDataCount = 0

  constructor(
      private router: ActivatedRoute,
      private service: AuthService,
      private route: Router,
      private swalAlert: SwalAlert
  ) {
    this.router.queryParams.subscribe(params => {
      this.idEmailForgot = params['id'];
      this.identity = params['identity'];
    });
  }

  ngOnInit(): void {
    this.getPositionEmail()
  }

  getPositionEmail() {
    this.startLoading()
    this.service.checkEmailValidatePosition(this.idEmailForgot ,this.identity).subscribe(
        resp => {
          this.stopLoading()
          if (resp.success) {
            this.emailValidatePosition = resp.data
          }
        }, error => {
          this.stopLoading()
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  confirmEmail(id) {
    this.loadingSaveReset = true
    let params
    if (id == 1) {
      params = {
        requestId: this.idEmailForgot,
        confirmationIdentity: this.identity,
        status: 1
      }
    } else if (id == 2) {
      params = {
        requestId: this.idEmailForgot,
        confirmationIdentity: this.identity,
        status: 2
      }
    }
    this.service.confirmUserPassword(params).subscribe(
        resp => {
          if (resp.success) {
            this.loadingSaveReset = false
            this.getPositionEmail()
            if (id == 2 || (this.emailValidatePosition.identity == 2 && !this.emailValidatePosition.confirmFromNewEmail)) {
              this.route.navigate(['/admin/login']);
            }
          }
        }, error => {
          this.loadingSaveReset = false
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /** Function Start Loading */
  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  /** Function Stop Loading */
  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true

    }
  }

  goToLogin() {
    this.route.navigate(['admin/login'])
  }
}

export class dataResponseEmail {
  constructor(
      public identity: number,
      public oldEmail: string,
      public newEmail: string,
      public confirmFromOldEmail: boolean,
      public confirmFromNewEmail: boolean
  ) {
  }
}
