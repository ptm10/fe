import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {QuestionareService} from "../../../../../core/services/Questionnare/questionare.service";
import {SwalAlert} from "../../../../../core/helpers/Swal";
import {Questionnare} from "../../../../../core/models/Questionnare";
import {QuestionEvent, QuestionEventAnswer} from "../../../../../core/models/event.model";
import {NgbRatingConfig} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-questionner',
  templateUrl: './questionner.component.html',
  styleUrls: ['./questionner.component.scss']
})
export class QuestionnerComponent implements OnInit {

    detailIdMonev: string
    createdFor: string
    listQuestionnare: Questionnare
    existingQuestionCompliance: QuestionEventAnswer[]  = []
    existingQuestionEconomy: QuestionEventAnswer[]  = []
    existingQuestionEfective: QuestionEventAnswer[]  = []
    existingQuestionEfisiens: QuestionEventAnswer[]  = []
    existingQuestionParticipantCompliance: QuestionEventAnswer[]  = []
    existingQuestionParticipantEconomy: QuestionEventAnswer[]  = []
    existingQuestionParticipantEffective: QuestionEventAnswer[]  = []
    existingQuestionParticipantEffisiens: QuestionEventAnswer[]  = []
    existingQuestionParticipantOtherQuestion: QuestionEventAnswer[]  = []
    valueEvaluationRating: number = 0
    successPageSave: boolean = false

    participantName: string
    institutionFrom: string
    email: string
    gender: string = null
    phoneNumber: string
    validationEmail: boolean
    timeout: any = null;


    // loading
    loadingSaveQuestion = false
    finishLoadData = true
    finishLoadDataCount = 0

    constructor(
      private activatedRoute: ActivatedRoute,
      private router: Router,
      private service: QuestionareService,
      public swalAlert: SwalAlert,
      private config: NgbRatingConfig,
    ) { }

    ngOnInit(): void {
      this.detailIdMonev = this.activatedRoute.snapshot.paramMap.get('eventId')
      this.createdFor = this.activatedRoute.snapshot.paramMap.get('createdFor')
      this.getListQuestionnare()
        this.config.max = 5;
    }

    validateEmail(email) {
        var that = this;
        clearTimeout(this.timeout);
        this.timeout = setTimeout(function () {
            const regularExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return that.validationEmail = regularExpression.test(String(email).toLowerCase());
        }, 500);
    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        return !(charCode > 31 && (charCode < 48 || charCode > 57));
    }

    /** Function Start Loading */
    startLoading() {
        if (this.finishLoadDataCount == 0) {
            this.finishLoadDataCount ++
            this.finishLoadData = false
        } else {
            this.finishLoadDataCount ++
        }
    }

    /** Function Stop Loading */
    stopLoading() {
        this.finishLoadDataCount --
        if (this.finishLoadDataCount == 0) {
            this.finishLoadData = true

        }
    }


    getListQuestionnare() {
      this.startLoading()
      this.service.listQuestionnare(this.detailIdMonev, this.createdFor).subscribe(
        resp => {
          if (resp.success) {
              this.stopLoading()
              this.listQuestionnare = resp.data
              this.listQuestionnare.question.forEach(x => {
                  x.ratingAnswer = 0
                  x.explanationAnswer = null
                  x.yesOrNoAnswer = null
                  if (x.questionCategory === 1) {
                      this.existingQuestionParticipantCompliance.push(x)
                  } else if (x.questionCategory === 2) {
                      this.existingQuestionParticipantEconomy.push(x)
                  } else if (x.questionCategory === 3) {
                      this.existingQuestionParticipantEffective.push(x)
                  } else if (x.questionCategory === 4) {
                      this.existingQuestionParticipantEffisiens.push(x)
                  } else if (x.questionCategory === 5) {
                      this.existingQuestionParticipantOtherQuestion.push(x)
                  }
              })
          }
        }, error => {
              this.stopLoading()
              this.swalAlert.showAlertSwal(error)
        })
    }

    goToHomePage() {
        this.router.navigate([''])
    }

    disableButtonSave() {
      let checkResultParticipantYesOrNo = false
      let checkResultParticipantExplanation = false
      let checkResultParticipantRatingAnswer = false

      let checkResultParticipantYesOrNoEconomy = false
      let checkResultParticipantExplanationEconomy = false
      let checkResultParticipantRatingAnswerEconomy = false

      let checkResultParticipantYesOrNoEffective = false
      let checkResultParticipantExplanationEffective = false
      let checkResultParticipantRatingAnswerEffective = false

      let checkResultParticipantYesOrNoEffisiens = false
      let checkResultParticipantExplanationEffisiens = false
      let checkResultParticipantRatingAnswerEffisiens = false

      let checkResultParticipantYesOrNoOtherQuestion = false
      let checkResultParticipantExplanationOtherQuestion = false
      let checkResultParticipantRatingAnswerOtherQuestion = false

      if (this.existingQuestionParticipantCompliance.length > 0) {
          let filterQuestionYesOrNo = this.existingQuestionParticipantCompliance.filter(x => x.questionType === 1 && x.yesOrNoAnswer === null)
          checkResultParticipantYesOrNo = filterQuestionYesOrNo.length > 0;
          let filterQuestionExplanation = this.existingQuestionParticipantCompliance.filter(x => x.questionType === 3 && !x.explanationAnswer)
          checkResultParticipantExplanation = filterQuestionExplanation.length > 0;
          let filterQuestionScore = this.existingQuestionParticipantCompliance.filter(x => x.questionType === 2 && x.ratingAnswer === 0)
          checkResultParticipantRatingAnswer = filterQuestionScore.length > 0;
      }

      if (this.existingQuestionParticipantEconomy.length > 0) {
          let filterQuestionYesOrNo = this.existingQuestionParticipantEconomy.filter(x => x.questionType === 1 && x.yesOrNoAnswer === null)
          checkResultParticipantYesOrNoEconomy = filterQuestionYesOrNo.length > 0;
          let filterQuestionExplanation = this.existingQuestionParticipantEconomy.filter(x => x.questionType === 3 && !x.explanationAnswer)
          checkResultParticipantExplanationEconomy = filterQuestionExplanation.length > 0;
          let filterQuestionScore = this.existingQuestionParticipantEconomy.filter(x => x.questionType === 2 && x.ratingAnswer === 0)
          checkResultParticipantRatingAnswerEconomy = filterQuestionScore.length > 0;
      }

      if (this.existingQuestionParticipantEffective.length > 0) {
          let filterQuestionYesOrNo = this.existingQuestionParticipantEffective.filter(x => x.questionType === 1 && x.yesOrNoAnswer === null)
          checkResultParticipantYesOrNoEffective = filterQuestionYesOrNo.length > 0;
          let filterQuestionExplanation = this.existingQuestionParticipantEffective.filter(x => x.questionType === 3 && !x.explanationAnswer)
          checkResultParticipantExplanationEffective = filterQuestionExplanation.length > 0;
          let filterQuestionScore = this.existingQuestionParticipantEffective.filter(x => x.questionType === 2 && x.ratingAnswer === 0)
          checkResultParticipantRatingAnswerEffective = filterQuestionScore.length > 0;
      }

      if (this.existingQuestionParticipantEffisiens.length > 0) {
          let filterQuestionYesOrNo = this.existingQuestionParticipantEffisiens.filter(x => x.questionType === 1 && x.yesOrNoAnswer === null)
          checkResultParticipantYesOrNoEffisiens = filterQuestionYesOrNo.length > 0;
          let filterQuestionExplanation = this.existingQuestionParticipantEffisiens.filter(x => x.questionType === 3 && !x.explanationAnswer)
          checkResultParticipantExplanationEffisiens = filterQuestionExplanation.length > 0;
          let filterQuestionScore = this.existingQuestionParticipantEffisiens.filter(x => x.questionType === 2 && x.ratingAnswer === 0)
          checkResultParticipantRatingAnswerEffisiens = filterQuestionScore.length > 0;
      }

      if (this.existingQuestionParticipantOtherQuestion.length > 0) {
          let filterQuestionYesOrNo = this.existingQuestionParticipantOtherQuestion.filter(x => x.questionType === 1 && x.yesOrNoAnswer === null)
          checkResultParticipantYesOrNoOtherQuestion = filterQuestionYesOrNo.length > 0;
          let filterQuestionExplanation = this.existingQuestionParticipantOtherQuestion.filter(x => x.questionType === 3 && !x.explanationAnswer)
          checkResultParticipantExplanationOtherQuestion = filterQuestionExplanation.length > 0;
          let filterQuestionScore = this.existingQuestionParticipantOtherQuestion.filter(x => x.questionType === 2 && x.ratingAnswer === 0)
          checkResultParticipantRatingAnswerOtherQuestion = filterQuestionScore.length > 0;
      }

      return checkResultParticipantYesOrNo || checkResultParticipantExplanation || checkResultParticipantRatingAnswer ||
          checkResultParticipantYesOrNoEconomy || checkResultParticipantExplanationEconomy || checkResultParticipantRatingAnswerEconomy ||
          checkResultParticipantYesOrNoEffective || checkResultParticipantExplanationEffective || checkResultParticipantRatingAnswerEffective ||
          checkResultParticipantYesOrNoEffisiens || checkResultParticipantExplanationEffisiens || checkResultParticipantRatingAnswerEffisiens ||
          checkResultParticipantYesOrNoOtherQuestion || checkResultParticipantExplanationOtherQuestion || checkResultParticipantRatingAnswerOtherQuestion
  }

    saveFormQuestion() {
    this.loadingSaveQuestion = true
    let answersAllQuestions = []
    if (this.existingQuestionParticipantCompliance.length > 0) {
        this.existingQuestionParticipantCompliance.forEach(x => {
            if (x.questionType === 1) {
                answersAllQuestions.push({
                    questionId: x.id,
                    answer: x.yesOrNoAnswer
                })
            } else if (x.questionType === 2) {
                answersAllQuestions.push({
                    questionId: x.id,
                    answer: x.ratingAnswer
                })
            } else if (x.questionType === 3) {
                answersAllQuestions.push({
                    questionId: x.id,
                    answer: x.explanationAnswer
                })
            }
        })
    }

      if (this.existingQuestionParticipantEconomy.length > 0) {
          this.existingQuestionParticipantEconomy.forEach(x => {
              if (x.questionType === 1) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.yesOrNoAnswer
                  })
              } else if (x.questionType === 2) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.ratingAnswer
                  })
              } else if (x.questionType === 3) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.explanationAnswer
                  })
              }
          })
      }

      if (this.existingQuestionParticipantEffective.length > 0) {
          this.existingQuestionParticipantEffective.forEach(x => {
              if (x.questionType === 1) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.yesOrNoAnswer
                  })
              } else if (x.questionType === 2) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.ratingAnswer
                  })
              } else if (x.questionType === 3) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.explanationAnswer
                  })
              }
          })
      }

      if (this.existingQuestionParticipantEffisiens.length > 0) {
          this.existingQuestionParticipantEffisiens.forEach(x => {
              if (x.questionType === 1) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.yesOrNoAnswer
                  })
              } else if (x.questionType === 2) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.ratingAnswer
                  })
              } else if (x.questionType === 3) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.explanationAnswer
                  })
              }
          })
      }

      if (this.existingQuestionParticipantOtherQuestion.length > 0) {
          this.existingQuestionParticipantOtherQuestion.forEach(x => {
              if (x.questionType === 1) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.yesOrNoAnswer
                  })
              } else if (x.questionType === 2) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.ratingAnswer
                  })
              } else if (x.questionType === 3) {
                  answersAllQuestions.push({
                      questionId: x.id,
                      answer: x.explanationAnswer
                  })
              }
          })
      }

    let params = {
        id: this.detailIdMonev,
        username: this.participantName,
        identityId: this.createdFor,
        gender: this.gender,
        institution: this.institutionFrom,
        email: this.email,
        phoneNumber: this.phoneNumber,
        answers: answersAllQuestions
    }

    this.service.saveQuestionnare(params).subscribe(
        resp => {
            if (resp.success) {
                this.loadingSaveQuestion = false
                this.successPageSave = true
                // this.goToHomePage()
            }
        }, error => {
            this.loadingSaveQuestion = false
            this.swalAlert.showAlertSwal(error)
        }
    )
    console.log(params)
  }
}
