import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnerComponent } from './questionner.component';

describe('QuestionnerComponent', () => {
  let component: QuestionnerComponent;
  let fixture: ComponentFixture<QuestionnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
