import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import {NgbAlertModule, NgbRatingModule} from '@ng-bootstrap/ng-bootstrap';
import { CarouselModule } from 'ngx-owl-carousel-o';

import { UIModule } from '../../../../shared/ui/ui.module';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';

import { AuthRoutingModule } from './auth-routing';
import { PasswordresetComponent } from './PasswordReset/passwordreset.component';
import {LayoutsModule} from "../../../../layouts/layouts.module";
import {SharedModule} from "../../../../shared/shared.module";
import { PasswordresetconfirmationComponent } from './PasswordResetConfirmation/passwordresetconfirmation.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ChangeConfirmPasswordComponent } from './change-confirm-password/change-confirm-password.component';
import { ChangeEmailComponent } from './change-email/change-email.component';
import { QuestionnerComponent } from './questionner/questionner.component';
import { DownloadFilesComponent } from './download-files/download-files.component';
@NgModule({
  declarations: [LoginComponent, SignupComponent, PasswordresetComponent, PasswordresetconfirmationComponent, ChangePasswordComponent, ChangeConfirmPasswordComponent, ChangeEmailComponent, QuestionnerComponent, DownloadFilesComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        NgbAlertModule,
        UIModule,
        AuthRoutingModule,
        CarouselModule,
        LayoutsModule,
        SharedModule,
        NgbRatingModule,
    ]
})
export class AuthModule { }
