import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from '../../../../../core/services/auth.service';
import { AuthfakeauthenticationService } from '../../../../../core/services/authfake.service';

import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { environment } from '../../../../../../environments/environment';
import {AuthService} from "../../../../../core/services/Auth/auth.service";
import {RequestLogin} from "../../../../../core/models/login.models";
import {CustomValidators} from "../../../../../shared/custom-validators/custom-validators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

/**
 * Login component
 */
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submittedEmail = false;
  submittedPassword = false;
  succesValidateEmail = false
  succesValidatePassword = false
  error = '';
  returnUrl: string;
  testing = true
  finishLogin = false
  showPasswordInputUsers = false
  checkEmailInput = false
  checkPasswordInput = false

  RequestLoginModels = new RequestLogin (null, null)

  // tslint:disable-next-line: max-line-length
  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService,
      private service: AuthService,
      private authFackservice: AuthfakeauthenticationService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.compose([
            CustomValidators.patternValidator(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/, { hasEmailValidate: true }),
          ]
      )]],
      password: ['', [Validators.compose([
        Validators.required,
        CustomValidators.patternValidator(/\d/, { hasNumber: true }),
        CustomValidators.patternValidator(/[A-Z]/, { hasCapitalCase: true }),
        CustomValidators.patternValidator(/[a-z]/, { hasSmallCase: true }),
        CustomValidators.patternValidator(/[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/, { hasSpecialCharacters: true }),
        Validators.minLength(6)
      ])]],
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  /** * Form submit **/
  onSubmit() {
    this.error = ''
    this.finishLogin = true
    this.submittedEmail = false
    this.submittedPassword = false
    this.succesValidateEmail = false
    this.succesValidatePassword = false





    // stop here if form is invalid

    if (this.f.email.value && this.f.password.value) {
      if (this.f.email.valid) {
        if (this.checkValidationEmailKemenag(this.f.email.value)) {
          if (this.f.password.valid ) {
            this.authFackservice.login(this.RequestLoginModels)
                .pipe(first())
                .subscribe(
                    data => {
                      this.finishLogin = false
                      this.succesValidateEmail = true
                      this.succesValidatePassword = true
                      this.submittedEmail = false
                      if (data.data.requiredChagePassword) {
                        this.router.navigate(['admin/reset-password-login'])
                      } else {
                        if (this.authFackservice.hasAuthority("LSP")) {
                          this.router.navigate(['/implement/kegiatan']);
                        } else {
                          this.router.navigate(['/dashboards/dashboard-component']);
                        }
                      }
                    },
                    error => {
                      this.finishLogin = false
                      this.submittedEmail = true;
                      this.submittedPassword = true;
                      this.error = error?.error ? error?.error?.message : '';
                    });
          } else {
            this.finishLogin = false
            this.submittedPassword = true
            this.submittedEmail = true;
            this.succesValidateEmail = true
            this.error = 'Pengguna tidak diketahui'
          }
        } else {
          this.finishLogin = false
          this.submittedEmail = true;
          this.submittedPassword = true
          this.error = 'Pengguna tidak diketahui'
        }
      } else {
        this.finishLogin = false
        this.submittedEmail = true;
        this.submittedPassword = true
        this.error = 'Email tidak valid'
      }
    } else if (!this.f.password.value && this.f.email.value) {
      this.finishLogin = false
      this.submittedEmail = false;
      this.submittedPassword = true
      this.error = 'Password tidak boleh kosong'
    } else if (!this.f.email.value && this.f.password.value) {
      this.finishLogin = false
      this.submittedEmail = true;
      this.submittedPassword = false
      this.error = 'Email Tidak Boleh Kosong'
    } else {
      this.finishLogin = false
      this.submittedEmail = true;
      this.submittedPassword = true
      this.error = 'Form tidak boleh kosong'
    }
  }

  checkValidationEmailKemenag(emailInput) {
    if (emailInput.includes("madrasah.kemenag.go.id")) {
      return true
    } else {
      return false
    }
  }

  showPasswordInput() {
    this.showPasswordInputUsers = !this.showPasswordInputUsers
  }
}
