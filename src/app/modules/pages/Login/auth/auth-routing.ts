import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { PasswordresetComponent } from './PasswordReset/passwordreset.component';
import {PasswordresetconfirmationComponent} from './PasswordResetConfirmation/passwordresetconfirmation.component'
import {ChangePasswordComponent} from './change-password/change-password.component'
import {ChangeConfirmPasswordComponent} from "./change-confirm-password/change-confirm-password.component";
import {ChangeEmailComponent} from "./change-email/change-email.component";
import {QuestionnerComponent} from "./questionner/questionner.component";
import {DownloadFilesComponent} from "./download-files/download-files.component";
const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'signup',
        component: SignupComponent
    },
    {
        path: 'reset-password/:id',
        component: PasswordresetComponent
    },
    {
        path: 'reset-password-confirmation/:id',
        component: PasswordresetconfirmationComponent
    },
    {
        path: 'change-password/:id',
        component: ChangePasswordComponent
    },
    {
        path: 'reset-password-login',
        component: ChangeConfirmPasswordComponent
    },
    {
        path: 'change-email',
        component: ChangeEmailComponent
    },
    {
        path: 'questionnaire/:eventId/:createdFor',
        component: QuestionnerComponent
    },
    {
        path: 'files/:id',
        component: DownloadFilesComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule { }
