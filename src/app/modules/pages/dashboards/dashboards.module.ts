import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DashboardsRoutingModule } from './dashboards-routing.module';
import { UIModule } from '../../../shared/ui/ui.module';
import { WidgetModule } from '../../../shared/widget/widget.module';

import { NgApexchartsModule } from 'ng-apexcharts';
import { NgbDropdownModule, NgbTooltipModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap'
import { SimplebarAngularModule } from 'simplebar-angular';

import { DefaultComponent } from './default/default.component';
import { DashboardComponentComponent } from './dashboard-component/dashboard-component.component';
import { TableauModule } from 'ngx-tableau';
import {FullCalendarModule} from "@fullcalendar/angular";
import {DateHelper} from "../../../core/helpers/date-helper";
import {DataTablesModule} from "angular-datatables";
import { EventTableComponent } from './event-table/event-table.component';
import { TableauAwpComponent } from './tableau-awp/tableau-awp.component';

@NgModule({
  declarations: [DefaultComponent, DashboardComponentComponent, EventTableComponent, TableauAwpComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DashboardsRoutingModule,
        UIModule,
        NgbDropdownModule,
        NgbTooltipModule,
        NgbNavModule,
        WidgetModule,
        NgApexchartsModule,
        SimplebarAngularModule,
        TableauModule,
        FullCalendarModule,
        DataTablesModule
    ],
    providers: [
        DateHelper,
    ]
})
export class DashboardsModule { }
