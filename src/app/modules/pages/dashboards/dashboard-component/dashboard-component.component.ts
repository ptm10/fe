import {Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {TableauComponent, VizCreateOptions} from "ngx-tableau";
import {DashboardService} from "../../../../core/services/Dashboard/dashboard.service";
import {AuthfakeauthenticationService} from "../../../../core/services/authfake.service";
import {HorizontaltopbarComponent} from "../../../../layouts/horizontaltopbar/horizontaltopbar.component";
import {ProfileService} from "../../../../core/services/Profile/profile.service";
import {CalendarOptions, EventClickArg, EventInput} from "@fullcalendar/core";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {FormBuilder, FormGroup} from "@angular/forms";
import Swal from "sweetalert2";
import {NgbModal, NgbNavChangeEvent} from "@ng-bootstrap/ng-bootstrap";
import {FullCalendarComponent} from "@fullcalendar/angular";
import {DateHelper} from "../../../../core/helpers/date-helper";
import {leangueIndoDT} from "../../../../shared/pagination-custom";
import {DataResponse} from "../../../../core/models/dataresponse.models";
import {environment} from "../../../../../environments/environment";
import {CommonCons} from "../../../../shared/CommonCons";
import {parse} from "date-fns";
import {DataTableDirective} from "angular-datatables";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {TotalEventByStatus} from "../../../../core/models/awp.model";
import {Subject} from "rxjs";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-dashboard-component',
  templateUrl: './dashboard-component.component.html',
  styleUrls: ['./dashboard-component.component.scss']
})
export class DashboardComponentComponent implements OnInit, OnDestroy {

  @ViewChild('editmodalShow') editmodalShow: TemplateRef<any>;
  @ViewChild('calendar') calendarComponent: FullCalendarComponent;
  @ViewChild(TableauComponent)
  tableauComponent: TableauComponent


  // Splitted Tableau Server URL and Report
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  data: any
  serverUrl = 'https://public.tableau.com';
  report = 'SuperSampleSuperstore/SuperDescriptive';
  testing = false
  loadingTableau = false
  selectedButtonComponent: number
  selectedButtonData: string
  componentUser: string
  breadCrumbItems: Array<{}>;
  checkedButton: number
  totalEventByStatus: TotalEventByStatus
  eventType: number
  // calender
  finishLoadDataCalender = true
  dataCalender: EventInput[]
  calenderModelEvent: EventActivityModel[]
  dataCalenderInput: EventInput[] = []
  tempDataCalender: EventInput[] = []
  startDate: Date
  endDate: Date
  id: string
  tittle: string
  backgroundColorEventCalender: string
  formData: FormGroup;
  formEditData: FormGroup;
  titleDate: Date
  date: Date
  existingDate: Date
  nextDate: Date
  previousDate: Date
  tittleEventSelected: string
  descriptionEventSelected: string
  staffEvent = []
  taskTypeEventSelected: number
  listDescriptionEventSelected: any
  listStaffEvent = []
  otherStaffEvent = []
  province: any
  regencies: any
  startDateEvent: any
  endDateEvent: any
  editEvent: any;
  selectedTabs: number

  // url tableau
  urlTableu: string
  urlTableuAwp: string
  prefixTableauProduction: string
  baseUrlTableauPmrms = environment.serverTableuDashboardPmrms

  // model calender
  calendarOptions: CalendarOptions = {
    headerToolbar: {
      left: '',
      right: ''
    },
    initialView: "dayGridMonth",
    themeSystem: "bootstrap",
    initialEvents: [],
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    // dateClick: this.openModalCalender.bind(this),
    eventClick: this.handleEventClick.bind(this),
    displayEventTime: false,
    customButtons: {
      myCustomButton: {
        text: 'custom!',
        click: function() {
          alert('clicked the custom button!');
        }
      }
    },
  };


  constructor(
      private service: DashboardService,
      public roles: AuthfakeauthenticationService,
      private formBuilder: FormBuilder,
      private modalService: NgbModal,
      private dateHelper: DateHelper,
      private http: HttpClient,
      private router: Router,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    if (environment.urlTableauProduction) {
      this.prefixTableauProduction = 'prod/'
    } else {
      this.prefixTableauProduction = 'dev/'
    }
    this.getListValueEvent()
    this.componentUser = localStorage.getItem('code_component')
    this.breadCrumbItems = [{ label: 'Dashboard', active: true }];
    this.checkedButton = 1
    this.selectedButtonData = 'awp'
    this.selectedTabs = 1
    if (this.roles.hasAuthority('Administrator')) {
      this.loginTableu(1, 1)
    } else if (this.componentUser === '4.4') {
      this.loginTableu(1, 1)
    } else {
      if (this.roles.hasAuthorityComponent('1')) {
        this.loginTableu(1, 1)
      } else if (this.roles.hasAuthorityComponent('2')) {
        this.loginTableu(2, 1)
      } else if (this.roles.hasAuthorityComponent('3')) {
        this.loginTableu(3, 1)
      } else if (this.roles.hasAuthorityComponent('4')) {
        this.loginTableu(4,1)
      }
    }
    this.date = new Date()
    this.existingDate = new Date()
    this.nextDate = new Date(this.existingDate.setMonth(this.existingDate.getMonth() + 1))
    this.previousDate = this.dateHelper.minusDate(new Date(this.date), 1)
    this.titleDate = new Date()
  }

  ngOnDestroy() {
    if (this.tableauComponent) {
      this.tableauComponent?.tableauViz?.dispose()
    }
  }

  // Options
  options: VizCreateOptions = {
    hideTabs: false,
    hideToolbar: false,
    width: '100%',
    height: '2500px',

    toolbarPosition: (event) => {
      console.log(event);
    },
    onFirstInteractive: (event) => {
      console.log('I was called', event);
    },
  };

  loginTableu(id, from) {
    let selectedButtonTableau
    if (from === 1) {
      this.selectedButtonComponent = id
    } else {
      this.selectedButtonData = id
    }
    this.loadingTableau = true
    this.service.loginTableu().subscribe(
        resp => {
          this.data = resp
          if (from == 1) {
            this.urlTableu = 'https://dashboardemis.kemenag.go.id/trusted/' + this.data + '/views/MEQR-'+ id+'/MEQR-Komponen' +id+ '?:embed=yes'
          } else if (from == 2) {
            switch (id) {
              case 'awp':
                selectedButtonTableau = 'AWP'
                break;
              case 'kegiatan':
                selectedButtonTableau = 'Kegiatan'
                break;
              case 'event':
                selectedButtonTableau = 'Event'
                break;
              case 'monev' :
                selectedButtonTableau = 'Monev'
                break;
              case 'risiko' :
                selectedButtonTableau = 'Risiko'
                break;
            }

            //URL Tableau Siap belajar
            this.urlTableuAwp = 'https://dashboardemis.kemenag.go.id/trusted/' + this.data + '/views/PMRMS_'+ selectedButtonTableau + '_' + this.prefixTableauProduction + id + '?:embed=yes'

            // URL Tableau with Ip
            // this.urlTableuAwp = this.baseUrlTableauPmrms + this.data + '/views/PMRMS_' + selectedButtonTableau +'_' + this.prefixTableauProduction + id

            // URL Tableau Public
            // if (id === 'awp') {
            //   this.urlTableuAwp = 'https://public.tableau.com/views/AWP2022-temp/Dashboard1?:language=en-US&:display_count=n&:origin=viz_share_link'
            // } else if (id === 'risiko') {
            //   this.urlTableuAwp = 'https://public.tableau.com/views/Risiko-temp/Dashboard1?:language=en-US&publish=yes&:display_count=n&:origin=viz_share_link'
            // } else {
            //   this.urlTableuAwp = 'https://dashboardemis.kemenag.go.id/trusted/' + this.data + '/views/pmrms/'+id+'?:embed=yes'
            // }
          }
          this.loadingTableau = false
        }, error => {
          this.loadingTableau = false
        }
    )
  }

  refreshCalender(changeEvent: NgbNavChangeEvent) {
    this.selectedTabs = changeEvent.nextId
    if (changeEvent.nextId == 1) {
      if (this.roles.hasAuthority('Administrator')) {
        this.loginTableu(1, 1)
      } else if (this.componentUser === '4.4') {
        this.loginTableu(1, 1)
      } else {
        if (this.roles.hasAuthorityComponent('1')) {
          this.loginTableu(1, 1)
        } else if (this.roles.hasAuthorityComponent('2')) {
          this.loginTableu(2, 1)
        } else if (this.roles.hasAuthorityComponent('3')) {
          this.loginTableu(3, 1)
        } else if (this.roles.hasAuthorityComponent('4')) {
          this.loginTableu(4, 1)
        }
      }
    } else if (changeEvent.nextId == 3) {
      this.urlTableu = null
      this.loginTableu(this.selectedButtonData, 2)
    } else if (changeEvent.nextId == 2) {
      this.finishLoadDataCalender = true
      this.listCalenderEvent()
    }

  }

  /** Change Data Calender */

  listCalenderEvent() {
    let params
    params = {
      self: true,
      staff: false,
      componentIds: []
    }
    this.service.calenderEvent(params).subscribe(
        resp => {
          if (resp.success) {
            this.tempDataCalender = []
            this.calenderModelEvent = resp.data
            this.calenderModelEvent.forEach(data => {
              data.backgroundColorCalender = ''
              this.startDate = new Date(data.startDate)
              this.endDate = new Date(data.endDate)
              this.id = data.id
              this.tittle = data.name
              this.backgroundColorEventCalender = data.backgroundColorCalender
              this.tempDataCalender.push({
                id: this.id,
                title: this.tittle,
                start: this.startDate,
                end: this.endDate,
                className: 'bg-komponenself text-white',
                extendedProps : {
                  description: data.description,
                  staffEvent: data.staff,
                  otherStaffEvent: data.otherStaff,
                  province: data.province,
                  regencies: data.regency,
                  startDate: new Date(data.startDate),
                  endDate: new Date(data.endDate)
                }
              })
            })
            // this.finishLoadData = true
            setTimeout(() => {
              this.dataCalenderInput = this.tempDataCalender
              this.calendarOptions.events = this.dataCalenderInput
              this.finishLoadDataCalender = false
            }, 1000 )
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /*** Event click modal show */
  handleEventClick(clickInfo: EventClickArg) {
    this.tittleEventSelected = ''
    this.descriptionEventSelected = ''
    this.tittleEventSelected = clickInfo.event.title
    this.descriptionEventSelected = clickInfo.event.extendedProps.description
    this.taskTypeEventSelected = clickInfo.event.extendedProps.taskType
    this.listStaffEvent = clickInfo.event.extendedProps.staffEvent
    this.otherStaffEvent = clickInfo.event.extendedProps.otherStaffEvent
    this.province = clickInfo.event.extendedProps.province
    this.regencies = clickInfo.event.extendedProps.regencies
    this.startDateEvent = clickInfo.event.extendedProps.startDate
    this.endDateEvent = clickInfo.event.extendedProps.endDate
    this.editEvent = clickInfo.event;
    this.formEditData = this.formBuilder.group({
      editTitle: clickInfo.event.title,
      dataDescription: clickInfo.event.extendedProps.description,
      start: clickInfo.event.start
    });
    this.modalService.open(this.editmodalShow, { centered: true });
  }

  moveDateCalender(from: number) {
    if (from === 1) {
      this.date.setMonth(this.date.getMonth() + 1)
    } else {
      this.date = this.dateHelper.minusDate(new Date(this.date), 1)
    }
    this.existingDate = new Date(this.date)
    this.nextDate = new Date(this.existingDate.setMonth(this.existingDate.getMonth() + 1))
    this.previousDate = this.dateHelper.minusDate(new Date(this.date), 1)
    this.titleDate = new Date(this.date)
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.gotoDate(this.date);
  }

  /**
   * Open extra large modal
   * @param selectedMenu extra large modal data
   * @param eventType
   */
  openModal(selectedMenu: any, eventType: any) {
    this.eventType = eventType;
    this.modalService.open(selectedMenu, { size: 'xl', centered: true })
  }

  /** * close modal */
  closeModal(){
    this.modalService.dismissAll()
  }

  getListValueEvent() {
    this.service.getTotalEvent().subscribe(
        resp => {
          if (resp.success) {
            this.totalEventByStatus = resp.data
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  goToDetailKegiatan(id) {
    this.closeModal()
    this.router.navigate(['activity/detail-event/' + id])
  }

}
