import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DefaultComponent } from './default/default.component';
import {DashboardComponentComponent} from "./dashboard-component/dashboard-component.component";
import {AuthGuard} from "../../../core/guards/auth.guard";

const routes: Routes = [
    {
        path: 'default',
        component: DefaultComponent, canActivate: [AuthGuard]
    },
    {
        path: 'dashboard-component',
        component: DashboardComponentComponent, canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardsRoutingModule {}
