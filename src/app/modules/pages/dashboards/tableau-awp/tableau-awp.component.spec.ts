import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauAwpComponent } from './tableau-awp.component';

describe('TableauAwpComponent', () => {
  let component: TableauAwpComponent;
  let fixture: ComponentFixture<TableauAwpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableauAwpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableauAwpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
