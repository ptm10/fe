import { Component, OnInit } from '@angular/core';
import {VizCreateOptions} from "ngx-tableau";

@Component({
  selector: 'app-tableau-awp',
  templateUrl: './tableau-awp.component.html',
  styleUrls: ['./tableau-awp.component.scss']
})
export class TableauAwpComponent implements OnInit {
  urlTableu: string

  constructor() { }

  ngOnInit(): void {
    this.urlTableu = 'https://public.tableau.com/views/AWP2022-temp/Dashboard1?:language=en-US&:display_count=n&:origin=viz_share_link'
  }

  options: VizCreateOptions = {
    hideTabs: false,
    hideToolbar: false,
    width: '100%',
    height: '2500px',

    toolbarPosition: (event) => {
      console.log(event);
    },
    onFirstInteractive: (event) => {
      console.log('I was called', event);
    },
  };

}
