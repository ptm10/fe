import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {DataTableDirective} from "angular-datatables";
import {HttpClient} from "@angular/common/http";
import {leangueIndoDT} from "../../../../shared/pagination-custom";
import {DataResponse} from "../../../../core/models/dataresponse.models";
import {environment} from "../../../../../environments/environment";
import {CommonCons} from "../../../../shared/CommonCons";
import {parse} from "date-fns";
import {EventActivityModel} from "../../../../core/models/event-activity.model";
import {Router} from "@angular/router";
import {SwalAlert} from "../../../../core/helpers/Swal";

@Component({
  selector: 'app-event-table',
  templateUrl: './event-table.component.html',
  styleUrls: ['./event-table.component.scss']
})
export class EventTableComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};

  // table event
  paramsLike = []
  filterData: any
  paramsIs = []
  start = 1
  modelEvent: EventActivityModel[]
  finishLoadDataCount = 0
  finishLoadData = true
  @Input() eventType: number
  @Output() rowClicked = new EventEmitter <string> ()

  constructor(
      private http: HttpClient,
      private router: Router,
      private swalAlert: SwalAlert
  ) { }

  ngOnInit(): void {
    this.getDataTable()
  }

  getDataTable() {
    this.startLoading()
    this.paramsIs = []
    let params = {
      field: "eventSelfStatus",
      dataType: "int",
      value: this.eventType
    }
    this.paramsIs.push(params)
    this.dtOptions = {
      pagingType: 'simple_numbers',
      pageLength: 10,
      serverSide: true,
      processing: false,
      searching: false,
      lengthChange: false,
      paging: true,
      info: false,
      language: leangueIndoDT,
      ajax: (dataTablesParameters: any, callback) => {
        const selectedColumn = dataTablesParameters.order[0].column;
        const params = {
          enablePage: true,
          page: (dataTablesParameters.start / dataTablesParameters.length),
          size: dataTablesParameters.size,
          paramLike: this.paramsLike,
          paramIs: this.paramsIs,
          paramIn: [],
          paramNotIn: [],
          sort: [
            {
              field: dataTablesParameters.columns[selectedColumn].data,
              direction: dataTablesParameters.order[0].dir
            }
          ]
        }
        this.http.post<DataResponse>(environment.serverAwp + 'event/self-event-by-status-datatable', params, CommonCons.getHttpOptions()).subscribe(resp => {
          this.stopLoading()
          if (resp.success) {
            // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            //   this.start = dtInstance.page.info().page * dtInstance.page.info().length;
            // });
            this.modelEvent = resp.data.content;
            this.modelEvent.forEach(data => {
              data.convertStartDate = parse(data.startDate, 'yyyy-MM-dd', new Date());
              data.convertEndDate = parse(data.endDate, 'yyyy-MM-dd', new Date());
            })
          } else {
          }
          callback({
            recordsTotal: resp.data.totalElements,
            recordsFiltered: resp.data.totalElements,
            data: []
          });
        }, error => {
          this.swalAlert.showAlertSwal(error)
        });
      },
      columns: [{ data: 'startDate' }, { data: 'name' },{ data: 'eventType.name' }, { data: 'province.name' }, { data: 'id' }],
      order: [[1, 'asc']]
    };
  }

  /**
   * function refresh datatable
   */
  refreshDatatable(): void {
    this.paramsLike = []
    if (this.filterData) {
      const paramsFirstName = {
        field: "name",
        dataType: "string",
        value: this.filterData
      }
      const paramsComponentName = {
        field: "eventType.name",
        dataType: "string",
        value: this.filterData
      }
      const paramsSubComponentDescription = {
        field: "province.name",
        dataType: "string",
        value: this.filterData
      }
      const paramsDescription = {
        field: "regency.name",
        dataType: "string",
        value: this.filterData
      }
      this.paramsLike.push(paramsFirstName)
      this.paramsLike.push(paramsComponentName)
      this.paramsLike.push(paramsSubComponentDescription)
      this.paramsLike.push(paramsDescription)
    } else {
      this.paramsLike = []
    }
    console.log(this.dtElement)

    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  startLoading() {
    if (this.finishLoadDataCount == 0) {
      this.finishLoadDataCount ++
      this.finishLoadData = false
    } else {
      this.finishLoadDataCount ++
    }
  }

  stopLoading() {
    this.finishLoadDataCount --
    if (this.finishLoadDataCount == 0) {
      this.finishLoadData = true
    }
  }

  goToDetailKegiatan(id, status) {
    this.rowClicked.emit(id)
  }


}
