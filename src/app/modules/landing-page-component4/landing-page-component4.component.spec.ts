import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageComponent4Component } from './landing-page-component4.component';

describe('LandingPageComponent4Component', () => {
  let component: LandingPageComponent4Component;
  let fixture: ComponentFixture<LandingPageComponent4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPageComponent4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingPageComponent4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
