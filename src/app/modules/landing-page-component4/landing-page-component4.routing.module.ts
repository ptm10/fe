import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LandingPageComponent4Component} from "./landing-page-component4.component";

const routes: Routes = [
    {
        path: 'component4',
        component: LandingPageComponent4Component
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LandingPageComponent4RoutingModule {}
