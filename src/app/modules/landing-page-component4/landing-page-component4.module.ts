import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {LandingPageComponent4RoutingModule} from "./landing-page-component4.routing.module";
import {TableauModule} from "ngx-tableau";
import {LandingPageComponent4Component} from "./landing-page-component4.component";


@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [LandingPageComponent4Component],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        NgbTooltipModule,
        LandingPageComponent4RoutingModule,
        NgbModule,
        TableauModule
    ],
  providers: []
})
export class LandingPageComponent4Module { }
