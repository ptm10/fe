import { Component, OnInit } from '@angular/core';
import {DashboardService} from "../../core/services/Dashboard/dashboard.service";
import {Router} from "@angular/router";
import {VizCreateOptions} from "ngx-tableau";

@Component({
  selector: 'app-landing-page-component4',
  templateUrl: './landing-page-component4.component.html',
  styleUrls: ['./landing-page-component4.component.scss']
})
export class LandingPageComponent4Component implements OnInit {

  showNavigationIndicators: any;
  data: any
  urlTableu: string
  loadingTableau = false
  constructor(
      private service: DashboardService,
      private router: Router
  ) { }

  ngOnInit(): void {
    this.loginTableu(4)
  }

  // Options
  options: VizCreateOptions = {
    hideTabs: false,
    hideToolbar: true,
    width: '100%',
    height: '1000px',
    toolbarPosition: (event) => {
      console.log(event);
    },
    onFirstInteractive: (event) => {
      console.log('I was called', event);
    },
  };

  loginTableu(id) {
    this.loadingTableau = true
    this.service.loginTableu().subscribe(
        resp => {
          this.data = resp
          console.log(this.data)
          this.urlTableu = 'https://dashboardemis.kemenag.go.id/trusted/' + this.data + '/views/MEQR-'+ id+'/MEQR-Komponen' +id+ '?:embed=yes'
          console.log('ini tableau', this.urlTableu)
          this.loadingTableau = false
        }, error => {
          this.loadingTableau = false
          console.log(error)
        }
    )
  }

  goToLogin() {
    this.router.navigate(['admin/login'])
  }

  goToHome() {
    this.router.navigate([''])
  }

}
