import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {LandingPageComponent2RoutingModule} from "./landing-page-component2.routing.module";
import {TableauModule} from "ngx-tableau";
import {LandingPageComponent2Component} from "./landing-page-component2.component";


@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [LandingPageComponent2Component],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        NgbTooltipModule,
        LandingPageComponent2RoutingModule,
        NgbModule,
        TableauModule
    ],
  providers: []
})
export class LandingPageComponent2Module { }
