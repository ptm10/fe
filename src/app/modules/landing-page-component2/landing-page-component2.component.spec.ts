import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageComponent2Component } from './landing-page-component2.component';

describe('LandingPageComponent2Component', () => {
  let component: LandingPageComponent2Component;
  let fixture: ComponentFixture<LandingPageComponent2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPageComponent2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingPageComponent2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
