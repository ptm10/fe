import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LandingPageComponent2Component} from "./landing-page-component2.component";

const routes: Routes = [
    {
        path: 'component2',
        component: LandingPageComponent2Component
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LandingPageComponent2RoutingModule {}
