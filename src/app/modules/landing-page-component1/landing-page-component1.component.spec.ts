import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageComponent1Component } from './landing-page-component1.component';

describe('LandingPageComponent1Component', () => {
  let component: LandingPageComponent1Component;
  let fixture: ComponentFixture<LandingPageComponent1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPageComponent1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingPageComponent1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
