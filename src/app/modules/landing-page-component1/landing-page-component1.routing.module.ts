import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LandingPageComponent1Component} from "./landing-page-component1.component";

const routes: Routes = [
    {
        path: 'component1',
        component: LandingPageComponent1Component
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LandingPageComponent1RoutingModule {}
