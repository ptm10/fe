import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {LandingPageComponent1RoutingModule} from "./landing-page-component1.routing.module";
import {LandingPageComponent1Component} from "./landing-page-component1.component";
import {TableauModule} from "ngx-tableau";

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [LandingPageComponent1Component],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        NgbTooltipModule,
        LandingPageComponent1RoutingModule,
        NgbModule,
        TableauModule
    ],
  providers: []
})
export class LandingPageComponent1Module { }
