import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
  NgbNavModule,
  NgbDropdownModule,
  NgbPaginationModule,
  NgbModalModule,
  NgbTooltipModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {HealthCheckComponent} from "./health-check.component";
import {HealthCheckRoutingModule} from "./health-check.routing.module";

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [HealthCheckComponent],
  imports: [
    CommonModule,
    NgbNavModule,
    NgbModalModule,
    FormsModule,
    Ng2SearchPipeModule,
    NgbDropdownModule,
    ReactiveFormsModule,
    Ng5SliderModule,
    NgSelectModule,
    NgbPaginationModule,
    NgbTooltipModule,
      HealthCheckRoutingModule
  ],
  providers: []
})
export class HealthCheckModule { }
