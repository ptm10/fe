import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng5SliderModule } from 'ng5-slider';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import {
    NgbNavModule,
    NgbDropdownModule,
    NgbPaginationModule,
    NgbModalModule,
    NgbTooltipModule, NgbModule
} from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import {LandingPageComponent3RoutingModule} from "./landing-page-component3.routing.module";
import {TableauModule} from "ngx-tableau";
import {LandingPageComponent3Component} from "./landing-page-component3.component";

@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [LandingPageComponent3Component],
    imports: [
        CommonModule,
        NgbNavModule,
        NgbModalModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgbDropdownModule,
        ReactiveFormsModule,
        Ng5SliderModule,
        NgSelectModule,
        NgbPaginationModule,
        NgbTooltipModule,
        LandingPageComponent3RoutingModule,
        NgbModule,
        TableauModule
    ],
  providers: []
})
export class LandingPageComponent3Module { }
