import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPageComponent3Component } from './landing-page-component3.component';

describe('LandingPageComponent3Component', () => {
  let component: LandingPageComponent3Component;
  let fixture: ComponentFixture<LandingPageComponent3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LandingPageComponent3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingPageComponent3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
