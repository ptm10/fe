import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LandingPageComponent3Component} from "./landing-page-component3.component";

const routes: Routes = [
    {
        path: 'component3',
        component: LandingPageComponent3Component
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LandingPageComponent3RoutingModule {}
