import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
    {
        id: 9,
        label: 'AWP',
        icon: 'mdi mdi-border-all',
        link: '/admin/awp',
    },
    {
        id: 9,
        label: 'Project Component',
        icon: 'mdi mdi-file',
        link: 'dashboard-component/project-component',
    },
    {
        id: 10,
        label: 'Administrasi',
        icon: 'mdi mdi-file',
        subItems: [
            {
                id: 3,
                label: 'Staf',
                link: 'administrasi/detail',
                parentId: 2
            },
        ]
    },

];

