import {Component, OnInit, AfterViewInit, Inject, ChangeDetectorRef} from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { LanguageService } from '../../core/services/language.service';

import { EventService } from '../../core/services/event.service';
import { AuthenticationService } from '../../core/services/auth.service';
import { AuthfakeauthenticationService } from '../../core/services/authfake.service';

import { DOCUMENT } from '@angular/common';

import {MENU, MENUAdmin, MENULsp, MENUSupervisor} from './menu';
import { MenuItem } from './menu.model';
import { environment } from '../../../environments/environment';
import {ProfileService} from "../../core/services/Profile/profile.service";
import {UsersModels} from "../../core/models/users.models";
import {FileService} from "../../core/services/Image/file.service";
import {NotificationModel} from "../../core/models/notification.model";
import Swal from "sweetalert2";
import {parse} from "date-fns";
import {SwalAlert} from "../../core/helpers/Swal";

@Component({
  selector: 'app-horizontaltopbar',
  templateUrl: './horizontaltopbar.component.html',
  styleUrls: ['./horizontaltopbar.component.scss']
})

/**
 * Horizontal Topbar and navbar specified
 */
export class HorizontaltopbarComponent implements OnInit, AfterViewInit {

  element;
  cookieValue;
  flagvalue;
  countryName;
  valueset;

  menuItems = [];
  menuApprove = []

  firstNameLogged: string
  lastNameLogged: string
  pictureId: string
  urlFiles = environment.serverFile
  urlPhotoProfile: any
  existProfilePicture = false
  userLogged: UsersModels
  listNotification: NotificationModel[]
  listNotificationExist: NotificationModel[]
  countNewMessage: number = 0
  countPermission: number = 0
  countActivity: number = 0
  loadingGoToTask = false
  validateStaf: number
  breadCrumbItems: Array<{}>;


  listLang = [
    { text: 'English', flag: 'assets/images/flags/us.jpg', lang: 'en' },
    { text: 'Spanish', flag: 'assets/images/flags/spain.jpg', lang: 'es' },
    { text: 'German', flag: 'assets/images/flags/germany.jpg', lang: 'de' },
    { text: 'Italian', flag: 'assets/images/flags/italy.jpg', lang: 'it' },
    { text: 'Russian', flag: 'assets/images/flags/russia.jpg', lang: 'ru' },
  ];

  // tslint:disable-next-line: max-line-length
  constructor(@Inject(DOCUMENT) private document: any, private router: Router, private eventService: EventService, private authService: AuthenticationService,
    private authFackservice: AuthfakeauthenticationService,
    public languageService: LanguageService,
    public service: ProfileService,
    private roles: AuthfakeauthenticationService,
    private serviceImage: FileService,
    private cdr: ChangeDetectorRef,
    private swalAlert: SwalAlert,

  // tslint:disable-next-line: variable-name
    public _cookiesService: CookieService) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.activateMenu();
      }
    });
  }

  ngOnInit(): void {
    this.element = document.documentElement;
    this.getDetailProfile()
    this.getListNotification()
    this.service.updateNotification.subscribe(
        resp => {
          this.getListNotification()
        }
    )
    this.breadCrumbItems = [{ label: 'AWP', active: true }];
    // this.initialize();

    this.cookieValue = this._cookiesService.get('lang');
    const val = this.listLang.filter(x => x.lang === this.cookieValue);
    this.countryName = val.map(element => element.text);
    if (val.length === 0) {
      if (this.flagvalue === undefined) { this.valueset = 'assets/images/flags/us.jpg'; }
    } else {
      this.flagvalue = val.map(element => element.flag);
    }
  }

  setLanguage(text: string, lang: string, flag: string) {
    this.countryName = text;
    this.flagvalue = flag;
    this.cookieValue = lang;
    this.languageService.setLanguage(lang);
  }

  /**
   * Logout the user
   */
  logout() {
    if (environment.defaultauth === 'firebase') {
      // this.authService.logout();
    } else {
      this.authFackservice.logout();
    }
    this.router.navigate(['/admin/login']);
  }

  /**
   * check menu
   */
  clickMenu(event) {
    let allMenu = document.getElementsByClassName('nav-link')
    for (let i = 0; i < allMenu.length; i++) {
      let findActive = allMenu[i].classList.contains('active')
      if (findActive) {
        allMenu[i].classList.remove('active')
      }
    }
    if (event !== null) {
      event.target.classList.add('active')
    }
    const links = document.getElementsByClassName('side-nav-link-ref');

    for (let i = 0; i < links.length; i++) {
      // reset menu
      this.resetMenuLinkChild(links[i]);
    }
  }

  resetMenuLinkChild(el) {
    const parent = el.parentElement;
    if (parent) {
      parent.classList.remove('active');
      const parent2 = parent.parentElement;
      this._removeAllClass('mm-active');
      this._removeAllClass('mm-show');
      if (parent2) {
        parent2.classList.remove('active');
        const parent3 = parent2.parentElement;
        if (parent3) {
          parent3.classList.remove('active');
          const parent4 = parent3.parentElement;
          if (parent4) {
            parent4.classList.remove('active');
            const parent5 = parent4.parentElement;
            if (parent5) {
              parent5.classList.remove('active');
              // const menuelement = document.getElementById("topnav-menu-content")
              // if (menuelement !== null) {
              //   if (menuelement.classList.contains('active'))
              //     this.clickMenu(null)
              // }
            }
          }
        }
      }
    }
  }

  /**
   * On menu click
   */
  onMenuClick(event) {
    const nextEl = event.target.nextElementSibling;
    if (nextEl) {
      const parentEl = event.target.parentNode;
      if (parentEl) {
        parentEl.classList.remove("show");
      } else {
        nextEl.classList.toggle("show");
      }
    }
    return false;
  }

  ngAfterViewInit() {
    this.activateMenu();
  }

  /**
   * remove active and mm-active class
   */
  _removeAllClass(className) {
    const els = document.getElementsByClassName(className);
    while (els[0]) {
      els[0].classList.remove(className);
    }
  }

  /**
   * Togglemenu bar
   */
  toggleMenubar() {
    const element = document.getElementById('topnav-menu-content');
    element.classList.toggle('show');
  }

  /**
   * Activates the menu
   */
  private activateMenu() {
    const resetParent = (el: any) => {
      const parent = el.parentElement;
      if (parent) {
        parent.classList.remove('active');
        const parent2 = parent.parentElement;
        this._removeAllClass('mm-active');
        this._removeAllClass('mm-show');
        if (parent2) {
          parent2.classList.remove('active');
          const parent3 = parent2.parentElement;
          if (parent3) {
            parent3.classList.remove('active');
            const parent4 = parent3.parentElement;
            if (parent4) {
              parent4.classList.remove('active');
              const parent5 = parent4.parentElement;
              if (parent5) {
                parent5.classList.remove('active');
                // const menuelement = document.getElementById("topnav-menu-content")
                // if (menuelement !== null) {
                //   if (menuelement.classList.contains('active'))
                //     this.clickMenu(null)
                // }
              }
            }
          }
        }
      }
    };

    // activate menu item based on location
    const links = document.getElementsByClassName('side-nav-link-ref');
    let matchingMenuItem = null;
    // tslint:disable-next-line: prefer-for-of
    // for (let i = 0; i < links.length; i++) {
    //   // reset menu
    //   resetParent(links[i]);
    // }
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < links.length; i++) {
      // tslint:disable-next-line: no-string-literal
      if (location.pathname === links[i]['pathname']) {
        console.log(location.pathname)
        matchingMenuItem = links[i];
        for (let i = 0; i < links.length; i++) {
          // reset menu
          resetParent(links[i]);
        }
        break;
      }
    }

    if (matchingMenuItem) {
      const parent = matchingMenuItem.parentElement;
      /**
       * We should come up with non hard coded approach
       */
      if (parent) {
        this.clickMenu(null)
        parent.classList.add('active');
        const parent2 = parent.parentElement;
        if (parent2) {
          parent2.classList.add('active');
          const parent3 = parent2.parentElement;
          if (parent3) {
            parent3.classList.add('active');
            const parent4 = parent3.parentElement;
            if (parent4) {
              parent4.classList.add('active');
              const parent5 = parent4.parentElement;
              if (parent5) {
                parent5.classList.add('active');
                const parent6 = parent5.parentElement;
                if (parent6) {
                  parent6.classList.add('active');
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * on settings button clicked from topbar
   */
  onSettingsButtonClicked() {
    document.body.classList.toggle('right-bar-enabled');
  }

  /**
   * Fullscreen method
   */
  fullscreen() {
    document.body.classList.toggle('fullscreen-enable');
    if (
      !document.fullscreenElement && !this.element.mozFullScreenElement &&
      !this.element.webkitFullscreenElement) {
      if (this.element.requestFullscreen) {
        this.element.requestFullscreen();
      } else if (this.element.mozRequestFullScreen) {
        /* Firefox */
        this.element.mozRequestFullScreen();
      } else if (this.element.webkitRequestFullscreen) {
        /* Chrome, Safari and Opera */
        this.element.webkitRequestFullscreen();
      } else if (this.element.msRequestFullscreen) {
        /* IE/Edge */
        this.element.msRequestFullscreen();
      }
    } else {
      if (this.document.exitFullscreen) {
        this.document.exitFullscreen();
      } else if (this.document.mozCancelFullScreen) {
        /* Firefox */
        this.document.mozCancelFullScreen();
      } else if (this.document.webkitExitFullscreen) {
        /* Chrome, Safari and Opera */
        this.document.webkitExitFullscreen();
      } else if (this.document.msExitFullscreen) {
        /* IE/Edge */
        this.document.msExitFullscreen();
      }
    }
  }

  /**
   * Initialize
   */

  initialize(): void {
    this.menuItems = []
    this.menuApprove = []
    if (this.roles.hasAuthority('Administrator')) {
      this.menuItems = MENUAdmin
    } else if (this.roles.hasAuthority('LSP')) {
      this.menuItems = MENULsp
    } else {
      if (this.validateStaf > 0) {
        this.menuItems = MENUSupervisor
      } else {
        this.menuItems = MENU
      }
    }
    // this.getCountNotificationCuti()
    // this.getCountNotificationActivity()
  }
  getCountNotificationCuti() {
    const x = this.menuItems.find(data => data.id == 10)
    const y = x.subItems.find(data => data.id == 21)
    if (this.countPermission !== 0) {
      y.badge.text = this.countPermission.toString()
    } else {
      y.badge.text = ''
    }
  }

  getCountNotificationActivity() {
    const x = this.menuItems.find(data => data.id == 10)
    const y = x.subItems.find(data => data.id == 20)
    if (this.countActivity !== 0) {
      y.badge.text = this.countActivity.toString()
    } else {
      y.badge.text = ''
    }
  }

  validateSuperVisor() {
    this.service.validateStaf().subscribe(
        resp => {
          if (resp.success) {
            this.validateStaf = resp.data
            this.initialize();
          }
        }, error => {
          this.swalAlert.showAlertSwal(error)
        }
    )
  }

  /**
   * Returns true or false if given menu item has child or not
   * @param item menuItem
   */
  hasItems(item: MenuItem) {
    return item.subItems !== undefined ? item.subItems.length > 0 : false;
  }


  getDetailProfile() {
    this.service.detailProfile().subscribe(
        resp => {
          if (resp.success) {
            this.userLogged = resp.data
            this.pictureId = this.userLogged?.profilePicture?.id
            this.getUrlPicture()
          }
        }
    )
  }

  getUrlPicture() {
    if (this.pictureId) {
      this.getFilePhotos()
      this.existProfilePicture = true
    } else {
      this.existProfilePicture = false
    }
  }

  getFilePhotos() {
    this.serviceImage.getUrlPhoto(this.pictureId).subscribe(
        resp => {
          let reader = new FileReader();
          reader.addEventListener("load", () => {
            this.urlPhotoProfile = reader.result;
          }, false);
          reader.readAsDataURL(resp);
        }
    )
  }

  getListNotification() {
    this.service.listNotification().subscribe(
        resp => {
          if (resp.success) {
            this.countNewMessage = 0
            this.countActivity = 0
            this.countPermission = 0
            this.listNotification = resp.data.content
            this.listNotificationExist = resp.data.content
            this.listNotification.forEach(dataResp => {
              dataResp.convertDateCreated = parse(dataResp.createdAt, 'dd-MM-yyyy HH:mm:ss', new Date());
              if (!dataResp.readed) {
                this.countNewMessage = this.countNewMessage + 1
                if (dataResp.taskType == 2) {
                  this.countPermission = this.countPermission + 1
                }
                if (dataResp.taskType == 3) {
                  this.countActivity = this.countActivity + 1
                }
              }
            })
            this.validateSuperVisor()
          }
        }
    )
  }

  searchFilter(e) {
    const searchStr = e.target.value
    this.listNotification = this.listNotificationExist.filter((product) => {
      return product.message.toLowerCase().search(searchStr.toLowerCase()) !== -1
    });
  }

  /**
   * Menu Profile
   */
  goToMenuProfile() {
    this.router.navigate(['/profile'])
  }

  goToMenuListTask() {
    this.router.navigate(['/task/list-task'])
  }

  goToDetailListTask(data) {
    this.loadingGoToTask = true
    const params = {
      readed: true
    }
    if (!data.readed) {
      if (data?.taskType == 1) {
        this.router.navigate(['task/detail-task/' + data.taskId])
      } else if (data?.taskType == 2) {
        this.router.navigate(['administrasi/detail-approve-permission/' + data.taskId])
      } else if (data?.taskType == 3) {
        this.router.navigate(['administrasi/detail-application-event/' + data.taskId])
      } else if (data?.taskType == 4) {
        this.router.navigate(['task/detail-add-task-staff/' + data.taskId])
      } else if (data?.taskType == 5) {
        this.router.navigate(['implement/detail-kegiatan-perencanaan/' + data.taskId])
      } else if (data?.taskType == 6) {
        this.router.navigate(['activity/detail-event/' + data.taskId])
      } else if (data?.taskType == 7) {
        this.router.navigate(['activity/detail-progress-event/' + data.taskId])
      } else if (data?.taskType == 8) {
        this.router.navigate(['activity/detail-report-event/' + data.taskId + '/-'])
      } else if (data?.taskType == 9) {
        this.router.navigate(['implement/detail-report-activity/' + data.taskId])
      }

      this.service.changeStatusRead(data.id, params).subscribe(
          resp => {
            if (resp.success) {
              this.getListNotification()
              this.loadingGoToTask = false
            }
          }, error => {
            this.loadingGoToTask = false
            // this.swalAlert.showAlertSwal(error)
          }
      )
    } else {
      this.loadingGoToTask = false
      if (data?.taskType == 1) {
        this.router.navigate(['task/detail-task/' + data.taskId])
      } else if (data?.taskType == 2) {
        this.router.navigate(['administrasi/detail-approve-permission/' + data.taskId])
      } else if (data?.taskType == 3) {
        this.router.navigate(['administrasi/detail-application-event/' + data.taskId])
      } else if (data?.taskType == 4) {
        this.router.navigate(['task/detail-add-task-staff/' + data.taskId])
      } else if (data?.taskType == 5) {
        this.router.navigate(['implement/detail-kegiatan-perencanaan/' + data.taskId])
      } else if (data?.taskType == 6) {
        this.router.navigate(['activity/detail-event/' + data.taskId])
      } else if (data?.taskType == 7) {
        this.router.navigate(['activity/detail-progress-event/' + data.taskId])
      } else if (data?.taskType == 8) {
        this.router.navigate(['activity/detail-report-event/' + data.taskId + '/-'])
      } else if (data?.taskType == 9) {
        this.router.navigate(['implement/detail-report-activity/' + data.taskId])
      }
    }
  }

}
