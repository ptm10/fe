import { MenuItem } from "./menu.model";
import { ResultOutputComponent } from "../../modules/pages/monev/result-output/result-output.component";

export const MENU: MenuItem[] = [
  {
    id: 15,
    label: "Dashboard",
    icon: "mdi mdi-monitor",
    link: "/dashboards/dashboard-component",
  },
  {
    id: 9,
    label: "Project Component",
    icon: "mdi mdi-file",
    link: "dashboard-component/project-component",
    role: ["Administrator"],
  },
  {
    id: 9,
    label: "AWP",
    icon: "mdi mdi-border-all",
    link: "/admin/awp",
  },
  {
    id: 11,
    label: "Kegiatan",
    icon: "mdi mdi-border-all",
    link: "/implement/kegiatan",
  },
  {
    id: 12,
    label: "Event",
    icon: "mdi mdi-clipboard-list",
    link: "/activity/event",
  },
  {
    id: 9,
    label: "Pustaka",
    icon: "mdi mdi-book-open",
    link: "file/pustaka",
  },
  {
    id: 9,
    label: "Monev",
    icon: "mdi mdi-chart-pie",
    link: "monev/list-monev",
    subItems: [
      {
        id: 16,
        label: "Output",
        parentId: 14,
        subItems: [
          {
            id: 20,
            label: "Target",
            link: "monev/list-monev/output",
            parentId: 16,
          },
          {
            id: 21,
            label: "Hasil",
            parentId: 16,
            link: "monev/monev-result-output/output",
          },
        ],
      },
      {
        id: 17,
        label: "Intermediate Outcome",
        parentId: 14,
        subItems: [
          {
            id: 3,
            label: "Target",
            link: "monev/monev-intermediate/intermediate",
            parentId: 17,
          },
          {
            id: 22,
            label: "Hasil",
            parentId: 17,
            link: "monev/monev-result-intermediate/intermediate",
          },
        ],
      },
      {
        id: 18,
        label: "Midterm Outcome",
        parentId: 14,
        subItems: [
          {
            id: 3,
            label: "Target",
            link: "monev/monev-midterm/Midterm",
            parentId: 18,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 18,
            link: "monev/monev-result-midterm/Midterm",
          },
        ],
      },
      {
        id: 19,
        label: "Longterm Outcome",
        parentId: 14,
        subItems: [
          {
            id: 3,
            label: "Target",
            link: "monev/monev-longterm/Longterm",
            parentId: 19,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 19,
            link: "monev/monev-result-longterm/Longterm",
          },
        ],
      },
      {
        id: 20,
        label: "Result Chain Diagram",
        parentId: 14,
        link: "monev/monev-result-chain",
      },
    ],
  },
  {
    id: 10,
    label: "Administrasi",
    icon: "mdi mdi-file",
    subItems: [
      {
        id: 3,
        label: "Direktori Staf",
        link: "administrasi/detail",
        parentId: 2,
      },
      {
        id: 4,
        label: "Penomoran Surat",
        link: "surat",
        parentId: 2,
      },
    ],
  },
];

export const MENUAdmin: MenuItem[] = [
  {
    id: 15,
    label: "Dashboard",
    icon: "mdi mdi-monitor",
    link: "/dashboards/dashboard-component",
  },
  {
    id: 9,
    label: "Project Component",
    icon: "mdi mdi-file",
    link: "dashboard-component/project-component",
    role: ["Administrator"],
  },
  {
    id: 9,
    label: "AWP",
    icon: "mdi mdi-border-all",
    link: "/admin/awp",
  },
  {
    id: 11,
    label: "Kegiatan",
    icon: "mdi mdi-clipboard-list",
    link: "/implement/kegiatan",
  },
  {
    id: 12,
    label: "Event",
    icon: "mdi mdi-clipboard-list",
    link: "/activity/event",
  },
  {
    id: 9,
    label: "Pustaka",
    icon: "mdi mdi-book-open",
    link: "file/pustaka",
  },
  {
    id: 14,
    label: "Monev",
    icon: "mdi mdi-chart-pie",
    link: "monev/list-monev",
    subItems: [
      {
        id: 16,
        label: "Output",
        parentId: 14,
        subItems: [
          {
            id: 20,
            label: "Target",
            link: "monev/list-monev/output",
            parentId: 16,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 16,
            link: "monev/monev-result-output/output",
          },
        ],
      },
      {
        id: 17,
        label: "Intermediate Outcome",
        parentId: 14,
        subItems: [
          {
            id: 3,
            label: "Target",
            link: "monev/monev-intermediate/intermediate",
            parentId: 17,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 17,
            link: "monev/monev-result-intermediate/intermediate",
          },
        ],
      },
      {
        id: 18,
        label: "Midterm Outcome",
        parentId: 14,
        subItems: [
          {
            id: 3,
            label: "Target",
            link: "monev/monev-midterm/Midterm",
            parentId: 18,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 18,
            link: "monev/monev-result-midterm/Midterm",
          },
        ],
      },
      {
        id: 19,
        label: "Longterm Outcome",
        parentId: 14,
        subItems: [
          {
            id: 3,
            label: "Target",
            link: "monev/monev-longterm/Longterm",
            parentId: 19,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 19,
            link: "monev/monev-result-longterm/Longterm",
          },
        ],
      },
      {
        id: 20,
        label: "Result Chain Diagram",
        parentId: 14,
        link: "monev/monev-result-chain",
      },
    ],
  },
  {
    id: 10,
    label: "Administrasi",
    icon: "mdi mdi-file",
    subItems: [
      {
        id: 3,
        label: "Direktori Staf",
        link: "administrasi/detail",
        parentId: 2,
      },
      {
        id: 4,
        label: "Penomoran Surat",
        link: "surat",
        parentId: 2,
      },
    ],
  },
  {
    id: 9,
    label: "Konfigurasi",
    icon: "mdi mdi-account-settings",
    subItems: [
      {
        id: 20,
        label: "Pengguna",
        link: "/configuration/list",
        parentId: 9,
      },
      {
        id: 21,
        label: "Formulir",
        link: "/configuration/list-event",
        parentId: 9,
      },
    ],
  },
];

export const MENUSupervisor: MenuItem[] = [
  {
    id: 15,
    label: "Dashboard",
    icon: "mdi mdi-monitor",
    link: "/dashboards/dashboard-component",
  },
  {
    id: 9,
    label: "Project Component",
    icon: "mdi mdi-file",
    link: "dashboard-component/project-component",
    role: ["Administrator"],
  },
  {
    id: 9,
    label: "AWP",
    icon: "mdi mdi-border-all",
    link: "/admin/awp",
  },
  {
    id: 11,
    label: "Kegiatan",
    icon: "mdi mdi-clipboard-list",
    link: "/implement/kegiatan",
  },
  {
    id: 12,
    label: "Event",
    icon: "mdi mdi-clipboard-list",
    link: "/activity/event",
  },
  {
    id: 9,
    label: "Pustaka",
    icon: "mdi mdi-book-open",
    link: "file/pustaka",
  },
  {
    id: 9,
    label: "Monev",
    icon: "mdi mdi-chart-pie",
    link: "monev/list-monev",
    subItems: [
      {
        id: 16,
        label: "Output",
        parentId: 14,
        subItems: [
          {
            id: 20,
            label: "Target",
            link: "monev/list-monev/output",
            parentId: 16,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 18,
            link: "monev/monev-result-output/output",
          },
        ],
      },
      {
        id: 17,
        label: "Intermediate Outcome",
        parentId: 14,
        subItems: [
          {
            id: 3,
            label: "Target",
            link: "monev/monev-intermediate/intermediate",
            parentId: 17,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 17,
            link: "monev/monev-result-intermediate/intermediate",
          },
        ],
      },
      {
        id: 18,
        label: "Midterm Outcome",
        parentId: 14,
        subItems: [
          {
            id: 3,
            label: "Target",
            link: "monev/monev-midterm/Midterm",
            parentId: 18,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 18,
            link: "monev/monev-result-midterm/Midterm",
          },
        ],
      },
      {
        id: 19,
        label: "Longterm Outcome",
        parentId: 14,
        subItems: [
          {
            id: 3,
            label: "Target",
            link: "monev/monev-longterm/Longterm",
            parentId: 19,
          },
          {
            id: 23,
            label: "Hasil",
            parentId: 19,
            link: "monev/monev-result-longterm/Longterm",
          },
        ],
      },
      {
        id: 20,
        label: "Result Chain Diagram",
        parentId: 14,
        link: "monev/monev-result-chain",
      },
    ],
  },
  {
    id: 10,
    label: "Administrasi",
    icon: "mdi mdi-file",
    subItems: [
      {
        id: 3,
        label: "Direktori Staf",
        link: "administrasi/detail",
        parentId: 2,
      },
      {
        id: 4,
        label: "Penomoran Surat",
        link: "surat",
        parentId: 2,
      },
    ],
  },
];

export const MENULsp: MenuItem[] = [
  {
    id: 11,
    label: "Kegiatan",
    icon: "mdi mdi-clipboard-list",
    link: "/implement/kegiatan",
  },
];
