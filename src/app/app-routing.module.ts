import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './core/guards/auth.guard';
import { LayoutComponent } from './layouts/layout.component';
import { LoginComponent } from './modules/pages/Login/auth/login/login.component';
import { Page404Component } from './shared/extrapages/page404/page404.component';
import {HealthCheckComponent} from "./modules/health-check/health-check.component";
import {LandingPageComponent} from "./modules/landing-page/landing-page.component";

const routes: Routes = [
  { path: 'admin', loadChildren: () => import('./modules/pages/Login/account.module').then(m => m.AccountModule) },
  // tslint:disable-next-line: max-line-length
  { path: '', component: LandingPageComponent, pathMatch: 'full'},
  { path: '', component: LayoutComponent, loadChildren: () => import('./modules/pages/pages.module').then(m => m.PagesModule), canActivate: [AuthGuard]},
  { path: 'pages', loadChildren: () => import('./shared/extrapages/extrapages.module').then(m => m.ExtrapagesModule), canActivate: [AuthGuard] },
  { path: 'health', loadChildren: () => import('./modules/health-check/health-check.module').then(m => m.HealthCheckModule)},
  { path: 'landing', loadChildren: () => import('./modules/landing-page-component1/landing-page-component1.module').then(m => m.LandingPageComponent1Module)},
  { path: 'landing2', loadChildren: () => import('./modules/landing-page-component2/landing-page-component2.module').then(m => m.LandingPageComponent2Module)},
  { path: 'landing3', loadChildren: () => import('./modules/landing-page-component3/landing-page-component3.module').then(m => m.LandingPageComponent3Module)},
  { path: 'landing4', loadChildren: () => import('./modules/landing-page-component4/landing-page-component4.module').then(m => m.LandingPageComponent4Module)},
  { path: '', component: HealthCheckComponent },
  { path: '404_not_found', component: Page404Component },
  { path: '**', component: Page404Component },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'top', relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
