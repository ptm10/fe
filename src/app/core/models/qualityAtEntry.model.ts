export class QualityAtEntryModel {
    constructor(
        public id: string,
        public number: number,
        public question: string,
        public createdAt: string,
        public updatedAt: string,
        public checked: any = null,
        public perbaikan: string
    ) {
    }
}
