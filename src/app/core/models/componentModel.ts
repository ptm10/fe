
export class ComponentModel {
    constructor(
        public id: string,
        public code: string,
        public description: string,
        public subComponent: ComponentModel[],
        public open: boolean,
        public checked: boolean = false,
    ) {
    }
}
