export class FileModel {
    constructor(
        public id: string,
        public filepath: string,
        public filename: string,
        public fileExt: string,
        public createdAt: string,
        public attachmentType: AttachmentType
    ) {
    }
}

export class AttachmentType {
    constructor(
        public id: string,
        public type: number,
        public fileType: string,
        public allowedMimeType: string,
        public allowedSizeInKb: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class FileSuccessIndicator {
    constructor(
        public id: string,
        public file: FileModel,
        public year: number,
        public fileId: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}
