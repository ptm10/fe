export class RoleModels {
  constructor(
    public id: string,
    public name: string,
    public createdAt: string,
    public updatedAt: string,
    public checked: boolean = false
  ) {}
}
