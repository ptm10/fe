export class DataResponse {
  success: boolean;
  code: string;
  message: string;
  data: any;

  static defaultValue() {
    return {
      success: null,
      code: null,
      message: null,
      data: null
    }
  }
}
