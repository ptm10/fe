export class PDOModel {
    constructor(
        public id: string,
        public name: string,
        public code: string,
        public description: string,
        public y_2020: string,
        public y_2021: string,
        public y_2022: string,
        public y_2023: string,
        public y_2024: string,
        public createdAt: string,
        public updatedAt: string,
        public selectedYearInformation: any,
        public target: string = ''
    ) {}
}

export class PdoIriModel {
    constructor(
        public id: string,
        public name: string,
        public code: string,
        public description: string,
        public iri: IriModel[],
        public pdo: PDOModel = null,
        public y_2020: string,
        public y_2021: string,
        public y_2022: string,
        public y_2023: string,
        public y_2024: string,
        public createdAt: string,
        public updatedAt: string,
        public selectedYearInformation: any,
        public target: string = null
    ) {
    }
}

export class IriModel {
    constructor(
        public id: string,
        public name: string,
        public code: string,
        public description: string,
        public iri: PDOModel,
        public y_2020: string,
        public y_2021: string,
        public y_2022: string,
        public y_2023: string,
        public y_2024: string,
        public createdAt: string,
        public updatedAt: string,
        public selectedYearInformation: any,
        public target: string = ''
    ) {}
}
