export class Pdfs {
    id?: string;
    src?: string;
    isCollapsed?: boolean;
    totalPages?: number;
    page = 1;
    pdfZoom?: number = 1;
}
