import {ComponentModel} from "./componentModel";
import {PDOModel} from "./PDO.model";
import {UsersModels} from "./users.models";
import {FileModel, FileSuccessIndicator} from "./file.model";

export class Monev {
    constructor(
        public id: string,
        public monevTypeId: string,
        public componentId: string,
        public component: ComponentModel,
        public subComponentId: string,
        public subComponent: ComponentModel,
        public pdo: PDOModel,
        public iri: PDOModel,
        public resultChainCode: string,
        public result: string,
        public successIndicator: string,
        public successIndicatorCode: string,
        public baselineValue: string,
        public targetDescription: string,
        public targetTotal: string,
        public cumulativeTarget2020: string,
        public cumulativeTargetDummy2020: boolean,
        public cumulativeTarget2021: string,
        public cumulativeTargetDummy2021: boolean,
        public cumulativeTarget2022: string,
        public cumulativeTargetDummy2022: boolean,
        public cumulativeTarget2023: string,
        public cumulativeTargetDummy2023: boolean,
        public cumulativeTarget2024: string,
        public cumulativeTargetDummy2024: boolean,
        public createdAt: string,
        public updatedAt: string,
        public targetYearNow: string,
        public resultIndicator: string = '',
        public result2020: string,
        public result2021: string,
        public result2022: string,
        public result2023: string,
        public result2024: string,
        public resultStatus2020: number,
        public resultStatus2021: number,
        public resultStatus2022: number,
        public resultStatus2023: number,
        public resultStatus2024: number,
        public status: number,
        public convertUpdated: Date,
        public updatedByUser: UsersModels,
        public awpReportSuccessIndicators: string,
        public rekaptulationActivity: RekaptulationActivity[],
        public resultYear: string,
        public questions: Question[],
    ) {
    }
}

export class Monev2 {
    constructor(
        public id: string,
        public name: string,
        public year: number,
        public monevTypeId: string,
        public componentId: string,
        public component: ComponentModel,
        public subComponentId: string,
        public subComponent: ComponentModel,
        public pdo: PDOModel,
        public iri: PDOModel,
        public resultChainCode: string,
        public result: string,
        public successIndicatorCode: string,
        public baselineValue: string,
        public targetDescription: string,
        public targetTotal: string,
        public cumulativeTarget2020: string,
        public cumulativeTargetDummy2020: boolean,
        public cumulativeTarget2021: string,
        public cumulativeTargetDummy2021: boolean,
        public cumulativeTarget2022: string,
        public cumulativeTargetDummy2022: boolean,
        public cumulativeTarget2023: string,
        public cumulativeTargetDummy2023: boolean,
        public cumulativeTarget2024: string,
        public cumulativeTargetDummy2024: boolean,
        public createdAt: string,
        public updatedAt: string,
        public targetYearNow: string,
        public resultIndicator: string = '',
        public result2020: string,
        public result2021: string,
        public result2022: string,
        public result2023: string,
        public result2024: string,
        public resultStatus2020: number,
        public resultStatus2021: number,
        public resultStatus2022: number,
        public resultStatus2023: number,
        public resultStatus2024: number,
        public allowEdit2020: boolean,
        public allowEdit2021: boolean,
        public allowEdit2022: boolean,
        public allowEdit2023: boolean,
        public allowEdit2024: boolean,
        public status: number,
        public convertUpdated: Date,
        public updatedByUser: UsersModels,
        public awpReportSuccessIndicators: string,
        public rekaptulationActivity: RekaptulationActivity[],
        public resultYear: string,
        public questions: Question[],
        public code: string,
        public successIndicator: SuccessIndicator[],
        public parrentId: string,
        public status2020: number,
        public status2021: number,
        public status2022: number,
        public status2023: number,
        public status2024: number,
        public resultStatusSelected: number
    ) {
    }
}

export class RekaptulationActivity {
    constructor(
        public id: string,
        public year: string,
        public successIndicatorId: string,
        public contribution: string,
        public componentCode: string,
        public name: string,
        public createdAt: string,
        public updatedAt: string,
        public active: boolean
    ) {
    }
}

export class Question {
    constructor(
        public id: string,
        public monevId: string,
        public monevQuestionId: string,
        public monevQuestion: MonevQuestion,
        public number: number,
        public yesOrNoAnswer: boolean,
        public year: number,
        public answer: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class MonevQuestion {
    constructor(
        public id: string,
        public question: string,
        public yesOrNo: boolean,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class Answer {
    constructor(
        public questionId: string,
        public number: number,
        public yesOrNo: boolean,
        public questionName: string,
        public yesOrNoAnswer: boolean,
        public answer: string,
    ) {
    }
}

export class ChildMonev {
    constructor(
        public id: string,
        public resultChainCode: string,
        public result: string,
        public resultStatus2020: number,
        public resultStatus2021: number,
        public resultStatus2022: number,
        public resultStatus2023: number,
        public resultStatus2024: number,
        public resultStatusSelected: number
    ) {
    }
}

export class AllMonev {
    constructor(
        public id: string,
        public resultChainCode: string,
        public result: string,
        public styleClass: string,
        public label: string,
        public children: AllMonev[],
        public expanded= true,
        public resultStatusSelected: number,
        public resultStatus2020: number,
        public resultStatus2021: number,
        public resultStatus2022: number,
        public resultStatus2023: number,
        public resultStatus2024: number,
        public type: string,
        public statusSelected: string
    ) {
    }
}


export class AllMonevMap {
    constructor(
        public label: string,
        public expanded: boolean,
        children: AllMonevMap[]
    ) {
    }
}

export class SuccessIndicator {
    constructor(
        public id: string,
        public code: string,
        public name: string,
        public pdo: PDOModel,
        public iri: PDOModel,
        public baselineValue: string,
        public targetDescription: string,
        public targetTotal: string,
        public monevResultChainId: string,
        public monevResultChain: MonevResultChain,
        public successIndicatorId: string,
        public successIndicator: SuccessIndicatorData,
        public result2020: string,
        public result2021: string,
        public result2022: string,
        public result2023: string,
        public result2024: string,
        public cumulativeTarget2020: string,
        public cumulativeTargetDummy2020: boolean,
        public cumulativeTarget2021: string,
        public cumulativeTargetDummy2021: boolean,
        public cumulativeTarget2022: string,
        public cumulativeTargetDummy2022: boolean,
        public cumulativeTarget2023: string,
        public cumulativeTargetDummy2023: boolean,
        public cumulativeTarget2024: string,
        public cumulativeTargetDummy2024: boolean,
        public notApplicable2020: boolean,
        public notApplicable2021: boolean,
        public notApplicable2022: boolean,
        public notApplicable2023: boolean,
        public notApplicable2024: boolean,
        public createdAt: string,
        public updatedAt: string,
        public rekaptulationActivity: RekaptulationActivity[],
        public targetYearNow: string,
        public resultIndicator: string = '',
        public filesUpload: Array<File> = [],
        public filesUploadId : any[] = [],
        public deletedFilesUploadExist: any[] = [],
        public filesExist: any[] = [],
        public files: FileSuccessIndicator[],
        public finalTarget: boolean,
        public notApplicableAllYear: boolean
    ) {
    }
}

export class MonevResultChain {
    constructor(
        public id: string,
        public year: number,
        public monevTypeId: string
    ) {
    }
}

export class SuccessIndicatorData {
    constructor(
        public id: string,
        public code: string,
        public name: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }

}
