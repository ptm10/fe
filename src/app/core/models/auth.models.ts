export class User {
    id: number;
    username: string;
    password: string;
    firstName?: string;
    lastName?: string;
    token?: string;
    email: string;
}

export class ForgotPassword {
    constructor(
        public requestId: string,
        public expired: string
    ) {
    }
}
