import {UsersModels} from "./users.models";
import {RolesModels} from "./roles.models";
import {ComponentModel} from "./componentModel";
import {Position} from "./RoleUsers";

export class resourcesModel {
    constructor(
        public id: string,
        public unit: string,
        public position: Position,
        public consultantType: string,
        public userId: string,
        public user: UsersModels,
        public roles: RolesModels[],
        public supervisiorId: string,
        public createdAt: string,
        public updatedAt: string,
        public filePhoto: any,
        public phoneNumber: string,
        public component: ComponentModel
    ) {}
}


