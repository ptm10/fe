import {resourcesModel} from "./resources.model";

export class PermissionModel {
    constructor(
        public id: string,
        public resourcesId: string,
        public resources: resourcesModel,
        public startDate: string,
        public endDate: string,
        public updatedBy: string,
        public approveStatus: string,
        public updatedByUser: string,
        public leaveRequestTypeId: string,
        public leaveRequestType: LeaveRequestType,
        public description: string,
        public address: string,
        public rejectedMessage: string,
        public createdAt: string,
        public updatedAt: string,
        public convertStartDate: Date,
        public countDays: any,
        public daysLength: number
    ) {
    }
}

export class LeaveRequestType {
    constructor(
        public id: string,
        public name: string,
        public maxDayLength: number,
        public description: string,
        public special: boolean,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class CheckDate {
    constructor(
        public worksDaysCount: number,
        public holidaysCount: number,
        public totalLeaveDays: number
    ) {
    }
}

export class ManagementHolidays {
    constructor(
        public holidayDate: string,
        public convertDate: any
    ) {
    }
}
