import {UsersModels} from "./users.models";

export class ResourceTaskModel {
    constructor(
        public id: string,
        public createdBy: string,
        public createdByUser: UsersModels,
        public taskType: number,
        public taskId: string,
        public description: string,
        public done: string,
        public updatedBy: string,
        public createdAt: string,
        public updatedAt: string,
        public taskDone: any[] = [],
        public status: number,
        public specialChar: boolean,
        public desc: string
    ) {
    }
}
