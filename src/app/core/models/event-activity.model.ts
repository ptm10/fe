import {Event, Kegiatan, MangementType, Pok, Province} from "./awp.model";
import {resourcesModel} from "./resources.model";
import {FileModel} from "./file.model";
import {UsersModels} from "./users.models";

export class EventActivityModel {
    constructor(
        public id: string,
        public awpImplementationId: string,
        public awpImplementation: Kegiatan,
        public name: string,
        public startDate: string,
        public endDate: string,
        public eventOtherInformation: string,
        public eventTypeId: string,
        public eventType: Event,
        public background: string,
        public description: string,
        public purpose: string,
        public eventOutput: string,
        public participantMale: number,
        public participantFemale: number,
        public participantCount: number,
        public participantOtherInformation: string,
        public contactPerson: resourcesModel,
        public contactPersonResources: resourcesModel,
        public provinceId: string,
        public province: Province,
        public regency: Province,
        public location: string,
        public budgetAvailable: boolean,
        public staff: Staff[],
        public otherStaff: OtherStaff[],
        public status: number,
        public rabFile: FileModel,
        public agendaFile: FileModel,
        public agendaFileId: string,
        public revisionMessage: string,
        public rejectedMessage: string,
        public createdByUser: resourcesModel,
        public convertStartDate: any,
        public convertEndDate: any,
        public convertPurpose: any,
        public budgetPok: any,
        public budgetAwp: any,
        public creatorAllowedEdit: boolean,
        public coordinatorAllowedApprove: boolean,
        public pmuTreasurerAllowedApprove: boolean,
        public pmuHeadAllowedApprove: boolean,
        public creatorAllowCreateReport: boolean,
        public pokEvent: boolean,
        public approvalNotes: ApprovalNotes[],
        public backgroundColorCalender: string,
        public staffOrAssistantAllowCreateProgressReport: boolean,
        public staffOrAssistantAllowCreateReport: boolean,
        public activityMode: MangementType,
        public nameOtherInformation: string,
        public locationOtherInformation: string,
        public participantTarget: string,
        public pok: Pok[],
        public rab: [],
        public eventId: string,
        public questionnaireLink: string,
        public repots : Reports[],
        public countUploadedFormFiles: boolean,
        public allowUploadQuestionnaireForm: boolean,
    ) {
    }
}

export class Reports {
    constructor(
        public id: string
    ) {
    }
}

export class Staff {
    constructor(
        public id: string,
        public eventId: string,
        public resourcesId: string,
        public resources: resourcesModel,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class OtherStaff {
    constructor(
        public id: string,
        public eventId: string,
        public name: string,
        public position: string,
        public institution: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class ApprovalNotes {
    constructor(
        public id: string,
        public eventId: string,
        public userId: string,
        public user: UsersModels,
        public createdFrom: number,
        public note: string,
        public approvalStatus: number,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}
