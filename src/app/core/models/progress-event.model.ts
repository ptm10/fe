import {Event, EventDocumentType, Kegiatan, MangementType, Province} from "./awp.model";
import {resourcesModel} from "./resources.model";
import {FileModel} from "./file.model";
import {ApprovalNotes, EventActivityModel, OtherStaff, Staff} from "./event-activity.model";
import {UsersModels} from "./users.models";

export class ProgressEventModel {
    constructor(
        public id: string,
        public eventId: string,
        public event: EventActivityModel,
        public eventOutput: string,
        public participantMale: number,
        public participantFemale: number,
        public participantCount: number,
        public participantOtherInformation: string,
        public eventOtherInformation: string,
        public startDate: string,
        public endDate: string,
        public provinceId: string,
        public province: Province,
        public regency: Province,
        public location: string,
        public attachmentFile: FileModel,
        public staff: Staff[],
        public otherStaff: OtherStaff[],
        public documents: Documents[],
        public status: number,
        public createdByUser: UsersModels,
        public createdAt: string,
        public updatedAt: string,
        public convertStartDate: any,
        public convertEndDate: any,
        public rraFile: FileModel,
        public approvalNotes: ApprovalNotes[],
        public pokEvent: boolean,
        public budgetPok: number,
        public budgetAvailable: boolean,
        public creatorAllowedEdit: boolean,
        public consultanAllowedApprove: boolean,
        public summary: string,
        public convertCreatedAt: any,
        public activityMode: MangementType,
        public locationOtherInformation: string
    ) {
    }
}


export class Documents {
    constructor(
        public id: string,
        public eventProgressReportId: string,
        public title: string,
        public date: string,
        public place: string,
        public description: string,
        public fileId: string,
        public file: FileModel,
        public eventDocumentType: EventDocumentType,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}
