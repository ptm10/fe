import {resourcesModel} from "./resources.model";
import {PDOModel} from "./PDO.model";
import {FileModel} from "./file.model";
import {UsersModels} from "./users.models";
import {ChatModel} from "./chat.model";

export class DetailTaskModel {
    constructor(
        public id: string,
        public title: string,
        public description: string,
        public createdBy: string,
        public createdFor: string,
        public createdByUser: UsersModels,
        public createdForUser: UsersModels,
        public createdByResources: resourcesModel,
        public createdForResources: resourcesModel,
        public pdoId: string,
        public pdo: PDOModel,
        public iriId: string,
        public iri: PDOModel,
        public deadlineDate: string,
        public status: number,
        public doneByCreator: boolean,
        public doneByReceiver: boolean,
        public rejectedMessage: string,
        public files: FilesDetail[],
        public comments: ChatModel[],
        public tags: Tags[],
        public createdAt: string,
        public updatedAt: string,
        public filePhoto: any,
        public filePhotoCreatedTask: any,
        public convertDate: any,
        public convertUpdatedAt: any,
        public fileEdit: FilesDetail[],
        public taskId: string
    ) {
    }
}

export class Tags {
    constructor(
        public id: string,
        public resourcesTaskId: string,
        public taskResourcesTagId: string,
        public taskResourcesTag: PDOModel,
    ) {
    }
}

export class FilesDetail {
    constructor(
        public id: string,
        public resourcesTaskId: string,
        public filesId: string,
        public files: FileModel,
        public uploadedFrom: number

    ) {
    }
}
