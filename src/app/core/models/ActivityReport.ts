import {Kegiatan, MangementType, MonevResult, Pok, Province, Regency} from "./awp.model";
import {UsersModels} from "./users.models";
import {ApprovalNotes} from "./event-activity.model";
import {FileModel} from "./file.model";

export class ActivityReport {
    constructor(
        public id: string,
        public awpImplementationId: string,
        public awpImplementation: Kegiatan,
        public startDate: string,
        public endDate: string,
        public eventOtherInformation: string,
        public eventVolume: number,
        public summary: string,
        public conclusionsAndRecommendations: string,
        public locationOtherInformation: string,
        public activityMode: MangementType,
        public participantTarget: string,
        public nameOtherInformation: string,
        public status: number,
        public pok: Pok[],
        public provincesRegencies: ActivityPlaces[],
        public createdBy: string,
        public createdByUser: UsersModels,
        public updatedBy: string,
        public updatedByUser: UsersModels,
        public participantOtherInformation: string,
        public purpose: string,
        public createdAt: string,
        public updatedAt: string,
        public convertStartDate: any,
        public convertEndDate: any,
        public convertCreatedAt: any,
        public convertUpdatedAt: any,
        public creatorAllowedEdit: boolean,
        public consultanAllowedApprove: boolean,
        public coordinatorAllowedApprove: boolean,
        public pmuTreasurerAllowedApprove: boolean,
        public pmuHeadAllowedApprove: boolean,
        public coordinatorConsultanAllowEdit: boolean,
        public approvalNotes: ApprovalNotes[],
        public allowApprove: boolean,
        public monev: MonevResult[],
        public bastFile: FileModel
    ) {
    }
}

export class ActivityPlaces {
    constructor(
        public id: string,
        public awpId: string,
        public province: Province,
        public regencies: Regency[],
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

