export class SummaryResult {
    constructor(
        public eventName: string,
        public totalRespondent: string,
        public respondents: Respondents[],
        public questionCategories: QuestionCategory[],
    ) {
    }
}

export class Respondents {
    constructor(
        public username: string,
        public gender: string,
        public institution: string,
        public email: string,
        public phoneNumber: any,
    ) {
    }
}

export class QuestionCategory {
    constructor(
        public category: number,
        public questionAnswers: QuestionAnswers[],
    ) {
    }
}

export class QuestionAnswers {
    constructor(
        public question: FormQuestionEventResult,
        public respondentCount: number,
        public answers = [],
        public answerYesCount: number,
        public answerNoCount: number,
        public answerScale1Count: number,
        public answerScale2Count: number,
        public answerScale3Count: number,
        public answerScale4Count: number,
        public answerScale5Count: number,
        public answerScaleScore: number,
    ) {
    }
}


export class FormQuestionEventResult {
    constructor(
        public id: string,
        public question: string,
        public eventFormId: string,
        public createdFor: number,
        public questionCategory: number,
        public questionType: number,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}


