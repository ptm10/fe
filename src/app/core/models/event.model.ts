import {resourcesModel} from "./resources.model";
import {EventListActivityModel, Transportation} from "./event-list-activity.model";
import {Event} from "./awp.model";

export class EventModel {
    constructor(
        public id: string,
        public resourcesId: string,
        public resources: resourcesModel,
        public eventId: string,
        public event: EventListActivityModel,
        public startDate: string,
        public endDate: string,
        public daysLength: number,
        public description: string,
        public address: string,
        public transportationId: string,
        public transportation: Transportation,
        public origin: string,
        public destination: string,
        public transportationStartTime: string,
        public transportationEndTime: string,
        public transportationCode: string,
        public approveStatus: number,
        public details: Detail[],
        public updatedBy: string,
        public updatedByUser: string,
        public rejectedMessage: string,
        public createdAt: string,
        public updatedAt: string,
        public convertCreatedAt: any,
        public convertStartActivity: Date,
        public convertEndActivity: Date,
        public convertUpdatedAt: any,
    ) {
    }
}

export class Detail {
    constructor(
        public id: string,
        public outOfTownEventId: string,
        public resourcesId: string,
        public resources: resourcesModel,
    ) {
    }
}


export class QuestionEvent {
    constructor(
        public id: string,
        public question: string,
        public eventFormId: string,
        public createdFor: number,
        public questionCategory: number,
        public questionType: number,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class FormQuestionEvent {
    constructor(
        public id: string,
        public eventTypeId: string,
        public eventType: Event,
        public questions: QuestionEvent[],
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}


export class QuestionEventAnswer {
    constructor(
        public id: string,
        public question: string,
        public eventFormId: string,
        public createdFor: number,
        public questionCategory: number,
        public questionType: number,
        public explanationAnswer: string,
        public ratingAnswer: number = 0,
        public yesOrNoAnswer: boolean,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class EventFormEmailParticipant {
    constructor(
        public fileName: string,
        public successCount: number,
        public failed: string[]
    ) {
    }
}
