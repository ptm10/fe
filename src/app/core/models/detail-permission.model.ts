import {ResourceTaskModel} from "./resource-task.model";
import {resourcesModel} from "./resources.model";
import {LeaveRequestType} from "./permission.model";

export class DetailPermissionModel {
    constructor(
        public id: string,
        public resourcesId: string,
        public resources: resourcesModel,
        public startDate: string,
        public endDate: string,
        public daysLength: number,
        public worksDays: number,
        public holidays: number,
        public updatedBy: string,
        public approveStatus: number,
        public updatedByUser: string,
        public leaveRequestType: LeaveRequestType,
        public description: string,
        public address: string,
        public rejectedMessage: string,
        public createdAt: string,
        public updatedAt: string,
        public convertStartDate: Date,
        public convertEndDate: Date,
        public convertUpdatedAt: Date,
        public convertCreatedAt: Date,
        public filePhoto: any,
        public countDays: any
    ) {}
}
