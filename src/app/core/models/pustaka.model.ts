import {FileModel} from "./file.model";
import {ComponentModel} from "./componentModel";

export class PustakaModel {
    constructor(
        public id: string,
        public name: string,
        public filesId: string,
        public files: FileModel,
        public version: string,
        public description: string,
        public createdBy: string,
        public createdByUsers: string,
        public createdAt: string,
        public updatedAt: string,
        public documentsClassification: DocumentsClassification,
        public component: ComponentModel,
        public tags: Tags[],
        public convertCreatedAt: any
    ) {}
}


export class DocumentsClassification {
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public createdAt: string,
        public updatedAt: string
    ) {
    }
}

export class Tags {
    constructor(
        public id: string,
        public generalDocumentsId: string,
        public generalDocumentsTagId: string,
        public generalDocumentsTag: DocumentsClassification,
        public createdAt: string,
        public updatedAt: string
    ) {
    }
}

export class GeneralDocumentsTag {
    constructor(
        public id: string,
        public name: string,
        public createdAt: string,
        public updatedAt: string
    ) {
    }
}
