import {RolesModels} from "./roles.models";
import {FileModel} from "./file.model";
import {ComponentModel} from "./componentModel";
import {resourcesModel} from "./resources.model";
import {ListUnit} from "./RoleUsers";
import {Province} from "./awp.model";

export class UsersModels {
  constructor(
    public id: string,
    public email: string,
    public firstName: string,
    public lastName: string,
    public roles: RolesModels[],
    public createdAt: string,
    public updatedAt: string,
    public profilePicture: FileModel,
    public lastLogin: string,
    public active: boolean,
    public editEmailExist: string[],
    public convertDateLastLogin: Date,
    public component: ComponentModel,
    public lspExpiredDate: string,
    public convertExpiredStartDate: Date,
    public convertExpiredEndDate: Date,
    public resources: resourcesModel,
    public lsp: Lsp,
    public unit: ListUnit,
    public province: Province
  ) {}
}

export class ChangeEmailModels {
  constructor(
      public id: string,
      public userId: string,
      public user: UsersModels,
      public email: string,
      public active: boolean,
  ) {
  }
}

export class Lsp {
  constructor(
      public id: string,
      public startDate: string,
      public endDate: string,
      public createdAt: string,
      public updatedAt: string,

  ) {}
}
