export class CalenderModel {
    constructor(
        public id: string,
        public resourcesId: string,
        public title: string,
        public description: string,
        public startDate: string,
        public endDate: string,
        public createdAt: string,
        public updatedAt: string,
        public taskType: number,
        public listDescription: any,
        public taskId: string
    ) {
    }
}

export class AddCalender {
    constructor(
        public id: string = null,
        public title: string = null,
        public start: Date = new Date(),
        public end: Date = new Date(),
        public className: string = null,
    ) {
    }
}
