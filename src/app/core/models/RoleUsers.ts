export class RoleUsers {
    constructor(
        public id: string,
        public name: string,
        public code: string,
        public createdAt: string,
        public updatedAt: string,
        public checked: boolean = false,
        public status: number,
        public supervisiorId: string
    ) {
    }
}

export class ListUnit {
    constructor(
        public id: string,
        public name: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class Position {
    constructor(
        public id: string,
        public unitId: string,
        public unit: ListUnit,
        public name: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}
