import {RoleModels} from "./role.models";

export class RolesModels {
  constructor(
    public id: string,
    public userId: string,
    public roleId: string,
    public role: RoleModels,
    public createdAt: string,
    public updatedAt: string,
    public checked: boolean = false
  ) {}
}
