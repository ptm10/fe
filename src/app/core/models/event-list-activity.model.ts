export class EventListActivityModel {
    constructor(
        public id: string,
        public awpId: string,
        public awp: Awp,
        public name: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class Awp {
    constructor(
        public id: string,
        public name: string,
        public componentId: string,
        public component: Component,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class Component {
    constructor(
        public id: string,
        public code: string,
        public description: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class Transportation {
    constructor(
        public id: string,
        public name: string,
        public description: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}
