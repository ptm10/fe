import {ComponentModel} from "./componentModel";
import {SubComponentModel} from "./subComponent.model";
import {FileModel} from "./file.model";
import {PdoIriModel, PDOModel} from "./PDO.model";
import {UsersModels} from "./users.models";
import {resourcesModel} from "./resources.model";
import {QualityAtEntryModel} from "./qualityAtEntry.model";
import {ApprovalNotes} from "./event-activity.model";
import {Monev2, SuccessIndicator} from "./monev";

export class AwpModel {
    constructor(
        public id: string,
        public name: string,
        public pok: string,
        public budgetAwp: number,
        public budgetPok: number,
        public description: string,
        public component: ComponentModel,
        public subComponent: ComponentModel,
        public subSubComponent: ComponentModel,
        public purpose: string,
        public eventManagementType: MangementType,
        public componentCode: ComponentModel,
        public startDate: string,
        public endDate: string,
        public eventOtherInformation: string,
        public provincesRegencies: Places[],
        public eventVolume: number,
        public eventType: Event,
        public nsInstitution: string,
        public nsCount: number,
        public nsOtherInformation: string,
        public participantCount: number,
        public participantOtherInformation: string,
        public eventOutput: string,
        public pdo: PdoAwp[],
        public iri: IriAwp[],
        public volume: number,
        public regency: string,
        public province: string,
        public nsTotal: number,
        public institution: string,
        public nsMale: number,
        public nsFemale: number,
        public tpTotal: number,
        public tpMale: number,
        public tpFemale: number,
        public participant: number,
        public notes: string,
        public participantMale: number,
        public participantFemale: number,
        public executor: string,
        public result: string,
        public year: number,
        public activities: FileModel,
        public createdAt: string,
        public updatedAt: string,
        public convertStartDate: Date,
        public convertEndDate: Date,
        public convertStartEventDate: Date,
        public convertEndEventDate: Date,
        public code: string,
        public status: number,
        public nameOtherInformation: string,
        public locationOtherInformation: string,
        public subSubComponentName: string,
        public activityMode: MangementType,
        public mitigationRisk: string,
        public rabFile: FileModel,
        public participantTarget: string,
        public descriptionRisk: string,
        public checked: boolean,
        public risk: string,
        public potentialRisk: number,
        public impactRisk: number,
        public symptomsRisk: string,
    ) {
    }
}

export class Kegiatan {
    constructor(
        public id: string,
        public name: string,
        public pok: Pok[],
        public awp: AwpModel,
        public componentCode: ComponentModel,
        public component: ComponentModel,
        public subComponent: ComponentModel,
        public subSubComponent: ComponentModel,
        public code: string,
        public pokEvent: boolean,
        public budgetPok: number,
        public budgetAwp: number,
        public projectOfficerResources: resourcesModel,
        public lspContractNumber: string,
        public lspContractData: string,
        public lspPic: string,
        public eventManagementType: MangementType,
        public purpose: string,
        public description: string,
        public startDate: string,
        public endDate: string,
        public eventOtherInformation: string,
        public provincesRegencies: Places[],
        public eventVolume: number,
        public eventType: Event,
        public nsInstitution: string,
        public nsMale: number,
        public nsFemale: number,
        public nsCount: number,
        public nsOtherInformation: string,
        public participantCount: number,
        public participantMale: number,
        public participantFemale: number,
        public participantOtherInformation: string,
        public descriptionRisk: string,
        public assumption: string,
        public potentialRisk: number,
        public mitigationRisk: any,
        public riskSymptoms: any,
        public eventOutput: string,
        public rabFileId: any,
        public rabFile: FileModel,
        public pdo: PdoAwp[],
        public iri: IriAwp[],
        public status: number,
        public riStatus: number,
        public revisionMessage: string,
        public rejectedMessage: string,
        public createdAt: string,
        public updatedAt: string,
        public convertStartDate: Date,
        public convertEndDate: Date,
        public convertStartEventDate: Date,
        public convertEndEventDate: Date,
        public createdByUser: UsersModels,
        public updatedByUser: UsersModels,
        public volume: number,
        public regency: string,
        public province: string,
        public nsTotal: number,
        public institution: string,
        public tpTotal: number,
        public tpMale: number,
        public tpFemale: number,
        public participant: number,
        public notes: string,
        public executor: string,
        public result: string,
        public year: number,
        public activities: FileModel,
        public symptomsRisk: string,
        public impactRisk: number,
        public creatorAllowedEdit: boolean,
        public coordinatorAllowedApprove: boolean,
        public pmuAllowedApprove: boolean,
        public awpImplementationQualityAtEntryAnswers: AwpImplementationQualityAtEntryAnswers[],
        public qualitAtEntryNote: string,
        public approvalNotes: ApprovalNotes[],
        public lspPicUser: UsersModels,
        public lspAllowCreateEventReport: boolean,
        public nameOtherInformation: string,
        public locationOtherInformation: string,
        public activityMode: MangementType,
        public participantTarget: string,
        public creatorAllowedCreateReport: boolean,
        public lspAllowCreateReport: boolean,
        public risk: string,
        public monev: MonevResult[],
        public coordinatorConsultanAllowedCtreateConceptNote: boolean,
        public backgroundColorCalender: string,
        public allowedDownloadReport: boolean
    ) {
    }
}

export class MonevResult {
    constructor(
        public id: string,
        public allowDelete: boolean,
        public awpImplementationId: string,
        public awpImplementationReportId: string,
        public resultChainId: string,
        public resultChain: Monev2,
        public successIndicator: successIndicator[],
        public resultIndicator: string = '',
    ) {
    }
}

export class successIndicator {
    constructor(
        public id: string,
        public allowDelete: boolean,
        public awpImplementationResultChainId: string,
        public awpImplementationReportResultChainId: string,
        public contribution: string,
        public successIndicatorId: string,
        public successIndicator: SuccessIndicator,
        public existing: boolean
    ) {
    }
}

export class Pok {
    constructor(
        public id: string,
        public awpImplementationId: string,
        public pokId: string,
        public pok: PokCode,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class MangementType {
    constructor(
        public id: string,
        public name: string,
        public code: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class PokCode {
    constructor(
        public id: string,
        public title: string,
        public code: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class Province {
    constructor(
        public id: string,
        public name: string,
        public code: string,
        public provinceId: string,
        public createdAt: string,
        public updatedAt: string,
    ) {}
}

export class Event {
    constructor(
        public id: string,
        public name: string,
        public type: number,
        public hoursNumber: string,
        public implementation: string,
        public purpose: string,
        public leader: string,
        public participant: string,
        public executor: string,
        public interactionPattern: string,
        public topic: string,
        public sample: string,
        public output: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class Places {
    constructor(
        public id: string,
        public awpId: string,
        public province: Province,
        public regencies: Regency[],
        public createdAt: string,
        public updatedAt: string,
        ) {
    }
}

export class Regency {
    constructor(
        public id: string,
        public awpId: string,
        public regency: Province,
        public createdAt: string,
        public updatedAt: string,
        ) {
    }
}

export class PdoAwp {
    constructor(
        public id: string,
        public awpId: string,
        public pdo: PDOModel,
    ) {
    }
}

export class IriAwp {
    constructor(
        public id: string,
        public awpId: string,
        public iri: PDOModel,
    ) {
    }
}

export class AwpImplementationQualityAtEntryAnswers {
    constructor(
        public id: string,
        public awpImplementationId: string,
        public qualityAtEntryQuestionId: string,
        public qualityAtEntryQuestion: QualityAtEntryModel,
        public answer: boolean,
        public revision: string,
        public createdAt: string,
        public updatedAt: string,
        ) {
    }
}

export class TotalEventByStatus {
    constructor(
        public done: string,
        public onGoing: string,
        public planning: string,
    ) {
    }
}

export class InformationAwpDoc {
    constructor(
        public id: string,
        public year: string,
        public keteranganKontribusiKomponent: string,
        public keteranganKemajuanProgram: string,
        public keteranganKendala: string,
        public keteranganPembelajaran: string,
        public keteranganRencanaKerja: string,
        public keteranganUsulanKegiatan: string,
        public dipaAwalKomponen1: string,
        public dipaAwalKomponen2: string,
        public dipaAwalKomponen3: string,
        public dipaAwalKomponen4: string,
        public dipaRevisiKomponen1: string,
        public dipaRevisiKomponen2: string,
        public dipaRevisiKomponen3: string,
        public dipaRevisiKomponen4: string,
        public dipaRealisasiKomponen1: string,
        public dipaRealisasiKomponen2: string,
        public dipaRealisasiKomponen3: string,
        public dipaRealisasiKomponen4: string,
        public dipaAwalRupiahMurni: string,
        public dipaRevisiRupiahMurni: string,
        public dipaRealisasiRupiahMurni: string,
        public hambatanKomponen1: string,
        public hambatanKomponen2: string,
        public hambatanKomponen3: string,
        public hambatanKomponen4: string,
        public fokusKomponen1: string,
        public fokusKomponen2: string,
        public fokusKomponen3: string,
        public fokusKomponen4: string,
        public pdo: PdoIriModel[],
        public files: Files[],
        public createdAt: string,
        public updatedAt: string,
        public createdBy: string,
        public createdByUser: UsersModels,
        public updatedBy: string,
        public updatedByUser: UsersModels,
        public otherFiles: Files[],

    ) {
    }
}

export class Files {
    constructor(
        public id: string,
        public awpDataId: string,
        public component: string,
        public key: string,
        public file: FileModel,
    ) {
    }
}

export class EventDocumentType {
    constructor(
        public id: string,
        public name: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}
