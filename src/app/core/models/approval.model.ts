import {RoleUsers} from "./RoleUsers";
import {RoleModels} from "./role.models";
import {RolesModels} from "./roles.models";
import {UsersModels} from "./users.models";

export class ApprovalModel {
    constructor(
        public id: string,
        public approveStatus: number,
        public user: UsersModels,
        public detail: PendingUpdate[],
        public createdAt: string,
        public updatedAt: string,
        public convertDateUpdate: Date
    ) {
    }
}


export class PendingUpdate {
    constructor(
        public id: string,
        public updateRoleRequestId: string,
        public roleId: string,
        public role: RoleModels,
        public requestStatus: number,
    ) {
    }
}
