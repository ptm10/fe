import {UsersModels} from "./users.models";

export class NotificationModel {
    constructor(
        public id: string,
        public userId: string,
        public taskType: number,
        public taskId: string,
        public message: string,
        public readed: boolean,
        public createdBy: string,
        public createdByUser: UsersModels,
        public createdAt: string,
        public updatedAt: string,
        public convertDateCreated: any
    ) {}
}
