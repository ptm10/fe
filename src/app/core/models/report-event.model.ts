import {ApprovalNotes, EventActivityModel, OtherStaff, Staff} from "./event-activity.model";
import {MangementType, Pok, Province} from "./awp.model";
import {FileModel} from "./file.model";
import {resourcesModel} from "./resources.model";
import {Documents} from "./progress-event.model";
import {UsersModels} from "./users.models";

export class ReportEventModel {
    constructor(
        public id: string,
        public eventId: string,
        public topic: string,
        public eventCode: string,
        public event: EventActivityModel,
        public provinceId: string,
        public province: Province,
        public regency: Province,
        public location: string,
        public startDate: string,
        public endDate: string,
        public participantMale: number,
        public participantFemale: number,
        public participantCount: number,
        public participantOtherInformation: string,
        public participantTarget: string,
        public implementationSummary: string,
        public eventEvaluationDescription: string,
        public conclusionsAndRecommendations: string,
        public attachmentFile: FileModel,
        public rraFile: FileModel,
        public status: number,
        public documents: Documents[],
        public evaluationPoints: EvaluationPoints[],
        public approvalNotes: ApprovalNotes[],
        public budgetSource: string,
        public budgetYear: number,
        public makNumber: string,
        public budgetCeiling: number,
        public rraBudget: number,
        public paragraph1: string,
        public paragraph2: string,
        public p3StartDate: string,
        public p3EndDate: string,
        public p3Involve: string,
        public paragraph4: string,
        public paragraph5: string,
        public technicalImplementer: string,
        public technicalImplementerResources: resourcesModel,
        public totalDuration: string,
        public schemaEvent: string,
        public creatorAllowedEdit: boolean,
        public consultanAllowedApprove: boolean,
        public coordinatorAllowedApprove: boolean,
        public pmuTreasurerAllowedApprove: boolean,
        public pmuHeadAllowedApprove: boolean,
        public eventOutput: string,
        public eventOtherInformation: string,
        public staff: Staff[],
        public otherStaff: OtherStaff[],
        public createdByUser: UsersModels,
        public createdAt: string,
        public updatedAt: string,
        public convertStartDate: any,
        public convertEndDate: any,
        public pokEvent: boolean,
        public budgetPok: number,
        public budgetAvailable: boolean,
        public summary: string,
        public p3StartDateConvertDate: any,
        public p3EndDateConvertDate: any,
        public p3Location: string,
        public convertCreatedAt: any,
        public activityMode: MangementType,
        public nameOtherInformation: string,
        public locationOtherInformation: string,
        public pok: Pok[],
        public participantTargets: ParticipantTargets[]
    ) {
    }
}

export class EvaluationPoints {
    constructor(
        public id: string,
        public value: number,
        public note: string,
        public evaluationPoint: EvaluationPoint,
        public createdAt: string,
        public updatedAt: string
    ) {
    }
}

export class EvaluationPoint {
    constructor(
        public id: string,
        public name: string,
        public createdAt: string,
        public updatedAt: string
    ) {
    }
}

export class ParticipantTargets {
    constructor(
        public id: string,
        public eventReportId: string,
        public participantTargetPositionId: string,
        public participanTargetPosition: EvaluationPoint,
        public value: string,
        public createdAt: string,
        public updatedAt: string
    ) {
    }
}
