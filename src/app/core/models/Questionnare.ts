import {QuestionEventAnswer} from "./event.model";

export class Questionnare {
    constructor(
        public id: string,
        public status: number,
        public createdFor: number,
        public eventType: string,
        public eventName: string,
        public question: QuestionEventAnswer[]
    ) {
    }
}

export class QuestionnareEvent {
    constructor(
        public id: string,
        public eventQuestionAnswerId: string,
        public eventQuestionId: string,
        public eventQuestion: EventQuestion,
        public explanationAnswer: string,
        public scaleAsnwer: number = 0,
        public yesOrNoAnswer: boolean,
        public openComment: boolean = false,
        public comment: string,
        public createdAt: string,
        public updatedAt: string,
    ) {
    }
}

export class EventQuestion {
    constructor(
        public id: string,
        public question: string,
        public eventFormId: string,
        public createdFor: number,
        public questionCategory: number,
        public questionType: number,
        public explanationAnswer: string,
        public scaleAsnwer: number = 0,
        public comment: number = 0,
        public yesOrNoAnswer: boolean,
        public openComment: boolean = false,
    ) {
    }
}
