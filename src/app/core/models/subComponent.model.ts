export class SubComponentModel {
    constructor(
        public id: string,
        public code: string,
        public description: string
    ) {
    }
}
