import {UsersModels} from "./users.models";

export class LoginModels {
  constructor(
    public accessToken: string,
    public tokenType: string,
    public expired: number,
    public user: UsersModels,
  ) {}
}


export class RequestLogin {
  constructor(
    public email: string,
    public password: string
  ) {}
}
