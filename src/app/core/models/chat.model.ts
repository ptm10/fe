export class ChatModel {
    constructor(
        public id: string,
        public resourcesTaskId: string,
        public message: string,
        public createdFrom: number,
        public createdAt: string,
        public updatedAt: string,
    ) {}
}
