export class DateHelper {
    minusDate(date: Date, minusMonth: number) {
        let data = date
        let expectMonth = date.getMonth() - minusMonth
        data.setMonth(data.getMonth() - minusMonth)
        if (data.getMonth() !== expectMonth) {
            data.setMonth(data.getMonth() - minusMonth)
        }
        return date
    }
}
