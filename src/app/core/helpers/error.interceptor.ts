import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import Swal from "sweetalert2";

import { AuthenticationService } from '../services/auth.service';
import {AuthfakeauthenticationService} from "../services/authfake.service";
import {Router} from "@angular/router";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService, private authFackservice: AuthfakeauthenticationService, private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if (err.status === 401) {
                // this.router.navigate(['/admin/login']);
                // Swal.fire({
                //     position: 'center',
                //     icon: 'error',
                //     title: 'Sesi anda sudah habis',
                // }).then(() => {
                //     this.removeToken()
                //     this.router.navigate(['/admin/login']);
                // })
            }
            const error = err || err.statusText;
            return throwError(error);
        }));
    }

    removeToken() {
        this.authFackservice.logout()

    }
}
