import Swal from "sweetalert2";
import {AuthfakeauthenticationService} from "../services/authfake.service";
import {Router} from "@angular/router";
import {Injectable} from "@angular/core";

@Injectable()
export class SwalAlert {
    constructor(public authFackservice: AuthfakeauthenticationService, public router: Router) {}
    showAlertSwal(error: any) {
        if (error.status === 401) {
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Sesi login anda sudah berakhir, mohon untuk login kembali',
            }).then(() => {
                this.removeToken()
                this.router.navigate(['/admin/login']);
            })
        } else if (error.status === 502) {
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'Terjadi Kesalahan Pada Sistem, Silahkan coba beberapa saat lagi',
            });
        } else {
            if (error?.error?.data) {
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: error?.error?.data[0]?.message,
                });
            } else {
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: error?.error?.message,
                });
            }
        }
    }
    removeToken() {
        this.authFackservice.logout()
    }
}

