import { Injectable } from '@angular/core';
import {
    Router,
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    ActivatedRoute,
    NavigationEnd, NavigationStart
} from '@angular/router';

import { AuthenticationService } from '../services/auth.service';
import { AuthfakeauthenticationService } from '../services/authfake.service';

import { environment } from '../../../environments/environment';
import {NavigationEvent} from "@ng-bootstrap/ng-bootstrap/datepicker/datepicker-view-model";

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    currentUrl: string;
    event$;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private authFackservice: AuthfakeauthenticationService,
        private activatedRoute: ActivatedRoute
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd ) {
                this.currentUrl = event.url;
                if (this.authFackservice.hasAuthority('LSP') && this.checkValidationMenuLsp(this.currentUrl)) {
                    this.router.navigate(['404_not_found'])
                    return false
                }
            }
        });

        const currentUser = localStorage.getItem('accessToken')
        if (currentUser !== null) {
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/admin/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }

    checkValidationMenuLsp(path) {
        let dataPathUrl = ['awp', 'project-component', 'detail-event', 'file', 'monev', 'administrasi', 'configuration', 'activity/event', 'dashboards']
        let find = dataPathUrl.filter(x => path.includes(x))
        return find.length > 0;
    }
}
