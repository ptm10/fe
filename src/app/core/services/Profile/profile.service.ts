import {EventEmitter, Injectable, Output} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {Observable} from "rxjs/Rx";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private prefixUrl = environment.serverAuth
  private prefixUrlUpload = environment.serverFile
  private prefixResources = environment.serverResource
  private prefixAuth= environment.serverAuth
  private prefixUrlAwp = environment.serverAwp

  public updateNotification = new EventEmitter();

  constructor(private http: HttpClient) {}

  paramsDtAwp = {
    enablePage: false,
    sort: [
      {
        field: "code",
        direction: "asc"
      }
    ]
  }
  paramsDtUnit = {
    enablePage: false,
    sort: [
      {
        field: "name",
        direction: "desc"
      }
    ]
  }

  paramsDt = {
    enablePage: false,
    "page": 0,
    "size": 10,
    "paramLike": [],
    "paramIs": [],
    "paramIn": [],
    "paramNotIn": [],
    "sort": [
      {
        "field": "createdAt",
        "direction": "desc"
      }
    ]
  }

  paramsDtRoles = {
    enablePage: false,
    sort: [
      {
        field: "code",
        direction: "asc"
      }
    ]
  }

  getToken() {
    return 'Bearer ' + localStorage.getItem('accessToken');
  }

  detailProfile(): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'auth/detail', CommonCons.getHttpOptions());
  }

  upload(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '4');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  changePassword(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'auth/update-password', data, CommonCons.getHttpOptions());
  }

  listRole(): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'auth/roles?enablePage=0&page=0&size=0&sort=asc&sortBy=name', CommonCons.getHttpOptions());
  }

  updateProfile(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'auth/update-profile', data, CommonCons.getHttpOptions());
  }

  pendingApproval(): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'approval/pending-update-role-request', CommonCons.getHttpOptions());
  }

  saveUsers(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'user', data ,CommonCons.getHttpOptions());
  }

  listNotification(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixResources + 'task-notification/datatable', this.paramsDt ,CommonCons.getHttpOptions());
  }

  changeStatusRead(id, data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixResources + 'task-notification/' + id, data, CommonCons.getHttpOptions());
  }

  validateStaf(): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixResources + 'resources/staff', CommonCons.getHttpOptions());
  }

  removeRolesChoosed(): Observable<DataResponse> {
    return this.http.delete<DataResponse>(this.prefixUrl + 'auth/cancel-update-role-request', CommonCons.getHttpOptions());
  }

  validateEmail(): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixAuth + 'auth/change-email-request', CommonCons.getHttpOptions());
  }

  deleteRequestChangeEmail(): Observable<DataResponse> {
    return this.http.delete<DataResponse>(this.prefixAuth + 'auth/cancel-update-email', CommonCons.getHttpOptions());
  }

  listAwp(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlAwp + 'component/datatable', this.paramsDtAwp, CommonCons.getHttpOptions());
  }

  listRoleDt(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'role/datatable', this.paramsDtRoles ,CommonCons.getHttpOptions());
  }

  listSupervisorByComponent(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixResources + 'resources/supervisior-by-component-role', data ,CommonCons.getHttpOptions());
  }

  listSupervisorByRolePcu(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixResources + 'resources/supervisior-pcu', data ,CommonCons.getHttpOptions());
  }

  listUnit(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixAuth + 'unit/datatable', this.paramsDtUnit ,CommonCons.getHttpOptions());
  }

  listPosition(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixAuth + 'position/datatable', data ,CommonCons.getHttpOptions());
  }
  
}
