import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {DataResponse} from "../../models/dataresponse.models";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ChangePasswordService {

  private prefixUrl = environment.serverAuth;

  constructor(private http: HttpClient) {}

  changePassword(dataRequest: any): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'auth/reset-password', dataRequest);
  }

  checkForgotPassword(id: any): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'auth/forgot-password/' + id);
  }
}
