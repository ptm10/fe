import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {BehaviorSubject, EMPTY, Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {environment} from "../../../environments/environment";
import {LoginModels} from "../models/login.models";

@Injectable({ providedIn: 'root' })
export class AuthfakeauthenticationService {
    private prefixUrl = environment.serverAuth;
    private currentUserSubject: BehaviorSubject<LoginModels>;
    public currentUser: Observable<LoginModels>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<LoginModels>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): LoginModels {
        return this.currentUserSubject.value;
    }

    login(login) {
        const rolesAuthority = []
        return this.http.post<any>(this.prefixUrl + 'auth/login', login)
            .pipe(map(user => {
                user.data?.user?.roles.forEach(rolesExist => {
                    rolesAuthority.push(rolesExist?.role?.name)
                })
                localStorage.setItem('accessToken', user.data?.accessToken);
                localStorage.setItem('tokenType', user.data?.tokenType);
                localStorage.setItem('expired', user.data?.expired);
                localStorage.setItem('user_id', user.data?.user?.id);
                localStorage.setItem('fullName', user.data?.user?.firstName);
                localStorage.setItem('lastName', user.data?.user?.lastName);
                localStorage.setItem('picture_id', user.data?.user?.profilePictureId);
                localStorage.setItem('role_name', JSON.stringify(rolesAuthority));
                localStorage.setItem('code_component', user.data?.user?.component?.code.toString());
                localStorage.setItem('unit', user.data?.user?.resources?.position?.unit?.name);
                localStorage.setItem('componentId', user.data?.user?.component?.id);
                this.currentUserSubject.next(user);
              return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('accessToken');
        localStorage.removeItem('tokenType');
        localStorage.removeItem('expired');
        localStorage.removeItem('user_id');
        localStorage.removeItem('fullName');
        localStorage.removeItem('lastName');
        localStorage.removeItem('role_name');
        localStorage.removeItem('picture_id');
        localStorage.removeItem('code_component');
        localStorage.removeItem('unit');
        localStorage.removeItem('componentId');
        this.currentUserSubject.next(null);
    }

    public hasAuthority(authorityName): boolean {
        const authorities = localStorage.getItem('role_name');
        if (authorities) {
            return authorities.includes(authorityName);
        } else {
            return false;
        }
    }

    public hasAuthorityComponent(authorityComponent): boolean {
        let authorities = localStorage.getItem('code_component');
        authorities = authorities.charAt(0)
        if (authorities) {
            return authorities.includes(authorityComponent);
        } else {
            return false;
        }
    }
}
