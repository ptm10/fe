import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Rx";
import {HttpClient} from "@angular/common/http";
import {CommonCons} from "../../../shared/CommonCons";
import {environment} from "../../../../environments/environment";
import {DataResponse} from "../../models/dataresponse.models";

@Injectable({
  providedIn: 'root'
})
export class PustakaService {

  prefixUrl = environment.serverFile
  prefixUrlKnowledge = environment.serverKnowledge
  prefixUrlAwp = environment.serverAwp

  constructor(private http: HttpClient) { }

  exportPdf(id: any) {
    return new Observable<boolean>(observer => {
      this.http.get(this.prefixUrl + '/download?id=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
          (response: any) => {
            const dataType = response.type;
            const binaryData = [];
            binaryData.push(response);
            const downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

            downloadLink.setAttribute('download', 'Pdf Data Laporan Document .pdf');
            document.body.appendChild(downloadLink);
            downloadLink.click();
            observer.next(true);
          }, error => {
            observer.error(false);
          }
      );
    });
  }

    exportPdfAwp(id: any, name: any) {
        return new Observable<boolean>(observer => {
            this.http.get(this.prefixUrlAwp + 'awp/download-pdf?id=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
                (response: any) => {
                    const dataType = response.type;
                    const binaryData = [];
                    binaryData.push(response);
                    const downloadLink = document.createElement('a');
                    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

                    downloadLink.setAttribute('download', name + '.pdf');
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    observer.next(true);
                }, error => {
                    observer.error(false);
                }
            );
        });
    }

    listFile(data): Observable<DataResponse> {
        return this.http.post<DataResponse>(this.prefixUrlKnowledge + 'general-documents-version/datatable', data,  CommonCons.getHttpOptions());
    }


}
