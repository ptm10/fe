import { TestBed } from '@angular/core/testing';

import { PustakaService } from './pustaka.service';

describe('PustakaService', () => {
  let service: PustakaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PustakaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
