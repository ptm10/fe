import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {Observable} from "rxjs/Rx";
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AdministrationService {

  private prefixUrl = environment.serverResource
  private prefixUrlUpload = environment.serverFile
  private prefixUrlAuth = environment.serverAuth
  private prefixUrlAwp = environment.serverAwp

  paramsDt = {
    enablePage: false,
    sort: [
      {
        field: "name",
        direction: "asc"
      }
    ]
  }

  paramsLeaveRequest = {
    enablePage: false,
    sort: [
      {
        field: "user.firstName",
        direction: "asc"
      }
    ]
  }

  paramsHolidayManagement = {
    enablePage: false,
    sort: [
      {
        field: "holidayDate",
        direction: "asc"
      }
    ]
  }

  getToken() {
    return 'Bearer ' + localStorage.getItem('accessToken');
  }

  paramsDtAwp = {
    enablePage: false,
    sort: [
      {
        field: "code",
        direction: "asc"
      }
    ]
  }

  constructor(private http: HttpClient) { }

  detailStaf(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'resources/' + id, CommonCons.getHttpOptions());
  }

  getListCalender(id): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'resources-schedule/datatable', id, CommonCons.getHttpOptions());
  }

  leaveRequestType(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'leave-request-type/datatable', this.paramsDt, CommonCons.getHttpOptions());
  }

  resourcesReplacement(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'resources/datatable', this.paramsLeaveRequest, CommonCons.getHttpOptions());
  }

  submitLeaveRequest(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'leave-request', data, CommonCons.getHttpOptions());
  }

  checkValidateDate(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'leave-request/check-date', data, CommonCons.getHttpOptions());
  }

  listHolidayManagement(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'holiday-management/datatable', this.paramsHolidayManagement, CommonCons.getHttpOptions());
  }

  detailPermission(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'leave-request/' + id, CommonCons.getHttpOptions());
  }


  detailResourceReplacement(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'resources-replacement/get-by-task-type-task-id?taskType=2&taskId=' + id, CommonCons.getHttpOptions());
  }

  updatePermission(id ,data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'leave-request/' + id, data, CommonCons.getHttpOptions());
  }

  deletePermission(data) {
    return this.http.delete<DataResponse>(this.prefixUrl + 'leave-request', CommonCons.deleteHttpOptions(data));
  }

  resourceAll(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'resources/all-datatable', this.paramsLeaveRequest, CommonCons.getHttpOptions());
  }

  updateDataUserStaf(id ,data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrlAuth + 'user/' + id, data, CommonCons.getHttpOptions());
  }

  updateDataResourceStaf(id ,data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'resources/' + id, data, CommonCons.getHttpOptions());
  }

  listAwp(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlAwp + 'component/datatable', this.paramsDtAwp, CommonCons.getHttpOptions());
  }


  listPdo(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'pdo/datatable', this.paramsDt, CommonCons.getHttpOptions());
  }

  listIri(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'iri/datatable', this.paramsDt, CommonCons.getHttpOptions());
  }

  listTag(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'task-resources-tag/datatable', this.paramsDt, CommonCons.getHttpOptions());
  }

  upload(fileToUpload: File, value): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', value.toString());
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  saveTask(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'resources-task', data, CommonCons.getHttpOptions());
  }

  detailAddTask(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'resources-task/' + id, CommonCons.getHttpOptions());
  }

  editTask(id, data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'resources-task/'+id, data, CommonCons.getHttpOptions());
  }

  exportPdf(id: any) {
    return new Observable<boolean>(observer => {
      this.http.get(this.prefixUrlUpload + '/download?id=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
          (response: any) => {
            const dataType = response.type;
            const binaryData = [];
            binaryData.push(response);
            const downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

            downloadLink.setAttribute('download', 'Pdf Data Laporan Document .pdf');
            document.body.appendChild(downloadLink);
            downloadLink.click();
            observer.next(true);
          }, error => {
            observer.error(false);
          }
      );
    });
  }

  addNewComment(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'resources-task/create-comment', data, CommonCons.getHttpOptions());
  }

  changeStatusReadTask(data) {
    return this.http.put<DataResponse>(this.prefixUrl + 'task-notification/update-by-task-id-task-type', data, CommonCons.getHttpOptions());
  }

  deleteTask(data) {
    return this.http.delete<DataResponse>(this.prefixUrl + 'resources-task', CommonCons.deleteHttpOptions(data));
  }

  createResources(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'resources', data, CommonCons.getHttpOptions());
  }

  updateResources(id, data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'resources/' + id, data, CommonCons.getHttpOptions());
  }

  listSupervisorByComponent(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'resources/supervisior-by-component-role', data ,CommonCons.getHttpOptions());
  }

  detailEvent(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrlAwp + 'event/' + id, CommonCons.getHttpOptions());
  }

}
