import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class FileService {

  prefixUrl = environment.serverFile

  constructor(private http: HttpClient) { }

  getToken() {
    return 'Bearer ' + localStorage.getItem('accessToken');
  }

  getUrlPhoto(id) {
    return this.http.get( this.prefixUrl+'/'+id,
        {headers: new HttpHeaders({
                'Authorization': this.getToken(),
                'Accept': 'image/png,image/jpeg,image/jpg'
              },
          ), responseType: 'blob'})
  }

    getUrlPdf(id) {
        return this.http.get( this.prefixUrl+'/'+id,
            {headers: new HttpHeaders({
                        'Authorization': this.getToken(),
                        'Accept': 'application/octet-stream'
                    },
                ), responseType: 'blob'})
    }

    getUrlWordExcel(id) {
        return this.http.get( this.prefixUrl+'/pdf/'+id,
            {headers: new HttpHeaders({
                        'Authorization': this.getToken(),
                        'Accept': 'application/octet-stream'
                    },
                ), responseType: 'blob'})
    }
}
