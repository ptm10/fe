import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  prefixUrl = environment.serverAuth
  prefixUrlAwp = environment.serverAwp

  constructor(private http: HttpClient) { }

  loginTableu(): Observable<any> {
    return this.http.get<any>(this.prefixUrl + 'auth/tableau-token', CommonCons.getHttpTableu());
  }

  calenderEvent(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlAwp + 'event/schedule', data,  CommonCons.getHttpOptions());
  }

  getTotalEvent(): Observable<any> {
    return this.http.get<any>(this.prefixUrlAwp + 'event/self-event-by-status', CommonCons.getHttpOptions());
  }


}
