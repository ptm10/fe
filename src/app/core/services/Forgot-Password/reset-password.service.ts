import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {RequestLogin} from "../../models/login.models";
import {DataResponse} from "../../models/dataresponse.models";
import {Observable} from "rxjs/Rx";

@Injectable({
  providedIn: 'root'
})
export class ResetPasswordService {

  private prefixUrl = environment.serverAuth;

  constructor(private http: HttpClient) {}

  resetPassword(dataRequest: any): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'auth/forgot-password', dataRequest);
  }
}
