import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Environment} from "@angular/cli/lib/config/workspace-schema";
import {environment} from "../../../../environments/environment";
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApplicationPermissionService {

  prefixUrl = environment.serverResource

  constructor(private http: HttpClient) { }

  detailApprovePermission(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'leave-request/' + id, CommonCons.getHttpOptions());
  }

  detailResourceReplacement(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'resources-replacement/get-by-task-type-task-id?taskType=2&taskId=' + id, CommonCons.getHttpOptions());
  }

  approveRejectPermission(id): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'leave-request/approve', id, CommonCons.getHttpOptions());
  }

  updateStatusTask(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'task/update-by-task-id-task-type', data, CommonCons.getHttpOptions());
  }

}
