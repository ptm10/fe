import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class QuestionareService {

  private prefixUrl = environment.serverAwp;

  paramsQuestion = {
    enablePage: false,
    // sort: [
    //   {
    //     field: "questionCategory",
    //     direction: "asc"
    //   },
    //   {
    //     field: "questionType",
    //     direction: "asc"
    //   }
    // ]
  }


  constructor(private http: HttpClient) { }

  listQuestionnare(eventId, createdFor): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'public/event-question/' + eventId + '/' + createdFor, this.paramsQuestion);
  }

  saveQuestionnare(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'public/event-question-asnwer', data);
  }
}
