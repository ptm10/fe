import { Injectable } from '@angular/core';
import {Environment} from "@angular/cli/lib/config/workspace-schema";
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Rx";
import {DataResponse} from "../../models/dataresponse.models";
import {RequestLogin} from "../../models/login.models";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private prefixUrl = environment.serverAuth;

  constructor(private http: HttpClient) {}

  loginDashboard(dataRequest: RequestLogin): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'auth/login', dataRequest);
  }

  confirmUserPassword(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'auth/confirm-change-email', data);
  }

  checkEmailValidatePosition(data, identity): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'auth/get-change-email-by-id-identity?id=' + data + '&identity=' + identity);
  }


}
