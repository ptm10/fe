import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {Observable} from "rxjs/Rx";

@Injectable({
  providedIn: 'root'
})
export class ListTaskService {

  prefixUrl = environment.serverAuth
  prefixUrlResource = environment.serverResource

  constructor(
      private http: HttpClient
  ) { }

  detailTask(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'approval/' + id, CommonCons.getHttpOptions());
  }

  acceptTask(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'approval',data,  CommonCons.getHttpOptions());
  }

  updateStatusTask(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrlResource + 'task/update-by-task-id-task-type', data, CommonCons.getHttpOptions());
  }
}
