import { TestBed } from '@angular/core/testing';

import { AwpService } from './awp.service';

describe('AwpService', () => {
  let service: AwpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AwpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
