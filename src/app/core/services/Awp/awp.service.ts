import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {DataResponse} from "../../models/dataresponse.models";
import {Observable} from "rxjs/Rx";
import {CommonCons} from "../../../shared/CommonCons";

@Injectable({
  providedIn: 'root'
})
export class AwpService {

  private prefixUrl = environment.serverAwp;
  private prefixUrlResource = environment.serverResource
  paramsEMT = {
    enablePage: false,
    sort: [
      {
        field: "name",
        direction: "asc"
      }
    ]
  }

  paramsEventType = {
    enablePage: false,
    sort: [
      {
        field: "type",
        direction: "asc"
      },
      {
        field: "name",
        direction: "asc"
      },
    ]
  }

  paramsQuestionEvent = {
      enablePage: false,
      sort: [
          {
              field: "question",
              direction: "asc"
          }
      ]
  }

  paramsDtForm = {
      enablePage: false,
      sort: [
          {
              field: "eventType.name",
              direction: "asc"
          }
      ]
  }


  constructor(private http: HttpClient) {}

  listYear(): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'awp/list-year', CommonCons.getHttpOptions());
  }

  detailAwp(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'awp/' + id, CommonCons.getHttpOptions());
  }

  eventManagementType(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event-management-type/datatable', this.paramsEMT,  CommonCons.getHttpOptions());
  }

  province(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'province/datatable', this.paramsEMT,  CommonCons.getHttpOptions());
  }

  eventType(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event-type/datatable', this.paramsEventType,  CommonCons.getHttpOptions());
  }

  pdo(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlResource + 'pdo/datatable', this.paramsEMT,  CommonCons.getHttpOptions());
  }

  iri(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlResource + 'iri/datatable', this.paramsEMT,  CommonCons.getHttpOptions());
  }

  city(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'regency/datatable', data,  CommonCons.getHttpOptions());
  }

  saveAwp(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'awp', data,  CommonCons.getHttpOptions());
  }

  updateAwp(id ,data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp/' + id, data,  CommonCons.getHttpOptions());
  }

  calenderEvent(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event/schedule', data,  CommonCons.getHttpOptions());
  }

    calenderActivity(data): Observable<DataResponse> {
        return this.http.post<DataResponse>(this.prefixUrl + 'awp-implementation/schedule', data,  CommonCons.getHttpOptions());
    }

  listModa() : Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'activity-mode/datatable', this.paramsEMT,  CommonCons.getHttpOptions());
  }

  listAllComponent(): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'component/group-list', CommonCons.getHttpOptions());
  }

  deleteAwp(id) {
    return this.http.delete<DataResponse>(this.prefixUrl + 'awp/' + id, CommonCons.getHttpOptions());
  }


  listResultContribution(data) : Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'monev/datatable-contribution', data,  CommonCons.getHttpOptions());
  }

  listPdoIri(): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrlResource + 'pdo/with-iri', CommonCons.getHttpOptions());
  }

  listAwpInformation(year): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'awp-data/by-year/' + year, CommonCons.getHttpOptions());
  }

  editInformationAwp(year, data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-data/' + year, data,  CommonCons.getHttpOptions());
  }

  listQuestionMonev() : Observable<DataResponse> {
      return this.http.post<DataResponse>(this.prefixUrl + 'event-question/datatable', this.paramsQuestionEvent,  CommonCons.getHttpOptions());
  }

  listQuestionMonevAll() : Observable<DataResponse> {
      return this.http.get<DataResponse>(this.prefixUrl + 'event-question/existing-questions',  CommonCons.getHttpOptions());
  }

  createQuestionEvent(data) : Observable<DataResponse> {
      return this.http.post<DataResponse>(this.prefixUrl + 'event-form', data,  CommonCons.getHttpOptions());
  }

  updateQuestionEvent(data, id) : Observable<DataResponse> {
      return this.http.put<DataResponse>(this.prefixUrl + 'event-form/' + id, data,  CommonCons.getHttpOptions());
  }

  deleteQuestionEvent(id) {
      return this.http.delete<DataResponse>(this.prefixUrl + 'event-form/' + id, CommonCons.getHttpOptions());
  }

  detailFormQuestionEvent(data) : Observable<DataResponse> {
      return this.http.get<DataResponse>(this.prefixUrl + 'event-form/' + data, CommonCons.getHttpOptions());
  }

  listAllForm() : Observable<DataResponse> {
      return this.http.post<DataResponse>(this.prefixUrl + 'event-form/datatable', this.paramsDtForm, CommonCons.getHttpOptions());
  }

  listEventFormLsp(data) : Observable<DataResponse> {
      return this.http.post<DataResponse>(this.prefixUrl + 'event/datatable-event-lsp', data, CommonCons.getHttpOptions());
  }

  uploadFormUser(data) : Observable<DataResponse> {
      return this.http.post<DataResponse>(this.prefixUrl + 'event/upload-form-user', data, CommonCons.getHttpOptions());
  }

  resultFormUser(eventId ,createdFor) : Observable<DataResponse> {
      return this.http.get<DataResponse>(this.prefixUrl + 'event-form-excel-file/result?eventId='+ eventId +'&createdFor='+ createdFor, CommonCons.getHttpOptions());
  }

  exportWordAwp(id: any, name: any) {
    return new Observable<boolean>(observer => {
      this.http.get(this.prefixUrl + 'awp/download-word?year=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
          (response: any) => {
            const dataType = response.type;
            const binaryData = [];
            binaryData.push(response);
            const downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

            downloadLink.setAttribute('download', name + '.docx');
            document.body.appendChild(downloadLink);
            downloadLink.click();
            observer.next(true);
          }, error => {
            observer.error(false);
          }
      );
    });
  }

    exportWordReportProject(data) {
        return new Observable<boolean>(observer => {
            this.http.post(this.prefixUrl + 'event-report/project-report', data, CommonCons.getHttpDownloadOptions()).subscribe (
                (response: any) => {
                    const dataType = response.type;
                    const binaryData = [];
                    binaryData.push(response);
                    const downloadLink = document.createElement('a');
                    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

                    downloadLink.setAttribute('download',  'Laporan Proyek.docx');
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    observer.next(true);
                }, error => {
                    observer.error(false);
                }
            );
        });
    }

  exportExcelAwp(data) {
    return new Observable<boolean>(observer => {
      this.http.post(this.prefixUrl + 'awp/download-excel', data, CommonCons.getHttpDownloadOptions()).subscribe (
          (response: any) => {
            const dataType = response.type;
            const binaryData = [];
            binaryData.push(response);
            const downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

            downloadLink.setAttribute('download', 'Excel AWP.xlsx');
            document.body.appendChild(downloadLink);
            downloadLink.click();
            observer.next(true);
          }, error => {
            observer.error(false);
          }
      );
    });
  }


  exportWordAwpConceptNote(id: any, name: any) {
    return new Observable<boolean>(observer => {
      this.http.get(this.prefixUrl + 'event/download-word?id=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
          (response: any) => {
            const dataType = response.type;
            const binaryData = [];
            binaryData.push(response);
            const downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

            downloadLink.setAttribute('download', name + '.docx');
            document.body.appendChild(downloadLink);
            downloadLink.click();
            observer.next(true);
          }, error => {
            observer.error(false);
          }
      );
    });
  }

    exportWordAwpReportActivity(id: any, name: any) {
        return new Observable<boolean>(observer => {
            this.http.get(this.prefixUrl + 'awp-implementation/download-word?id=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
                (response: any) => {
                    const dataType = response.type;
                    const binaryData = [];
                    binaryData.push(response);
                    const downloadLink = document.createElement('a');
                    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

                    downloadLink.setAttribute('download', name + '.docx');
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    observer.next(true);
                }, error => {
                    observer.error(false);
                }
            );
        });
    }

    exportWordTemplateForm() {
        return new Observable<boolean>(observer => {
            this.http.get(this.prefixUrl + 'event/download-template-form-user', CommonCons.getHttpDownloadOptions()).subscribe (
                (response: any) => {
                    const dataType = response.type;
                    const binaryData = [];
                    binaryData.push(response);
                    const downloadLink = document.createElement('a');
                    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

                    downloadLink.setAttribute('download',  'Template_Email.xlsx');
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    observer.next(true);
                }, error => {
                    observer.error(false);
                }
            );
        });
    }


}
