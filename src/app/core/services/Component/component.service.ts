import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {DataResponse} from "../../models/dataresponse.models";
import {Observable} from "rxjs/Rx";
import {CommonCons} from "../../../shared/CommonCons";

@Injectable({
  providedIn: 'root'
})
export class ComponentService {

  private prefixUrl = environment.serverAwp;

  constructor(private http: HttpClient) {}

  listComponent(data): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'component/group-list?year=' + data, CommonCons.getHttpOptions());
  }

  listComponentProjectComponent(year): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'component/group-list?year=' + year, CommonCons.getHttpOptions());
  }
}
