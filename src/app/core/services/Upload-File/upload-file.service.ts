import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs/Rx";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  paramsDt = {
    enablePage: false,
  }

  paramsTag = {
    enablePage: false,
    sort: [
      {
        field: "name",
        direction: "asc"
      }
    ]
  }

  private prefixUrl = environment.serverFile
  private prefixUrlKnowledge = environment.serverKnowledge
  private prefixUrlAwp = environment.serverAwp
  private prefixUrlResource = environment.serverResource

  getToken() {
    return 'Bearer ' + localStorage.getItem('accessToken');
  }

  constructor(private http: HttpClient) { }

  paramsDtAwp = {
    enablePage: false,
    sort: [
      {
        field: "code",
        direction: "asc"
      }
    ]
  }

  paramsHolidayManagement = {
    enablePage: false,
    sort: [
      {
        field: "holidayDate",
        direction: "asc"
      }
    ]
  }

  upload(fileToUpload: File, fileNumber): Observable<any> {
    const endpoint = this.prefixUrl;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*',
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', fileNumber);
    return this.http.post<any>(endpoint, formData, {
      headers: headers,
      reportProgress: true,
      observe: 'events',
    });
  }

  listDocumentType(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlKnowledge + 'documents-type/datatable', this.paramsDt, CommonCons.getHttpOptions());
  }

  saveUploadFile(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlKnowledge + 'general-documents', data, CommonCons.getHttpOptions());
  }

  listDocumentClassification(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlKnowledge + 'documents-classification/datatable', this.paramsDt, CommonCons.getHttpOptions());
  }

  listUnit(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlAwp + 'component/datatable', this.paramsDtAwp, CommonCons.getHttpOptions());
  }

  listHolidayManagement(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlResource + 'holiday-management/datatable', this.paramsHolidayManagement, CommonCons.getHttpOptions());
  }

  listTag(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlKnowledge + 'general-documents-tag/datatable', this.paramsTag, CommonCons.getHttpOptions());
  }
}
