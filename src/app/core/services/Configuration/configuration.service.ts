import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {Observable} from "rxjs/Rx";

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  private prefixUrl = environment.serverAuth
  private prefixUrlUpload = environment.serverFile
  private prefixUrlAwp = environment.serverAwp
  private prefixResources = environment.serverResource

  constructor(private http: HttpClient) {}

  paramsDtAwp = {
    enablePage: false,
    sort: [
      {
        field: "code",
        direction: "asc"
      }
    ]
  }

  paramsDtRoles = {
    enablePage: false,
    sort: [
      {
        field: "code",
        direction: "asc"
      }
    ]
  }

  getToken() {
    return 'Bearer ' + localStorage.getItem('accessToken');
  }

  detailConfiguration(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'user/' + id, CommonCons.getHttpOptions());
  }

  upload(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '4');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  listRole(): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'auth/roles?enablePage=0&page=0&size=0&sort=asc&sortBy=name', CommonCons.getHttpOptions());
  }

  updateConfiguration(id, data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'user/' + id, data, CommonCons.getHttpOptions());
  }

  listAwp(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlAwp + 'component/datatable', this.paramsDtAwp, CommonCons.getHttpOptions());
  }

  listRoleDt(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'role/datatable', this.paramsDtRoles ,CommonCons.getHttpOptions());
  }

  listSupervisorByComponent(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixResources + 'resources/supervisior-by-component-role', data ,CommonCons.getHttpOptions());
  }

  listKegiatanLsp(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlAwp + 'awp-implementation/datatable-all', data ,CommonCons.getHttpOptions());
  }

}
