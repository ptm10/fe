import { Injectable } from '@angular/core';
import {DataResponse} from "../models/dataresponse.models";
import {CommonCons} from "../../shared/CommonCons";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class KegiatanService {

  private prefixUrlUpload = environment.serverFile

  getToken() {
    return 'Bearer ' + localStorage.getItem('accessToken');
  }

  paramsReportKegiatan = {
    enablePage: false,
    sort: [
      {
        field: "createdAt",
        direction: "desc"
      }
    ]
  }

  paramsEMT = {
    enablePage: false,
    sort: [
      {
        field: "name",
        direction: "asc"
      }
    ]
  }

  paramsQuality = {
    enablePage: false,
    sort: [
      {
        field: "number",
        direction: "asc"
      }
    ]
  }

  paramsLeaveRequest = {
    enablePage: false,
    sort: [
      {
        field: "user.firstName",
        direction: "asc"
      }
    ]
  }

  paramsDtAwp = {
    enablePage: false,
    sort: [
      {
        field: "code",
        direction: "asc"
      }
    ]
  }

  private prefixUrl = environment.serverAwp;
  private prefixUrlResource = environment.serverResource
  private prefixUrlAuth = environment.serverAuth

  constructor(private http: HttpClient) { }

  detailKegiatan(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'awp-implementation/' + id, CommonCons.getHttpOptions());
  }

  detailReportKegiatan(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'awp-implementation-report/' + id, CommonCons.getHttpOptions());
  }

  eventManagementType(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event-management-type/datatable', this.paramsEMT,  CommonCons.getHttpOptions());
  }

  projectOfficer(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlResource + 'resources/all-datatable', data,  CommonCons.getHttpOptions());
  }

  uploadInformationAwp(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '4');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  upload(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '8');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  uploadKegiatan(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '9');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  uploadAttachment(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '11');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  uploadDocumentation(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '12');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  uploadKegiatanRaa(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '13');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  uploadFileEmailParticipant(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '15');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }


  uploadFileBast(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '16');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }

  saveImplementation(id ,data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-implementation/planning/' + id,data,  CommonCons.getHttpOptions());
  }

  updateFromCoordinator(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-implementation/approve-by-coordinator',data,  CommonCons.getHttpOptions());
  }

  updateFromPmu(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-implementation/approve-by-pmu',data,  CommonCons.getHttpOptions());
  }


  exportFile(id: any, name: string) {
    return new Observable<boolean>(observer => {
      this.http.get(this.prefixUrlUpload + '/download?id=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
          (response: any) => {
            const dataType = response.type;
            const binaryData = [];
            binaryData.push(response);
            const downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

            downloadLink.setAttribute('download',  name);
            document.body.appendChild(downloadLink);
            downloadLink.click();
            observer.next(true);
          }, error => {
            observer.error(false);
          }
      );
    });
  }

  updateStatusTask(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrlResource + 'task/update-by-task-id-task-type', data, CommonCons.getHttpOptions());
  }

  qualityAtEntry(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'quality-at-entry-question/datatable', this.paramsQuality,  CommonCons.getHttpOptions());
  }

  resourceAll(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlResource + 'resources/all-datatable', this.paramsLeaveRequest, CommonCons.getHttpOptions());
  }

  createCn(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event', data, CommonCons.getHttpOptions());
  }

  listAwp(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'component/datatable', this.paramsDtAwp, CommonCons.getHttpOptions());
  }

  listLsp(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrlAuth + 'user/datatable-lsp', data, CommonCons.getHttpOptions());
  }

  listEventLsp(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'awp-implementation/datatable-lsp-choice', data, CommonCons.getHttpOptions());
  }

  listAllEvent(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event-report/datatable', data, CommonCons.getHttpOptions());
  }

  listEventReport(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event-report/datatable', data, CommonCons.getHttpOptions());
  }

  listCodePok(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'pok/datatable', this.paramsDtAwp, CommonCons.getHttpOptions());
  }

  addKegiatanReport(params): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'awp-implementation-report', params, CommonCons.getHttpOptions());
  }

  listReportKegiatanStaff(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'awp-implementation-report/datatable', data, CommonCons.getHttpOptions());
  }

  listReportKegiatanLsp(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'awp-implementation-report/datatable', data, CommonCons.getHttpOptions());
  }

  detailActivityReport(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'awp-implementation-report/' + id, CommonCons.getHttpOptions());
  }

  updateActivityReport(id, data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-implementation-report/' + id, data, CommonCons.getHttpOptions());
  }

  updateFromConsultantActivityReport(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-implementation-report/approve-by-consultan', data, CommonCons.getHttpOptions());
  }

  updateFromCoordinatorActivityReport(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-implementation-report/approve-by-coordinator', data, CommonCons.getHttpOptions());
  }

  updateFromTreasureActivityReport(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-implementation-report/approve-by-treasurer-pmu', data, CommonCons.getHttpOptions());
  }

  updateFromHeadPmuActivityReport(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-implementation-report/approve-by-head-pmu', data, CommonCons.getHttpOptions());
  }

  updateLspActivityReport(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'awp-implementation-report/approve', data, CommonCons.getHttpOptions());
  }

  listResultChain(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'monev/datatable-result-chain', data, CommonCons.getHttpOptions());
  }

  listIndicatorCode(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'monev/datatable-success-indicator', data, CommonCons.getHttpOptions());
  }

  listResultChainAll(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'monev-result-chain/datatable', data, CommonCons.getHttpOptions());
  }

  listRiskInRik(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'rik-risk/datatable', this.paramsEMT, CommonCons.getHttpOptions());
  }
}
