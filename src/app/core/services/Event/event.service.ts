import { Injectable } from '@angular/core';
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EventService {

  paramsHolidayManagement = {
    enablePage: false,
    sort: [
      {
        field: "holidayDate",
        direction: "asc"
      }
    ]
  }
  paramsListActivityDt = {
    enablePage: false,
    sort: [
      {
        field: "name",
        direction: "asc"
      }
    ]
  }

  paramsAllUsers = {
    enablePage: false
  }



  prefixUrl = environment.serverResource
  prefixUrlAwp = environment.serverAwp
  prefixUrlFiles = environment.serverFile

  constructor(private http: HttpClient) { }

  listHolidayManagement(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'holiday-management/datatable', this.paramsHolidayManagement, CommonCons.getHttpOptions());
  }

  listEventActivity(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event/datatable', this.paramsListActivityDt, CommonCons.getHttpOptions());
  }

  listAllUserResources(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'resources/all-datatable', this.paramsAllUsers, CommonCons.getHttpOptions());
  }

  listTransportation(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'transportation/datatable', this.paramsListActivityDt, CommonCons.getHttpOptions());
  }

  saveEvent(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'out-of-town-event', data, CommonCons.getHttpOptions());
  }

  getDetailEvent(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'out-of-town-event/' + id, CommonCons.getHttpOptions());
  }

  approveRejectEvent(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'out-of-town-event/approve', data, CommonCons.getHttpOptions());
  }

  updateStatusTask(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'task/update-by-task-id-task-type', data, CommonCons.getHttpOptions());
  }

  getResultAllQuestionParticipant(data): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrlAwp + 'event-question-answer/result-participant?eventId='+ data, CommonCons.getHttpOptions());
  }

  getResultAllQuestionEvaluator(data): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrlAwp + 'event-question-answer/result-evaluator?eventId='+ data, CommonCons.getHttpOptions());
  }

  getDetailsFile(data): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrlFiles + '/get-by-id?id=' + data, CommonCons.getHttpOptions());
  }
}
