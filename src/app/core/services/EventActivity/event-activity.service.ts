import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";

@Injectable({
  providedIn: 'root'
})
export class EventActivityService {

  paramsDtEvaluationPoint = {
      enablePage: false,
      sort: [
          {
              field: "name",
              direction: "asc"
          }
      ]
  }

  prefixUrl = environment.serverAwp
  private prefixUrlResource = environment.serverResource
  private prefixUrlUpload = environment.serverFile

  constructor(private http: HttpClient) { }

  detailEvent(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'event/' + id, CommonCons.getHttpOptions());
  }

  detailEventLastReport(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'event-progress-report/last-report?eventId=' + id, CommonCons.getHttpOptions());
  }

  getListActivity(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'awp-implementation/datatable-concept-note', data, CommonCons.getHttpOptions());
  }

  updateCn(id ,data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'event/' + id, data, CommonCons.getHttpOptions());
  }

  updateFromCoordinator(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'event/approve-by-coordinator', data, CommonCons.getHttpOptions());
  }

  updateFromTreasure(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'event/approve-by-treasurer-pmu', data, CommonCons.getHttpOptions());
  }

  updateFromHeadPmu(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'event/approve-by-head-pmu', data, CommonCons.getHttpOptions());
  }

  updateStatusTask(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrlResource + 'task/update-by-task-id-task-type', data, CommonCons.getHttpOptions());
  }

  updateFromConsultantEventProgress(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'event-progress-report/approve-by-consultan', data, CommonCons.getHttpOptions());
  }

  exportFile(id: any, name: string) {
    return new Observable<boolean>(observer => {
      this.http.get(this.prefixUrlUpload + '/download?id=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
          (response: any) => {
            const dataType = response.type;
            const binaryData = [];
            binaryData.push(response);
            const downloadLink = document.createElement('a');
            downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

            downloadLink.setAttribute('download',  name);
            document.body.appendChild(downloadLink);
            downloadLink.click();
            observer.next(true);
          }, error => {
            observer.error(false);
          }
      );
    });
  }

  createEventProgress(data) {
      return this.http.post<DataResponse>(this.prefixUrl + 'event-progress-report', data, CommonCons.getHttpOptions());
  }
  updateEventProgress(id, data) {
      return this.http.put<DataResponse>(this.prefixUrl + 'event-progress-report/' + id, data, CommonCons.getHttpOptions());
  }

  getListEventProgress(data): Observable<DataResponse> {
     return this.http.post<DataResponse>(this.prefixUrl + 'event-progress-report/datatable', data, CommonCons.getHttpOptions());
  }
  getListEventReport(data): Observable<DataResponse> {
     return this.http.post<DataResponse>(this.prefixUrl + 'event-report/datatable', data, CommonCons.getHttpOptions());
  }

  getListEventProgressPcu(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event-progress-report/datatable-all', data, CommonCons.getHttpOptions());
  }

  getListEventReportPcu(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'event-report/datatable-all', data, CommonCons.getHttpOptions());
  }

  detailEventProgress(id) {
     return this.http.get<DataResponse>(this.prefixUrl + 'event-progress-report/' + id, CommonCons.getHttpOptions());
  }

  detailEventReport(id) {
     return this.http.get<DataResponse>(this.prefixUrl + 'event-report/' + id, CommonCons.getHttpOptions());
  }

  /** POST save event */
  createEvent(data) {
    return this.http.post<DataResponse>(this.prefixUrl + 'event', data, CommonCons.getHttpOptions());
  }

  /** PUT update event */
  updateEvent(id, data){
    return this.http.put<DataResponse>(this.prefixUrl + 'event/' + id, data, CommonCons.getHttpOptions());
  }

  createEventReport(data) {
    return this.http.post<DataResponse>(this.prefixUrl + 'event-report', data, CommonCons.getHttpOptions());
  }

  /** PUT update event-report */
  updateEventReport(id ,data) {
      return this.http.put<DataResponse>(this.prefixUrl + 'event-report/' + id, data, CommonCons.getHttpOptions());
  }

  getListEventEvaluation(): Observable<DataResponse> {
     return this.http.post<DataResponse>(this.prefixUrl + 'event-evaluation-point/datatable', this.paramsDtEvaluationPoint, CommonCons.getHttpOptions());
  }

  getListDocumentType(): Observable<DataResponse> {
      return this.http.post<DataResponse>(this.prefixUrl + 'event-document-type/datatable', this.paramsDtEvaluationPoint, CommonCons.getHttpOptions());
  }

  getListParticipantEvent(): Observable<DataResponse> {
     return this.http.post<DataResponse>(this.prefixUrl + 'participant-target-position/datatable', this.paramsDtEvaluationPoint, CommonCons.getHttpOptions());
  }

  updateFromConsultantEventReport(data): Observable<DataResponse> {
     return this.http.put<DataResponse>(this.prefixUrl + 'event-report/approve-by-consultan', data, CommonCons.getHttpOptions());
  }

  updateFromCoordinatorEventReport(data): Observable<DataResponse> {
     return this.http.put<DataResponse>(this.prefixUrl + 'event-report/approve-by-coordinator', data, CommonCons.getHttpOptions());
  }

  updateFromTreasureEventReport(data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'event-report/approve-by-treasurer-pmu', data, CommonCons.getHttpOptions());
  }

  updateFromHeadPmuEventReport(data): Observable<DataResponse> {
   return this.http.put<DataResponse>(this.prefixUrl + 'event-report/approve-by-head-pmu', data, CommonCons.getHttpOptions());
  }

  getListEventQuestion(data): Observable<DataResponse> {
   return this.http.post<DataResponse>(this.prefixUrl + 'event-report/datatable-question', data, CommonCons.getHttpOptions());
  }

  getListEventAddQuestion(data): Observable<DataResponse> {
   return this.http.post<DataResponse>(this.prefixUrl + 'event-question/datatable', data, CommonCons.getHttpOptions());
  }

    exportWordAwpEventReport(id: any, name: any) {
        return new Observable<boolean>(observer => {
            this.http.get(this.prefixUrl + 'event-report/download-word?id=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
                (response: any) => {
                    const dataType = response.type;
                    const binaryData = [];
                    binaryData.push(response);
                    const downloadLink = document.createElement('a');
                    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

                    downloadLink.setAttribute('download', name + '.docx');
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    observer.next(true);
                }, error => {
                    observer.error(false);
                }
            );
        });
    }

    exportFilesFromOpenDoc(id: any, name: any, ext: any) {
        return new Observable<boolean>(observer => {
            this.http.get(this.prefixUrlUpload + '/download?id=' + id, CommonCons.getHttpDownloadOptions()).subscribe (
                (response: any) => {
                    const dataType = response.type;
                    const binaryData = [];
                    binaryData.push(response);
                    const downloadLink = document.createElement('a');
                    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, {type: dataType}));

                    downloadLink.setAttribute('download', name + '.' + ext);
                    document.body.appendChild(downloadLink);
                    downloadLink.click();
                    observer.next(true);
                }, error => {
                    observer.error(false);
                }
            );
        });
    }


}
