import { TestBed } from '@angular/core/testing';

import { MonevService } from './monev.service';

describe('MonevService', () => {
  let service: MonevService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MonevService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
