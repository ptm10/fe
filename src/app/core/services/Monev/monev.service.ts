import { Injectable } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {DataResponse} from "../../models/dataresponse.models";
import {CommonCons} from "../../../shared/CommonCons";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MonevService {

  prefixUrl = environment.serverAwp
  prefixUrlResource = environment.serverResource
  private prefixUrlUpload = environment.serverFile


  getToken() {
    return 'Bearer ' + localStorage.getItem('accessToken');
  }

  paramsDtQuestion = {
    enablePage: false,
    sort: [
      {
        field: "createdAt",
        direction: "desc"
      }
    ]
  }

  constructor(
      private http: HttpClient
  ) { }

  detailMonev(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'monev/' + id, CommonCons.getHttpOptions());
  }

  detailMonevResultChain(id): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'monev-result-chain/' + id, CommonCons.getHttpOptions());
  }

  updateMonev(id ,data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'monev-result-chain/' + id, data, CommonCons.getHttpOptions());
  }

  updateResult(id ,data): Observable<DataResponse> {
    return this.http.put<DataResponse>(this.prefixUrl + 'monev-result-chain/update-result/' + id, data, CommonCons.getHttpOptions());
  }

  listChild(id, year): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'monev/child?id=' + id + '&year=' + year, CommonCons.getHttpOptions());
  }

  listAllMonev(year): Observable<DataResponse> {
    return this.http.get<DataResponse>(this.prefixUrl + 'monev-result-chain/group-list?year=' + year, CommonCons.getHttpOptions());
  }

  listAllQuestion(): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'monev-question/datatable', this.paramsDtQuestion,  CommonCons.getHttpOptions());
  }

  createOutput(data): Observable<DataResponse> {
    return this.http.post<DataResponse>(this.prefixUrl + 'monev-result-chain', data,  CommonCons.getHttpOptions());
  }

  deleteResultChain(data): Observable<DataResponse> {
    return this.http.delete<DataResponse>(this.prefixUrl + 'monev-result-chain/' + data, CommonCons.getHttpOptions());
  }

  uploadResultMonev(fileToUpload: File): Observable<any> {
    const endpoint = this.prefixUrlUpload;
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      Authorization: this.getToken(),
      Accept: '*/*'
    });
    formData.append('files', fileToUpload, fileToUpload.name);
    formData.append('attachmentType', '14');
    return this.http.post<any>(endpoint, formData, {
      headers: headers
    });
  }
}
