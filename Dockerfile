FROM registry.impstudio.id/pub/docker-images:node16-stable

WORKDIR /opt/app

# Install dependencies
RUN apt update -y && apt install -y nginx

COPY . .

ARG BUILD_ENV=production

RUN npm install -g @angular/cli@latest
RUN npm install
RUN ng build --configuration ${BUILD_ENV}

COPY ./docker/default.conf /etc/nginx/sites-available/default

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
